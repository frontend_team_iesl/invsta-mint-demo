var width = 0;
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$('#email').keyup(function () {
    $('#error-email').text('');
});
$('#password').keyup(function () {
    $('#error-password').text('');
});
$('#fullName').keyup(function () {
    $('#error-fullName').text('');
});
$('#dob').change(function () {
    $('#error-dob').text('');
});
$('#inviteCode').keyup(function () {
    $('#error-inviteCode').text('');
});
$('#countryCode').keyup(function () {
    $('#error-countryCode').text('');
});
$('#mobileNo').keyup(function () {
    $('#error-mobileNo').text('');
});
$("input[type=radio]").click(function () {
    $('#error-generateOtp').text('');
});
$('#otp').keyup(function () {
    $('#error-generateOtp').text('');
});
function validate(ele) {
    current_fs = ele.parent();
    regexp = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\+\-\_\@\#\$\%\&\!\*\^\=\(\)\.\,]).{8,15})/;
    var id = current_fs.attr('id');
    if (id === 'first-fs') {
        var result = true;
        var email = document.getElementById('email');
        if (email.value === '') {
            document.getElementById('error-email').innerHTML = "Enter your email.";
            result = result && false;
        }
        var password = document.getElementById('password');
        if (password.value === '') {
            document.getElementById('error-password').innerHTML = "Enter your password.";
            result = result && false;
        }
        if (!regexp.test(password.value)) {
            document.getElementById('error-password').innerHTML = "Password must have one upper case letter, one lower case letter, one special character, one numeric character and length must be in 8 to 15 characters";
            result = result && false;
        }
        return result;
    } else if (id === 'second-fs') {
        var fullName = document.getElementById('fullName');
        var result = true;
        regnameexp = /^[a-zA-Z\s]+$/;
        if (fullName.value === '') {
            document.getElementById('error-fullName').innerHTML = "Enter your fullName.";
            result = result && false;
        }
        if (!regnameexp.test(fullName.value)) {
            document.getElementById('error-fullName').innerHTML = "Name should be in alphabetical characters";
            result = result && false;
        }
        var dob = document.getElementById('dob');
        if (dob.value === '') {
            document.getElementById('error-dob').innerHTML = "Enter your dob.";
            result = result && false;
        }
        return result;
    } else if (id === 'third-fs') {
        var inviteCode = document.getElementById('inviteCode');
        var result = true;
        if (inviteCode.value === '') {
            document.getElementById('error-inviteCode').innerHTML = "Enter your invite code.";
            result = result && false;
        }
        return result;
    } else if (id === 'fourth-fs') {
        var countryCode = document.getElementById('countryCode');
        var result = true;
        if (countryCode.value === '') {
            document.getElementById('error-countryCode').innerHTML = "Enter your country code.";
            result = result && false;
        }
        var mobileNo = document.getElementById('mobileNo');
        if (mobileNo.value === '') {
            document.getElementById('error-mobileNo').innerHTML = "Enter your mobile no.";
            result = result && false;
        }
        return result;
    }
    return true;
}
function validation() {
    var mobileNo = document.getElementById('mobileNo');
    var result = true;
    if (!($("#f-option").is(":checked") || $("#s-option").is(":checked"))) {
        document.getElementById('error-generateOtp').innerHTML = "Please choose one option to send OTP";
        result = result && false;
    }
    if (mobileNo.value === '') {
        document.getElementById('error-generateOtp').innerHTML = "Mobile number is required.";
        result = result && false;
    }
    return result;
}
function moveNextProcess(ele) {
    if (animating)
        return false;
    animating = true;
    current_fs = ele.parent();
    next_fs = ele.parent().next();
    //activate next step on progressbar using the index of next_fs
//    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
//    var elem = document.getElementById("myBar");
//    if (width < 100) {
//        width += 33;
//        elem.style.width = width + '%';
//    }
//hide the current fieldset with style
    current_fs.animate(
            {
                opacity: 0
            },
            {
                step: function (now, mx) {
//as the opacity of current_fs reduces to 0 - stored in "now"
//1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'relative'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 0,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
}

function movePrevProcess(ele) {
    if (animating)
        return false;
    animating = true;
    current_fs = ele.parent();
    previous_fs = ele.parent().prev();
    //de-activate current step on progressbar
//    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
//    var elem = document.getElementById("myBar");
//    if (width < 100) {
//        width = width - 33;
//        elem.style.width = width + '%';
//        //elem.innerHTML = width + '%';
//    }
//hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
//as the opacity of current_fs reduces to 0 - stored in "now"
//1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 0,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

$(".submit").click(function () {
    return false;
});

//jQuery.validator.addMethod("onespecial", function (value, element) {
//    var pattern = /^(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;
//    return (pattern.test(value));
//}, "Your password must contain 1 special character.");
///**
// * Custom validator for contains at least one upper-case letter.
// */
//$.validator.addMethod("OneUppercaseLetter", function (value, element) {
//    return this.optional(element) || /[A-Z]+/.test(value);
//}, "Must have at least one uppercase letter");

$("#msform").validate({
    rules: {
        email: "required",
        email: true,
        password: {
            required: true,
            minlength: 8,
            maxlength: 15,
            onespecial: true,
            OneUppercaseLetter: true
        },
        fullName: {
            required: true,
            minlength: 3,
            maxlength: 30
        },
        phone: {
            required: true,
            number: true,
            minlength: 5,
            maxlength: 20
        },
        dob: {
            required: true,
        },
        gender: {
            required: true
        }
        ,
        addres: {
            required: true,
        },
        income1: {
            required: true,
        },
        income2: {
            required: true,
        },
        bankName: {
            required: true,
        },
        accNumber: {
            required: true,
            number: false,
            maxlength: 18
        },
//            emotion:{
//              required: true,   
//            }

        tinRadio: {
            required: true,
        },
        ird: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Enter your Email",
            email: "Please enter a valid email address.",
        },
        password: {
            required: "Enter your password",
            pass: "Password should be minimum of 8 and maximum of 15 characters."
        },
        fullName: {
            required: "Enter your full name",
        },
        phone: {
            required: "Enter your phone/mobile number",
            number: "Please enter numeric value with no spaces"
        },
        dob: {
            required: "Enter your date of birth",
        },
        gender: {
            required: "Please select gender",
        },
        addres: {
            required: "Please enter address",
        },
        income1: {
            required: "Please enter amount",
        },
        income2: {
            required: "Please enter  amount",
        },
        bankName: {
            required: "Please enter  Bank Name",
        },
        accNumber: {
            required: "Please enter  Account Number.",
            number: "Please enter numeric value with no spaces.",
            maxlength: " maximum of 20 characters."
        },
//             emotion:{
//              required: "Please select atleast one option",   
//            }
    }
});
isNotEmpty = function (ele) {
    if (ele.val() !== "") {
        return true;
    } else {
        return false;
    }
};