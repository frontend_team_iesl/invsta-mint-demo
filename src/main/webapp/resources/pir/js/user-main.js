/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function otpGeneration(ele) {
    var mNo = $("#mobileNo").val();
    var cCo = $("#countryCode").val();
    var sTy = $('.senderType:checked').val();
    cCo = cCo.replace(/\+/g, "");
    var url = './rest/cryptolabs/api/sdjklfdfklffksdfksdk?mNo=' + mNo + '&cCo=' + cCo + '&sTy=' + sTy;
    document.getElementById('error-loader').style.display = 'block';
    document.getElementById('error-generateOtp').innerHTML = "";
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            // alert(data);
            $('#otp-block').show();
            document.getElementById('error-loader').style.display = 'none';
            document.getElementById('error-generateOtp').innerHTML = "Please check your Inbox";
            $('input:radio[name="senderType"]').prop("checked", false);
        },
        error: function (e) {
            var mobileNo = document.getElementById('mobileNo');
            var result = true;
            if (mobileNo.value === '') {
                document.getElementById('error-loader').style.display = 'none';
                document.getElementById('error-generateOtp').innerHTML = "Mobile number is required.";
                $('input:radio[name="senderType"]').prop("checked", false);
                result = result && false;
            } else {
                document.getElementById('error-loader').style.display = 'none';
                document.getElementById('error-generateOtp').innerHTML = "Please check your number and country code.";
                $('input:radio[name="senderType"]').prop("checked", false);
                result = result && false;
            }
            return result;
        }
    });
    return sTy;
}

function verifyOTP(ele) {
    var mNo = $("#mobileNo").val();
    var oTP = $("#onetimepassword").val();
    var url = './rest/cryptolabs/api/jdfslkfsdjlksdjlkjdflkds?mNo=' + mNo + '&otp=' + oTP;
    // alert(url);
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            var otp = $("#otp");
            otp.modal('hide');
            if (data.success === true) {
                $("#verify-otp-btn").hide();
                $("#continue-4to5-btn").show();
            }
        }
    });
}

//function balChart(type) {
//    var url = './rest/cryptolabs/api/balanceChart?type=' + type;
//    $.ajax({
//        url: url,
//        type: 'GET',
//        async: true,
//        dataType: "json",
//        success: function (data) {
////            alert(data);
////            $(".currentBalance").text('USD $' + data.totalBalance);
//            chart1(data);
//            if (type === 'today') {
//                $(".todayActive").addClass("active");
//            } else if (type === 'week') {
//                $(".weekActive").addClass("active");
//            } else if (type === 'month') {
//                $(".monthActive").addClass("active");
//            }
//        }
//    });
//}
// init line chart if element exists
//function chart1(data) {
//    var labels = data.labels;
//    var values = data.values;
//    var total = values[values.length - 1];
//    $(".currentBalance").text('USD $' + total);
//    Highcharts.chart('chart1', {
//        title: {
//            text: ''
//        },
//        subtitle: {
//            text: ''
//        },
//        exporting: {enabled: false},
//        credits: {
//            enabled: false
//        },
//        yAxis: {
//            title: {
//                text: ''
//            }
//        },
//        xAxis: {
//            categories: labels
//        },
//        legend: {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle'
//        },
//        plotOptions: {
//            line: {
//                dataLabels: {
//                    enabled: false
//                },
//                enableMouseTracking: true
//            }
//        },
//        series: [{
//                showInLegend: false,
//                name: 'Balance',
//                data: values
//            }],
//        responsive: {
//            rules: [{
//                    condition: {
//                        maxWidth: 500
//                    },
//                    chartOptions: {
//                        legend: {
//                            layout: 'horizontal',
//                            align: 'center',
//                            verticalAlign: 'bottom'
//                        }
//                    }
//                }]
//        }
//    });
//}

function balChart(type) {
    var url = './rest/cryptolabs/api/balanceChart?type=' + type;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            chart1(data);
        }
    });
}
// init line chart if element exists
function chart1(data) {
    var value = data.values[data.values.length - 1];
    var values = [];
    var time = (new Date()).getTime();
    var i = 0;
    for (i = -49; i <= 0; i += 1) {
        values.push({
            x: time + i * 1000,
            y: value + Math.random()
        });
    }
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    Highcharts.chart('chart1', {
        chart: {
            type: 'line',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(
                            function () {
                                var x = (new Date()).getTime();
                                var y = value + Math.random();
                                series.addPoint([x, y], true, true);
                                $(".currentBalance").text('USD $' + y.toFixed(2));
                            }
                    , 1000);
                }
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
                name: 'Live data',
                data: values
            }]
    });
}

function investmentsChart(db) {
    investmentsPieChart(db);
    investmentsLineChart(db);
}

function investmentsActual(idc, db, perf) {
    var investments = new Array();
    var actualAmount = 0;
    $.each(db, function (i, inv) {
        investments.push({
            name: inv.fundName,
            y: eval(inv.value)
        });
        actualAmount = actualAmount + eval(inv.value);
    });
    $('#actualInvested').text('USD $' + actualAmount.toFixed(2));
    var chart = new Highcharts.chart(idc, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {enabled: false},
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
                name: 'Investments',
                innerSize: '55%',
                colorByPoint: true,
                data: investments
            }]
    });
    centerText(chart, 'legend-1', perf);
}

function investmentsCurrent(idc, db) {
    var investments = new Array();
    var amount = 0;
    $.each(db, function (i, inv) {
        investments.push({
//            name: '(' + eval(i + 1) + ')' + inv.fundName,
//            name:  inv.fundName,
            y: eval(inv.value)
        });
        amount = amount + eval(inv.value);
    });
    $('#currentInvAmount').text(amount);

    Highcharts.chart(idc, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {enabled: false},
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.y:.1f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
                name: 'Investments',
                innerSize: '75%',
                colorByPoint: true,
                data: investments
            }]
    });
}

function investmentsLineChart(db) {
    var objs = new Array();
    var index = 0;
    var max = {
        id: 'lineChart',
        ilumony: new Array(),
        actual: new Array(),
        bank: new Array(),
        years: new Array()
    };
    $.each(db, function (i, inv) {
        var obj = interestChart(inv);
        if (obj.years > max.years) {
            max.ilumony = obj.ilumony;
            max.actual = obj.actual;
            max.bank = obj.bank;
            max.years = obj.years;
            index = i;
        }
        objs.push(obj);
    });
//    alert(max.actual);
    $.each(objs, function (i, obj) {
        if (i !== index) {
            $.each(max.years, function (j, year) {
                var ilumony = 0;
                var actual = 0;
                var bank = 0;
                if (obj.ilumony.length > j) {
                    ilumony = obj.ilumony[j];
                    actual = obj.actual[j];
                    bank = obj.bank[j];
                }
                max.ilumony[j] = max.ilumony[j] + ilumony;
                max.actual[j] = max.actual[j] + actual;
                max.bank[j] = max.bank[j] + bank;
            });
        }
    });
    lineChart(max.id, max.ilumony, max.actual, max.bank, max.years);
}

function interestChart(inv) {
    var currentmonth = new Date().getMonth() + 1;
    var currentYear = new Date().getYear() + 1900;
    var year = inv.years;
    var upFrontValue = inv.investmentAmount;
    var monthlyValue = inv.regularlyAmount;
    var investmentInterest = inv.interestRate;
    upFrontValue = upFrontValue.replace(/,/g, "");
    monthlyValue = monthlyValue.replace(/,/g, "");
    investmentInterest = investmentInterest.replace(/,/g, "");
    var yearsArr = new Array();
    var ilumony = new Array();
    var bank = new Array();
    var actual = new Array();
    var actualBal = 0;
    var curr = 0;
    var yearsNum = eval(year);
    var monthNum = 0;
    var pmonths = 0;
    while (curr <= yearsNum) {
        if (curr === 0) {
            monthNum = (12 - currentmonth);
            actualBal = eval(upFrontValue) + eval(monthlyValue * monthNum);
        } else if (curr === yearsNum) {
            monthNum = (currentmonth);
            actualBal = eval(actualBal) + eval(monthlyValue * monthNum);
        } else {
            monthNum = (12);
            actualBal = eval(actualBal) + eval(monthlyValue * monthNum);
        }
        pmonths = pmonths + monthNum;
        var balance = calculateMonthsBal(pmonths, upFrontValue, investmentInterest, monthlyValue);
        var yearlypridicted = balance.principalVal;
        var yearlybank = balance.bankValue;
        ilumony.push(parseInt(yearlypridicted));
        bank.push(parseInt(yearlybank));
        actual.push(parseInt(actualBal));
        var cY = eval(currentYear + curr);
        yearsArr.push(parseInt(cY));
        curr++;
    }
    return {
        id: 'lineChart' + inv.investmentId,
        ilumony: ilumony,
        actual: actual,
        bank: bank,
        years: yearsArr
    };
}

function lineChart(id, ilumony, actual, bank, yearsArr) {
//    alert(id);
    Highcharts.chart(id, {
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting: {enabled: false},
        credits: {
            enabled: false
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: yearsArr[0]
            }
        },
        series: [{
                name: 'Minto Value',
                data: ilumony
            }],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
        }
    });
}

function displayContinue(btn) {
    var amt = $("#investment-fund-modal-upFrontValue").val();
    var fid = $("#investment-fund-modal-fundId").val();
    document.getElementById("investment-fund-modal-hideImg").style.display = 'block';
    if (amt > 0) {
        var url = './rest/cryptolabs/api/asd0lkjald0jfdsjsad0insight?amt=' + amt + '&fId=' + fid + '&curr=' + currency;
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (data) {
//                alert(data.transaction.Success);
                var txn = data.transaction;
                if (txn.Success === true) {
                    var url = txn.NavigateURL;
                    document.getElementById("NET_BANKING").href = url;
                    var am = $("#investment-fund-modal-upFrontValue");
                    am.attr('readonly', true);
                    $("#investment-fund-modal-continue").show();
//                    alert(data.localAmount);
                    $(".localInvestmentAmount").val(data.localAmount);
                    $(".investmentAmount").val(data.usdAmount);
                    document.getElementById("investment-fund-modal-hideImg").style.display = 'none';
                    document.getElementById("investment-fund-modal-hideImg").style.display = 'none';
                }
            }
        });
    }
}

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
}