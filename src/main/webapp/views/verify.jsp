<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>

        
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Verify</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                  
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Verify</h6>
                                                        </div>
                                                        <div class="admin-checkout">
                                                            <div class="container">
                                                                <div class="row">
                                                                    
                                                                    <div class="col-md-4"><a href="./home-verification">
                                                                            <div class="checkout-box">
                                                                                <p>IDENTITY VERIFICATION</p>
                                                                                <div class="checkout-box-image">
                                                                                    <h2>idu</h2>
                                                                                </div>
                                                                            </div></a>
                                                                    </div>
                                                                    <div class="col-md-4"><a href="javascript:void(0)">
                                                                            <div class="checkout-box">
                                                                                <p>MOBILE VERIFICATION</p>
                                                                                <div class="checkout-box-image">
                                                                                    <h2>MOBILE</h2>
                                                                                </div>
                                                                            </div></a>
                                                                    </div>
                                                                    <div class="col-md-4"><a href="javascript:void(0)">
                                                                            <div class="checkout-box">
                                                                                <p>SEARCH PDF HISTORY</p>
                                                                                <div class="checkout-box-image">
                                                                                    <h2>HISTORY</h2>
                                                                                </div>
                                                                            </div></a>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>       
                                                </div>
                                            </div>
                                        </div>
                                   
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
                 <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
        <script>
            $('.clickinput').on("click", function () {
                var recemt = $(this).closest('.funds-deatil');
                recemt.find(".offer-input").toggle();
            });
            $(document).ready(function () {
                $(".loader").fadeOut("slow");
                $('.hidediv1').hide();
                $('.hidediv2').hide();
                $('.hidediv3').hide();
            });
            $('.democlick').click(function () {
                var data = $(this).data("target");
                $('.showclick').hide();
                $(data).show();
            });
        </script>
    </body>
</html>