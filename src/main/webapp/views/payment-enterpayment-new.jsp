<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>



        <style>

            img.logo {
                width: 40%;
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
            .content_body {
                max-width: 100%;
                margin: auto;
                margin-top: 10px;
                background: #e0e0df9e;
                border-radius: 10px;
            }

            .content-section {
                padding: 40px;
            }
            .btns {
                display: flex;
                justify-content: space-between;
            }
            #msform fieldset:not(:first-of-type) {
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <!--                    <div class="content-i">
                                            <div class="content-box">
                                                <div class="row">
                    
                                                    <div class="col-md-12 invst-option">
                                                        <div class="card mt-3 tab-card">
                                                             <div class="card-header tab-card-header">
                                                                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                                            <li class="nav-item">
                                                                                    <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                    <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                    <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                                            </li>
                                                                    </ul>
                                                            </div> 
                    
                                                            <div class="tab-content" id="myTabContent">
                                                                <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                                    <div class="row1">
                                                                        <div class="col-md-12">
                                                                            <div class="element-wrapper">
                                                                                <h6 class="element-header">Payment</h6>
                                                                            </div>
                    
                                                                            <div class="form-data">
                                                                                <div class="container">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12  box-payment-space">
                                                                                            <div class="content-body payment-box-space payment-overflow">
                                                                                                <div class="line-progress">
                                                                                                    <div class="line-bar2">
                                                                                                        <div class="line-dot">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="line-data">
                                                                                                        <span>Amount</span>
                                                                                                        <span>You</span>
                                                                                                        <span>Recipient</span>
                                                                                                        <span>Review</span>
                                                                                                        <span>Pay</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="header-border">
                                                                                                </div>
                                                                                                <div class="input-content-text">
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-12">
                                                                                                            <h5 class="text-center">How much would you like to transfer?</h5>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="currency-page">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="body-section">
                    
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12">
                    
                                                                                                                <div class="calculator-page">
                                                                                                                    <div class="calculator-input">
                                                                                                                        <p class="input-text">You Send</p>
                                                                                                                        <input type="text" class="flag-input" placeholder="0" >
                                                                                                                    </div>
                                                                                                                    <div class="calculator-dropdown">
                                                                                                                        <ul class="list-unstyled">
                                                                                                                            <li class="init"><img class="flag-images" src="./resources/images/nzd.png"> NZD<i class="fa fa-angle-down dropdown-flag"></i></li>
                                                                                                                            <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD </li>
                                                                                                                            <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                                            <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                                            <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </div>
                    
                                                                                                            </div>
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="calculate-data see-space">
                                                                                                                    <a href="javascript:void(0)" class="see-toggle">See calculation</a>
                                                                                                                </div>
                                                                                                                <div class="calculate-data data-toggle">
                                                                                                                    <span class="six-value six-new">0 INR</span>
                                                                                                                    <select class="select-space">
                                                                                                                        <option value="1">Bank Transfer</option>
                                                                                                                    </select>
                                                                                                                    <span>fee</span>
                                                                                                                </div>
                                                                                                                <div class="calculate-data data-toggle">
                                                                                                                    <span class="six-value six-new">9.10 NZD Our fees</span>
                                                                                                                </div>
                                                                                                                <div class="calculate-data">
                                                                                                                    <span class="six-value">9.10 NZD Total fees</span>
                                                                                                                </div>
                                                                                                                <div class="calculate-data data-toggle">
                                                                                                                    <span class="six-value six-new">99,198.78 INR Amount weâ€™ll convert</span>
                                                                                                                </div>
                                                                                                                <div class="calculate-data cal-result">
                                                                                                                    <span class="six-value">0.660900</span> <span class="rate-value">Guaranteed rate (48hrs)</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12">
                    
                                                                                                                <div class="calculator-page">
                                                                                                                    <div class="calculator-input">
                                                                                                                        <p class="input-text">You Send</p>
                                                                                                                        <input type="text" class="flag-input" placeholder="0">
                                                                                                                    </div>
                                                                                                                    <div class="calculator-dropdown2">
                                                                                                                        <ul class="list-unstyled">
                                                                                                                            <li class="init2"><img class="flag-images" src="./resources/images/usd.png"> USD<i class="fa fa-angle-down dropdown-flag "></i></li>
                                                                                                                            <li data-value="value 1"><img class="flag-images" src="./resources/images/nzd.png"> NZD</li>
                                                                                                                            <li data-value="value 2"><img class="flag-images" src="./resources/images/usd.png"> USD</li>
                                                                                                                            <li data-value="value 3"><img class="flag-images" src="./resources/images/inr.png"> INR</li>
                                                                                                                            <li data-value="value 4"><img class="flag-images" src="./resources/images/gbp.png"> GBP</li>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="shoul-data">
                                                                                                                    <p>Should arrive <span class="six-value">by Aug 2</span></p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="result-btn">
                                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl">Compare Price</a>
                                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl" id="continue">Continue</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                    
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                         <a href="#" class="btn btn-primary" id="viewmore1">View More</a>               
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->



                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="content_body">
                                <form id="msform">
                                    <fieldset id="step1">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                    <div class="content-body payment-box-space">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar2">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="header-border">
                                                                        </div>
                                                                        <div class="input-content-text">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">Existing recipients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next-off">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">One Off</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next1">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Regular</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 9524</p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                    <div class="col-md-12">
                                                                                                        <div class="btns">
                                                                                                            <input type="button" name="previous" value="Previous" class="previous1 ">
                                                                                                            <input type="button" name="previous" value="Next" class="next1">
                                                                                                        </div>
                                                                                                    </div>-->
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step21">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                    <div class="content-body payment-box-space">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar5">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="header-border">
                                                                        </div>
                                                                        <div class="input-content-text">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">One Off</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <p>Enter the Amount You want to invest</p> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous-off" id="">Back</a>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next21" id="">Next</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous2 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next2">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step2">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12  box-payment-space">
                                                                    <div class="content-body payment-box-space">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar5">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="header-border">
                                                                        </div>
                                                                        <div class="input-content-text">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">Regular</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <p>Lump Sum</p> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <p>Regular Amount</p> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <input type="text" placeholder="Enter Amount">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <p>Frequency</p> 
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="payment-regular">
                                                                                        <select>
                                                                                            <option>1 Month</option>
                                                                                            <option>3 Months</option>
                                                                                            <option>6 Months</option>
                                                                                            <option>1 Year</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous2" id="">Back</a>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next2" id="">Next</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous2">
                                                                                                        <input type="button" name="previous" value="Next" class="next2">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step3">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 box-payment-space">
                                                                    <div class="content-body payment-box-space">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar">
                                                                                <div class="line-dot">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text">

                                                                            <div class="row">
                                                                                <!--                                                                                <div class="col-md-12">
                                                                                                                                                                    <div class="body-section">
                                                                                                                                                                        <h6 class="body-data">Existing recipients</h6>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>-->
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <h5 class="text-center" style="text-align:left!important">How would you like to pay?</h5>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="body-bank radoi-set">
                                                                                        <label class="bank-radio nextcredit">
                                                                                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                            <span class="bank-card">Direct Credit</span>
                                                                                            <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                            <input type="radio" name="radio">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="bank-radio next3">
                                                                                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                            <span class="bank-card">Direct Debit</span>
                                                                                            <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                            <input type="radio" name="radio">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous3" id="">Back</a>
                                                                                        <!--                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next3" id="">Next</a>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous3 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next3">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step4">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="content-body">
                                                                        <div class="line-progress">
                                                                            <div class="line-bar6">
                                                                                <div class="line-dot line-adjust">
                                                                                </div>
                                                                            </div>
                                                                            <div class="line-data">
                                                                                <span>Amount</span>
                                                                                <span>You</span>
                                                                                <span>Recipient</span>
                                                                                <span>Review</span>
                                                                                <span>Pay</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-content-text">

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="body-section">
                                                                                        <h6 class="body-data">Existing recipients</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next4">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Use Existing Account</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-md-4 continue">
                                                                                    <a href="javascript:void(0)" class="next-debit">
                                                                                        <div class="body-box payment-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Enter New Account</h6>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="payment-btn">
                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back previous4" id="">Back</a>
                                                                                        <!--                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next2" id="">Next</a>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-12">
                                                                                                    <div class="btns">
                                                                                                        <input type="button" name="previous" value="Previous" class="previous4 ">
                                                                                                        <input type="button" name="previous" value="Next" class="next4">
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step5">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar7 ">
                                                                                    <div class="line-dot ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="currency-page1">
                                                                                    <div class="trans-detail">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Bank Account Details for Payment</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Account Number</span> 
                                                                                                        <span class="second-span">12345678912</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Bank Name</span> 
                                                                                                        <span class="second-span">ANZ Bank New Zealand</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Branch</span> 
                                                                                                        <span class="second-span">New Zealand</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Reference Details for Payment</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Reference Number</span> 
                                                                                                        <span class="second-span">998756789845</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Date</span> 
                                                                                                        <span class="second-span">Tuesday, 22 October 2019</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Time</span> 
                                                                                                        <span class="second-span">20:19:37</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="instruction-btn">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="tree-btn">
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl">I've made payment</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">I'll make payment later</a>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="payment-btn">
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous5" id="">Back</a>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next5" id="">Next</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <fieldset id="step6">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar1 ">
                                                                                    <div class="line-dot ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="currency-page1">
                                                                                    <div class="trans-detail">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail-2">
                                                                                                    <div class="trans-bank-2">
                                                                                                        <h6 class="body-data">Use Existing Account</h6>
                                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Account Number</span> 
                                                                                                        <span class="second-span">12345678912</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Bank Name</span> 
                                                                                                        <span class="second-span">ANZ Bank New Zealand</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data-2">
                                                                                                        <span class="first-span">Branch</span> 
                                                                                                        <span class="second-span">New Zealand</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="payment-btn">
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous6" id="">Back</a>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next6" id="">Next</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <fieldset id="step7">
                                        <div class="content-section">
                                            <div class="row">
                                                <div class="col-md-12 thanku">
                                                    <div class="form-data">
                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar1 ">
                                                                                    <div class="line-dot ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="currency-page2">
                                                                                    <div class="trans-detail">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="col-md-12"> 
                                                                                                    <div class="debit-content_body only-debit-form">

                                                                                                        <div class="debit-content-section">
                                                                                                            <div class="row">
                                                                                                                <div class="col-md-7">
                                                                                                                    <div class="text-data">
                                                                                                                        <h6>Direct Debit Instruction:</h6>
                                                                                                                        <p>Details of the bank account from which the payment is to be mode:</p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-5">
                                                                                                                    <div class="debit-bank-name digit-1">
                                                                                                                        <h6>Authorisation Code</h6>
                                                                                                                        <input type="text" id="pincode-input1" class="form-control" placeholder="0" maxlength="" value="0618754">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                                        <input type="text" class="form-control" placeholder="Name of Bank Account" maxlength="20">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                                        <input type="text" class="form-control" placeholder="Bank Account Number" maxlength="11">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name digit">
                                                                                                                        <h6>Payer Particulars</h6>
                                                                                                                        <input type="text" id="pincode-input2" class="form-control" placeholder="" maxlength="" value="MINT">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-name digit">
                                                                                                                        <h6>Payer Code</h6>
                                                                                                                        <input type="text" id="pincode-input3" class="form-control" placeholder="" maxlength="" value="INVESTMENTS">
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="debit-bank-name digits">
                                                                                                                        <h6>Payer Reference</h6>
                                                                                                                        <input type="text" id="pincode-input4" class="form-control" placeholder="" maxlength="">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="debit-bank-pera">
                                                                                                                        <p>I/We authorize until further notice to debit my account with all amounts with the authorization code for direct debit.I/ We acknowledge and accept that the bank accepts the authority only upon the conditions listed below.</p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-pera">
                                                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl debit-own-btn previous7">BACK</a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-6">
                                                                                                                    <div class="debit-bank-pera">
                                                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl debit-own-btn next7">I ACCEPT</a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-md-12">
                                                                                                                    <div class="term-value">
                                                                                                                        <h5>CLIENT TERMS AND CONDITIONS</h5>
                                                                                                                        <p>
                                                                                                                        <ol>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                                <ol>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                </ol>
                                                                                                                            </li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                                <ol>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                                </ol>
                                                                                                                            </li>
                                                                                                                        </ol>
                                                                                                                        </p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
<!--                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="payment-btn">
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl  payment-back previous7" id="">Back</a>
                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-styl payment-back next7" id="">Next</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>-->
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </fieldset>
                                                <fieldset id="step8">
                                                    <div class="content-section">
                                                        <div class="row">
                                                            <div class="col-md-12 thanku">
                                                                <div class="form-data">
                                                                    <div class="form-data">
                                                                        <div class="container">
                                                                            <div class="row">
                                                                                <div class="col-md-12 box-payment-space">
                                                                                    <div class="content-body payment-box-space">
                                                                                        <div class="line-progress">
                                                                                            <div class="line-bar1 ">
                                                                                                <div class="line-dot ">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="line-data">
                                                                                                <span>Amount</span>
                                                                                                <span>You</span>
                                                                                                <span>Recipient</span>
                                                                                                <span>Review</span>
                                                                                                <span>Pay</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="header-border">

                                                                                        </div>
                                                                                        <div class="input-content-text">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="trans-detail-2">
                                                                                                        <div class="trans-bank-2">
                                                                                                            <h6 class="body-data payment-thanks">Thanks</h6>
                                                                                                            <!--                                                                                                    <a href="">Edit</a>-->
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                        </div>
                                        </div>

                                        </div>
                                        </div>
                                        <div class="display-type"></div>
                                        </div>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
                                        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
                                        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
                                        
                                        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
                                        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
                                        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
                                        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
                                        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
                                        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
                                        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
                                        <script src="https://code.highcharts.com/highcharts.js" ></script> 
                                        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
                                        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
                                        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
                                        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
                                        <script type="text/javascript" src="resources/js/bootstrap-pincode-input.js"></script>
                                        <script>
                                            $(document).ready(function () {
                                                $('#pincode-input2').pincodeInput({hidedigits: false, inputs: 12});
                                                $('#pincode-input1').pincodeInput({hidedigits: false, inputs: 7});
                                                $('#pincode-input3').pincodeInput({hidedigits: false, inputs: 12});
                                                $('#pincode-input4').pincodeInput({hidedigits: false, inputs: 25});

                                            });
                                        </script>
                                        <script type="text/javascript">
                                            $(window).load(function () {

                                                $(".loader").fadeOut("slow");
                                            });
                                        </script>

                                        <!--        <script>
                                                    //        $('#viewmore1').click(function () {
                                                    //            $('.hidediv1').show();
                                                    //            $('#viewmore1').hide();
                                                    //        });
                                                    //        $('#viewmore2').click(function () {
                                                    //            $('.hidediv2').show();
                                                    //            $('#viewmore2').hide();
                                                    //        });
                                                    //        $('#viewmore3').click(function () {
                                                    //            $('.hidediv3').show();
                                                    //            $('#viewmore3').hide();
                                                    //        });
                                                    $('.clickinput').on("click", function () {
                                                        var recemt = $(this).closest('.funds-deatil');
                                                        recemt.find(".offer-input").toggle();
                                                    });
                                                    $(document).ready(function () {
                                                        $(".loader").fadeOut("slow");
                                                        $('.hidediv1').hide();
                                                        $('.hidediv2').hide();
                                                        $('.hidediv3').hide();
                                                    });
                                                    $('.democlick').click(function () {
                                                        var data = $(this).data("target");
                                                        $('.showclick').hide();
                                                        $(data).show();
                                                    });
                                                </script>-->
                                        <script>
                                            $(".calculator-dropdown ul").on("click", ".init", function () {
                                                $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
                                            });

                                            var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
                                            $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                                                allOptions.removeClass('selected');
                                                $(this).addClass('selected');
                                                $(".calculator-dropdown ul").children('.init').html($(this).html());
                                                allOptions.toggle();
                                            });
                                        </script>
                                        <script>
                                            $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                                                $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
                                            });

                                            var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
                                            $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                                                allOption.removeClass('selected');
                                                $(this).addClass('selected');
                                                $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                                                allOption.toggle();
                                            });
                                        </script>
                                        <script>
                                            $(document).ready(function () {
                                                $(".data-toggle").hide();
                                                $(".loader").fadeOut("slow");
                                                $(".see-toggle").click(function () {
                                                    $(".data-toggle").toggle();
                                                });
                                                $("#continue").click(function () {
                                                    window.location.href = './regularmethod';
                                                });
                                            });
                                        </script>
                                        <script>
                                            $(".previous2").click(function () {
                                                $("#step2").hide();
                                                $("#step1").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous-off").click(function () {
                                                $("#step21").hide();
                                                $("#step1").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous3").click(function () {
                                                $("#step3").hide();
                                                $("#step2").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous4").click(function () {
                                                $("#step4").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous5").click(function () {
                                                $("#step5").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous6").click(function () {
                                                $("#step6").hide();
                                                $("#step4").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".previous7").click(function () {
                                                $("#step7").hide();
                                                $("#step4").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next1").click(function () {
                                                $("#step2").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next-off").click(function () {
                                                $("#step21").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next2").click(function () {
                                                $("#step3").show();
                                                $("#step2").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next21").click(function () {
                                                $("#step3").show();
                                                $("#step21").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next3").click(function () {
                                                $("#step4").show();
                                                $("#step3").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next4").click(function () {
                                                $("#step6").show();
                                                $("#step4").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".nextcredit").click(function () {
                                                $("#step5").show();
                                                $("#step3").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next5").click(function () {
                                                $("#step8").show();
                                                $("#step5").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next6").click(function () {
                                                $("#step8").show();
                                                $("#step6").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next7").click(function () {
                                                $("#step8").show();
                                                $("#step7").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                            $(".next-debit").click(function () {
                                                $("#step7").show();
                                                $("#step4").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            });
                                        </script>

                                        </body>
                                        </html>
