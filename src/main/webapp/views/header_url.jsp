<head>
    <link href="./resources/favicon.png" rel="shortcut icon"/>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
    <link href='./resources/backoffice/jquery-ui.css' rel='stylesheet'>
    <link href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' rel='stylesheet'>
    <!--<link href="./resources/backoffice/style.css" rel="stylesheet"/>-->
    <!--<link href="./resources/backoffice/main.css" rel="stylesheet"/>-->
    <!--<link href="./resources/backoffice/custom.css" rel="stylesheet"/>-->
    <link href="./resources/backoffice/simple-line-icons.css" rel="stylesheet"/>
    <link href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css' rel='stylesheet prefetch'/>
    <!--<link href="./resources/css/main.css" rel="stylesheet"/>-->
    <!--<link href="./resources/css/main1.css" rel="stylesheet"/>-->
    <link href="./resources/css/sweetalert.css" rel="stylesheet"/>
    <!--<link href="./resources/css/newcss/new-main.css" rel="stylesheet"/>-->
    <!--<link href="./resources/css/newcss/new-custom.css" rel="stylesheet"/>-->
    <!--<link href="./resources/css/custom.css" rel="stylesheet"/>-->
    <link href="./resources/css/idVerification.css" rel="stylesheet"/>
    <!--<link href="./resources/css/acc-style.css" rel="stylesheet">-->
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <link href="./resources/css/bootstrap-pincode-input.css" rel="stylesheet"/>
    <link href="./resources/css/signup/intlTelInput.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="./resources/css/signup/style.css" rel="stylesheet">-->
    
    <link href="./resources/css/signup/new-custom.css" rel="stylesheet">
    <link href="./resources/css/my-css.css" rel="stylesheet">
    
    <!--<link href="./resources/css/signup/new.css" rel="stylesheet">-->
    <link href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' rel='stylesheet'>
    <link href="./resources/css/signup/intlTelInput.css" rel="stylesheet">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css' rel='stylesheet'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css' rel='stylesheet'>
    <link href='https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css' rel='stylesheet'>
    <link href='https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css' rel='stylesheet'>
    <link href="resources/css/osfonts.css" rel="stylesheet"> 
</head>


