<%-- 
    Document   : more-investor-view
    Created on : 23 Oct, 2019, 10:56:34 AM
    Author     : IESL
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${joint.moreInvestorList}" var="moreInvestor" varStatus="loop">
    <fieldset class="morestep1 more-investor-fs more-investor-info" id="step6${loop.index}1">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please provide the below details for Investor  <span class="director-count" ></span>: <span class="director-name investername"></span>
                </h5> 
            </div>
            <div class="input-content">
                <div class="row">
                    <!--  <div class="col-sm-12">
                            <p><span class="director-name investername">Jon Snow</span></p>
                    </div> -->
                    <div class="col-sm-6">
                        <label class="label_input">
                            Preferred first name (if applicable) 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="hidden" class="more-investor-title">
                        <input type="hidden" class="more-investor-fname">
                        <input type="hidden" class="more-investor-email">
                        <input type="hidden" class="more-investor-radio">
                        <input type="text" class="more-investor-preferred-name" placeholder="Enter preferred name (optional)" value="${moreInvestor.fullName}">
                        <span></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Date of birth
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="input-field more-investor-field clear-more-investor more-investor-dob" name="dob" placeholder="dd/mm/yyyy" value="${moreInvestor.date_of_Birth}" data-format="dd/mm/yyyy" data-lang="en" required onchange="removeDate();"  value="${moreInvestor.date_of_Birth}"/>
                        <span class="error error-more-dob"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of residence 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group country-set flag-drop">
                        <input type="text" class="form-control more-investor-field more-investor-countryname" name="countryCode" placeholder="-Select-" value="${moreInvestor.country_residence}"  readonly="readonly">                                                                                                                                                                                                   
                        <span class="error error-countryOptions"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Occupation 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <select name="occupation" class="more-investor-occupation selectoption otherOcc form-group more-select-occupation" onchange="selectOccupation(this)">
                            <option value="-Select-">-Select-</option>
                            <c:forEach items="${occupation}" var="occ">
                                <option value="${occ.mmc_occu_id}" 
                                        <c:if test="${occ.mmc_occu_id eq moreInvestor.occupation}">
                                            selected                                                                     
                                        </c:if> >${occ.mmc_occu_name}</option>
                            </c:forEach>
                            <option value="0">Other</option>
                        </select>
                        <input type="text" name="Occupation" placeholder="Enter occupation " class=" selectOcc input-field more-input-occupation" onclick ="removeSpanError()"/>
                        <span class="error error-more-occupation"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous6 action-button-previous"  onclick="prev7(this)" value="Previous" />
        <input type="button" name="next" class="next7 action-button" onclick="next7(this)" value="Continue" />
    </fieldset>
    <fieldset class="morestep2 more-investor-fs more-investor-info" id="step6${loop.index}2">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please provide the contact details for Investor : <span class="director-name investername">Jon Snow</span>
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <div class="col-sm-4">
                        <label class="label_input">
                            Home address
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <button class="director-btn postal-address all-btn-color upper-btn same-as-investor1" onclick="sameAddress(this)">Same as Investor 1</button>
                    </div>
                    <div class="col-sm-4">
                        <input type="address" class="form-control input-field more-investor-address clear-more-investor address1" placeholder="Enter new address" onclick ="removeSpanError()"  value="${moreInvestor.homeAddress}"/>
                        <span class="error error-more-address"></span>
                        <input type="hidden" class="more-investor-postalcode"/>
                    </div>
                    <div class="col-sm-4">
                        <label class="label_input">
                            Mobile number 
                        </label>
                    </div>
                    <div class="col-sm-8 mobile_number first-mobile mobile-space">
                        <input type="text" class="form-control more-investor-codenumber more-investor-countrycode codenumber jont-mob" placeholder="" readonly="readonly">
                        <input type="tel" class="form-control error codename more-investor-mobile clear-more-investor" name="mobileNo"  placeholder="Enter mobile number" onclick ="removeSpanError()" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="${moreInvestor.mobile_number}">
                        <span class="error error-more-mobile"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous7 action-button-previous"   onclick="prev8(this)" value="Previous" />
        <input type="button" name="next" class="next8 action-button" onclick="next8(this)" value="Continue" />
    </fieldset>
    <fieldset class="morestep3 more-investor-fs more-investor-info"  id="step6${loop.index}3">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please provide identificaiton details for Investor : <span  class="director-count"></span> <span class="director-name investername">Jon Snow</span>
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <!-- <div class="col-sm-12">
                            <p><span class="director-name investername">Jon Snow</span></p>
                    </div> -->
                    <div class="col-sm-6">
                        <label class="label_input detail-2">
                            Which type of ID are you providing
                        </label>
                    </div>
                    <div class="col-sm-6 ">
                        <select class="selectoption form-group Id_Type" onchange="changeFund(this)">
                            <option value="1">NZ Driver Licence</option>
                            <option value="2">NZ Passport</option>
                            <option value="3">Other</option>
                        </select>
                    </div>
                    <div class="row drivery-licence2">
                        <div class="col-sm-6">
                            <label class="label_input">
                                First name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="first_name more_licence_first_name" name="licence_first_name" placeholder="Enter first name" onclick = "removeSpanError();"/>
                            <span class="error error_more_licence_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="middle_name more_licence_middle_name" value="${moreInvestor.middleName}" name="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                            <span class="error error_more_licence_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="last_name more_licence_last_name" value="${moreInvestor.firstName}"  name="licence_last_name" placeholder="Enter last name" onclick="removeSpanError();" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                            <span class="error error_more_licence_last_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Licence number 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field more-investor-no1 clear-more-investor more-investor-licenseNumber" name="license_number" placeholder="Enter licence number "  onclick="removeSpanError();" value="${moreInvestor.license_number}"/>
                            <span class="error error-more-licenseNumber"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="input-field more-investor-exp clear-more-investor lic_expiry_Date more-investor-licenseExpiryDate" name="dob" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" data-lang="en" onchange="removeDate();" required value="${moreInvestor.licence_expiry_Date}"/>                                 
                            <span class="error error-more-licenseExpiryDate"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Version number 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="input-field more-investor-licence-ver clear-more-investor lic_verson_number more-investor-versionNumber" placeholder="Enter version number" onclick="removeSpanError();" onkeypress='return (event.charCode >= 48 && event.charCode <= 57)' value="${moreInvestor.licence_verson_number}"/>
                            <span class="error error-more-versionNumber"></span>
                        </div>
                    </div>
                    <div class="row passport-select2">
                        <div class="col-sm-6">
                            <label class="label_input">
                                First name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="more_passport_first_name first_name" name="passport_first_name" placeholder="Enter first name" onclick="removeSpanError();"/>
                            <span class="error error_passport_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="more_passport_middle_name middle_name" name="passport_middle_name" placeholder="Enter middle name"/>
                            <span class="error error_passport_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="more_passport_last_name last_name" name="passport_last_name" placeholder="Enter last name" onclick="removeSpanError();"/>
                            <span class="error error_passport_last_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Passport number  
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field more-investor-no2 clear-more-investor more-investor-passportNumber" name="passport_number" placeholder="Enter passport number" onclick="removeSpanError();"  value="${moreInvestor.passport_number}"/>
                            <span class="error error-more-passportNumber"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text"  class="input-field more-investor-exp clear-more-investor pass_expiry more-investor-passportExpiryDate" name="passport_expiry" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" onchange="removeDate();" data-lang="en" required value="${moreInvestor.passport_expiry}"/>                                 
                            <span class="error error-more-passportExpiryDate"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Country of issue  
                            </label>
                        </div>
                        <div class="col-sm-6 flag-drop">
                            <input type="text" class="form-control more-investor-countryname more-investor-passportCountryOfIssue countryname" placeholder="Enter Country Code" readonly="readonly" value="${moreInvestor.passport_issue_by}">
                            <span class="error error-more-investor-passportCountryOfIssue"></span>
                        </div>
                    </div>
                    <div class="row other-select2">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Type of ID   
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field more-investor-id-type clear-more-investor more-investor-typeOfId" placeholder="Enter ID type" onclick = "removeSpanError();" value="${moreInvestor.other_id_type}"/>
                            <span class="error error-more-typeOfId"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                First name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name"/>
                            <span class="error" id="error_other_first_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Middle name 
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name"/>
                            <span class="error" id="error_other_middle_name"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Last name 
                            </label>
                        </div>                                                    
                        <div class="col-sm-6">
                            <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name"/>
                            <span class="error" id="error_other_last_name"></span>
                        </div> 
                        <div class="col-sm-6">
                            <label class="label_input">
                                Expiry date 
                            </label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" name="dob" class="input-field more-investor-exp clear-more-investor more-investor-typeOfIdExpiryDate" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" data-lang="en" onchange="removeDate();"   value="${moreInvestor.other_id_expiry}"/>                                 
                            <span class="error error-more-typeOfIdExpiryDate"></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Country of issue
                            </label>
                        </div>
                        <div class="col-sm-6 flag-drop">
                            <input type="text" class="form-control more-investor-countryname more-investor-typeOfIdCountryOfIssue countryname" placeholder="Enter Country Code" readonly="readonly" value="${moreInvestor.other_id_issueBy}">
                            <span class="error error-more-investor-typeOfIdCountryOfIssue"></span>
                        </div>
                        <div class="col-sm-129 closestcls">
                            <input type="file" class="form-control more-investor-file attach-btn clear-more-investor checkname"  accept="image/x-png,image/jpeg,image/*,.txt,.pdf" onchange="checkname(this)">
                            <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile" style="display: none"><i class="far fa-times-circle"></i></a>
                            <span class="error error-more-file"></span>
                        </div>
                    </div>
                    <input type="hidden" name="more_investor_verify" id="more_investor_verify" class="more_investor_verify" value="false">
                    <input type="hidden" name="more_investor_verify" id="investorPep_verify" value="" class="more_investorPep_verify">

                </div>
            </div>
        </div>
        <div class="footer_form">
            <!--<img src="./resources/images/red.png">-->	
        </div>
        <input type="button" name="previous" class="previous8 action-button-previous" onclick="prev9(this)" value="Previous" />
        <input type="button" name="next" class="next9 action-button" onclick="next9(this)" value="Continue" />
    </fieldset>
    <fieldset class="morestep4 more-investor-fs more-investor-info" id="step6${loop.index}4">
        <div class="content-section">
            <div class="element-wrapper">
                <h5 class="element-header">
                    Please enter the tax details for Investor : <span class="director-count"></span><span class="director-name investername"></span>
                </h5>
            </div>
            <div class="input-content">
                <div class="row">
                    <!--                    <div class="col-sm-12">
                                            <p><span class="director-name investername">Jon Snow</span></p>
                                        </div> -->
                    <div class="col-sm-6">
                        <label class="label_input">
                            Prescribed Investor Rate (PIR) 
                        </label>
                    </div>
                    <div class="col-sm-3 details-pos">
                        <select class="selectoption form-group aml-select presInvestRate">
                            <option <c:if test="${moreInvestor.pir eq '28'}">selected</c:if> value="28%">28%</option>
                            <option <c:if test="${moreInvestor.pir eq '17.5'}">selected</c:if> value="17.5%">17.5%</option>
                            <option <c:if test="${moreInvestor.pir eq '10.5'}">selected</c:if> value="10.5%">10.5%</option>
                            </select>  
                        </div>
                        <div class="col-sm-3">
                            <a href="https://www.ird.govt.nz/roles/portfolio-investment-entities/using-prescribed-investor-rates" target="_blank" class="aml-link pir">What's my PIR?</a>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                IRD Number 
                            </label>
                        </div>
                        <div class="col-sm-6 validate-ird">
                            <input type="text" class="form-control input-field more-investor-irdno clear-more-investor more-investor-irdNumber" value="${moreInvestor.ird_Number }"  placeholder="XXX-XXX-XXX" onkeypress="checkird(this, event)" onchange="checkird(this, event)"/>
                        <a class="ird-check" href="javascript:void(0)" style="display: none"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                        <span class="error error_more_irdNumber" ></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input" style="text-align:left">
                            Are you a US citizen or US tax resident, or a tax resident in any other country?
                        </label>
                    </div>
                    <div class="col-sm-6 ">
                        <select class="selectoption selectoption2 form-group usCitizen more-investor-uscitizen more-investor-usCitizen" onchange="changeCountry(this)">
                            <option value="1">No</option>
                            <option value="2">Yes</option>
                        </select>
                    </div>
                    <div class="row yes-option2">
                        <div class="row yes-new3 checktindata1">
                            <div class="col-sm-12">
                                <h5 class="element-header aml-text">
                                    Please enter all of the countries (excluding NZ) of which you are a tax resident. 
                                </h5>
                            </div> 
                            <div class="col-sm-6">
                                <label class="label_input" style="text-align:left">
                                    Country of tax residence
                                </label>
                            </div> 
                            <div class="col-sm-6 details-pos flag-drop">
                                <input type="text" class="form-control countrynameoutnz more-investor-tex_residence_Country clear-more-investor more-investor-countryOfTaxResidence" placeholder="Enter Country Code" readonly="readonly" >     
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Tax Identification Number (TIN)
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-field more-investor-TIN clear-more-investor tin_unavailable" placeholder="Enter TIN " onkeyup="myFunction()" value="${moreInvestor.tin}"/>
                                <span class="error-TIN"error-resn_tin_unavailable></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Reason if TIN not available  
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-field more-investor-resn_tin_unavailable clear-more-investor resn_tin_unavailable" requiredmore-investor-resn_tin_unavailable="required" placeholder="" value="${moreInvestor.resn_tin_unavailable}"/>
                                <span class="error-resn_tin_unavailable"></span>
                            </div>
                        </div>
                        <div class="col-sm-129 add-another">
                            <a class="add-another2 all-btn-color add-country-another3" href="javascript:void(0)" onclick="addAnotherCountry(this)">Add another country </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_form">
            <img src="./resources/images/red.png">	
        </div>
        <input type="button" name="previous" class="previous9 action-button-previous"   onclick="prev10(this)"  value="Previous" />
        <input type="button" name="next" class="next10 action-button"  onclick="next10(this)" value="Continue" />
    </fieldset>
</c:forEach>
<fieldset class="morestep1" id="morestep1" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please provide the below details for Investor  <span class="director-count" ></span>: <span class="director-name investername"></span>
            </h5> 
        </div>
        <div class="input-content">
            <div class="row">
                <!--  <div class="col-sm-12">
                        <p><span class="director-name investername">Jon Snow</span></p>
                </div> -->
                <div class="col-sm-6">
                    <label class="label_input">
                        Preferred first name (if applicable) 
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="hidden" class="more-investor-title" value=""/>
                    <input type="hidden" class="more-investor-fname"/>
                    <input type="hidden" class="more-investor-email"/>
                    <input type="hidden" class="more-investor-radio">
                    <input type="text" class="more-investor-preferred-name" placeholder="Enter preferred name(Optional)"/>
                    <span></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Date of birth
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="input-field more-investor-field clear-more-investor more-investor-dob" name="dob" placeholder="dd/mm/yyyy" value="" data-format="dd/mm/yyyy" data-lang="en" onchange="removeDate();" required/>
                    <span class="error error-more-dob"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Country of residence 
                    </label>
                </div>
                <div class="col-sm-6 form-group country-set flag-drop">
                    <input type="text" class="form-control more-investor-field more-investor-countryname countryname" name="countryCode"  placeholder="-Select-"  readonly="readonly">                                                                                                                                                                                                   
                    <span class="error error-countryOptions"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Occupation 
                    </label>
                </div>
                <div class="col-sm-6">
                    <select name="occupation" class="more-investor-occupation selectoption otherOcc form-group more-select-occupation" onchange="selectOccupation(this)">
                        <option value="-Select-">-Select-</option>
                        <c:forEach items="${occupation}" var="occ">
                            <option value="${occ.mmc_occu_id}" 
                                    <c:if test="${occ.mmc_occu_id eq moreInvestor.occupation}">
                                        selected                                                                     
                                    </c:if> >${occ.mmc_occu_name}</option>
                        </c:forEach>
                        <option value="0">Other</option>
                    </select>
                    <input type="text" name="Occupation" placeholder="Enter occupation " class=" selectOcc input-field more-input-occupation" onclick ="removeSpanError()"/>
                    <span class="error error-more-occupation"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous6 action-button-previous"  onclick="prev7(this)" value="Previous" />
    <input type="button" name="next" class="next7 action-button" onclick="next7(this)" value="Continue" />
</fieldset>
<fieldset class="morestep2" id="morestep2"  style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please provide the contact details for Investor : <span class="director-name investername">Jon Snow</span>
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-4">
                    <label class="label_input">
                        Home address
                    </label>
                </div>
                <div class="col-sm-4">
                    <button class="director-btn postal-address all-btn-color upper-btn same-as-investor1" onclick="sameAddress(this)">Same as Investor 1</button>
                </div>
                <div class="col-sm-4">
                    <input type="address" class="form-control input-field more-investor-address clear-more-investor address1" placeholder="Enter new address" onclick ="removeSpanError()"/>
                    <input type="hidden" class="more-investor-postalcode"/>
                    <span class="error error-more-address"></span>
                </div>
                <div class="col-sm-4">
                    <label class="label_input">
                        Mobile number 
                    </label>
                </div>
                <div class="col-sm-8 mobile_number first-mobile mobile-space">
                    <input type="text" class="form-control codenumber more-investor-codenumber more-investor-countrycode jont-mob" placeholder="" readonly="readonly">
                    <input type="tel" class="form-control error codename more-investor-mobile clear-more-investor" name="mobileNo" placeholder="Enter mobile number" onclick ="removeSpanError()" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                    <span class="error error-more-mobile"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous7 action-button-previous"   onclick="prev8(this)" value="Previous" />
    <input type="button" name="next" class="next8 action-button" onclick="next8(this)" value="Continue" />
</fieldset>
<fieldset class="morestep3"  id="morestep3"  style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please provide identificaiton details for Investor : <span  class="director-count"></span> <span class="director-name investername">Jon Snow</span>
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <!-- <div class="col-sm-12">
                        <p><span class="director-name investername">Jon Snow</span></p>
                </div> -->
                <div class="col-sm-6">
                    <label class="label_input detail-2">
                        Which type of ID are you providing
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption form-group Id_Type" onchange="changeFund(this)">
                        <option value="1">NZ Driver Licence</option>
                        <option value="2">NZ Passport</option>
                        <option value="3">Other</option>
                    </select>
                </div>
                <div class="row drivery-licence2">
                    <div class="col-sm-6">
                        <label class="label_input">
                            First name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name more_licence_first_name" name="licence_first_name" placeholder="Enter first name" onclick = "removeSpanError();"/>
                        <span class="error error_more_licence_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name more_licence_middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                        <span class="error error_more_licence_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last name 
                        </label>.
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="last_name more_licence_last_name"  name="licence_last_name" placeholder="Enter last name" onclick = "removeSpanError();" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                        <span class="error error_more_licence_last_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Licence number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field more-investor-no1 clear-more-investor more-investor-licenseNumber" name="license_number" placeholder="Enter licence number " onclick = "removeSpanError();"/>
                        <span class="error error-more-licenseNumber"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" class="input-field more-investor-exp clear-more-investor lic_expiry_Date more-investor-licenseExpiryDate" name="dob" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" onchange="removeDate();" data-lang="en" required/>                                 
                        <span class="error error-more-licenseExpiryDate"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Version number 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="input-field more-investor-licence-ver clear-more-investor lic_verson_number more-investor-versionNumber" placeholder="Enter version number" onclick = "removeSpanError();" onkeypress='return (event.charCode >= 48 && event.charCode <= 57)'/>
                        <span class="error error-more-versionNumber"></span>
                    </div>
                </div>
                <div class="row passport-select2">
                    <div class="col-sm-6">
                        <label class="label_input">
                            First name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="more_passport_first_name first_name" name="passport_first_name" placeholder="Enter first name" onclick = "removeSpanError();"/>
                        <span class="error error_passport_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="more_passport_middle_name middle_name" name="passport_middle_name" placeholder="Enter middle name"/>
                        <span class="error error_passport_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="more_passport_last_name last_name" name="passport_last_name" placeholder="Enter last name" onclick = "removeSpanError();"/>
                        <span class="error error_passport_last_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Passport number  
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field more-investor-no2 clear-more-investor more-investor-passportNumber" name="passport_number" placeholder="Enter passport number" onclick = "removeSpanError();" />
                        <span class="error error-more-passportNumber"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text"  class="input-field more-investor-exp clear-more-investor pass_expiry more-investor-passportExpiryDate" name="passport_expiry" placeholder="dd/mm/yyyy" onchange="removeDate();" data-format="dd/mm/yyyy" data-lang="en" required/>                                 
                        <span class="error error-more-passportExpiryDate"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of issue  
                        </label>
                    </div>
                    <div class="col-sm-6 flag-drop">
                        <input type="text" class="form-control more-investor-countryname more-investor-passportCountryOfIssue" placeholder="Enter Country Code" readonly="readonly">
                        <span class="error error-more-investor-passportCountryOfIssue"></span>
                    </div>
                </div>
                <div class="row other-select2">
                    <div class="col-sm-6">
                        <label class="label_input">
                            Type of ID   
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-field more-investor-id-type clear-more-investor more-investor-typeOfId" placeholder="Enter ID type" onclick = "removeSpanError();" />
                        <span class="error error-more-typeOfId"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            First name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name"/>
                        <span class="error" id="error_other_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name"/>
                        <span class="error" id="error_other_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last name 
                        </label>
                    </div>                                                    
                    <div class="col-sm-6">
                        <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name"/>
                        <span class="error" id="error_other_last_name"></span>
                    </div> 
                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="dob" class="input-field more-investor-exp clear-more-investor more-investor-typeOfIdExpiryDate" placeholder="dd/mm/yyyy" onchange="removeDate();" data-format="dd/mm/yyyy" data-lang="en" required/>                                 
                        <span class="error error-more-typeOfIdExpiryDate"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of issue  
                        </label>
                    </div>
                    <div class="col-sm-6 flag-drop">
                        <input type="text" class="form-control more-investor-countryname more-investor-typeOfIdCountryOfIssue countryname" placeholder="Enter Country Code" readonly="readonly">
                        <span class="error error-more-investor-typeOfIdCountryOfIssue"></span>
                    </div>
                    <div class="col-sm-129 closestcls">
                        <input type="file" class="form-control more-investor-file attach-btn clear-more-investor checkname" onclick = "removeSpanError();"  accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="checkname(this)" >
                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile" style="display: none"><i class="far fa-times-circle"></i></a>
                        <span class="error error-more-file"></span>
                    </div>
                </div>
                <input type="hidden" name="more_investor_verify" id="more_investor_verify" value="">
                <input type="hidden" name="more_investor_verify" id="" value="" class="more_investorPep_verify">


            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous8 action-button-previous" onclick="prev9(this)" value="Previous" />
    <input type="button" name="next" class="next9 action-button" onclick="next9(this)" value="Continue" />
</fieldset>
<fieldset class="morestep4" id="morestep4" style="display: none">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the tax details for Investor : <span class="director-count"></span><span class="director-name investername"></span>
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <!-- <div class="col-sm-12">
                        <p><span class="director-name investername">Jon Snow</span></p>
                </div> -->
                <div class="col-sm-6">
                    <label class="label_input">
                        Prescribed Investor Rate (PIR)
                    </label>
                </div>
                <div class="col-sm-3 details-pos">
                    <select class="selectoption form-group aml-select presInvestRate">
                        <option value="28%">28%</option>
                        <option value="17.5%">17.5%</option>
                        <option value="10.5%">10.5%</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <a href="https://www.ird.govt.nz/roles/portfolio-investment-entities/using-prescribed-investor-rates" target="_blank" class="aml-link pir">What?s my PIR?</a>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        IRD Number 
                    </label>
                </div>
                <div class="col-sm-6 validate-ird">
                    <input type="text" class="form-control input-field more-investor-irdno clear-more-investor more-investor-irdNumber"  placeholder="XXX-XXX-XXX" onclick = "removeSpanError();"   onkeypress="checkird(this, event)" onchange="checkird(this, event)"/>
                    <a class="ird-check" href="javascript:void(0)" style="display: none"><i class="fa fa-check-circle" aria-hidden="true" ></i></a>
                    <span class="error error_more_irdNumber" ></span>
                </div>
                <!--                <div class="col-sm-6">
                                    <label class="label_input">
                                        What is the source of funds or wealth for this account?
                                    </label>
                                </div>
                                <div class="col-sm-6 ">
                                    <select class="selectoption">
                                        <option value="0">-Select- </option>
                                        <option value="1">Property sale </option>
                                        <option value="2"> Personal</option>
                                        <option value="3"> employment</option>
                                        <option value="4"> Financial investment</option>
                                        <option value="5">  Business sale</option>
                                        <option value="6">  Inheritance/gift</option>
                                        <option value="7"> Other </option>
                                    </select>
                                </div>-->
                <div class="col-sm-6">
                    <label class="label_input" style="text-align:left">
                        Are you a US citizen or US tax resident, or a tax resident in any other country?
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption selectoption2 form-group usCitizen more-investor-uscitizen more-investor-usCitizen" onchange="changeCountry(this)">
                        <option value="1">No</option>
                        <option value="2">Yes</option>
                    </select>
                </div>
                <div class="row yes-option2">
                    <div class="row yes-new3 checktindata1">
                        <div class="col-sm-12">
                            <h5 class="element-header aml-text">
                                Please enter all of the countries (excluding NZ) of which you are a tax resident. 
                            </h5>
                        </div> 
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Country of tax residence
                            </label>
                        </div> 
                        <!--more-investor-tex_residence_Country, more-investor-TIN, more-investor-resn_tin_unavailable-->
                        <div class="col-sm-6 details-pos flag-drop">
                            <input type="text" class="form-control countrynameoutnz more-investor-tex_residence_Country clear-more-investor more-investor-countryOfTaxResidence" placeholder="Enter Country Code" readonly="readonly">    
                            <span class="error  error-more-investor-tex_residence_Country" ></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Tax Identification Number (TIN)
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field more-investor-TIN clear-more-investor tin_unavailable" placeholder="Enter TIN " onkeyup="myFunction()" onclick = "removeSpanError();"/>
                            <span class="error error-TIN" ></span>
                        </div>
                        <div class="col-sm-6">
                            <label class="label_input">
                                Reason if TIN not available  
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control input-field more-investor-resn_tin_unavailable clear-more-investor resn_tin_unavailable" requiredmore-investor-resn_tin_unavailable="required" placeholder="" onclick = "removeSpanError();"/>
                            <span class="error error-resn_tin_unavailable"></span>
                        </div>
                    </div>
                    <div class="col-sm-129 add-another">
                        <a class="add-another2 all-btn-color add-country-another3" href="javascript:void(0)" onclick="addAnotherCountry(this)">Add another country </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous9 action-button-previous"   onclick="prev10(this)"  value="Previous" />
    <input type="button" name="next" class="next10 action-button"  onclick="next10(this)" value="Continue" />
</fieldset>	