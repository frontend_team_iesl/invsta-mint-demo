
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment Fund</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <div class="Portfolio-page">
                                <!--                                <div class="header-image">
                                <sec:authorize access="hasAuthority('ADMIN')">
                                    <div class="edit-using-btn">
                                        <a id="home-tab"  href="./update-portfolio-${pc}" role="tab"  ><i class="fa fa-edit"></i>Edit</a>
                                    </div>
                                </sec:authorize>
                                <div class="container1">
                                    <div class="header-logo">
                                        <div class="header-logo_row">
                                            <div class="cols-1 add-pic">
                                                <img src="${portfolioDetail.portfolio_pic}">
                                            </div>
                                            <div class="cols-2 add-font">
                                                  <p>Single Currency Portfolio</p> 
                                                <h1 id="fund-name ">${portfolioDetail.fundName} </h1>

                                                <div class="info-port companies-dta-value-fund ">

                                                    <div class="info-port-text">
                                                        <div class="up_makt">
                                                            <p>Unit price</p>
                                                            <i class="fa fa-arrow-up"></i>
                                                            <span id="fund-price">

                                                            </span>

                                                        </div>
                                                    </div>



                                                    <div class="info-port-text">
                                                        <div class="up_makt">
                                                            <p>Fund size </p>
                                                            <i class="fa fa-arrow-up"></i>
                                                            <span id="fund-size">

                                                            </span>

                                                        </div>
                                                    </div>



                                                    <div class="info-port-text">
                                                        <div class="up_makt">
                                                            <p>1 Year Return</p>
                                                            <i class="fa fa-arrow-up"></i>
                                                            <span id="fund-return">

                                                            </span>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>-->

                                <div class="modal fade" id="add-investment-fund-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">${portfolioDetail.fundName} </h5>
                                                <!--                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                    <span aria-hidden="true">&times;</span>
                                                                                                </button>-->
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">

                                                    <!--                                                    <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label>Portfolio Name :</label><br>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <div class="dropdown">
                                                                                                                    <select name="invest" class="invest">
                                                                                                                        <option value="290002">Mint Australia NZ Active Equity Ret</option>
                                                                                                                        <option value="290004">Mint Retail Property Trust</option>
                                                                                                                        <option value="290006">Mint Diversified Income Fund</option>
                                                                                                                        <option value="290012">Mint Diversified Growth Fund</option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Beneficiary Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                                <option >-select-</option>
                                                                <c:forEach items="${beneficiaryDetails}" var="category">
                                                                    <option value="${category.getId()}">${category.getName()}</option>
                                                                </c:forEach> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" id="investmentName" name ="investmentName" pattern="Enter Investment Name"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Amount :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="number" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount" min="0"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 bankaccounthide">
                                                        <div class="form-group">
                                                            <label>Bank Account Details:</label><br>
                                                        </div>
                                                    </div>
                                                </div>
                                                <c:forEach items="${bankAccountDetails}" var="bank">
                                                    <div  class="bankaccountdiv">
                                                        <input type="hidden" name="id" class="bankid" value="${bank.getId()}">
                                                        <input type="hidden" name="id" class="beneficiarid" value="${bank.getBeneficiaryId()}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Account Name
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group accountname">
                                                                    ${bank.getAccountName()}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Bank
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group bankname">
                                                                    ${bank.getBank()}  
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Branch
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group branchname">
                                                                    ${bank.getBranch()} 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Account 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group account">
                                                                    ${bank.getAccount()} 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="btn-pay">
                                                                    <button type="button" class="btn btn-primary saveInvestmentbtn pay-model">pay</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </c:forEach>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary close-model" data-dismiss="modal">Close</button>
                                                <button type="button" id="saveInvestmentbtn" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="container2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-box funds-radius">
                                                <div class="funds-line">
                                                    <c:forEach items="${allPortfolios}" var="portfolio">
                                                        <div class="funds-number">
                                                            <a href="./portfolio-${portfolio.getCode()}">
                                                                <span class="fund-box"></span> <span class="fund-name-data">${portfolio.getName()}</span> 
                                                            </a>
                                                        </div>
                                                    </c:forEach>
                                                    <!--                                                    <div class="funds-number">
                                                                                                            <a href="./portfolio-290012">
                                                                                                                <span class="fund-box fund-box-2"></span> <span class="fund-name-data">Mint Diversified Growth Fund</span> 
                                                                                                            </a>
                                                                                                        </div>
                                                                                                        <div class="funds-number">
                                                                                                            <a href="./portfolio-290006">
                                                                                                                <span class="fund-box fund-box-3"></span> <span class="fund-name-data">Mint Diversified Income Fund</span> 
                                                                                                            </a>
                                                                                                        </div>
                                                                                                        <div class="funds-number">
                                                                                                            <a href="./portfolio-290002">
                                                                                                                <span class="fund-box fund-box-4"></span> <span class="fund-name-data">Mint Australasian Equity Fund</span> 
                                                                                                            </a>
                                                                                                        </div>-->
                                                </div>
                                                <div class="new-invest-btn">
                                                    <a href="./make-investment-${pc}" class="invest-btn-style">Make Investments</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="invest-fund-header">
                                                <h2 class="fund-size">${portfolioDetail.fundName}</h2>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="fund-live-value">
                                                <div class="price-value">
                                                    <span class="price-name">Unit Price</span> <span class="value-name" id="fund-price"><i class="fa fa-arrow-up" aria-hidden="true"></i>$${portfoliosPrice.getMidPrice()}</span>
                                                </div>
                                                <div class="price-value">
                                                    <span class="price-name">Fund Size</span> <span class="value-name"  id="fund-size" contenteditable="true"><i class="fa fa-arrow-up" aria-hidden="true" ></i>$000.0m</span>
                                                </div>
                                                <div class="price-value">
                                                    <span class="price-name">1 Year Return</span> <span class="value-name" id="fund-return" contenteditable="true"><i class="fa fa-arrow-up" aria-hidden="true" ></i>0.0%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row Portfolio-page-text">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="new-design-hearder">Performance Since Inception </h6>

                                            </div>

                                            <div class="char_section1">
                                                <div class="element-box">
                                                    <div class="new-invest-chart">
                                                        <div class="inception-section">
                                                            <h5>Invested Since Inception</h5>
                                                            <h4 id="performaceInception">$90,688</h4>
                                                        </div>
                                                        <!--                                                        <div class="month-section">
                                                                                                                    <div class="month-section">
                                                                                                                        <span class="mont-drop">Year </span> 
                                                                                                                        <select class="date-option">
                                                                                                                            <option>2019</option>
                                                                                                                            <option>2018</option>
                                                                                                                            <option>2017</option>
                                                                                                                            <option>2016</option>
                                                                                                                        </select>
                                                                                                                    </div>
                                                                                                                    <div class="month-section">
                                                                                                                        <span class="mont-drop">Month </span> 
                                                                                                                        <select class="date-option">
                                                                                                                            <option>Feb</option>
                                                                                                                            <option>Jan</option>
                                                                                                                            <option>Dec</option>
                                                                                                                            <option>Nov</option>
                                                                                                                        </select>
                                                                                                                    </div>
                                                                                                                </div>-->
                                                    </div>
                                                    <!--<div id="fundPerformanceChart1" style="height: 400px; overflow: hidden;" data-highcharts-chart="1"></div>-->
                                                    <div id="stockbalance" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 mt-3">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Fund Performance </h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="new-perform">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label" >
                                                                            1 Month
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="1-month"  contenteditable="true"></div>
                                                                            <p class="date_upd"> as at 31 mar 2019</p>
                                                                        </div>

                                                                        <div class="trending trending-up 1-monthTrending" hidden>
                                                                            <span contenteditable="true">12.95</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            3 Months
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="3-month"  contenteditable="true"></div>
                                                                            <p class="date_upd"> as at 31 mar 2019</p>
                                                                        </div>



                                                                        <div class="trending trending-up 3-monthTrending" hidden>
                                                                            <span>10.78</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 

                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            1 Year
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="1-year" contenteditable="true" >14.08</div>
                                                                            <p class="date_upd"> as at 31 mar 2019</p>
                                                                        </div>


                                                                        <div class="trending trending-up 1-yearTrending" hidden>
                                                                            <span>24.04</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                        <div class="label">
                                                                            5 Years
                                                                        </div>
                                                                        <div class="value">
                                                                            <div class="down-data" id="5-year"  contenteditable="true">10.61</div>
                                                                            <p class="date_upd"> as at 31 mar 2019</p> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <form id="myForm" action="./update-PortfolioDetail" method="POST" enctype="multipart/form-data">
                                                                <input type="hidden" name="Portfolio">
                                                                <!--                                                                    <input type="hidden" name="portfolio_pic">-->
                                                                <input type="hidden" name="portfolio_pic" value="${portfolioDetail.portfolio_pic}">
                                                                <input type="hidden" name="FundOverview">
                                                                <input type="hidden" name="MonthlyFundUpdate">
                                                                <input type="hidden" name="FeaturesOftheFund">
                                                                <input type="hidden" name="monthlyReturnValue">
                                                                <input type="hidden" name="fundName">

                                                                <!--<input type="submit" value="Submit"/>-->
                                                            </form>
                                                        </div>

                                                        <div class="element-wrapper">
                                                            <div class="btn-warning2">
                                                                <button class="updateportfolio " onclick="updatePortfolio();">
                                                                    <i class="fa fa-save" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <h6 class="new-design-hearder">Fund Overview </h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="row">
                                                                <div class="col-md-12 update-fund-pera" id="paragraph" contenteditable="true">${portfolioDetail.getFundOverview()}</div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Risk Indicator <div class="pulsating-circle circle-2 risk-circle pulsating-around2 " data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                                    <span class="tooltiptext tolltip-1">Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund.</span>
                                                                </div></h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box risk-radius">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                    <div class="new-risk-section">
                                                                        <div class="low-risk">
                                                                            <h6>Lower risk</h6>
                                                                        </div>
                                                                        <div class="low-risk risk-width">
                                                                            <div class="risk-point">
                                                                                <span class="risk">1</span>
                                                                                <span class="risk">2</span>
                                                                                <span class="risk">3</span>
                                                                                <span class="risk">4</span>
                                                                                <span class="risk">5</span>
                                                                                <span class="risk">6</span>
                                                                                <span class="risk">7</span>
                                                                                <span class="risk">8</span>
                                                                                <span class="risk">9</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="low-risk">
                                                                            <h6>Higher risk</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="risk-data">
                                                            <h6>Potentially lower returns</h6>
                                                            <h6 class="risk-color">Potentially higher returns</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <form action="./UpdateTotalFmcaInvestmentMix" method="post" id="submit-UpdateTotalFmcaInvestmentMix" name="submit-complete2" class="editable-btn">  

                                                            <div class="element-wrapper">
                                                                <div class="btn-warning2">
                                                                    <button type="submit" class="save_btn" id="btn-UpdateTotalFmcaInvestmentMix-save" style="" >
                                                                        <i class="fa fa-save" aria-hidden="true"></i>

                                                                    </button>
                                                                </div>
                                                                <h6 class="new-design-hearder">Target Investment Mix</h6>
                                                            </div>
                                                            <div class="element-box">
                                                                <div class="el-chart-w" id="container19" style="height:448px">
                                                                    <table id="portfolio-tableFmcaInvestmentMix" class="table table-bordered" style="height: 255px; max-width: 600px; margin: 0px auto;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Investment Name
                                                                                    <input type="hidden" name="portfolio" value="${pc}">
                                                                                </th>
                                                                                <th>
                                                                                    price
                                                                                </th>
                                                                                <th>
                                                                                    ColorCode

                                                                                </th>
                                                                                <th>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="targetdata1">


                                                                        <tfoot>
                                                                            <tr>
                                                                                <th class="ab">
                                                                                    <a href="javascript:void(0)" onclick="onTargetFund1();"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>
                                                                                </th>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                    </form>


                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <form action="./UpdatefmcaTopAsset" method="post" id="submit-complete2" name="submit-complete2" class="editable-btn"> 

                                                            <div class="element-wrapper">
                                                                <div class="btn-warning2">
                                                                    <button type="submit" class="save_btn" id="btn-save2" style="" >
                                                                        <i class="fa fa-save" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                                <h6 class="new-design-hearder">Sector Allocation</h6>
                                                            </div>
                                                            <div class="element-box">
                                                                <div class="el-chart-w"  style="height:448px">
                                                                    <table id="portfolio-table" class="table table-bordered" style="height: 255px; max-width: 600px; margin: 0px auto;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    investmentName
                                                                                    <input type="hidden" name="portfolio" value="${pc}">
                                                                                </th>
                                                                                <th>
                                                                                    price
                                                                                </th>
                                                                                <th>
                                                                                    ColorCode

                                                                                </th>
                                                                                <th>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="targetdata">


                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    <a href="javascript:void(0)" onclick="onTargetFund();"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>

                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </form>                     
                                                    </div>  
                                                    <div class="col-md-4">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Performance</h6>
                                                        </div>
                                                        <div class="element-box22">
                                                            <div class="card tab-card">
                                                                <div class="card-header tab-card-header">
                                                                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                                        <li class="nav-item active">
                                                                            <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Last Year</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">2 Years ago</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">3 Years ago</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="four-tab" data-toggle="tab" href="#four" role="tab" aria-controls="Four" aria-selected="false">Return Average</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-content" id="myTabContent">
                                                                    <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                                        <div id="invest-bar-1" style="height: 400px; margin: 0 auto"></div>              
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                                                                        <div id="invest-bar-2" style="height: 400px; margin: 0 auto"></div>                
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                                                                        <div id="invest-bar-3" style="height: 400px; margin: 0 auto"></div>                
                                                                    </div>
                                                                    <div class="tab-pane fade p-3" id="four" role="tabpanel" aria-labelledby="four-tab">
                                                                        <div id="invest-bar-4" style="height: 400px; margin: 0 auto"></div>             
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  

                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Month in review</h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="element-wrapper download-btn-right">
                                                                <a href="" class="fund-detail-btn">Download Full Update</a>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12" >
                                                                    <h6><span id="portheading">${portfolioDetail.getMonthlyReturnValue()}</span></h6>
                                                                </div>
                                                                <div class="col-md-12" id="portfoliooverview" contenteditable="true">${portfolioDetail.getMonthlyFundUpdate()}</div>

                                                            </div>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Features of the fund</h6>
                                                        </div>
                                                        <div class="element-box">
                                                            <div class="new-feature-text">${portfolioDetail.getFeaturesOftheFund()}</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Portfolio Manager</h6>
                                                        </div>
                                                        <div class="element-box manager-style">
                                                            <!--                                                                                                                <div class="element-wrapper">
                                                                                                                                                                                <h6 class="element-header">Portfolio Manager</h6>
                                                                                                                                                                            </div> -->
                                                            <div class="invest-team">
                                                                <div class="row">
                                                                    <form id="investerForm" action="./UpdateInvestmentTeam" method="post" enctype="multipart/form-data">
                                                                        <input type="hidden" name="portfolio" value="${pc}"/>
                                                                        <c:forEach items="${investmentTeam}" var="team">
                                                                            <div class="col-md-12 col-sm-12 five-div invest-team-images manager-section">
                                                                                <div class="row">
                                                                                    <div class="col-md-2  invest-team-images">
                                                                                        <img src="${team.pic_url}" alt="image" class="Imgurl">
                                                                                        <div class="camra-icon camra-icon1">
                                                                                            <input type="file" name="image_file" onchange="readURL(this);"/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="manager-content">
    <!--                                                                                        <h6>${team.place}</h6>
                                                                                        <p>${team.name}</p>-->
                                                                                            <select name="place">
                                                                                                <option value="Head of Investments" <c:if test="${team.place eq 'Head of Investments'}"> selected </c:if>>Head of Investments</option>
                                                                                                <option value="Portfolio Manager"<c:if test="${team.place eq 'Portfolio Manager'}"> selected </c:if>>Portfolio Manager</option>
                                                                                                </select>
                                                                                                <input type="hidden" name="pic_url" value="${team.pic_url}"/>
                                                                                            <input type="text" name="name" value="${team.name}"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="col-md-12 col-sm-12 five-div invest-team-images">
                                                                                <div class="bio-input" contenteditable="true">${team.team_bio}</div>

                                                                            </div>
                                                                        </c:forEach>
                                                                        <div class="btn-warning3 submit-data btn-botom">
                                                                            <button type="submit" class="updateportfolio " onclick="submitTeam();">Submit</button>
                                                                        </div>
                                                                        <input type="hidden" name="team_bio">
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Resources</h6>
                                                        </div>
                                                        <div class="element-box-new">
                                                            <c:forEach items="${investmentPdf}" var="pdf">
                                                                <div class="resources-pdf element-box risk-radius pdf-area">
                                                                    <p>${pdf.pdf_name} </p>
                                                                    <span class="download-icon">
                                                                        <a href="${pdf.pdf_url}"><img src="resources/images/download-down.png"></a>
                                                                    </span>
                                                                </div>
                                                            </c:forEach>
                                                            <!--                                                            <div class="resources-pdf">
                                                                                                                            <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>-->
                                                            <!--                                                                </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint SRI Policy</p>
                                                                                                                            <span >
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>UNPRI Report</p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>-->

                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-wrapper">
                                                            <h6 class="new-design-hearder">Quarterly Fund Updates</h6>
                                                        </div>
                                                        <div class="element-box-new">
                                                            <c:forEach items="${quarterlyPdf}" var="pdf">
                                                                <div class="resources-pdf element-box risk-radius pdf-area">
                                                                    <p>${pdf.pdf_name} </p>
                                                                    <span class="download-icon">
                                                                        <a href="${pdf.pdf_url}"><img src="resources/images/download-down.png"></a>
                                                                    </span>
                                                                </div>
                                                            </c:forEach>
                                                            <!--                                                            <div class="resources-pdf">
                                                                                                                            <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>-->
                                                            <!--                                                                </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint SRI Policy</p>
                                                                                                                            <span >
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>UNPRI Report</p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-sm-6 col-md-2">
                                                                                    <div class="side_barr">
                                                                                        <a class="button_inv btn-style" data-target="#add-investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">Make Investment</a>
                                                                                        <h3>Other Funds</h3>
                                                                                        <div class="other_port">
                                        
                                        <c:forEach items="${otherPortfolioDetail}" var="port">
                                            <a href="javascript:void(0)" id="fundlink1">
                                                <div class="other_row">
                                                    <div class="other_row-img">
                                                        <img src="./resources/images/globe-plane.jpg">
                                                    </div>
                                                    <div class="other_row-text">
                                                        <p><b id="other-fund1">${port.fundName}</b></p>
                                                    </div>
                                                </div>
                                            </a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="./resources/js/sweetalert.min.js"></script>

        <script>

                                                                                function readURL(input) {
                                                                                    if (input.files && input.files[0]) {
                                                                                        var reader = new FileReader();
                                                                                        reader.onload = function (e) {
                                                                                            $(input).closest('.invest-team-images').find('.Imgurl').attr('src', e.target.result);
                                                                                        };
                                                                                        reader.readAsDataURL(input.files[0]);

                                                                                    }
                                                                                }

                                                                                var fmcaTopAssets = [];
                                                                                var fmcaTopAssetsColorCode = [];
                                                                                var totalFmcaInvestmentMix = [];
                                                                                var totalFmcaInvestmentMixcolorCode = [];
                                                                                var portfolioCode;
                                                                                var performanceChartDate = [];
                                                                                var performanceChartData = [];
                                                                                $(document).ready(function () {
                                                                                    $('.new-feature-text').find('p').attr("contenteditable", "true");

                                                                                    $.each(${fmcaTopAssets}, function (idx, val) {
                                                                                        //                alert(JSON.stringify(val.investmentName));
                                                                                        fmcaTopAssets.push([val.investmentName, val.price]);
                                                                                        fmcaTopAssetsColorCode.push(val.colorCode);
                                                                                    });
                                                                                    $.each(${totalFmcaInvestmentMix}, function (idx, val2) {
                                                                                        //                alert(JSON.stringify(val2.InvestmentName));
                                                                                        totalFmcaInvestmentMix.push([val2.InvestmentName, val2.Price]);
                                                                                        totalFmcaInvestmentMixcolorCode.push(val2.colorCode);
                                                                                    });
                                                                                    //            piechart1();
                                                                                    //            alert(JSON.stringify(totalFmcaInvestmentMix));
                                                                                    $('.bankaccountdiv').hide();
                                                                                    $('.bankaccounthide').hide();
                                                                                    var pc = '${pc}';
                                                                                    portfolioCode = pc;
                                                                                    var fundName = '${portfolioDetail.fundName}';
                                                                                    piechart1();
                                                                                    $.each(${performancePrices}, function (idx, val) {
                                                                                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                                                                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                                                                        var d = new Date(val.CreatedDate);
                                                                                        //document.write("The current month is " + monthNames[d.getMonth()]);
                                                                                        //var str = "Hello world!";
                                                                                        var res = d.getFullYear().toString();
                                                                                        var yy = res.substring(2, 4);
                                                                                        console.log("The current year is " + [val.CreatedDate]);
                                                                                        console.log("The current year is " + res);
                                                                                        performanceChartDate.push(monthNames[d.getMonth()] + " " + yy);
                                                                                        performanceChartData.push(val.LastPrice);
                                                                                    });
                                                                                    var inceptionNav = performanceChartData[0];
                                                                                    var todayNav = performanceChartData[performanceChartData.length - 1];
                                                                                    var performaceInception = ((todayNav - inceptionNav) / inceptionNav) * 100;
                                                                                    $('#performaceInception').text(performaceInception.toFixed(2) + '%');
//                                                                                        areachart1(fundName);
            <c:forEach items="${fmcaTopAssets}" var="aShare" varStatus="map">
                                                                                    htmlCode = '<tr>';
                                                                                    //                                                     
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="investmentName" value="${aShare.investmentName}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" value="${aShare.price}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" value="${aShare.colorCode}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    addTargetDeletedata('${map.index+1}');
                                                                                    targetlastindex = '${map.index+2}';
                                                                                    htmlCode = htmlCode + '</tr>';
                                                                                    $("#targetdata").append(htmlCode);
            </c:forEach>
            <c:forEach items="${totalFmcaInvestmentMix}" var="aShare" varStatus="map">
                                                                                    htmlCode = '<tr>';
                                                                                    //                                                     
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="investmentName" value="${aShare.investmentName}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" value="${aShare.price}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" value="${aShare.colorCode}" required/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    addTargetDeletedata1('${map.index+1}');
                                                                                    targetlastindex1 = '${map.index+2}';
                                                                                    htmlCode = htmlCode + '</tr>';
                                                                                    $("#targetdata1").append(htmlCode);
            </c:forEach>


                                                                                    function addTargetDeletedata(idx) {
                                                                                        htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund(' + (idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                                                                                        htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></i></a>';
                                                                                        htmlCode = htmlCode + '</td>';
                                                                                    }
                                                                                    function addTargetDeletedata1(idx) {
                                                                                        htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//                htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund1(' + (idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                                                                                        htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow1(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></i></a>';
                                                                                        htmlCode = htmlCode + '</td>';
                                                                                    }

                                                                                });
                                                                                function onTargetFundDeleteRow(anchor) {
                                                                                    var idx = anchor.parentNode.parentNode.rowIndex;
                                                                                    document.getElementById("portfolio-table").deleteRow(idx);
                                                                                    percentageTotal();
                                                                                }
                                                                                function onTargetFundDeleteRow1(anchor) {
                                                                                    var idx = anchor.parentNode.parentNode.rowIndex;
                                                                                    document.getElementById("portfolio-tableFmcaInvestmentMix").deleteRow(idx);
                                                                                    percentageTotal();
                                                                                }
                                                                                function onTargetFund() {
                                                                                    //                                        alert(0);
                                                                                    var htmlCode = '';
                                                                                    htmlCode = '<tr>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control" type="text" name="investmentName" id="investmentName" required="required" placehoder="investmentName" value="investmentName" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode !== 32) && !(event.charCode >=48 && event.charCode <=57 ))"/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" id="prices" value="0" required="required" />';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" id="colorCode" value="0" required="required" />';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund(' + eval(idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                                                                                    htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></a>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '</tr>';
                                                                                    $("#targetdata").append(htmlCode);
                                                                                }
                                                                                function onTargetFund1() {
                                                                                    //                                        alert(0);
                                                                                    var htmlCode = '';
                                                                                    htmlCode = '<tr>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control" type="text" name="investmentName" id="investmentName" required="required" placehoder="investmentName" value="investmentName" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode !== 32) && !(event.charCode >=48 && event.charCode <=57 ))"/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="prices" id="prices" value="0" required="required" onchange=" percentageTotal();"/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right">';
                                                                                    htmlCode = htmlCode + '<input class="form-control taget-alloc-per" type="text" step="0.01" name="colorCode" id="colorCode" value="0" required="required" onchange=" percentageTotal();"/>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '<td class="text-right d-flex justify-content-betweeen delete-btn-end">';
//            htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFund1(' + eval(idx) + ');"><span class="extra-tooltip"></span><i class="fa fa-plus"></i></a>';
                                                                                    htmlCode = htmlCode + '     <a href="javascript:void(0)" onclick="onTargetFundDeleteRow1(this);"><span class="extra-tooltip"></span><i class="fa fa-trash-o"></i></a>';
                                                                                    htmlCode = htmlCode + '</td>';
                                                                                    htmlCode = htmlCode + '</tr>';
                                                                                    $("#targetdata1").append(htmlCode);
                                                                                    percentageTotal();
                                                                                }





//                                                                                        });
                                                                                $('#beneficiaryId').change(function () {
                                                                                    $('.bankaccountdiv').hide();
                                                                                    $('.bankaccounthide').hide();
                                                                                    var beneficiaryval = $('#beneficiaryId option:selected').text();
                                                                                    $('#investmentName').val(beneficiaryval);
                                                                                    var beneficiaryid = $(this).val();
                                                                                    var beneficiarvals = document.getElementsByClassName('beneficiarid');
                                                                                    for (var i = 0; i < beneficiarvals.length; i++) {
                                                                                        var beneficiarval = beneficiarvals[i];
                                                                                        var root = $(beneficiarval).closest('.bankaccountdiv');
                                                                                        if (beneficiarval.value === beneficiaryid) {
                                                                                            root.show();
                                                                                            $('.bankaccounthide').show();
                                                                                        }
                                                                                    }

                                                                                });
                                                                                $('#saveInvestmentbtn').click(function () {
                                                                                    var root = $(this).closest(".bankaccountdiv");
                                                                                    var bankAccountId = root.find('.bankid').val();
                                                                                    var investmentName = $('#investmentName').val();
                                                                                    var investmentAmount = $('#investmentAmount').val();
                                                                                    var beneficiaryId = $('#beneficiaryId option:selected').val();
                                                                                    var applicationId = '${applicationId}';
                                                                                    var fundCode = '${pc}';
                                                                                    var fundName = '${portfolioDetail.fundName}';
                                                                                    var obj = {portfolioName: fundName, investmentName: investmentName, investedAmount: investmentAmount, portfolioCode: fundCode, applicationId: applicationId, beneficiaryId: beneficiaryId, bankAccountId: bankAccountId};
                                                                                    swal({
                                                                                        title: "Proceed",
                                                                                        text: "Investment is progress.",
                                                                                        type: "info",
                                                                                        timer: 2500,
                                                                                        showConfirmButton: true
                                                                                    });
                                                                                    $.ajax({
                                                                                        type: 'POST',
                                                                                        url: './rest/groot/db/api/admin/pending-investment',
                                                                                        data: JSON.stringify(obj),
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {

                                                                                            swal({
                                                                                                title: "Success",
                                                                                                text: "Investment is successfully.",
                                                                                                type: "success",
                                                                                                timer: 2500,
                                                                                                showConfirmButton: true
                                                                                            });
                                                                                            location.reload(true);
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            alert("error");
                                                                                        }
                                                                                    });
                                                                                });
        </script>
        <script>
            areachart1(fundName) {
                Highcharts.chart('stockbalance', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {

                    },
                    xAxis: {
                        categories: performanceChartDate,
                        tickInterval: 150,
                        labels: {
                            style: {
                                color: '#333',
                                fontSize: '12px',
                                textTransform: 'uppercase'
                            },
                            y: 20,
                            x: 10
                        },
                        lineColor: '#dadada'
                    },
                    yAxis: {
                        min: 0,
                        //                        max: 4,
                        tickInterval: 0.5,
                        title: {
                            text: ''
                        },
                        //                labels: {
                        //                    formatter: function () {
                        //                        return this.value / 1000 + 'k';
                        //                    }
                        //                }
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'center',
                        verticalAlign: 'bottom',
                        enabled: false

                    },
                    plotOptions: {
                        series: {
                            color: '#93dbd0',
                            shadow: true,
                            lineWidth: 3,
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        pointFormatter: function () {
                            var isNegative = this.y < 0 ? '-' : '';
                            return  isNegative + '$' + Math.abs(this.y.toFixed(2));
                        }
                    },
                    series: [{
                            name: '',
                            data: performanceChartData
                        }],
                    responsive: {
                        rules: [{
                                condition: {
                                    maxWidth: 500
                                }
                            }]
                    }

                });
            }
        </script> 
        <script>
            $(document).ready(function () {
                $("#view_transaction").hide();
                $("#view_transaction_status").click(function () {
                    $("#view_transaction").toggle();
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".company-show1").hide();
                $(".company-show2").hide();
                $(".company-show3").hide();
                $(".company-show4").hide();
                $(".company-show5").hide();
                $(".company-show6").hide();
                $(".company-show7").hide();
                $(".company-show8").hide();
                $(".company-show9").hide();
                $(".companyshow1").click(function () {
                    $(".company-show1").toggle();
                });
                $(".companyshow2").click(function () {
                    $(".company-show2").toggle();
                });
                $(".companyshow3").click(function () {
                    $(".company-show3").toggle();
                });
                $(".companyshow4").click(function () {
                    $(".company-show4").toggle();
                });
                $(".companyshow5").click(function () {
                    $(".company-show5").toggle();
                });
                $(".companyshow6").click(function () {
                    $(".company-show6").toggle();
                });
                $(".companyshow7").click(function () {
                    $(".company-show7").toggle();
                });
                $(".companyshow8").click(function () {
                    $(".company-show8").toggle();
                });
                $(".companyshow9").click(function () {
                    $(".company-show9").toggle();
                });
            });
        </script>

        <script>
            function piechart1() {
                Highcharts.setOptions({
                    colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                            // colors: fmcaTopAssetsColorCode
                });
                Highcharts.chart('container-221', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        maxHeight: 70,
                    },
                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 150,
                            depth: 45,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false,
                                format: '{point.percentage:.1f} %'
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            name: 'Brands',
                            colorByPoint: true,
                            data: fmcaTopAssets
                        }]
                });
                Highcharts.setOptions({
                    // colors: totalFmcaInvestmentMixcolorCode
                    colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                });
                Highcharts.chart('container19', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 150,
                            depth: 45,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                distance: 2,
                                connectorWidth: 0,
                                enabled: false,
                                format: '{point.percentage:.1f} %'
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                            name: 'Brands',
                            colorByPoint: true,
                            data: totalFmcaInvestmentMix
                        }]
                });
            }
        </script>



        <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/get-FundPerformance-${pc}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log("data-->" + data);
                        var obj = JSON.parse(data);
                        $(".loader").hide();
                        console.log(typeof obj);
                        $('#1-month').text(obj.onemonth);
                        $('.1-monthTrending').find('span').text(obj.onemonthTrending);
                        $('#3-month').text(obj.threemonth);
                        $('.3-monthTrending').find('span').text(obj.threemonthTrending);
                        $('#1-year').text(obj.oneyear);
                        $('.date_upd').text(obj.lastUpdate);
                        $('.1-yearTrending').find('span').text(obj.oneyeartrending);
                        $('#5-year').text(obj.fiveyear);
                        //                    $('#fund-price').text(obj.unit_price);
                        $('#fund-size').text(obj.fund_size);
                        $('#fund-return').text(obj.one_year_return);
                        $('.risk').each(function (i) {
                            var statsValue = $(this).text();
                            if (statsValue === obj.riskIndicator) {
                                $(this).addClass("risk-point-radius");
                            }
                        });
                        console.log('here is end');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                    alert(${pc});
                        alert("error");
                    }
                });
            });

            $(".risk-point span").on("click", function () {
                $(".risk-point span").removeClass("risk-point-radius");
                $(this).addClass("risk-point-radius");
            });



        </script>

        <script>

            function updatePortfolio() {

                $('.new-feature-text').find('p').removeAttr("contenteditable");
                var Portfolio =${pc};
                var FundOverview = $('.update-fund-pera').html();
                //        
                var MonthlyFundUpdate = $('#portfoliooverview').html();
                var FeaturesOftheFund = $('.feature-text').html();
                var monthlyReturnValue = $('.portheading').text();
                var fund = $('#fund-name').text();
                //            alert(fund);
                //            alert(JSON.stringify(FundOverview));
                //            alert(JSON.stringify(Portfolio));
                //            alert(JSON.stringify(MonthlyFundUpdate));
                //            alert(JSON.stringify(FeaturesOftheFund));
                $('form input[name=Portfolio]').val(Portfolio);
                $('form input[name=FundOverview]').val(FundOverview);
                $('form input[name=MonthlyFundUpdate]').val(MonthlyFundUpdate);
                $('form input[name=FeaturesOftheFund]').val(FeaturesOftheFund);
                $('form input[name=monthlyReturnValue]').val(monthlyReturnValue);
                $('form input[name=fundName]').val(fund);
                $("#myForm").submit();
            }
            ;

        </script>
        <script>

            function  updateFundPerformance() {
                var portfolio = '${pc}';
                var onemonth = $('#1-month').text();
                var onemonthTrending = $('.1-monthTrending').text();
                var threemonth = $('#3-month').text();
                var threemonthTrending = $('.3-monthTrending').text();
                var oneyear = $('#1-year').text();
                var oneyeartrending = $('.1-yearTrending').text();
                var fiveyear = $('#5-year').text();
                var unit_price = $('#fund-price').text();
                var fund_size = $('#fund-size').text();
                var one_year_return = $('#fund-return').text();
                var riskIndicator = $('.indicator-box .active').text();
                var obj = {portfolio: portfolio, onemonth: onemonth, riskIndicator: riskIndicator, onemonthTrending: onemonthTrending, threemonth: threemonth, threemonthTrending: threemonthTrending, oneyear: oneyear, oneyeartrending: oneyeartrending, fiveyear: fiveyear, unit_price: unit_price, fund_size: fund_size, one_year_return: one_year_return};
                console.log(JSON.stringify(obj));
                //            alert(JSON.stringify(obj));
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/update-FundPerformance',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(obj),
                    success: function (data, textStatus, jqXHR) {
                        alert("sucess");
                        location.reload();


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });

            }
            ;
            //        function  updateCurrentFundPerformance() {
            //            var portfolio = '${pc}';
            //            var unit_price = $('#fund-price').text();
            //            var fund_size = $('#fund-size').find('span').text();
            //            var one_year_return = $('#fund-return').text();
            //
            //            var obj = {portfolio: portfolio, unit_price: unit_price, fund_size: fund_size, one_year_return: one_year_return};
            //            console.log(JSON.stringify(obj));
            //            $.ajax({
            //                type: 'POST',
            //                url: './rest/groot/db/api/update-CurrentFundPerformance',
            //                headers: {"Content-Type": 'application/json'},
            //                data: JSON.stringify(obj),
            //                success: function (data, textStatus, jqXHR) {
            //                    alert("sucess");
            //
            //
            //                },
            //                error: function (jqXHR, textStatus, errorThrown) {
            //                    alert("error");
            //                }
            //            });
            //
            //        }
            //        ;

        </script>
        <!--    <script>
                var funddetails = new Map();
        
                $(document).ready(function () {
        
                    $('.feature-text').find('p').attr("contenteditable", "true");
        
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/get-FundPerformance-${pc}',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            console.log("sucess" + data);
                            var obj = JSON.parse(data);
                            //                    alert(JSON.stringify(obj));
                            console.log("obj" + obj);
                            funddetails = obj;
                            //                    alert(JSON.stringify(funddetails));
        
                            $('#1-month').text(funddetails.onemonth);
                            $('.1-monthTrending').text(funddetails.onemonthTrending);
                            $('#3-month').text(funddetails.threemonth);
                            $('.3-monthTrending').text(funddetails.threemonthTrending);
                            $('#1-year').text(funddetails.oneyear);
                            $('.1-yearTrending').text(funddetails.oneyeartrending);
                            $('#5-year').text(funddetails.fiveyear);
                            $('#fund-price').text(funddetails.unit_price);
                            $('#fund-size').text(funddetails.fund_size);
                            $('#fund-return').text(funddetails.one_year_return);
                            $('.risk-btns').each(function (i) {
                                var statsValue = $(this).text();
                                if (statsValue === funddetails.riskIndicator) {
                                    $(this).addClass("active");
                                }
                            });
        
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("error");
                        }
                    });
        
        
        
                });
        
        
        
                $(".indicator-box span").on("click", function () {
                    $(".indicator-box span").removeClass("active");
                    $(this).addClass("active");
                });
        
        
        
            </script>-->



    </body>
</html>