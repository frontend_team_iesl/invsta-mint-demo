<%-- 
    Document   : new-password
    Created on : 16 Dec, 2019, 10:00:20 AM
    Author     : Admin
--%>


<%-- 
    Document   : login
    Created on : Jul 24, 2019, 4:16:48 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="home" value="${pageContext.request.contextPath}"/>

<html lang="en">
    <head>
        <title>Login V2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./resources/favicon.png" rel="shortcut icon"/>
        <!--===============================================================================================-->	
        <!--<link rel="icon" type="image/png" href="./resources/js/login-js/images/icons/favicon.ico"/>-->
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/iconic/css/material-design-iconic-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animate.css"> 
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/util.css">
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/main.css">
        <!--===============================================================================================-->
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/sweetalert.css" rel="stylesheet"/>
        <link href="./resources/css/main1.css" rel="stylesheet"/>
        <style>
            .login100-form-bgbtn {
                background: #65b9ac;
            } 
            .focus-input100::before {
                background: #65b9ac;
            }
            .btn-show-pass {
                color: #65b9ac;
            }
            .btn-show-pass:hover {
                color: #65b9ac;
            }
            #new-paswrde-page button#bt {
                font-family: Poppins-Medium;
                font-size: 15px;
                color: #fff;
                line-height: 1.2;
                text-transform: uppercase;
                display: -webkit-box;
                display: -webkit-flex;
                display: -moz-box;
                display: -ms-flexbox;
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 0 20px;
                width: 100%;
                height: 50px;
                border-radius: 50px;
                background: #65b9ac;
                border: 2px solid #65b9ac;
                transition: .4s;
                margin-top: 85px;
                float: none;
            }
            #new-paswrde-page button#bt:hover {
                background: #fff;
                border: 2px solid #65b9ac;
                color: black;
            }
            #new-paswrde-page textarea:focus, input:focus {
                border-color: #65b9ac !important;
                background: #65b9ac1f;
            }
            section#new-paswrde-page label {
                color: #424242;
                padding: 0;
                margin: 5px 0px 0px;
            }
            section#new-paswrde-page input#password {
                padding: 0;
                margin: 0px 0px 33px;
                border-bottom: 2px solid #91d9cf;    
                font-size: 14px;
                float: left;

            }
            section#new-paswrde-page input#reEnterPass {
                padding: 0;
                border-bottom: 2px solid #91d9cf;
                font-size: 14px;
                float: left;
            }
           
             #new-paswrde-page span.login100-form-title.p-b-022 img {
                 width: 132px;
             }
             #new-paswrde-page fieldset {
                 margin-top: 0rem;
                 width: 390px;
                 background: #fff;
                 border-radius: 10px;
                 overflow: hidden;
                 padding: 77px 55px 77px 55px;
                 box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
                 -moz-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
                 -webkit-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
                 -o-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
                 -ms-box-shadow: 0 5px 10px 0px rgba(0, 0, 0, 0.1);
             }
             #new-paswrde-page .wrap-login100.my {
                 box-shadow: none;
                 padding: 0;
                 width: auto;
                 margin: 35px 0px;
             }
             #new-paswrde-page .container-login100 {
               padding: 0px 15px;
             }
             #new-paswrde-page i.zmdi.zmdi-eye {
                 position: relative;
                 top: 34px;
                 left: 109px;
                 color: #65b9ac;
                 cursor: pointer;
             }
             i.zmdi.zmdi-eye.new {
                 left: 127px !important;
             }
             @media only screen and (max-width: 360px) {
                 i.zmdi.zmdi-eye.new {
                     left: 74px !important;
                 }
                 #new-paswrde-page i.zmdi.zmdi-eye {

                     left: 57px;

                 }
             } 

        </style>
    </head>
    <body>
        <section id="new-paswrde-page">
            <div class="limiter">
                <div class="container-login100">
                    <fieldset id="f12">
                        <span class="login100-form-title p-b-022">
                             <img src="resources/images/mint.png">
                            </span>
                        <div class="wrap-login100 my">                   
                            <form action="./createPassword" method="POST" id="first_form">
                                <input type="hidden" id="hiddenEmail" name="token" value="${token}">
                                <label>Create New  Password</label>         
                                <input  class="input100"  type="password" id="password" placeholder="Enter password" name="password" required />
                               <span class="btn-show-eyee">
                                    <i class="zmdi zmdi-eye"></i>
                                </span>
                                <label> Re-enter password  </label>          
                                <input class="input100" type="password" id="reEnterPass" name="re_enter_password" placeholder="Re-Enter your password" onkeyup="myfunction1()" required="required">
                                <span class="btn-show-eyee">
                                    <i class="zmdi zmdi-eye new"></i>
                                </span>
                                <br><span id="error-span" class="error"></span><br>
                                <button type="submit" id="bt" class="forgetbutton" >Save </button>
                            </form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </section>
        <!--===============================================================================================-->
        <!--===============================================================================================-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="./resources/js/login-js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/popper.js"></script>
        <script src="./resources/js/login-js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/moment.min.js"></script>
        <script src="./resources/js/login-js/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/main.js"></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
        <script>
                                    $(document).ready(function () {
                                        $("#f2").hide();
                                        if (${not empty message}) {
                                            swal({
                                                title: '${title}',
                                                text: '${message}',
                                                timer: 2000,
                                                showConfirmButton: false
                                            });
                                        }
                                        if (${not empty user.role}) {
                                            if ('${user.role}' === "SUBMISSION_COMPANY" || '${user.role}' === "SUBMISSION_TRUST") {
                                                swal({
                                                    title: "Info",
                                                    text: "Thank you for providing your application to us. You will be able to successfully login to your account post successful verification of your application. We will notify you through a confirmation email once this process is completed.",
                                                    timer: 2000,
                                                    showConfirmButton: false
                                                });
                                            }
                                        }

                                    });

        </script>

        <script type="text/javascript">
            function myfunction1() {
                var pass = $("#password").val();
                var re = $("#reEnterPass").val();
                if (pass !== re) {
                    $("span").text("Password is miss mach ");
                }
                if (pass == re) {
                    $("error").text('');
                    $("span").text('');
                }
            }
        </script>
    </body>
</html>

