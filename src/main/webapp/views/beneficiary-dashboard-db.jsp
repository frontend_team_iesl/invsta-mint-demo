<%-- 
    Document   : dashboard
    Created on : Jul 25, 2019, 2:38:52 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Beneficiary Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        </head>
        <body>
            <div class="loader">
                <ul class="circle-loader">
                    <li class="center"></li>
                    <li class="item item-1"></li>
                    <li class="item item-2"></li>
                    <li class="item item-3"></li>
                    <li class="item item-4"></li>
                    <li class="item item-5"></li>
                    <li class="item item-6"></li>
                    <li class="item item-7"></li>
                    <li class="item item-8"></li>
                </ul>
            </div>
            <!--        <div id="overlay">
                        <img src="./resources/images/Preloader_3.gif" alt="Loading" />
                        Loading...
                    </div>-->
            <div class="all-wrapper menu-side with-side-panel">
                <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="content-w">
                        <!--------------------
                        START - Breadcrumbs
                -------------------->
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item ">
                                <a href="./home">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">Welcome</a>
                            </li>
                            <li class="breadcrumb-item">
                            </li>
                        </ul>
                        <!--------------------
                        END - Breadcrumbs
                          -------------------->
                        <div class="content-panel-toggler">
                            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                        </div>
                        <div class="content-i">
                            <div class="content-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <p>Hi <span class="logged-user-name first-name">${user.name}</span>, Welcome to invsta. Here’s an overview of your investments with us:</p><br>
                                            <h6 class="element-header">
                                                Investment Overview
                                            </h6>
                                            <div class="element-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller historical-list">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_all_days">Historical Performance</a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview">
                                                            <!--                                                        <div class="label" style="display:inline-block;">
                                                                                                                        Amount Invested
                                                                                                                                                                                        <div class="pulsating-circle pulsating-around" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"> 
                                                                                                                                                                                            <span class="tooltiptext">The Live Balance chart provides an indication of the real-time balance of your  portfolio, and is reliant on external data sources which may be different to what is reported on exchanges. The actual daily closing value and withdrawal value of your portfolio will be different. This balance does not include any cash that is available in your account.</span></div>
                                                            
                                                                                                                    </div>-->
                                                            <div class="pop-one label" onmouseover="myFunction()" onmouseout="myFunction()" style="display:inline-block;">
                                                                Contributions 
                                                                <span class="popup-text " id="myPopup">
                                                                    <div class="pop-main">
                                                                        <div class="section-pop-1">
                                                                            <span>Contributions</span>
                                                                            <span class="currentBalance">$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Reinvested Distns</span>
                                                                            <span>$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Distns</span>
                                                                            <span>$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Withdrawals</span>
                                                                            <span class="withdrawal">$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Fees Paid/Rebates</span>
                                                                            <span>-$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Tax Owed/Rebates</span>
                                                                            <span class="taxOwed">-$0.00</span>
                                                                        </div>
                                                                        <div class="section-pop-1">
                                                                            <span>Gain/Losses</span>
                                                                            <span>$0.00</span>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="label current-label" style="float:right; display:inline-block; ">
                                                                CURRENT Value
                                                                <!--                                                                <div class="pulsating-circle2 pulsating-around2" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                                                                                    <span class="tooltiptext">(previous day close, incl cash in wallet) This represents the actual value of your  portfolio investments based on the previous day closing value, and includes any cash you may have available in your cash account.</span>
                                                                                                                                </div>-->
                                                            </div>
                                                            <div class="" style="padding:0 0 10px;width: 100%;float: left;">
                                                                <div class="value currentBalance">$ 0.00 

                                                                </div>
                                                                <div class="value sumWalletBalance" style="float:right;">
                                                                    <div class="sumWalletBal" >  $ 0.00   </div>
                                                                    <span class="founder-icon-color"><div class="percentage" id="percentageid"> 0.0% </div> 
                                                                        <!--                                                                        <img class="income-high" src="./resources/images/ins.gif"></img>-->
                                                                        <div class="pulsating-circle2 pulsating-around2 one" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                            <span class="tooltiptext dashboard-toltip">This is an annualised* money weighted** return after fees and tax, since you have been invested in the fund. *The return weights how long each amount has been invested for by how much has been invested to work out the average years each dollar has been invested for. If you have been invested for less than one year, then the return is not annualised. **The return takes into account when your cash flows occur and how big they are. Past performance is not a reliable indicator of future returns.</span>
                                                                        </div>
                                                                    </span>
                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="tab-pane active" id="tab_all_days">

                                                            <div id="stockhistorical" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                        <div class="tab-pane" id="investment-composition">
                                                            <div class="label">
                                                                BALANCE   <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                            </div>
                                                            <div class="el-tablo">
                                                                <div class="value prev-actual-balance">$ 0.00

                                                                </div>
                                                            </div>
                                                            <div id="composition-chart" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                        <div class="element-box">
                                                                                        <div class="new-invest-chart">
                                                                                            <div class="start-amnt second-historical">
                                                                                                <div class="tab-pane active custom-add-box">
                                                                                                    <div class="" style="">
                                                                                                        Contributions
                                                                                                    </div>
                                                                                                    <div class="value add-value  ">$0.0</div>
                                                                                                </div>
                                                                                                <div class="custom-add-box-1" style="">
                                                                                                    <div class=" current-label add-label">
                                                                                                        Current Value
                                                                                                    </div>
                                                                                                    <div class="value add-value sumWalletBalancenew clr-add" style="">
                                                                                                        <div class="sumWalletBal ">$0.0</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="stockhistorical" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                                                                    </div>-->
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">


                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Investment Breakdown 
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">

                                                        <div class="tab-pane active" id="tab_latest_week-cons-invs-pieChart" style="" aria-expanded="false">
                                                            <div class=" el-tablo">

                                                                <div class="el-chart-w" id="investmentBreakdown" style="height:420px"></div>
                                                                <div id="legend-2"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                            Performance    
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#tab_overview-cons-invs-pieChart" aria-expanded="true">Last Year
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart2" aria-expanded="true">2 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart3" aria-expanded="true">3 Years ago
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_overview-cons-invs-pieChart4" aria-expanded="true">Average Return
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago1YearsBarChart" style="height: 370px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart2" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago2YearsBarChart" style="height: 370px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart3" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="ago3YearsBarChart" style="height: 370px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_overview-cons-invs-pieChart4" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="avgBarChart" style="height: 370px;"></div>
                                                                <div id="legend-1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="element-wrapper">
                                            <div class="element-box max-box">
                                                <div class="os-tabs-w">
                                                    <div class="os-tabs-controls">

                                                        <ul class="nav nav-tabs justify-content-center" role="tablist">

                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="tabhome224-tab" data-toggle="tab" href="#tabhome224" role="tab" aria-controls="tabhome224" aria-selected="false">
                                                                    Asset Allocation</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                            <div class=" el-tablo">
                                                                <div id="assetAllocation" style="height:420px""></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-board" id="investments">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Investments </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="portfolio" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                <h6 style="margin-bottom:0px; display: inline-block; margin-right: 5px;" class="pcName">invsta Australasian Equity Fund </h6>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller box-margin">
                                                            <div class="label">
                                                                Contributions 
                                                            </div>
                                                            <div class="value contribution">
                                                                NZ$ 0.00 
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller box-margin">
                                                            <div class="label ">
                                                                Current Balance
                                                            </div>
                                                            <div class="value marketValue" id="currentInvSqr">
                                                                NZ$ 0.00
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller box-margin">
                                                            <div class="label">
                                                                Performance
                                                            </div>
                                                            <div class="arrow-text">
                                                                <div class="value per- per-color- percentage performance-increase performancerate">
                                                                    0.00 % 
                                                                    <!--                                                                    <i class="fa fa-arrow-up" aria-hidden="true"></i>-->
                                                                </div>

                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller box-margin">
                                                            <div   class="lg-latestweek" style="height:70px; width:100%"></div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="show_more_btn">
                                                    <input type="hidden" class="investment-code">
                                                    <input type="hidden" class="portfolio-code">
                                                    <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)" onclick="showmemore(this)">Show Me More</a>
                                                </div>
                                            </div>
                                        </div>                                                                             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form id="showmemoreform" action="./show-me-more" method="post">
                    <input type="hidden" name="ic" id="investment-code"/>
                    <input type="hidden" name="pc" id="portfolio-code"/>                           
                </form>
                <div class="display-type"></div>
            </div>
        <jsp:include page="footer.jsp"/>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
        <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
        <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script>
                                                        var returnrate = [];
                                                        var perfomacarr = [];
                                                        var performanceChartDate = [];
                                                        var performanceChartData = [];
                                                        function executeBEDB() {

                                                            var idurl = '${id}';
                                                            var ic = '${ic}';
                                                            if (ic !== '') {
                                                                idurl = '${id}&ic=${ic}';
                                                                        }
//                    $.ajax({
//                        type: 'GET',
//                        url: './rest/groot/db/api/beneficiary-dashboard?id='+idurl,
//                        headers: {"Content-Type": 'application/json'},
//                        success: function (data, textStatus, jqXHR) {
//                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                            if (obj.investmentHoldings.length > 0
//                                    && obj.investmentPerformances.length > 0
//                                    && obj.fmcaInvestmentMix.length > 0) {
//                                dashboard(obj);
//                                $(".loader").hide();
//                            } else {




                                                                        $.ajax({
                                                                            type: 'GET',
                                                                            url: './rest/groot/db/api/investment-holdings?id=' + idurl,
                                                                            headers: {"Content-Type": 'application/json'},
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                console.log("InvestmentHolding" + data);
                                                                                $(".loader").hide();
                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                if (obj.investmentHoldings.length > 0) {
                                                                                    investmentHoldings(obj);
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/groot/db/api/investment-performances?id=' + idurl,
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            if (obj.investmentPerformances.length > 0) {
                                                                                                investmentPerformances(obj);
                                                                                            }
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/3rd/party/api/beneficiary-${user.beneficiairyId}',
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            console.log(JSON.stringify(obj));
                                                                                            if (obj.FirstName !== "" && obj.FirstName !== null) {
                                                                                                $('.first-name').text(obj.FirstName);
                                                                                            } else {
                                                                                                $('.first-name').text('${user.name}');
                                                                                            }
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/groot/db/api/investment-return-rate?ic=' + obj.investmentHoldings[0].InvestmentCode,
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            var returnrateper = obj.investmentReturnRate;
                                                                                            $('#percentageid').text((returnrateper * 100).toFixed(2) + "%");
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });
                                                                                    $.ajax({
                                                                                        type: 'GET',
                                                                                        url: './rest/groot/db/api/fmca-investment-mix?id=' + idurl,
                                                                                        headers: {"Content-Type": 'application/json'},
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            console.log('success' + data);
                                                                                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                            if (obj.fmcaInvestmentMix.length > 0) {
                                                                                                fmcaInvestmentMix(obj);
                                                                                            }
                                                                                        },
                                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                                            console.log('error');
                                                                                        }
                                                                                    });

                                                                                } else {
                                                                                    swal({
                                                                                        title: "Info",
                                                                                        text: "You have no investment yet",
                                                                                        timer: 2000,
                                                                                        showConfirmButton: false
                                                                                    });
                                                                                }
                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log('error');
                                                                            }
                                                                        });
//                            }
//                        },
//                        error: function (jqXHR, textStatus, errorThrown) {
//                            console.log('error');
//                        }
//                    });
                                                                    }

                                                                    var investmentAmount = 0;
                                                                    var withdrawal = 0;
                                                                    var taxOwed = 0;
                                                                    var CurrentValue = 0;
                                                                    var portfolio = document.getElementById("portfolio");
                                                                    var investments = document.getElementById("investments");
                                                                    var fundMap = new Map();
                                                                    var codeArr = [];
                                                                    var totalUnits = 0;
                                                                    var investmentcode = "";
                                                                    function investmentHoldings(obj) {
                                                                        var investmentHoldings = obj.investmentHoldings;
                                                                        var arr = [];

                                                                        for (var i = 0; i < investmentHoldings.length; i++) {
                                                                            var performances = investmentHoldings[i];
                                                                            var clone = portfolio.cloneNode(true);
                                                                            var h6 = clone.getElementsByClassName("pcName")[0];
                                                                            var portfolioName = performances.PortfolioName.replace("Mint","Invsta");
                                                                            if (!portfolioName.toString().includes("Invsta")) {
                                                                                portfolioName = "Invsta " + portfolioName;
                                                                            }
                                                                            h6.innerHTML = portfolioName;
                                                                            withdrawal = withdrawal + performances.Withdrawals;
                                                                            taxOwed = taxOwed + performances.TaxOwed;
                                                                            var cont = clone.getElementsByClassName("contribution")[0];
                                                                            cont.innerHTML = '$' + performances.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                            investmentAmount = investmentAmount + performances.Contributions;
                                                                            var marVal = clone.getElementsByClassName("marketValue")[0];
                                                                            marVal.innerHTML = '$' + performances.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                            var latestweek = clone.getElementsByClassName("lg-latestweek")[0];
                                                                            var j = i + 1;
                                                                            latestweek.setAttribute("id", "lg-latestweek" + j);
                                                                            CurrentValue = CurrentValue + performances.MarketValue;
                                                                            clone.style.display = 'block';
                                                                            totalUnits += performances.Units;
//                                                                                    var href = './show-me-more-db-' + performances.InvestmentCode + '?pc=' + performances.PortfolioCode;
                                                                            investmentcode = performances.InvestmentCode;
                                                                            perfomacarr.push({investmentCode: performances.InvestmentCode, portfolioCode: performances.PortfolioCode});
//                                                                            clone.getElementsByClassName("showmemore")[0].setAttribute('href', "javascript:void(0)");
                                                                            var investment = clone.getElementsByClassName("investment-code")[0];
                                                                            var portcode = clone.getElementsByClassName("portfolio-code")[0];
                                                                            investment.value = performances.InvestmentCode;
                                                                            portcode.value = performances.PortfolioCode;
                                                                            arr.push([portfolioName, performances.MarketValue]);
                                                                            fundMap[performances.PortfolioCode] = portfolioName;
                                                                            codeArr.push(performances.PortfolioCode);
                                                                            investments.appendChild(clone);
                                                                        }
                                                                        historicalchart(codeArr, investmentcode);
                                                                        $(".currentBalance").html("$" + investmentAmount.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                        $(".withdrawal").html("$" + withdrawal.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                        $(".taxOwed").html("$" + taxOwed.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                        document.getElementsByClassName("sumWalletBal")[0].innerHTML = "$" + CurrentValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                        pieChart('investmentBreakdown', arr, true);
//                                                                        weekchart(codeArr);
                                                                        performacediv(perfomacarr);

                                                                    }

                                                                    function allPortfolioChart(obj) {

                                                                        var i = 0;
                                                                        var firstdate;
                                                                        for (var firstKey in obj) {
                                                                            if (i === 0) {
                                                                                firstdate = firstKey;
                                                                                i++;
                                                                            } else {
                                                                                break;
                                                                            }
                                                                        }
                                                                        var startdate = new Date(firstdate);
                                                                        var year = startdate.getFullYear();
                                                                        var month = startdate.getMonth();
                                                                        var day = startdate.getDate();

                                                                        $.each(obj, function (idx, val) {
                                                                            performanceChartData.push(val);
                                                                        });
                                                                        splinechart2(year, month, day);
                                                                    }

                                                                    function investmentPerformances(obj) {
                                                                        var investmentPerformances = obj.investmentPerformances;
                                                                        var performanceMap = new Map();
                                                                        var keys = new Array();
                                                                        for (var i = 0; i < investmentPerformances.length; i++) {
                                                                            var performance = investmentPerformances[i];
                                                                            var PortfolioArray = performanceMap[performance.Portfolio];
                                                                            if (typeof PortfolioArray === 'undefined') {
                                                                                PortfolioArray = new Array();
                                                                                keys.push(performance.Portfolio);
                                                                            }
                                                                            PortfolioArray.push(performance);
                                                                            performanceMap[performance.Portfolio] = PortfolioArray;
                                                                        }
                                                                        var avgSeries = [],
                                                                                ago1YearSeries = [],
                                                                                ago2YearSeries = [],
                                                                                ago3YearSeries = [];
                                                                        for (var key of keys) {//3 Keys
                                                                            var PortfolioArray = performanceMap[key]; var fundName = fundMap[key];
                                                                            if (typeof fundName === 'undefined') {
                                                                                fundName = key;
                                                                            }
                                                                            if (key === 'All') {
                                                                                fundName = 'Summary';
                                                                            }
                                                                            var avgPeriod = PortfolioArray[PortfolioArray.length - 1];
                                                                            var avgRR = avgPeriod.XIRRReturnRate * 100;
//                                                                                returnrate.push(avgRR);
                                                                            avgSeries.push({
                                                                                name: fundName,
                                                                                data: [avgRR]
                                                                            });
                                                                            var ago1YearPeriod = PortfolioArray[PortfolioArray.length - 2];
                                                                            var ago1YearRR = ago1YearPeriod.XIRRReturnRate * 100;
                                                                            ago1YearSeries.push({
                                                                                name: fundName,
                                                                                data: [ago1YearRR]
                                                                            });

                                                                            var ago2YearPeriod = PortfolioArray[PortfolioArray.length - 3];
                                                                            var ago2YearRR = ago2YearPeriod.XIRRReturnRate * 100;
                                                                            ago2YearSeries.push({
                                                                                name: fundName,
                                                                                data: [ago2YearRR]
                                                                            });

                                                                            var ago3YearPeriod = PortfolioArray[PortfolioArray.length - 4];
                                                                            var ago3YearRR = ago3YearPeriod.XIRRReturnRate * 100;
                                                                            ago3YearSeries.push({
                                                                                name: fundName,
                                                                                data: [ago3YearRR]
                                                                            });
                                                                        }
//                                                                            console.log(JSON.stringify(returnrate));
//                                                                            var perVal = document.getElementsByClassName("performancerate");
//                                                                            for (var i = 0; i < perVal.length - 1; i++) {
//                                                                                perVal[i + 1].innerHTML = returnrate[i].toFixed(2) + "%";
//                                                                            }
                                                                        barChart('avgBarChart', avgSeries);
                                                                        barChart('ago1YearsBarChart', ago1YearSeries);
                                                                        barChart('ago2YearsBarChart', ago2YearSeries);
                                                                        barChart('ago3YearsBarChart', ago3YearSeries);
//                                                                        getcharts();
                                                                    }

                                                                    function fmcaInvestmentMix(obj) {
                                                                        var assetAllocation = [];
                                                                        var fmcaInvestmentMix = obj.fmcaInvestmentMix;
//                    var fmcaInvestmentMixByTable = obj.fmcaInvestmentMixByTable;
//                    $.each(fmcaInvestmentMix, function (idx, val) {
//                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
//                    });
                                                                        $.each(fmcaInvestmentMix, function (idx, val) {
                                                                            assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                                                                        });
                                                                        pieChart('assetAllocation', assetAllocation, false);
                                                                    }
                                                                    function weekchart(arr, obj) {
                                                                        var i = 0;
                                                                        for (var firstKey in obj) {
                                                                            var weekarr = [];

                                                                            if (firstKey === arr[i]) {
                                                                                var weekdate = obj[firstKey];
                                                                                for (var weekday in weekdate) {
                                                                                    weekarr.push(weekdate[weekday]);
                                                                                }
                                                                                i++;
                                                                                getcharts("lg-latestweek" + i, weekarr);
                                                                            }
                                                                        }
                                                                    }
                                                                    function performacediv(arr) {
                                                                        var perVal = document.getElementsByClassName("performancerate");
                                                                        var obj = {id: 1, investmentHoldings: arr};
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: './rest/groot/db/api/investment-return-rate',
                                                                            headers: {"Content-Type": 'application/json'},
                                                                            data: JSON.stringify(obj),
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                console.log('success' + data);
                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                var returnrateper = obj.investmentReturnRate;
                                                                                console.log(JSON.stringify(returnrateper));
                                                                                for (var j = 0; j < returnrateper.length; j++) {
                                                                                    perVal[j + 1].innerHTML = (returnrateper[j] * 100).toFixed(2) + "%";
                                                                                }
                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log('error');
                                                                            }
                                                                        });
                                                                    }
                                                                    function historicalchart(arr, ic) {
                                                                        var portfolios = arr.toString();
//                                                                            var portfolio;
//                                                                        $.ajax({
//                                                                            type: 'GET',
//                                                                            url: './rest/groot/db/api/getallPerformanceforchart?ic=' + ic + '&portfolios=' + portfolios,
//                                                                            headers: {"Content-Type": 'application/json'},
//                                                                            success: function (data, textStatus, jqXHR) {
//                                                                                console.log('success' + data);
//                                                                                var obj = JSON.stringify(data);
//                                                                                var obj = JSON.parse(obj.replace(/[\n\t\r]/g, ' '));
//                                                                                console.log('success' + JSON.stringify(obj));
//                                                                                allPortfolioChart(obj);
//                                                                                
//
//                                                                            },
//                                                                            error: function (jqXHR, textStatus, errorThrown) {
//                                                                                console.log('error');
//                                                                            }
//                                                                        });

                                                                        $.ajax({
                                                                            type: 'GET',
                                                                            url: './rest/groot/db/api/getUnitsPerformancechart?ic=' + ic + '&portfolios=' + portfolios,
                                                                            headers: {"Content-Type": 'application/json'},
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                console.log('week-success' + data);
                                                                                var obj = JSON.stringify(data);
                                                                                var obj = JSON.parse(obj.replace(/[\n\t\r]/g, ' '));
                                                                                console.log('week-success' + JSON.stringify(obj));
//                                                                                console.log('oneportolioo  ' + JSON.stringify(obj.A290002));
                                                                                allPortfolioChart(obj.valueByDate);
                                                                                weekchart(arr, obj);

                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log('error');
                                                                            }
                                                                        });



                                                                    }

                                                                    function dashboard(obj) {
                                                                        investmentHoldings(obj);
                                                                        investmentPerformances(obj);
                                                                        fmcaInvestmentMix(obj);
                                                                    }

                                                                    executeBEDB();
        </script>
        <script>
            function pieChart(idc, arr, mktval) {
                Highcharts.setOptions({
                    lang: {
                        thousandsSep: ','
                    }
                });
                Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
                var ptfmt = '';
                if (mktval) {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} % <br>Market Value: {point.y}';
                } else {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} %';
                }
                Highcharts.chart(idc, {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 0
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    tooltip: {
                        valueDecimals: 2,
                        pointFormat: ptfmt,
                        valuePrefix: '$',

                    },
//                    tooltip: {
//                    valueDecimals:2,
//                    pointFormatter: function () {
//                        var isNegative = this.y < 0 ? '-' : '';
//                        return  isNegative + '$' + Math.abs(this.y.toFixed(4).toString());
//                    }
//                },
                    plotOptions: {
                        pie: {
                            innerSize: 100,
                            depth: 45,
                            dataLabels: {enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            },
                        },
                    },
                    series: [{
                            showInLegend: true,
                            name: '',
                            data: arr
                        }]
                });
            }

            function barChart(idc, series) {
                Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
                Highcharts.chart(idc, {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''},
                    xAxis: {
                        categories: [''],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
//                        max: 30,
                        tickInterval: 5,
                        labels: {
                            format: '{value}%'
                        },
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0,
                            borderWidth: 0
                        }
                    },
                    series: series
                });
            }
        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
            Highcharts.chart('stockbalance', {
                chart: {
                    type: 'spline',
                },
                title: {
                    text: ''
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        '2017 Jan', '2017 Apr', '2017 Jul', '2017 Oct',
                        '2018 Jan', '2018 Apr', '2018 Jul', '2018 Oct',
                        '2019 Jan', '2019 Apr', '2019 Jul'],
                    tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        y: 20,
                        x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: false

                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 3,
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    pointFormatter: function () {
                        var isNegative = this.y < 0 ? '-' : '';
                        var s = isNegative + '$' + Math.abs(this.y.toFixed(0));
                        var b = s.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        return b;
//                             var isNegative = this.y < 0 ? '-' : '';
//                        return  isNegative + '$' + Math.abs(this.y.toFixed(4).toString());
                    }
                },
                series: [{
                        name: '',
                        data: [
//                            90688,
//                            90688 + 970.00,
//                            90688 + 888.80,
//                            90688 + 1009.60,
//                            90688 + 1202.45,
//                            90688 + 1460.47,
//                            90688 + 1800.66,
//                            90688 + 2092.99,
//                            90688 + 2207.57,
//                            90688 + 2654.50,
//                            90688 + 3037.57,
//                            90688 + 4883.94,
//                            90688 + 5762.66,
//                            90688 + 6864.18,
//                            90688 + 7903.55,
//                            90688 + 9018.43,
//                            90688 + 10095.45,
//                            90688 + 13109.44,
//                            90688 + 19630.21,
//                            90688 + 22010.11,
                            113081.42,
                            114035.62,
                            115055.92,
                            117057.41,
                            120343.07,
                            123068.20,
                            125022.82,
                            127051.06,
                            128001.64,
                            130004.75,
                            134644.35,
                        ]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            }
                        }]
                }

            });
        </script>        

        <script>
            function  getcharts(idc, arr) {
                Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
                Highcharts.chart(idc, {
                    title: {
                        text: ''
                    },
                    legend: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    xAxis: {
                        categories: [''],
                        title: {
                            text: null
                        },
                        labels: {enabled: false, }

                    },
                    yAxis: {
                        gridLineWidth: 0,
                        labels: {enabled: false, }, title: {
                            text: ''
                        },
                    },
                    tooltip: {
                        pointFormatter: function () {
                            var isNegative = this.y < 0 ? '-' : '';
                            var s = isNegative + '$' + Math.abs(this.y.toFixed(2));
                            var b = s.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            return b;
//                             var isNegative = this.y < 0 ? '-' : '';
//                        return  isNegative + '$' + Math.abs(this.y.toFixed(4).toString());
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                            name: 'Balance',
                            data: arr
                        }],
                });
            }
//                Highcharts.chart('lg-latestweek2', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Balance',
//                            data: [10000, 9700, 8700, 9280, 8900, 9758.43]
//                        }],
//                });
//
//                Highcharts.chart('lg-latestweek3', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Balance',
//                            data: [10000, 9700, 8500, 9280, 9100, 10898.47]
//                        }],
//                });
//
//                Highcharts.chart('lg-latestweek4', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Balance',
//                            data: [10000, 8700, 9000, 9280, 8900, 10258.28]
//                        }],
//                });
//
//                Highcharts.chart('lg-latestweek5', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Balance',
//                            data: [10000, 9700, 8700, 9280, 8900, 11540.38]
//                        }],
//                });
//
//                Highcharts.chart('lg-latestweek6', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Investment Balance',
//                            data: [10000, 9700, 8700, 9280, 8900, 11629.15]
//                        }],
//                });
//
//                Highcharts.chart('lg-latestweek7', {
//                    title: {
//                        text: ''
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    navigation: {
//                        buttonOptions: {
//                            enabled: false
//                        }
//                    },
//                    xAxis: {
//                        categories: [''],
//                        title: {
//                            text: null
//                        },
//                        labels: {enabled: false, }
//
//                    },
//                    yAxis: {
//                        gridLineWidth: 0,
//                        labels: {enabled: false, }, title: {
//                            text: ''
//                        },
//                    },
//                    credits: {
//                        enabled: false
//                    },
//                    series: [{
//                            name: 'Investment Balance',
//                            data: [10000, 9700, 8596, 9280, 10890, 12545.00]
//                        }],
//                });

        </script>
        <script>
            Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
            Highcharts.chart('composition-chart', {
                chart: {
                    type: 'spline',
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                yAxis: {
                    title: {
                        text: 'Performance'
                    }
                },
                series: [{
                        name: 'Magellan Global Equities Fund (MGE)',
                        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                    }, {
                        name: 'Magellan Global Equities Fund (Currency Hedged) (MHG)',
                        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                    }, {
                        name: 'Magellan Global Trust (MGG)',
                        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                    }, {
                        name: 'Magellan Global Fund',
                        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                    }, {
                        name: 'Magellan Global Fund (Hedged)',
                        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]

                    }]
            });
        </script>
        <script>
            function myFunction() {
                var popup = document.getElementById("myPopup");
                popup.classList.toggle("show");
            }
        </script>
        <script>
            function splinechart2(year, month, day) {

                Highcharts.stockChart('stockhistorical', {
                    chart: {
                        events: {
                            load: function () {
                                if (!window.TestController) {
                                    this.setTitle(null, {
                                        text: ''
                                    });
                                }
                            }
                        },
                        zoomType: 'x',
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },

                    rangeSelector: {

                        buttons: [{
                                type: 'day',
                                count: 3,
                                text: '3d'
                            }, {
                                type: 'week',
                                count: 1,
                                text: '1w'
                            }, {
                                type: 'month',
                                count: 1,
                                text: '1m'
                            }, {
                                type: 'month',
                                count: 6,
                                text: '6m'
                            }, {
                                type: 'year',
                                count: 1,
                                text: '1y'
                            }, {
                                type: 'all',
                                text: 'All'
                            }],
                        selected: 5
                    },
                    plotOptions: {
                        series: {
                            color: '#2c94ec',
                            lineWidth: 2,
                        }
                    },

                    yAxis: {
                        title: {
                            text: ''
                        }
                    },

                    title: {
                        text: ''
                    },

                    subtitle: {
                        text: '' // dummy text to reserve space for dynamic subtitle
                    },
                    navigator: {
                        enabled: true
                    },
                    scrollbar: {
                        barBackgroundColor: '#2c94ec',
                        barBorderRadius: 7,
                        barBorderWidth: 0,
                        barBorderHeight: 1,
                        buttonBackgroundColor: '#2c94ec',
                        buttonBorderWidth: 0,
                        buttonBorderRadius: 7,
                        trackBackgroundColor: 'none',
                        trackBorderWidth: 1,
                        trackBorderRadius: 8,
                        trackBorderColor: '#CCC'
                    },

                    series: [{
                            name: 'Value',
                            data: performanceChartData,
                            type: 'spline',
                            pointStart: Date.UTC(year, month, day),
                            pointInterval: 3600 * 1000 * 24,
                            tooltip: {
                                xDateFormat: '%d-%m-%Y',
                                valueDecimals: 2,
                                valuePrefix: '$'
                            }
                        }]

                });
            }
            function showmemore(ele) {
                var root = ele.closest('.show_more_btn');
                var ic = $(root).find('.investment-code');
                var pc = $(root).find('.portfolio-code');
                $('#investment-code').val(ic.val());
                $('#portfolio-code').val(pc.val());
                $('#showmemoreform').submit();
//                document.getElementById("trust").href = "./trust-signup?email='" + email + "'&password='" + password + "'";
            }
        </script>

    </body>
</html>