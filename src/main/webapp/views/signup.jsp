<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en" >
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./resources/favicon.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./resources/css/signup/custom.css">
        <link rel="stylesheet" href="./resources/css/signup/style.css">
        <link rel="stylesheet" href="./resources/css/signup/new.css">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <style>
            img.logo {
                width: 30%;
            }
            .save-class{
                display: none;
                /*display: flex;*/
            }
            .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
                top: 6px;
                position: relative;
                left: 20px;
                width: 0px;
                height: 0;
                border-left: 3px solid transparent;
                border-right: 3px solid transparent;
                border-top: 6px solid black;
            }    

            /*            span#error-generateOtp {
                            width: 100%;
                            text-align: center;
                             float: left; 
                            display: table;
                            margin-top: 14px;
                        }*/
            .signup-btn-west a {
                color: #fff;
                font-size: 14px;
            }
            .signup-btn-west p {
                background: linear-gradient(to right, #2c94ec, #304bb7);
                color: #fff;
                padding: 7px 10px;
                border-radius: 10px;
                overflow: hidden;
                font-size: 14px;
                border: 1px solid #2f5fc5;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="loged_user signup-btn-west">
                <a href="./login?logout"><p>Already have an Account Login</p></a>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="content_body">
                        <form id="msform" name="msRegform" class="form-signup" action="/chelmer" method="POST" autocomplete="off">
                            <div class="w3-light-grey">
                                <!--<div id="myBar" class="progress-bar" style="width:0%"></div>-->
                            </div>

                            <div class="logo-w">
                                <a href="#"><img class="logo" alt="" src="./resources/images/mint.png" ></a>
                            </div>
                            <div class="save-new-btn save-class">
                                <input type="hidden" name="register_id" id="register_id" value="${person.reg_id}">
                                <input type="hidden" name="register_type" id="register_type" value="INDIVIDUAL_ACCOUNT">
                                <input type="hidden" name="step" id="step" value="1">
                                <a href="javascript:void();" class=" saveExit director-btn all-btn-color save-font">Save & Exit</a>
                                <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title=" "><img src="./resources/images/i-icon.png"></a></span>-->
                                <div class="pulsating-circle4 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                    <span class="tooltiptext">You can save and exit this application at any time by clicking on this Save &amp; Exit button. We will then send you an email from which you can resume your application when you are ready.</span>
                                </div>
                            </div>
                            <fieldset id="step1">
                                <div class="content-section">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <!--                                            <h5 class="element-header4">
                                                                                            To make an investment, you'll need to create an account and provide some personal information.
                                                                                        </h5>-->
                                            <div class="element-wrapper">
                                                <h5 class="element-header">
                                                    To make an investment, you'll need to create an account and provide some personal information. 
                                                </h5>
                                            </div>

                                            <h5 class="element-header4">
                                                This should only take a few minutes, you  will need the following information on hand:
                                                <ul>
                                                    <li>ID details (driver licence or passport)</li>
                                                    <li>IRD number</li>
                                                    <li>Bank account details </li>
                                                    <li>Additional information will be required for Trusts & Company accounts </li>
                                                </ul>
                                            </h5>

                                        </div>
                                    </div>

                                    <!-- <span class="focus-border" style="display:block;" id="validationId" ></span> -->
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <a href="javascript:void(0)" name="previous" id="haveAccount" class="action-button-previous new-mint link-button"  > Already have account</a>
                                <input type="button" name="next" class="next10 action-button new-mint"  value="Create account"/>
                            </fieldset>
                            <fieldset id="step2">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            What type of account is this? 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-md-6 my-2">
                                                <a id="personal" class="" href="./personal-signup"  onclick="personal()">
                                                    <div class="button-lg">
                                                        <div class="btn-circle">
                                                            <div class="circle-img-area">
                                                                <img src="./resources/images/icon/img1.png">
                                                            </div>
                                                        </div>
                                                        <div class="btn-name">Individual Account </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-6 my-2">
                                                <a id="joint" class="" href="./joint-account" onclick="joint()">
                                                    <div class="button-lg">
                                                        <div class="btn-circle">
                                                            <div class="circle-img-area">
                                                                <img src="./resources/images/icon/img2.png">
                                                            </div>
                                                        </div>
                                                        <div class="btn-name">Joint Account</div>
                                                    </div>
                                                </a>
                                            </div>                                
                                            <div class="col-md-6 my-2" id="backon">
                                                <a id="company" class="" href="./company-signup" onclick="company()">

                                                    <div class="button-lg">
                                                        <div class="btn-circle">
                                                            <div class="circle-img-area">
                                                                <img src="./resources/images/icon/img3.png">
                                                            </div>
                                                        </div>
                                                        <div class="btn-name">Company Account</div>
                                                    </div>
                                                </a>
                                            </div>                                
                                            <div class="col-md-6 my-2">
                                                <a id="trust" class="" href="./trust-signup" onclick="trust()">
                                                    <div class="button-lg">
                                                        <div class="btn-circle">
                                                            <div class="circle-img-area">
                                                                <img src="./resources/images/icon/img4.png">
                                                            </div>
                                                        </div>
                                                        <div class="btn-name">Trust Account</div>
                                                    </div>
                                                </a>
                                            </div> 
                                            <div class="col-md-6 minor-account">
                                                <a id="minor" class="" href="./minor-signup" onclick="minor()">

                                                    <div class="button-lg">
                                                        <div class="btn-circle">
                                                            <div class="circle-img-area">
                                                                <img src="./resources/images/icon/img5.png">
                                                            </div>
                                                        </div>
                                                        <div class="btn-name">Minor Account </div>
                                                    </div>
                                                </a>
                                            </div> 
                                        </div>

                                    </div>
                                    <span class="focus-border" style="display:block;" id="validationId" ></span>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous1 action-button-previous" value="Previous" />
                                <input type="button" name="next" id="allAccount"  class="action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step3">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide your details below.  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Title
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption nameTitle form-group" name="title" id="src_of_fund" >
                                                    <option <c:if test = "${person.title eq 'Mr'}"> selected </c:if> >Mr</option>
                                                    <option <c:if test = "${person.title eq 'Mrs'}"> selected </c:if> >Mrs</option>
                                                    <option <c:if test = "${person.title eq 'Miss'}"> selected </c:if> >Miss</option>
                                                    <option <c:if test = "${person.title eq 'Master'}"> selected </c:if> >Master</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Full name
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class=" fullName form-control input-field" id="fullName" name="fullName" required="required" value="${person.fullName}" placeholder="Enter full name " onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                <span class="error" id="span-fullName"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Preferred name (if applicable) 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="preferredName" id="preferredName" placeholder="Enter preferred first name (optional)" class=" input-field" value="${person.preferredName}"   required onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                <span class="error" id="span-preferredName"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="date_of_Birth" id="dob" placeholder="dd/mm/yyyy" class=" input-field date-of-birth" value="${person.date_of_Birth}" data-format="dd/mm/yyyy" data-lang="en" required/>
                                                <span class="error" id="span-dob"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of residence 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="country_residence form-control countryname countrynameone" value="${person.country_residence}" id="countrynamefirst" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                <span class="error" id="span-countryname"></span>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Occupation 
                                                </label>
                                            </div>
                                            <div class="col-sm-6" >
                                                <select name="occupation" class="Occupation selectoption  otherOcc  form-group">
                                                    <option value="-Select-">-Select-</option>
                                                    <c:forEach items="${occupation}" var="occ">
                                                        <option value="${occ.mmc_occu_id}" 
                                                                <c:if test="${occ.mmc_occu_id eq person.occupation}">
                                                                    selected                                                                     
                                                                </c:if> >${occ.mmc_occu_name}</option>
                                                    </c:forEach>
                                                    <option value="0">Other</option>
                                                </select>
                                                <input type="text" name="Occupation" id="input-occupation" placeholder="Enter occupation " class="Occupation selectOcc input-field" />
                                                <span class="error" id="span-occupation"></span>
                                            </div>
                                            <!--                                                <div class="col-sm-6">
                                                                                            <label class="label_input">
                                                                                                Occupation 
                                                                                            </label>
                                                                                        </div>-->
                                            <!--                                            <div class="col-sm-6" >
                                                                                           <div class="custom-select">
                                              <select>
                                                <option value="0">Select car:</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                                <option value="6">Jaguar</option>
                                                <option value="7">Land Rover</option>
                                                <option value="8">Mercedes</option>
                                                <option value="9">Mini</option>
                                                <option value="10">Nissan</option>
                                                <option value="11">Toyota</option>
                                                <option value="12">Volvo</option>
                                              </select>
                                            </div>
                                                                                            <input type="text" name="Occupation" id="input-occupation" placeholder="Enter occupation " class="Occupation selectOcc input-field" />
                                                                                            <span class="error" id="span-occupation"></span>
                                                                                        </div>-->
                                            <!--                                            
                                                                                     
                                            <!--                                            <div class="col-sm-12">
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <label class="label_input">
                                                                                                        Other Occupation 
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <input type="text" name="occupation" value="${person.occupation}" id="occupation" placeholder="Enter occupation" class=" Occupation input-field"/>
                                                                                                    <span class="error" id="span-occupation"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>-->

                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you working with an Investment Adviser?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class=" working_with_adviser selectoption form-group avisor-click">
                                                    <c:if test = "${person.working_with_adviser eq ''}">
                                                        <option value="">${person.working_with_adviser}</option>
                                                    </c:if>
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row advisor-show">
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select Advisor Company
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor_company form-group" id="" >
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.companyName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Select an Adviser 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption advisor form-group" id="" >
                                                    <c:forEach items="${advisories}" var="advisor">
                                                        <option value="${advisor.login_id}">${advisor.advisorName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous2 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next3 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step4">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your contact details.  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Home address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="postal_code" id="postal_code" class=" form-control input-field" value=""/>
                                                <input type="text" id="address" class=" homeAddress form-control input-field"  placeholder="Enter home address" value="${person.homeAddress}"/>
                                                <span class="error" id="span-address"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    <div class="pulsating-circle4 circler" data-toggle="tooltip" title="Mobile number is mandatory while  the other number is optional." data-placement="right" style="vertical-align:text-bottom"></div>
                                                    Mobile number (required)
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="mobile_number first-mobile top-show">
                                                    <input type="text" class=" mobile_country_code mobile_country_code form-control codenumber"  id="countryCode3" value="${person.mobile_country_code}" name="countryCode" required="required" placeholder="Enter Country Code"  readonly="readonly">
                                                    <input type="tel" id="mobile" class=" mobile_number1 form-control error codename mobile_number1" value="${person.mobile_number}"  name="mobileNo" required="required" placeholder="Enter mobile number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                </div>
                                                <span class="error" id="span-mobile"></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="label_input"  style="text-align:left">
                                                    Other number (optional) 
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class=" optional_num_type selectoption form-group" id="" >
                                                    <c:if test = "${person.optional_num_type eq ''}">
                                                        <option value="${person.optional_num_type}">${person.optional_num_type}</option>
                                                    </c:if>
                                                    <option value="Home">Home</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="optional_num_code form-control codenumber"  id="countryCode" name="countryCode" required="required" value="${person.optional_num_code}" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class=" optional_num form-control error codename" id="mobileNo" name="mobileNo" value="${person.optional_num}" required="required" placeholder="Enter other number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                <span class="error" id="span-mobileNo"></span>
                                            </div>
                                            <!-- 	<div class="col-sm-6">
                                                            <label class="label_input">
                                                                    Work phone number (optional) 
                                                            </label>
                                                    </div>
                                                    <div class="col-sm-6 mobile_number first-mobile mobile-index2">
                                                            <input type="text" class="form-control codenumber"  id="countryCode2" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                            <input type="tel" class="form-control error codename" name="mobileNo" required="required" placeholder="Enter work number (optional)">
                                                            
                                                    </div> -->

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous3 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next4 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step5">
                                <div class="content-section">


                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Account security: mobile verification 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <small class="verification-otp1">
                                                        To verify your mobile number, please choose to receive a unique verification code via text message or phone call. 
                                                    </small>
                                                </div>
                                                <!-- <div class="col-sm-5">
                                                        <label class="label_input">
                                                        Mobile Number
                                                        </label>
                                                        </div>
                                                        <div class="col-sm-7 mobile_number">
                                                        <input type="text" class="form-control veri-detail"  id="countryCode5" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                        <input type="tel" class="form-control error" id="mobileNo" name="mobileNo" required="required" placeholder="Enter mobile number">
                                                        <span class="error" id="error-mobileNo"></span><span class="error" id="error-countryCode"></span>
                                                </div> -->
                                            </div>
                                            <!-- <div class="row" id="otp-block">
                                                    <div class="col-sm-5">
                                                    <label class="label_input">
                                                    Verification code
                                                    </label>
                                                    </div>
                                                    <div class="col-sm-7">
                                                    <input type="text" class="form-control error" id="otp" name="otp" required="required" placeholder="Enter one time passcode">
                                                    <span class="error" id="error-otp"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    </div>
                                            </div> -->
                                            <div class="row">
                                                <div class="col-sm-12 otp-icon">
                                                    <ul class="radio_button">
                                                        <li>
                                                            <input type="radio" class="senderType" id="f-option"  name="senderType" value="sms" style="width: auto;">
                                                            <img src="./resources/images/icon/message-icon.png" style="width: 20px; margin-left: 5px;">
                                                            <label for="f-option"></label>
                                                            <div class="check"></div>
                                                        </li>
                                                        <li>
                                                            <input type="radio" class="senderType " id="s-option" name="senderType" value="sms" style="width: auto;">
                                                            <i class="fas fa-phone"></i>
                                                            <label for="s-option"></label>
                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>
                                                    </ul>
                                                    <span class="error" id="error-generateOtp"></span>
                                                </div>
                                                <!--                                                <div class="col-sm-6">
                                                        <input type="button" name="next" id="generate-otp" class="action-button" value="Generate OTP" />
                                                </div>-->                                              


                                            </div>

                                            <div class="row otp-area">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Verification code 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="verification" placeholder="Enter unique code " id="verification" class="input-field otpkey"  onkeypress='return (event.charCode >= 48 && event.charCode <= 57)' maxlength="6"/>
                                                    <span class="spanverification" id="error-verification"></span>
                                                </div>
                                            </div>
                                            <!-- <div class="face" id="error-loader" style="display:none" >
                                                    <div class="hand" ></div>
                                            </div> -->
                                            <small class="verification-otp">  If you have not received your unique verification code within one minute, you can request another by   <a href="#">clicking here.</a>.</small>
                                        </div>
                                    </div>



                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous4 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next5 action-button otpNext" value="Continue" />
                            </fieldset>
                            <fieldset id="step6">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your identification details.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input detail-2">
                                                    Which type of ID are you providing 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class=" id_type selectoption form-group" id="src_of_fund1">
                                                    <c:if test = "${person.id_type eq ''}">
                                                        <option value="${person.id_type}">${person.id_type}</option>
                                                    </c:if>

                                                    <option value="1">NZ Driver Licence</option>
                                                    <option value="2">NZ Passport</option>
                                                    <option value="3">Other </option>

                                                </select>
                                            </div>
                                            <div class="row drivery-licence">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="licence_first_name" id="Licence_first_name" placeholder="Enter first name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error-Licence_first_name"></span>
                                                </div>                      
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="licence_last_name" id="Licence_last_name" placeholder="Enter last name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error-Licence_last_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Licence number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class=" License_number form-control input-field" id="Licence_number" name="license_number" value="${person.license_number}" required="required" placeholder="Enter licence number " onkeyup="myFunction()" />
                                                    <span class="error" id="error-Licence_number"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Issue date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="licence_issue_Date" id="Issue_date" placeholder="dd/mm/yyyy" class="licence_issue_Date input-field issuedate"  data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error_license_issue_date"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="licence_expiry_Date" id="Expiry_date" placeholder="dd/mm/yyyy" class="licence_expiry_Date input-field dob1" value="${person.licence_expiry_Date}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error_Expiry_date"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Version number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="licence_verson_number" placeholder="Enter version number" value="${person.licence_verson_number}" class=" licence_verson_number input-field" id="licence_Verson_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                    <span class="error" id="error_licence_Verson_number"></span>
                                                </div>

                                            </div>
                                            <div class="row passport-select">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="passport_first_name" id="passport_first_name" placeholder="Enter first name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="passport_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="passport_last_name" id="passport_last_name" placeholder="Enter last name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_last_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Passport number  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class=" passport_number form-control input-field" id="passport_number" name="passport_number" value="${person.passport_number}" required="required" placeholder="Enter passport number" onkeyup="myFunction()" />
                                                    <span class="error" id="error_passport_number" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Issue date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="passport_issue_Date" id="passport_issue_date" placeholder="dd/mm/yyyy" class="passport_issue_Date input-field issuedate"  data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error_passport_issue_date"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="passport_expiry" id="dob2" placeholder="dd/mm/yyyy" class="passport_expiry input-field dob1" value="${person.passport_expiry}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error_dob2"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="issue_by form-control countryname"  id="passport_issue_by" name="passport_issue_by" value="${person.passport_issue_by}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="error_passport_issue_by" ></span>
                                                </div>
                                            </div>
                                            <div class="row other-select">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Type of ID   
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class=" other_id_type form-control input-field" id="other_id_issueBy" name="other_id_type" value="${person.other_id_type}" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                                                    <span class="error" id="error_other_id_issueBy" ></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" id="other_first_name" name="other_first_name" placeholder="Enter first name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="other_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>                                                    
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" id="other_last_name" name="other_last_name" placeholder="Enter last name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_last_name"></span>
                                                </div>   

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="other_id_expiry" id="IdExpirydate" placeholder="dd/mm/yyyy" class="other_id_expiry input-field dob1" value="${person.other_id_expiry}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                                                    <span class="error" id="error_IdExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">                                               
                                                    <input type="text" class=" other_id_issueBy form-control countryname"  id="other_id_country" name="other_id_issueBy" value="${person.other_id_issueBy}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="error_other_id_country" ></span>
                                                </div>
                                                <div class="col-sm-129 closestcls">
                                                    <input type="file" name="myFile" id="other_id_myFile" class="attach-btn checkname" onchange="documentFileUpload(this);" accept="image/x-png,image/jpeg,image/*,.txt,.pdf">
                                                    <span class="shownamedata"></span></span><a href="javascript:void(0);" onclick="removedatadiv(this);"><i class=" removefile far fa-times-circle"></i></a>
                                                    <span class="error" id="error_other_id_myFile"></span>
                                                </div>

                                            </div>
                                            <input type="hidden" name="more_investor_verify" id="investor_verify" value="false">
                                            <input type="hidden" name="more_investor_verify" id="investorPep_verify" value="false">
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous5 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next6 action-button verify-dl" value="Continue" />
                            </fieldset>
                            <fieldset id="step7">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your tax information.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input input-size-text ">
                                                    Prescribed Investors Rate (PIR)
                                                </label>
                                            </div>
                                            <div class="col-sm-3 details-pos">
                                                <select class="selectoption  PIR form-group aml-select redy-select" id="Investors_Rate">
                                                    <option value="28%">28%</option>
                                                    <option value="17.5%">17.5%</option>
                                                    <option value="10.5%">10.5%</option>
                                                </select>

                                            </div>
                                            <div class="col-sm-3">
                                                <a href="javascript:void(0)" id="pir" class="aml-link">What’s my PIR?</a>
                                                <span class="error" id="error_Investors_Rate"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD Number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 validate-ird">
                                                <input type="text" class=" IRD_Number  form-control input-field" name="fullName" id="IRD_Number" value="${person.ird_number}" required="required" placeholder="xxx-xxx-xxx " onkeypress="checkird(this, event)" onchange="checkird(this, event)"/>
                                                <a class="ird-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                <span class="error" id="error_IRD_Number"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    What is the source of funds or wealth for this account? 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption" id="wealth_src">
                                                    <c:if test = "${not empty person.wealth_src}">
                                                        <option value="${person.wealth_src}">${person.wealth_src}</option>
                                                    </c:if>
                                                    <option value="0">–Select–</option>
                                                    <option value="1"> Property sale </option>
                                                    <option value="2"> Personal</option>
                                                    <option value="3"> Employment</option>
                                                    <option value="4"> Financial investment</option>
                                                    <option value="5"> Business sale</option>
                                                    <option value="6"> Inheritance/gift</option>
                                                    <option value="7"> Other </option>
                                                </select>
                                                <span class="error" id="error_wealth_src"></span>
                                            </div>
                                            <div class="col-sm-6 des-togle">
                                                <label class="label_input" style="text-align:left">
                                                    Description 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 des-togle">
                                                <input type="text" class="" id="" name="other_description" placeholder="Enter description"/>
                                                <span class="error" id=""></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you a US citizen or US tax resident, or a tax resident in any other country?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption1 form-group">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                            <div class="row yes-option">
                                                <div class="row yes-new checktindata">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">Please enter all of the countries (excluding NZ) of which you are a tax resident. </h5>
                                                    </div>
                                                    <c:if test = "${empty person.countryTINList}">
                                                        <div class="col-sm-6">
                                                            <label class="label_input" style="text-align:left">
                                                                Country of tax residence:
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6 details-pos flag-drop">
                                                            <input type="text" class=" tex_residence_Country form-control countrynameoutnz"  id="countryname2" name="countryCode" value="${taxResident.country}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                            <span class="error countrynameoutnz-error" ></span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label class="label_input">
                                                                Tax Identification Number (TIN)
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class=" TIN form-control input-field" name="fullName" value="${taxResident.tin}" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                            <span class="error TIN-error" ></span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label class="label_input">
                                                                Reason if TIN not available 
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class=" resn_tin_unavailable form-control input-field" name="fullName" value="${taxResident.reason}" required="required" placeholder=""/>
                                                            <span class="error resn_tin_unavailable-error" ></span>
                                                        </div>
                                                    </c:if>
                                                    <c:forEach items="${person.countryTINList}" var="taxResident">
                                                        <div class="col-sm-6">
                                                            <label class="label_input" style="text-align:left">
                                                                Country of tax residence:
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6 details-pos flag-drop">
                                                            <input type="text" class=" tex_residence_Country form-control countrynameoutnz"  id="countryname2" name="countryCode" value="${taxResident.country}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                                            <span class="error countrynameoutnz-error" ></span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label class="label_input">
                                                                Tax Identification Number (TIN) 
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class=" TIN form-control input-field" name="fullName" value="${taxResident.tin}" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                                            <span class="error TIN-error" ></span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label class="label_input">
                                                                Reason if TIN not available 
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class=" resn_tin_unavailable form-control input-field" name="fullName" value="${taxResident.reason}" required="required" placeholder=""/>
                                                            <span class="error resn_tin_unavailable-error" ></span>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                                <div class="col-sm-129 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another">Add another country</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous6 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next7 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step8">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your bank account details (for distributions and/or withdrawals). 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Bank name 
                                                </label>
                                            </div>
                                            <!--                                            <div class="col-sm-6 details-pos">
                                                                                            <select class="selectoption  bank_name form-group aml-select" id="bank_name" value="">
                                            <c:if test = "${not empty person.bank_name}">
                                                <option value="${person.bank_name}">${person.bank_name}</option>
                                            </c:if>
                                            <option value="No Bank Selected">–Select–</option>
                                            <option value="Bank of New Zealand" data-id="02">Bank of New Zealand</option>
                                            <option value="ANZ Bank New Zealand" data-id="01">ANZ Bank New Zealand</option>
                                            <option value="ASB Bank" data-id="12">ASB Bank</option>
                                            <option value="Westpac" data-id="03">Westpac</option>
                                            <option value="Heartland Bank" data-id="03">Heartland Bank</option>
                                            <option value="Kiwibank" data-id="38">Kiwibank</option>
                                            <option value="SBS Bank" data-id="03">SBS Bank</option>
                                            <option value="TSB Bank" data-id="15">TSB Bank</option>
                                            <option value="The Co-operative Bank" data-id="02">The Co-operative Bank</option>
                                            <option value="NZCU" data-id="03">NZCU</option>
                                            <option value="Rabobank New Zealand" data-id="03">Rabobank New Zealand</option>
                                            <option value="National Bank of New Zealand" data-id="03">National Bank of New Zealand</option>
                                            <option value="National Australia Bank" data-id="08">National Australia Bank</option>
                                            <option value="Industrial and Commercial Bank of China" data-id="10">Industrial and Commercial Bank of China</option>
                                            <option value="PostBank" data-id="11">PostBank</option>
                                            <option value="Trust Bank Southland" data-id="13">Trust Bank Southland</option>
                                            <option value="Trust Bank Otago" data-id="14">Trust Bank Otago</option>
                                            <option value="Trust Bank Canterbury" data-id="16">Trust Bank Canterbury</option>
                                            <option value="Trust Bank Waikato" data-id="17">Trust Bank Waikato</option>
                                            <option value="Trust Bank Bay of Plenty" data-id="18">Trust Bank Bay of Plenty</option>
                                            <option value="Trust Bank South Canterbury" data-id="19">Trust Bank South Canterbury</option>
                                            <option value="Trust Bank Auckland" data-id="21">Trust Bank Auckland</option>
                                            <option value="Trust Bank Central" data-id="20">Trust Bank Central</option>
                                            <option value="Trust Bank Wanganui" data-id="22">Trust Bank Wanganui</option>
                                            <option value="Westland Bank" data-id="24">Westland Bank</option>
                                            <option value="Trust Bank Wellington" data-id="23">Trust Bank Wellington</option>
                                            <option value="Countrywide" data-id="25">Countrywide</option>
                                            <option value="United Bank" data-id="29">United Bank</option>
                                            <option value="HSBC" data-id="30">HSBC</option>
                                            <option value="Citibank" data-id="31">Citibank</option>
                                            <option value="Other" class="otherBank" data-id="00">Other</option>
                                        </select>
                                        <span class="error" id="error_bank_name"></span>
                                    </div>-->
                                            <div class="col-sm-6 bank-margin1 mb-2">

                                                <div class="mm-dropdown signup-flag">
                                                    <c:choose>
                                                        <c:when test="${not empty person.bank_name}">
                                                            <div class="textfirst">${person.bank_name}<img class="drop-img" style="width:10px" src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" /></div>
                                                            </c:when >
                                                            <c:otherwise>
                                                            <div class="textfirst">-Select-<img class="drop-img" style="width:10px" src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" /></div>

                                                        </c:otherwise>
                                                    </c:choose>
                                                    <ul class="select">
                                                        <c:forEach items="${allBankCode}" var="bankCode" varStatus="idx">

                                                            <li class="input-option" data-id="${bankCode.bankCode}" data-value="${bankCode.bankName}"><img src="${bankCode.bankLogo}" />${bankCode.bankName}</li>


                                                        </c:forEach>
                                                        <li class="input-option"  data-id= "" data-value="Other">
                                                            Other
                                                        </li>
                                                    </ul>
                                                    <span class="error" id="error_bank_name"></span> 
                                                    <input type="hidden" class="option" name="namesubmit" value="" />
                                                    <input type="hidden" class="bank_name" name="bank_name1" id="bank_name" value="" />

                                                </div>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <label class="label_input" style="text-align:left">
                                                    Other Bank name
                                                </label>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <input type="text" class="Other_Bank_name form-control input-field" name="other_bank_name" id="other_other_name" value="" required="required" placeholder="Bank name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                                                <span class="error" id="error_Other_Bank_name"></span>
                                            </div>
                                            <div class="col-sm-6" >
                                                <label class="label_input" style="text-align:left">
                                                    Account name
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="acount_holder_name form-control input-field" name="acount_holder_name" id="acount_holder_name" value="${person.acount_holder_name}" required="required" placeholder="Enter your account name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                                                <span class="error" id="error_acount_holder_name"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account number
                                                </label>
                                            </div>
                                            <div class="col-sm-6  validate-ird">
                                                <!--<input type="hidden" class="form-control input-field account_number" value="" id="demo" name="accountNumber" required="required" placeholder="BB-bbbb-AAAAAAA-SSS" />-->
                                                <input type="text" class="form-control input-field account_number" value="${joint.account_number}" id="accountNumber" onkeypress="myFunction7()" name="accountNumber" required="required"  placeholder="xx-xxxx-xxxxxxx-xxx" />
                                                <a class="account-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                <input type="hidden" class="isAccount_number_verified " />
                                                <span class="error" id="error_acount_holder_number" ></span>
                                            </div>
                                            <!-- <div class="col-sm-129">
                                                    <input type="file" name="myFile" class="attach-btn2">
                                            </div> -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="new-attach-info">
                                                    <div class="col-sm-129 closestcls">
                                                        <input type="file" name="myFile" id="bank_document" class="attach-btn2 checkname" onchange="documentFileUpload(this);" accept="image/*,.pdf">
                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatadiv(this);"><i class=" removefile far fa-times-circle"></i></a>
                                                        <span class="error" id="error_bank_document" ></span>
                                                    </div>
                                                    <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking"><img src="./resources/images/i-icon.png"></a></span>-->
                                                    <div class="pulsating-circle6" data-toggle="tooltip" title="To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking." data-placement="right" style="vertical-align:text-bottom"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous7 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next8 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step9">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please read and accept the below Terms and Conditions to continue.  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="text-center">Agreement of Terms</h4>
                                                    <div class="terms_cond" style="overflow:auto;height: 200px;word-break: break-word;">
                                                        <ol>
                                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                                <ol>
                                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions. Capitalised words are defined in clause 22.</li>
                                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition, tolerance to risk and trading experience.
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>SERVICES PROVIDED</b>
                                                                <ol>
                                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services") in relation to digital currencies (such as, but not limited to, Bitcoin and Ethereum) selected by us from time to time (?Digital Currency?):
                                                                        <ol>
                                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                                            <li>Chat and user support services.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We do not deal in digital currencies or tokens which are or may be Financial Products under New Zealand law on the
                                                                        Platform. For this reason, we are not required to hold any licence or authorization in relation to portfolio management of
                                                                        the Digital Currency we do support on the Platform. If we become aware that any Digital Currency we have supported on
                                                                        the Platform is or may be (in our sole discretion) a Financial Product, you authorize and instruct us to take immediate steps
                                                                        to divest any such holdings at the price applying at the time of disposal.</li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>USER PROFILE</b>
                                                                <ol>
                                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                                        <ol>
                                                                            <li>be 18 years of age or older;</li>
                                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                                </ol>
                                                            </li><li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                                <ol>
                                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                                    <li>For lump sum investments, you may give instructionstoexecuteaTransactionviaour Platform usingyour User Profile.</li>
                                                                    <li>For regular monthly investments, you may select a monthly contribution amount when you create your User Profile.
                                                                        This amount may be amended from time to time via our Platform.</li>
                                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy
                                                                        for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the
                                                                        maximum number of units (including fractions of units, if available) of the relevant investments in accordance with
                                                                        your selected Portfolio. Any remaining money after a Transaction has been executed will be held in your Wallet. You
                                                                        acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.
                                                                        You appoint us, and we accept the appointment, to act as your agent for the execution of any Transaction, in
                                                                        accordance with these Terms and Conditions.</li>
                                                                    <li>We will use reasonable endeavors to execute Transactions within 24 hours of receiving allocated funds into your
                                                                        Wallet (subject to these Terms and Conditions, including clause 4.10). The actual price of a particular investment is
                                                                        set by the issuer, provider or the market (as applicable) at the time the order is executed by us. We may set a
                                                                        minimum transaction amount at any time. We may choose not to process any orders below the minimum transaction
                                                                        amount at our discretion. </li>
                                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                                    <li>After a Transaction has been executed, we reserve the right to void any Transaction which we consider contains any
                                                                        manifest error or is contrary to any law or these Terms and Conditions. In the absence of our fraud or willful default,
                                                                        we will not be liable to you for any loss, cost, claim, demand or expense following any such action on our part or
                                                                        otherwise in relation to the manifest error or Transaction.  </li>
                                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                                    <li>Any Transaction via the Platform is deemed to take place in New Zealand and you are deemed to take possession
                                                                        of your Portfolio in New Zealand
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>PORTFOLIO REBALANCING</b>
                                                                <ol>
                                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                                <ol>
                                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                                    <li>We will notify you of any changes to the Target Asset Allocation promptly following these changes taking effect.
                                                                        Notification of any such changes to the Target Asset Allocation are made via the Platform by way of an update to
                                                                        the Target Asset Allocation details provided for each Portfolio. </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                                <ol>
                                                                    <li><b>Digital Currency Transactions:</b> Invsta processes supported Digital Currency according to the instructions received
                                                                        from its users and we do not guarantee the identity of any party to a Transaction. It is your responsibility to verify all
                                                                        transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a
                                                                        Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the
                                                                        Transaction by the Digital Currency network. A Transaction is not complete while it is in a pending state. Funds
                                                                        associated with Transactions that are in a pending state will be designated accordingly, and will not be included in
                                                                        your Invsta Account balance or be available to conduct Transactions. We may charge network fees (miner fees) to
                                                                        process a Transaction on your behalf. We will calculate the network fee in our sole discretion by reference to the 
                                                                        cost to us. </li>
                                                                    <li><b>Digital Currency Storage &amp; Transmission Delays:</b> Invsta securely stores all Digital Currency private keys in our control
                                                                        in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information
                                                                        from offline storage in order to facilitate a Transaction in accordance with your instructions, which may delay the
                                                                        initiation or crediting of a Transaction for 48 hours or more. You accept that a Digital Currency Transaction facilitated
                                                                        by Invsta may be delayed.</li>
                                                                    <li><b>Operation of Digital Currency Protocols.</b> Invsta does not own or control the underlying software protocols which
                                                                        govern the operation of Digital Currencies supported on our platform. By using the Invsta platform, you acknowledge
                                                                        and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no
                                                                        guarantee of their functionality, security, or availability; and (ii) that the underlying protocols may be subject to
                                                                        sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function,
                                                                        and/or essential nature of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that
                                                                        Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its
                                                                        sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely.
                                                                        You acknowledge and agree that Invsta assumes absolutely no responsibility in respect of an unsupported branch
                                                                        of a forked Digital Currency and/or protocol.</li>
                                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                                    <li>You agree that Digital Curreny investments and any other assets held in your Wallet <b>("Client Property")</b> may be
                                                                        pooled with the investments and other assets of other clients and therefore your holdings may not be individually
                                                                        identifiable within the Wallet. </li>
                                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>("Client Money")</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                                    <li>Money received from you or a third party for your benefit, which includes your Client Money held pending
                                                                        investment, payment to you as the proceeds and income from selling investments, may attract interest from the
                                                                        bank at which it is deposited. You consent to such interest being deducted from that bank account and being
                                                                        retained by us as part of the fees for using the Services.
                                                                    </li>
                                                                    <li>We will keep detailed records of all your Client Property and Client Money in your Wallet at all times. </li>
                                                                    <li>Client Property and Client Money received or purchased by us on your behalf will be held on bare trust as your
                                                                        nominee in the name or to the order of Invsta or any other approved third party custodian to our order. </li>
                                                                    <li>You confirm that you are the beneficial owner of all of the Client Property and Client Money in your account, or you
                                                                        are the sole trustee on behalf of the beneficial owner, and that they are and will remain free from any encumbrance.</li>
                                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?.
                                                                        Withdrawals will be processed on a best endeavors basis as outlined in clause 4.6. Withdrawals of Client Money will
                                                                        be processed and paid into a bank account nominated by you in the same name of your User Profile upon
                                                                        settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn
                                                                        from your Wallet. </li>
                                                                    <li>It is up to you to understand whether and to what extent, any taxes apply to any Transactions through the Services
                                                                        or the Platform, or in relation to your Portfolio. We accept no responsibility for, nor make any representation in respect
                                                                        of, your tax liability.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>OUR WEBSITE POLICY</b>
                                                                <ol>
                                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>RISK WARNINGS</b>
                                                                <ol>
                                                                    <li>You acknowledge and agree that:
                                                                        <ol>
                                                                            <li>investing in Digital Currency is unlike investing in traditional currencies, goods or commodities: it involves
                                                                                significant and exceptional risks and the losses can be substantial. You should carefully consider and assess
                                                                                whether trading or holding of digital currency is suitable for you depending upon your financial condition,
                                                                                tolerance to risk and trading experience and consider whether you should take independent financial
                                                                                advice; </li>
                                                                            <li>we have not advised you to, or recommended that you should, use the Platform and/or Services, or trade
                                                                                and/or hold Digital Currency;</li>
                                                                            <li>unlike other traditional forms of currency, Digital Currency is decentralised and is not backed by a central
                                                                                bank, government or legal entities. As such, the value of Digital Currency can be extremely volatile and may
                                                                                swing depending upon the market, confidence of investors, competing currencies, regulatory
                                                                                announcements or changes, technical problems or any other factors. We give no warranties or
                                                                                representations as to the future value of any Digital Currency and accept no liability for any change in value
                                                                                of your Portfolio; and
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You acknowledge that we do not issue or deal in Financial Products or provide any financial advice and no offer or
                                                                        other disclosure document has been, or will be, prepared in relation to the Services, the Platform and/or any of the
                                                                        Digital Currencies, under the Financial Markets Conduct Act 2013, the Financial Advisers Act 2008 or any other similar
                                                                        legislation in any jurisdiction.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DISPUTE RESOLUTION</b>
                                                                <ol>
                                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:<br>
                                                                        <b>Financial Services Complaints Limited (FSCL)</b><br>
                                                                        PO Box 5967, Lambton Quay<br>
                                                                        Wellington, 6145<br> 
                                                                        Email: info@fscl.org.nz<br>
                                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You authorize us to:
                                                                        <ol>
                                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                                            <li>Record all telephone conversations with you;</li>
                                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                                        <ol>
                                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                                        <ol>
                                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                                        <ol>
                                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                                <ol>
                                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                                    <li>
                                                                        You accept that there may be circumstances where we are required to suspend or disable your User Profile to meet
                                                                        our anti-money laundering obligations.
                                                                    </li>
                                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>LIMITATION OF LIABILITY</b>
                                                                <ol>
                                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                                    <li>You acknowledge that:
                                                                        <ol>
                                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                                <ol>
                                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest in Crypto Currency Portfolios.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                                    <li>The provisions of this clause 14 will extend to all our employees, agents and contractors, and to all corporate entities
                                                                        in which we may have an interest and to all entities which may distribute our publications.</li>
                                                                    <li>Despite anything else in these Terms and Conditions, if we are found to be liable for any loss, cost, damage or
                                                                        expense arising out of or in connection with your use of the Platform or the Services or these Terms and Conditions,
                                                                        our maximum aggregate liability to you will be limited to two times the total amount of fees and charges that you
                                                                        have paid to us in the previous twelve months in accordance with clause 15 below.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>FEES AND CHARGES FOR SERVICES</b>
                                                                <ol>
                                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges ("Fees"):
                                                                        <ol>
                                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                                            <table class="table-bordered">
                                                                                <tbody><tr>
                                                                                        <td>Portfolio Management Fee</td>
                                                                                        <td>Description</td>
                                                                                        <td>Other Expenses</td>
                                                                                        <td>Performance Fee</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Up to 3% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                                        <td>Based on total investment value, charged monthly</td>
                                                                                        <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                                        <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You must also pay for any other fees and charges for any add on services you obtain via the Platform or as specified
                                                                        in these Terms and Conditions.</li>
                                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                                </ol>
                                                            </li>

                                                            <li>
                                                                <b>TERMINATION OF YOUR USER PROFILE</b>
                                                                <ol>
                                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to) where:
                                                                        <ol>
                                                                            <li>you are insolvent or in liquidation or bankruptcy; or</li>
                                                                            <li>you have not paid Fees or other amounts due under these Terms and Conditions by the due date</li>
                                                                            <li>you gain or attempt to gain unauthorised access to the Platform or another member?s User Profile or Wallet;</li>
                                                                            <li>we consider any conduct by you (whether or not that conduct is related to the Platform or the Services) puts
                                                                                the Platform, the Services or other users at risk; </li>
                                                                            <li>you use or attempt to use the Platform in order to perform illegal or criminal activities;</li>
                                                                            <li>your use of the Platform is subject to any pending investigation, litigation or government proceeding;
                                                                            </li>
                                                                            <li>you fail to pay or fraudulently pay for any transactions;</li>
                                                                            <li>you breach these Terms and Conditions and, where capable of remedy, fail to remedy such breach within 30
                                                                                days? written notice from us specifying the breach and requiring it to be remedied;</li>
                                                                            <li>your conduct may, in our reasonable opinion, bring us into disrepute or adversely affect our reputation or
                                                                                image; and/or</li>
                                                                            <li>we receive a valid request from a law enforcement or government agency to do so.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may also terminate these Terms and cease to provide the Services and the Platform if we undergo an insolvency
                                                                        event, meaning that where that party becomes unable to pay its debts as they fall due, or a statutory demand is
                                                                        served, a liquidator, receiver or manager (or any similar person) is appointed, or any insolvency procedure under
                                                                        the Companies Act 1993 is instituted or occurs. </li>
                                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the
                                                                        proceeds of sale (less any applicable Fees) to a bank account nominated by you. </li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>ASSIGNMENT</b>
                                                                <ol>
                                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>INDEMNITY</b>
                                                                <ol>
                                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                                        <ol>
                                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                                            <li>which they may incur or suffer as a result of:
                                                                                <ol>
                                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>AMENDMENTS</b>
                                                                <ol>
                                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                                        <ol>
                                                                            <li>Notice on our website; or</li>
                                                                            <li>Direct communication with you via email,</li>
                                                                        </ol>
                                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                                    </li>
                                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>NOTICES</b>
                                                                <ol>
                                                                    <li>Any notice or other communication ("Notice") given for the purposes of these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Must be in writing; and</li>
                                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                                        <ol>
                                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>                
                                                            </li>

                                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                                <ol>
                                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DEFINITIONS</b><br>
                                                                <b>"Account"</b> means the Client Property and Client Money we hold for you and represents an entry in your name on the
                                                                general ledger of ownership of Digital Currency and money maintained and held by<br> 

                                                                <!-- <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br> -->

                                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.<br>

                                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.<br>
                                                                <b>"Client Money"</b> has the meaning given to it in clause 7.6.<br>
                                                                <b>"Client Property"</b> has the meaning given to it in clause 7.5.<br>
                                                                <b>"Digital Currency"</b> means the supported tokens or cryptocurrencies offered on the Platform.<br>

                                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.<br> 

                                                                <!-- <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br> -->

                                                                <!-- <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br>  -->
                                                                <!-- <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br>  -->
                                                                <b>"Minor"</b> means a person under the age of 18.<br>
                                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website <br>

                                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. <br>
                                                                <b>"Portfolio Manager"</b> means Invsta and associated people responsible for managing your Portfolio.<br>

                                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services <br>

                                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.<br>

                                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.<br>

                                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.<br>

                                                            </li>

                                                            <li><b>GENERAL INTERPRETATION</b>
                                                                <ol>
                                                                    <li>In these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Unless the context otherwise requires, references to:
                                                                                <ol>
                                                                                    <li>"we", "us", Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                                </ol>
                                                                            </li>
                                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                                </ol>
                                                            </li>

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:20px">
                                                <div class="col-sm-12" style="text-align: center">
                                                    <!--                                                    <a href="./resources/images/pdf/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                                                                           text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>-->
                                                    <a href="javascript:void(0)" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                       text-decoration: underline;"> Download Client Terms and Conditions</a>
                                                </div>
                                                <div class="col-sm-12 condition-details">
                                                    <!--                                                    <input class="checkbox-check" id="term_condition" type="checkbox" name="term_condition">
                                                                                                        <label class="label_input" style="text-align:left">
                                                                                                            I have read and agree to the Terms and Conditions
                                                                                                        </label>-->
                                                    <label class="last-check">I have read and agree to the Terms and Conditions
                                                        <input type="checkbox" class="checkbox-check" id="term_condition" name="term_condition">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <span class="error" id="error_term_condition" ></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous8 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next9 action-button" value="Accept" />
                            </fieldset>
                            <fieldset id="step10">
                                <div class="content-section">
                                    <div class="row">
                                        <div class="col-md-12 thanku">

                                            <div class="element-wrapper">
                                                <h5 class="element-header">
                                                    Thank for submitting your application details, we'll let you know if we require anything further.
                                                </h5>
                                            </div>
                                            <h5 class="element-header3">
                                                To continue to your account and make an investment, please verify your email address by clicking on the link in the email we have sent.
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>

                                <div class="col-md-12" style="max-width: 113px;margin: auto;">
                                    <div class="ok-submit">
                                        <input type="button" name="Submit" id="submit" class=" action-button-previous new-ok-btn " value="OK" />
                                    </div>
                                </div>

                                <!-- <input type="button" name="next" class="next12 action-button" value="Accept" /> -->
                            </fieldset>
                            <fieldset id="step11">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter your email address and create a password.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Email address
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="email" class="email input-field" id="email" name="email" value="${person.email}" required="required" placeholder="Enter email" onblur="isEmailAlreadyExist();">

                                            </div>
                                            <div class="col-sm-12 validation-error">
                                                <span class="error" id="error-email"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Create your password
                                                </label>
                                            </div>
                                            <div class="col-sm-6 password">
                                                <input type="text" class="password input-field pwd" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and one specail character, and at least 8 or more characters" required="required" placeholder="Enter password"  >
                                                <span toggle="#password" class="fa fa-fw field-icon toggle-password fa-eye-slash" title="Show password"></span>
                                                <span class="error-grey" id="error-password"></span>
                                            </div>

                                        </div>
                                    </div>
                                    <span class="focus-border" style="display:block;" id="validationId" ></span>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous11 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next1 action-button"  value="Continue"/>
                            </fieldset>
                        </form>
                        <form id="typeform" action="./signUpPath" method="post">
                            <input type="hidden" name="type"/>
                            <input type="hidden" name="email"/>
                            <input type="hidden" name="password"/>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.MultiStep Form -->
        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
        <script src="./resources/js/index.js"></script>        
        <script src="./resources/js/intlTelInput_1.js"></script>
        <script src="./resources/js/intlTelInput_2.js"></script>
        <script src="./resources/js/intlTelInput_3.js"></script>
        <script src="./resources/js/user-main.js"></script>
        <script type="text/javascript" src="./resources/js/countries.js"></script>
        <script src="./resources/js/sweetalert.min.js"></script>
        <script src="./resources/js/bankValidator/bankValidator.js"></script>
        <script>
                                                    var bankFile = '';
                                                    var otherFile = '';
                                                    function documentFileUpload(selectedfile) {
//                                                        var selectedfile = document.getElementById("bank_document").files;

//                                                        $(".loadingcircle").show();
                                                        var id = selectedfile.id;
                                                        var value = selectedfile.value;
                                                        id = id.trim();
                                                        selectedfile = selectedfile.files;
                                                        if (selectedfile.length > 0) {
                                                            var imageFile = selectedfile[0];
                                                            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
                                                            if (allowedExtensions.exec(value)) {
                                                                var size = selectedfile[0].size;
                                                                if (size < 2008576) {
                                                                    var fileReader = new FileReader();
                                                                    fileReader.onload = function (fileLoadedEvent) {
                                                                        if (id === "bank_document") {
                                                                            bankFile = fileLoadedEvent.target.result;
                                                                            console.log("bankFileSRC" + bankFile);
//                                                                            $(".loadingcircle").hide();
                                                                        }
                                                                        if (id === "other_id_myFile") {
                                                                            otherFile = fileLoadedEvent.target.result;
                                                                            console.log("otherFileSRC" + otherFile);
//                                                                            $(".loadingcircle").hide();
                                                                        }

                                                                    };
                                                                    fileReader.readAsDataURL(imageFile);
                                                                }
                                                            }

                                                        }
                                                    }
                                                    $('.pwd').keyup(function () {
                                                        var password = $("#password").val();
                                                        var passwordExpression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*[\s]).{8,16}$/;
                                                        if (password === "" || passwordExpression.test(password)) {
                                                            $("#error-password").text('');
                                                        } else {
                                                            var passwordTitle = "Password must contain at least one number, one uppercase and lowercase letter, one special character  and be between 8-16 characters";
                                                            $("#error-password").html(passwordTitle);
                                                        }
                                                    });




                                                    $('.account_number').keyup(function () {
                                                        var accountnumber = $('.account_number').val();
                                                        $('.account-check').hide();
                                                        $('.isAccount_number_verified').val('false');
//                                                        alert(accountnumber);
                                                        if (accountnumber.length === 19) {

                                                            var AccountArray = accountnumber.split('-');
//                                                            alert(AccountArray);
                                                            var bk = AccountArray[0];
                                                            var brch = AccountArray[1];
                                                            var acct = AccountArray[2];
                                                            var suf = AccountArray[3];
//                                                            alert(bk + "  --" + brch + "  " + acct + "   " + suf);
                                                            var resl = isValidNZBankNumber(bk, brch, acct, suf);
//                                                            alert(resl);
                                                            if (resl) {
                                                                $('.account-check').show();
                                                                $('.isAccount_number_verified').val('true');

                                                            } else {
                                                                $('.isAccount_number_verified').val('false');
//                                                                $('.account-check').hide();
                                                                $('#error_acount_holder_number').text('account number is invalid');

                                                            }
                                                        }

                                                    });






        </script>
        <script>
            $(document).ready(function () {

//                $(".otpNext").attr("disabled", true);
                $(".account-check").hide();
                $(".ird-check").hide();
                $(".removefile").hide();
                $(".selectOcc").hide();
                $('.save-new-btn').hide();
                var step = '${person.step}';
                if (step === '2') {
                    $("#step1").hide();
                    $("#step11").show();
                    $('.save-new-btn').show();
                }
                if (step === '3') {
                    $("#step1").hide();
                    $("#step2").show();
                    $('.save-new-btn').show();
                }
                if (step === '4') {
                    $("#step1").hide();
                    $("#step3").show();
                    $('.save-new-btn').show();
                }
                if (step === '5') {
                    $("#step1").hide();
                    $("#step4").show();
                    $('.save-new-btn').show();
                }
                if (step === '6') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step5").show();
                }
                if (step === '7') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step6").show();
                }
                if (step === '8') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step7").show();
                }
                if (step === '9') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step8").show();
                }
                if (step === '10') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step9").show();
                }
                if (step === '11') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step10").show();
                }
                if (step === '12') {
                    $('.save-new-btn').show();
                    $("#step1").hide();
                    $("#step11").show();
                }
            });
            $(".Occupation").click(function () {
                var Occupation = $('.Occupation').val();
                if (Occupation === "0") {
                    $(".selectOcc").show();
                } else {
                    $(".selectOcc").hide();
                }

            });
        </script>       
        <script>
            $(document).ready(function () {
                console.log('${person}');
                $('.yes-option').hide();
            <c:if test="${not empty person.countryTINList}">
                //                 $(".selectoption1 option:selected").text("yes");
                $(".selectoption1").val('2');
                $('.yes-option').show();
            </c:if>
                $(".toggal_other").hide();
                $(window).keydown(function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        return false;
                    }
                });
            });

            function initAutocomplete() {
                var input = document.getElementById('address');
                var options = {
                    types: ['address'],
                    componentRestrictions: {
                        country: 'nz'
                    }
                };
                autocomplete = new google.maps.places.Autocomplete(input, options);
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    console.log(JSON.stringify(place));
                    for (var i = 0; i < place.address_components.length; i++) {
                        for (var j = 0; j < place.address_components[i].types.length; j++) {
                            if (place.address_components[i].types[j] === "postal_code") {
                                document.getElementById('postal_code').value = place.address_components[i].long_name;

                            }
                        }
                    }
                });
            }
//            google.maps.event.addDomListener(window, "load", initAutocomplete);

            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            adultDOB = date.setFullYear(year - 18, month, day);
            //            alert(adultDOB);
            //            var arr1 = adultDOB1.split("-");
            //            var year1 = parseInt(arr1[0]);
            //            var month1 = parseInt(arr1[1]);
            //            var day1 = parseInt(arr1[2]);
            $("#countryCode").intlTelInput_1();
            $("#countryCode2").intlTelInput_1();
            $("#countryCode3").intlTelInput_1();
            $("#countryCode4").intlTelInput_1();
            $("#countryCode5").intlTelInput_1();
            $(".countryname").intlTelInput_2();
            $(".countrynameoutnz").intlTelInput_3();
            $('#otp-block').hide();
            $(document).ready(function () {
                $('.passport-select').hide();
                $('.other-select').hide();
                $('#mobileNo').keyup(function () {
                    $('#error-generateOtp').text('');
                });
                $('#otp').keyup(function () {
                    $('#error-generateOtp').text('');
                });
                $('#fullName').keyup(function () {
                    $('.first_name').val('');
                    $('.middle_name').val('');
                    $('.last_name').val('');
                    var fn = $('#fullName').val();
                    fn = fn.replace(/(^[\s]*|[\s]*$)/g, '');
                    fn = fn.replace(/\s+/g, ' ');
                    var fnarr = fn.split(" ");
                    $('.first_name').val(fnarr[0]);
                    if (fnarr.length >= 3) {
                        $('.last_name').val(fnarr[fnarr.length - 1]);
                        fnarr.shift();
                        fnarr.pop();
                        fnarr = fnarr.toString().replace(/,/g, ' ');
                        $('.middle_name').val(fnarr);
                    } else if (fnarr.length === 3) {
                        $('.middle_name').val(fnarr[1]);
                        $('.last_name').val(fnarr[2]);
                    } else if (fnarr.length === 2) {
                        $('.last_name').val(fnarr[1]);
                    }
                });
                $("#dob").datepicker({
                    yearRange: (year - 80) + ':' + year,
                    changeMonth: true,
                    changeYear: true,
                    'maxDate': new Date(adultDOB),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $(".dob1").datepicker({
                    yearRange: (year) + ':' + (year + 80),
                    changeMonth: true,
                    changeYear: true,
                    'minDate': new Date(),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
                $(".issuedate").datepicker({
                    yearRange: (year - 20) + ':' + (year),
                    changeMonth: true,
                    changeYear: true,
                    'minDate': new Date(adultDOB),
                    'maxDate': new Date(),
                    dateFormat: 'dd/mm/yy'
                }).datepicker().attr('readonly', 'readonly');
//                $('input:radio[name="senderType"]').change(function () {
//                    var mNo = $('.mobile_number1').val();
//                    var cCo = $('.mobile_country_code').val();
//                    var sTy = $('.senderType:checked').val();
//                    cCo = cCo.replace(/\+/g, "");
//
//                    genrateotp(mNo, cCo, sTy);
//
//                    $('#otp-block').show();
//                    document.getElementById('error-loader').style.display = 'none';
//                    document.getElementById('error-generateOtp').innerHTML = "Please check your messages";
//                    $('input:radio[name="senderType"]').prop("checked", false);
//                });

                $('.otpkey').keyup(function () {
                    $('.spanverification').html("");
                    if ($('.otpkey').val().length === 6) {
//                                                                
//                                                                side");
                        var mNo = $('.mobile_number1').val();
                        var cCo = $('.mobile_country_code').val();
                        var otp = $('.otpkey').val();
                        cCo = cCo.replace(/\+/g, "");
                        validateOTP(mNo, cCo, otp);
                    }
                });

                $('#fourthfs-continue').click(function () {
                    var ele = $(this);
                    moveNextProcess(ele);
                });
            });
            clearValidationId3 = function () {
                $("#validationId3").html('');
            };
            isInviteCodeUsed = function (ele) {
                var inviteCode = $('#inviteCode').val();
                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                $.ajax({
                    url: url,
                    type: "GET",
                    async: false,
                    success: function (response) {
                        if (response === 'true') {
                            $("#error-inviteCode").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
                            $("#error-inviteCode").focus();
                            moveNextProcess(ele);
                            return true;
                        } else {
                            $("#error-inviteCode").css({'color': 'red', 'margin-left': '10px'}).html('Invite Code is used or invalid.');
                            $("#error-inviteCode").focus();
                            return false;
                        }
                    },
                    error: function (e) {
                        //handle error
                    }
                });
            };
        </script>
        <script>
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") === "text") {
                    input.attr("type", "password");
                } else {
                    input.attr("type", "text");
                }
            });
        </script>

        <script>
            function checkAccountNO(obj) {
                str = obj.value.replace('', '');
                if (str.length > 18) {
                    str = str.substr(0, 18);
                } else
                {
                    str = str.replace(/([A-Za-z0-9]{2})([[A-Za-z0-9]{4})([[A-Za-z0-9]{7})([[A-Za-z0-9]{4}$)/gi, "$1-$2-$3-$4"); //mask numbers (xxx) xxx-xxxx    
                    //                    //alert("22222");
                }
                //alert("last = "+str);
                obj.value = str;
            }
//            function checkird(obj) {
//                str = obj.value.replace('', '');
//                if (str.length > 10) {
//                    str = str.substr(0, 10);
//                } else
//                {
//                    str = str.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{3})([0-9]{2}$)/gi, "$1-$2-$3");
//                }
//                obj.value = str;
//            }
            function checkird(obj, event) {
                $(".ird-check").hide();
                if (event.charCode === 8 || event.charCode >= 48 && event.charCode <= 57) {
                    var ac = $(obj).val();
                    ac = ac.replace(/-/g, '');
                    if (ac.length > 2) {
                        var ac20 = ac.substr(0, 3);
                        var ac21 = ac.substr(3);
                        $(obj).val(ac20 + '-' + ac21);
                    }
                    if (ac.length > 5) {
                        var ac60 = ac.substr(0, 3);//4
                        var ac61 = ac.substr(3, 3);//4
                        var ac62 = ac.substr(6, 2);//3
                        $(obj).val(ac60 + '-' + ac61 + '-' + ac62);
                    }
                } else {
//                    alert(event.charCode);
                    event.preventDefault();
                }
                $('.error').text("");
            }
        </script>

        <script>

            var x = 0;
            $(".previous1").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step2").hide();
                $("#step11").show();
            });
            $(".previous2").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step3").hide();
                $("#step2").show();
            });
            $(".previous3").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step4").hide();
                $("#step3").show();
            });
            $(".previous4").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step5").hide();
                $("#step4").show();
            });
            $(".previous5").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step6").hide();
                $("#step5").show();
            });
            $(".previous6").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step7").hide();
                $("#step6").show();
            });
            $(".previous7").click(function () {
                $("#step8").hide();
                $("#step7").show();
            });
            $(".previous8").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step9").hide();
                $("#step8").show();
            });
            $(".previous9").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $("#step10").hide();
                $("#step9").show();
            });
            $(".previous11").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) - 1);
                $('.save-new-btn').hide();
                $("#step11").hide();
                $("#step1").show();
            });
            $(".next1").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                var password = $("#password").val();
                var email = $("#email").val();
                var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                //                var passwordExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
                var passwordExpression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*[\s]).{8,16}$/;
                var passwordTitle = "Password must contain at least one number, one uppercase and lowercase letter, one special character  and be between 8-16 characters";
//                if (email === "" || !emailExpression.test(email)) {
//                    $("#error-email").text("Please enter a valid email address, such as example@email.com ");
//                } else if (password === "" || !passwordExpression.test(password)) {
//                    $("#error-password").text(passwordTitle);
//                } else {
                    $("#allAccount").hide();
                    $("#step2").show();
                    $("#step11").hide();
//                }
            });
            $(".next2").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                $("#step3").show();
                $("#step2").hide();
            });
            $('.country').click(function () {
                $('#span-countryname').text('');
            });

            $(".next3").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                var fullName = $("#fullName").val();
                fullName = fullName.trim();
                var countryname = $("#countrynamefirst").val();
//               countryname = countryname.trim();
//                alert(countryname);

                var occupation = $(".Occupation").val();
                var inputOccupation = $("#input-occupation").val();
//                alert(occupation);
                var dob = $(".date-of-birth").val();
                dob = dob.trim();
                //                alert(dob);
                if (fullName === "") {
                    $("#span-fullName").text("This field is required ");
                } else if (dob === "") {
                    $("#span-dob").text("This field is required ");

                } else if (countryname === " –Select–" || countryname === "–Select–") {
                    $("#span-countryname").text("This field is required ");
                } else if (occupation === "-Select-" || occupation === "Other" && inputOccupation === "") {
                    $("#span-occupation").text("This field is required ");
                } else {
                    $("#step4").show();
                    $("#step3").hide();
//                   var a = $("#Licence_first_name").val('');
                }
            });
            $(".next4").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                var address = $("#address").val();
                address = address.trim();
                var mobile = $("#mobile").val();
                mobile = mobile.trim();
                if (address === "") {
                    $("#span-address").text("This field is required ");
                } else if (mobile === "") {
                    $("#span-mobile").text("This field is required ");
                } else {
                    $("#step5").show();
                    $("#step4").hide();
                }
            });
            $(".next5").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                var text = $('.spanverification').html();
                var verification = $("#verification").val();
                verification = verification.trim();
                if (verification === "") {
                    $("#error-verification").text("This field is required ");
                }
//                else if (text !== "Verification successful") {
//                } 
                else {
                    $("#step6").show();
                    $("#step5").hide();
                }
            });
            $('.country').click(function () {
                $('#error_passport_issue_by').text('');
            });
            $('.country').click(function () {
                $('#error_other_id_country').text('');
            });

            var First_name = "", Middle_name = "", last_Name = "";


            $(".next6").click(function () {
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var License_number = '';
                var licence_verson_number = '';
                var licence_expiry_Date = '';
                var passport_number = '';
                var passport_expiry = '';

                var x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);

                var index = $("#src_of_fund1 option:selected").val();
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    var licenseFirstName = $("#Licence_first_name").val();
                    firstName = licenseFirstName;
                    var Licence_middle_name = $("#Licence_middle_name").val();
                    middleName = Licence_middle_name;
                    var licenseLastName = $("#Licence_last_name").val();
                    lastName = licenseLastName;
                    var licenseNumber = $("#Licence_number").val();
                    licenseNumber = licenseNumber.trim();
                    License_number = licenseNumber;
                    var Expirydate = $("#Expiry_date").val();
                    Expirydate = Expirydate.trim();
                    licence_expiry_Date = Expirydate;
                    var licence_verson_number = $("#licence_Verson_number").val();
                    licence_verson_number = licence_verson_number.trim();
                    First_name = firstName, Middle_name = middleName, last_Name = lastName;
                    if (licenseFirstName === "") {
                        $("#error-Licence_first_name").text("This field is required ");
                    } else if (licenseLastName === "") {
                        $("#error-Licence_last_name").text("This field is required ");
                    } else if (licenseNumber === "") {
                        $("#error-Licence_number").text("This field is required ");
                    } else if (Expirydate === "") {
                        $("#error_Expiry_date").text("This field is required ");
                    } else if (licence_verson_number === "") {
                        $("#error_licence_Verson_number").text("This field is required ");
                    } else {
                        $("#step7").show();
                        $("#step6").hide();
                    }
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    var PassportFirstName = $("#passport_first_name").val();
                    firstName = PassportFirstName;
                    var PassportLastName = $("#passport_last_name").val();
                    lastName = PassportLastName;
                    var PassportMiddleName = $("#passport_middle_name").val();
                    middleName = PassportMiddleName;
                    var Passportnumber = $("#passport_number").val();
                    passport_number = Passportnumber;
                    var dob2 = $("#dob2").val();
                    passport_expiry = dob2;
                    var passportIssue = $("#passport_issue_by").val();
                    dob2 = dob2.trim();
                    Passportnumber = Passportnumber.trim();
                    First_name = firstName, Middle_name = middleName, last_Name = lastName;
                    if (PassportFirstName === "") {
                        $("#error_passport_first_name").text("This field is required ");
                    } else if (PassportLastName === "") {
                        $("#error_passport_last_name").text("This field is required ");
                    } else if (Passportnumber === "") {
                        $("#error_passport_number").text("This field is required ");
                    } else if (dob2 === "") {
                        $("#error_dob2").text("This field is required ");
                    } else if (passportIssue === " -Select-") {
                        $("#error_passport_issue_by").text("This field is required ");
                    } else {
                        $("#step7").show();
                        $("#step6").hide();
                    }
                    $('.country').click(function () {
                        $('#error_other_id_country').text('');
                    });
                } else {

                    var TypeofID = $("#other_id_issueBy").val();
                    var OT_first_name = $("#other_first_name").val();
                    var OT_middle_name = $("#other_middle_name").val();
                    var OT_last_name = $("#other_last_name").val();
                    var IdExpirydate = $("#IdExpirydate").val();
                    var IdDocument = $("#other_id_myFile").val();
                    var Country = $("#other_id_country").val();
                    First_name = OT_first_name, Middle_name = OT_middle_name, last_Name = OT_last_name;
                    TypeofID = TypeofID.trim();
                    if (TypeofID === "") {
                        $("#error_other_id_issueBy").text("This field is required ");
                    } else if (OT_first_name === "") {
                        $("#error_other_first_name").text("This field is required ");
                    } else if (OT_last_name === "") {
                        $("#error_other_last_name").text("This field is required ");
                    } else if (IdExpirydate === "") {
                        $("#error_IdExpirydate").text("This field is required ");
                    } else if (Country === " -Select-") {
                        $("#error_other_id_country").text("This field is required ");
                    } else if (IdDocument === "") {
                        $("#error_other_id_myFile").text("Please attach copy of ID document ");
                    } else {
                        $("#step7").show();
                        $("#step6").hide();
                    }
                }
                var Title = $('.nameTitle option:selected').text();
                var Date_of_Birth = $('#dob').val();
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date, title: Title,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
//                                swal({
//                                    title: "Success",
//                                    text: "Data is Verified.",
//                                    type: "info",
//                                    timer: 3500,
//                                    showConfirmButton: true
//                                });
                                $('#investor_verify').val('true');
                            } else {
//                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                $('#investor_verify').val('true');
                            } else {
//                                alert("wrong data");
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/pep-verification',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var obj = JSON.parse(data);
//                        alert(JSON.stringify(  obj.watchlistAML[0].verified));
//                        if (index === "1") {
                        if (obj.watchlistAML[0].verified) {
//                              
                            $('#investorPep_verify').val('true');
                        }
//                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });

            });

            $(".next7").click(function () {
                var j = 0;

                var IRD_Number = $("#IRD_Number").val().trim();
                var wealth_src = $('#wealth_src option:selected').text();
                var secondselect = $('.selectoption1  option:selected').val();
                var validate = validateIrd(IRD_Number);
                if (validate) {
                    if (wealth_src === "–Select–") {
                        $("#error_wealth_src").text("This field is required");
                    } else if (secondselect === "2") {
                        var tindivs = document.getElementsByClassName('checktindata');
                        for (var i = 0; i < tindivs.length; i++) {
                            var tindiv = tindivs[i];
                            var tinnum = tindiv.getElementsByClassName('TIN')[0];
                            var tinerror = tindiv.getElementsByClassName('TIN-error')[0];
                            var reason = tindiv.getElementsByClassName('resn_tin_unavailable')[0];
                            var reasonerror = tindiv.getElementsByClassName('resn_tin_unavailable-error')[0];
                            var country = tindiv.getElementsByClassName('countrynameoutnz')[0];
                            var countryerror = tindiv.getElementsByClassName('countrynameoutnz-error')[0];
                            if (country.value === " -Select-") {
                                countryerror.innerHTML = "This field is required ";
                            } else if (tinnum.value === "" && reason.value === "") {
                                tinerror.innerHTML = "This field is required ";
                                reasonerror.innerHTML = "This field is required ";
                            } else {
                                j++;
                            }
                        }
                        if (tindivs.length > j) {
                        } else {
                            x = $('[name="step"]').val();
                            $("#step").attr("value", parseInt(x) + 1);
                            $("#step8").show();
                            $("#step7").hide();
                        }
                    } else {
                        x = $('[name="step"]').val();
                        $("#step").attr("value", parseInt(x) + 1);
                        $("#step8").show();
                        $("#step7").hide();
                    }
                }
            });

            $('.bank_name').change(function () {
                if ($('.bank_name option:selected').val() === 'Other') {
                    $(".toggal_other").show();
                } else {
                    $(".toggal_other").hide();
                }
                var data = $('.bank_name option:selected').data('id');

                $('#accountNumber').val(data);
            });
            $(".next8").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                var bank_name = $('.bank_name').val();
                var other_name = $("#other_other_name").val();
                var acount_holder_name = $("#acount_holder_name").val();
                acount_holder_name = acount_holder_name.trim();
                var acount_holder_number = $("#accountNumber").val();
                var bank_document = $("#bank_document").val();
                var ifAcVErified = $('.isAccount_number_verified').val();
                acount_holder_number = acount_holder_number.trim();
//                alert(bank_name);
                if (bank_name === "" || bank_name === "–Select–" || bank_name === "Other" && other_name === "") {
                    if (bank_name === "–Select–" || bank_name === "") {
                        $("#error_bank_name").text("This field is required ");
                    } else {
                        $("#error_Other_Bank_name").text("This field is required ");
                    }
                } else if (acount_holder_name === "") {
                    $("#error_acount_holder_name").text("This field is required ");
                } else if (acount_holder_number === "") {
                    $("#error_acount_holder_number").text("This field is required");
                } else if (acount_holder_number.length !== 19 || ifAcVErified === "false") {
                    $("#error_acount_holder_number").text("account number is invalid");
                } else if (bank_document === "") {
                    $("#error_bank_document").text("Please attach verification of your bank account.");
                } else {
                    $("#step9").show();
                    $("#step8").hide();
                }
            });
            $(".next9").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                $('.save-new-btn').hide();
                if ($('input[name=term_condition]').is(':checked')) {
                    $("#step10").show();
                    $("#step9").hide();
                } else {
                    $("#error_term_condition").text("Please read and agree to the Terms and Conditions");

                }

            });
            $(".next10").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                $('.save-new-btn').show();
                $("#step11").show();
                $("#step1").hide();
            });
        </script>
        <script>
            function myFunction7() {
                var ac = document.getElementById("accountNumber").value;
                if (ac.length === 2) {
                    var ac20 = ac.substr(0, 2);
                    var a = ac20 + '-';
                    $("#accountNumber").val(a);
                }
                if (ac.length === 7) {
                    var ac21 = ac.substr(7);
                    var b = ac + '-' + ac21;
                    $("#accountNumber").val(b);
                }
                if (ac.length === 15) {
                    var ac212 = ac.substr(15);
//                alert("c14 7S");
                    var c = ac + '-' + ac212;
                    $("#accountNumber").val(c);
                }
                if (ac.length > 18) {
                    var ac23 = ac.substr(0, 18);
                    var d = ac23;
                    $("#accountNumber").val(d);
                }
            }
        </script>
        <script>
            $('.des-togle').hide();
            $('#wealth_src').change(function () {
                var val = $("#wealth_src option:selected").val();
                if (val === "7") {
                    $('.des-togle').show();
                }
                if (val === "6") {
                    $('.des-togle').hide();
                }
                if (val === "5") {
                    $('.des-togle').hide();
                }
                if (val === "4") {
                    $('.des-togle').hide();
                }
                if (val === "3") {
                    $('.des-togle').hide();
                }
                if (val === "2") {
                    $('.des-togle').hide();
                }
                if (val === "1") {
                    $('.des-togle').hide();
                }
                if (val === "0") {
                    $('.des-togle').hide();
                }
            });
            $('#src_of_fund1').change(function () {
                var val = $("#src_of_fund1 option:selected").val();
                if (val === "1") {
                    $('.verifybtn').show();
                    $('.passport-select').hide();
                    $('.other-select').hide();
                    $('.drivery-licence').show();
                }
                if (val === "2") {
                    $('.verifybtn').show();
                    $('.passport-select').show();
                    $('.other-select').hide();
                    $('.drivery-licence').hide();
                }
                if (val === "3") {
                    $('.verifybtn').hide();
                    $('.passport-select').hide();
                    $('.other-select').show();
                    $('.drivery-licence').hide();
                }
            });
            $('.selectoption1').change(function () {
                var val = $(".selectoption1 option:selected").val();
                if (val === "1") {
                    $('.yes-option').hide();
                }
                if (val === "2") {
                    $('.yes-option').show();
                }

            });
        </script>
        <script>
            $(document).ready(function () {
                $("#add-country-another").click(function () {
                    $(".yes-new").append("<div class='row checktindata removecountry'><div class='col-sm-6'><label class='label_input ' style='text-align:left'>Country of tax residence:</label></div>\n\
    <div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control countrynameoutnz tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameoutnz-error' ></span></div>\n\
    <div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div>\n\
    <div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='fullName' required='required' placeholder='Enter TIN' onkeyup='removetinspan()' /><span class='error TIN-error' ></span></div>\n\
    <div class='col-sm-6'><label class='label_input'>Reason if TIN not available :</label></div><div class='col-sm-6'><input type='text' class='form-control input-field resn_tin_unavailable' name='fullName' required='required' placeholder='' onkeyup='removetinspan()'/><span class='error resn_tin_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameoutnz").intlTelInput_3();
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.advisor-show').hide();
            });
            $('.avisor-click').change(function () {
                var val = $(".avisor-click option:selected").val();
                if (val === "1") {
                    $('.advisor-show').hide();
                }
                if (val === "2") {
                    $('.advisor-show').show();
                }

            });
            function removetinspan() {
                $('.error').text('');
            }
            function removecountrydiv(ele) {
                ele.closest('.removecountry').remove();
            }
        </script>
        <script>
            var Country = [];
            var Tin = [];
            var reasonTin = [];
            $('#pir').click(function () {
                $('#pir').attr("href", "https://www.ird.govt.nz/roles/portfolio-investment-entities/using-prescribed-investor-rates");
                window.open(this.href);
                return false;
            });
            $('#haveAccount').click(function () {
                $('#haveAccount').attr("href", "./haveAccount");
            });
            $('#submit').click(function () {
                $(this).off();
                var status = "SUBMISSION";
                saveData(status);
                location.href = "https://www.mintasset.co.nz/";

            });
            $('.saveExit').click(function () {
                var status = "PENDING";
                saveData(status);
            });
            saveData = function (status) {
                var email = $('.email').val();
                var password = $('#password').val();
                var raw_password = $('#password').val();
                var Title = $('.nameTitle option:selected').text();
                var fullName = $('.fullName').val();
                var preferredName = $('#preferredName').val();
                var Date_of_Birth = $('#dob').val();
                var country_residence = $('.country_residence').val();
                if (country_residence === " -Select-") {
                    country_residence = "";
                }

                var Occupation = $('.Occupation').val();
                var OccupationStatus = $('.Occupation option:selected').text();
                var working_with_adviser = $('.working_with_adviser option:selected').text();
                var advisor_company = $('.advisor_company option:selected').val();
                var advisor = $('.advisor option:selected').val();
                var homeAddress = $('.homeAddress').val();
                var mobile_country_code = $('.mobile_country_code').val();
                var mobile_number = $('.mobile_number1').val();
                var optional_num_type = $('.optional_num_type option:selected').text();
                var optional_num_code = $('.optional_num_code').val();
                var optional_num = $('.optional_num').val();
                var id_type = $('.id_type option:selected').text();
                var License_number = $('.License_number').val();
                var licence_expiry_Date = $('.licence_expiry_Date').val();
                var licence_issue_date = $('.licence_issue_Date').val();
                var licence_verson_number = $('.licence_verson_number').val();
                var passport_number = $('.passport_number').val();
                var passport_expiry = $('.passport_expiry').val();
                var passport_issue_date = $('.passport_issue_Date').val();
                var passport_issue_by = $('.issue_by').val();
                if (passport_issue_by === " –Select–" || passport_issue_by === "–Select–") {
                    passport_issue_by = "";
                }
                var investor_idverified = $('#investor_verify').val();
                var investor_PEPverified = $('#investorPep_verify').val();
                var other_id_type = $('.other_id_type').val();
                var other_id_expiry = $('.other_id_expiry').val();
                var other_id_issueBy = $('.other_id_issueBy').val();
                if (other_id_issueBy === " –Select–" || other_id_issueBy === "–Select–") {
                    other_id_issueBy = "";
                }

                var PIR = $('.PIR option:selected').text();
                var IRD_Number = $('.IRD_Number').val();
//                IRD_Number = IRD_Number.replace(/-/g, '');
                var wealth_src = $('#wealth_src option:selected').text();
                if (wealth_src === " –Select–" || wealth_src === "–Select–") {
                    wealth_src = "";
                }
                var inputCountry = $(".tex_residence_Country");
                var inputTIN = $(".TIN");
                var inputReasonTin = $(".resn_tin_unavailable");
                for (var i = 0; i < inputCountry.length; i++) {
                    if (inputCountry !== " -Select-") {
                        var country = $(inputCountry[i]).val();
                        if (country === " -Select-") {
                            country = "";
                        }
                        Country.push(country);
                        Tin.push($(inputTIN[i]).val());
                        reasonTin.push($(inputReasonTin[i]).val());
                    }
                }
                var taxCountries = Country;
                var tins = Tin;
                var reasonTins = reasonTin;
                var bank_name = $('.bank_name').val();
//                if (bank_name === " –Select–" || bank_name === "–Select–") {
//                    bank_name = "";
//                }
                if (bank_name === "Other") {
                    bank_name = $('.Other_Bank_name').val();
                }
//                alert(bank_name);
                var acount_holder_name = $('.acount_holder_name').val();
                var account_number = $('.account_number').val();
                var step_id = $('[name="step"]').val();
                var reg_type = $('[name="register_type"]').val();
                var reg_id = $('[name="register_id"]').val();
                var postalCode = $('#postal_code').val();
                if (postalCode !== "") {
                    homeAddress = homeAddress + ", " + postalCode;
                }
//                alert(country_residence);
                var DataObj = {email: email, password: password, raw_password: raw_password, title: Title, fullName: fullName, preferredName: preferredName, date_of_Birth: Date_of_Birth,
                    country_residence: country_residence, occupation: Occupation, OccupationStatus: OccupationStatus, working_with_adviser: working_with_adviser,
                    advisor_company: advisor_company, advisor: advisor, homeAddress: homeAddress, mobile_country_code: mobile_country_code,
                    mobile_number: mobile_number, optional_num_type: optional_num_type, optional_num_code: optional_num_code,
                    optional_num: optional_num, id_type: id_type, license_number: License_number, licence_expiry_Date: licence_expiry_Date, licence_issue_date: licence_issue_date,
                    licence_verson_number: licence_verson_number, passport_number: passport_number, passport_expiry: passport_expiry, passport_issue_date: passport_issue_date,
                    passport_issue_by: passport_issue_by, other_id_type: other_id_type, other_id_expiry: other_id_expiry,
                    other_id_issueBy: other_id_issueBy, pir: PIR, ird_number: IRD_Number, wealth_src: wealth_src, taxCountries: taxCountries,
                    tins: tins, reasonTins: reasonTins, bank_name: bank_name, acount_holder_name: acount_holder_name, bankFile: bankFile, otherFile: otherFile,
                    account_number: account_number, step: step_id, status: status, reg_id: reg_id, reg_type: reg_type, investor_idverified: investor_idverified, postalCode: postalCode,
                    firstName: First_name, middleName: Middle_name, lastName: last_Name, pepStatus: investor_PEPverified};
                console.log(JSON.stringify(DataObj));
                swal({
                    title: "",
                    text: "Application data being saved...  ",
                    type: "success",
                    timer: 3500,
                    showConfirmButton: false
                });
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/person-registeration',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        swal({
                            title: "Success",
                            text: "save data is successfull.",
                            type: "success",
                            timer: 2500,
                            showConfirmButton: false
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            };

            $(".verify-dl").click(function () {

                var index = $("#src_of_fund1 option:selected").val();
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';

                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = $('input[name=licence_first_name]').val();
                    middleName = $('input[name=licence_middle_name]').val();
                    lastName = $('input[name="licence_last_name"]').val();
                    var License_number = $('.License_number').val();
                    var licence_expiry_Date = $('.licence_expiry_Date').val();
                    var licence_verson_number = $('.licence_verson_number').val();
                    if (firstName === "") {
                        $("#error_licence_first_name").text("This field is required ");
                        return false;
                    } else if (License_number === "") {
                        $("#error-Licence_number").text("This field is required ");
                        return false;
                    } else if (licence_verson_number === "") {
                        $("#error_licence_Verson_number").text("This field is required ");
                        return false;
                    }

                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = $('input[name=passport_first_name]').val();
                    middleName = $('input[name=passport_middle_name]').val();
                    lastName = $('input[name="passport_last_name"]').val();
                    var passport_number = $('input[name="passport_number"]').val();
                    var dob2 = $("#dob2").val();
                    dob2 = dob2.trim();
                    passport_number = passport_number.trim();
                    if (firstName === "") {
                        $("#error_passport_first_name").text("This field is required ");
                        return false;
                    } else if (passport_number === "") {
                        $("#error_passport_number").text("This field is required ");
                        return false;
                    } else if (dob2 === "") {
                        $("#error_dob2").text("This field is required ");
                        return false;
                    }
                }
                var Date_of_Birth = $('#dob').val();
                var passport_expiry = $('input[name="passport_expiry"]').val();
//                var passport_expiry = "2017-10-10";
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
//                                swal({
//                                    title: "Success",
//                                    text: "Data is Verified.",
//                                    type: "info",
//                                    timer: 3500,
//                                    showConfirmButton: true
//                                });
                                $('#investor_verify').val('true');
                            } else {
                                $('#investor_verify').val('false');
//                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
//                                swal({
//                                    title: "Success",
//                                    text: "Data is Verified.",
//                                    type: "info",
//                                    timer: 3500,
//                                    showConfirmButton: true
//                                });
                                $('#investor_verify').val('true');
                            } else {
                                $('#investor_verify').val('false');
//                                alert("wrong data");
                            }

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            });
        </script>
        <script>
            //            $('#term_condition').click(function () {
            //                if (!$(this).is(':checked')) {
            //                    $('#error_term_condition').text('');
            //                }
            //            });
//            function personal() {
//                var email = $('.email').val();
//                var password = $('#password').val();
//                $('#typeform input[name=type]').val('INDIVIDUAL_ACCOUNT');
//                $('#typeform input[name=email]').val(email);
//                $('#typeform input[name=password]').val(password);
//                $('#typeform').submit();
////                document.getElementById("trust").href = "./personal-signup?email='" + email + "'&password='" + password + "'";
//            }
//            function minor() {
//                var email = $('.email').val();
//                var password = $('#password').val();
//                $('#typeform input[name=type]').val('MINOR_ACCOUNT');
//                $('#typeform input[name=email]').val(email);
//                $('#typeform input[name=password]').val(password);
//                $('#typeform').submit();
////                document.getElementById("trust").href = "./trust-signup?email='" + email + "'&password='" + password + "'";
//            }
//            function trust() {
//                var email = $('.email').val();
//                var password = $('#password').val();
//                $('#typeform input[name=type]').val('TRUST_ACCOUNT');
//                $('#typeform input[name=email]').val(email);
//                $('#typeform input[name=password]').val(password);
//                $('#typeform').submit();
////                document.getElementById("trust").href = "./trust-signup?email='" + email + "'&password='" + password + "'";
//            }
//            function company() {
//                var email = $('.email').val();
//                var password = $('#password').val();
//                $('#typeform input[name=type]').val('COMPANY_ACCOUNT');
//                $('#typeform input[name=email]').val(email);
//                $('#typeform input[name=password]').val(password);
//                $('#typeform').submit();
//                //                document.getElementById("company").href = "./company-signup?email='" + email + "'&password='" + password + "'";
//            }
//            function joint() {
//                var email = $('.email').val();
//                var password = $('#password').val();
//                $('#typeform input[name=type]').val('JOINT_ACCOUNT');
//                $('#typeform input[name=email]').val(email);
//                $('#typeform input[name=password]').val(password);
//                $('#typeform').submit();
//                //                document.getElementById("joint").href = "./joint-account?email='" + email + "'&password='" + password + "'";
//            }
        </script>
        <script>
            $('.senderType').click(function () {
                $(".spanverification").html('');
                $("#verification").val('');
                //                var cCo = $("#countryCode3").val();
                var mNo = $('.mobile_number1').val();
                var cCo = $('.mobile_country_code').val();
                var sTy = $('.senderType:checked').val();
                cCo = cCo.replace(/\+/g, "");
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                //document.getElementById('error-loader').style.display = 'block';
                document.getElementById('error-generateOtp').innerHTML = "";
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        $('#error-generateOtp').html("Please check your messages");
                        $('#otp-block').show();
                        //            document.getElementById('error-loader').style.display = 'none';
                        $('input:radio[name="senderType"]').prop("checked", false);
                    },
                    error: function (e) {
                        var mobileNo = document.getElementsByClassName('mobile_number1');
                        var result = true;
                        $('#otp-block').show();
                        if (mobileNo.value === '') {
                            //document.getElementById('error-loader').style.display = 'none';
                            //                 $('#error-generateOtp').text("Mobile number is required.");
                            $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        } else {
                            //document.getElementById('error-loader').style.display = 'none';

                            //                 $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        }
                        return result;
                    }
                });
                return sTy;
            });
        </script>
        <script>
            $('.checkname').change(function () {
                console.log("daya");
                var root = $(this).closest('.closestcls');
                var name = $(this).val();
                var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
                if (!allowedExtensions.exec(name)) {
                    root.find('.error').text("File format not allowed (only images and pdf files are allowed)");
                    $(".removefile").hide();
                    root.find('.shownamedata').text("");
                    root.find('.checkname').val('');
                } else {
                    var a = this.files[0].size;
                    if (a < 2008576) {
                        var filename = name.split('\\').pop().split('/').pop();
                        filename = filename.substring(0, filename.lastIndexOf('.'));
                        console.log(name);
                        root.find('.shownamedata').text("File: " + filename);
                        $(".removefile").show();
                    } else {
                        root.find('.error').text("Max. File size 2 Mb");
                        $(".removefile").hide();
                        root.find('.shownamedata').text("");
                        root.find('.checkname').val('');
                    }
                }
            });


        </script>
        <script>
            function validateIrd(irdNumber) {
                const irdToUse = irdNumber.replace(/-/g, '');
                var returnval = false;
                if (irdToUse.length !== 9) {
                    $("#error_IRD_Number").text("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
                } else if (irdToUse < 10000000 || irdToUse > 150000000) {
                    $("#error_IRD_Number").text("IRD number must be in the range of 10000000 - 150000000");
                } else {
//                var originalIrd = irdToUse.padStart(9, '0');
                    var originalIrd = irdToUse;
                    var count = 0;
                    var irdFirstWeight = '32765432';
                    getCheckDigit(originalIrd, irdFirstWeight);
                    function getCheckDigit(originalIrd, irdWeight) {
                        var ird = originalIrd.substr(0, originalIrd.length - 1);
                        var lastDigit = originalIrd.substr(-1);
                        var total = 0;
                        for (var i = 0; i < ird.length; i++) {
                            var j = i + 1;
                            var irdDigit = ird.substring(i, j);
                            var irdWeightDigit = irdWeight.substring(i, j);
                            total = total + (irdDigit * irdWeightDigit);
                        }
                        var remainder = total % 11;
                        if (remainder === 0) {
                            $(".ird-check").show();
                            returnval = true;
                        } else {
                            var calCheck = 11 - remainder;
                            if (calCheck >= 0 && calCheck <= 9) {
                                if (calCheck === parseInt(lastDigit)) {
                                    $(".ird-check").show();
                                    returnval = true;
                                } else {
                                    $("#error_IRD_Number").text("IRD Number is Invalid");
                                    returnval = false;
                                }
                            } else {
                                var irdSecondWeight = '74325276';
                                count++;
                                if (count === 1) {
                                    getCheckDigit(originalIrd, irdSecondWeight);
                                }
                                $("#error_IRD_Number").text("IRD Number is Invalid");
                                returnval = false;
                            }
                        }
                    }
                }
                return returnval;

            }

            function  removedatadiv(ele) {
                var root = ele.closest('.closestcls');
                $(root).find('.shownamedata').text("");
                $(root).find('.checkname').val("");
                $(".removefile").hide();
            }

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
        <script>
            var x, i, j, selElmnt, a, b, c;
            /*look for any elements with the class "custom-select":*/
            x = document.getElementsByClassName("custom-select");
            for (i = 0; i < x.length; i++) {
                selElmnt = x[i].getElementsByTagName("select")[0];
                /*for each element, create a new DIV that will act as the selected item:*/
                a = document.createElement("DIV");
                a.setAttribute("class", "select-selected");
                a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                x[i].appendChild(a);
                /*for each element, create a new DIV that will contain the option list:*/
                b = document.createElement("DIV");
                b.setAttribute("class", "select-items select-hide");
                for (j = 1; j < selElmnt.length; j++) {
                    /*for each option in the original select element,
                     create a new DIV that will act as an option item:*/
                    c = document.createElement("DIV");
                    c.innerHTML = selElmnt.options[j].innerHTML;
                    c.addEventListener("click", function (e) {
                        /*when an item is clicked, update the original select box,
                         and the selected item:*/
                        var y, i, k, s, h;
                        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                        h = this.parentNode.previousSibling;
                        for (i = 0; i < s.length; i++) {
                            if (s.options[i].innerHTML == this.innerHTML) {
                                s.selectedIndex = i;
                                h.innerHTML = this.innerHTML;
                                y = this.parentNode.getElementsByClassName("same-as-selected");
                                for (k = 0; k < y.length; k++) {
                                    y[k].removeAttribute("class");
                                }
                                this.setAttribute("class", "same-as-selected");
                                break;
                            }
                        }
                        h.click();
                    });
                    b.appendChild(c);
                }
                x[i].appendChild(b);
                a.addEventListener("click", function (e) {
                    /*when the select box is clicked, close any other select boxes,
                     and open/close the current select box:*/
                    e.stopPropagation();
                    closeAllSelect(this);
                    this.nextSibling.classList.toggle("select-hide");
                    this.classList.toggle("select-arrow-active");
                });
            }
            function closeAllSelect(elmnt) {
                /*a function that will close all select boxes in the document,
                 except the current select box:*/
                var x, y, i, arrNo = [];
                x = document.getElementsByClassName("select-items");
                y = document.getElementsByClassName("select-selected");
                for (i = 0; i < y.length; i++) {
                    if (elmnt == y[i]) {
                        arrNo.push(i)
                    } else {
                        y[i].classList.remove("select-arrow-active");
                    }
                }
                for (i = 0; i < x.length; i++) {
                    if (arrNo.indexOf(i)) {
                        x[i].classList.add("select-hide");
                    }
                }
            }
            /*if the user clicks anywhere outside the select box,
             then close all select boxes:*/
            document.addEventListener("click", closeAllSelect);
        </script>

        <script>

            $(".select li").click(function () {

                $("#error_bank_name").text('');
                var a = $(this).data('id');
                var b = $(this).data('value');
                $('#accountNumber').val(a);
                $('#bank_name').val(b);
                if (b === 'Other') {
                    $('.toggal_other').show();
                } else {
                    $('.toggal_other').hide();
                }
            });

            $('.textfirst').click(function () {
                $('#bank_name').val("");

            });


            $(function () {
                // Set
                var main = $('div.mm-dropdown .textfirst');
                var li = $('div.mm-dropdown > ul > li.input-option');
                var inputoption = $("div.mm-dropdown .option");
                var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" />';

                // Animation
                main.click(function () {
                    main.html(default_text);
                    li.toggle('fast');
                });

                // Insert Data
                li.click(function () {
                    // hide
                    li.toggle('fast');
//                    bankName = $(this).data('value');
                    var liid = $(this).data('id');
                    var lihtml = $(this).html();
                    main.html(lihtml);
                    inputoption.val(liid);
                });
            });
        </script>
    </body>
</html>								
