<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>




        <style>
            
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Pending Transactions</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">                                  
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                        </div>
                                        <h6 class="element-header">
                                            Pending & Cancelled Bank to Cash Wallet 
                                            <img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">
<!--                                        <div class="pdf-btn">
                                                    <button class="btn btn-primary" value="">Generate PDF</button>
                                                </div>-->
                                        </h6>
                                        <div class="element-box">
                                            <table id="" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="" ><span >Date Time</span></th>
                                                    <th data-field="" ><span >Transaction Id</span></th>
                                                    <th data-field="" ><span >Customer Id/ Name/ Email</span></th>
                                                    <th data-field="" ><span >Master</span></th>
                                                    <th data-field="" ><span >Status</span></th>
                                                    <th data-field="" ><span>Amount</span></th>
                                                    <th data-field="" ><span>Local Amount</span></th>
                                                    <th data-field="" ><span>A/c Details</span></th>
                                                    <th data-field="" ><span>Description</span></th>
                                                    <th data-field="" ><span>Actions</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                  
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                        </div>
                                        <h6 class="element-header">
                                            Pending & Cancelled User Investment & Sell Requests <img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-box">
                                            <table id="" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="" ><span >Date Time</span></th>
                                                    <th data-field="" ><span >Transaction Id</span></th>
                                                    <th data-field="" ><span >Customer Id/ Name/ Email</span></th>
                                                    <th data-field="" ><span >Email</span></th>
                                                    <th data-field="" ><span >Portfolio</span></th>
                                                    <th data-field="" ><span>Action</span></th>
                                                    <th data-field="" ><span>Status</span></th>
                                                    <th data-field="" ><span>Amount</span></th>
                                                    <th data-field="" ><span>Percentage</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
     <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
   
    <!--    <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/all-funds',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $(".loader").hide();
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $.each(obj, function (idx, val) {
                            if (val.Code === "290002") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore1').attr('href', id1);
                            }
                            if (val.Code === "290004") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore2').attr('href', id1);
                            }
                            if (val.Code === "290006") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore3').attr('href', id1);
                            }
                            if (val.Code === "290012") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore4').attr('href', id1);
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
        </script>-->
    <script>
//        $('#viewmore1').click(function () {
//            $('.hidediv1').show();
//            $('#viewmore1').hide();
//        });
//        $('#viewmore2').click(function () {
//            $('.hidediv2').show();
//            $('#viewmore2').hide();
//        });
//        $('#viewmore3').click(function () {
//            $('.hidediv3').show();
//            $('#viewmore3').hide();
//        });
        $('.clickinput').on("click", function () {
            var recemt = $(this).closest('.funds-deatil');
            recemt.find(".offer-input").toggle();
        });
        $(document).ready(function () {
            $(".loader").fadeOut("slow");
            $('.hidediv1').hide();
            $('.hidediv2').hide();
            $('.hidediv3').hide();
        });
        $('.democlick').click(function () {
            var data = $(this).data("target");
            $('.showclick').hide();
            $(data).show();
        });
    </script>
</body>
</html>