<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">News & Research</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <!--                            <div class="row">
                                                            <div class="col-md-4">
                            
                                                                <div class="element-box">
                                                                    <div class="support-box">
                                                                        <h5>London</h5>
                                                                        <p><span class="support-adress">Call us:</span> <span>+ (123) 1300-656-1046</span></p>
                                                                        <p><span class="support-adress">Office</span> <span>No.01 - 399-0</span></p>
                                                                        <p><span>Lorem Street City Melbourne</span></p>
                                                                        <a href="javascript:void(0);" class="btn btn-info a-btn-slide-text "><span><strong>Google Maps</strong></span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                            
                                                                <div class="element-box">
                                                                    <div class="support-box">
                                                                        <h5>Lisbon</h5>
                                                                        <p><span class="support-adress">Call us:</span> <span>+ (123) 1300-656-1046</span></p>
                                                                        <p><span class="support-adress">Office</span> <span>No.02 - 399-0 Lorem Street</span></p>
                                                                        <p><span>City London</span></p>
                                                                        <a href="javascript:void(0);" class="btn btn-info a-btn-slide-text "><span><strong>Google Maps</strong></span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                            
                                                                <div class="element-box">
                                                                    <div class="support-box">
                                                                        <h5>Melbourne</h5>
                                                                        <p><span class="support-adress">Call us:</span> <span>+ (123) 1300-656-1046</span></p>
                                                                        <p><span class="support-adress">HeadQuarters - No.:</span> <span>01 - 399-0</span></p>
                                                                        <p><span>Lorem Street City Melbourne</span></p>
                                                                        <a href="javascript:void(0);" class="btn btn-info a-btn-slide-text "><span><strong>Google Maps</strong></span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <!--<iframe height="645px" width="100%" src="https://www.mintasset.co.nz/news/" name="iframe_a"></iframe>--> 
                                </div>
                            </div>
<!--                            <div class="row">
                                <div class="col-md-4">
                                    <a href="javascript:void(0)">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>Live Chat</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:void(0)">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>Send Ticket</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:void(0)">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>Phone Now</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:void(0)">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-book" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>Knowledgebase</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:void(0)">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>Cliente Area</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a href="./home-faq-page">
                                        <div class="element-box">
                                            <div class="support-other">
                                                <div class="support-box-icon">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="icon-data">
                                                    <h5>FAQ</h5>
                                                    <p>Lorem Ipsum is simply dummy text printing.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
     <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>

    <!--    <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/all-funds',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $(".loader").hide();
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $.each(obj, function (idx, val) {
                            if (val.Code === "290002") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore1').attr('href', id1);
                            }
                            if (val.Code === "290004") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore2').attr('href', id1);
                            }
                            if (val.Code === "290006") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore3').attr('href', id1);
                            }
                            if (val.Code === "290012") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore4').attr('href', id1);
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
        </script>-->
    <script>
//        $('#viewmore1').click(function () {
//            $('.hidediv1').show();
//            $('#viewmore1').hide();
//        });
//        $('#viewmore2').click(function () {
//            $('.hidediv2').show();
//            $('#viewmore2').hide();
//        });
//        $('#viewmore3').click(function () {
//            $('.hidediv3').show();
//            $('#viewmore3').hide();
//        });
        $('.clickinput').on("click", function () {
            var recemt = $(this).closest('.funds-deatil');
            recemt.find(".offer-input").toggle();
        });
        $(document).ready(function () {
            $(".loader").hide();
            $('.hidediv1').hide();
            $('.hidediv2').hide();
            $('.hidediv3').hide();
        });
        $('.democlick').click(function () {
            var data = $(this).data("target");
            $('.showclick').hide();
            $(data).show();
        });
    </script>
</body>
</html>