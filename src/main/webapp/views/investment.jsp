<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        <style>
           
            .menu-side .layout-w {

                min-height: 669px;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row">
                                                    <!--                                                    <div class="col-md-12">
                                                                                                            <div class="element-wrapper">
                                                                                                                <h6 class="element-header">Investment Options </h6>
                                                                                                            </div>
                                                                                                            <p>We have designed four retail managed funds, each offering a different investment objective and level of risk. </p>
                                                                                                        </div>-->
                                                    <c:set var = "data"  value = "#demo"/>
                                                    <c:set var = "id"  value = "${0}"/>
                                                    <%--<c:forEach items="${allPortfolios}" var="portfolio">--%>
                                                    <%--<c:set var = "id"  value = "${id+1}"/>--%>
                                                    <!--
                                                                                                            <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="<c:out value="${data}"/><c:out value="${id}"/>">
                                                                                                                <a href="#">
                                                                                                                    <div class="ilumony-box text-center democlick" style="cursor: pointer; background: #0f256e;">
                                                                                                                        <div class="portfolio_overlay">
                                                                                                                            <h5 class="color3 portfolio_name">${portfolio.getName()}</h5>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a> 
                                                    
                                                                                                            </div>-->
                                                    <%--</c:forEach>--%>

                                                    <!--                                                    <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo2">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Australasian Property Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>
                                                    
                                                                                                        <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo3">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Diversified Income Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>
                                                    
                                                                                                        <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo4">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Diversified Growth Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>-->

                                                    <!--                                                    <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo2">
                                                                                                                <h4>Mint Australasian Property Fund</h4>
                                                                                                                <p>This is a single asset class Fund, investing predominantly in Australasian listed property securities. Investors should expect returns and risk commensurate with the listed property sector of the New Zealand and Australian share markets.</p>
                                                                                                                <p>Our objective is to outperform the S&P/NZX All Real Estate (Industry Group) Gross Index (which is the FundÃ¢ÂÂs relevant market index) by 1% per annum, before fees, over the medium to long-term.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore2" href="investment-fund-2.html">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo3">
                                                                                                                <h4>Mint Diversified Income Fund</h4>
                                                                                                                <p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.</p>
                                                                                                                <p>The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is MintÃ¢ÂÂs lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore3" href="investment-fund-3.html">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->
                                                    <!--                                                    <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo4">
                                                                                                                <h4>Mint Diversified Growth Fund</h4>
                                                                                                                <p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities. </p>
                                                                                                                <p>The objective of the Fund is to deliver returns in excess of the Consumer Price Index (CPI) by 4.5% per annum, before fees, over the medium to long-term. Investors should expect returns and risk to sit between the risk profiles of the property and equities asset classes.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore4" href="investment-fund-{}">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->

                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo5">
                                                            <h4>Aditya Birla Sun Life International Equity Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo6">
                                                            <h4>Kotak US Equity Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo7">
                                                            <h4>Kotak US Equity Growth Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!--                                                        <div class="element-wrapper">
                                                                                                                    <h6 class="element-header">Investment Options </h6>
                                                                                                                </div>-->
                                                        <p class="each">We have designed four retail managed funds, each offering a differrent investment objective and level of risk.</p>
                                                        <br>
                                                        <p class="each">INVESTMENT OPTIONS</p>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="new-fund">
                                                            <div id="accordion" class="dropdown">
                                                                <c:set var = "data1"  value = "demo"/>
                                                                <c:set var = "id1"  value = "${0}"/>
                                                                <c:forEach items="${allPortfolios}" var="portfolio">
                                                                    <c:set var = "id1"  value = "${id1+1}"/>
                                                                    <div class="card">
                                                                        <a class="card-link collapsed" data-toggle="collapse" href="#<c:out value="${data1}"/><c:out value="${id1}"/>" aria-expanded="false">
                                                                            <div class="arrwa">
                                                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                            </div>
                                                                            <div class="card-header">
                                                                                ${portfolio.getName()}
                                                                            </div>
                                                                        </a>
                                                                        <div id="<c:out value="${data1}"/><c:out value="${id1}"/>" class="collapse" data-parent="#accordion" aria-expanded="false" style="">
                                                                            <div class="card-body">
                                                                                ${portfolio.fundOverview}
                                                                                <div class="new-fund-btn">
                                                                                    <form method="post" action="./portfolio">
                                                                                        <input type="hidden" name="pc" value="${portfolio.getCode()}">
                                                                                    <button class="mint-btn-design low-padding" type="submit">Show Me More</button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:forEach>
                                                                <!--                                                                    <div class="card">
                                                                                                                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                                                                                                            <div class="arrwa">
                                                                                                                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                                                                                            </div>
                                                                                                                                            <div class="card-header">
                                                                                                                                                Mint Diversified Growth Fund
                                                                                                                                            </div>
                                                                                                                                        </a>
                                                                                                                                        <div id="collapseTwo" class="collapse" data-parent="#accordion" aria-expanded="false" style="">
                                                                                                                                            <div class="card-body">
                                                                                                                                                This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities. The objective of the Fund is to deliver returns in excess of the Consumer Price Index (CPI) by 4.5% per annum, before fees, over the medium to long-term. Investors should expect returns and risk to sit between the risk profiles of the property and equities asset classes.
                                                                                                                                                <div class="new-fund-btn">
                                                                                                                                                    <a class="btn btn-primary btn-style" href="./portfolio-">Show Me More</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="card">
                                                                                                                                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                                                                                                                            <div class="arrwa">
                                                                                                                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                                                                                            </div>
                                                                                                                                            <div class="card-header">
                                                                                                                                                Mint Diversified Income Fund
                                                                                                                                            </div>
                                                                                                                                        </a>
                                                                                                                                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                                                                                                            <div class="card-body">
                                                                                                                                                This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is Mint’s lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.
                                                                                                                                                <div class="new-fund-btn">
                                                                                                                                                    <a class="btn btn-primary btn-style" href="./portfolio-">Show Me More</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="card">
                                                                                                                                        <a class="card-link" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                                                                                                                            <div class="arrwa">
                                                                                                                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                                                                                            </div>
                                                                                                                                            <div class="card-header">
                                                                                                                                                Mint Australasian Equity Fund   
                                                                                                                                            </div>
                                                                                                                                        </a>
                                                                                                                                        <div id="collapseFour" class="collapse" data-parent="#accordion" aria-expanded="true" style="">
                                                                                                                                            <div class="card-body">
                                                                                                                                                This is a single asset class Fund, investing predominantly in Australasian equities, and targeting medium to long-term growth. Investors should expect returns and risk commensurate with the New Zealand and Australian share markets.Our objective is to outperform the S&amp;P/NZX50 Gross Index (which is the Fund’s relevant market index) by 3% per annum, before fees, over the medium to long-term.
                                                                                                                                                <div class="new-fund-btn">
                                                                                                                                                    <a class="btn btn-primary btn-style" href="./portfolio-">Show Me More</a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>	
                                                <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                            </div>
                                            <div class="tab-pane fade p-31" id="two" role="tabpanel" aria-labelledby="two-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>Through our funds and direct investments, we have invested into 170+ companies. Search for and view the details of particular companies here</p></div>
                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo8">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/chipilogo.png) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">CHIPI</h5>
                                                                </div>
                                                            </div>
                                                        </a> 

                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo9">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/ladder.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Think Ladder</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo12">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe3.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Google</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo13">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Canon</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo14">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe5.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Coca-Cola</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo8">
                                                            <h4>CHIPI</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="investment-company.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo9">
                                                            <h4>Think Ladder</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="investment-company-ladder.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo10">
                                                            <h4>Toyota</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo11">
                                                            <h4>Apple</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo12">
                                                            <h4>Google</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo13">
                                                            <h4>Canon</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo14">
                                                            <h4>Coca-Cola</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="#" class="btn btn-primary" id="viewmore2">View More</a>              
                                            </div>
                                            <div class="tab-pane fade p-31" id="three" role="tabpanel" aria-labelledby="three-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>See the details of new offers coming soon and current investment opportunities </p></div>
                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo15">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2020</h5>
                                                                </div>
                                                            </div>
                                                        </a> 

                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo16">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2021</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo17">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2022</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo18">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2023</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo19">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2024</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo20">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2025</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo21">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe5.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2026</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo15">
                                                            <h4>New Fund 2020</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo16">
                                                            <h4>New Fund 2021</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (hedged to AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new"> -->
                                                            <!-- <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a> -->
                                                            <!-- </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo17">
                                                            <h4>New Fund 2022</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo18">
                                                            <h4>New Fund 2023</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of a 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo19">
                                                            <h4>New Fund 2024</h4>
                                                            <p>10.0% of the excess return of the units of the Fund above the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (hedged to AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo20">
                                                            <h4>New Fund 2025</h4>
                                                            <p>10% (20% for Class B units) of the excess return above the Absolute Return performance hurdle of 10% per annum. Additionally, Performance Fees are subject to a high water mark. For further details on Performance Fees, please read the relevant PDS and the Additional Information Booklet.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo21">
                                                            <h4>New Fund 2026</h4>
                                                            <p>10.0% of the excess return of the Units of the Fund above the higher of the Index Relative Hurdle (S&P Global Infrastructure Index A$ Hedged Net Total Return) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <a href="#" class="btn btn-primary" id="viewmore3">View More</a>   -->            
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>

        <!--    <script>
                $(document).ready(function () {
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/all-funds',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            $(".loader").hide();
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(obj);
                            $.each(obj, function (idx, val) {
                                if (val.Code === "290002") {
                                    var id1 = './portfolio-' + val.Code;
                                    $('.showmemore1').attr('href', id1);
                                }
                                if (val.Code === "290004") {
                                    var id1 = './portfolio-' + val.Code;
                                    $('.showmemore2').attr('href', id1);
                                }
                                if (val.Code === "290006") {
                                    var id1 = './portfolio-' + val.Code;
                                    $('.showmemore3').attr('href', id1);
                                }
                                if (val.Code === "290012") {
                                    var id1 = './portfolio-' + val.Code;
                                    $('.showmemore4').attr('href', id1);
                                }
                            });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });
                });
            </script>-->
        <script>
            //        $('#viewmore1').click(function () {
            //            $('.hidediv1').show();
            //            $('#viewmore1').hide();
            //        });
            //        $('#viewmore2').click(function () {
            //            $('.hidediv2').show();
            //            $('#viewmore2').hide();
            //        });
            //        $('#viewmore3').click(function () {
            //            $('.hidediv3').show();
            //            $('#viewmore3').hide();
            //        });
            $('.clickinput').on("click", function () {
                var recemt = $(this).closest('.funds-deatil');
                recemt.find(".offer-input").toggle();
            });
            $(document).ready(function () {
                $(".loader").hide();
                $('.hidediv1').hide();
                $('.hidediv2').hide();
                $('.hidediv3').hide();
            });
            $('.democlick').click(function () {
                var data = $(this).data("target");
                $('.showclick').hide();
                $(data).show();
            });
        </script>
    </body>
</html>