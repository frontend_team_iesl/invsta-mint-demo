<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>



        <style>

            img.logo {
                width: 40%;
            }

            .continue {
                cursor: pointer; 
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Recipients</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row1">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Payment Recipients </h6>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">Who are you sending money to?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="body-section">
                                                                                            <h6 class="body-data">Existing recipients</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Myself</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">David</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 9524</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="body-section">
                                                                                            <h6 class="body-data">New recipient</h6>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Myself</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 0704</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Someone Else</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 9524</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 continue">
                                                                                        <div class="body-box">
                                                                                            <div class="body-icon">
                                                                                                <i class="fa fa-user" aria-hidden="false"></i>
                                                                                            </div>
                                                                                            <h6 class="icon-name">Company Account</h6>
                                                                                            <img src="./resources/images/nz-flag.png"> <span class="flag-data">USD Account</span>
                                                                                            <p>ending with 8354</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script type="text/javascript">
            $(window).load(function () {

                $(".loader").fadeOut("slow");
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".loader").fadeOut("slow");
                $(".continue").click(function () {
                    window.location.href = './review';
                });
            });
        </script>
    </body>
</html>