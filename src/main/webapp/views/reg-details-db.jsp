
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>


    </head>
    <body>
        <!--<div class="loader"></div>-->
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Transactions</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12 invst-option main-profile">
                                    <div class="card mt-3 tab-card user-show-background">
                                        <c:if test="${not empty person}">
                                            <table id="individual-person-detail" class="display nowrap table-bordered" cellspacing="0" width="100%" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false"
                                                   data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="false"
                                                   data-click-to-select="false"
                                                   data-toolbar="#toolbar">
                                                <a href="data:application/xml;charset=utf-8,your code here" class="tbl-add-btn mint-btn-design low-padding mb-1 user-show-btn" download="filename.html">Save</a>
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Title</td>
                                                        <td>${person.title}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Full Name</td>
                                                        <td>${person.fullName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Preferred name (if applicable) 
                                                            Enter preferred first name (optional)</td>
                                                        <td>${person.fullName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Date of birth</td>
                                                        <td>${person.date_of_Birth}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td> Country of residence</td>
                                                        <td>${person.country_residence}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>Occupation</td>
                                                        <td>${person.occupation}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>Are you working with an Investment Adviser?</td>
                                                        <td>${person.working_with_adviser}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>8</td>
                                                        <td>Advisor Company</td>
                                                        <td></td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>9</td>
                                                        <td>Advisor</td>
                                                        <td></td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>10</td>
                                                        <td>Home address</td>
                                                        <td>${person.homeAddress}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>11</td>
                                                        <td>Mobile number</td>
                                                        <td>${person.mobile_country_code} ${person.mobile_number}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>12</td>
                                                        <td>${person.optional_num_type}(Optional)</td>
                                                        <td>${person.optional_num_code} ${person.optional_num}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>13</td>
                                                        <td>Which type of ID are you providing</td>
                                                        <td>${person.id_type} ${person.other_id_type}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>14</td>
                                                        <td>Number</td>
                                                        <td>${person.license_number} ${person.passport_number} </td> 
                                                    </tr>   
                                                    <tr>
                                                        <td>15</td>
                                                        <td>Expiry date</td>
                                                        <td>${person.licence_expiry_Date} ${person.passport_expiry} ${person.other_id_expiry}</td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>16</td>
                                                        <td>Version number</td>
                                                        <td>${person.licence_verson_number}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>17</td>
                                                        <td>Issue By</td>
                                                        <td>${person.passport_issue_by} ${person.other_id_issueBy}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>18</td>
                                                        <td>Prescribed Investors Rate (PIR)</td>
                                                        <td>28%</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>19</td>
                                                        <td>IRD Number</td>
                                                        <td>${person.ird_number}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>20</td>
                                                        <td>Are you a US citizen or US tax resident, or a tax resident in any other country?</td>
                                                        <td>${person.tex_residence_Country}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>21</td>
                                                        <td>Country of tax residence</td>
                                                        <td>${person.tex_residence_Country}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>22</td>
                                                        <td>Tax Identification Number (TIN)</td>
                                                        <td>${person.tin}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>23</td>
                                                        <td> Reason if TIN not available</td>
                                                        <td>${person.resn_tin_unavailable}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>24</td>
                                                        <td>Bank name</td>
                                                        <td>${person.bank_name}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>25</td>
                                                        <td> Account holder name</td>
                                                        <td>${person.acount_holder_name}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>26</td>
                                                        <td> Account number</td>
                                                        <td>${person.account_number}</td> 
                                                    </tr>
                                                </tbody>
                                            </table>    
                                        </c:if>
                                        <c:if test="${not empty joint}">
                                            <table id="individual-person-detail" class="display nowrap table-bordered" cellspacing="0" width="100%" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false"
                                                   data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="false"
                                                   data-click-to-select="false"
                                                   data-toolbar="#toolbar">
                                                <a href="data:application/xml;charset=utf-8,your code here" class="mint-btn-design low-padding mb-1" download="filename.html">Save</a>
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Title</td>
                                                        <td>${joint.title}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Full Name</td>
                                                        <td>${joint.fullName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Preferred name (if applicable) 
                                                            Enter preferred first name (optional)</td>
                                                        <td>${joint.fullName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Date of birth</td>
                                                        <td>${joint.date_of_Birth}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td> Country of residence</td>
                                                        <td>${joint.country_residence}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>Occupation</td>
                                                        <td>${joint.occupation}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>Are you working with an Investment Adviser?</td>
                                                        <td>${joint.working_with_adviser}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>8</td>
                                                        <td>Advisor Company</td>
                                                        <td></td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>9</td>
                                                        <td>Advisor</td>
                                                        <td></td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>10</td>
                                                        <td>Home address</td>
                                                        <td>${joint.homeAddress}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>11</td>
                                                        <td>Mobile number</td>
                                                        <td>${joint.mobile_country_code} ${joint.mobile_number}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>12</td>
                                                        <td>${joint.optional_num_type}(Optional)</td>
                                                        <td>${joint.optional_num_code} ${joint.optional_num}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>13</td>
                                                        <td>Which type of ID are you providing</td>
                                                        <td>${joint.id_type} ${joint.other_id_type}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>14</td>
                                                        <td>Number</td>
                                                        <td>${joint.license_number} ${joint.passport_number} </td> 
                                                    </tr>   
                                                    <tr>
                                                        <td>15</td>
                                                        <td>Expiry date</td>
                                                        <td>${joint.licence_expiry_Date} ${joint.passport_expiry} ${joint.other_id_expiry}</td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>16</td>
                                                        <td>Version number</td>
                                                        <td>${joint.licence_verson_number}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>17</td>
                                                        <td>Issue By</td>
                                                        <td>${joint.passport_issue_by} ${joint.other_id_issueBy}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>18</td>
                                                        <td>Prescribed Investors Rate (PIR)</td>
                                                        <td>${joint.pir}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>19</td>
                                                        <td>IRD Number</td>
                                                        <td>${joint.ird_Number}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>20</td>
                                                        <td>Are you a US citizen or US tax resident, or a tax resident in any other country?</td>
                                                        <td></td> 
                                                    </tr>
                                                    <c:forEach items="${joint.countryTINList}" var="tin"  varStatus="loop">
                                                        <tr>
                                                            <td>21</td>
                                                            <td>Country of tax residence</td>
                                                            <td>${tin.country}</td> 
                                                        </tr>
                                                        <tr>
                                                            <td>22</td>
                                                            <td>Tax Identification Number (TIN)</td>
                                                            <td>${tin.tin}</td> 
                                                        </tr>
                                                        <tr>
                                                            <td>23</td>
                                                            <td> Reason if TIN not available</td>
                                                            <td>${tin.reason}</td> 
                                                        </tr>
                                                    </c:forEach>
                                                    <tr>
                                                        <td>24</td>
                                                        <td>Bank name</td>
                                                        <td>${joint.bank_name}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>25</td>
                                                        <td> Account holder name</td>
                                                        <td>${joint.acount_holder_name}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>26</td>
                                                        <td> Account number</td>
                                                        <td>${joint.account_number}</td> 
                                                    </tr>
                                                    <c:forEach items="${joint.moreInvestorList}" var="investor">
                                                        <tr class="more-investor">
                                                            <td>1</td>
                                                            <td>Title</td>
                                                            <td>${investor.title}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>2</td>
                                                            <td>Full Name</td>
                                                            <td>${investor.fullName}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>4</td>
                                                            <td>Date of birth</td>
                                                            <td>${investor.date_of_Birth}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>5</td>
                                                            <td> Country of residence</td>
                                                            <td>${investor.country_residence}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>6</td>
                                                            <td>Occupation</td>
                                                            <td>${investor.occupation}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>10</td>
                                                            <td>Home address</td>
                                                            <td>${investor.homeAddress}</td> 
                                                        </tr>  
                                                        <tr class="more-investor">
                                                            <td>11</td>
                                                            <td>Mobile number</td>
                                                            <td>${investor.mobile_country_code} ${investor.mobile_number}</td> 
                                                        </tr>  
                                                        <tr class="more-investor">
                                                            <td>12</td>
                                                            <td>${investor.optional_num_type}(Optional)</td>
                                                            <td>${investor.optional_num_code} ${investor.optional_num}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>13</td>
                                                            <td>Which type of ID are you providing</td>
                                                            <td>${investor.id_type} ${investor.other_id_type}</td> 
                                                        </tr>  
                                                        <tr class="more-investor">
                                                            <td>14</td>
                                                            <td>Number</td>
                                                            <td>${investor.license_number} ${investor.passport_number} </td> 
                                                        </tr>   
                                                        <tr class="more-investor">
                                                            <td>15</td>
                                                            <td>Expiry date</td>
                                                            <td>${investor.licence_expiry_Date} ${investor.passport_expiry} ${investor.other_id_expiry}</td> 
                                                        </tr> 
                                                        <tr class="more-investor">
                                                            <td>16</td>
                                                            <td>Version number</td>
                                                            <td>${investor.licence_verson_number}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>17</td>
                                                            <td>Issue By</td>
                                                            <td>${investor.passport_issue_by} ${investor.other_id_issueBy}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>18</td>
                                                            <td>Prescribed Investors Rate (PIR)</td>
                                                            <td>${investor.pir}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>19</td>
                                                            <td>IRD Number</td>
                                                            <td>${investor.ird_Number}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>20</td>
                                                            <td>Are you a US citizen or US tax resident, or a tax resident in any other country?</td>
                                                            <td></td> 
                                                        </tr>
                                                        <c:forEach items="${investor.countryTINList}" var="tin"  varStatus="loop">
                                                            <tr class="more-investor">
                                                                <td>21</td>
                                                                <td>Country of tax residence</td>
                                                                <td>${tin.country}</td> 
                                                            </tr>
                                                            <tr class="more-investor">
                                                                <td>22</td>
                                                                <td>Tax Identification Number (TIN)</td>
                                                                <td>${tin.tin}</td> 
                                                            </tr>
                                                            <tr class="more-investor">
                                                                <td>23</td>
                                                                <td> Reason if TIN not available</td>
                                                                <td>${tin.reason}</td> 
                                                            </tr>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </tbody>
                                            </table>    
                                        </c:if>
                                        <c:if test="${not empty company}">
                                            <table id="individual-person-detail" class="display nowrap table-bordered" cellspacing="0" width="100%" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false"
                                                   data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="false"
                                                   data-click-to-select="false"
                                                   data-toolbar="#toolbar">
                                                <a href="data:application/xml;charset=utf-8,your code here" class="mint-btn-design low-padding mb-1"download="filename.html">Save</a>
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> Name</td>
                                                        <td>${company.companyName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Country of incorporation</td>
                                                        <td>${company.countryOfResidence}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Date of Incorporation</td>
                                                        <td>${company.dateOfInco}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td><c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> Registration Number</td>
                                                        <td>${company.registerNumber}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td> <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> address of incorporation</td>
                                                        <td>${company.companyAddress}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>Postal address (if different from registered address)</td>
                                                        <td>${company.postalAddress}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>Are you working with an Investment Adviser?</td>
                                                        <td>${company.investAdviser}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>8</td>
                                                        <td>Select Advisor Company</td>
                                                        <td>${company.companyAdvisor}</td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>9</td>
                                                        <td>Select an Adviser</td>
                                                        <td>${company.selectAdviser}</td> 
                                                    </tr>  

                                                    <tr>
                                                        <td>10</td>
                                                        <td>What is the PIR rate?</td>
                                                        <td>${company.pir}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>11</td>
                                                        <td><c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> IRD Number</td>
                                                        <td>${company.companyIRDNumber}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>12</td>
                                                        <td>What is the source of funds or wealth for this account?</td>
                                                        <td>${company.sourceOfFundsText}</td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>13</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> a listed issuer?</td>
                                                        <td>${company.isCompanyListed} </td> 
                                                    </tr>  
                                                    <tr>
                                                        <td>14</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> a Government Registered</td>
                                                        <td>${company.isCompanyGovernment}</td> 
                                                    </tr>   
                                                    <tr>
                                                        <td>15</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> the New Zealand Police?</td>
                                                        <td>${company.isCompanyNZPolice} </td> 
                                                    </tr> 
                                                    <tr>
                                                        <td>16</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> a Financial Institution for FATCA or CRS purposes?</td>
                                                        <td>${company.isCompanyFinancialInstitution}</td> 
                                                    </tr>

                                                    <tr>
                                                        <td>17</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> an Active or Passive Non-Financial Entity for FATCA and CRS purposes?</td>
                                                        <td>${company.isCompanyActivePassive}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>18</td>
                                                        <td>Is the <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose> a US citizen or US tax resident, or a tax resident in any other country?</td>
                                                        <td></td> 
                                                    </tr>
                                                    <c:forEach items="${company.countryTINList}" var="tin"  varStatus="loop">
                                                        <tr>
                                                            <td>19</td>
                                                            <td>Country of tax residence</td>
                                                            <td>${company.country}</td> 
                                                        </tr>
                                                        <tr>
                                                            <td>20</td>
                                                            <td>Tax Identification Number (TIN)</td>
                                                            <td>${company.tin}</td> 
                                                        </tr>
                                                        <tr>
                                                            <td>21</td>
                                                            <td> Reason if TIN not available</td>
                                                            <td>${company.reason}</td> 
                                                        </tr>
                                                    </c:forEach>
                                                    <tr>
                                                        <td>22</td>
                                                        <td>Bank name</td>
                                                        <td>${company.bankName}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>23</td>
                                                        <td> Account holder name</td>
                                                        <td>${company.nameOfAccount}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>24</td>
                                                        <td> Account number</td>
                                                        <td>${company.accountNumber}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>25</td>
                                                        <td>Full name of <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trustee/Beneficial Owner</c:when> 
                                                                <c:otherwise>Director/Shareholder</c:otherwise>
                                                            </c:choose></td></td>
                                                        <td>${company.fname}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>26</td>
                                                        <td>Date of birth</td>
                                                        <td>${company.dateOfBirth}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>27</td>
                                                        <td>Occupation</td>
                                                        <td>${company.occupation}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>28</td>
                                                        <td>Country of residence</td>
                                                        <td>${company.holderCountryOfResidence}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>29</td>
                                                        <td>Position in <c:choose> 
                                                                <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                <c:otherwise>Company</c:otherwise>
                                                            </c:choose></td>
                                                        <td>${company.positionInCompany}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>30</td>
                                                        <td>Home address</td>
                                                        <td>${company.address}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>31</td>
                                                        <td>Mobile number (Primary)</td>
                                                        <td>${company.countryCode3} ${company.mobileNo}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>32</td>
                                                        <td>Other Mobile number (${company.otherNumber})</td>
                                                        <td> ${company.otherCountryCode} ${company.otherMobileNo}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>33</td>
                                                        <td>Which type of ID are you providing</td>
                                                        <td>${company.srcOfFund}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>34</td>
                                                        <td>License number</td>
                                                        <td>${company.licenseNumber}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>35</td>
                                                        <td>Expiry Date</td>
                                                        <td>${company.licenseExpiryDate}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>36</td>
                                                        <td>Version number</td>
                                                        <td>${company.versionNumber}</td> 
                                                    </tr>
                                                    <tr>
                                                        <td>37</td>
                                                        <td>IRD number</td>
                                                        <td>${company.irdNumber}</td> 
                                                    </tr>

                                                    <c:forEach items="${company.moreInvestorList}" var="investor">
                                                        <tr class="more-investor">
                                                            <td>1</td>
                                                            <td>Full name of <c:choose> 
                                                                    <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trustee/Beneficial Owner</c:when> 
                                                                    <c:otherwise>Director/Shareholder</c:otherwise>
                                                                </c:choose></td>
                                                            <td>${investor.fname}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>4</td>
                                                            <td>Date of birth</td>
                                                            <td>${investor.dateOfBirth}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>6</td>
                                                            <td>Occupation</td>
                                                            <td>${investor.occupation}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>5</td>
                                                            <td> Country of residence</td>
                                                            <td>${investor.holderCountryOfResidence}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>6</td>
                                                            <td>Position in <c:choose> 
                                                                    <c:when test="${company.reg_type eq 'TRUST_ACCOUNT'}">Trust</c:when> 
                                                                    <c:otherwise>Company</c:otherwise>
                                                                </c:choose></td>
                                                            <td>${investor.positionInCompany}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>7</td>
                                                            <td>Home address</td>
                                                            <td>${investor.address}</td> 
                                                        </tr>  

                                                        <tr class="more-investor">
                                                            <td>8</td>
                                                            <td>Mobile number (Primary)</td>
                                                            <td>${investor.countryCode3} ${investor.mobileNo}</td>  
                                                        </tr>  
                                                        <tr class="more-investor">
                                                            <td>9</td>
                                                            <td>Other Mobile number (${company.otherNumber})</td>
                                                            <td> ${investor.otherCountryCode} ${investor.otherMobileNo}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>10</td>
                                                            <td>Which type of ID are you providing</td>
                                                            <td>${investor.srcOfFund} ${investor.typeOfID}</td> 
                                                        </tr>  
                                                        <tr class="more-investor">
                                                            <td>11</td>
                                                            <td>Number</td>
                                                            <td>${investor.licenseNumber} ${investor.passportNumber} </td> 
                                                        </tr>   
                                                        <tr class="more-investor">
                                                            <td>12</td>
                                                            <td>Expiry date</td>
                                                            <td>${investor.licenseExpiryDate} ${investor.passportExpiryDate} </td> 
                                                        </tr> 
                                                        <tr class="more-investor">
                                                            <td>13</td>
                                                            <td>Version number</td>
                                                            <td>${investor.versionNumber}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>14</td>
                                                            <td>Issue By</td>
                                                            <td>${investor.typeCountryOfIssue} </td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>15</td>
                                                            <td>IRD Number</td>
                                                            <td>${investor.irdNumber}</td> 
                                                        </tr>
                                                        <tr class="more-investor">
                                                            <td>16</td>
                                                            <td>Are you a US citizen or US tax resident, or a tax resident in any other country?</td>
                                                            <td></td> 
                                                        </tr>
                                                        <c:forEach items="${investor.countryTINList}" var="tin"  varStatus="loop">
                                                            <tr class="more-investor">
                                                                <td>21</td>
                                                                <td>Country of tax residence</td>
                                                                <td>${tin.country}</td> 
                                                            </tr>
                                                            <tr class="more-investor">
                                                                <td>22</td>
                                                                <td>Tax Identification Number (TIN)</td>
                                                                <td>${tin.tin}</td> 
                                                            </tr>
                                                            <tr class="more-investor">
                                                                <td>23</td>
                                                                <td> Reason if TIN not available</td>
                                                                <td>${tin.reason}</td> 
                                                            </tr>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </tbody>
                                            </table>    
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <!--    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
            <script src="https://code.highcharts.com/highcharts.js" ></script> 
            <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
            <script src="https://code.highcharts.com/modules/cylinder.js"></script>-->
        <!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> -->

        <script src='https://code.jquery.com/jquery-1.12.3.js'></script>
        <script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
        <script src='https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'></script>
        <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'></script>
        <script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'></script>
        <script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'></script>
        <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'></script>
        <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'></script>
        <script  src="./script.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                var table = document.getElementById('individual-person-detail');
                var rows = table.getElementsByTagName('tr');
                var j = rows.length;
                for (var i = 1; i < j; i++) {
                    var snocell = rows[i].getElementsByTagName('td')[0];
                    snocell.innerHTML = i;
                }
                $('#individual-person-detail').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'excelFlash', 'excel', 'pdf', 'print', {
                            text: 'Reload',
                            action: function (e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        }
                    ]
                });
                //                $('#individual-person-detail td').css('background','red!important');
            });
        </script>


    </body>
</html>