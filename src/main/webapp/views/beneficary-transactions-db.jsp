
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
       <style>
           .content-box {
               background: #F5F9F8;
           }
           .table-scroll table {
               margin: 0;

           }
           .card.mt-3.tab-card {
               background: #f5f9f8;
               border: none;
           }
        </style>
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Transactions</a>
                        </li>
                        <!--                        <li class="breadcrumb-item">
                                                    <span></span>
                                                </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">                                  
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                        </div>
                                        <h6 class="element-header">
                                            CASH & TRANSACTIONS 
                                            <!--<img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">-->
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <a class="fund-weight" href="#pending-scroll" data-scroll="">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">
                                                                PENDING INVESTMENT AMOUNT
                                                            </div>
                                                            <div class="value" id="pendingInvestmentAmount">
                                                                $0.00
                                                            </div>
                                                            <div class="trending trending-down-basic">
                                                                <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                            </div>
                                                        </div></a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a class="fund-weight" href="javascript:void(0)" data-scroll="">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">
                                                                PENDING WITHDRAWAL AMOUNT
                                                            </div>
                                                            <div class="value currentBalance" id="pendingWithdrawalAmount">
                                                                $0.00
                                                            </div>
                                                            <div class="trending trending-down-basic">
                                                                <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <div class="col-sm-4">
                                                    <a class="fund-weight" href="#complete-scroll" data-scroll="">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">
                                                                LAST TRANSACTION
                                                            </div>
                                                            <div class="value currentBalance" id="latestTransaction">
                                                                $0.00
                                                            </div>
                                                            <div class="trending trending-down-basic">
                                                                <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a class="fund-weight" href="javascript:void(0)" data-scroll="">
                                                        <div class="element-box el-tablo">
                                                            <div class="label">
                                                                ACCOUNT STATUS
                                                            </div>
                                                            <div class="value currentBalance">
                                                                Active
                                                            </div>
                                                            <div class="trending trending-down-basic">
                                                                <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 invst-option" id="complete-scroll">
                                    <div class="element-wrapper"> 
                                        <h6 class="element-header">
                                            Completed Transactions
                                        </h6>
                                        <select id='table-filter' class="cash-select">
                                            <option value=''>All</option>
                                            <!--                                            <option value='Mint Australasian Equity Fund'>Mint Australasian Equity Fund</option>
                                                                                        <option value='Mint Australasian Property Fund'>Mint Australasian Property Fund</option>
                                                                                        <option value='Mint Diversified Income Fund'>Mint Diversified Income Fund</option>
                                                                                        <option value='Mint Diversified Growth Fund'>Mint Diversified Growth Fund</option>-->
                                        </select>
                                        <div class="card mt-3 tab-card">
                                            <div class="table-scroll">
                                                <table id="transactions-table" class="table table-striped table-bordered" style="width:100%">

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 invst-option" id="pending-scroll">
                                    <div class="element-wrapper"> 
                                        <h6 class="element-header">
                                            Pending Transactions
                                        </h6>
                                        <select id="table-filterPending" class="cash-select">
                                            <option value=''>All</option>
                                            <!--                                            <option value='Mint Australasian Equity Fund'>Mint Australasian Equity Fund</option>
                                                                                        <option value='Mint Australasian Property Fund'>Mint Australasian Property Fund</option>
                                                                                        <option value='Mint Diversified Income Fund'>Mint Diversified Income Fund</option>
                                                                                        <option value='Mint Diversified Growth Fund'>Mint Diversified Growth Fund</option>-->
                                        </select>
                                        <div class="card mt-3 tab-card">
                                            <div class="table-scroll">
                                                <table id="pending-transactions-table" class="table table-striped table-bordered" style="width:100%">




                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>

        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        <script>
            var PendinginvestmentAmount = 0;
            var PendingwithdrawalAmount = 0;
            var transactionTable;
            var transactionsData = new Array();
            var PendingTransactionsData = new Array();
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/investment-transactions?id=${id}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $(".loader").hide();
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log("all Transactions__----------------" + JSON.stringify(obj.investmentTransactions.EffectiveDate));
                        var transactions = obj.investmentTransactions;
                        var pendingTransactions = obj.pendingTransactions;
                        console.log("all Transactions__2----------------" + JSON.stringify(pendingTransactions));
                        $.each(transactions, function (idx, val) {
                            transactionsData.push([(val.EffectiveDate).split("T")[0], val.PortfolioName.replace("Mint","Invsta"), val.TransactionTypeDescription, val.TypeDisplayName, val.SubType, val.Units, val.Price, val.Value, val.Tax]);
                            if (($('#table-filter option[value="' + val.PortfolioName + '"]').val() === undefined)) {
                                $('#table-filter').append("<option value='" + val.PortfolioName + "'>" + val.PortfolioName + "</option> ");
                            } else {
//    // code to run if it is there
//    
                            }

                        });
                        $.each(pendingTransactions, function (idx, val) {
                            PendingTransactionsData.push([val.created_ts.split(" ")[0], val.investmentcode, val.name, val.beneficiaryName, val.portfolioName, val.amount, val.type, [val.id, val.tabletype]]);
//                            console.log(" Each pending Transaction " + PendingTransactionsData);
                            if (($('#table-filterPending option[value="' + val.portfolioName + '"]').val() === undefined)) {
                                $('#table-filterPending').append("<option value='" + val.portfolioName + "'>" + val.portfolioName + "</option> ");
                            } else {
//    // code to run if it is there
//    
                            }
                        });
                        if (transactions.length > 0) {
                            transactionTable = $('#transactions-table').DataTable({
                                data: transactionsData,
                                columns: [
                                    {title: "Effective Date"},
                                    {title: "Portfolio Name"},
                                    {title: "Transaction Type Description"},
                                    {title: "Type Display Name"},
                                    {title: "Sub Type"},
                                    {title: "Units"},
                                    {title: "Price"},
                                    {title: "Value"},
                                    {title: "Tax"}
                                ],
                                order: [[0, "desc"]]

                            });
                            //                  
                        } else {
                            $('#table-filter').hide();
                        }
                        //                      alert(pendingTransactions.length);
                        if (pendingTransactions.length > 0) {
                            PendingTransactionTable = $('#pending-transactions-table').DataTable({
                                data: PendingTransactionsData,
                                columns: [
                                    {title: "Date"},
                                    {title: "Investment Code"},
                                    {title: "Name"},
                                    {title: "Beneficiary Name"},
                                    {title: "Portfolio Name"},
                                    {title: "Amount"},
                                    {title: "Type"},
                                    {title: ""}
                                ],
                                columnDefs: [{
//                                        "data": i,
//                                        "defaultContent": '<a href="" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Delete</strong></span></a>',
                                        "targets": -1,
//                                    href = "javascript:void(0)"
//                                        href = "./cancelTransection-' + data + '
                                        mRender: function (data, type, row) {
                                            return '<a   href = "javascript:void(0)" data-id="' + data + '"  class="btn btn-info cancel-btn  cancelTransaction" onclick="CancelTransaction(this);"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Cancel</strong></span> </a>';
                                        }
                                    }],
                                order: [[0, "desc"]]
                            });
                        } else {
                            $('#table-filterPending').hide();
                        }
                        SetTransaction(transactions, pendingTransactions);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
            function SetTransaction(transactions, pendingTransactions) {
                $.each(pendingTransactions, function (idx, val) {
                    if (val.amount !== "" && val.amount !== null) {
                        if (val.type === "Investment") {
                            PendinginvestmentAmount = PendinginvestmentAmount + parseInt(val.amount);
                            $('#pendingInvestmentAmount').text("$" + PendinginvestmentAmount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        } else {
                            PendingwithdrawalAmount = PendingwithdrawalAmount + parseInt(val.amount);
                            $('#pendingWithdrawalAmount').text("$" + PendingwithdrawalAmount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        }
                    }

                });
                if (transactions.length > 0 && pendingTransactions.length > 0) {
                    var latestCompleteTransactionDate = new Date(transactions[0].EffectiveDate);
                    var latestPendingTransactionDate = new Date(pendingTransactions[0].created_ts);
                    if (latestCompleteTransactionDate.getTime() > latestPendingTransactionDate.getTime()) {
                        $('#latestTransaction').text("$" + transactions[0].Value);
                    } else {
                        $('#latestTransaction').text("$" + pendingTransactions[0].amount);
                    }
                } else if (transactions.length > 0) {
                    $('#latestTransaction').text("$" + transactions[0].Value);
                } else if (pendingTransactions.length > 0) {
                    $('#latestTransaction').text("$" + pendingTransactions[0].amount);
                }
            }
            $('#table-filter').on('change', function () {
                transactionTable.search(this.value).draw();
            });
            $('#table-filterPending').on('change', function () {
                PendingTransactionTable.search(this.value).draw();
            });
        </script>
        <script>
            jQuery(document).ready(function ($) {
                function scrollToSection(event) {
                    event.preventDefault();
                    var $section = $($(this).attr('href'));
                    $('html, body').animate({
                        scrollTop: $section.offset().top - 90
                    }, 500);
                }
                $('[data-scroll]').on('click', scrollToSection);
            }(jQuery));
        </script>
        <script>

            function  CancelTransaction(e) {
//               alert("inside this");
                var url = "";
                var activityId = $(e).data('id');
//            alert("inside this " + activityId);
                var data = activityId.split(',');
//           alert(data);
//           alert(data[0]);
//           alert(data[1]);
                if (data[1] === 'pending_transaction') {
                    url = './cancelTransection-' + data[0];
//            alert(url);
                } else {
                    url = './cancelInvestment-' + data[0];
//            alert(url);
                }
                $.confirm({
                    title: 'CANCEL TRANSACTION?',
                    content: 'Click Proceed To Cancel the transaction.',
                    autoClose: 'cancel|8000',
//                autoClose: false,
                    buttons: {
                        deleteUser: {
                            text: 'Proceed',
                            action: function () {
//                            alert("url is" + url);
//                            var activityId = $(this).data('id');

                                $.ajax({
                                    type: 'GET',
                                    url: url,
                                    //                    data: JSON.stringify(obj),
                                    success: function (data, textStatus, jqXHR) {
//                                    alert(' deleted sucessfully');
                                        var table = $('#pending-transactions-table').DataTable();
                                        table.row($(e).parents('tr'))
                                                .remove()
                                                .draw();
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
//                                    alert(textStatus);
                                    }
                                });
//                            $.alert('Deleted the transaction!' + activityId);
                            }
                        },
                        cancel: function () {
//                        $.alert('action is canceled');
                        }
                    }
                });
            }
            ;


        </script>
    </body>
</html>