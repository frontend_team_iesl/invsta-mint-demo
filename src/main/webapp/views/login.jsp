 
<%-- 
    Document   : login
    Created on : Jul 24, 2019, 4:16:48 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="home" value="${pageContext.request.contextPath}"/>

<html lang="en">
    <head>
        <title>Login V2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link href="./resources/favicon.png" rel="shortcut icon"/>
        <!--===============================================================================================-->	
        <!--<link rel="icon" type="image/png" href="./resources/js/login-js/images/icons/favicon.ico"/>-->
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/fonts/iconic/css/material-design-iconic-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animate.css"> 
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/util.css">
        <link rel="stylesheet" type="text/css" href="./resources/css/login-css/main.css">
        <!--===============================================================================================-->
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/sweetalert.css" rel="stylesheet"/>
        <link href="./resources/css/main1.css" rel="stylesheet"/>
        <style>
            .login100-form-bgbtn {
                background: linear-gradient(to right, #2c94ec, #304bb7);
            } 
            .focus-input100::before {
                background: linear-gradient(to right, #2c94ec, #304bb7);
            }
            .btn-show-pass {
                color: #2c94ec;
            }
            .btn-show-pass:hover {
                color: #65b9ac;
            }
             .forgetbutton {
                padding: 2%;
            }
            .forgetbutton:hover {
                padding-left: 2%;
                border-radius: 15px;
                background-color: #65b9ac;
            }
            #f2 .wrap-login100-form-btn {
                height: 50px;
                color: white;
                text-transform: uppercase;
                transition: .4s;
            }
            #f2 .wrap-login100-form-btn:hover {
                color: white;
                background: #54565a;
            }
            #f2 input#Email1 {
                padding: 0;
            }
            #f2 .wrap-login100 {
                padding: 77px 55px 77px 55px;
            }
        </style>
    </head>
    <body style="padding:0px!important">

        <div class="limiter">
            <div class="container-login100">
                <fieldset id="f12">
                    <div class="wrap-login100">
                        <form class="login100-form validate-form" action="<c:url value='/j_spring_security_check'/>" id="loginForm" method="POST">
                            <!--                        <span class="login100-form-title p-b-26">
                                                        Welcome
                                                    </span>-->
                            <span class="login100-form-title p-b-48">
                                <!--<i class="zmdi zmdi-font"></i>-->
                                <img src="resources/images/mint.png">
                            </span>
                            <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                                <lable class="enter-details">Email</lable>
                                <input  class="input100"  type="email" id="Email" placeholder="Enter email" name="username" required />
                                <span class="focus-input100" data-placeholder=""></span>
                            </div>

                            <div class="wrap-input100 validate-input" data-validate="Enter password">
                                <lable class="enter-details">Password</lable>
                                <span class="btn-show-pass">
                                    <i class="zmdi zmdi-eye"></i>
                                </span>
                                <input  class="input100" type="password" id="password" placeholder="Enter password" name="password" required />
                                <!--                            <input class="input100" type="password" name="pass">-->
                                <span class="focus-input100" data-placeholder=""></span>
                            </div>

                            <div class="container-login100-form-btn">
                                <div class="wrap-login100-form-btn">
                                    <div class="login100-form-bgbtn"></div>
                                    <button id="logbtn" class="login100-form-btn">
                                        Login
                                    </button>
                                </div>
                            </div>
                            <%--<c:if test="${message  eq  ''}">--%>
                            <!--one 2345-->
                            <%--</c:if>--%>
                            <!--<div class="text-center p-t-115">-->
                            <!--message-->
                            <!--</div>-->

                            <div class="text-center p-t-50">
                                <span class="txt1">
                                    Don't have an account?
                                </span>

                                <a class="txt2" id="txt2" href="./signUpPath">
                                    Sign Up
                                </a><br>
                                <a id="forgetbtn" href="#">Forget Password</a>
                            </div>
                        </form>
                    </div>
                </fieldset>
               
                         
                            <fieldset id="f2" style="display: none">
                    <div class="wrap-login100">  
                        <form class="login100-form validate-form" action="./forgetPassword" id="loginForm1" method="POST">
                            <span class="login100-form-title p-b-48">
                                <img src="resources/images/mint.png">
                            </span>
                            <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                                <lable class="enter-details">Enter you Email</lable>
                                <input  class="input100"  type="email" id="Email1" placeholder="Enter email" name="hiddenEmail" />
                                <span class="focus-input100" data-placeholder=""></span>
                            </div>
                            <div class="container-login100-form-btn">
                                <div class="wrap-login100-form-btn">
                                    <div class="login100-form-bgbtn"></div>
                                    <button  class="wrap-login100-form-btn">
                                        Send reset link
                                    </button>
                                </div>
                            </div>
                            <div class="text-center p-t-50">
                                <span class="txt1">
                                    Don't have an account?
                                </span>
                                <a class="txt2" id="txt21" href="./signUpPath">
                                    Sign Up
                                </a>
                            </div>
                        </form>
<!--                                                    <form action="./forgetPassword" method="POST">
                                                    <input type="hidden" id="hiddenEmail" name="hiddenEmail" value="">
                                                    <input type="submit" id="forgetbtn" value="Forget Password">
                                                </form>-->
                    </div>
                </fieldset>
                 
            </div>
        </div>


        <div id="dropDownSelect1"></div>

        <!--===============================================================================================-->
        <!--===============================================================================================-->
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="./resources/js/login-js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/popper.js"></script>
        <script src="./resources/js/login-js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/moment.min.js"></script>
        <script src="./resources/js/login-js/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="./resources/js/login-js/main.js"></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
        <!--<script>-->
        <!--            $('#txt2').click(function () {
                        $('#txt2').attr("href", "./signUpPath");
                    });-->
        <!--</script>-->
        <script>
            $(document).ready(function () {
                $("#f2").hide();
                if (${not empty message}) {
                    swal({
                        title: '${title}',
                        text: '${message}',
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
                if (${not empty user.role}) {
                    if ('${user.role}' === "SUBMISSION_COMPANY" || '${user.role}' === "SUBMISSION_TRUST") {
                        swal({
                            title: "Info",
                            text: "Thank you for providing your application to us. You will be able to successfully login to your account post successful verification of your application. We will notify you through a confirmation email once this process is completed.",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }

            });

        </script>
         <script>
            $("#logbtn").click(function () {
                var a = $("#Email").val();
                $("#Email1").val(a);
//                alert(a);
            });
            $("#forgetbtn").click(function () {
                var a = $("#Email").val();
                $("#hiddenEmail").val(a);
                $("#f12").hide();
                $("#f2").show();
//                               alert(a);
            });
        </script>
<!--        <script>
            $("#forgetbtn").click(function() {
               var a = $("#Email").val();
               $("#hiddenEmail").val(a);
                             
});
        </script>-->
    </body>
</html>

