<script type="text/javascript">

    $(document).ready(function () {
        checkComm();//check communication
        // Reset the timer
        window.onload = resetTimer;  // on load of tab
        window.onmousemove = resetTimer;  // catches mouse move as well
        window.onmousedown = resetTimer;  // catches touchscreen presses as well      
        window.ontouchstart = resetTimer; // catches touchscreen swipes as well 
        window.onclick = resetTimer;      // catches touchpad clicks as well
        window.onkeypress = resetTimer;
        window.addEventListener('scroll', resetTimer, true); // improved; see comments
    });
    // Set timeout variables.
    var warningTime = 60000 * 10;//Display swal after 10 minutes;
    var timeoutTime = 60000 * 15;//Display swal again after 5 minute warningTime
    var sessionTimeoutUrl = './session?timeout=true'; // URL to session expired page.
    var warningTimer, timeoutTimer;

// Reset timers.
    function resetTimer() {
//        checkComm(); 
        var currenttime = new Date().getTime();
        window.localStorage.setItem("lastupdate", parseInt(currenttime / 1000));
        clearTimeout(warningTimer);
        clearTimeout(timeoutTimer);
        warningTimer = setTimeout("IdleWarning()", warningTime);
        timeoutTimer = setTimeout("IdleTimeout()", timeoutTime);
    }

    checkComm = function () {
        $.ajax({
            type: 'GET',
            url: './rest/groot/db/api/check-communication',
            headers: {"Content-Type": 'application/json'},
            success: function (data) {
                console.log('successfully checked communication');
                window.localStorage.setItem("lastupdate", parseInt(data / 1000));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error');
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

// Show idle timeout warning dialog.
    function IdleWarning() {
        var time = window.localStorage.getItem("lastupdate");
        var currenttime = new Date().getTime();
        if (parseInt(currenttime / 1000) >= parseInt(time) + 600) {
            swal({
                title: "Warning",
                text: "No activity has done from 10 minutes. So It will be session timeout If you will not do any activity.",
//                  timer: 3500,
                showConfirmButton: true
            });
        }
        // Add code in the #timeout element to call ResetTimeOutTimer() if
        // the "Stay Logged In" button is clicked
    }

// Logout the user.
    function IdleTimeout() {
        var time = window.localStorage.getItem("lastupdate");
        var currenttime = new Date().getTime();
        if (parseInt(currenttime / 1000) >= parseInt(time) + 900) {

//            swal({
//                title: "Warning",
//                text: "No activity has done from 15 minutes. Session has been expired due to no activity.",
//                showConfirmButton: true
//            });
            window.location = sessionTimeoutUrl;
        }
    }

</script>