<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel wrapper-again ">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${portfolioDetail.fundName}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                </div>
                <div class="row payment-page-style">
                    <div class="col-md-12 ">
                        <div class="content_background">
                        <div class="investment-form">
                            <form id="msform">
                                <fieldset id="step1">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-4">
                                                                    </div>
                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Frequency</span>
                                                                    <span>Amount </span>
                                                                    <span>Payment Type</span>
                                                                    <span>Review</span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text animated fadeIn">
                                                                <div class="row">
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="form-group">
                                                                                                                                                <label>Beneficiary Name :</label><br>
                                                                                                                                            </div>
                                                                                                                                        </div>-->
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="form-group">
                                                                                                                                                <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                                                                                                                    <option value="select">-Select-</option>
                                                                    <c:forEach items="${beneficiaryDetails}" var="beneficiary">
                                                                        <option value="${beneficiary.getId()}">${beneficiary.getName()}</option>
                                                                    </c:forEach> 
                                                                </select>
                                                                <span class="error" id="error-beneficiaryId"></span>
                                                            </div>
                                                        </div>-->
                                                                    <div class="col-md-12">
                                                                        <div class="sell-info">
                                                                            <p>Investment Name :</p>
                                                                            <p class="InvName"></p>
                                                                            <!--<input type="text" class="removerror" name ="investmentName" pattern="Enter Beneficary Name" placeholder="Enter Investment Name"/>-->
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <div class="body-section">
                                                                            <h6 class="body-data">Select Payment Type:</h6>
                                                                        </div>
                                                                        <div class="selct-info-btn">

                                                                            <a href="javascript:void(0)" class="next-off chkdiv">
                                                                                <input type="radio" name="invType" class="invType " value="ONE_OFF" style="display: none;"/>
                                                                                <div class="on-btn payment-hover">
                                                                                    <img src="./resources/images/icon-2.png">
                                                                                    <h6 class="icon-name">One off investment </h6>
                                                                                </div>
                                                                            </a>
                                                                            <a href="javascript:void(0)" class="next1 chkdiv">
                                                                                <input type="radio" name="invType" class="invType" value="REGULAR" style="display: none;"/>
                                                                                <div class="on-btn payment-hover">
                                                                                    <img src="./resources/images/icon-1.png">
                                                                                    <h6 class="icon-name">Regular investment  </h6>
                                                                                </div>
                                                                            </a>
                                                                            <a href="javascript:void(0)" class="next-regular chkdiv">
                                                                                <input type="radio" name="invType" class="invType " value="ONE_OFF_REGULAR" style="display: none;"/>
                                                                                <div class="on-btn both-btn payment-hover">
                                                                                    <img src="./resources/images/icon-2.png">
                                                                                    <h6 class="icon-name">One off + regular investment </h6>
                                                                                </div>
                                                                            </a>


                                                                        </div> 
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </fieldset>
                                <fieldset id="step21">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot ">
                                                                    </div>
                                                                    <div class="line-dot progress-1 ">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-4">
                                                                    </div>
                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Frequency</span>
                                                                    <span>Amount</span>
                                                                    <span>Payment Type</span>
                                                                    <span>Review</span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text backgroud-add animated fadeIn">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="body-section">
                                                                            <img src="./resources/images/icon-2.png">
                                                                            <h6 class="body-data">One Off</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">


                                                                        <div class="row">
                                                                            <div class="sell-info">
                                                                                <div class="col-md-6">
                                                                                    <p>Enter the amount you want to invest now:</p>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" class="removerror removedata investedamount" name="investmentName" value="${investmentLaterDetails.investedAmount}" placeholder="Enter $ amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                                    <span class="error" id="error-amount"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">

                                                                        <div class="payment-btn1 btn-select1 ">
                                                                            <a href="javascript:void(0)" class="btn_payment previous-off payment-hover" id="">Back</a>
                                                                            <a href="javascript:void(0)" class="btn_payment1  next21 payment-hover" id="">Next</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step2">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot progress-1 ">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-4">
                                                                    </div>
                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Frequency </span>
                                                                    <span>Amount </span>
                                                                    <span>Payment Type</span>
                                                                    <span>Review </span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text backgroud-add animated fadeIn">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="body-section">
                                                                            <img src="./resources/images/icon-1.png">
                                                                            <h6 class="body-data">Regular Investment </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="payment-regular add-ragular">
                                                                                                                                                <p>Initial Amount</p> 
                                                                                                                                                <input type="text" class="lumpsum removedata" value="${investmentLaterDetails.investedAmount}" placeholder="Enter Amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                                                                                                <span class="error" id="error-initial"></span></div>
                                                                                                                                        </div>-->
                                                                    <div class="col-md-8">
                                                                        <div class="payment-regular add-ragular">
                                                                            <p>Regular investment amount</p> 
                                                                            <input type="text" class="regularamount removedata" value="${investmentLaterDetails.regularAmount}" placeholder="Enter $ amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                            <span class="error" id="error-regular"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="payment-regular add-ragular border-dlt">
                                                                            <p>Frequency</p> 
                                                                            <select class="selectfrequecy">

                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Weekly'}">selected </c:if> value="Weekly">Weekly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Fortnightly'}">selected </c:if> value="Fortnightly">Fortnightly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Monthly'}">selected </c:if> value="Monthly">Monthly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Quarterly'}">selected </c:if> value="Quarterly">Quarterly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'SemiAnnually'}">selected </c:if> value="SemiAnnually">SemiAnnually</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Annually'}">selected </c:if> value="Annually">Annually</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="debit-data">
                                                                            <div class="col-md-12 mb-3">
                                                                                <h6 class="sell-content">All direct debit payments are processed on the 20th of each month.</h6>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="payment-regular add-ragular">
                                                                                    <p>Select start month</p> 
                                                                                    <select class="selectdate">
                                                                                        <option  value="December">20th December 2019</option>
                                                                                        <option  value="January">20th January 2020</option>
                                                                                        <option  value="February">20th February 2020</option>
                                                                                        <option  value="March">20th March 2020</option>
                                                                                        <option  value="April">20th April 2020</option>
                                                                                        <option  value="May">20th May 2020</option>
                                                                                    </select>
                                                                                    <!--<input type="text" class="regularamount removedata selectDate dob1" value="${investmentLaterDetails.startDate}" name="startDate" id="startDate" placeholder="dd/mm/yyyy">-->
                                                                                <!--<span class="error" id="error-startDate"></span>-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="payment-regular add-ragular border-dlt">
                                                                                <p>Enter final payment date</p> 
                                                                                <select class="selectfinal">
                                                                                    <option  value="January"> January 2020</option>
                                                                                    <option  value="February"> February 2020</option>
                                                                                    <option  value="March"> March 2020</option>
                                                                                    <option  value="April"> April 2020</option>
                                                                                    <option  value="May"> May 2020</option>
                                                                                    <option  value="December"> June 2020</option>
                                                                                </select>
                                                                            <!--<input type="text" class="regularamount removedata selectDate dob1"  value="${investmentLaterDetails.endDate}"  name="startDate" id="endDate" placeholder="dd/mm/yyyy">-->
                                                                                <!--<span class="error" id="error-endDate"></span>-->
                                                                            </div>
                                                                        </div> 
                                                                    </div>
                                                                    <div class="col-md-12">

                                                                        <div class="payment-btn1 btn-select1 ">
                                                                            <a href="javascript:void(0)" class="btn_payment previous2 payment-hover" id="">Back</a>
                                                                            <a href="javascript:void(0)" class="btn_payment1  next2 payment-hover" id="">Next</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step31">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot progress-1 ">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-4">
                                                                    </div>
                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Frequency </span>
                                                                    <span>Amount </span>
                                                                    <span>Payment Type</span>
                                                                    <span>Review </span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text backgroud-add animated fadeIn">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="body-section">
                                                                            <img src="./resources/images/icon-1.png">
                                                                            <h6 class="body-data">One Off + Regular Investment </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="payment-regular add-ragular">
                                                                                                                                                <p>Initial Amount</p> 
                                                                                                                                                <input type="text" class="lumpsum removedata" value="" placeholder="Enter Amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                                                                                                <span class="error" id="error-initial"></span></div>
                                                                                                                                        </div>-->
                                                                    <div class="col-md-6">
                                                                        <div class="payment-regular add-ragular">
                                                                            <p>Initial investment amount</p> 
                                                                            <input type="text" class="regularamount removedata in-amount" value="${investmentLaterDetails.regularAmount}" placeholder="Enter $ amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                            <span class="error" id="error-initial-offregular"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="payment-regular add-ragular">
                                                                            <p>Regular investment amount</p> 
                                                                            <input type="text" class="regularamount removedata re-amount" value="${investmentLaterDetails.regularAmount}" placeholder="Enter $ amount" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || (event.charCode == 46)'>
                                                                            <span class="error" id="error-offregular"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="payment-regular add-ragular border-dlt">
                                                                            <p>Frequency</p> 
                                                                            <select class="selectfrequecy">

                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Weekly'}">selected </c:if> value="Weekly">Weekly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Fortnightly'}">selected </c:if> value="Fortnightly">Fortnightly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Monthly'}">selected </c:if> value="Monthly">Monthly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Quarterly'}">selected </c:if> value="Quarterly">Quarterly</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'SemiAnnually'}">selected </c:if> value="SemiAnnually">SemiAnnually</option>
                                                                                <option  <c:if test="${investmentLaterDetails.selectfrequecy eq 'Annually'}">selected </c:if> value="Annually">Annually</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <!--                                                                        <div class="col-md-6">
                                                                                                                                                    <div class="payment-regular add-ragular">
                                                                                                                                                        <p>Start Date</p> 
                                                                                                                                                        <input type="text" class="regularamount removedata selectDate dob1" value="${investmentLaterDetails.startDate}" name="startDate" id="startDate" placeholder="dd/mm/yyyy">
                                                                                                                                                    <span class="error" id="error-startDate"></span>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-md-6">
                                                                                                                                                <div class="payment-regular add-ragular border-dlt">
                                                                                                                                                    <p>End Date</p> 
                                                                                                                                                    <input type="text" class="regularamount removedata selectDate dob1"  value="${investmentLaterDetails.endDate}"  name="startDate" id="endDate" placeholder="dd/mm/yyyy">
                                                                                                                                                    <span class="error" id="error-endDate"></span></div>
                                                                                                                                            </div>-->
                                                                    <div class="col-md-12">

                                                                        <div class="payment-btn1 btn-select1 ">
                                                                            <a href="javascript:void(0)" class="btn_payment previous32 payment-hover" id="">Back</a>
                                                                            <a href="javascript:void(0)" class="btn_payment1  next32 payment-hover" id="">Next</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step3">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12 box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot progress-2">
                                                                    </div>
                                                                    <div class="line-dot progress-1 ">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-4">
                                                                    </div>
                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Frequency </span>
                                                                    <span>Amount </span>
                                                                    <span>Payment Type</span>
                                                                    <span>Review </span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text backgroud-add animated fadeIn">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="body-section">
                                                                            <h5 class="text-center " style="text-align:left!important">How would you like to pay?</h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="body-bank radoi-set">
                                                                            <div class="bank-credit">
                                                                                <label class="bank-radio nextcredit">
                                                                                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                    <span class="bank-card">Direct Credit</span>
                                                                                    <p class="bank-card-text direct-credit-text">We'll provide bank account details for you to make an online direct credit payment to. It usually takes 1 - 2 days for us to receive your payment. </p>
                                                                                    <input type="radio" name="directamount"  class="directamount" value="CREDIT" <c:if test="${investmentLaterDetails.paymentMethod eq 'CREDIT'}">checked </c:if>>
                                                                                        <input type="hidden" class="notifyId" value="" />

                                                                                        <span class="checkmark"></span>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="bank-debit">
                                                                                    <label class="bank-radio next3">
                                                                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                        <span class="bank-card">Direct Debit</span>
                                                                                        <p class="bank-card-text direct-debit-text">invsta Initiated - Usually Takes 3-4 Business Days</p>
                                                                                        <input type="radio" name="directamount" class="directamount" value="DEBIT">
                                                                                        <span class="checkmark"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="payment-btn1 btn-select1 ">
                                                                                <a href="javascript:void(0)" class="btn_payment previous3 payment-hover" id="">Back</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step4">
                                        <div class="row">
                                            <div class="col-md-12 thanku">
                                                <div class="form-data">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 box-payment-space">
                                                                <div class="line-progress">
                                                                    <div class="line-bar2">
                                                                        <div class="line-dot">
                                                                        </div>
                                                                        <div class="line-dot progress-3">
                                                                        </div>
                                                                        <div class="line-dot progress-1 ">
                                                                        </div>
                                                                        <div class="line-dot progress-2">
                                                                        </div>
                                                                        <div class="line-dot add-dot dot-show-3">
                                                                        </div>
                                                                        <div class="line-dot add-dot dot-show-4">
                                                                        </div>
                                                                    </div>
                                                                    <div class="line-data">
                                                                        <span>Frequency </span>
                                                                        <span>Amount </span>
                                                                        <span>Payment Type</span>
                                                                        <span>Review</span>
                                                                        <span>Confirmation</span>
                                                                    </div>
                                                                </div>
                                                                <div class="input-content-text animated fadeIn bank-user">

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="body-section">
                                                                                <h6 class="body-data">Select which bank account the direct debit payment to come from</h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                    <c:forEach items="${bankAccountDetails}" var="bank">
                                                                        <div class="col-md-4 continue oneoffdiv selectBank">

                                                                            <a href="javascript:void(0)" class="next4">
                                                                                <input type="radio" name="accountId" class="accountid" data-id="${bank.getBankAccountId()}" value="${bank.getId()}" style="display: none;"/>
                                                                                <!--                                                                                <div class="on-btn">
                                                                                                                                                                    <img src="./resources/images/icon-2.png">
                                                                                                                                                                    <h6 class="icon-name">One Off</h6>
                                                                                                                                                                </div>-->
                                                                                <input type="hidden" class="bankCode" value="${bank.getBank()}">
                                                                                <input type="hidden" class="bankaccountnum" value="${bank.getAccount()}">
                                                                                <input type="hidden" class="bankaccountname" value="${bank.getAccountName()}">
                                                                                <input type="hidden" class="branhnum" value="${bank.getBranch()}">
                                                                                <div class="body-box">
                                                                                    <p><b>Existing account on file:</b></p>
                                                                                    <span class="flag-data">${bank.getCurrency()} Account</span>
                                                                                    <p>ending with <span class="add-nmbr">${fn:substring(bank.getAccount(), 2, 6)}</span></p>
                                                                                    <p>${user.name}</p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-md-4 continue regulardiv selectBank">
                                                                            <a href="javascript:void(0)" class="next4">
                                                                                <input type="radio" name="accountId" class="accountid" data-id="${bank.getBankAccountId()}" value="${bank.getId()}" style="display: none;"/>
                                                                                <!--                                                                                <div class="on-btn">
                                                                                                                                                                    <img src="./resources/images/icon-1.png">
                                                                                                                                                                    <h6 class="icon-name">Regular</h6>
                                                                                                                                                                </div>-->
                                                                                <input type="hidden" class="bankCode" value="${bank.getBank()}">
                                                                                <input type="hidden" class="bankaccountnum" value="${bank.getAccount()}">
                                                                                <input type="hidden" class="bankaccountname" value="${bank.getAccountName()}">
                                                                                <input type="hidden" class="branhnum" value="${bank.getBranch()}">
                                                                                <div class="body-box">
                                                                                    <p><b>Existing account on file:</b></p>
                                                                                    <span class="flag-data">${bank.getCurrency()} Account</span>
                                                                                    <p>ending with <span class="add-nmbr">${fn:substring(bank.getAccount(), 2, 6)}</span></p>
                                                                                    <p>${user.name}</p>
                                                                                </div>
                                                                            </a>
                                                                        </div>                                                                    
                                                                    </c:forEach>

                                                                    <div class="col-md-4 continue">
                                                                        <a href="javascript:void(0)" class="next-debit">
                                                                            <div class="body-box">
                                                                                <div class="body-icon">
                                                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                                                    <h6 class="icon-name">Enter New Account</h6>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="payment-btn1 btn-select1 ">
                                                                            <a href="javascript:void(0)" class="btn_payment previous4 payment-hover" id="">Back</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step5">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="form-data">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 box-payment-space">
                                                                <div class="line-progress">
                                                                    <div class="line-bar2">
                                                                        <div class="line-dot">
                                                                        </div>
                                                                        <div class="line-dot progress-3">
                                                                        </div>
                                                                        <div class="line-dot progress-1 ">
                                                                        </div>
                                                                        <div class="line-dot progress-2">
                                                                        </div>
                                                                        <div class="line-dot add-dot dot-show-3">
                                                                        </div>
                                                                        <div class="line-dot add-dot dot-show-4">
                                                                        </div>
                                                                    </div>
                                                                    <div class="line-data">
                                                                        <span>Frequency </span>
                                                                        <span>Amount </span>
                                                                        <span>Payment Type</span>
                                                                        <span>Review</span>
                                                                        <span>Confirmation</span>
                                                                    </div>
                                                                </div>
                                                                <div class="input-content-text backgroud-add animated fadeIn">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5 class="body-data">Payment Details </h5>
                                                                            <div class="trans-bank-2">

                                                                                <h6 class="">To make this <span class="oneregulartext">new investment of</span> <span class="payment-amount"> $x,xxx</span> into the ${fundBankAccountDetails.portfolioName}, <span class="headingformail">please visit your online banking and arrange to pay your investment amount into the bank account given below, using the reference details provided.  </span> </h6>
<!--                                                                                <h6 class="body-data portfolioName">${fundBankAccountDetails.portfolioName}:</h6>-->
                                                                                <!--<a href="">Edit</a>-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="trans-detail-2">
                                                                                <div class="trans-bank-2">
                                                                                    <h6 class="body-data">Bank account details for payment</h6>
                                                                                    <input type="hidden" id="notifyId" value="${investmentLaterDetails.notifyId}">
                                                                                    <!--                                                                                                    <a href="">Edit</a>-->
                                                                                </div>
                                                                                <div class="trans-data-2">
                                                                                    <span class="first-span">Account Name</span> 
                                                                                    <span class="second-span bankCreditAccountName">${fundBankAccountDetails.accountName} </span>
                                                                                </div>
                                                                                <div class="trans-data-2">
                                                                                    <span class="first-span">Account Number</span> 
                                                                                    <span class="referance-copy">
                                                                                        <span class="second-span bankCreditAccountNum">${fundBankAccountDetails.accountNumber}</span>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary copy-link"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="trans-data-2">
                                                                                    <span class="first-span">Bank Name</span> 
                                                                                    <span class="second-span">ANZ Bank New Zealand</span>
                                                                                </div>
                                                                                <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">Branch</span> 
                                                                                                                                                                    <span class="second-span">New Zealand</span>
                                                                                                                                                                </div>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="trans-detail-2">
                                                                                <div class="trans-bank-2">
                                                                                    <h6 class="body-data">Reference details for payment</h6>
                                                                                    <!--                                                                                                    <a href="">Edit</a>-->
                                                                                </div>
                                                                                <div class="trans-data-2">
                                                                                    <span class="first-span">Reference Number </span> 
                                                                                    <span class="referance-copy">
                                                                                        <span class="second-span randomnumber"></span>
                                                                                        <a href="javascript:void(0)" class="btn btn-primary copy-link"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="trans-detail-2">
                                                                                <!--                                                                                <div class="trans-bank-2">
                                                                                                                                                                    <h6 class="body-data">Particulars</h6>
                                                                                                                                                                    <a href="">Edit</a>
                                                                                                                                                                </div>-->
                                                                                <div class="trans-data-2">
                                                                                    <span class="first-span">Particulars </span> 
                                                                                    <span class="second-span full-name"></span>
                                                                                </div>
                                                                                <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">First Name </span> 
                                                                                                                                                                    <span class="second-span first-name"></span>
                                                                                                                                                                </div>-->
                                                                                <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">Last Name</span> 
                                                                                                                                                                    <span class="second-span lastName">James</span>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">First Name</span> 
                                                                                                                                                                    <span class="second-span firstName">Peter</span>
                                                                                                                                                                </div>-->
                                                                                <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">Date</span> 
                                                                                                                                                                    <span class="second-span">Tuesday, 22 October 2019</span>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="trans-data-2">
                                                                                                                                                                    <span class="first-span">Time</span> 
                                                                                                                                                                    <span class="second-span">20:19:37</span>
                                                                                                                                                                </div>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="instruction-btn">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="tree-btn">
                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style payment-click paymentDone" data-id="Pending">I've made payment</a>
                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style payment-click paymentLater" data-id="PAYMENT_LATER">I'll make payment later</a>

                                                                                </div>

                                                                            </div>
                                                                            <div class="col-md-12 mt-3">
                                                                                <div class="payment-btn1 btn-select1 ">
                                                                                    <a href="javascript:void(0)" class="btn_payment previous5 payment-hover" id="">Back</a>
                                                                                    <a href="javascript:void(0)" class="btn_payment1  next5 payment-hover confirm-investment disable-invest" id="" >Confirm Investment  </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step6">
                                    <div class="content-section">
                                        <div class="row">
                                            <div class="col-md-12 thanku">
                                                <div class="form-data">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="line-progress">
                                                                        <div class="line-bar2">
                                                                            <div class="line-dot">
                                                                            </div>
                                                                            <div class="line-dot progress-3">
                                                                            </div>
                                                                            <div class="line-dot progress-1 ">
                                                                            </div>
                                                                            <div class="line-dot progress-2">
                                                                            </div>
                                                                            <div class="line-dot add-dot dot-show-3">
                                                                            </div>
                                                                            <div class="line-dot add-dot dot-show-4">
                                                                            </div>
                                                                        </div>
                                                                        <div class="line-data">
                                                                            <span>Frequency </span>
                                                                            <span>Amount </span>
                                                                            <span>Payment Type</span>
                                                                            <span>Review</span>
                                                                            <span>Confirmation</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="input-content-text backgroud-add animated fadeIn">

                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="trans-detail-2">
                                                                                    <div class="trans-bank-2">
                                                                                        <h6 class="body-data">Use Existing Account</h6>
                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                    </div>
                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span">Account Number</span> 
                                                                                        <span class="referance-copy">
                                                                                            <span class="second-span accountnumspan"></span>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary copy-link"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span">Bank Name</span> 
                                                                                        <span class="second-span accountnamespan">ANZ Bank New Zealand</span>
                                                                                    </div>
                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span">Branch</span> 
                                                                                        <span class="second-span branhnumspan">New Zealand</span>
                                                                                    </div>
                                                                                    <!--                                                                                    <div class="trans-data-2">
                                                                                                                                                                            <h6 class="body-data">Particulars</h6>
                                                                                                                                                                        </div>-->
                                                                                    <!--                                                                                    <div class="trans-data-2">
                                                                                                                                                                            <span class="first-span">Last Name</span> 
                                                                                                                                                                            <span class="second-span last-name"></span>
                                                                                                                                                                        </div>
                                                                                    -->                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span ">Particulars</span> 
                                                                                        <span class="second-span full-name"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="trans-detail-2">
                                                                                    <div class="trans-bank-2">
                                                                                        <h6 class="body-data">Reference Details for Payment</h6>
                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                    </div>
                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span">Reference Number</span>
                                                                                        <span class="referance-copy">
                                                                                            <span class="second-span randomnumber"></span>
                                                                                            <a href="javascript:void(0)" class="btn btn-primary copy-link"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="trans-detail-2">
                                                                                    <div class="trans-bank-2">
                                                                                        <h6 class="body-data">Particulars</h6>
                                                                                        <!--                                                                                                    <a href="">Edit</a>-->
                                                                                    </div>
                                                                                    <div class="trans-data-2">
                                                                                        <span class="first-span">Particulars</span> 
                                                                                        <span class="second-span full-name"></span>
                                                                                    </div>
                                                                                    <!--                                                                                    <div class="trans-data-2">
                                                                                                                                                                            <span class="first-span ">First Name</span> 
                                                                                                                                                                            <span class="second-span first-name"></span>
                                                                                                                                                                        </div>-->
                                                                                    <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                        <span class="first-span">Last Name</span> 
                                                                                                                                                                        <span class="second-span lastName">James</span>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="trans-data-2">
                                                                                                                                                                        <span class="first-span">First Name</span> 
                                                                                                                                                                        <span class="second-span firstName">Peter</span>
                                                                                                                                                                    </div>-->
                                                                                    <!--                                                                                <div class="trans-data-2">
                                                                                                                                                                        <span class="first-span">Date</span> 
                                                                                                                                                                        <span class="second-span">Tuesday, 22 October 2019</span>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="trans-data-2">
                                                                                                                                                                        <span class="first-span">Time</span> 
                                                                                                                                                                        <span class="second-span">20:19:37</span>
                                                                                                                                                                    </div>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 mt-3">
                                                                                <div class="payment-btn1 btn-select1">
                                                                                    <a href="javascript:void(0)" class="previous6 btn_payment payment-hover" id="">Back</a>
                                                                                    <a href="javascript:void(0)" class="next6 btn_payment1 payment-hover" id="">Next</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </fieldset>
                                <fieldset id="step7">
                                    <div class="content-section">
                                        <div class="row">
                                            <div class="col-md-12 thanku">
                                                <div class="form-data">
                                                    <div class="form-data">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="line-progress">
                                                                        <div class="line-bar2">
                                                                            <div class="line-dot">
                                                                            </div>
                                                                            <div class="line-dot progress-3">
                                                                            </div>
                                                                            <div class="line-dot progress-1 ">
                                                                            </div>
                                                                            <div class="line-dot progress-2">
                                                                            </div>

                                                                            <div class="line-dot add-dot dot-show-3">
                                                                            </div>
                                                                            <div class="line-dot add-dot dot-show-4">
                                                                            </div>
                                                                        </div>
                                                                        <div class="line-data">
                                                                            <span>Frequency </span>
                                                                            <span>Amount </span>
                                                                            <span>Payment Type</span>
                                                                            <span>Review</span>
                                                                            <span>Confirmation</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="input-content-text backgroud-add animated fadeIn">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-12"> 
                                                                                    <div class="debit-content_body only-debit-form">
                                                                                        <div class="debit-content-section">
                                                                                            <div class="row">
                                                                                                <div class="col-md-7">
                                                                                                    <div class="text-data">
                                                                                                        <h6>Direct Debit Instruction:</h6>
                                                                                                        <p>Details of the bank account from which the payment is to be mode:</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-5">
                                                                                                    <div class="debit-bank-name digit-1">
                                                                                                        <h6>Authorisation Code</h6>
                                                                                                        <input type="text" id="pincode-input1" class="form-control" placeholder="0" maxlength="" value="0618754">
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                        <input type="text" class="form-control" id="accountholdername" placeholder="Account Holder Name" maxlength="20">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">

                                                                                                </div>
                                                                                                <!--                                                                                                <div class="col-md-6">
                                                                                                                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                                                                                                                        <select class="selectoption form-group aml-select bank_name" id="bankName">
                                                                                                                                                                                                        <option value="No Bank Select yet">–Select–</option>
                                                                                                                                                                                                        <option value="Bank of New Zealand" data-id="02">Bank of New Zealand</option>
                                                                                                                                                                                                        <option value="ANZ Bank New Zealand" data-id="01">ANZ Bank New Zealand</option>
                                                                                                                                                                                                        <option value="ASB Bank" data-id="12">ASB Bank</option>
                                                                                                                                                                                                        <option value="Westpac" data-id="03">Westpac</option>
                                                                                                                                                                                                        <option value="Heartland Bank" data-id="03">Heartland Bank</option>
                                                                                                                                                                                                        <option value="Kiwibank" data-id="38">Kiwibank</option>
                                                                                                                                                                                                        <option value="SBS Bank" data-id="03">SBS Bank</option>
                                                                                                                                                                                                        <option value="TSB Bank" data-id="15">TSB Bank</option>
                                                                                                                                                                                                        <option value="The Co-operative Bank" data-id="02">The Co-operative Bank</option>
                                                                                                                                                                                                        <option value="NZCU" data-id="03">NZCU</option>
                                                                                                                                                                                                        <option value="Rabobank New Zealand" data-id="03">Rabobank New Zealand</option>
                                                                                                                                                                                                        <option value="National Bank of New Zealand" data-id="03">National Bank of New Zealand</option>
                                                                                                                                                                                                        <option value="National Australia Bank" data-id="08">National Australia Bank</option>
                                                                                                                                                                                                        <option value="Industrial and Commercial Bank of China" data-id="10">Industrial and Commercial Bank of China</option>
                                                                                                                                                                                                        <option value="PostBank" data-id="11">PostBank</option>
                                                                                                                                                                                                        <option value="Trust Bank Southland" data-id="13">Trust Bank Southland</option>
                                                                                                                                                                                                        <option value="Trust Bank Otago" data-id="14">Trust Bank Otago</option>
                                                                                                                                                                                                        <option value="Trust Bank Canterbury" data-id="16">Trust Bank Canterbury</option>
                                                                                                                                                                                                        <option value="Trust Bank Waikato" data-id="17">Trust Bank Waikato</option>
                                                                                                                                                                                                        <option value="Trust Bank Bay of Plenty" data-id="18">Trust Bank Bay of Plenty</option>
                                                                                                                                                                                                        <option value="Trust Bank South Canterbury" data-id="19">Trust Bank South Canterbury</option>
                                                                                                                                                                                                        <option value="Trust Bank Auckland" data-id="21">Trust Bank Auckland</option>
                                                                                                                                                                                                        <option value="Trust Bank Central" data-id="20">Trust Bank Central</option>
                                                                                                                                                                                                        <option value="Trust Bank Wanganui" data-id="22">Trust Bank Wanganui</option>
                                                                                                                                                                                                        <option value="Westland Bank" data-id="24">Westland Bank</option>
                                                                                                                                                                                                        <option value="Trust Bank Wellington" data-id="23">Trust Bank Wellington</option>
                                                                                                                                                                                                        <option value="Countrywide" data-id="25">Countrywide</option>
                                                                                                                                                                                                        <option value="United Bank" data-id="29">United Bank</option>
                                                                                                                                                                                                        <option value="HSBC" data-id="30">HSBC</option>
                                                                                                                                                                                                        <option value="Citibank" data-id="31">Citibank</option>
                                                                                                                                                                                                        <option value="Other">Other</option>
                                                                                                                                                                                                        </select>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </div>-->
                                                                                                <div class="col-md-6 bank-margin">
                                                                                                    <div class="bank-img-icon">
                                                                                                        <ul class="prod-gram">
                                                                                                            <li class="init">–Select–</li>
                                                                                                            <li><img src='./resources/images/bank/BNZ.png' />Bank of New Zealand</li>
                                                                                                            <li><img src='./resources/images/bank/anz.png' />ANZ Bank New Zealand</li>
                                                                                                            <li><img src='./resources/images/bank/ASB.jpg' />ASB Bank</li>
                                                                                                            <li><img src='./resources/images/bank/Westpac.png' />Westpac</li>
                                                                                                            <li><img src='./resources/images/bank/heartland.png' />Heartland Bank</li>
                                                                                                            <li><img src='./resources/images/bank/Kiwibank.png' />Kiwibank</li>
                                                                                                            <li><img src='./resources/images/bank/sbs.jpg' />SBS Bank</li>
                                                                                                            <li><img src='./resources/images/bank/TSB.jpg' />TSB Bank</li>
                                                                                                            <li><img src='./resources/images/bank/Co-operative.jpg' />The Co-operative Bank</li>
                                                                                                            <li><img src='./resources/images/bank/NZCU.png' />NZCU</li>
                                                                                                            <li><img src='./resources/images/bank/Rabobank.jpg' />Rabobank New Zealand</li>
                                                                                                            <li><img src='./resources/images/bank/national.png' />National Bank of New Zealand</li>
                                                                                                            <li><img src='./resources/images/bank/nab.jpg' />National Australia Bank</li>
                                                                                                            <li><img src='./resources/images/bank/ICBC.png' />Industrial and Commercial Bank of China</li>
                                                                                                            <li><img src='./resources/images/bank/PostBank.jpg' />PostBank</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Southland</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Otago</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Canterbury</li>
                                                                                                            <li><img src='./resources/images/bank/trust-waikato.jpg' />Trust Bank Waikato</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Bay of Plenty</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank South Canterbury</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Auckland</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Central</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wanganui</li>
                                                                                                            <li><img src='./resources/images/bank/westland.png' />Westland Bank</li>
                                                                                                            <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wellington</li>
                                                                                                            <li><img src='./resources/images/bank/countrywide.jpg' />Countrywide</li>
                                                                                                            <li><img src='./resources/images/bank/united.jpg' />United Bank</li>
                                                                                                            <li><img src='./resources/images/bank/HSBC.jpg' />HSBC</li>
                                                                                                            <li><img src='./resources/images/bank/city-bank.png' />Citibank</li>
                                                                                                            <li>Other</li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="debit-bank-name name-bank">
                                                                                                        <input type="text" class="form-control" id="accountNumber" placeholder="Bank Account Number"  onkeypress="checkAccountNO()">
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <div class="debit-bank-name digit">
                                                                                                        <h6>Payer Particulars</h6>
                                                                                                        <input type="text" id="pincode-input2" class="form-control" placeholder="" maxlength="" value="MINT">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="debit-bank-name digit">
                                                                                                        <h6>Payer Code</h6>
                                                                                                        <input type="text" id="pincode-input3" class="form-control" placeholder="" maxlength="" value="INVESTMENTS">
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12">
                                                                                                    <div class="debit-bank-name digits">
                                                                                                        <h6>Payer Reference</h6>
                                                                                                        <input type="text" id="pincode-input4" class="form-control" placeholder="" maxlength="">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div class="debit-bank-pera">
                                                                                                        <p>I/We authorize until further notice to debit my account with all amounts with the authorization code for direct debit.I/ We acknowledge and accept that the bank accepts the authority only upon the conditions listed below.</p>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12">
                                                                                                    <div class="debit-bank-pera acpt-btn">
                                                                                                        <a href="javascript:void(0)" class="btn btn-primary btn-styl debit-own-btn next7">Confirm</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div class="term-value">
                                                                                                        <h5>CLIENT TERMS AND CONDITIONS</h5>
                                                                                                        
                                                                                                        <ol>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                <ol>
                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                </ol>
                                                                                                            </li>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                            <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                                                                                <ol>
                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</li>
                                                                                                                </ol>
                                                                                                            </li>
                                                                                                        </ol>
                                                                                                       
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 mt-3">
                                                                                                    <div class="payment-btn1 btn-select1">
                                                                                                        <a href="javascript:void(0)" class="btn_payment payment-hover previous7">BACK</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                            </fieldset>
                                            <fieldset id="step8">
                                                <div class="row">
                                                    <div class="col-md-12 thanku">
                                                        <div class="form-data">
                                                            <div class="form-data">
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-md-12 box-payment-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar2">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                    <div class="line-dot progress-4">
                                                                                    </div>
                                                                                    <div class="line-dot progress-1 ">
                                                                                    </div>
                                                                                    <div class="line-dot progress-2">
                                                                                    </div>
                                                                                    <div class="line-dot progress-3">
                                                                                    </div>
                                                                                    <div class="line-dot add-dot dot-show-4">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Frequency </span>
                                                                                    <span>Amount </span>
                                                                                    <span>Payment Type</span>
                                                                                    <span>Review</span>
                                                                                    <span>Confirmation</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="input-content-text animated fadeIn">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="trans-detail-2">
                                                                                            <div class="trans-bank-2">
                                                                                                <h6 class="body-data payment-thanks">Thanks</h6>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    </div>
                                    </div>
                                    <div class="display-type"></div>
                                    <jsp:include page="footer.jsp"/>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
                                    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
                                    <script src="https://code.highcharts.com/highcharts.js" ></script> 
                                    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
                                    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
                                    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
                                    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
                                    <script type="text/javascript" src="resources/js/bootstrap-pincode-input.js"></script>
                                    <script src="./resources/js/sweetalert.min.js"></script>
                                    <script>
                                                                                                            var Payment = '';
                                                                                                            var notifyId = null;
                                                                                                            var investmentId = '';
                                                                                                            $(document).ready(function () {
                                                                                                                var date = new Date();
                                                                                                                var day = date.getDate();
                                                                                                                var month = date.getMonth();
                                                                                                                var year = date.getFullYear();
                                                                                                                adultDOB = date.setFullYear(year - 18, month, day);
                                                                                                                if ('${investmentLaterDetails}' !== '') {
                                                                                                                    investmentId = '${investmentLaterDetails.id}';
                                                                                                                    $(".previous5").hide();
                                                                                                                    $(".paymentLater").hide();
                                                                                                                    $("#step1").hide();
                                                                                                                    $("#step5").show();
                                                                                                                }

                                                                                                                $('#pincode-input2').pincodeInput({hidedigits: false, inputs: 12});
                                                                                                                $('#pincode-input1').pincodeInput({hidedigits: false, inputs: 7});
                                                                                                                $('#pincode-input3').pincodeInput({hidedigits: false, inputs: 12});
                                                                                                                $('#pincode-input4').pincodeInput({hidedigits: false, inputs: 25});
                                                                                                                $(".dob1").datepicker({
                                                                                                                    yearRange: (year) + ':' + (year + 80),
                                                                                                                    changeMonth: true,
                                                                                                                    changeYear: true,
                                                                                                                    'minDate': new Date(),
                                                                                                                    dateFormat: 'dd/mm/yy'
                                                                                                                }).datepicker().attr('readonly', 'readonly');
                                                                                                            });
                                    </script>

                                    <script>
                                        $(".calculator-dropdown ul").on("click", ".init", function () {
                                            $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
                                        });
                                        var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
                                        $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                                            allOptions.removeClass('selected');
                                            $(this).addClass('selected');
                                            $(".calculator-dropdown ul").children('.init').html($(this).html());
                                            allOptions.toggle();
                                        });
                                    </script>
                                    <script>
                                        $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                                            $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
                                        });
                                        var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
                                        $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                                            allOption.removeClass('selected');
                                            $(this).addClass('selected');
                                            $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                                            allOption.toggle();
                                        });
                                    </script>
                                    <script>
                                        $(document).ready(function () {
//                                            var randomnum = Math.floor((Math.random() * 10000) + 1);
                                            if ('${ic}' !== '') {
                                                $('.randomnumber').text('${ic}');
                                            } else if ('${user.investmentCode}' !== '') {
                                                $('.randomnumber').text('${user.investmentCode}');
                                            }
                                            $(".data-toggle").hide();
                                            $(".loader").fadeOut("slow");
                                            $(".see-toggle").click(function () {
                                                $(".data-toggle").toggle();
                                            });
                                            $("#continue").click(function () {
                                                window.location.href = './regularmethod';
                                            });
                                            $('.selectBank').click(function () {
                                                $('.selectBank').removeClass('selectedbank');
                                                $(this).addClass('selectedbank');
                                            });
                                            $('#beneficiaryId').change(function () {
                                                //                                                $('.bankaccountdiv').hide();
                                                //                                                $('.bankaccounthide').hide();
                                                var beneficiaryval = $('#beneficiaryId option:selected').text();
                                                $('#investmentName').val(beneficiaryval);
                                                $('.error').text("");
                                                //                                                var beneficiaryid = $(this).val();
                                                //                                                var beneficiarvals = document.getElementsByClassName('beneficiarid');
                                                //                                                for (var i = 0; i < beneficiarvals.length; i++) {
                                                //                                                    var beneficiarval = beneficiarvals[i];
                                                //                                                    var root = $(beneficiarval).closest('.bankaccountdiv');
                                                //                                                    if (beneficiarval.value === beneficiaryid) {
                                                //                                                        root.show();
                                                //                                                        $('.bankaccounthide').show();
                                                //                                                    }
                                                //                                                }
                                            });
                                            //
                                            //                                            var date = new Date();
                                            //                                            var day = date.getDate();
                                            //                                            var month = date.getMonth();
                                            //                                            var year = date.getFullYear();
                                            //                                            var selectDate = date.setFullYear(year - 18, month, day);
                                            //                                            $(".selectDate").datepicker({
                                            //                                                yearRange: (year) + ':' + (year + 80),
                                            //                                                changeMonth: true,
                                            //                                                changeYear: true,
                                            //                                                'minDate': new Date(),
                                            //                                                dateFormat: 'dd/mm/yy'
                                            //                                            }).datepicker().attr('readonly', 'readonly');

                                        });
                                    </script>
                                    <script>
                                        <c:if test="${not empty investmentLaterDetails.investedAmount}">
                                        $('.payment-amount').html('$' + '${investmentLaterDetails.investedAmount}');
                                        </c:if>
                                        $('.payment-click').click(function () {
                                            $('.payment-click').addClass('transparent-style');
                                            $(this).removeClass('transparent-style');
                                            Payment = $(this).data('id');
                                            $('.confirm-investment').removeClass('disable-invest');
                                        });
                                        var BeneId;
                                        var InvName;
                                        var InvCode = '${user.investmentCode}';
                                        <c:forEach items="${beneficiaryDetails}" var="beneficiary">
                                        BeneId = '${beneficiary.getId()}';
                                        InvName = '${beneficiary.getName()}';
                                        $('.InvName').text(InvName);
                                        </c:forEach>
                                        $(".removerror").click(function () {
                                            $('.error').text("");
                                        });
                                        $(".removerror").change(function () {
                                            $('.error').text("");
                                        });
                                        $(".previous2").click(function () {
                                            $('.removedata').val("");
                                            $("#step2").hide();
                                            $("#step3").show();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".previous32").click(function () {

                                            $('.removedata').val("");
                                            $("#step31").hide();
                                            $("#step1").show();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".previous-off").click(function () {
                                            $('.removedata').val("");
                                            $("#step21").hide();
                                            $("#step1").show();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".previous3").click(function () {
                                        <%--<c:choose>--%>
                                        <%--<c:when test="${investmentLaterDetails.investmentType eq 'ONE_OFF'}">--%>
//                                            $("#step3").hide();
//                                            $("#step21").show();
                                        <%--</c:when>--%>
                                        <%--<c:when test="${investmentLaterDetails.investmentType eq 'REGULAR'}">--%>
//                                            $("#step3").hide();
//                                            $("#step2").show();
                                        <%--</c:when>--%>
                                        <%--<c:when test="${investmentLaterDetails.investmentType eq 'ONE_OFF_REGULAR'}">--%>
//                                            $("#step3").hide();
//                                            $("#step31").show();
                                        <%--</c:when>--%>
                                        <%--</c:choose>--%>
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "ONE_OFF") {
                                                $("#step3").hide();
                                                $("#step21").show();
                                            } else if (invtype === "REGULAR") {
                                                $("#step3").hide();
                                                $("#step1").show();
                                            } else if (invtype === "ONE_OFF_REGULAR") {
                                                $("#step3").hide();
                                                $("#step2").hide();
                                                $("#step31").show();
                                            }
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".previous4").click(function () {
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "REGULAR") {
                                                $("#step4").hide();
                                                $("#step2").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            } else {
                                                $("#step4").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }

                                        });
                                        $(".previous5").click(function () {
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "REGULAR") {
                                                $("#step5").hide();
                                                $("#step2").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            } else {
                                                $("#step5").hide();
                                                $("#step3").show();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }


                                        });
                                        $(".previous6").click(function () {
                                            $("#step6").hide();
                                            $("#step4").show();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".previous7").click(function () {
                                            $("#step7").hide();
                                            $("#step4").show();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        checkedRadios = function (evt) {

                                        };
                                        $(".next1").click(function (evt) {
                                            $('.oneregulartext').html("new investment of");
                                            $(".bank-debit").show();
                                            $('.direct-credit-text').html(" You select your payment date and frequency. We'll provide bank account details for you to make an online direct credit and regular automatic payment to. It usually takes 1 - 2 days for us to receive your payment. ");
                                            $('.direct-debit-text').html("All direct debit payments are monthly, and are processed on the 20th of each month. You'll need to complete an online direct debit form. It can take up to 14 days for direct debit requests to be processed. ");
                                            checkedRadios(evt);
                                            var invName = $("#investmentName").val();
                                            if (invName === "") {
                                                $("#error-investmentName").text("This field is required");
                                            } else {
                                                $("#step3").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        });
                                        $(".next-regular").click(function (evt) {
                                            checkedRadios(evt);
                                            $('.oneregulartext').html("one off and regular investment");
                                            $('.payment-amount').html("");
                                            var invName = $("#investmentName").val();
                                            if (invName === "") {
                                                $("#error-investmentName").text("This field is required");
                                            } else {
                                                $("#step31").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        });
                                        $(".next-off").click(function (evt) {
                                            checkedRadios(evt);
                                            $('.oneregulartext').html("new investment of");
                                            var invName = $("#investmentName").val();
                                            if (invName === "") {
                                                $("#error-investmentName").text("This field is required");
                                            } else {
                                                $("#step21").show();
                                                $("#step1").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        });
                                        $(".next2").click(function () {
                                            var reg_amount = $('.regularamount').val();
                                            $('.payment-amount').html('$' + reg_amount);
                                            var initialamount = $('.lumpsum').val();
                                            var regularamount = $('.regularamount').val();
                                            var startDate = $('#startDate').val();
                                            var endDate = $('#endDate').val();
                                            var paymentmethod = $('input[name=directamount]:checked').val();
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "REGULAR") {
                                                $('.bank-debit').show();
                                                $('.direct-credit-text').html(" You select your payment date and frequency. We'll provide bank account details for you to make an online direct credit and regular automatic payment to. It usually takes 1 - 2 days for us to receive your payment. ");
                                                $('.direct-debit-text').html("All direct debit payments are monthly, and are processed on the 20th of each month. You'll need to complete an online direct debit form. It can take up to 14 days for direct debit requests to be processed. ");
                                            }
                                            if (initialamount === "") {
                                                $('#error-initial').text("This field is required");
                                            } else if (regularamount === "") {
                                                $('#error-regular').text("This field is required");
                                            } else if (startDate === "") {
                                                $('#error-startDate').text("This field is required");
                                            } else if (endDate === "") {
                                                $('#error-endDate').text("This field is required");
                                            } else if (paymentmethod === "CREDIT") {
                                                $("#step5").show();
                                                $("#step2").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            } else if (paymentmethod === "DEBIT") {
                                                $("#step4").show();
                                                $("#step2").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        });
                                        $(".next32").click(function () {
                                            var initialamount = $('.in-amount').val();
                                            var regularamount = $('.re-amount').val();
//                                            var startDate = $('#startDate').val();
//                                            var endDate = $('#endDate').val();
//                                        $('.payment-amount').html('$' + initialamount);
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "ONE_OFF_REGULAR") {
                                                $('.bank-debit').hide();
                                                $('.direct-credit-text').html("You select your payment date and frequency. We'll provide bank account details for you to make an online direct credit and regular automatic payment to. It usually takes 1 - 2 days for us to receive your payment.");
                                                $('.headingformail').html(" visit your online banking and set up an initial direct credit, and an automatic payment  into the bank account details given below, using the reference details provided.  ");
                                            }
                                            if (initialamount === "") {
                                                $('#error-initial-offregular').text("This field is required");
                                            } else if (regularamount === "") {
                                                $('#error-offregular').text("This field is required");
//                                            } else if (startDate === "") {
//                                            $('#error-startDate').text("This field is required");
//                                            } else if (endDate === "") {
//                                            $('#error-endDate').text("This field is required");
                                            } else {
                                                $("#step3").show();
                                                $("#step31").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        });
                                        $(".next21").click(function () {
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "ONE_OFF") {
                                                $('.bank-debit').hide();
                                                $('.direct-credit-text').html(" We'll provide bank account details for you to make an online direct credit payment to. It usually takes 1 - 2 days for us to receive your payment. ");
                                                $('.headingformail').html(" please visit your online banking and arrange to pay your investment amount into the bank account given below, using the reference details provided. ");
                                            }
                                            var investedamount = $('.investedamount').val();
//                                            if (investedamount === "") {
//                                                $('#error-amount').text("This field is required");
//                                            } else {
                                                $("#step3").show();
                                                $("#step21").hide();
                                                $("html, body").animate({scrollTop: 0}, "fast");
//                                            }
                                        });
                                        $(".next3").click(function () {
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "REGULAR") {
                                                $('.headingformail').html(" please visit your online banking and arrange to pay your investment amount into the bank account given below, using the reference details provided. ");
                                                $(".debit-data").show();
                                                $("#step2").show();
                                                $("#step3").hide();
                                            } else {
                                                $("#step4").show();
                                                $("#step3").hide();
                                            }

                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".next4").click(function () {
                                            var accountnum = $(this).find('.bankaccountnum').val();
//                                            var accountname = $(this).find('.bankaccountname').val();
                                            var branhnum = $(this).find('.branhnum').val();
                                            $('.accountnumspan').text(accountnum);
//                                            $('.accountnamespan').text(accountname);
                                            $('.branhnumspan').text(branhnum);
                                            $("#step6").show();
                                            $("#step4").hide();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".nextcredit").click(function () {
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "REGULAR") {
                                                $(".debit-data").hide();
                                                $('.direct-credit-text').html(" You select your payment date and frequency. We'll provide bank account details for you to make an online direct credit and regular automatic payment to. It usually takes 1 - 2 days for us to receive your payment. ");
                                                $('.direct-debit-text').html("All direct debit payments are monthly, and are processed on the 20th of each month. You'll need to complete an online direct debit form. It can take up to 14 days for direct debit requests to be processed. ");
                                                $("#step2").show();
                                                $("#step3").hide();
                                            } else {
                                                $("#step5").show();
                                                $("#step3").hide();
                                            }
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".next5").bind('click', function () {
                                            $("#step8").show();
                                            $("#step5").hide();
                                            $("html, body").animate({scrollTop: 0}, "fast");
//                                            Payment = "Pending";
                                            notifyId = $("#notifyId").val();
                                            saveInvestment(notifyId);
                                        });
                                        $(".next6").bind('click', function () {
                                            $("#step8").show();
                                            $("#step6").hide();
                                            $("html, body").animate({scrollTop: 0}, "fast");
//                                            Payment = "Pending";
                                            saveInvestment(notifyId);
                                        });
                                        $(".next7").click(function () {
                                            $("#step8").show();
                                            $("#step7").hide();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            setBankAccount();
                                        });
                                        $(".next-debit").click(function () {
                                            $("#step7").show();
                                            $("#step4").hide();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        });
                                        $(".chkdiv").click(function () {
                                            $('.invType').removeAttr('checked');
                                            $(this).find('.invType').attr('checked', 'checked');
                                        });
                                        $(".removedata").click(function () {
                                            $('.error').text("");
                                        });
                                        $(".removedata").change(function () {
                                            $('.error').text("");
                                        });
                                        $(".bank-radio").click(function () {
                                            $('.directamount').removeAttr('checked');
                                            $(this).find('.directamount').attr('checked', 'checked');
                                            var amount = $('.investedamount').val();
                                            var offregamount = $('.in-amount').val();
                                            var reg_amount = $('.regularamount').val();
                                            var invtype = $('input[name=invType]:checked').val();
                                            if (invtype === "ONE_OFF") {
                                                $('.payment-amount').html('$' + amount);
                                                $('.regulardiv').hide();
                                                $('.oneoffdiv').show();
                                            } else if (invtype === "REGULAR") {
                                                $('.payment-amount').html('$' + reg_amount);
                                                $('.regulardiv').show();
                                                $('.oneoffdiv').hide();
                                            } else if (invtype === "ONE_OFF_REGULAR") {
//                                        $('.payment-amount').html('$' + offregamount);  
                                                $('.regulardiv').show();
                                                $('.oneoffdiv').hide();
                                            }
                                        });
                                        function checkAccountNO() {
                                            var ac = document.getElementById("accountNumber").value;
                                            if (ac.length === 2) {
                                                var ac20 = ac.substr(0, 2);
                                                var a = ac20 + '-';
                                                $("#accountNumber").val(a);
                                            }
                                            if (ac.length === 7) {
                                                var ac21 = ac.substr(7);
                                                var b = ac + '-' + ac21;
                                                $("#accountNumber").val(b);
                                            }
                                            if (ac.length === 15) {
                                                var ac212 = ac.substr(15);
                                                var c = ac + '-' + ac212;
                                                $("#accountNumber").val(c);
                                            }
                                            if (ac.length > 18) {
                                                var ac23 = ac.substr(0, 18);
                                                var d = ac23;
                                                $("#accountNumber").val(d);
                                            }
                                        }
                                        $('.bank_name').change(function () {
                                            var data = $('.bank_name option:selected').data('id');
                                            $('#accountNumber').val(data);
                                        });
                                        function saveInvestment(notifyId) {
                                            var invtype = $('input[name=invType]:checked').val();
                                            var paymentmethod = $('input[name=directamount]:checked').val();
                                            var bankAccountId;
                                            var bankAccountNumber;
                                            var bankAccountName;
                                            var bankId;
                                            var id = investmentId;
                                            var notifyId = notifyId;
                                            var payment = '';
                                            ;
                                            if (Payment === null || Payment === '') {
                                                payment = "Pending";
                                            } else {
                                                payment = Payment;
                                            }
                                            var investmentName = InvName;
//                                            var investmentCode = InvCode;
                                            var investedamount = '';
                                            var beneficiaryId = BeneId;
                                            var fundName = $(".portfolioName").val();
                                            var applicationId;
                                            if ('${applicationId}' !== '') {
                                                applicationId = '${applicationId}';
                                            }
                                            var fundCode = '${pc}';
                                            var regularamount = '';
                                            var startDate = '';
                                            var endDate = '';
                                            var investmentCode = '';
                                            var selectfrequecy = '';
                                            var bankCode = '';
                                            var bankName = '';
                                            if (invtype === "ONE_OFF" && paymentmethod === "CREDIT") {
                                                bankAccountNumber = $('.bankCreditAccountNums').text();
                                                bankAccountName = $('.bankCreditAccountName').text();
                                                if (bankCode === "") {
                                                    bankName = "ANZ Bank New Zealand";
                                                }
                                                investedamount = $('.investedamount').val();
                                            } else if (invtype === "REGULAR" && paymentmethod === "DEBIT") {
                                                bankAccountId = $('.selectedbank').find('.accountid').val();
                                                bankId = $('.selectedbank').find('.accountid').data("id");
                                                bankAccountNumber = $('.selectedbank').find('.bankaccountnum').val();
                                                bankAccountName = $('.selectedbank').find('.bankaccountname').val();
                                                bankCode = $('.selectedbank').find('.bankCode').val();
//                                                investedamount = $('.lumpsum').val();
                                                startDate = $('.selectdate option:selected').val();
                                                endDate = $('.selectfinal option:selected').val();
                                                regularamount = $('.regularamount').val();
                                                investedamount = regularamount;
                                                selectfrequecy = $('.selectfrequecy option:selected').val();
                                            } else if (invtype === "REGULAR" && paymentmethod === "CREDIT") {
                                                bankAccountNumber = $('.bankCreditAccountNum').text();
                                                bankAccountName = $('.bankCreditAccountName').text();
                                                if (bankCode === "") {
                                                    bankName = "ANZ Bank New Zealand";
                                                }
//                                                investedamount = $('.lumpsum').val();
                                                startDate = $('.selectdate option:selected').val();
                                                endDate = $('.selectfinal option:selected').val();
                                                regularamount = $('.regularamount').val();
                                                investedamount = regularamount;
                                                selectfrequecy = $('.selectfrequecy option:selected').val();
                                            } else if (invtype === "ONE_OFF_REGULAR" && paymentmethod === "CREDIT") {
                                                bankAccountNumber = $('.bankCreditAccountNum').text();
                                                bankAccountName = $('.bankCreditAccountName').text();
                                                if (bankCode === "") {
                                                    bankName = "ANZ Bank New Zealand";
                                                }
                                                investedamount = $('.in-amount').val();
//                                                startDate = $('#startDate').val();
//                                                endDate = $('#endDate').val();
                                                regularamount = $('.re-amount').val();
                                                selectfrequecy = $('.selectfrequecy option:selected').val();
                                            }
                                            if ('${ic}' !== "") {
                                                investmentCode = '${ic}';
                                            } else {
                                                investmentCode = InvCode;
                                            }

                                            $(".next5").unbind('click');
                                            $(".next6").unbind('click');
                                            var obj = {id: id, applicationId: applicationId, investmentType: invtype, regularAmount: regularamount, selectfrequecy: selectfrequecy, investmentName: investmentName, investmentCode: investmentCode,
                                                status: payment, notifyId: notifyId, bankCode: bankCode, bankName: bankName,
                                                investedAmount: investedamount, portfolioName: fundName, portfolioCode: fundCode, beneficiaryId: beneficiaryId, bankApiId: bankId, bankAccountNumber: bankAccountNumber, bankAccountName: bankAccountName,
                                                bankAccountId: bankAccountId, paymentMethod: paymentmethod, startDate: startDate, endDate: endDate, investmentcode: investmentCode, type: "INV"};
                                            console.log(JSON.stringify(obj));
                                            // Make Payment for Investment Option 

                                            $.ajax({
                                                type: 'GET',
                                                url: './rest/groot/db/api/check-communication',
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data) {
//                                                    alert(data);
                                                    if (data) {
                                                        swal({
                                                            title: "Thanks!",
                                                            text: "We have received your investment instructions.",
//                                                type: "info",
//                                                timer: 2500,
                                                            showConfirmButton: true
                                                        }, function () {
                                                            window.location = "./home";
                                                        });
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                  location.reload(true);
                                                    console.log('error');
                                                }
                                            });





//                                            swal({
//                                                title: "Thanks!",
//                                                text: "We have received your investment instructions.",
////                                                type: "info",
////                                                timer: 2500,
//                                                showConfirmButton: true
//                                            }, function () {
//                                                window.location = "./home";
//                                            });
                                            var Url = './rest/groot/db/api/admin/pending-investment';
                                            // Create Pending Transaction From Add Fund Click
                                            if ('${ic}' !== "") {
//                                                swal({
//                                                    title: "Thanks!",
//                                                    text: "We have received your investment instructions.",
////                                                    type: "info",
////                                                    timer: 2500,
//                                                    showConfirmButton: true
//                                                }, function () {
//                                                    window.location = "./home";
//                                                });
                                                Url = './rest/groot/db/api/admin/pending-transaction';
                                            }


                                            $.ajax({
                                                type: 'POST',
                                                url: Url,
                                                data: JSON.stringify(obj),
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data, textStatus, jqXHR) {

                                                    //                                                    swal({
                                                    //                                                        title: "Success",
                                                    //                                                        text: "Investment is successfully.",
                                                    //                                                        type: "success",
                                                    //                                                        timer: 2500,
                                                    //                                                        showConfirmButton: true
                                                    //                                                    });
//                                                    location.reload(true);

                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    //                                                    alert("error");
                                                }
                                            });
                                        }
                                        function  setBankAccount() {
                                            var pincodeinput = $('#pincode-input1').val();
                                            var beneficiaryId = $('#beneficiaryId option:selected').val();
                                            var accountholdername = $('#accountholdername').val();
                                            var bankName = $('.bank_name option:selected').val();
                                            var accountNumber = $('#accountNumber').val();
                                            var obj = {beneficiaryId: beneficiaryId, pincodeinput: pincodeinput, accountHolderName: accountholdername, bankName: bankName, accountNumber: accountNumber};
                                            console.log(JSON.stringify(obj));
                                            //                                            alert(JSON.stringify(obj));

                                            $.ajax({
                                                type: 'GET',
                                                url: './rest/groot/db/api/check-communication',
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data) {
                                                    swal({
                                                        title: "Success",
                                                        text: "Addition of bank account under process. We will add the bank account upon its successful verification.",
                                                        type: "info",
                                                        timer: 2500,
                                                        showConfirmButton: true
                                                    });
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    console.log('error');

                                                }
                                            });

                                            $.ajax({
                                                type: 'POST',
                                                url: './rest/groot/db/api/admin/pending-bankAccount',
                                                data: JSON.stringify(obj),
                                                headers: {"Content-Type": 'application/json'},
                                                success: function (data, textStatus, jqXHR) {

                                                    //                                                    swal({
                                                    //                                                        title: "Success",
                                                    //                                                        text: "Bank Registeration in successfully.",
                                                    //                                                        type: "success",
                                                    //                                                        timer: 2500,
                                                    //                                                        showConfirmButton: true
                                                    //                                                    });
                                                    //                                                    location.reload(true);

                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    //                                                    alert("error");
                                                }
                                            });
                                        }
                                        $.ajax({
                                            type: 'GET',
                                            url: './rest/3rd/party/api/beneficiary-${user.beneficiairyId}',
                                            headers: {"Content-Type": 'application/json'},
                                            success: function (data, textStatus, jqXHR) {
                                                console.log('success' + data);
                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                console.log(JSON.stringify(obj));
//                                                $('.first-name').text(obj.FirstName);
//                                                $('.last-name').text(obj.LastName);
                                                $('.full-name').text(obj.FirstName + " " + obj.LastName);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log('error');
                                            }
                                        });
                                    </script>
                                    <script>
                                        $(document).ready(function () {
                                            $(document).on("click", "ul.prod-gram .init", function () {
                                                $(this).parent().find('li:not(.init)').toggle();
                                            });
                                            var allOptions = $("ul.prod-gram").children('li:not(.init)');
                                            $("ul.prod-gram").on("click", "li:not(.init)", function () {
                                                allOptions.removeClass('selected');
                                                $(this).addClass('selected');
                                                $(this).parent().children('.init').html($(this).html());
                                                $(this).parent().find('li:not(.init)').toggle();
                                            });
                                        });

                                    </script>

                                    </body>
                                    </html>
