
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>

        <style>
            .selectedbank{
                background-color: #65b9ac;
            }
            p.date_upd {
                position: absolute;
                bottom: 0;
                font-size: 12px;
                left: 12px;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment Fund</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <div class="Portfolio-page">
                                <div class="header-image">
                                    <sec:authorize access="hasAuthority('ADMIN')">
                                        <div class="edit-using-btn">
                                            <a id="home-tab"  href="./update-portfolio-${pc}" role="tab"  ><i class="fa fa-edit"></i>Edit</a>
                                        </div>
                                    </sec:authorize>
                                    <div class="container1">
                                        <div class="header-logo">
                                            <div class="header-logo_row">
                                                <div class="cols-1 add-pic">
                                                    <img src="${portfolioDetail.portfolio_pic}">
                                                </div>
                                                <div class="cols-2 add-font">
                                                    <!--  <p>Single Currency Portfolio</p> -->
                                                    <h1 id="fund-name ">${portfolioDetail.fundName} </h1>

                                                    <div class="info-port companies-dta-value-fund ">

                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>Unit price</p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-price">

                                                                </span>

                                                            </div>
                                                        </div>



                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>Fund size </p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-size">

                                                                </span>

                                                            </div>
                                                        </div>



                                                        <div class="info-port-text">
                                                            <div class="up_makt">
                                                                <p>1 Year Return</p>
                                                                <i class="fa fa-arrow-up"></i>
                                                                <span id="fund-return">

                                                                </span>
                                                                <!-- <i class="fas fa-arrow-up"></i>
                                                                <span>
                                                                        14.15%
                                                                </span> -->

                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

<!--                                <div class="modal fade" id="add-investment-fund-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">${portfolioDetail.fundName} </h5>
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                    <span aria-hidden="true">&times;</span>
                                                                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label>Portfolio Name :</label><br>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <div class="dropdown">
                                                                                                                    <select name="invest" class="invest">
                                                                                                                        <option value="290002">Mint Australia NZ Active Equity Ret</option>
                                                                                                                        <option value="290004">Mint Retail Property Trust</option>
                                                                                                                        <option value="290006">Mint Diversified Income Fund</option>
                                                                                                                        <option value="290012">Mint Diversified Growth Fund</option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Beneficiary Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                                <option >-select-</option>
                                                                <c:forEach items="${beneficiaryDetails}" var="category">
                                                                    <option value="${category.getId()}">${category.getName()}</option>
                                                                </c:forEach> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Name :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" id="investmentName" name ="investmentName" pattern="Enter Investment Name"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Investment Amount :</label><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="number" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount" min="0"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 bankaccounthide">
                                                        <div class="form-group">
                                                            <label>Bank Account Details:</label><br>
                                                        </div>
                                                    </div>
                                                </div>
                                                <c:forEach items="${bankAccountDetails}" var="bank">
                                                    <div  class="bankaccountdiv">
                                                        <input type="hidden" name="id" class="bankid" value="${bank.getId()}">
                                                        <input type="hidden" name="id" class="beneficiarid" value="${bank.getBeneficiaryId()}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Account Name
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group accountname">
                                                                    ${bank.getAccountName()}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Bank
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group bankname">
                                                                    ${bank.getBank()}  
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Branch
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group branchname">
                                                                    ${bank.getBranch()} 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group model-name">
                                                                    Account 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group account">
                                                                    ${bank.getAccount()} 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="btn-pay">
                                                                    <a type="button" class="btn selectBank" data-id="${bank.getId()}">Select</a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </c:forEach>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary close-model" data-dismiss="modal">Close</button>
                                                <button type="button" id="saveInvestmentbtn" class="btn btn-primary">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="container ">
                                    <div class="row Portfolio-page-text">
                                        <div class="col-sm-12 col-md-10">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Performance Since Inception</h6>
                                            </div>
                                            <div class="char_section1">
                                                <div id="fundPerformanceChart1" style="height: 400px; overflow: hidden;" data-highcharts-chart="1"></div>
                                                <div class="row">
                                                    <div class="col-md-12 mt-3">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Fund Performance </h6>
                                                        </div>
                                                        <div id="elementboxcontent" class="">

                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label" >
                                                                            1 Month
                                                                        </div>
                                                                        <div class="value" id="1-month" >
                                                                        </div>

                                                                        <div class="trending trending-up 1-monthTrending" hidden>
                                                                            <span>12.95</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 
                                                                        <p class="date_upd"> as at 31 mar 2019</p>


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            3 Months
                                                                        </div>
                                                                        <div class="value" id="3-month" >
                                                                            5.63
                                                                        </div>



                                                                        <div class="trending trending-up 3-monthTrending" hidden>
                                                                            <span>10.78</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 
                                                                        <p class="date_upd"> as at 31 mar 2019</p>

                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            1 Year
                                                                        </div>
                                                                        <div class="value" id="1-year" >
                                                                            14.08
                                                                        </div>


                                                                        <div class="trending trending-up 1-yearTrending" hidden>
                                                                            <span>24.04</span>
                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                        </div> 
                                                                        <p class="date_upd"> as at 31 mar 2019</p>


                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="element-box el-tablo centered trend-in-corner smaller">
                                                                        <div class="label">
                                                                            5 Years
                                                                        </div>
                                                                        <div class="value" id="5-year" >
                                                                            14.61
                                                                        </div>
                                                                        <p class="date_upd"> as at 31 mar 2019 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">

                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Fund Overview  </h6>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 update-fund-pera" id="paragraph">
                                                                    ${portfolioDetail.getFundOverview()}                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box">
                                                            <!-- <div class="element-wrapper">
                                                                    <h6 class="element-header">Fund Overview  </h6>
                                                            </div> -->
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="risk-indicator">
                                                                        <div class="element-wrapper">
                                                                            <h6 class="element-header">Risk Indicator <div class="pulsating-circle circle-2" data-toggle="tooltip" title="Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund." data-placement="right" style="vertical-align:text-bottom"></div></h6>

                                                                        </div>
                                                                        <div class="Higher">
                                                                            <span class="risk-set">Lower risk </span>
                                                                            <span>Higher risk </span>
                                                                        </div>
                                                                        <span class="indicator-box">
                                                                            <span class="risk-btns ">1</span>
                                                                            <span class="risk-btns addactive-cls">2</span>
                                                                            <span class="risk-btns">3</span>
                                                                            <span class="risk-btns">4</span>
                                                                            <span class="risk-btns">5</span>
                                                                            <span class="risk-btns">6</span>
                                                                            <span class="risk-btns">7</span>
                                                                        </span>
                                                                        <div class="Higher">
                                                                            <span  class="risk-set">Potentially <br>lower returns</span>
                                                                            <span>Potentially <br>higher returns</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Target Investment Mix </h6>
                                                            </div>
                                                            <div class="el-chart-w" id="container19" style="height:340px"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Sector Allocation</h6>

                                                            </div>
                                                            <div class="el-chart-w" id="container-221" style="height:340px"></div>
                                                        </div>
                                                    </div>                                                   
                                                    <div class="col-md-12">

                                                        <div id="elementboxcontent" class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header"> Month in review  <a href="" class="fund-detail-btn">Download Full Update</a></h6>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12" >
                                                                    <h6><span id="portheading">${portfolioDetail.getMonthlyReturnValue()}</span></h6>
                                                                </div>
                                                                <div class="col-md-12" id="portfoliooverview">
                                                                    ${portfolioDetail.getMonthlyFundUpdate()}
                                                                </div>
                                                            </div>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Features of the fund </h6>
                                                            </div>
                                                            <div class="feature-text">
                                                                ${portfolioDetail.getFeaturesOftheFund()}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">

                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Portfolio Manager </h6>
                                                            </div> 
                                                            <div class="invest-team">
                                                                <div class="row">
                                                                    <c:forEach items="${investmentTeam}" var="team">
                                                                        <div class="col-md-3 col-sm-3 five-div invest-team-images">

                                                                            <img src="${team.pic_url}" alt="image">
                                                                            <p>${team.name}</p>
                                                                            <h6>${team.place}</h6>
                                                                        </div>


                                                                        <div class="col-md-9 col-sm-9 five-div invest-team-images">
                                                                            <!--                                                                        <div class="element-wrapper">
                                                                                                                                                        <h6 class="element-header">BIO</h6>
                                                                                                                                                    </div> -->
                                                                            <div class="bio-input">
                                                                                ${team.team_bio}                                                                  </div>

                                                                        </div>
                                                                    </c:forEach>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Resources </h6>
                                                            </div>
                                                            <c:forEach items="${investmentPdf}" var="pdf">
                                                                <div class="resources-pdf">
                                                                    <p>${pdf.pdf_name} </p>
                                                                    <span>
                                                                        <a href="${pdf.pdf_url}"><i class="fa fa-file-pdf-o"></i></a>
                                                                    </span>
                                                                </div>
                                                            </c:forEach>
                                                            <!--                                                            <div class="resources-pdf">
                                                                                                                            <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>-->
                                                            <!--                                                                </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint SRI Policy</p>
                                                                                                                            <span >
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>UNPRI Report</p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>-->

                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 resource-pdf">
                                                        <div class="element-box">
                                                            <div class="element-wrapper">
                                                                <h6 class="element-header">Quarterly Fund Updates  </h6>
                                                            </div>
                                                            <c:forEach items="${quarterlyPdf}" var="pdf">
                                                                <div class="resources-pdf">
                                                                    <p>${pdf.pdf_name} </p>
                                                                    <span>
                                                                        <a href="${pdf.pdf_url}"><i class="fa fa-file-pdf-o"></i></a>
                                                                    </span>
                                                                </div>
                                                            </c:forEach>
                                                            <!--                                                            <div class="resources-pdf">
                                                                                                                            <p>Mint Statement of Investment Policy Objectives 2019 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/PDS-SIPO/Mint-Statement-of-Investment-Policy-Objectives-2019.pdf">Download Pdf</a>-->
                                                            <!--                                                                </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint Asset Management Fund Financial Statements FY19 </p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-Asset-Management-Funds-Financial-Statements-FY19.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>Mint SRI Policy</p>
                                                                                                                            <span >
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/Mint-SRI-Policy.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                        <div class="resources-pdf">
                                                                                                                            <p>UNPRI Report</p>
                                                                                                                            <span>
                                                                                                                                <a href="https://www.mintasset.co.nz/assets/Uploads/UNPRI-Report-2019.pdf">Download Pdf</a>
                                                                                                                            </span>
                                                                                                                        </div>-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-2">
                                            <div class="side_barr">
                                                <a href="./make-investment-${pc}" class="button_inv btn-style" >Make Investment</a>
                                                <!--<a class="button_inv btn-style" data-target="#add-investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">Make Investment</a>-->
                                                <h3>Other Funds</h3>
                                                <div class="other_port">

                                                    <c:forEach items="${otherPortfolioDetail}" var="port">
                                                        <a href="javascript:void(0)" id="fundlink1">
                                                            <div class="other_row">
                                                                <div class="other_row-img">
                                                                    <img src="./resources/images/globe-plane.jpg">
                                                                </div>
                                                                <div class="other_row-text">
                                                                    <p><b id="other-fund1">${port.fundName}</b></p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
   <jsp:include page="footer.jsp"/>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="./resources/js/sweetalert.min.js"></script>
    <script>

        var fmcaTopAssets = [];
        var fmcaTopAssetsColorCode = [];
        var totalFmcaInvestmentMix = [];
        var totalFmcaInvestmentMixcolorCode = [];
        var portfolioCode;
        var performanceSinceinceptionDate = [];
        var performanceSinceinceptionData = [];
        $(document).ready(function () {

            $.each(${fmcaTopAssets}, function (idx, val) {
//                alert(JSON.stringify(val.investmentName));
                fmcaTopAssets.push([val.investmentName, val.price]);
                fmcaTopAssetsColorCode.push(val.colorCode);
            });
            $.each(${totalFmcaInvestmentMix}, function (idx, val2) {
//                alert(JSON.stringify(val2.InvestmentName));
                totalFmcaInvestmentMix.push([val2.InvestmentName, val2.Price]);
                totalFmcaInvestmentMixcolorCode.push(val2.colorCode);
            });
//            piechart1();
//            alert(JSON.stringify(totalFmcaInvestmentMix));
            $('.bankaccountdiv').hide();
            $('.bankaccounthide').hide();
            var pc = '${pc}';
            portfolioCode = pc;
            var fundName = '${portfolioDetail.fundName}';



            piechart1();


            $.each(${performanceSinceinception}, function (idx, val) {
                performanceSinceinceptionDate.push(val.date);
                performanceSinceinceptionData.push(parseInt(val.data));
            });
            areachart1(fundName);
//            $.ajax({
//                type: 'GET',
//                url: './rest/groot/db/api/all-funds',
//                headers: {"Content-Type": 'application/json'},
//                success: function (data, textStatus, jqXHR) {
//                    $(".loader").hide();
//                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                    $.each(obj, function (idx, val) {
//                        if (val.Code === pc) {
//                            if (val.Name.includes("Mint")) {
//                                fundName = val.Name;
//                            } else {
//                                fundName = 'Mint ' + val.Name;
//                            }
//                            fundName = fundName.replace('Wholesale', '');
//                            $('#fund-name').text(fundName);
//                            $('#fund-name-modal').text(fundName);
//                        }
//                        if (val.Code === "290002") {
//                            $('#other-fund1').text(val.Name);
//                            if ($('#other-fund1').text() === fundName) {
//                                $('#fundlink1').hide();
//                            }
//                            var id1 = './investment-fund-' + val.Code;
//                            $('#fundlink1').attr('href', id1);
//                        }
//                        if (val.Code === "290004") {
//                            $('#other-fund2').text('Mint ' + val.Name);
//                            if ($('#other-fund2').text() === fundName) {
//                                $('#fundlink2').hide();
//                            }
//                            var id1 = './investment-fund-' + val.Code;
//                            $('#fundlink2').attr('href', id1);
//                        }
//                        if (val.Code === "290006") {
//                            $('#other-fund3').text('Mint ' + val.Name.replace('Wholesale', ''));
//                            if ($('#other-fund3').text() === fundName) {
//                                $('#fundlink3').hide();
//                            }
//                            var id1 = './investment-fund-' + val.Code;
//                            $('#fundlink3').attr('href', id1);
//                        }

//                        if (val.Code === "290002") {
//                            $('#other-fund4').text('Mint ' + val.Name);
//                            if ($('#other-fund4').text() === fundName) {
//                                $('#fundlink4').hide();
//                            }
//                            var id1 = './investment-fund-' + val.Code;
//                            $('#fundlink4').attr('href', id1);
//                        }
//                    });
//                    if (pc === "290002") {
            //                        $('#paragraph').html("<p>This is a single asset class Fund, investing predominantly in Australasian equities, and targeting medium to long-term growth. Investors should expect returns and risk commensurate with the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX50 Gross Index (which is the Fund`s relevant market index) by 3% per annum, before fees, over the medium to long-term.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of Juneprompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out,there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June(and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0%(+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest ratesensitive holdings in the portfolio, with Auckland Airport,Contact Energy and Meridian Energy leading the way for us.The main negative contributors were the a2 Milk Company,Fletcher Building and Link Administration Holdings.</p><p>During the month, we increased holdings in Afterpay TouchGroup and Lend Lease Group, and we added Arvida Group.We exited Link Administration Holdings (having lost convictionin the name), and eased back a little on Mercury andMainfreight into very strong share prices.</p><p>Arvida raised new equity during the month to acquire three villages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield developmentopportunities.</p>");
//                        $('#1-month').text('3.21%');
//                        $('#3-month').text('5.63%');
//                        $('#1-year').text('14.08%');
//                        $('#5-year').text('14.61%');
//                        $('#fund-size').text('$157.7$');
//                        $('#fund-return').text('14.08%');
            //                        $('#portheading').text('Our portfolio returned 3.21% for the month.');
//                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Actively managed 'high conviction' unit trust investing in securities listed on the stock exchanges of Australia and New Zealand.</p><p>Targeting returns above the NZX 50 Gross Index + 3.0% per annum, before fees over the medium to long-term.</p><p>No fixed allocation between countries, regions or sectors.</p><p>Consistent returns via Total Return approach + Active Management (GARP style - Growth At the Right Price).</p><p>1.15% Management Fee (GST inclusive, no performance fee).</p></div><div class='col-md-6'><p>Cash - where attractive opportunities do not exist the Trust will hold cash/yielding securities.</p><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>Target 25 - 40 holdings.</p><p>Currency risk hedged at manager`s discretion.</p></div></div>");

//                    }
//                    if (pc === "290004") {
            //                        $('#paragraph').html("<p>This is a single asset class Fund, investing predominantly in Australasian listed property securities. Investors should expect returns and risk commensurate with the listed property sector of the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX All Real Estate (Industry Group) Gross Index (which is the Fund`s relevant market index) by 1% per annum, before fees, over the medium to long-term.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The top positive contributors were holdings in Precinct Property and Kiwi Property Group. The key detractors were holdings in Aveo Group and Lend Lease Group.</p> \n\
            //                                                      <p>Over the month, there was a flurry of capital raisings and we participated in the Arvida, Charter Hall Long WALE REIT and GPT issues. We also increased exposure to Stride and Lend Lease. We reduced exposure to Kiwi Property Group, Mirvac and Investore as they reached price targets. Arvida (ARV) announced during June that it was acquiring three villages, two in Tauranga and one in Queenstown, for $180m - partly funded by new equity. The acquisition also provides ARV with additional development opportunities.</p> \n\
            //                                                      <p>Charter Hall and Abacus together launched a takeover offer for Australian Unity Office (AOF) after acquiring a 20% stake on market. GPT raised $0.8bn to acquire two Sydney office assets. With retail REITs still trading below NTA, Scentre Group sold a large asset and launched a share buyback.</p>");
//                        piechart2();
//                        areachart2(fundName);
//                        $('#1-month').text('5.01%');
//                        $('#3-month').text('10.19%');
//                        $('#1-year').text('25.00%');
//                        $('#5-year').text('12.24%');
//                        $('#fund-size').text('$76.1$');
//                        $('#fund-return').text('25.00%');
            //                        $('#portheading').text('Our portfolio returned 5.01% for the month.');
//                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Open-ended 'high conviction' Australia and New Zealand actively managed listed property and property related equity trust.</p><p>Can hold up to 60% of listed property and property related investments in Australia.</p><p>Targeting returns above the S&P/NZX All Real Estate Gross Index + 1.0% p.a, before fees.</p><p>Consistent returns via Total Return approach + Active Management (GARP style).</p><p>Suitable for Investors with a minimum 5 year investment horizon.</p></div><div class='col-md-6'><p>Cash where attractive opportunities do not exist the Trust will hold cash/yielding securities.</p><p>0.86% Management Fee (GST exclusive, no performance fee).</p><p>Target 15 - 30 holdings.</p><p>Currency risk hedged at Manager's discretion.</p></div></div> ");
//                    }
//                    if (pc === "290006") {
            //                        $('#paragraph').html("<p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.</p><p>The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is Mint`s lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now). While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p> <p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest rate sensitive holdings in the portfolio, with Auckland Airport, Contact Energy and Meridian Energy leading the way for us. The main negative contributors were the a2 Milk Company, Fletcher Building and Link Administration Holdings.</p> <p>During the month, we increased holdings in Afterpay Touch Group and Lend Lease Group, and we added Arvida Group. We exited Link Administration Holdings (having lost conviction in the name), and eased back a little on Mercury and Mainfreight into very strong share prices.</p> <p>Arvida raised new equity during the month to acquire three villages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield development opportunities.</p>");
//                        piechart3();
//                        areachart3(fundName);
//                        $('#1-month').text('1.45%');
//                        $('#3-month').text('2.87%');
//                        $('#1-year').text('7.01%');
//                        $('#5-year').text('-');
//                        $('#fund-size').text('$114.1$');
//                        $('#fund-return').text('7.01%');
            //                        $('#portheading').text('Our portfolio returned 1.45% for the month.');
//                        $('#fundnamespan').text(fundName + ': ');
            //                        $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Provides investors with a combination of income and capital growth.</p><p>Has a lower risk profile than funds investing only in equities.</p><p>Targeting sustainable returns above the CPI index + 3.0% per annum, with low volatility.</p><p>Will hold combination of both domestic and global fixed interest and equity securities.</p></div><div class='col-md-6'><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>0.86% Management fee (GST inclusive, no performance fee).</p><p>Currency risk hedged at manager's discretion.</p></div></div>");
//                    }
//                    if (pc === "290012") {
            //                        $('#paragraph').html("<p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities. </p><p>The objective of the Fund is to deliver returns in excess of the Consumer Price Index (CPI) by 4.5% per annum, before fees, over the medium to long-term. Investors should expect returns and risk to sit between the risk profiles of the property and equities asset classes.</p>");
            //                                                                                            $('#marketoverview').html("<p>A trade ceasefire between the United States and China towards the end of June prompted hope that the trade war would not escalate further (for now).While the trade war impact on the United States economy is yet to fully play out, there is an expectation that the US Federal Reserve will cut rates earlier rather than later. The Reserve Bank of Australia cut their cash rate by 0.25% in early June (and again by the same amount thus far in July). This leaves their cash rate at a record low of 1%. The Reserve Bank of New Zealand held rates at their latest review (at 1.5%), but made it clear that a lower rate may be needed. In summary ? interest rates are lower for longer, reigniting the global demand for higher yielding assets.</p><p>The NZ listed property sector had another very strong month with a return of 6.0% (+12% for the quarter, and +22% for the year to date). Top performers for the month were Augusta and Precinct. The NZ share market too was strong (+3.8%, +7% for the quarter, +19% ytd), with June?s top performers being Mercury Energy, Gentrack, and Auckland Airport. In Australia, Gold was the best performing sector, and bond proxies like REITs and infrastructure were also strong. Global equities were very strong, led by the USA, France and Germany ? regaining their losses from May.</p>");
            //                        $('#portfoliooverview').html("<p>The main positive contributions came from the interest rate sensitive holdings in the portfolio, with Auckland Airport,Contact Energy and Meridian Energy leading the way for us.The main negative contributors were the a2 Milk Company,Fletcher Building and Link Administration Holdings.</p><p>During the month, we increased holdings in Afterpay TouchGroup and Lend Lease Group, and we added Arvida Group.We exited Link Administration Holdings (having lost convictionin the name), and eased back a little on Mercury andMainfreight into very strong share prices.</p><p>Arvida raised new equity during the month to acquire threevillages, two in Tauranga and one in Queenstown, for $180m. The acquisition adds 326 independent living units to Arvida&apos;s existing portfolio of 3,677 units and beds. The acquisition alsoprovides Arvida with additional brownfield development opportunities.</p>");
//                        piechart4();
//                        areachart4(fundName);
//                        $('#1-month').text('3.66%');
//                        $('#3-month').text('4.01%');
//                        $('#1-year').text('-');
//                        $('#5-year').text('-');
//                        $('#fund-size').text('$5.8$');
//                        $('#fund-return').text('-%');
//                                                $('#portheading').text('Our portfolio returned 3.66% for the month.');
//                        $('#fundnamespan').text(fundName + ': ');
//                                                $('.feature-text').html("<div class='row'><div class='col-md-6'><p>Provides investors with long-term capital growth.</p><p>Has a risk profile between that of the property and equities asset classes</p><p>Targeting sustainable returns above the CPI Index + 4.5% per annum, with moderate to high volatility.</p><p>Will hold a combination of International and Australasian equities, fixed interest, listed property and cash.</p></div><div class='col-md-6'><p>Suitable for investors with a minimum 5 year investment horizon.</p><p>1.09% Management fee (GST inclusive, no performance fee).</p><p>Currency risk hedged at manager's discretion.</p></div></div>");
//                    }
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                }
//            });
        });

        //        $('#paragraph').html('');
        //        $('#portfoliooverview').html('');
        //        $('#portheading').html('');
        //        $('.feature-text').html('');
        //                $.ajax({
        //                type: 'GET',
        //                url: './rest/3rd/party/api/fund-details-' + pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    //                        alert(data);
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    $.each(obj.pricePortfolioUnitPrices, function (idx, val) {
        //                        priceArr.push(val.Prices);
        //
        //                    });
        //                    price = priceArr[priceArr.length - 1];
        //                    $('#fund-price').text(price);
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        //            });
        //            $.ajax({
        //                type: 'GET',
        //                url: './rest/groot/db/api/investment-fund-details-'+pc,
        //                headers: {"Content-Type": 'application/json'},
        //                success: function (data, textStatus, jqXHR) {
        //                    $(".loader").hide();
        //                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
        //                    console.log(JSON.stringify(obj));
        //                    $.each(obj, function (idx, val) {
        //                        console.log(JSON.stringify(val[0].Portfolio));
        //                        $('#paragraph').html(val[0].FundOverview);
        //                        $('#portfoliooverview').html(val[0].MonthlyFundUpdate);
        //                        $('#portheading').html(val[0].monthlyReturnValue);
        //                        $('.feature-text').html(val[0].FeaturesOftheFund);
        //                    });
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                    alert(textStatus);
        //                }
        //            });
        //      });
        //        price = priceArr[priceArr.length - 1];
        //        $('#fund-price').text(price);
        //        },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                }
        $('.selectBank').click(function () {
            $('.selectBank').removeClass('selectedbank');
            $(this).addClass('selectedbank');
        });
        $('#beneficiaryId').change(function () {
            $('.bankaccountdiv').hide();
            $('.bankaccounthide').hide();
            var beneficiaryval = $('#beneficiaryId option:selected').text();
            $('#investmentName').val(beneficiaryval);
            var beneficiaryid = $(this).val();
            var beneficiarvals = document.getElementsByClassName('beneficiarid');
            for (var i = 0; i < beneficiarvals.length; i++) {
                var beneficiarval = beneficiarvals[i];
                var root = $(beneficiarval).closest('.bankaccountdiv');
                if (beneficiarval.value === beneficiaryid) {
                    root.show();
                    $('.bankaccounthide').show();
                }
            }

        });
        $('#saveInvestmentbtn').click(function () {
            var bankAccountId = $('.selectedbank').data("id");
            var investmentName = $('#investmentName').val();
            var investmentAmount = $('#investmentAmount').val();
            var beneficiaryId = $('#beneficiaryId option:selected').val();
            var applicationId = '${applicationId}';
            var fundCode = '${pc}';
            var fundName = '${portfolioDetail.fundName}';
            var obj = {portfolioName: fundName, investmentName: investmentName, investedAmount: investmentAmount, portfolioCode: fundCode, applicationId: applicationId, beneficiaryId: beneficiaryId, bankAccountId: bankAccountId};
            swal({
                title: "Proceed",
                text: "Investment is progress.",
                type: "info",
                timer: 2500,
                showConfirmButton: true
            });
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/api/admin/pending-investment',
                data: JSON.stringify(obj),
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {

                    swal({
                        title: "Success",
                        text: "Investment is successfully.",
                        type: "success",
                        timer: 2500,
                        showConfirmButton: true
                    });
                    location.reload(true);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
        });
    </script>
    <script>
        Highcharts.chart('livebalance1', {
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            time: {
                useUTC: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    color: '#03A9F4',
                    shadow: true,
                    lineWidth: 3,
                    marker: {
                        enabled: false
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {

                title: {
                    text: 'Portfolio Investments ($)'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br/>',
                pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: 'Random data',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                                time = (new Date()).getTime(),
                                i;
                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: Math.random()
                            });
                        }
                        return data;
                    }())
                }]
        });
    </script>

    <script>
        Highcharts.chart('stockbalance1', {
            chart: {
                type: 'area'
            },
            accessibility: {
                description: ''
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            xAxis: [{
                    categories: ['21 Oct', '28 Oct', '4 Nov', '11 Nov', '18 Nov', '25 Nov',
                        '1 Dec', '8 Dec']

                }],
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000 + 'k';
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                    name: '',
                    data: [
                        0, 110, 250, 300, 410, 535,
                        709
                    ]
                }, ]
        });
    </script>
    <!--    <script>
            $.getJSON('https://www.highcharts.com/samples/data/aapl-c.json', function (data) {
    
                // Create the chart
                Highcharts.stockChart('container', {
    
                    rangeSelector: {
                        selected: 1
                    },
                    title: {
                        text: 'AAPL Stock Price'
                    },
                    navigator: {
                        enabled: false
                    },
                    series: [{
                            name: 'AAPL Stock Price',
                            data: data,
                            tooltip: {
                                valueDecimals: 2
                            }
                        }]
                });
            });
        </script>-->

    <script>
        $(document).ready(function () {
            $("#view_transaction").hide();
            $("#view_transaction_status").click(function () {
                $("#view_transaction").toggle();
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".company-show1").hide();
            $(".company-show2").hide();
            $(".company-show3").hide();
            $(".company-show4").hide();
            $(".company-show5").hide();
            $(".company-show6").hide();
            $(".company-show7").hide();
            $(".company-show8").hide();
            $(".company-show9").hide();
            $(".companyshow1").click(function () {
                $(".company-show1").toggle();
            });
            $(".companyshow2").click(function () {
                $(".company-show2").toggle();
            });
            $(".companyshow3").click(function () {
                $(".company-show3").toggle();
            });
            $(".companyshow4").click(function () {
                $(".company-show4").toggle();
            });
            $(".companyshow5").click(function () {
                $(".company-show5").toggle();
            });
            $(".companyshow6").click(function () {
                $(".company-show6").toggle();
            });
            $(".companyshow7").click(function () {
                $(".company-show7").toggle();
            });
            $(".companyshow8").click(function () {
                $(".company-show8").toggle();
            });
            $(".companyshow9").click(function () {
                $(".company-show9").toggle();
            });
        });
    </script>
    <script>
        Highcharts.chart('fund-container22', {
            chart: {
                type: 'pie'
            },
            credits: {
                enabled: false,
            },
            exporting: {
                enabled: false,
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.y:,.2f}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                },
            },
            series: [{
                    showInLegend: true,
                    name: 'Percentage',
                    data: [
                        ['Australia 24.2%', 24.2],
                        ['China 17.3%', 17.3],
                        ['Korea 15.6%', 15.6],
                        ['Hong Kong 9.2%', 9.2],
                        ['India 7.8%', 7.8],
                        ['Taiwan 7.0%', 7.0],
                        ['Thailand 4.7%', 4.7],
                        ['Singapore 4.5%', 4.5],
                        ['Indonesia 4.2%', 4.2],
                        ['Other 5.5%', 5.5],
                    ]
                }]
        });
    </script>
    <script>
        function piechart1() {
            Highcharts.setOptions({
//                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                colors: fmcaTopAssetsColorCode
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: fmcaTopAssets
                    }]
            });
            Highcharts.setOptions({
                colors: totalFmcaInvestmentMixcolorCode
//                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: totalFmcaInvestmentMix
                    }]
            });
        }
    </script>
    <script>
        function piechart2() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                legend: {
                    maxHeight: 70,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Diversified REITs',
                                y: 55,
                            }, {
                                name: 'Real Estate Operating Companies',
                                y: 15
                            }, {
                                name: 'Industrial REITs',
                                y: 8
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 6
                            }, {
                                name: 'Specialised REITs',
                                y: 5,
                            }, {
                                name: 'Retail REITs ',
                                y: 4
                            }, {
                                name: 'Real Estate Development',
                                y: 3
                            }, {
                                name: 'Health Care',
                                y: 3
                            }, {
                                name: 'Office REITs',
                                y: 2,
                            }, {
                                name: 'Residential REITs',
                                y: 1
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#93dacf', '#3b888a']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and cash equivalents',
                                y: 5
                            }, {
                                name: 'Listed property',
                                y: 95
                            }]
                    }]
            });
        }
    </script>
    <script>
        function piechart3() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'New Zealand fixed interest ',
                                y: 56,
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 15
                            }, {
                                name: 'Listed property',
                                y: 12
                            }, {
                                name: 'International fixed interest',
                                y: 6
                            }, {
                                name: 'Australasian equities',
                                y: 6
                            }, {
                                name: 'International equities',
                                y: 5
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#7ba0c4', '#3b888a', '#8f8d8c', '#c54b38', '#93dacf']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and Cash Equivalents',
                                y: 5
                            }, {
                                name: 'Listed property',
                                y: 15
                            }, {
                                name: 'Fixed Interest',
                                y: 65
                            }, {
                                name: 'International Equities',
                                y: 10
                            }, {
                                name: 'Australasian equities',
                                y: 5
                            }]
                    }]
            });
            $('.risk-btns').removeClass('active');
            $('.addactive-cls').addClass('active');
        }
    </script>
    <script>
        function piechart4() {
            Highcharts.setOptions({
                colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
            });
            Highcharts.chart('container-221', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 70,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'International equities',
                                y: 59,
                            }, {
                                name: 'Australasian equities',
                                y: 14
                            }, {
                                name: 'New Zealand fixed interest',
                                y: 13
                            }, {
                                name: 'Cash and cash equivalents',
                                y: 10
                            }, {
                                name: 'Listed property ',
                                y: 4
                            }]
                    }]
            });
            Highcharts.setOptions({
                colors: ['#93dacf', '#3b888a', '#8f8d8c', '#c54b38', '#7ba0c4']
            });
            Highcharts.chart('container19', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false,
                },
                title: {
                    text: ''
                },
                legend: {
                    maxHeight: 70,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 0,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            distance: 2,
                            connectorWidth: 0,
                            enabled: false,
                            format: '{point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                                name: 'Cash and Cash Equivalents',
                                y: 5
                            }, {
                                name: 'Listed Property',
                                y: 5
                            }, {
                                name: 'Fixed Interest',
                                y: 15
                            }, {
                                name: 'International Equities',
                                y: 60
                            }, {
                                name: 'Australasian equities',
                                y: 15
                            }]
                    }]
            });
        }
    </script>
    <script>
        function areachart1(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
//                    categories: [
//                        'Feb-07', 'Feb-08', 'Feb-09', 'Feb-10', 'Feb-11', 'Feb-12', 'Feb-13', 'Feb-14', 'Feb-15', 'Feb-16', 'Feb-17', 'Feb-18', 'Feb-19'
//                    ],
                    categories: performanceSinceinceptionDate,
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 6000,
                    max: 33000,
                    tickInterval: 3000,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: performanceSinceinceptionData
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>
    <script>
        function areachart2(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Dec-07', 'Dec-08', 'Dec-09', 'Dec-10', 'Dec-11', 'Dec-12', 'Dec-13', 'Dec-14', 'Dec-15', 'Dec-16', 'Dec-17', 'Dec-18'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 6000,
                    max: 24000,
                    tickInterval: 2000,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            7270.00,
                            8200.00,
                            9309.60,
                            10229.45,
                            11229.45,
                            12035.03,
                            14060.47,
                            16469.66,
                            17275.78,
                            18603.49,
                            19575.7]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>
    <script>
        function areachart3(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Aug-14', 'Aug-15', 'Aug-16', 'Aug-17', 'Aug-18'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 9000,
                    max: 13000,
                    tickInterval: 500,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            10570.00,
                            11509.60,
                            11729.45,
                            12335.03]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>
    <script>
        function areachart4(fundName) {
            Highcharts.chart('fundPerformanceChart1', {
                chart: {
                    type: 'area'
                },
                title: {
                    text: '$10,000 invested since inception '
                },
                subtitle: {

                },
                xAxis: {
                    categories: [
                        'Dec-18', 'Jan-19', 'Feb-19', 'Mar-19', 'Apr-19', 'May-19', 'Jun-19'
                    ],
                    //tickInterval: 1,
                    labels: {
                        style: {
                            color: '#333',
                            fontSize: '12px',
                            textTransform: 'uppercase'
                        },
                        //                    y: 20,
                        //                    x: 10
                    },
                    lineColor: '#dadada'
                },
                yAxis: {
                    min: 9000,
                    max: 11000,
                    tickInterval: 250,
                    labels: {
                        format: '$ {value}',
                        //                    style: {
                        //                        color: Highcharts.getOptions().colors[2]
                        //                    }
                    },
                    title: {
                        text: ''
                    },
                    opposite: true
                },
                credits: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        enabled: false
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    verticalAlign: 'bottom',
                    enabled: true
                },
                plotOptions: {
                    series: {
                        color: '#93dbd0',
                        shadow: true,
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: fundName,
                        data: [10000.00,
                            10070.00,
                            10200.00,
                            10259.60,
                            10429.45,
                            10535.03,
                            10555.03]
                    }],
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }
    </script>


    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: './rest/groot/db/api/get-FundPerformance-${pc}',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    console.log("data-->" + data);
                    var obj = JSON.parse(data);
                    $(".loader").hide();
                    console.log(typeof obj);
                    $('#1-month').text(obj.onemonth);
                    $('.1-monthTrending').find('span').text(obj.onemonthTrending);
                    $('#3-month').text(obj.threemonth);
                    $('.3-monthTrending').find('span').text(obj.threemonthTrending);
                    $('#1-year').text(obj.oneyear);
                    $('.1-yearTrending').find('span').text(obj.oneyeartrending);
                    $('#5-year').text(obj.fiveyear);
                    $('#fund-price').text(obj.unit_price);
                    $('#fund-size').text(obj.fund_size);
                    $('#fund-return').text(obj.one_year_return);
                    $('.risk-btns').each(function (i) {
                        var statsValue = $(this).text();
                        if (statsValue === obj.riskIndicator) {
                            $(this).addClass("active");
                        }
                    });
                    console.log('here is end');
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(${pc});
                    alert("error");
                }
            });
        });
    </script>
</body>
</html>