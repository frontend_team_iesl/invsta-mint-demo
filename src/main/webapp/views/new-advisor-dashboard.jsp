<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Advisor Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        <style>

            .profile-img{
                text-align: center;
            }
            .profile-img img{
                width: 70%;
                height: 100%;
            }
            .profile-img .file {
                position: relative;
                overflow: hidden;
                margin-top: -20%;
                width: 70%;
                border: none;
                border-radius: 0;
                font-size: 15px;
                background: #212529b8;
            }
            .profile-img .file input {
                position: absolute;
                opacity: 0;
                right: 0;
                top: 0;
            }
            .profile-head h5{
                color: #333;
            }
            .profile-head h6{
                color: #0062cc;
            }
            .profile-edit-btn{
                border: none;
                border-radius: 1.5rem;
                width: 70%;
                padding: 2%;
                font-weight: 600;
                color: #6c757d;
                cursor: pointer;
            }
            .proile-rating{
                font-size: 12px;
                color: #818182;
                margin-top: 5%;
            }
            .proile-rating span{
                color: #495057;
                font-size: 15px;
                font-weight: 600;
            }
            .profile-head .nav-tabs{
                margin-bottom:5%;
            }
            .profile-head .nav-tabs .nav-link{
                font-weight:600;
                border: none;
            }
            .profile-head .nav-tabs .nav-link.active{
                border: none;
                border-bottom:2px solid #0062cc;
            }
            .profile-work{
                padding: 14%;
                margin-top: -15%;
            }
            .profile-work p{
                font-size: 12px;
                color: #818182;
                font-weight: 600;
                margin-top: 10%;
            }
            .profile-work a{
                text-decoration: none;
                color: #495057;
                font-weight: 600;
                font-size: 14px;
            }
            .profile-work ul{
                list-style: none;
            }
            .profile-tab label{
                font-weight: 600;
            }
            .profile-tab p{
                font-weight: 600;
                color: #0062cc;
            }
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }
            .advisor-pie-section {
                padding: 0;
                border-radius: 39px;
            }



            .advisor-pie-section span:nth-child(2) {
                background: #e5f5f3;
                padding: 6px;
                width: 129px;
                text-align: center;
                border-radius: 40px;
            }

            .advisor-pie-section span {
                padding: 7px 14px;
            }

            .content-i {
                background: #f5f9f8;
            }



            .advisor-pie-data.owl_add .advisor-pie-section {
                background: #e5f5f3;
                padding: 0;
                border-radius: 39px;
            }



            .advisor-pie-data.owl_add .advisor-pie-section span:nth-child(2) {
                background: #ffff;
                padding: 6px;
                width: 200px;
                text-align: center;
                border-radius: 40px;
            }

            .advisor-pie-data.owl_add .advisor-pie-section span {
                padding: 7px 14px;
            }
            .element-box {
                background: white;
            }
            .clr-cng {
                background: #d9f4f0;
            }


        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"></jsp:include>  
                    <div class="content-w">
                        <!--------------------
                        START - Breadcrumbs
                -------------------->
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item ">
                                <a href="./home">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span>Investment</span>
                            </li>
                            <li class="breadcrumb-item">
                                <span></span>
                            </li>
                        </ul>
                        <!--------------------
                        END - Breadcrumbs
                -------------------->
                        <div class="content-panel-toggler">
                            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                        </div>
                        <div class="content-i">
                            <div class="content-box">
                                <div class="das-board" id="investments" hidden>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Investments </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="portfolio" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="element-box ele-overlay" style="background:url(https://backoffice.invsta.io/pocv/resources/img/) center center no-repeat ;background-size:cover">
                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;" class="pcName">Australasian Equity Fund</h6>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                Contribution
                                                            </div>
                                                            <div class="value contribution">
                                                                NZ$ 0.00 
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label ">
                                                                Current Balance
                                                            </div>
                                                            <div class="value marketValue" id="currentInvSqr">
                                                                NZ$ 0.00
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div class="label">
                                                                Performance
                                                            </div>
                                                            <div class="arrow-text">
                                                                <div class="value per- per-color- percentage">
                                                                    33.66 %
                                                                </div>

                                                                <i class=" os-icon os-icon-arrow-up6"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="element-box el-tablo centered trend-in-corner smaller">
                                                            <div id="lg-latestweek1" style="height:70px; width:100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="show_more_btn">
                                                    <a class="btn btn-primary btn-style showmemore" href="javascript:void(0)">Show Me More</a>
                                                </div>
                                            </div>
                                        </div>                                                                             
                                    </div>
                                </div>
                                <div class="row">                                  
                                    <div class="col-sm-12">
                                        <div class="element-wrapper">
                                            <!--                                        <div class="element-actions">
                                                                                        <button type="button" class="btn btn-info btn-lg advisorModel" data-toggle="modal" data-target="#advisorModel" onclick="advisorModel(this);">Add Advisor </button>
                                                                                    </div>-->
                                            <h6 class="element-header">
                                                Advisor Dashboard <img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">
                                            </h6>
                                            <div class="element-content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <div class="element-box">
                                                                    <div id="advisor-pie" style="min-width: 310px; height: 250px; max-width: 600px; margin: 0 auto"></div>

                                                                </div>
                                                            </div> 
                                                            <div class="col-md-5">
                                                                <div class="advisor-pie-data  owl_add">
                                                                    <div class="advisor-pie-section">
                                                                        <span>Contribution</span> 
                                                                        <span class="contributionspan">$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section">
                                                                        <span>Reinvested Distns</span> 
                                                                        <span>$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section">
                                                                        <span>Distns</span> 
                                                                        <span>-$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section">
                                                                        <span>Withdrawals</span> 
                                                                        <span class="withdrawalspan">-$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section">
                                                                        <span>Tax/Paid</span> 
                                                                        <span>-$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section">
                                                                        <span>Gains/Losses</span> 
                                                                        <span>$0.0</span> 
                                                                    </div>
                                                                    <div class="advisor-pie-section advisor-border">
                                                                        <span>Value</span> 
                                                                        <span class="totalvalspan">$0.0</span> 
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo clr-cng">
                                                                    <div class="label">
                                                                        Total Customers
                                                                    </div>
                                                                    <div class="value totaluser">
                                                                        4

                                                                    </div>
                                                                    <div class="trending trending-up">
                                                                        <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo clr-cng ">
                                                                    <div class="label">
                                                                        Total Investments
                                                                    </div>
                                                                    <div class="value totalinvestment">
                                                                        425

                                                                    </div>
                                                                    <div class="trending trending-down-basic">
                                                                        <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="element-box el-tablo clr-cng">
                                                                    <div class="label">
                                                                        Current Balance
                                                                    </div>
                                                                    <div class="value contributionspan">
                                                                        152,963.02

                                                                    </div>
                                                                    <div class="trending trending-down-basic">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="advisor-crousel">
                                            <div class="carousel-wrap">
                                                <div class="owl-carousel owl-loaded owl-drag" id="owl-items" style="">

                                                    <!--<div class="owl-stage" style="transform: translate3d(-1370px, 0px, 0px); transition: all 0.25s ease 0s; width: 2742px;"><div class="owl-item cloned" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN932"><div id="advisor-pie11" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="3"><div id="highcharts-ad2o2vw-45" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-49-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 112.71866549167699 15.450562264463443 L 126.35933274583849 48.22528113223172 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 112.78422855915376 15.423313709218135 A 71 71 0 0 1 133.73255069532956 10.277167200306621 L 136.86627534766478 45.63858360015331 A 35.5 35.5 0 0 0 126.39211427957687 48.21165685460907 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 133.80327665006652 10.27093511345997 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 136.90163832503325 45.635467556729985 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class D</tspan><tspan dy="21" x="150">[MIN932]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$17550</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$0</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$17550</span> </div></div></div></div></div><div class="owl-item cloned" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN931"><div id="advisor-pie10" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="2"><div id="highcharts-ad2o2vw-40" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-44-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 92.84361379001783 27.92198911399371 L 116.42180689500891 54.46099455699685 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 92.89671537024861 27.874859274646354 A 71 71 0 0 1 118.78831547748797 13.242606014417703 L 129.39415773874398 47.12130300720885 A 35.5 35.5 0 0 0 116.44835768512431 54.43742963732318 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 118.85608346602206 13.221428212124636 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 129.428041733011 47.11071410606232 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class C</tspan><tspan dy="21" x="150">[MIN931]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$33750</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$-4069</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$33750</span> </div></div></div></div></div><div class="owl-item cloned" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN932"><div id="advisor-pie11" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="3"><div id="highcharts-ad2o2vw-45" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-49-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 112.71866549167699 15.450562264463443 L 126.35933274583849 48.22528113223172 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 112.78422855915376 15.423313709218135 A 71 71 0 0 1 133.73255069532956 10.277167200306621 L 136.86627534766478 45.63858360015331 A 35.5 35.5 0 0 0 126.39211427957687 48.21165685460907 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 133.80327665006652 10.27093511345997 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 136.90163832503325 45.635467556729985 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class D</tspan><tspan dy="21" x="150">[MIN932]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$17550</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$0</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$17550</span> </div></div></div></div></div><div class="owl-item" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN931"><div id="advisor-pie10" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="2"><div id="highcharts-ad2o2vw-40" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-44-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker    " transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 92.84361379001783 27.92198911399371 L 93.50778824367953 28.669566732106475 A 70 70 0 1 0 139.98574287574135 11.000001451897106 Z" class="highcharts-halo highcharts-color-0" data-z-index="-1" fill-opacity="0.25" visibility="hidden"></path><path fill="rgb(84,86,90)" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 92.84361379001783 27.92198911399371 L 116.42180689500891 54.46099455699685 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0         "></path><path fill="rgb(173,77,60)" d="M 92.89671537024861 27.874859274646354 A 71 71 0 0 1 118.78831547748797 13.242606014417703 L 129.39415773874398 47.12130300720885 A 35.5 35.5 0 0 0 116.44835768512431 54.43742963732318 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1     "></path><path fill="rgb(185,187,190)" d="M 118.85608346602206 13.221428212124636 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 129.428041733011 47.11071410606232 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2      "></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series     " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150"> Invest Now Class C</tspan><tspan dy="21" x="150">[MIN931]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g><g class="highcharts-label highcharts-tooltip      highcharts-color-0" style="pointer-events:none;white-space:nowrap;" data-z-index="8" transform="translate(113,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#54565a" stroke-width="1"></path><text x="8" data-z-index="1" style="font-size:12px;color:#333333;cursor:default;fill:#333333;" y="20"><tspan style="font-size: 10px">Mint Australia NZ Active Equity Ret</tspan><tspan x="8" dy="15">Brands: </tspan><tspan style="font-weight:bold" dx="0">88.5%</tspan></text></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$33750</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$-4069</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$33750</span> </div></div></div></div></div><div class="owl-item active" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN932"><div id="advisor-pie11" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="3"><div id="highcharts-ad2o2vw-45" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root " style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-49-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker " transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#b9bbbe" d="M 133.80327665006652 10.27093511345997 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.902771404335 11.000067524316961 A 70 70 0 0 0 133.89055444372755 11.26711912594645 Z" class="highcharts-halo highcharts-color-2" data-z-index="-1" fill-opacity="0.25" visibility="hidden"></path><path fill="rgb(84,86,90)" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 112.71866549167699 15.450562264463443 L 126.35933274583849 48.22528113223172 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0   "></path><path fill="#ad4d3c" d="M 112.78422855915376 15.423313709218135 A 71 71 0 0 1 133.73255069532956 10.277167200306621 L 136.86627534766478 45.63858360015331 A 35.5 35.5 0 0 0 126.39211427957687 48.21165685460907 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1  "></path><path fill="rgb(185,187,190)" d="M 133.80327665006652 10.27093511345997 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 136.90163832503325 45.635467556729985 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2  "></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series  " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class D</tspan><tspan dy="21" x="150">[MIN932]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g><g class="highcharts-label highcharts-tooltip highcharts-color-0" style="pointer-events:none;white-space:nowrap;" data-z-index="8" transform="translate(113,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 182.5 0.5 C 185.5 0.5 185.5 0.5 185.5 3.5 L 185.5 44.5 C 185.5 47.5 185.5 47.5 182.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#54565a" stroke-width="1"></path><text x="8" data-z-index="1" style="font-size:12px;color:#333333;cursor:default;fill:#333333;" y="20"><tspan style="font-size: 10px">Mint Australia NZ Active Equity Ret</tspan><tspan x="8" dy="15">Brands: </tspan><tspan style="font-weight:bold" dx="0">93.7%</tspan></text></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$17550</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$0</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$17550</span> </div></div></div></div></div><div class="owl-item cloned active" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN931"><div id="advisor-pie10" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="2"><div id="highcharts-ad2o2vw-40" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-44-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 92.84361379001783 27.92198911399371 L 116.42180689500891 54.46099455699685 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 92.89671537024861 27.874859274646354 A 71 71 0 0 1 118.78831547748797 13.242606014417703 L 129.39415773874398 47.12130300720885 A 35.5 35.5 0 0 0 116.44835768512431 54.43742963732318 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 118.85608346602206 13.221428212124636 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 129.428041733011 47.11071410606232 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class C</tspan><tspan dy="21" x="150">[MIN931]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$33750</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$-4069</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$33750</span> </div></div></div></div></div><div class="owl-item cloned active" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN932"><div id="advisor-pie11" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="3"><div id="highcharts-ad2o2vw-45" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-49-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 112.71866549167699 15.450562264463443 L 126.35933274583849 48.22528113223172 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 112.78422855915376 15.423313709218135 A 71 71 0 0 1 133.73255069532956 10.277167200306621 L 136.86627534766478 45.63858360015331 A 35.5 35.5 0 0 0 126.39211427957687 48.21165685460907 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 133.80327665006652 10.27093511345997 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 136.90163832503325 45.635467556729985 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class D</tspan><tspan dy="21" x="150">[MIN932]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$17550</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$0</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$17550</span> </div></div></div></div></div><div class="owl-item cloned" style="width: 332.667px; margin-right: 10px;"><div class="item"><div class="element-box"><a href="./home-beneficiary-dashboard-db?id=14&amp;ic=MIN931"><div id="advisor-pie10" style="height: 370px; max-width: 300px; margin: 0px auto; overflow: hidden;" data-highcharts-chart="2"><div id="highcharts-ad2o2vw-40" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 300px; height: 370px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="https://www.w3.org/2000/svg" width="300" height="370" viewBox="0 0 300 370"><desc>Created with Highcharts 7.2.0</desc><defs><clipPath id="highcharts-ad2o2vw-44-"><rect x="0" y="0" width="280" height="162" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="300" height="370" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="10" y="116" width="280" height="162"></rect><rect fill="none" class="highcharts-plot-border" data-z-index="1" x="10" y="116" width="280" height="162"></rect><g class="highcharts-series-group" data-z-index="3"><g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-pie-series  highcharts-tracker" transform="translate(10,116) scale(1 1)" style="cursor:pointer;"><path fill="#54565a" d="M 139.98553920253764 10.000001472638488 A 71 71 0 1 1 92.84361379001783 27.92198911399371 L 116.42180689500891 54.46099455699685 A 35.5 35.5 0 1 0 139.99276960126883 45.500000736319244 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-0"></path><path fill="#ad4d3c" d="M 92.89671537024861 27.874859274646354 A 71 71 0 0 1 118.78831547748797 13.242606014417703 L 129.39415773874398 47.12130300720885 A 35.5 35.5 0 0 0 116.44835768512431 54.43742963732318 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-1"></path><path fill="#b9bbbe" d="M 118.85608346602206 13.221428212124636 A 71 71 0 0 1 139.90138242439693 10.000068488950063 L 139.95069121219845 45.50003424447503 A 35.5 35.5 0 0 0 129.428041733011 47.11071410606232 Z" transform="translate(0,0)" stroke="#ffffff" stroke-width="1" opacity="1" stroke-linejoin="round" class="highcharts-point highcharts-color-2"></path></g><g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-pie-series " transform="translate(10,116) scale(1 1)"></g></g><g class="highcharts-exporting-group" data-z-index="3"></g><text x="150" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"><tspan>Adminis Custodial</tspan><tspan dy="21" x="150">Nominees Limited-</tspan><tspan dy="21" x="150">InvestNow Class C</tspan><tspan dy="21" x="150">[MIN931]</tspan></text><text x="150" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="115"></text><text x="10" text-anchor="start" class="highcharts-caption" data-z-index="4" style="color:#666666;fill:#666666;" y="367"></text><g class="highcharts-legend" data-z-index="7" transform="translate(24,290)"><rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="251" height="65" visibility="visible"></rect><g data-z-index="1"><g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-0" data-z-index="1" transform="translate(8,3)"><text x="21" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2" y="15"><tspan>Mint Australia NZ Active Equity Ret</tspan></text><rect x="2" y="4" width="12" height="12" fill="#54565a" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-1" data-z-index="1" transform="translate(8,21)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Retail Property Trust</tspan></text><rect x="2" y="4" width="12" height="12" fill="#ad4d3c" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g><g class="highcharts-legend-item highcharts-pie-series highcharts-color-2" data-z-index="1" transform="translate(8,39)"><text x="21" y="15" style="color:#333333;cursor:pointer;font-size:12px;font-weight:bold;fill:#333333;" text-anchor="start" data-z-index="2"><tspan>Mint Diversified Income Fund Retail</tspan></text><rect x="2" y="4" width="12" height="12" fill="#b9bbbe" rx="6" ry="6" class="highcharts-point" data-z-index="3"></rect></g></g></g></g></svg></div></div></a><div class="advisor-pie-data"><div class="advisor-pie-section"><span>Contribution</span><span>$33750</span></div><div class="advisor-pie-section"><span>Reinvested Distns</span><span>$0.0</span></div><div class="advisor-pie-section"><span>Distns</span><span>-$0.0</span></div><div class="advisor-pie-section"><span>Withdrawals</span><span>-$-4069</span> </div><div class="advisor-pie-section"><span>Tax/Paid</span><span>-$0.0</span></div> <div class="advisor-pie-section"><span>Gains/Losses</span> <span>$0.0</span> </div><div class="advisor-pie-section advisor-border"> <span>Value</span><span>$33750</span> </div></div></div></div></div></div></div><div class="owl-nav disabled"><div class="owl-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></div><div class="owl-next"><i class="fa fa-caret-right" aria-hidden="true"></i></div></div><div class="owl-dots disabled"><div class="owl-dot active"><span></span></div></div></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="element-wrapper">

                                            <h6 id="error-message">

                                            </h6>

                                        <c:forEach items="${allPortfolios}" var="portfolio">
                                            <div class="mint-Diversified">
                                                <a href="./portfolio-${portfolio.getCode()}">
                                                    <h5>${portfolio.getName()} </h5>
                                                </a>
                                                <!--<p>Performance<span>33.66%<span></p>-->
                                                            </div> 
                                                        </c:forEach>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <div class="container">
                                                                    <h6 class="hearder-int">Pending Investments:</h6>
                                                                    <table id="pending-invetsment" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                                           data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                                        <thead>
                                                                            <tr>
                                                                                <th data-field="name" ><span >Name</span></th>
                                                                                <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                                                <th data-field="investmentName" ><span >Investment Name</span></th>
                                                                                <th data-field="investedAmount" ><span >Invested Amount</span></th>
                                                                                <th data-field="portfolioCode" ><span>Portfolio Code</span></th>
                                                                                <th data-field="status" ><span>status</span></th>
                                                                                <!--<th data-formatter="viewButtonFormatter">Action</th>-->

                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>

                                                                <div class="container ">
                                                                    <h6 class="hearder-int">Pending Transactions:</h6>
                                                                    <table id="pending-transaction" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                                           data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                                        <thead>
                                                                            <tr>
                                                                                <th data-field="name" ><span>Name</span></th>
                                                                                <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                                                <th data-field="investmentcode" ><span >Investment Code</span></th>
                                                                                <th data-field="amount" ><span>Amount</span></th>
                                                                                <th data-field="type" ><span>Type</span></th>
                                                                                <th data-field="portfolioCode" ><span>Portfolio Code</span></th>
                                                                                <!--<th data-formatter="viewButtonFormatter1">Action</th>-->
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>

                                                                <div class="container emp-profile">
                                                                    <h6 class="hearder-int">Pending Registrations:</h6>
                                                                    <table id="pending-registration" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                                           data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                                        <thead>
                                                                            <tr>
                                                                                <!--<th data-field="id" ><span>Id</span></th>-->
                                                                                <th data-field="username"><span>Username</span></th>
                                                                                <th data-field="name"><span>Full Name</span></th>
                                                                                <th data-field="dob"><span>D.O.B</span></th>
                                                                                <th data-field="reg_type"><span>Reg. Type</span></th>
                                                                                <th data-field="step" ><span>Step</span></th>
                                                                                <th data-formatter="showDetailsButtonFormatter">Show Details</th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="display-type"></div>
                                                        </div>

                                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                                                        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
                                                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
                                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
                                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
                                                        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
                                                        <script src="https://code.highcharts.com/highcharts.js"></script>
                                                        <script src="https://code.highcharts.com/modules/exporting.js"></script>
                                                        <script src="https://code.highcharts.com/modules/export-data.js"></script>
                                                        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
                                                        <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
                                                        <script src='https://use.fontawesome.com/826a7e3dce.js'></script>
                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 

                                                        <script>
                                                            function viewButtonFormatter(value, row, index) {
                                                                return '<a href="./investment-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                                                            }
                                                            function viewButtonFormatter1(value, row, index) {
                                                                return '<a href="./transection-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                                                            }
                                                            function showDetailsButtonFormatter(value, row, index) {
                                                                return '<a href="./reg-details-db-' + row.token + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Show Details</strong></span></a>';
                                                            }
                                                            function deleteapplicationformatter(value, row, index) {
                                                                return '<a href="./delete-application-' + row.ApplicationId + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Delete</strong></span></a>';
                                                            }
                                                            function BeneficiariesLenght(value, row, index) {
                                                                var length = row.Beneficiaries.length;
                                                                return length;
                                                            }
                                                        </script>
                                                        <script>
                                                            $(document).ready(function () {
                                                                var userInvestments = '';
                                                                var userinv =${userInvestments};
                                                                console.log("data is ----------------" + '${userInvestments}');
                                                                if (userinv !== "") {
                                                                    userInvestments = userinv;

                                                                }
                                                                var uniqueArr = [];
                                                                var totalsum = [];
                                                                var countribution = 0;
                                                                var withdwarl = 0;
                                                                var totalval = 0;
                                                                $.each(userInvestments, function (idx, inv) {
                                                                    if (uniqueArr.indexOf(inv) === -1) {
                                                                        uniqueArr.push(inv);
                                                                    }
                                                                });
                                                                var invMap = new Map();
                                                                var benMap = new Map();
                                                                for (var i = 0; i < userInvestments.length; i++) {
                                                                    var inv = userInvestments[i];
                                                                    var invArr = invMap[inv.investmentCode];
                                                                    if (typeof invArr === 'undefined') {
                                                                        invArr = new Array();
                                                                    }
                                                                    invArr.push(inv);
                                                                    invMap[inv.investmentCode] = invArr;

                                                                    var ben = benMap[inv.beneficiairyId];
                                                                    if (typeof ben === 'undefined') {
                                                                        benMap[inv.beneficiairyId] = inv.beneficiairyId;
                                                                    }
                                                                }
                                                                var keys = Object.keys(invMap);
                                                                $('.totalinvestment').text(keys.length);
                                                                var bens = Object.keys(benMap);
                                                                $('.totaluser').text(bens.length);
                                                                for (var i = 0; i < keys.length; i++) {
                                                                    var key = keys[i];
                                                                    console.log(key);
                                                                    var sum = 0;
                                                                    var inv_code = '';
                                                                    var invArr = invMap[key];
                                                                    for (var j = 0; j < invArr.length; j++) {
                                                                        var inv = invArr[j];
                                                                        inv_code = inv.investmentCode;
                                                                        sum += parseFloat(inv.att_value);
                                                                        countribution += parseFloat(inv.att_value);
                                                                        withdwarl += parseFloat(inv.wdw_value);
                                                                    }
                                                                    totalsum.push([inv_code, sum]);
                                                                }
                                                                totalval = countribution + withdwarl;
                                                                var witrl = Math.abs(withdwarl);
                                                                $('.contributionspan').text("$" + countribution.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                $('.withdrawalspan').text("-$" + witrl.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                $('.totalvalspan').text("$" + totalval.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                                                                advisorpiechart(totalsum);
                                                                totalsum = [];
                                                                var countribution = 0;
                                                                var withdwarl = 0;
                                                                var totalval = 0;
                                                                var ic;
                                                                var sum = 0;
                                                                var userId;
                                                                for (var i = 0; i < keys.length; i++) {
                                                                    var key = keys[i];
                                                                    var sum = 0;
                                                                    var inv_code = '';
                                                                    var invArr = invMap[key];
                                                                    for (var j = 0; j < invArr.length; j++) {
                                                                        var inv = invArr[j];
                                                                        inv_code = inv.name + ' [' + inv.investmentCode + ']';
                                                                        ic = inv.investmentCode;
                                                                        var port_code = inv.portfolioName;
                                                                        sum += parseFloat(inv.att_value);
                                                                        countribution += parseFloat(inv.att_value);
                                                                        withdwarl += parseInt(inv.wdw_value);
                                                                        totalsum.push([port_code, sum]);
                                                                        userId = inv.userId;
                                                                    }
                                                                    totalval = countribution + withdwarl;
                                                                    var id = "advisor-pie1" + i;
                                                                    withdwarl = Math.abs(withdwarl);
                                                                    //                        $('#owl-items').append("<div class='item'><div class='element-box'><div id='" + id + "' style=' height: 300px; max-width: 300px; margin: 0 auto'></div><div class='advisor-pie-data'><div class='advisor-pie-section'><span>Contribution</span><span>$" + countribution + "</span></div><div class='advisor-pie-section'><span>Reinvested Distns</span><span>$0.0</span></div><div class='advisor-pie-section'><span>Distns</span><span>-$0.0</span></div><div class='advisor-pie-section'><span>Withdrawals</span><span>-$" + withdwarl + "</span> </div><div class='advisor-pie-section'><span>Tax/Paid</span><span>-$0.0</span></div> <div class='advisor-pie-section'><span>Gains/Losses</span> <span>$0.0</span> </div><div class='advisor-pie-section advisor-border'> <span>Value</span><span>$" + countribution + "</span> </div></div></div></div>");
                                                                    $('#owl-items').append("<div class='item'><div class='element-box'><a href='./home-beneficiary-dashboard-db?id=" + userId + "&ic=" + ic + "'><div id='" + id + "' style=' height: 370px; max-width: 300px; margin: 0 auto'></div></a><div class='advisor-pie-data'><div class='advisor-pie-section'><span>Contribution</span><span>$" + countribution.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "</span></div><div class='advisor-pie-section'><span>Reinvested Distns</span><span>$0.0</span></div><div class='advisor-pie-section'><span>Distns</span><span>-$0.0</span></div><div class='advisor-pie-section'><span>Withdrawals</span><span>-$" + withdwarl.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "</span> </div><div class='advisor-pie-section'><span>Tax/Paid</span><span>-$0.0</span></div> <div class='advisor-pie-section'><span>Gains/Losses</span> <span>$0.0</span> </div><div class='advisor-pie-section advisor-border'> <span>Value</span><span>$" + totalval.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "</span> </div></div></div></div>");

                                                                    userpiechart(id, totalsum, inv_code);
                                                                    totalsum = [];
                                                                    var countribution = 0;
                                                                    var withdwarl = 0;
                                                                    var totalval = 0;
                                                                    sum = 0;
                                                                }
                                                                $.ajax({
                                                                    type: 'GET',
                                                                    url: './rest/groot/db/api/admin/get-pending-transaction',
                                                                    headers: {"Content-Type": 'application/json'},
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                        console.log(obj);
                                                                        $('#pending-transaction').bootstrapTable('load', obj);
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.log(jqXHR);
                                                                    }
                                                                });
                                                                $.ajax({
                                                                    type: 'GET',
                                                                    url: './rest/groot/db/api/admin/get-pending-investment',
                                                                    headers: {"Content-Type": 'application/json'},
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                        console.log(JSON.stringify(obj));
                                                                        $('#pending-invetsment').bootstrapTable('load', obj);
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.log(jqXHR);
                                                                    }
                                                                });
                                                                $.ajax({
                                                                    type: 'GET',
                                                                    url: './rest/groot/db/api/get-pending-registration',
                                                                    headers: {"Content-Type": 'application/json'},
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                        console.log(JSON.stringify(obj));
                                                                        $('#pending-registration').bootstrapTable('load', obj);
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.log(jqXHR);
                                                                    }
                                                                });
//                                                                $.ajax({
//                                                                    type: 'GET',
//                                                                    url: './get-user-information',
//                                                                    headers: {"Content-Type": 'application/json'},
//                                                                    success: function (data, textStatus, jqXHR) {
//                                                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                                                                        console.log(JSON.stringify(obj));
//                                                                        $('.currentBalance').text(obj.amount);
//
//                                                                    },
//                                                                    error: function (jqXHR, textStatus, errorThrown) {
//                                                                        console.log(jqXHR);
//                                                                    }
//                                                                });
                                                                $('.owl-carousel').owlCarousel({
                                                                    loop: true,
                                                                    margin: 10,
                                                                    nav: true,
                                                                    navText: [
                                                                        "<i class='fa fa-caret-left'></i>",
                                                                        "<i class='fa fa-caret-right'></i>"
                                                                    ],
                                                                    autoplay: true,
                                                                    autoplayHoverPause: true,
                                                                    responsive: {
                                                                        0: {
                                                                            items: 1
                                                                        },
                                                                        600: {
                                                                            items: 3
                                                                        },
                                                                        1000: {
                                                                            items: 3
                                                                        }
                                                                    }
                                                                });

                                                            });
                                                        </script>
                                                        <script>
                                                            function advisorpiechart(totalsum) {
                                                                Highcharts.setOptions({
                                                                    colors: ['#54565a', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                                                                });
                                                                Highcharts.chart('advisor-pie', {
                                                                    chart: {
                                                                        plotBackgroundColor: null,
                                                                        plotBorderWidth: null,
                                                                        plotShadow: false,
                                                                        type: 'pie'
                                                                    },
                                                                    title: {
                                                                        text: 'Summary',
                                                                        margin: -5
                                                                    },
                                                                    legend: {
                                                                        align: 'right',
                                                                        verticalAlign: 'middle',
                                                                        layout: 'vertical',
                                                                        title: {
                                                                            //                            text:[["rony"],["neeraj"]]
                                                                        }

                                                                    },
                                                                    tooltip: {
                                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                                    },
                                                                    navigation: {
                                                                        buttonOptions: {
                                                                            enabled: false
                                                                        }
                                                                    },
                                                                    credits: {
                                                                        enabled: false
                                                                    },
                                                                    plotOptions: {
                                                                        pie: {
                                                                            allowPointSelect: true,
                                                                            cursor: 'pointer',
                                                                            dataLabels: {
                                                                                enabled: true,
                                                                                distance: 5,
                                                                            },
                                                                            showInLegend: true
                                                                        }
                                                                    },
                                                                    series: [{
                                                                            name: 'Brands',
                                                                            innerSize: '50%',
                                                                            colorByPoint: true,
                                                                            data: totalsum
                                                                        }]
                                                                });
                                                            }
                                                        </script>
                                                        <script>
                                                            function userpiechart(id, totalsum, title) {
                                                                Highcharts.chart(id, {
                                                                    chart: {
                                                                        plotBackgroundColor: null,
                                                                        plotBorderWidth: null,
                                                                        plotShadow: false,
                                                                        type: 'pie'
                                                                    },
                                                                    title: {
                                                                        text: title
                                                                    },

                                                                    tooltip: {
                                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                                    },
                                                                    navigation: {
                                                                        buttonOptions: {
                                                                            enabled: false
                                                                        }
                                                                    },
                                                                    credits: {
                                                                        enabled: false
                                                                    },
                                                                    plotOptions: {
                                                                        pie: {
                                                                            allowPointSelect: true,
                                                                            cursor: 'pointer',
                                                                            dataLabels: {
                                                                                enabled: false
                                                                            },
                                                                            showInLegend: true
                                                                        }
                                                                    },
                                                                    series: [{
                                                                            name: 'Brands',
                                                                            innerSize: '50%',
                                                                            colorByPoint: true,
                                                                            data: totalsum
                                                                        }]
                                                                });

                                                            }
                                                        </script>
                                                        <script>
                                                            function executeBEDB() {
                                                                $.ajax({
                                                                    type: 'GET',
                                                                    url: './rest/groot/db/api/beneficiary-dashboard?id=${id}',
                                                                    headers: {"Content-Type": 'application/json'},
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                        if (obj.investmentHoldings.length > 0
                                                                                && obj.investmentPerformances.length > 0
                                                                                && obj.fmcaInvestmentMix.length > 0) {
                                                                            dashboard(obj);
                                                                            $(".loader").hide();
                                                                        } else {
                                                                            $.ajax({
                                                                                type: 'GET',
                                                                                url: './rest/groot/db/api/investment-holdings?id=${id}',
                                                                                headers: {"Content-Type": 'application/json'},
                                                                                success: function (data, textStatus, jqXHR) {
                                                                                    $(".loader").hide();
                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                    if (obj.investmentHoldings.length > 0) {
                                                                                        investmentHoldings(obj);
                                                                                        $.ajax({
                                                                                            type: 'GET',
                                                                                            url: './rest/groot/db/api/investment-performances?id=${id}',
                                                                                            headers: {"Content-Type": 'application/json'},
                                                                                            success: function (data, textStatus, jqXHR) {
                                                                                                console.log('success' + data);
                                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                if (obj.investmentPerformances.length > 0) {
                                                                                                    investmentPerformances(obj);
                                                                                                }
                                                                                            },
                                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                                console.log('error');
                                                                                            }
                                                                                        });
                                                                                        $.ajax({
                                                                                            type: 'GET',
                                                                                            url: './rest/groot/db/api/fmca-investment-mix?id=${id}',
                                                                                            headers: {"Content-Type": 'application/json'},
                                                                                            success: function (data, textStatus, jqXHR) {
                                                                                                console.log('success' + data);
                                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                if (obj.fmcaInvestmentMix.length > 0) {
                                                                                                    fmcaInvestmentMix(obj);
                                                                                                }
                                                                                            },
                                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                                console.log('error');
                                                                                            }
                                                                                        });
                                                                                    } else {
                                                                                        swal({
                                                                                            title: "Info",
                                                                                            text: "You have no investment yet",
                                                                                            timer: 2000,
                                                                                            showConfirmButton: false
                                                                                        });
                                                                                    }
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    console.log('error');
                                                                                }
                                                                            });
                                                                        }
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.log('error');
                                                                    }
                                                                });
                                                            }

                                                            var investmentAmount = 0;
                                                            var CurrentValue = 0;
                                                            var portfolio = document.getElementById("portfolio");
                                                            var investments = document.getElementById("investments");
                                                            function investmentHoldings(obj) {
                                                                var investmentHoldings = obj.investmentHoldings;
                                                                var arr = [];
                                                                var fundMap = new Map();
                                                                for (var i = 0; i < investmentHoldings.length; i++) {
                                                                    var performances = investmentHoldings[i];
                                                                    var clone = portfolio.cloneNode(true);
                                                                    var h6 = clone.getElementsByClassName("pcName")[0];
                                                                    h6.innerHTML = performances.InvestmentCode + " :: " + performances.PortfolioName;
                                                                    var cont = clone.getElementsByClassName("contribution")[0];
                                                                    cont.innerHTML = '$' + performances.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                    investmentAmount = investmentAmount + performances.Contributions;
                                                                    var marVal = clone.getElementsByClassName("marketValue")[0];
                                                                    marVal.innerHTML = '$' + performances.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                    ;
                                                                    CurrentValue = CurrentValue + performances.MarketValue;
                                                                    clone.style.display = 'block';
                                                                    var href = './show-me-more-db-' + performances.InvestmentCode + '?pc=' + performances.PortfolioCode;
                                                                    clone.getElementsByClassName("showmemore")[0].setAttribute('href', href);
                                                                    arr.push([performances.PortfolioName, performances.MarketValue]);
                                                                    fundMap[performances.PortfolioCode] = performances.PortfolioName;
                                                                    investments.appendChild(clone);
                                                                }
                                                                document.getElementsByClassName("currentBalance")[0].innerHTML = "$" + investmentAmount.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                //                    document.getElementsByClassName("sumWalletBal")[0].innerHTML = "$" + CurrentValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                                pieChart('investmentBreakdown', arr, true);
                                                            }

                                                            function investmentPerformances(obj) {
                                                                var investmentPerformances = obj.investmentPerformances;
                                                                var performanceMap = new Map();
                                                                var fundMap = new Map();
                                                                var keys = new Array();
                                                                for (var i = 0; i < investmentPerformances.length; i++) {
                                                                    var performance = investmentPerformances[i];
                                                                    var PortfolioArray = performanceMap[performance.Portfolio];
                                                                    if (typeof PortfolioArray === 'undefined') {
                                                                        PortfolioArray = new Array();
                                                                        keys.push(performance.Portfolio);
                                                                    }
                                                                    PortfolioArray.push(performance);
                                                                    performanceMap[performance.Portfolio] = PortfolioArray;
                                                                }
                                                                var avgSeries = [],
                                                                        ago1YearSeries = [],
                                                                        ago2YearSeries = [],
                                                                        ago3YearSeries = [];
                                                                for (var key of keys) {//3 Keys
                                                                    var PortfolioArray = performanceMap[key]; var fundName = fundMap[key];
                                                                    if (typeof fundName === 'undefined') {
                                                                        fundName = key;
                                                                    }
                                                                    var avgPeriod = PortfolioArray[PortfolioArray.length - 1];
                                                                    var avgRR = avgPeriod.XIRRReturnRate * 100;
                                                                    avgSeries.push({
                                                                        name: fundName,
                                                                        data: [avgRR]
                                                                    });
                                                                    var ago1YearPeriod = PortfolioArray[PortfolioArray.length - 2];
                                                                    var ago1YearRR = ago1YearPeriod.XIRRReturnRate * 100;
                                                                    ago1YearSeries.push({
                                                                        name: fundName,
                                                                        data: [ago1YearRR]
                                                                    });

                                                                    var ago2YearPeriod = PortfolioArray[PortfolioArray.length - 3];
                                                                    var ago2YearRR = ago2YearPeriod.XIRRReturnRate * 100;
                                                                    ago2YearSeries.push({
                                                                        name: fundName,
                                                                        data: [ago2YearRR]
                                                                    });

                                                                    var ago3YearPeriod = PortfolioArray[PortfolioArray.length - 4];
                                                                    var ago3YearRR = ago3YearPeriod.XIRRReturnRate * 100;
                                                                    ago3YearSeries.push({
                                                                        name: fundName,
                                                                        data: [ago3YearRR]
                                                                    });
                                                                }
                                                                barChart('avgBarChart', avgSeries);
                                                                barChart('ago1YearsBarChart', ago1YearSeries);
                                                                barChart('ago2YearsBarChart', ago2YearSeries);
                                                                barChart('ago3YearsBarChart', ago3YearSeries);
                                                            }

                                                            function fmcaInvestmentMix(obj) {
                                                                var assetAllocation = [];
                                                                var fmcaInvestmentMix = obj.fmcaInvestmentMix;
                                                                //                    var fmcaInvestmentMixByTable = obj.fmcaInvestmentMixByTable;
                                                                //                    $.each(fmcaInvestmentMix, function (idx, val) {
                                                                //                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                                                                //                    });
                                                                $.each(fmcaInvestmentMix, function (idx, val) {
                                                                    assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                                                                });
                                                                pieChart('assetAllocation', assetAllocation, false);
                                                            }

                                                            function dashboard(obj) {
                                                                investmentHoldings(obj);
                                                                investmentPerformances(obj);
                                                                fmcaInvestmentMix(obj);
                                                            }

                                                            executeBEDB();
                                                        </script>
                                                        <script>
                                                            function pieChart(idc, arr, mktval) {
                                                                Highcharts.setOptions({
                                                                    colors: ['#91d9cf', '#54565a', '#879fc2', '#ad4d3c', '#b9bbbe', '#e2b4ac', '#38a193', '#323436', '435d83', '682e24', '6c7075', 'eed2cd']
                                                                });
                                                                var ptfmt = '';
                                                                if (mktval) {
                                                                    ptfmt = '{series.name} <br>{point.percentage:.2f} % <br>Market Value: {point.y}';
                                                                } else {
                                                                    ptfmt = '{series.name} <br>{point.percentage:.2f} %';
                                                                }
                                                                Highcharts.chart(idc, {
                                                                    chart: {
                                                                        type: 'pie',
                                                                        options3d: {
                                                                            enabled: true,
                                                                            alpha: 0
                                                                        }
                                                                    },
                                                                    credits: {
                                                                        enabled: false,
                                                                    },
                                                                    exporting: {
                                                                        enabled: false,
                                                                    },
                                                                    legend: {
                                                                        enabled: true,
                                                                    },
                                                                    title: {
                                                                        text: ''
                                                                    },
                                                                    subtitle: {
                                                                        text: ''
                                                                    },
                                                                    tooltip: {
                                                                        pointFormat: ptfmt
                                                                    },
                                                                    plotOptions: {
                                                                        pie: {
                                                                            innerSize: 100,
                                                                            depth: 45,
                                                                            dataLabels: {enabled: false,
                                                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                                                                style: {
                                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                                }
                                                                            },
                                                                        },
                                                                    },
                                                                    series: [{
                                                                            showInLegend: true,
                                                                            name: '',
                                                                            data: arr
                                                                        }]
                                                                });
                                                            }

                                                            function barChart(idc, series) {
                                                                Highcharts.setOptions({
                                                                    colors: ['#606065', '#c64b38', '#2d2a26']
                                                                });
                                                                Highcharts.chart(idc, {
                                                                    chart: {
                                                                        type: 'column'
                                                                    },
                                                                    title: {
                                                                        text: ''
                                                                    },
                                                                    subtitle: {
                                                                        text: ''},
                                                                    xAxis: {
                                                                        categories: [''],
                                                                        crosshair: true
                                                                    },
                                                                    yAxis: {
                                                                        min: 0,
                                                                        max: 30,
                                                                        tickInterval: 5,
                                                                        labels: {
                                                                            format: '{value}%'
                                                                        },
                                                                        title: {
                                                                            text: ''
                                                                        }
                                                                    },
                                                                    tooltip: {
                                                                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                                                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                                                                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                                                        footerFormat: '</table>',
                                                                        shared: true,
                                                                        useHTML: true
                                                                    },
                                                                    credits: {
                                                                        enabled: false
                                                                    },
                                                                    navigation: {
                                                                        buttonOptions: {
                                                                            enabled: false
                                                                        }
                                                                    },
                                                                    plotOptions: {
                                                                        column: {
                                                                            pointPadding: 0,
                                                                            borderWidth: 0
                                                                        }
                                                                    },
                                                                    series: series
                                                                });
                                                            }
                                                        </script>
                                                        <script>
                                                            Highcharts.chart('stockbalance', {
                                                                chart: {
                                                                    type: 'spline',
                                                                },
                                                                title: {
                                                                    text: ''
                                                                },
                                                                subtitle: {

                                                                },
                                                                xAxis: {
                                                                    categories: [
                                                                        '2017 Jan', '2017 Feb', '2017 Mar', '2017 Apr', '2017 May', '2017 Jun', '2017 Jul', '2017 Aug', '2017 Sep', '2017 Oct', '2017 Nov', '2017 Dec',
                                                                        '2018 Jan', '2018 Feb', '2018 Mar', '2018 Apr', '2018 May', '2018 Jun', '2018 Jul', '2018 Aug', '2018 Sep', '2018 Oct', '2018 Nov', '2018 Dec',
                                                                        '2019 Jan', '2019 Feb', '2019 Mar', '2019 Apr', '2019 May', '2019 Jun', '2019 Jul'],
                                                                    tickInterval: 1,
                                                                    labels: {
                                                                        style: {
                                                                            color: '#333',
                                                                            fontSize: '12px',
                                                                            textTransform: 'uppercase'
                                                                        },
                                                                        y: 20,
                                                                        x: 10
                                                                    },
                                                                    lineColor: '#dadada'
                                                                },
                                                                yAxis: {
                                                                    title: {
                                                                        text: ''
                                                                    }
                                                                },
                                                                credits: {
                                                                    enabled: false
                                                                },
                                                                navigation: {
                                                                    buttonOptions: {
                                                                        enabled: false
                                                                    }
                                                                },
                                                                legend: {
                                                                    layout: 'vertical',
                                                                    align: 'center',
                                                                    verticalAlign: 'bottom',
                                                                    enabled: false

                                                                },
                                                                plotOptions: {
                                                                    series: {
                                                                        color: '#93dbd0',
                                                                        shadow: true,
                                                                        lineWidth: 3,
                                                                        marker: {
                                                                            enabled: false
                                                                        }
                                                                    }
                                                                },
                                                                tooltip: {
                                                                    pointFormatter: function () {
                                                                        var isNegative = this.y < 0 ? '-' : '';
                                                                        return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                                                                    }
                                                                },
                                                                series: [{
                                                                        name: '',
                                                                        data: [90688,
                                                                            90688 + 970.00,
                                                                            90688 + 888.80,
                                                                            90688 + 1009.60,
                                                                            90688 + 1202.45,
                                                                            90688 + 1460.47,
                                                                            90688 + 1800.66,
                                                                            90688 + 2092.99,
                                                                            90688 + 2207.57,
                                                                            90688 + 2654.50,
                                                                            90688 + 3037.57,
                                                                            90688 + 4883.94,
                                                                            90688 + 5762.66,
                                                                            90688 + 6864.18,
                                                                            90688 + 7903.55,
                                                                            90688 + 9018.43,
                                                                            90688 + 10095.45,
                                                                            90688 + 13109.44,
                                                                            90688 + 19630.21,
                                                                            90688 + 22010.11,
                                                                            113081.42,
                                                                            114035.62,
                                                                            115055.92,
                                                                            117057.41,
                                                                            120343.07,
                                                                            123068.20,
                                                                            125022.82,
                                                                            127051.06,
                                                                            128001.64,
                                                                            130004.75,
                                                                            134644.35]
                                                                    }],
                                                                responsive: {
                                                                    rules: [{
                                                                            condition: {
                                                                                maxWidth: 500
                                                                            }
                                                                        }]
                                                                }

                                                            });
                                                        </script>  
                                                        </body>
                                                        </html>