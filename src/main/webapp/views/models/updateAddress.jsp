<%-- 
    Document   : updateAddress
    Created on : Sep 27, 2019, 11:16:46 AM
    Author     : ADMIN
--%>

<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="addressModel" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Address Detail</h4>
                </div>
                <div class="modal-body new-model-text">
                    <input type="hidden" id="beneIdAdd" class="beneId" value="">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Country of residence
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style country-style">
                                <input type="text" class=" tex_residence_Country form-control countryname"  id="countryname2" name="countryCode" value="" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                <span class="error" id="error_bank_name"></span>
                            </div>
                        </div>
                        <!--                        <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Type
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 details-pos">
                                                    <div class="profile-form-style">
                                                    <select class="selectoption  form-group aml-select" id="address_type" value="">
                                                        <option value="No Type Selected">-Select-</option>
                                                        <option value="Postal">Postal</option>
                                                        <option value="Registered_Office">Registered_Office</option>
                                                        <option value="Residential">Residential</option>
                                                        <option value="Other" class="otherBank">Other</option>
                                                    </select>
                                                    <span class="error" id="error_bank_name"></span>
                                                </div>
                                                </div>-->
                        <div class="col-sm-6">
                            <label class="label_input" style="text-align:left">
                                Address
                            </label>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="profile-form-style">
                                <input type="hidden" name="postal_code" id="postal_code" class=" form-control input-field all-address" value=""/>
                                <input type="hidden" name="addressId" id="addressId" class=" form-control" value=""/>
                                <input type="text" id="all-address" class=" homeAddress form-control input-field" placeholder="Enter home address" value="" >
                                <span class="error" id="error_address"></span>
                            </div>
                        </div>
                        <!--                        <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Address Line 2
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" id="address1" class=" homeAddress form-control input-field" placeholder="Enter home address" value="" autocomplete="off">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Address Line 3
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" id="address2" class=" homeAddress form-control input-field" placeholder="Enter home address" value="" autocomplete="off">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Address Line 4
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" id="address3" class=" homeAddress form-control input-field" placeholder="Enter home address" value="" autocomplete="off">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        City
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" class="account_number form-control input-field" id="city" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Region
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" class="account_number form-control input-field" id="region" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        State
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" class="account_number form-control input-field" id="state" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Postal Code
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" class="account_number form-control input-field" id="postalcode" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Effective Date
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                    <input type="text" class="account_number form-control input-field" id="effectivedate" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                    <span class="error" id="error_acount_holder_number"></span>
                                                </div>
                                                </div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info address-save" >Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


    $('#all-address').keyup(function () {
        $('#error_address').text('');
    });

//    $('.address-save').click( function () {
    $('.address-save').bind('click', function () {
        console.log("-----------------------------------------------------------number of clicks 1");
        var country = $('#countryname2').val();
        var addressId = $('#addressId').val();
        var address = $('#all-address').val();
//        var address1 = $('#address1').val();
//        var address2 = $('#address2').val();
//        var address3 = $('#address3').val();
//        var city = $('#city').val();
//        var region = $('#region').val();
//        var state = $('#state').val();
        var postalcode = $('#postal_code').val();
//        alert(postalcode);
//        var effectivedate = $('#effectivedate').val();
        var beneId = $('#beneIdAdd').val();
//        alert(beneId);
        var obj = {country: country, postalCode: postalcode, addressLine1: address, beneficiaryId: beneId,addressID:addressId};
//        var obj = {country: country, type: address_type, addressLine1: address, addressLine2: address1,
//            addressLine3: address2, addressLine4: address3, city: city,
//            region: region, state: state, postalCode: postalcode, effectiveDate: effectivedate, beneficiaryId: beneId};
        if (address.trim() === "") {
            $('#error_address').text('Please enter the valid address.');
        } else {
//        alert(JSON.stringify(obj));
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/api/address-registeration',
                headers: {"Content-Type": 'application/json'},
                data: JSON.stringify(obj),
                success: function (data, textStatus, jqXHR) {
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                    console.log(data);


                    if (obj.message === "true") {
                        swal({
                            title: "Sucess",
                            text: "Your address has been updated successfully.",
                            type: "info",
                            timer: 3500,
                            showConfirmButton: true
                        });
                    } else {
                        swal({
                            title: "Failed",
                            text: "Please enter the valid address.",
                            type: "info",
                            timer: 3500,
                            showConfirmButton: true
                        });
                    }
                    $('.address-save').unbind('click');

                    location.reload();


                }, error: function (jqXHR, textStatus, errorThrown) {
                    $('.address-save').unbind('click');
//                alert(textStatus);
                }
            });
        }
    });

//    $(".countryname").intlTelInput_2();

</script>
