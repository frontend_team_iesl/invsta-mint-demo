<%-- 
    Document   : updateAddress
    Created on : Sep 27, 2019, 11:16:46 AM
    Author     : ADMIN
--%>

<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade documentVerify" id="" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Address Detail</h4>
                </div>
                <div class="modal-body new-model-text">
                    <input type="hidden" id="id" class="id" value="">
                    <input type="hidden" id="regId" class="regId" value="">
                    <input type="hidden" id="regType" class="regType" value="">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="zoo-content_body">
                                <form id="msform">
                                    <fieldset id="step1" style="display: none;">
                                        <div class="country-option-header">
                                            <span class="region-text">Region / Country</span>
                                            <span class="region-text-icon"><a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i></a></span>
                                        </div>
                                        <div class="zoo-content-section">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="select-country__option">Select a Region / Country to view your subscribed data sources:</p>
                                                    <div class="select-country__region-header">
                                                        <h5 class="select-country__region-name">APAC</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="select-radios-zoo" style="">
                                                        <p>
                                                            <input type="radio" id="test1" name="radio-group" value="Australia">
                                                            Australia <label for="test1"></label>
                                                        </p>
                                                        <p>
                                                            <input type="radio" id="test2" name="radio-group" value="Cambodia">
                                                            Cambodia <label for="test2"></label>
                                                        </p>
                                                        <p>
                                                            <input type="radio" id="test3" name="radio-group" value=" New Zealand">
                                                            New Zealand <label for="test3"></label>
                                                        </p>
                                                        <p>
                                                            <input type="radio" id="test4" name="radio-group" value="Vietnam">
                                                            Vietnam <label for="test4"></label>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="zoo-btns">
                                                        <!--<input type="button" name="previous" value="Previous Page" class="previous1 country-option-btn">-->
                                                        <input type="button" name="previous" value="Next Page" class="next1 country-option-btn">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step2" style="display: none;">
                                        <div class="country-option-header">
                                            <span class="region-text region"> New Zealand</span>
                                            <span class="region-text-icon"><a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i></a></span>
                                        </div>
                                        <div class="zoo-content-section">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="select-country__option">Select data source(s):</p>

                                                </div>
                                                <div class="col-md-12">
                                                    <div class="select-radios-zoo-check">
                                                        <ul class="unstyled centered">
                                                            <li class="lable-cut">
                                                                <input class="styled-checkbox idtype" name="idtype" id="styled-checkbox-1" type="radio" value="license">
                                                                Driver's Licence<label for="styled-checkbox-1"></label>
                                                            </li>
                                                            <li class="lable-cut">
                                                                <input class="styled-checkbox idtype" name="idtype" id="styled-checkbox-2" type="radio" value="passport">
                                                                Passport<label for="styled-checkbox-2"></label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="zoo-btns">
                                                        <input type="button" name="previous" value="Previous Page" class="previous2 country-option-btn">
                                                        <input type="button" name="previous" value="Next Page" class="next2 country-option-btn">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="step3" style="display: none;">
                                        <div class="country-option-header">
                                            <span class="region-text region"> New Zealand</span>
                                            <!--<span class="region-text-icon"><a href=""><i class="fa fa-pencil" aria-hidden="true"></i></a></span>-->
                                        </div>
                                        <div class="zoo-content-section">
                                            <div class="row">
                                                <div class="col-md-12 mt-3">
                                                    <p class="select-country__option userdata"> New Zealand Consumer Data </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="country-option-header">
                                                        <span class="region-text">Personal Information</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>FIRST NAME</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle first_name_right " style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close first_name_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data" name="first_name" id="first_name">
                                                                    <span class="error" id="error_first_name"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>MIDDLE NAME</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle middle_name_right" style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close middle_name_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data" name="middle_name" id="middle_name">
                                                                    <span class="error" id="error_first_name"></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>LAST NAME</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle last_name_right" style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close last_name_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data" name="last_name" id="last_name">
                                                                    <span class="error" id="error_last_name"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>DATE OF BIRTH</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle dob_right" style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close dob_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="dd/mm/yyyy" class="input-data  dob date_of_birth" name="date_of_birth" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 passport" style="display: block">
                                                    <div class="row">
                                                        <div class="col-md-12 mt-3 ">
                                                            <div class="country-option-header">
                                                                <span class="region-text">Passport Details</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="select-zoo-form">
                                                                <h6>Passport Number</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle passport_number_right" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data " name="passport_number" id="Passportnumber">
                                                                    <span class="error" id="spanPassportnumber"></span>
                                                                </div></div>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <div class="select-zoo-form">
                                                                <h6>Passport Expiry</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle passport_expiry_right" style="display: none;"></i>
                                                                    <input type="text" placeholder="dd/mm/yyyy" class="input-data  passportExp exp" name="passport_expiry" >
                                                                    <span class="error" id="error-passportExpiry"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 license" style="display: block">
                                                    <div class="row">
                                                        <div class="col-md-12  mt-3 ">
                                                            <div class="country-option-header">
                                                                <span class="region-text">Driving Licence details</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>Licence Number</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle License_number_right" style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close License_number_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data" name="license_number" id="licenseNumber">
                                                                    <span class="error" id="spanlicenseNumber"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="select-zoo-form">
                                                                <h6>Licence Version</h6>
                                                                <div class="add-check-btn">
                                                                    <i class="fa fa-check-circle lic_verson_number_right" style="display: none;"></i>
                                                                    <i class="fa fa-remove btn-close lic_verson_number_wrong" style="display: none;"></i>
                                                                    <input type="text" placeholder="" class="input-data" name="license_version" id="Versionnumber">
                                                                    <span class="error" id="spanVersionnumber"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="zoo-btns">
                                                        <input type="button" name="previous" value="Previous Page" class="previous3 country-option-btn">
                                                        <input type="button" name="previous" value="Verify" class="country-option-btn verify-dl">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info address-save" style="display:none">Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        adultDOB = date.setFullYear(year - 18, month, day);
        $(".dob").datepicker({
            yearRange: (year - 80) + ':' + year,
            changeMonth: true,
            changeYear: true,
            maxDate: new Date(adultDOB),
            dateFormat: 'dd/mm/yy'
        }).datepicker().attr('readonly', 'readonly');
        $(".exp").datepicker({
//          yearRange: (year) + ':' + (year + 80),
            yearRange: (year - 80) + ':' + (year + 80),
            changeMonth: true,
            changeYear: true,
//            maxDate: new Date(),
            dateFormat: 'dd/mm/yy'
        }).datepicker().attr('readonly', 'readonly');
        $('#step1').show();

    });
    $('.previous2').click(function () {
        $('#step1').show();
        $('#step2').hide();
    });
    $('.previous3').click(function () {
        $('#step2').show();
        $('#step3').hide();
    });
    $('.next1').click(function () {
        $('#step1').hide();
        $('#step2').show();
    });
    $('.next2').click(function () {
        var idtype = $('.idtype:checked').val();
        if (idtype === "license") {
            $('.license').show();
            $('.passport').hide();
            $("#step3").show();
            $("#step2").hide();

        } else if (idtype === "passport") {
            $('.license').hide();
            $('.passport').show();
            $("#step3").show();
            $("#step2").hide();
        }
    });
    $('.input-data').keyup(function () {
        $('.error').text('');
    });
    $(".verify-dl").click(function () {
        var url = '';
        var idType = '';
        var issuedBy = "New Zealand";
        var id = $('#id').val();
        var regId = $('#regId').val();
        var regType = $('#regType').val();
        var firstName = $('input[name=first_name]').val();
        var middleName = $('input[name=middle_name]').val();
        var lastName = $('input[name="last_name"]').val();
        var idtype = $('.idtype:checked').val();
        if (idtype === "license") {
            url = './rest/groot/db/api/dl-verification';
            idType = "NZ Driver Licence";
            var License_number = $('input[name="license_number"]').val();
            var licence_verson_number = $('input[name="license_version"]').val();
            if (firstName === "") {
                $("#error_first_name").text("This field is required ");
                return false;
            } else if (lastName === "") {
                $("#error_last_name").text("This field is required ");
                return false;
            } else if (License_number === "") {
                $("#spanlicenseNumber").text("This field is required ");
                return false;
            } else if (licence_verson_number === "") {
                $("#spanVersionnumber").text("This field is required ");
                return false;
            }
        } else if (idtype === "passport") {
            url = './rest/groot/db/api/pp-verification';
            idType = "NZ Passport";
            var passport_number = $('input[name="passport_number"]').val();
            var passport_expiry = $('input[name="passport_expiry"]').val();
            if (firstName === "") {
                $("#error_first_name").text("This field is required ");
                return false;
            } else if (lastName === "") {
                $("#error_last_name").text("This field is required ");
                return false;
            } else if (passport_number === "") {
                $("#spanPassportnumber").text("This field is required ");
                return false;
            }
        }
        var Date_of_Birth = $('input[name="date_of_birth"]').val();
        var DataObj = {license_number: License_number, id_type: idType, reg_type: regType, issuedBy: issuedBy, id: id,
            licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName, reg_id: regId,
            middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
        console.log(JSON.stringify(DataObj));
        alert(JSON.stringify(DataObj));

        swal({
            title: "Processing",
            text: "Please wait while we are verifying your details.",
            type: "info",
            timer: 3500,
            showConfirmButton: true
        });
        $.ajax({
            type: 'POST',
            url: url,
            headers: {"Content-Type": 'application/json'},
            data: JSON.stringify(DataObj),
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                var obj = JSON.parse(data);
                console.log(JSON.stringify(obj));
                if (idtype === "license") {
                    if (obj.driversLicence.verified) {
                        swal({
                            title: "Success",
                            text: "Data is Verified.",
                            type: "info",
//                            timer: 3500,
                            showConfirmButton: true
                        }, function () {
                            window.location = "./home";
                        });
                        if (obj.driversLicence.fields.firstName === "Yes") {
                            $('.first_name_wrong').hide();
                            $('.first_name_right').show();


                        } else if (obj.driversLicence.fields.firstName === "No") {

                            $('.first_name_right').hide();
                            $('.first_name_wrong').show();

                        }
                        if (obj.driversLicence.fields.middleName === "Yes") {
                            $('.middle_name_wrong').hide();
                            $('.middle_name_right').show();

                        } else if (obj.driversLicence.fields.middleName === "No") {
                            $('.middle_name_right').hide();
                            $('.middle_name_wrong').show();

                        }
                        if (obj.driversLicence.fields.lastName === "Yes") {
                            $('.last_name_right').show();
                            $('.last_name_wrong').hide();

                        } else if (obj.driversLicence.fields.lastName === "No") {

                            $('.last_name_right').hide();
                            $('.last_name_wrong').show();

                        }
                        if (obj.driversLicence.fields.dateOfBirth === "Yes") {
                            $('.dob_wrong').hide();
                            $('.dob_right').show();

                        } else if (obj.driversLicence.fields.dateOfBirth === "No") {

                            $('.dob_right').hide();
                            $('.dob_wrong').show();

                        }
                        if (obj.driversLicence.fields.licenceNo === "Yes") {
                            $('.License_number_wrong').hide();
                            $('.License_number_right').show();

                        } else if (obj.driversLicence.fields.licenceNo === "No") {

                            $('.License_number_right').hide();
                            $('.License_number_wrong').show();

                        }

                    } else {
                        swal({
                            title: "Failed",
                            text: "Wrong Detils.",
                            type: "info",
                            timer: 3500,
                            showConfirmButton: true
                        });
                        $('.fa-check-circle ').hide();
                        $('.btn-close').hide();


//                                alert("wrong data");
                    }
                } else if (idtype === "passport") {
                    if (obj.passport.verified) {
                        swal({
                            title: "Success",
                            text: "Data is Verified.",
                            type: "info",
//                            timer: 3500,
                            showConfirmButton: true
                        }, function () {
                            window.location = "./home";
                        });

                        $('.first_name_right').show();
                        $('.middle_name_right').show();
                        $('.last_name_right').show();
                        $('.dob_right').show();
                        $('.passport_expiry_right').show();
                        $('.passport_number_right').show();


                    } else {
                        swal({
                            title: "Failed",
                            text: "Wrong Detils.",
                            type: "info",
                            timer: 3500,
                            showConfirmButton: true
                        });
                        $('.first_name_right').hide();
                        $('.middle_name_right').hide();
                        $('.last_name_right').hide();
                        $('.dob_right').hide();
                        $('.passport_expiry_right').hide();
                        $('.passport_number_right').hide();
                        $('.fa-check-circle ').hide();
//                                alert("wrong data");
                    }

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(" inside error" + jqXHR);
            }
        });
    });

</script>
