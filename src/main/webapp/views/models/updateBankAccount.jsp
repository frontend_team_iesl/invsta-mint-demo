<%-- 
    Document   : updateBankAccount
    Created on : Sep 27, 2019, 11:36:06 AM
    Author     : ADMIN
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="bankModel" role="dialog">
        <div class="modal-dialog bank-model-set">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bank Account Detail</h4>
                </div>
                <div class="modal-body new-model-text">
                    <input type="hidden" class="account_number form-control input-field" id="Currency" name="Currency" value="" required="required" placeholder="Enter  Currency" onkeydown="checkAccountNO(this)">

                    <input type="hidden" id="investIdBank" value="">  
                    <input type="hidden" id="beneIdBank"  value="">  
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Bank name 
                            </label>
                        </div>
                        <!--                        <div class="col-sm-6 details-pos">
                                                    <div class="profile-form-style">
                                                        <select class="selectoption  bank_name form-group aml-select" id="bank_name" value="">
                                                            <option value="0">-Select-</option>
                                                            <option value="Bank of New Zealand" data-id="02">Bank of New Zealand</option>
                                                            <option value="ANZ Bank New Zealand" data-id="01">ANZ Bank New Zealand</option>
                                                            <option value="ASB Bank" data-id="12">ASB Bank</option>
                                                            <option value="Westpac" data-id="03">Westpac</option>
                                                            <option value="Heartland Bank" data-id="03">Heartland Bank</option>
                                                            <option value="Kiwibank" data-id="38">Kiwibank</option>
                                                            <option value="SBS Bank" data-id="03">SBS Bank</option>
                                                            <option value="TSB Bank" data-id="15">TSB Bank</option>
                                                            <option value="The Co-operative Bank" data-id="02">The Co-operative Bank</option>
                                                            <option value="NZCU" data-id="03">NZCU</option>
                                                            <option value="Rabobank New Zealand" data-id="03">Rabobank New Zealand</option>
                                                            <option value="National Bank of New Zealand" data-id="">National Bank of New Zealand</option>
                                                            <option value="National Australia Bank" data-id="08">National Australia Bank</option>
                                                            <option value="Industrial and Commercial Bank of China" data-id="10">Industrial and Commercial Bank of China</option>
                                                            <option value="PostBank" data-id="11">PostBank</option>
                                                            <option value="Trust Bank Southland" data-id="13">Trust Bank Southland</option>
                                                            <option value="Trust Bank Otago" data-id="14">Trust Bank Otago</option>
                                                            <option value="Trust Bank Canterbury" data-id="16">Trust Bank Canterbury</option>
                                                            <option value="Trust Bank Waikato" data-id="17">Trust Bank Waikato</option>
                                                            <option value="Trust Bank Bay of Plenty" data-id="18">Trust Bank Bay of Plenty</option>
                                                            <option value="Trust Bank South Canterbury" data-id="19">Trust Bank South Canterbury</option>
                                                            <option value="Trust Bank Auckland" data-id="21">Trust Bank Auckland</option>
                                                            <option value="Trust Bank Central" data-id="20">Trust Bank Central</option>
                                                            <option value="Trust Bank Wanganui" data-id="22">Trust Bank Wanganui</option>
                                                            <option value="Westland Bank" data-id="24">Westland Bank</option>
                                                            <option value="Trust Bank Wellington" data-id="23">Trust Bank Wellington</option>
                                                            <option value="Countrywide" data-id="25">Countrywide</option>
                                                            <option value="United Bank" data-id="29">United Bank</option>
                                                            <option value="HSBC" data-id="30">HSBC</option>
                                                            <option value="Citibank" data-id="31">Citibank</option>
                                                            <option value="Other" class="otherBank">Other</option>
                                                        </select>
                                                        <span class="error" id="error_bank_name"></span>
                                                    </div>
                                                </div>-->
                        <div class="col-sm-6 bank-margin1">
                            <!--                            <div class="bank-img-icon bank-logo-all">
                                                            <ul class="prod-gram">
                                                                <div><li class="init">-Select-</li>
                                                                <li><img src='./resources/images/bank/BNZ.png' />Bank of New Zealand</li>
                                                                <li><img src='./resources/images/bank/anz.png' />ANZ Bank New Zealand</li>
                                                                <li><img src='./resources/images/bank/ASB.jpg' />ASB Bank</li>
                                                                <li><img src='./resources/images/bank/Westpac.png' />Westpac</li>
                                                                <li><img src='./resources/images/bank/heartland.png' />Heartland Bank</li>
                                                                <li><img src='./resources/images/bank/Kiwibank.png' />Kiwibank</li>
                                                                <li><img src='./resources/images/bank/sbs.jpg' />SBS Bank</li>
                                                                <li><img src='./resources/images/bank/TSB.jpg' />TSB Bank</li>
                                                                <li><img src='./resources/images/bank/Co-operative.jpg' />The Co-operative Bank</li>
                                                                <li><img src='./resources/images/bank/NZCU.png' />NZCU</li>
                                                                <li><img src='./resources/images/bank/Rabobank.jpg' />Rabobank New Zealand</li>
                                                                <li><img src='./resources/images/bank/national.png' />National Bank of New Zealand</li>
                                                                <li><img src='./resources/images/bank/nab.jpg' />National Australia Bank</li>
                                                                <li><img src='./resources/images/bank/ICBC.png' />Industrial and Commercial Bank of China</li>
                                                                <li><img src='./resources/images/bank/PostBank.jpg' />PostBank</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Southland</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Otago</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Canterbury</li>
                                                                <li><img src='./resources/images/bank/trust-waikato.jpg' />Trust Bank Waikato</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Bay of Plenty</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank South Canterbury</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Auckland</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Central</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wanganui</li>
                                                                <li><img src='./resources/images/bank/westland.png' />Westland Bank</li>
                                                                <li><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wellington</li>
                                                                <li><img src='./resources/images/bank/countrywide.jpg' />Countrywide</li>
                                                                <li><img src='./resources/images/bank/united.jpg' />United Bank</li>
                                                                <li><img src='./resources/images/bank/HSBC.jpg' />HSBC</li>
                                                                <li><img src='./resources/images/bank/city-bank.png' />Citibank</li>
                                                                <li>Other</li>
                                                                </div>
                                                            </ul>
                                                        </div>-->
                            <div class="mm-dropdown">
                                <div class="textfirst">-Select-<img class="drop-img" style="width:10px" src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" /></div>
                                <ul class="select">
                                    <c:forEach items="${allBankCode}" var="bankCode" varStatus="idx">

                                        <li class="input-option" data-id="${bankCode.bankCode}" data-value="${bankCode.bankName}"><img src="${bankCode.bankLogo}" />${bankCode.bankName}</li>

                                        <%--<li class="input-option" data-value="02"><img src="./resources/images/bank/BNZ.png" alt=""/>'${bankCode.bankName}'</li>--%>

                                    </c:forEach>
                                    <!--                                    <li class="input-option" data-value="02"><img src="./resources/images/bank/BNZ.png" alt=""/>Bank of New Zealand</li>
                                                                        <li class="input-option" data-value="01"><img src="./resources/images/bank/anz.png" alt=""/>ANZ Bank New Zealand</li>
                                                                        <li class="input-option" data-value="12"><img src="./resources/images/bank/ASB.jpg" alt=""/>ASB Bank</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/Westpac.png' />Westpac</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/heartland.png' />Heartland Bank</li>
                                                                        <li class="input-option" data-value="38"><img src='./resources/images/bank/Kiwibank.png' />Kiwibank</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/sbs.jpg' />SBS Bank</li>
                                                                        <li class="input-option" data-value="15"><img src='./resources/images/bank/TSB.jpg' />TSB Bank</li>
                                                                        <li class="input-option" data-value="02"><img src='./resources/images/bank/Co-operative.jpg' />The Co-operative Bank</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/NZCU.png' />NZCU</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/Rabobank.jpg' />Rabobank New Zealand</li>
                                                                        <li class="input-option" data-value="03"><img src='./resources/images/bank/national.png' />National Bank of New Zealand</li>
                                                                        <li class="input-option" data-value="08"><img src='./resources/images/bank/nab.jpg' />National Australia Bank</li>
                                                                        <li class="input-option" data-value="10"><img src='./resources/images/bank/ICBC.png' />ICBC</li>
                                                                        <li class="input-option" data-value="11"><img src='./resources/images/bank/PostBank.jpg' />Post Bank</li>
                                                                        <li class="input-option" data-value="13"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Southland</li>
                                                                        <li class="input-option" data-value="14"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Otago</li>
                                                                        <li class="input-option" data-value="16"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Canterbury</li>
                                                                        <li class="input-option" data-value="17"><img src='./resources/images/bank/trust-waikato.jpg' />Trust Bank Waikato</li>
                                                                        <li class="input-option" data-value="18"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Bay of Plenty</li>
                                                                        <li class="input-option" data-value="19"><img src='./resources/images/bank/trust-bank.png' />Trust Bank South Canterbury</li>
                                                                        <li class="input-option" data-value="21"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Auckland</li>
                                                                        <li class="input-option" data-value="20"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Central</li>
                                                                        <li class="input-option" data-value="22"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wanganui</li>
                                                                        <li class="input-option" data-value="24"><img src='./resources/images/bank/westland.png' />Westland Bank</li>
                                                                        <li class="input-option" data-value="23"><img src='./resources/images/bank/trust-bank.png' />Trust Bank Wellington</li>
                                                                        <li class="input-option" data-value="25"><img src='./resources/images/bank/countrywide.jpg' />Countrywide</li>
                                                                        <li class="input-option" data-value="29"><img src='./resources/images/bank/united.jpg' />United Bank</li>
                                                                        <li class="input-option" data-value="30"><img src='./resources/images/bank/HSBC.jpg' />HSBC</li>
                                                                        <li class="input-option" data-value="31"><img src='./resources/images/bank/city-bank.png' />Citibank</li>-->
                                    <li class="input-option" data-value="">Other</li>
                                </ul>
                                <span class="error" id="error_bank_name"></span>
                                <input type="hidden" class="option" name="namesubmit" value="" />
                                <input type="hidden" class="bank_name1" name="bank_name1" id="bank_name1" value="" />
                            </div>
                        </div>
                        <div class="col-sm-6 toggal_other mt-2">
                            <label class="label_input" style="text-align:left">
                                Other Bank name
                            </label>
                        </div>
                        <div class="col-sm-6 toggal_other profile-form-style">
                            <input type="text" class="Other_Bank_name form-control input-field" name="other_bank_name" id="other_bank_name" value="" required="required" placeholder="Bank name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                            <span class="error" id="error_Other_Bank_name"></span>
                        </div>



                        <div class="col-sm-6 mt-2">
                            <label class="label_input" style="text-align:left">
                                Account name
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <div class="profile-form-style">
                                <input type="text" class="acount_holder_name form-control input-field" name="acount_holder_name" id="acount_holder_name" value="" required="required" placeholder="Enter your account name" onkeypress="return ((event.charCode >= 65 & amp; & amp; event.charCode <= 90) || (event.charCode >= 97 & amp; & amp; event.charCode <= 122) || (event.charCode == 32))">
                                <span class="error" id="error_acount_holder_name"></span>
                            </div>
                        </div>
                        <div class="col-sm-6 mt-2">
                            <label class="label_input" style="text-align:left">
                                Account number
                            </label>
                        </div>
                        <div class="col-sm-6 validate-ird">
                            <div class="profile-form-style">
                                <input type="text" class="account_number form-control input-field" id="account_number" name="acount_holder_number" value="" required="required" placeholder="xx-xxxx-xxxxxxx-xxx"   onkeypress="myFunction7()">
                                <a class="account-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                <input type="hidden" class="isAccount_number_verified " />
                                <span class="error" id="error_acount_holder_number"></span>
                            </div>
                        </div>
                        <!--                        <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Branch
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                        <input type="text" class="account_number form-control input-field" id="Branch" name="Branch" value="" required="required" placeholder="Enter Bank Branch " onkeydown="checkAccountNO(this)">
                                                        <span class="error" id="error_acount_holder_number"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Suffix
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                        <input type="tel" class="account_number form-control input-field" id="Suffix" name="Suffix" value="" required="required" placeholder="Enter Suffix" onkeydown="checkAccountNO(this)">
                                                        <span class="error" id="error_acount_holder_number"></span>
                                                    </div>
                                                </div>-->
                        <!--                        <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Currency
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <div class="profile-form-style">
                                                        <span class="error" id="error_acount_holder_number"></span>
                                                    </div>
                                                </div>-->
                        <!-- <div class="col-sm-129">
                                <input type="file" name="myFile" class="attach-btn2">
                        </div> -->
                        <div class="col-md-12">
                            <div class="new-attach-info">
                                <!--                                <div class="col-sm-129 closestcls">
                                                                    <input type="file" name="myFile" id="bank_document" class="attach-btn2 checkname" onchange="documentFileUpload(this);" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatadiv(this);"><i class=" removefile far fa-times-circle" style="display: none;"></i></a>
                                                                    <span class="error" id="error_bank_document"></span>
                                                                </div>-->
                                <div class="new-attach-btn">
                                    <input id="bank_doc" type="file" placeholder="Add profile picture"  onchange="documentFileUpload(this);"  accept="image/x-png,image/jpeg,image/*,.txt,.pdf" />
                                    <label for="bank_doc">Attach copy of statement</label>
                                    <div class="pulsating-circle2 pulsating-around2 profile-bank-info" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                        <span class="tooltiptext dashboard-toltip">To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking.</span>
                                    </div>
                                    <span class="error" id="error_bank_document"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info bank-save" >Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</div>
<script src="./resources/js/bankValidator/bankValidator.js"></script>
<script>

    $(document).ready(function () {
//                $(".otpNext").attr("disabled", true);
        $('.account-check').hide();
    });

    $('#account_number').keyup(function () {
        $("#error_acount_holder_number").text('');
        var accountnumber = $('#account_number').val();
        $('.account-check').hide();
        $('.isAccount_number_verified').val('false');
//                                               alert(accountnumber.length);
        if (accountnumber.length === 19) {

            var AccountArray = accountnumber.split('-');
//                                                            alert(AccountArray);
            var bk = AccountArray[0];
            var brch = AccountArray[1];
            var acct = AccountArray[2];
            var suf = AccountArray[3];
//                                                            alert(bk + "  --" + brch + "  " + acct + "   " + suf);
            var resl = isValidNZBankNumber(bk, brch, acct, suf);
//                                                            alert(resl);
            if (resl) {
//                alert("1");
                $('.account-check').show();
                $('.isAccount_number_verified').val('true');

            } else {
//                  alert("2");
                $('.isAccount_number_verified').val('false');
                $('#error_acount_holder_number').text('please enter a valid Account number.');

            }
        }

    });






    $('.bank_name1').change(function () {

        $("#error_bank_name").text('');
    });
    $('#acount_holder_name').keyup(function () {
        $("#error_acount_holder_name").text('');
    });
//    $('#account_number').keyup(function () {
//        $("#error_acount_holder_number").text('');
//    });
    $('#bank_document').change(function () {
        $("#error_bank_document").text('');
    });


    var bankFile = '';
    var otherFile = '';
    function documentFileUpload(selectedfile) {
//                                                        var selectedfile = document.getElementById("bank_document").files;
        var id = selectedfile.id;
        id = id.trim();
        selectedfile = selectedfile.files;
        if (selectedfile.length > 0) {
            var imageFile = selectedfile[0];
            var fileReader = new FileReader();
            fileReader.onload = function (fileLoadedEvent) {
                if (id === "bank_doc") {
                    bankFile = fileLoadedEvent.target.result;
                    console.log("bankFileSRC" + bankFile);
                }
                if (id === "other_id_myFile") {
                    otherFile = fileLoadedEvent.target.result;
                    console.log("otherFileSRC" + otherFile);
                }
            };
            fileReader.readAsDataURL(imageFile);
        }
    }
    $("#other_bank_name").keyup(function () {
        $('#error_Other_Bank_name').text('');
    });
    $("#bank_doc").change(function () {
        $("#error_bank_document").text('');
    });


    $(".select li").click(function () {
        $("#error_acount_holder_name").text('');
        $("#error_acount_holder_number").text('');
        $("#error_bank_document").text('');
        $('#error_bank_name').text('');
        $('#error_Other_Bank_name').text('');

        var a = $(this).data('id');
//        var a = $(this).data('value');
        var b = $(this).text();
        $('#account_number').val(a);
        $('#bank_name1').val(b);
        if ($(this).text() === 'Other') {
            $('.toggal_other').show();
        } else {
            $('.toggal_other').hide();
        }
    });




    $('.bank-save').bind('click', function () {

//        alert();
        var investId = $("#investIdBank").val();
        var beneId = $("#beneIdBank").val();
        var bankName = $("#bank_name1").val();
//        alert(bankName);

        var OtherBank = $("#other_bank_name").val();

//        alert(bankName);
//        var bankName = $('.bank_name option:selected').data('id');
        var accountName = $("#acount_holder_name").val();
        var accountNumber = $("#account_number").val();
        var branch = $("#Branch").val();
        var suffix = $("#Suffix").val();
        var ifAcVErified = $('.isAccount_number_verified').val();
//        alert(bankName);
        var bank_document = $("#bank_doc").val();
        if (bankName === "") {

            $("#error_bank_name").text(" please select a bank name.");
        } else if (bankName === "Other" && OtherBank === "") {

            $("#error_Other_Bank_name").text(" please enter a valid bank name ");
        } else if (accountName === "") {
            $("#error_acount_holder_name").text("please enter a valid account name");
        } else if (accountNumber.length !== 19 || ifAcVErified === "false") {
            $("#error_acount_holder_number").text("please enter a valid Account number.");
        } else if (bank_document === "") {
            $("#error_bank_document").text("please attach bank statement file");
        } else {

            if (bankName === "Other") {
                bankName = OtherBank;
            }
//            alert();
            $('.bank-save').unbind('click');
//        var obj = {bankName: bankName, accountHolderName: accountName, accountNumber: accountNumber, branch: branch,
//            suffix: suffix, currency: currency, beneficiaryId: beneId, InvestmentCode: investId};
            var obj = {bankName: bankName, accountHolderName: accountName, accountNumber: accountNumber, branch: branch,
                suffix: suffix, beneficiaryId: beneId, bankFile: bankFile};
            console.log("data obj" + "==============" + JSON.stringify(obj));
            swal({
                    title: "",
                    text: "Bank data being saved...  ",
                    type: "success",
                    timer: 8000,
                    showConfirmButton: false
                });
            $.ajax({
                type: 'POST',
                url: './rest/groot/db/api/admin/pending-bankAccount',
                headers: {"Content-Type": 'application/json'},
                data: JSON.stringify(obj),
                success: function (data, textStatus, jqXHR) {
//                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                console.log(data);
                    location.reload();
                }, error: function (jqXHR, textStatus, errorThrown) {
//                    alert(textStatus);
                }
            });
        }
    });
    $('.bank_name').change(function () {
        if ($('.bank_name option:selected').val() === 'Other') {
            $(".toggal_other").show();
        } else {
            $(".toggal_other").hide();
        }
        var data = $('.bank_name option:selected').data('id');
//               alert(data);
        $('#account_number').val(data);
    });


</script>

<script>
    function myFunction7() {
        var ac = document.getElementById("account_number").value;
        if (ac.length === 2) {
            var ac20 = ac.substr(0, 2);
            var a = ac20 + '-';
            $("#account_number").val(a);
        }
        if (ac.length === 7) {
            var ac21 = ac.substr(7);
            var b = ac + '-' + ac21;
            $("#account_number").val(b);
        }
        if (ac.length === 15) {
            var ac212 = ac.substr(15);
//                alert("c14 7S");
            var c = ac + '-' + ac212;
            $("#account_number").val(c);
        }
        if (ac.length > 18) {
            var ac23 = ac.substr(0, 18);
            var d = ac23;
            $("#account_number").val(d);
        }
    }
    $(document).ready(function () {
        $(".toggal_other").hide();
    });

</script>

<!----------------------new attach btn start------------------------->
<script>
    $(".new-attach-btn [type=file]").on("change", function () {
        // Name of file and placeholder
        var file = this.files[0].name;
        var dflt = $(this).attr("placeholder");
        if ($(this).val() != "") {
            $(this).next().text(file);
        } else {
            $(this).next().text(dflt);
        }
    });
</script>
<!----------------------new attach btn end------------------------->
<!----------------------bank btn start------------------------->
<!--<script>
    $(document).ready(function () {
        $(document).on("click", "ul.prod-gram .init", function () {
            $(this).parent().find('li:not(.init)').toggle();
        });
        var allOptions = $("ul.prod-gram").children('li:not(.init)');
        $("ul.prod-gram").on("click", "li:not(.init)", function () {
            allOptions.removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().children('.init').html($(this).html());
            $(this).parent().find('li:not(.init)').toggle();
        });
    });

</script>-->
<script>
    $(function () {
        // Set
        var main = $('div.mm-dropdown .textfirst')
        var li = $('div.mm-dropdown > ul > li.input-option')
        var inputoption = $("div.mm-dropdown .option")
        var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" />';

        // Animation
        main.click(function () {
            main.html(default_text);
            li.toggle('fast');
        });

        // Insert Data
        li.click(function () {
            // hide
            li.toggle('fast');
            var livalue = $(this).data('value');
            var lihtml = $(this).html();
            main.html(lihtml);
            inputoption.val(livalue);
        });
    });
</script>
<!----------------------bank btn end------------------------->
