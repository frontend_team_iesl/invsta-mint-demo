<%-- 
    Document   : updateBankAccount
    Created on : Sep 27, 2019, 11:36:06 AM
    Author     : ADMIN
--%>
<style>
    .new-model-text label.label_input {
        color: #2d2a26;
    }
    .new-model-text .form-control:focus {
        color: #464a4c;
        background-color: #fff;
        border-color: #999!important;
        outline: none;
    }
    .mobile_number.first-mobile {
        display: flex;
    }
    .mobile_number.first-mobile {
        display: flex;
    }

    .first-mobile input {
        border-radius: 0px;
        margin-bottom: 15px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 13px;
        border-bottom: 1px solid #e6d9d9;
        border-top: none;
        border-right: none;
        border-left: none;
        margin-top: 0;
    }
    .intl-tel-input input {
        padding: 10px;
        padding-left: 38px;
    }
    span.error.mobileUpdate-error {
    margin: 0;
    margin-top: -10px;
    width: 100%;
    TEXT-ALIGN: RIGHT;
}
</style>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="phoneModel" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update phone number</h4>
                </div>
                <div class="modal-body new-model-text">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                               Mobile number
                            </label>
                        </div>
                        <input type="hidden" name="beneId" id="beneId"  class="beneId" value=""/>
                        <div class="col-sm-6 mobile_number first-mobile mobile-down profile-no">
                            <input type="text" class="form-control codenumber"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                            <input type="tel" class="form-control error codename" id="mobileNo" name="mobileNo" required="required" placeholder="Enter mobile number">
                           
                        </div> <span class="error mobileUpdate-error"></span>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                               Home number
                            </label>
                        </div>
                        <input type="hidden" name="beneId" id="beneId"  class="beneId" value=""/>
                        <div class="col-sm-6 mobile_number first-mobile mobile-down">
                            <!--<input type="text" class="form-control codenumber"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">-->
                            <input type="tel" class="form-control error " id="HomeNumber" name="mobileNo" required="required" placeholder="Enter mobile number">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                                Business number
                            </label>
                        </div>
                        <input type="hidden" name="beneId" id="beneId"  class="beneId" value=""/>
                        <div class="col-sm-6 mobile_number first-mobile mobile-down">
                            <!--<input type="text" class="form-control codenumber"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">-->
                            <input type="tel" class="form-control error " id="BusinessNumber" name="mobileNo" required="required" placeholder="Enter mobile number">
                        </div>
                    </div>
<!--                    <div class="row">
                        <div class="col-sm-6">
                            <label class="label_input">
                                 Type
                            </label>                          
                        </div>
                        <div class="col-sm-6">
                        <div class="profile-form-style">
                            <select class="selectoption  Type_name form-group aml-select" id="Type" value="">
                                <option value="No Type Selected">-Select-</option>
                                <option value="Mobile">Mobile</option>
                                <option value="Business">Business</option>
                                <option value="Home">Home</option>
                            </select>
                            <span class="error" id="error_Type"></span>                         
                        </div>
                        </div>
                    </div>-->
                    <!--                    <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Other Mobile phone number (optional)
                                                </label>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile">
                                                <input type="text" class="form-control codenumber"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename" id="mobileNo" name="mobileNo" required="required" placeholder="Enter mobile number">
                                            </div>
                                        </div>-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-info phone-submit" >Submit</button>
                    <button type="button" class="btn btn-default btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


