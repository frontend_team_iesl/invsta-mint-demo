<!--Minor Information Start-->
<fieldset id="step8" class="minor">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please provide the details of the minor account holder (must be under 18). 
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input">
                        Title
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption nameTitle form-group" name="title" id="src_of_fund" >
                        <option <c:if test = "${person.title eq 'Mr'}"> selected </c:if> >Mr</option>
                        <option <c:if test = "${person.title eq 'Mrs'}"> selected </c:if> >Mrs</option>
                        <option <c:if test = "${person.title eq 'Miss'}"> selected </c:if> >Miss</option>
                        <option <c:if test = "${person.title eq 'Master'}"> selected </c:if> >Master</option>
                    </select>
                </div>

                <div class="col-sm-6">
                    <label class="label_input">
                        Full name
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" class=" fullName form-control input-field" id="minorfullName" name="fullName" required="required" value="${person.fullName}" placeholder="Enter full name " onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                    <span class="error" id="span-minor-fullName"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Preferred name (if applicable) 
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" name="preferredName" id="minorpreferredName" placeholder="Enter preferred first name (optional)" class=" input-field" value="${person.preferredName}"    onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                    <span class="error" id="span-preferredName"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Date of birth
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" name="date_of_Birth" id="minordob" placeholder="dd/mm/yyyy" class=" input-field minordob" value="${person.date_of_Birth}" data-format="dd/mm/yyyy" data-lang="en" required/>
                    <span class="error" id="span-minor-dob"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Country of residence 
                    </label>
                </div>
                <div class="col-sm-6 form-group country-set flag-drop">
                    <input type="text" class="country_residence form-control countryname countrynameone" value="${person.country_residence}" id="minorcountrynamefirst" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">    
                    <span class="error" id="span-minor-countryname"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input">
                        Home address
                    </label>
                </div>

                <div class="col-sm-6">
                    <input type="address" class="form-control input-field more-investor-address clear-more-investor"  id="minoraddress" placeholder="Enter new address" />
                    <span class="error" id="span-minor-address"></span>
                </div>
                <div class="col-sm-12">
                    <button class="director-btn postal-address all-btn-color upper-btn" id="same-as-investor1">Same as primary investor</button>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous7 action-button-previous" value="Previous" />
    <input type="button" name="next" class="next8 action-button" value="Continue" />
</fieldset>
<fieldset id="step9" class="minor">

    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the identification details for the primary investor. 
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input detail-2">
                        Which type of ID are you providing 
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class=" id_type selectoption form-group" id="src_of_fund2">
                        <c:if test="${person.id_type eq ''}">
                            <option value="${person.id_type}">${person.id_type}</option>
                        </c:if>
                        <option value="4">Birth Certificate </option>
                        <option value="3">Other </option>
                    </select>
                </div>
                <div class="row drivery-licence">
                    <div class="col-sm-6">
                        <label class="label_input">
                            First Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name error-remove" name="licence_first_name" id="minor_first_name" placeholder="Enter first name"/>
                        <span class="error error-minor" id="error_minor_first_name"></span>
                    </div>                      
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name error-remove" name="licence_middle_name"  id="minor_middle_name" placeholder="Enter middle name"/>
                        <span class="error error-minor" id="error_minor_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="last_name error-remove" name="licence_last_name" id="minor_last_name" placeholder="Enter last name"/>
                        <span class="error error-minor" id="error_minor_last_name"></span>
                    </div>
                    <div class="col-sm-129 closestcls">
                        <input type="file" name="myFile" id="minor_birth_certificate" class="attach-btn checkname error-remove add-cntnt" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                        <span class="shownamedata"></span></span><a href="javascript:void(0);" onclick="removedatadiv(this);"><i class=" removefile far fa-times-circle"></i></a>
                        <span class="error error-minor" id="error_minor_birth_certificate"></span>
                    </div>
                </div>
                <div class="row other-select">
                    <div class="col-sm-6">
                        <label class="label_input">
                            Type of ID   
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class=" other_id_type form-control input-field" id="minor_other_id_issueBy" name="other_id_type" value="${person.other_id_type}" required="required" placeholder="Enter ID type" onkeyup="myFunction()" />
                        <span class="error error-minor" id="error_minor_other_id_issueBy" ></span>
                    </div>

                    <div class="col-sm-6">
                        <label class="label_input">
                            First Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="first_name error-remove" id="minor_other_first_name " name="other_first_name" placeholder="Enter first name"/>
                        <span class="error error-minor" id="error_minor_other_first_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Middle Name 
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="middle_name error-remove"  id="minor_other_middle_name " name="other_middle_name" placeholder="Enter middle name"/>
                        <span class="error error-minor" id="error_minor_other_middle_name"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Last Name 
                        </label>
                    </div>                                                    
                    <div class="col-sm-6">
                        <input type="text" class="last_name error-remove" id="minor_other_last_name" name="other_last_name" placeholder="Enter last name"/>
                        <span class="error error-minor" id="error_minor_other_last_name"></span>
                    </div>   


                    <div class="col-sm-6">
                        <label class="label_input">
                            Expiry date 
                        </label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="other_id_expiry" id="minor_IdExpirydate" placeholder="dd/mm/yyyy" class="other_id_expiry input-field dob1 error-remove" value="${person.other_id_expiry}" data-format="DD/MM/YY" data-lang="en" required/>                                 
                        <span class="error error-minor" id="error_minor_IdExpirydate"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="label_input">
                            Country of issue  
                        </label>
                    </div>
                    <div class="col-sm-6 flag-drop">                                               
                        <input type="text" class=" other_id_issueBy form-control countryname error-remove"  id="minor_other_id_country" name="other_id_issueBy" value="${person.other_id_issueBy}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                        <span class="error error-minor" id="error_minor_other_id_country" ></span>
                    </div>
                    <div class="col-sm-129 closestcls">
                        <input type="file" name="myFile" id="minor_other_id_myFile" class="attach-btn checkname error-remove" accept="image/x-png,image/gif,image/jpeg,.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                        <span class="shownamedata"></span></span><a href="javascript:void(0);" onclick="removedatadiv(this);"><i class=" removefile far fa-times-circle"></i></a>
                        <span class="error error-minor" id="error_minor_other_id_myFile"></span>
                    </div>
                </div>
                <input type="hidden" name="more_investor_verify" id="investor_verify" value="false">
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous8 action-button-previous" value="Previous" />
    <input type="button" name="next" class="next9 action-button verify-dl" value="Continue" />
</fieldset>
<fieldset id="step10" class="minor">
    <div class="content-section">
        <div class="element-wrapper">
            <h5 class="element-header">
                Please enter the tax information (if available) for the minor account holder.
            </h5>
        </div>
        <div class="input-content">
            <div class="row">
                <div class="col-sm-6">
                    <label class="label_input input-size-text ">
                        Prescribed Investors Rate (PIR)
                    </label>
                </div>
                <div class="col-sm-3 details-pos">
                    <select class="selectoption  PIR form-group aml-select redy-select" id="minor_Investors_Rate">
                        <option value="28%">28%</option>
                        <option value="17.5%">17.5%</option>
                        <option value="10.5%" selected>10.5%</option>
                    </select>

                </div>
                <div class="col-sm-3">
                    <a href="javascript:void(0)" id="pir" class="aml-link">What?s my PIR?</a>
                    <span class="error" id="error_Investors_Rate"></span>
                </div>

                <div class="col-sm-6">
                    <label class="label_input">
                        IRD Number 
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" class=" IRD_Number  form-control input-field" name="fullName" id="minor_IRD_Number" value="${person.ird_number}" required="required" placeholder="xxx-xxx-xxx " onkeydown="checkird(this)" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
                    <span class="error" id="minor_error_IRD_Number"></span>
                </div>
                <div class="col-sm-6">
                    <label class="label_input" style="text-align:left">
                        Are you a US citizen or US tax resident, or a tax resident in any other country?
                    </label>
                </div>
                <div class="col-sm-6 ">
                    <select class="selectoption selectoption2 form-group">
                        <option value="1">No</option>
                        <option value="2">Yes</option>
                    </select>
                </div>
                <div class="row yes-option1">
                    <div class="row yes-new1 checktindata1">
                        <div class="col-sm-12">
                            <h5 class="element-header aml-text">Please enter all of the countries (excluding NZ) of which you are a tax resident. </h5>
                        </div>
                        <c:if test = "${empty person.countryTINList}">
                            <div class="col-sm-6">
                                <label class="label_input" style="text-align:left">
                                    Country of tax residence:
                                </label>
                            </div>
                            <div class="col-sm-6 details-pos flag-drop">
                                <input type="text" class=" tex_residence_Country form-control countrynameoutnz"  id="countryname2" name="countryCode" value="${taxResident.country}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                <span class="error minor_countrynameoutnz-error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Tax Identification Number (TIN)
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="minor_TIN form-control input-field" name="fullName" value="${taxResident.tin}" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                <span class="error TIN_minor_error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Reason if TIN not available 
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class=" minor_resn_tin_unavailable form-control input-field" name="fullName" value="${taxResident.reason}" required="required" placeholder=""/>
                                <span class="error error_minor_resn_tin_unavailable" ></span>
                            </div>
                        </c:if>
                        <c:forEach items="${person.countryTINList}" var="taxResident">
                            <div class="col-sm-6">
                                <label class="label_input" style="text-align:left">
                                    Country of tax residence:
                                </label>
                            </div>
                            <div class="col-sm-6 details-pos flag-drop">
                                <input type="text" class=" tex_residence_Country form-control countrynameoutnz"  id="countryname2" name="countryCode" value="${taxResident.country}" required="required" placeholder="Enter Country Code" readonly="readonly">    
                                <span class="error minor_countrynameoutnz_error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Tax Identification Number (TIN) 
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class=" TIN form-control input-field" name="fullName" value="${taxResident.tin}" required="required" placeholder="Enter TIN " onkeyup="myFunction()" />
                                <span class="error minor_TIN_error" ></span>
                            </div>
                            <div class="col-sm-6">
                                <label class="label_input">
                                    Reason if TIN not available 
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class=" resn_tin_unavailable form-control input-field" name="fullName" value="${taxResident.reason}" required="required" placeholder=""/>
                                <span class="error minor_resn_tin_unavailable_error" ></span>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="col-sm-129 add-another">
                        <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another1">Add another country</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_form">
        <!--<img src="./resources/images/red.png">-->	
    </div>
    <input type="button" name="previous" class="previous9 action-button-previous" value="Previous" />
    <input type="button" name="next" class="next10 action-button" value="Continue" />
</fieldset>