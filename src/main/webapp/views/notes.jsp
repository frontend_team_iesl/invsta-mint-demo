<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta-</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/
         <jsp:include page="header_url.jsp"></jsp:include>
        <style>
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
                display: block;
            }
            .ilumony-box:hover .portfolio_overlay {
                display: block;
            }
            .funds-deatil {
                background: #fff;
                padding: 1rem;
                border-radius: 4px;
                margin-bottom: 10px;
            }
            .show_more_btn_new {
                position: absolute;
                top: 0;
                right: 0px;
                margin: 11px 27px;
            }
            .funds-deatil h4 {
                max-width: 80%;
                margin-bottom: 15px;
            }
            .collapse {
                display: none;
            }
            .collapse.show {
                display: block;
            }

            .menu-side .layout-w {

                min-height: 669px;
            }

        </style>
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Notes</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Notes </h6>
                                                        </div>
                                                        <div class="main-option">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="main-sparkline13-hd">

                                                                        <button type="submit" class="btn btn-primary add-btn" data-toggle="modal" onclick="toggleButton('#addNote')" data-target="#PrimaryModalalert">Add Note</button>
                                                                    </div>

                                                                    <div class="main-deleteline">
                                                                        <button class="btn icon-btn btn-danger btn-xs clean-btn" data-title="Delete" id="noteDelete" data-toggle="modal" data-target="#delete"  href="#">
                                                                            <span class="glyphicon btn-glyphicon glyphicon-trash img-circle text-danger"></span><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                                        <!--                                                                        <div class="pull-right">
                                                                                                                                                    <input class="form-control type-form" type="text" placeholder="Search">
                                                                                                                                                </div>-->
                                                                    </div>
                                                                    <!--                                                                                                                                        <table id="all-notes" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar" class="table table-bordered table-hover">
                                                                                                                                                                                                        <thead>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <th data-field="id" data-formatter="deleteNoteCheckBoxFormatter"><input type="checkbox" name="note_all_check" class="delete-note-check check_all" onclick="checkboxclick(this);"/></th>
                                                                                                                                                                                                        <th data-field="note" data-formatter="viewButtonFormatter"><span>Note</span></th>
                                                                                                                                                                                                        <th data-field="created_ts" ><span>Date </span></th>
                                                                                                                                                                                                        <th data-field="related_to" ><span>Related To</span></th>
                                                                                                                                                                                                        <th data-formatter="viewButtonFormatter">Action</th>
                                                                                                                                        
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        </thead>
                                                                                                                                                                                                        <tfoot style="display: none;"><tr></tr></tfoot><tbody><tr class="no-records-found"><td colspan="4">No matching records found</td></tr></tbody>
                                                                                                                                                                                                        </table>-->

                                                                    <div class="card mt-3 tab-card">
                                                                        <table id="all-notes" class="table table-striped table-bordered" style="width:100%">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th data-field="id" ><input type="checkbox" name="note_all_check" class="delete-note-check check_all" onclick="checkboxclick(this);"/></th>
                                                                                    <th data-field="note" data-formatter="viewButtonFormatter"><span>Note</span></th>
                                                                                    <th data-field="created_ts" ><span>Date </span></th>
                                                                                    <th data-field="related_to" ><span>Related To</span></th>
                                                                                    <th data-formatter="viewButtonFormatter">Action</th>

                                                                                </tr>
                                                                            </thead>
                                                                            <!--<tfoot style="display: none;"><tr></tr></tfoot><tbody><tr class="no-records-found"><td colspan="4">No matching records found</td></tr></tbody>-->
                                                                        </table>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade in" role="dialog" >
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-close-area modal-close-df">
                                                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                                                    </div>
                                                                    <div class="modal-body note-form">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Notes</label>
                                                                                    <br><span class="reqNoteFields"></span>
                                                                                    <input type="hidden" name="id" id="id" value="">
                                                                                    <input type="text" id="note" name="note" class="form-control spaceprevent" placeholder="Add a note about...">
                                                                                </div>            
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label>Related To</label>
                                                                                            <select class="form-control multichoosenlist" name="related_to" id="related_to" required="">
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--                                                                                    <div class="col-md-6">
                                                                                                                                                                            <div class="notify-btn notifyset">
                                                                                                                                                                                <div class="row">
                                                                                                                                                                                    <div class="col-md-12">
                                                                                                                                                                                        <input class="ms1" type="text" name="notify_email">
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                    
                                                                                                                                                                            </div>
                                                                                    
                                                                                                                                                                        </div>-->
                                                                                    <!--                                                                                    <div class="col-md-6">
                                                                                                                                                                            <div class="members">
                                                                                                                                                                                <select name="group_id">
                                                                                                                                                                                    <option value="0">Everyone</option>
                                                                                                                                                                                                        <option value="1">Only me</option>
                                                                                    
                                                                                                                                                                                </select>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>    -->
                                                                                </div>
                                                                                <div class="form-group btn-add-cls">
                                                                                    <button class="btn btn-primary waves-effect waves-light" id="addNote" style="padding: 9px 20px; display: inline-block;">Add Note</button>
                                                                                    <button class="btn btn-primary btn-info" id="updateNote" style="display: none;">Update</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div></div>
                                                        <c:set var = "data"  value = "#demo"/>
                                                        <c:set var = "id"  value = "${0}"/>
                                                        <c:forEach items="${allPortfolios}" var="portfolio">
                                                            <c:set var = "id"  value = "${id+1}"/>

                                                        <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="<c:out value="${data}"/><c:out value="${id}"/>">
                                                            <a href="#">
                                                                <div class="ilumony-box text-center democlick" style="cursor: pointer; background: #0f256e;">
                                                                    <div class="portfolio_overlay">
                                                                        <h5 class="color3 portfolio_name">${portfolio.getName()}</h5>
                                                                    </div>
                                                                </div>
                                                            </a> 

                                                        </div>
                                                    </c:forEach>

                                                    <!--                                                    <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo2">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Australasian Property Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>
                                                    
                                                                                                        <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo3">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Diversified Income Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>
                                                    
                                                                                                        <div class="col-md-3 col-sm-4 democlick" data-toggle="collapse" data-target="#demo4">
                                                                                                            <a href="#">
                                                                                                                <div class="ilumony-box text-center" style="cursor: pointer; background: #0f256e;">
                                                                                                                    <div class="portfolio_overlay">
                                                                                                                        <h5 class="color3 portfolio_name">Mint Diversified Growth Fund</h5>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </a>    
                                                                                                        </div>-->
                                                    <c:set var = "data1"  value = "demo"/>
                                                    <c:set var = "id1"  value = "${0}"/>
                                                    <c:forEach items="${allPortfolios}" var="portfolio">
                                                        <c:set var = "id1"  value = "${id1+1}"/>
                                                        <div class="col-md-12 list-manage">
                                                            <div class="funds-deatil collapse showclick" id="<c:out value="${data1}"/><c:out value="${id1}"/>">
                                                                <h4>${portfolio.getName()}</h4>
                                                                <p>This is a single asset class Fund, investing predominantly in Australasian equities, and targeting medium to long-term growth. Investors should expect returns and risk commensurate with the New Zealand and Australian share markets.</p>
                                                                <p>Our objective is to outperform the S&P/NZX50 Gross Index (which is the FundÃ¢ÂÂs relevant market index) by 3% per annum, before fees, over the medium to long-term.</p>
                                                                <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style showmemore1" href="./portfolio-${portfolio.getCode()}">Show Me More</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                    <!--                                                    <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo2">
                                                                                                                <h4>Mint Australasian Property Fund</h4>
                                                                                                                <p>This is a single asset class Fund, investing predominantly in Australasian listed property securities. Investors should expect returns and risk commensurate with the listed property sector of the New Zealand and Australian share markets.</p>
                                                                                                                <p>Our objective is to outperform the S&P/NZX All Real Estate (Industry Group) Gross Index (which is the FundÃ¢ÂÂs relevant market index) by 1% per annum, before fees, over the medium to long-term.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore2" href="investment-fund-2.html">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo3">
                                                                                                                <h4>Mint Diversified Income Fund</h4>
                                                                                                                <p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.</p>
                                                                                                                <p>The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is MintÃ¢ÂÂs lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore3" href="investment-fund-3.html">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->
                                                    <!--                                                    <div class="col-md-12 list-manage">
                                                                                                            <div class="funds-deatil collapse showclick" id="demo4">
                                                                                                                <h4>Mint Diversified Growth Fund</h4>
                                                                                                                <p>This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities. </p>
                                                                                                                <p>The objective of the Fund is to deliver returns in excess of the Consumer Price Index (CPI) by 4.5% per annum, before fees, over the medium to long-term. Investors should expect returns and risk to sit between the risk profiles of the property and equities asset classes.</p>
                                                                                                                <div class="show_more_btn_new">
                                                                                                                    <a class="btn btn-primary btn-style showmemore4" href="investment-fund-{}">Show Me More</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo5">
                                                            <h4>Aditya Birla Sun Life International Equity Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo6">
                                                            <h4>Kotak US Equity Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo7">
                                                            <h4>Kotak US Equity Growth Fund</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="show-me-more-1.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                            </div>
                                            <div class="tab-pane fade p-31" id="two" role="tabpanel" aria-labelledby="two-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>Through our funds and direct investments, we have invested into 170+ companies. Search for and view the details of particular companies here</p></div>
                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo8">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/chipilogo.png) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">CHIPI</h5>
                                                                </div>
                                                            </div>
                                                        </a> 

                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo9">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/ladder.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Think Ladder</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo12">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe3.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Google</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo13">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Canon</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 hidediv2" data-toggle="collapse" data-target="#demo14">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe5.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">Coca-Cola</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo8">
                                                            <h4>CHIPI</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="investment-company.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo9">
                                                            <h4>Think Ladder</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="investment-company-ladder.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo10">
                                                            <h4>Toyota</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo11">
                                                            <h4>Apple</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo12">
                                                            <h4>Google</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo13">
                                                            <h4>Canon</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo14">
                                                            <h4>Coca-Cola</h4>
                                                            <p> The scheme operates as a fund-of-fund and has invested 99.88% of its assets in the US Opportunities Fund and the remaining 0.12% in cash and debt instruments (as on January 31, 2019). The schemeÃ¢ÂÂs underlying mutual fund invests a majority of its assets in equity securities of US companies which demonstrate growth at higher levels than the overall US economy.</p>
                                                            <div class="show_more_btn_new">
                                                                <a class="btn btn-primary btn-style" href="invest-company-show-me-more.html">Show Me More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="#" class="btn btn-primary" id="viewmore2">View More</a>              
                                            </div>
                                            <div class="tab-pane fade p-31" id="three" role="tabpanel" aria-labelledby="three-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>See the details of new offers coming soon and current investment opportunities </p></div>
                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo15">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2020</h5>
                                                                </div>
                                                            </div>
                                                        </a> 

                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo16">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2021</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo17">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2022</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4" data-toggle="collapse" data-target="#demo18">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2023</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo19">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2024</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo20">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe4.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2025</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>

                                                    <div class="col-md-3 col-sm-4 " data-toggle="collapse" data-target="#demo21">
                                                        <a href="#">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/globe5.jpg) center center no-repeat; background-size: 100% auto;">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">New Fund 2026</h5>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo15">
                                                            <h4>New Fund 2020</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo16">
                                                            <h4>New Fund 2021</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (hedged to AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new"> -->
                                                            <!-- <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a> -->
                                                            <!-- </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo17">
                                                            <h4>New Fund 2022</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the performance fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo18">
                                                            <h4>New Fund 2023</h4>
                                                            <p>10.0% of excess return over the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (AUD)) and the Absolute Return Hurdle (the yield of a 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo19">
                                                            <h4>New Fund 2024</h4>
                                                            <p>10.0% of the excess return of the units of the Fund above the higher of the Index Relative Hurdle (MSCI World Net Total Return Index (hedged to AUD)) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo20">
                                                            <h4>New Fund 2025</h4>
                                                            <p>10% (20% for Class B units) of the excess return above the Absolute Return performance hurdle of 10% per annum. Additionally, Performance Fees are subject to a high water mark. For further details on Performance Fees, please read the relevant PDS and the Additional Information Booklet.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="funds-deatil collapse" id="demo21">
                                                            <h4>New Fund 2026</h4>
                                                            <p>10.0% of the excess return of the Units of the Fund above the higher of the Index Relative Hurdle (S&P Global Infrastructure Index A$ Hedged Net Total Return) and the Absolute Return Hurdle (the yield of 10-year Australian Government Bonds). Additionally, the Performance Fees are subject to a high water mark.</p>
                                                            <!-- <div class="show_more_btn_new">
                                                                    <a class="btn btn-primary btn-style" href="javascript:void(0)">Show Me More</a>
                                                            </div> -->
                                                            <a href="javascript:void(0)" class="clickinput">Express Interest</a>
                                                            <input type="text" placeholder="Enter $ Amount" class="offer-input" style="display: none;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <a href="#" class="btn btn-primary" id="viewmore3">View More</a>   -->            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

        <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>	
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 

        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>


        <script>
                                                                                        var noteMap = new Map();
                                                                                        var noteMapData = new Map();
                                                                                        var relatedToMap = new Map();
                                                                                        function deleteNoteCheckBoxFormatter(value, row, index) {
                                                                                            return '<input type="checkbox" data-id="' + row.id + '" name="delete_note_check" class="countcheckbox" onclick="checkboxoneclick(this);"/>';
                                                                                        }

                                                                                        function viewButtonFormatter(value, row, index) {
                                                                                            return '<span><strong style="color:#000">' + row.note + '</strong></span><a href="javascript:void(0)"  data-toggle="modal" data-target="#PrimaryModalalert" class="btn btn-primary note-edit-icon" onclick="grabData(' + row.id + ')"><span class="opertunity-edit"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>';
                                                                                        }

                                                                                        function grabData(id) {
                                                                                            var data = noteMap[id];
                                                                                            console.log(JSON.stringify(data));
                                                                                            $('.note-form input[name="id"]').val(data.id);
                                                                                            $('.note-form input[name="note"]').val(data.note);
                                                                                            $('.note-form select[name="related_to"]').val(data.related_to).trigger("chosen:updated");
                                                                                            //                                                                    $('.opportunity-form select[name=opp_contact]').val(data.contact).trigger("chosen:updated");
                                                                                            //                                                                    $(".opportunity-form select[name=pipeline]").val(data.pipeline);                                                     
                                                                                            toggleButton("#updateNote");
                                                                                        }


                                                                                        function checkboxclick(ele) {
                                                                                            if (ele.checked !== true)
                                                                                            {
                                                                                                $('#noteDelete').attr('disabled', 'disabled');
                                                                                                $(".countcheckbox").prop('checked', false);
                                                                                            } else
                                                                                            {
                                                                                                $('#noteDelete').removeAttr('disabled');
                                                                                                $(".countcheckbox").prop('checked', true);
                                                                                            }
                                                                                        }
                                                                                        function checkboxoneclick(ele) {

                                                                                            var checkedNum = $('.countcheckbox:checked').length;
                                                                                            var total_lenght = $('.countcheckbox').length;
                                                                                            if (!checkedNum) {
                                                                                                $('#noteDelete').attr('disabled', 'disabled');
                                                                                                $(".check_all").prop('checked', false);
                                                                                            } else {

                                                                                                $('#noteDelete').removeAttr('disabled');
                                                                                            }

                                                                                            if (checkedNum === total_lenght)
                                                                                            {

                                                                                                $(".check_all").prop('checked', true);
                                                                                            } else
                                                                                            {
                                                                                                $(".check_all").prop('checked', false);
                                                                                            }
                                                                                        }



                                                                                        function toggleButton(idc) {
                                                                                            $('.note-form .btn-primary').hide();
                                                                                            $(idc).show();
                                                                                        }

                                                                                        $('.clickinput').on("click", function () {
                                                                                            var recemt = $(this).closest('.funds-deatil');
                                                                                            recemt.find(".offer-input").toggle();
                                                                                        });
                                                                                        $(document).ready(function () {
                                                                                            $('#noteDelete').attr('disabled', 'disabled');

                                                                                            $.ajax({
                                                                                                type: 'GET',
                                                                                                url: './rest/groot/db/api/fetchNote',
                                                                                                headers: {"Content-Type": 'application/json'},
                                                                                                success: function (data, textStatus, jqXHR) {
                                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                                                                                        $('#all-notes').bootstrapTable('load', obj);
                                                                                                    $('#all-notes').DataTable({
                                                                                                        data: obj,
                                                                                                        'columns': [
                                                                                                            {'data': 'id'},
                                                                                                            {'data': 'note'},
                                                                                                            {'data': 'created_ts'},
                                                                                                            {'data': 'related_to'}
                                                                                                        ],
                                                                                                        columnDefs: [

                                                                                                            {
                                                                                                                "targets": 4,
                                                                                                                mRender: function (data, type, row) {
                                                                                                                    return '<span><strong style="color:#000">' + row.note + '</strong></span><a href="javascript:void(0)"  data-toggle="modal" data-target="#PrimaryModalalert" class="btn btn-primary note-edit-icon" onclick="grabData(' + row.id + ')"><span class="opertunity-edit"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>';
                                                                                                                }
                                                                                                            },
                                                                                                            {
                                                                                                                "targets": 0,
                                                                                                                mRender: function (data, type, row) {
                                                                                                                    return '<input type="checkbox" data-id="' + data + '" name="delete_note_check" class="countcheckbox" onclick="checkboxoneclick(this);"/>';
                                                                                                                }
                                                                                                            }]
                                                                                                    });
                                                                                                    console.log(data);
                                                                                                    $.each(obj, function (idx, val) {
                                                                                                        noteMap[val.id] = val;
//                                                                                                    noteMapData.push([val.id, val.note, val.created_ts, val.related_to])
                                                                                                    });
                                                                                                }, error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    alert(textStatus);
                                                                                                }
                                                                                            });
                                                                                            $.ajax({
                                                                                                type: 'GET',
                                                                                                url: './rest/groot/db/api/get-pending-registration',
                                                                                                headers: {"Content-Type": 'application/json'},
                                                                                                success: function (data, textStatus, jqXHR) {
                                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                    setRelatedTo(obj);
                                                                                                    $.each(obj, function (idx, val) {
                                                                                                        relatedToMap[val.username] = val;
                                                                                                    });
                                                                                                    $('.multichoosenlist').chosen();
                                                                                                    console.log(obj);
                                                                                                },
                                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    console.log(jqXHR);
                                                                                                }
                                                                                            });
                                                                                            $(".loader").fadeOut("slow");
                                                                                            $('.hidediv1').hide();
                                                                                            $('.hidediv2').hide();
                                                                                            $('.hidediv3').hide();
                                                                                        });
                                                                                        $('.democlick').click(function () {
                                                                                            var data = $(this).data("target");
                                                                                            $('.showclick').hide();
                                                                                            $(data).show();
                                                                                        });
                                                                                        $("#addNote").click(function () {
                                                                                            var note = $("#note").val();
                                                                                            var related_to = $("#related_to").val();
                                                                                            var obj = {note: note, related_to: related_to};
//                                                                                            swal({
//                                                                                                title: "Progress",
//                                                                                                text: "Note is created progress.",
//                                                                                                type: "info",
//                                                                                                timer: 3500,
//                                                                                                showConfirmButton: true
//                                                                                            });
                                                                                            console.log(JSON.stringify(obj));
                                                                                            $.ajax({
                                                                                                type: 'POST',
                                                                                                url: './rest/groot/db/api/addNote',
                                                                                                headers: {"Content-Type": 'application/json'},
                                                                                                data: JSON.stringify(obj),
                                                                                                success: function (data, textStatus, jqXHR) {
                                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                    swal({
                                                                                                        title: "Success",
                                                                                                        text: "Note is created successfully.",
                                                                                                        type: "info",
                                                                                                        timer: 3500,
                                                                                                        showConfirmButton: true
                                                                                                    });
                                                                                                    console.log(data);
                                                                                                    location.reload();
                                                                                                }, error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    alert(textStatus);
                                                                                                }
                                                                                            });
                                                                                        });
                                                                                        $("#updateNote").click(function () {
                                                                                            var id = $("#id").val();
                                                                                            var note = $("#note").val();
                                                                                            var related_to = $("#related_to").val();
                                                                                            var obj = {id: id, note: note, related_to: related_to};
                                                                                            swal({
                                                                                                title: "Progress",
                                                                                                text: "Note is updated progress.",
                                                                                                type: "info",
                                                                                                timer: 3500,
                                                                                                showConfirmButton: true
                                                                                            });
                                                                                            console.log(JSON.stringify(obj));
                                                                                            $.ajax({
                                                                                                type: 'POST',
                                                                                                url: './rest/groot/db/api/updateNote',
                                                                                                headers: {"Content-Type": 'application/json'},
                                                                                                data: JSON.stringify(obj),
                                                                                                success: function (data, textStatus, jqXHR) {
                                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                    swal({
                                                                                                        title: "Successs",
                                                                                                        text: "Note is updated successfully.",
                                                                                                        type: "info",
                                                                                                        timer: 3500,
                                                                                                        showConfirmButton: true
                                                                                                    });
                                                                                                    location.reload();
                                                                                                    console.log(data);
                                                                                                }, error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    alert(textStatus);
                                                                                                }
                                                                                            });
                                                                                        });
//                                                                                        $("#noteDelete").click(function () {
//                                                                                            alert();
//                                                                                        });

                                                                                        $("#noteDelete").confirm({
                                                                                            title: 'DELETE NOTE?',
                                                                                            content: 'click proceed to delete the  selected note.',
//                                                                                            autoClose: 'cancel|8000',
//                autoClose: false,
                                                                                            buttons: {
                                                                                                deleteUser: {
                                                                                                    text: 'Proceed',
                                                                                                    action: function () {

                                                                                                        var deleteNoteCheckBoxes = $('input[name="delete_note_check"]:checked'); //delete_group_check
                                                                                                        var arr = new Array();
                                                                                                        $.each(deleteNoteCheckBoxes, function (index, value) {
                                                                                                            var id = value.getAttribute('data-id');
                                                                                                            arr.push(id);
                                                                                                        });
                                                                                                        var obj = {ids: arr};
                                                                                                        var x = JSON.stringify(obj);
//                                                                                                     
                                                                                                        console.log(JSON.stringify(obj));
                                                                                                        $.ajax({
                                                                                                            type: 'POST',
                                                                                                            url: './rest/groot/db/api/deleteNote',
                                                                                                            headers: {"Content-Type": 'application/json'},
                                                                                                            data: JSON.stringify(obj),
                                                                                                            success: function (data, textStatus, jqXHR) {
                                                                                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                                swal({
                                                                                                                    title: "Successs",
                                                                                                                    text: "Note is deleted successfully.",
                                                                                                                    type: "info",
                                                                                                                    timer: 3500,
                                                                                                                    showConfirmButton: true
                                                                                                                });
                                                                                                                console.log(data);
                                                                                                                location.reload();
                                                                                                            }, error: function (jqXHR, textStatus, errorThrown) {
                                                                                                                alert(textStatus);
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                },
                                                                                                cancel: function () {
//                        $.alert('action is canceled');
                                                                                                }
                                                                                            }
                                                                                        });

        </script>
        <script>
            function setRelatedTo(contactList) {

                var elements = document.getElementsByClassName("multichoosenlist");
                for (var j = 0; j < elements.length; j++) {
                    var optgroup = document.createElement("optgroup");
                    optgroup.label = "BENEFICIARIES:";
                    //        var optgroup1 = document.createElement("optgroup");
                    //        optgroup1.label = "Trust:";
                    //        var optgroup2 = document.createElement("optgroup");
                    //        optgroup2.label = "Person:";
                    //        var optgroup3 = document.createElement("optgroup");
                    //        optgroup3.label = "opportunities:";
                    for (var i = 0; i < contactList.length; i++) {
                        var contact = contactList[i];
                        var option = document.createElement("option");
                        option.text = contact.username !== null ? contact.username : contact.name;
                        option.value = contact.username;
                        //                    option.value = contact.reg_type + '-' + contact.id;
                        //            
                        if (contact.reg_type === 'INDIVIDUAL_ACCOUNT') {
                            optgroup.appendChild(option);
                        }
                        //            if (contact.master === 'TRUST') {
                        //                optgroup1.appendChild(option);
                        //            }
                        ////                        alert(contact.login_user);
                        //            if (contact.master === 'PERSON' && contact.login_user !== 'true') {
                        //                optgroup2.appendChild(option);
                        //            }
                        //            if (contact.master === 'OPPORTUNITY') {
                        //                optgroup3.appendChild(option);
                        //            }

                    }
                    var element = elements[j];
                    //        element.appendChild(optgroup2);
                    //        element.appendChild(optgroup1);
                    element.appendChild(optgroup);
                    //        element.appendChild(optgroup3);
                }
            }
        </script>
    </body>
</html>