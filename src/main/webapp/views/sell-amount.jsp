<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>

    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel wrapper-again">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${portfolioDetail.fundName}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="content_background">
                        <div class="investment-form">
                            <form id="msform">
                                <fieldset id="step1">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space s">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1 show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2 show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3 show-3">
                                                                    </div>

                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Sell Amount </span>
                                                                    <span>Payment info</span>
                                                                    <span>Acceptance</span>
                                                                    <span>Confirmation</span>

                                                                </div>
                                                            </div>
                                                            <div class="input-content-text sell-form">
                                                                <div class="row">
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="form-group">
                                                                                                                                                <label>Beneficiary Name :</label><br>
                                                                                                                                            </div>
                                                                                                                                        </div>-->
                                                                    <!--                                                                    <div class="col-md-4">
                                                                                                                                            <div class="form-group">
                                                                                                                                                <select name="beneficiaryId" id="beneficiaryId"  class="form-group">
                                                                                                                                                    <option value="select">-Select-</option>
                                                                 
                                                                </select>
                                                                <span class="error" id="error-beneficiaryId"></span>
                                                            </div>
                                                        </div>-->

                                                                    <div class="col-md-12">
                                                                        <div class="element-wrapper">
                                                                            <h6 class="element-header">Details of Sale</h6>
                                                                        </div>
                                                                        <!--<h5 class="body-data">Details of Sale </h5>-->
                                                                        <div class="sell-info">
                                                                            <p>Account Name:</p>
                                                                            <input type="text" class="removerror"  id="beneficiaryname" name ="investmentName" data-id="${investmentHoldings.getBeneficiaryId()}" value="${investmentHoldings.getName()}" pattern="Enter Beneficary Name" placeholder="Enter Beneficary Name" readonly="readonly"/>
                                                                            <input type="hidden" id="unitprice" value="${investmentHoldings.getPrice()}"  readonly="readonly"/>
                                                                        </div>
                                                                        <span class="error" id="error-investmentName"></span>
                                                                        <div class="sell-info">
                                                                            <p>Name of Fund</p>
                                                                            <input type="text" class="removerror"   name ="investmentName" data-id=""  value="${portfolioDetail.fundName}" placeholder="Fund Name" readonly="readonly"/>
                                                                            <input type="hidden" id="" value=""  readonly="readonly"/>
                                                                        </div>
                                                                        <span class="error" id="error-investmentName"></span>
                                                                        <div class="sell-info">
                                                                            <p>Available Balance ($):</p>
                                                                            <input type="text" class="removerror" id="availablebalce" name ="investmentName" value=" <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${investmentHoldings.getMarketValue()}"/>" pattern="Enter Balance" placeholder="Enter Balance" readonly="readonly"/>
                                                                        </div>
                                                                        <div class="sell-info">
                                                                            <p>Available Units:</p>
                                                                            <input type="text" class="removerror" id="availableunits" name ="investmentName" value="<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${investmentHoldings.getUnits()}"/>" pattern="Enter Units" placeholder="Enter Units" readonly="readonly"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="redemption-info">
                                                                            <div class="body-section">
                                                                                <h6 class="body-data">Amount to Sell</h6>

                                                                            </div>
                                                                        </div>
                                                                        <h6 class="sell-content">Enter the $ amount, % amount, or number of units you wish to sell now. </h6>
                                                                        <div class="chose-currncy">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="currncy-type">
                                                                                        <h6>Amount ($)</h6>
                                                                                        <input type="text" class="removedata" id="amounttype" name ="investmentName" pattern="Enter Beneficary Name" placeholder="$" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 46'/>
                                                                                        <span class="error amountTypeError"></span>
                                                                                    </div>
                                                                                    <span class="error" id="error-redemption"></span>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="currncy-type">
                                                                                        <h6>Percentage</h6>
                                                                                        <input type="text" class="removedata" id="percentagetype" name ="investmentName" pattern="Enter Beneficary Name" placeholder="%" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 46'/>
                                                                                        <span class="error percentageError"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="currncy-type">
                                                                                        <h6>Units</h6>
                                                                                        <input type="text" class="removedata" id="unittype" name ="investmentName" pattern="Enter Beneficary Name" placeholder="UNITS" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 46'/>
                                                                                        <span class="error UnitsError"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="sell-info">
                                                                        <p>Approximate sale amount:</p><div class="pulsating-circle2 pulsating-around2 sell-dot" style="vertical-align:text-bottom;   " data-toggle="tooltip" title="">
                                                                            <span class="tooltiptext dashboard-toltip sell-tool">Exact amount will vary, depending on the price of units at sale, and taking into account any fees and/or tax amounts as required.</span>
                                                                        </div>
                                                                        <input type="text" class="removerror" id="redemptionamount" name ="investmentName" pattern="Enter Units"  placeholder="$" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 46' readonly="readonly"/>
                                                                    </div>
                                                                    
                                                                    <!--                                                                        <div class="sell-info">
                                                                                                                                                <p>Gross Proceed ($):</p>
                                                                                                                                                <input type="text" class="removerror" id="grossproceed" name ="investmentName" pattern="Enter Units"  placeholder="$" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 46' readonly="readonly"/>
                                                                                                                                            </div>
                                                                                                                                            <span class="error" id="error-gross"></span>-->

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">

                                                                        <div class="payment-btn1 btn-select1 ">
                                                                            <a href="./home" class="btn_payment payment-hover" id="">Cancel</a>
                                                                            <a href="javascript:void(0)" class="btn_payment1  next1 payment-hover" id="">Next</a>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>






                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>

                                <fieldset id="step2">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12  box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot progress-1 main-progress">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-1 show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2 show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3 show-3">
                                                                    </div>

                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Sell Amount </span>
                                                                    <span>Payment info</span>
                                                                    <span>Acceptance</span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>

                                                            <div class="input-content-text backgroud-add">
                                                                <div class="redemption-info ">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Payment Information </h6>
                                                                    </div>
                                                                    <!--<h5 class="body-data">Payment Information </h5>-->
                                                                    <div class="body-section">
                                                                        <h6 class="body-data">Select bank account for payment of sale proceeds:</h6>
                                                                    </div>
                                                                    <div class="sell-info">
                                                                        <div class="row" style="width:100%">
                                                                            <c:forEach items="${bankAccountDetails}" var="bank">
                                                                                <div class="col-md-6 continue  selectBank">
                                                                                    <a href="javascript:void(0)" class="next2">
                                                                                        <input type="radio" name="accountId" class="accountid" value="${bank.getId()}" style="display: none;"/>
                                                                                        <input type="hidden" class="bankaccountnum" value="${bank.getAccount()}">
                                                                                        <input type="hidden" class="bankaccountname" value="${bank.getAccountName()}">
                                                                                        <input type="hidden" class="branhnum" value="${bank.getBranch()}">
                                                                                        <div class="body-box">
                                                                                            <p><b>Existing bank account on file: </b></p>
                                                                                            <span class="flag-data ">${bank.getCurrency()} Account</span>
                                                                                            <p>ending with <span class="add-nmbr">${fn:substring(bank.getAccount(), 2, 6)}</span></p>
                                                                                            <p>${investmentHoldings.getName()}</p>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>                                                                 
                                                                            </c:forEach>
                                                                        </div>
                                                                    </div>


                                                                    <div class="payment-btn1 btn-select1 ">
                                                                        <a href="javascript:void(0)" class="btn_payment previous2 payment-hover" id="">Back</a>
                                                                        <!--<a href="javascript:void(0)" class="btn_payment1  next2 payment-hover" id="">Next</a>-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </fieldset>
                                <fieldset id="step3">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12 box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot progress-2 main-progress-2 ">
                                                                    </div>
                                                                    <div class="line-dot progress-1 main-progress show-1">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-2 show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3 show-3">
                                                                    </div>

                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Sell Amount </span>
                                                                    <span>Payment info</span>
                                                                    <span>Acceptance</span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>

                                                            <div class="input-content-text backgroud-add">  
                                                                <div class="term-part">
                                                                    <h5>Terms and Conditions</h5>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                                </div>

                                                                <!-- <div class="term-accpt">
                                                                     <input type="checkbox" class="removerror" id="acceptCheckbox" name="acceptcheck" >
                                                                     <label>
                                                                      I agree to the terms and conditions of Mint platform and hereby accept all the clauses and further request Mint to validate the transfer of funds  
                                                                      </label>
                                                                  </div>-->
                                                                <div class="row">
                                                                    <div class="col-sm-12 condition-details">
                                                                        <label class="last-check">I agree to the terms and conditions of Invsta platform and hereby accept all the clauses and further request Invsta to validate the transfer of funds
                                                                            <input type="checkbox" class="checkbox-check removerror" id="acceptCheckbox" name="acceptcheck">
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <span class="error" id="error-checkbox"></span>
                                                                <div class="payment-btn1 btn-select1 ">
                                                                    <a href="javascript:void(0)" class="btn_payment previous3 payment-hover" id="">Back</a>
                                                                    <a href="javascript:void(0)" class="btn_payment1  next3 payment-hover" id="">I Accept</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step4">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="form-data">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12 box-payment-space">
                                                            <div class="line-progress">
                                                                <div class="line-bar2">
                                                                    <div class="line-dot">
                                                                    </div>
                                                                    <div class="line-dot main-progress-3 progress-3">
                                                                    </div>
                                                                    <div class="line-dot progress-1 main-progress show-1 ">
                                                                    </div>
                                                                    <div class="line-dot progress-2 main-progress-2 show-2">
                                                                    </div>
                                                                    <div class="line-dot add-dot dot-show-3 show-3">
                                                                    </div>

                                                                </div>
                                                                <div class="line-data">
                                                                    <span>Sell Amount </span>
                                                                    <span>Payment info</span>
                                                                    <span>Acceptance</span>
                                                                    <span>Confirmation</span>
                                                                </div>
                                                            </div>
                                                            <div class="input-content-text sell-form">
                                                                <div class="col-md-12">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Review and Confirm</h6>
                                                                    </div>
                                                                    <!--<h5 class="body-data">Review and Confirm </h5>-->
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="sell-info">
                                                                        <p>Account Name:</p>
                                                                        <input type="text" class="removerror"  id="" name ="investmentName" data-id="${investmentHoldings.getBeneficiaryId()}" value="${investmentHoldings.getName()}" pattern="Enter Beneficary Name" placeholder="Enter Beneficary Name" readonly="readonly"/>
                                                                        <input type="hidden" id="unitprice" value="${investmentHoldings.getPrice()}"  readonly="readonly"/>
                                                                    </div>
                                                                    <div class="sell-info">
                                                                        <p>Fund Name</p>
                                                                        <input type="text" class="removerror" id=""  value="${portfolioDetail.fundName}" placeholder="" readonly="readonly"/>
                                                                    </div>
                                                                    <span class="error" id="error-investmentName"></span>
                                                                    <div class="sell-info">
                                                                        <p>Available Units:</p>
                                                                        <input type="text" class="removerror" id="availableunits" value="<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${investmentHoldings.getUnits()}"/>"  placeholder="" readonly="readonly"/>
                                                                    </div>
                                                                    <div class="sell-info">
                                                                        <p>Units Being Sold:</p>
                                                                        <input type="text" class="removerror" id="soldunits"  placeholder="" readonly="readonly"/>
                                                                    </div>
                                                                    <div class="sell-info">
                                                                        <p>Approximate sale value: </p>
                                                                        <input type="text" class="removerror" id="reviewgross"   placeholder="" readonly="readonly"/>
                                                                    </div>

                                                                    <div class="sell-info">
                                                                        <p>Reference No:</p>
                                                                        <input type="text" class="removerror" value="${ic}"   placeholder="" readonly="readonly"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="payment-btn1 btn-select1 ">
                                                                        <a href="javascript:void(0)" class="btn_payment previous4 payment-hover" id="">Back</a>
                                                                        <a href="javascript:void(0)" class="btn_payment1 next4 last-btn payment-hover" id="">Confirm Sale </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>




                            </form>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script type="text/javascript" src="resources/js/bootstrap-pincode-input.js"></script>
        <script src="./resources/js/sweetalert.min.js"></script>
        <script>
                                                                            $(document).ready(function () {
//                   var randomnum= Math.floor((Math.random() * 10000) + 1);
//                  $('.randomnumber').text("MIN"+randomnum);
                                                                                $('#pincode-input2').pincodeInput({hidedigits: false, inputs: 12});
                                                                                $('#pincode-input1').pincodeInput({hidedigits: false, inputs: 7});
                                                                                $('#pincode-input3').pincodeInput({hidedigits: false, inputs: 12});
                                                                                $('#pincode-input4').pincodeInput({hidedigits: false, inputs: 25});
                                                                            });
        </script>

        <script>
            $(".calculator-dropdown ul").on("click", ".init", function () {
                $(this).closest(".calculator-dropdown ul").children('li:not(.init)').toggle();
            });

            var allOptions = $(".calculator-dropdown ul").children('li:not(.init)');
            $(".calculator-dropdown ul").on("click", "li:not(.init)", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown ul").children('.init').html($(this).html());
                allOptions.toggle();
            });
        </script>
        <script>
            $(".calculator-dropdown2 ul").on("click", ".init2", function () {
                $(this).closest(".calculator-dropdown2 ul").children('li:not(.init2)').toggle();
            });

            var allOption = $(".calculator-dropdown2 ul").children('li:not(.init2)');
            $(".calculator-dropdown2 ul").on("click", "li:not(.init2)", function () {
                allOption.removeClass('selected');
                $(this).addClass('selected');
                $(".calculator-dropdown2 ul").children('.init2').html($(this).html());
                allOption.toggle();
            });
            $('.selectBank').click(function () {
                $('.selectBank').removeClass('selectedbank');
                $(this).addClass('selectedbank');
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".data-toggle").hide();
                $(".loader").hide();
                $(".see-toggle").click(function () {
                    $(".data-toggle").toggle();
                });
            });
        </script>
        <script>
            $(".removerror").click(function () {
                $('.error').text("");
            });
            $(".removerror").change(function () {
                $('.error').text("");
            });
            $(".previous2").click(function () {
                $("#step2").hide();
                $("#step1").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });

            $(".previous3").click(function () {
                $("#step3").hide();
                $("#step2").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });
            $(".previous4").click(function () {
                $("#step4").hide();
                $("#step3").show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });

            $(".next1").click(function (evt) {
                var redemption = $("#redemptionamount").val();
//                if (redemption === "" || parseInt(redemption) === 0) {
//                    if (redemption === "") {
//                        $("#error-redemption").text("This field is required");
//                    } else if (parseInt(redemption) === 0) {
//                        $("#error-redemption").text("Amount should be greater than 0");
//                    }
//                } else {
                    $("#step2").show();
                    $("#step1").hide();
                    $("html, body").animate({scrollTop: 0}, "fast");
//                }
            });
            $(".next2").click(function () {
                $("#step3").show();
                $("#step2").hide();
                $("html, body").animate({scrollTop: 0}, "fast");
            });

            $(".next3").click(function () {
                if (!$('input[name=acceptcheck]').is(':checked')) {
                    $("#error-checkbox").text("Please read and agree to the Terms and Conditions");
                } else {
                    var amount = $('#redemptionamount').val();
                    $('#reviewgross').val(amount);
                    var balunits = $('#unittype').val();
                    var balpercentage = $('#percentagetype').val();
                    $('#soldunits').val(balunits + " units (" + balpercentage + ")");
                    $("#step4").show();
                    $("#step3").hide();
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            });
            $(".next4").bind('click', function () {

                saveInvestment();
            });

            function checkAccountNO() {
                var ac = document.getElementById("accountNumber").value;
                if (ac.length === 2) {
                    var ac20 = ac.substr(0, 2);
                    var a = ac20 + '-';
                    $("#accountNumber").val(a);
                }
                if (ac.length === 7) {
                    var ac21 = ac.substr(7);
                    var b = ac + '-' + ac21;
                    $("#accountNumber").val(b);
                }
                if (ac.length === 15) {
                    var ac212 = ac.substr(15);
                    var c = ac + '-' + ac212;
                    $("#accountNumber").val(c);
                }
                if (ac.length > 18) {
                    var ac23 = ac.substr(0, 18);
                    var d = ac23;
                    $("#accountNumber").val(d);
                }
            }
            $('#amounttype').keyup(function () {
                $('.amountTypeError').text('');
                $('.percentageError').text('');
                $('.UnitsError').text('');
                $('#error-redemption').text('');
                var totalamount = $('#availablebalce').val();
                var totalunits = $('#availableunits').val();
                var unitprice = $('#unitprice').val();
                var amount = $(this).val();

                if (amount > parseFloat(totalamount.replace(/,/g, ''))) {
                    $('.amountTypeError').text("Amount can't be greater than available balance");
                    $('#redemptionamount').val('');
                } else {
//                alert(totalamount+"--------"+amount);
                    var percentage = (amount / parseFloat(totalamount.replace(/,/g, ''))) * 100;
                    var balunits = amount / unitprice;
                    if (amount === "") {
                        percentage = "";
                        balunits = "";
                    } else {
                        percentage = percentage.toFixed(2) + "%";
                        balunits = balunits.toFixed(2);
                    }
                    $('#unittype').val(balunits);
                    $('#redemptionamount').val(amount);
                    $('#percentagetype').val(percentage);
//                $('#grossproceed').val(amount);
                }
            });
            $('#percentagetype').keyup(function () {
                $('.amountTypeError').text('');
                $('.percentageError').text('');
                $('.UnitsError').text('');
                $('#error-redemption').text('');
                var totalamount = $('#availablebalce').val();
                var unitprice = $('#unitprice').val();
                var percentage = $(this).val().replace('%', "");
                if (percentage > 100) {
                    $('.percentageError').text("Percentage can't be greater than 100");
                    $('#redemptionamount').val('');
                } else {
//                alert(totalamount+"-----"+percentage);
                    var balance = (parseFloat(totalamount.replace(/,/g, '')) * percentage) / 100;
//                 alert(parseFloat(totalamount.replace(/,/g, ''))+"-----"+percentage+" ---"+balance);
                    var balunits = balance / unitprice;
                    if (percentage === "") {
                        balance = "";
                        balunits = "";
                    } else {
                        balance = balance.toFixed(2);
                        balunits = balunits.toFixed(2);
                    }
                    $('#unittype').val(balunits);
                    $('#redemptionamount').val(balance);
                    $('#amounttype').val(balance);
//                $('#grossproceed').val(balance);
                }
            });
            $('#unittype').keyup(function () {
                $('.amountTypeError').text('');
                $('.percentageError').text('');
                $('.UnitsError').text('');
                $('#error-redemption').text('');
                var totalamount = $('#availablebalce').val();
                var totalunits = $('#availableunits').val();
                var unitprice = $('#unitprice').val();
                var units = $(this).val();
                if (units > parseFloat(totalunits.replace(/,/g, ''))) {
                    $('.UnitsError').text("Units  can't be greater than available units");
                    $('#redemptionamount').val('');
                } else {
                    var balance = (unitprice * units);
                    var balpercenage = (balance / parseFloat(totalamount.replace(/,/g, ''))) * 100;

                    if (units === "") {
                        balance = "";
                        balpercenage = "";
                    } else {
                        balance = balance.toFixed(2);
                        balpercenage = balpercenage.toFixed(2) + "%";
                    }

                    $('#percentagetype').val(balpercenage);
                    $('#redemptionamount').val(balance);
                    $('#amounttype').val(balance);
//                $('#grossproceed').val(balance);
                }
            });

            function saveInvestment() {
                var bankAccountId = $('.selectedbank').find('.accountid').val();
                var investedAmount = $('#redemptionamount').val();
                var beneficiaryId = $('#beneficiaryname').data("id");
                var beneficiaryName = $('#beneficiaryname').val();
                var applicationId = '${applicationId}';
                var fundCode = '${pc}';
                var investmentCode = '${ic}';
                var unitprice = $('#unitprice').val();
                var units = $('#unittype').val();
                var portfolioName = '${portfolioDetail.fundName}';
                var obj = {id: "", beneficiaryName: beneficiaryName, applicationId: applicationId, bankAccountId: bankAccountId, investedAmount: investedAmount, portfolioCode: fundCode, beneficiaryId: beneficiaryId,
                    investmentcode: investmentCode, portfolioName: portfolioName, type: "WDW", status: "Pending", unitprice: unitprice, units: units};

                console.log(JSON.stringify(obj));
                swal({
                    title: "We've received your sell request",
                    text: "We'll confirm via email once this has been processed.",
//                    type: "info",
//                    timer: 2500,
                    showConfirmButton: true
                }, function () {
                    window.location = "./home";
                });

                $(".next4").unbind('click');
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/admin/pending-transaction',
                    data: JSON.stringify(obj),
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {

//                        location.reload(true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });


            }
        </script>

    </body>
</html>
