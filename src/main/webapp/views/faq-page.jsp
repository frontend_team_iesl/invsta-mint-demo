<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>

        <style>
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
          
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="element-box stick-faq">
                                        <div class="side-faq">
                                            <h5 class="faq-head">FAQ.</h5>
                                            <ul>
                                                <li><a href="#server-1" data-scroll="">How do I invest with invsta Asset Management?</a></li>
                                                <li><a href="#server-2" data-scroll="">How do I get product information such as PDS, SIPO, Fact Sheets and Fund Updates?</a></li>
                                                <li><a href="#server-3" data-scroll=""> What is a Portfolio Investment Entity (PIE)?</a></li>
                                                <li><a href="#server-4" data-scroll=""> What is the PIE regime?</a></li>
                                                <li><a href="#server-5" data-scroll="">What is a Prescribed Investor Rate (PIR)?</a></li>
                                                <li><a href="#server-6" data-scroll=""> Any unanswered questions?</a></li>
                                                <li><a href="#server-7" data-scroll="">  How do I top up my investment?</a></li>
                                                <li><a href="#server-8" data-scroll=""> How do I check on the valuation of my investment?</a></li>
                                                <li><a href="#server-9" data-scroll=""> Can I switch between invsta funds?</a></li>
                                                <li><a href="#server-10" data-scroll=""> How do I get a tax certificate?</a></li>
                                                <li><a href="#server-11" data-scroll=""> How do I redeem all or part of my investment?</a></li>
                                                <li><a href="#server-12" data-scroll="">   How do I change my income reinvestment choice or my bank details for income payments?</a></li>
                                                <li><a href="#server-13" data-scroll=""> How do I notify a change of address?</a></li>
                                                <li><a href="#server-14" data-scroll="">  Will we receive dividends or distributions?</a></li>
                                                <li><a href="#server-15" data-scroll=""> How are fees paid?</a></li>
                                                <li><a href="#server-16" data-scroll="">How do I make a complaint?</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-1">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I invest with invsta Asset Management?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   
                                     <p> Application forms can be found at the back of the Product Disclosure Statement (PDS) which can be downloaded directly from the website,
                                         obtained through a financial adviser or you can contact us for a hard copy. Completed application forms should be sent to: invsta Asset Management, 
                                         c/o MMC Limited, PO Box 106 039, Auckland 1143. Payment can be made by cheque, electronic banking and/or direct debit. Further details can be found in the PDS.<br><br>
                                           Please note that the first application for an investment with invsta Asset Management will require proof of identity together with your IRD number and PIR (Prescribed Investor Rate).</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-2">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I get product information such as PDS, SIPO, Fact Sheets and Fund Updates?</h5>
                                                </div>
                                                <div class="side-pera">
                                                    
                                         <p>You may obtain information including a PDS, Statement of Investment Policy and Objectives (SIPO) and product information from either your financial adviser,
                                             downloading the offer documents from the website, or by emailing info@invsta.com and we will forward documents to you.
                                             This information can also be found at www.business.govt.nz/disclose.</p>
    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-3">
                                             <div class="side-faq">
                                                    <h5 class="faq-head">What is a Portfolio Investment Entity (PIE)?</h5>
                                                </div>
                                                <div class="side-pera">
                                                    
      <p>Under changes to New Zealand legislation a managed fund may register to be a Portfolio Investment Entity which means investment income within the managed fund is subject to different rules relating to tax.
          All our retail products are currently registered as PIEs.
          Among these rules capital gains on all New Zealand and some Australian shares incur no tax while other income is subject to tax calculated on the basis of each investor’s Prescribed Investment Rate (PIR).</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-4">
                                               <div class="side-faq">
                                                    <h5 class="faq-head">What is the PIE regime?</h5>
                                                </div>
                                                <div class="side-pera">
                                                    <p>      The Portfolio Investment Entity (PIE) regime is a regime for the taxation of PIE managed funds. The PIE's income is attributed to its investors, based on their shares of the PIE, and is taxed at each investor's Prescribed Investor Rate (PIR). The PIE regime creates tax advantages for some investors, mainly because the maximum PIR is 28%. The regime therefore provides tax benefits to investors who are on a 30% or 33% marginal tax rate and invest in a PIE.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-5">
                                               <div class="side-faq">
                                                    <h5 class="faq-head">What is a Prescribed Investor Rate (PIR)?</h5>
                                                </div>
                                                <div class="side-pera">
                                                        <p>  A PIR is the tax rate used to calculate tax on taxable income. Each investor, whether an individual, company, trustee, registered charity or superannuation fund, has a PIR which you must supply at the time of investment. In order to work out which rate applies to you please contact the IRD www.ird.govt.nz  </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-6">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">Any unanswered questions?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>Please visit the Existing Investors TAB.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-7">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I top up my investment?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p> Additional investments can be made on the Application Form attached to the PDS or by contacting us. Any subsequent investment will not require you to re-submit proof of identity.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-8">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I check on the valuation of my investment?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>      Our website is updated weekly on Tuesday morning for last week's unit price and at month end two days after the valuation has been signed.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-9">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">Can I switch between invsta funds?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>     Yes, you can switch your investments between the invsta funds at no cost. You will need to complete an application form for the new fund and send this to our administrators with instructions to switch your holding.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-10">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I get a tax certificate?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>      A tax statement will be sent to all investors following the financial year end at 31 March. If you are due a rebate, we will liaise with the IRD on your behalf and will purchase additional units in the fund with the proceeds. If you owe money to the IRD we will deduct units from your holding to pay the IRD.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-11">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I redeem all or part of my investment?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>
      A request to redeem part or all of your investment must be made in writing to the Administration Manager, invsta Asset Management, c/o MMC, PO Box 106-039, Auckland 1143. The letter must include your client number and the bank account details proceeds are to be paid to and be signed by the same people who signed the application form.
    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-12">
                                              <div class="side-faq">
                                                    <h5 class="faq-head"> How do I change my income reinvestment choice or my bank details for income payments?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>A request to change these details must be made in writing to the Administration Manager, invsta Asset Management, c/o MMC, PO Box 106-039, Auckland 1143.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-13">
                                              <div class="side-faq">
                                                    <h5 class="faq-head"> How do I notify a change of address?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>   Notification of a change of address must be made in writing to the Administration Manager, invsta Asset Management, c/o MMC, PO Box 106-039, Auckland 1143.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-14">
                                              <div class="side-faq">
                                                    <h5 class="faq-head"> Will we receive dividends or distributions?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>      The invsta Diversified Income Fund intends paying a quarterly distribution to investors. If investors in this fund do not wish to receive distributions, they can tick the box on the application to have these reinvested. The invsta Australasian Equity Fund, the invsta Australasian Property Fund and the invsta Diversified Growth Fund do not intend to pay distributions. If you are invested in these funds and wish to receive an income stream from your investment please contact us.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-15">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">  How are fees paid?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>      The funds are subject to the costs associated with running them which include an annual management, administration, custody, trustee and audit fees. These are deducted directly from the fund and the unit price posted on the website at each month end incorporates these.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="element-box" id="server-16">
                                              <div class="side-faq">
                                                    <h5 class="faq-head">How do I make a complaint?</h5>
                                                </div>
                                                <div class="side-pera">
                                                   <p>
      Any complaint should be directed in writing to invsta Asset Management Limited, PO Box 91649 Auckland Mail Centre, and attention: Compliance Manager. You may also contact the Trustee in writing at Public Trust, PO Box 1598 Shortland Street, Auckland 1140, and attention: Manager Client Services.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script type="text/javascript">
        $(window).load(function () {

            $(".loader").fadeOut("slow");
        });
    </script>
    <!--    <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/all-funds',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $(".loader").hide();
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $.each(obj, function (idx, val) {
                            if (val.Code === "290002") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore1').attr('href', id1);
                            }
                            if (val.Code === "290004") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore2').attr('href', id1);
                            }
                            if (val.Code === "290006") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore3').attr('href', id1);
                            }
                            if (val.Code === "290012") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore4').attr('href', id1);
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
        </script>-->
    <script>
//        $('#viewmore1').click(function () {
//            $('.hidediv1').show();
//            $('#viewmore1').hide();
//        });
//        $('#viewmore2').click(function () {
//            $('.hidediv2').show();
//            $('#viewmore2').hide();
//        });
//        $('#viewmore3').click(function () {
//            $('.hidediv3').show();
//            $('#viewmore3').hide();
//        });
        $('.clickinput').on("click", function () {
            var recemt = $(this).closest('.funds-deatil');
            recemt.find(".offer-input").toggle();
        });
        $(document).ready(function () {
            $(".loader").fadeOut("slow");
            $('.hidediv1').hide();
            $('.hidediv2').hide();
            $('.hidediv3').hide();
        });
        $('.democlick').click(function () {
            var data = $(this).data("target");
            $('.showclick').hide();
            $(data).show();
        });
    </script>
    <script>
				jQuery(document).ready(function($) {
					// Scroll to the desired section on click
					// Make sure to add the `data-scroll` attribute to your `<a>` tag.
					// Example: 
					// `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.
					function scrollToSection(event) {
						event.preventDefault();
						var $section = $($(this).attr('href')); 
						$('html, body').animate({
							scrollTop: $section.offset().top-90
						}, 500);
					}
					$('[data-scroll]').on('click', scrollToSection);
				}(jQuery));
			</script>
</body>
</html>