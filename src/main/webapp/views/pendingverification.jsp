<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="./resources/favicon.png" rel="shortcut icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <!--<link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>-->
        <!--<link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/css/style.css" rel="stylesheet"/>
        <!--<link href="https://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/idVerification.css" rel="stylesheet"/>
        <link href="./resources/css/bootstrap-pincode-input.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--  <link href="resources/css/osfonts.css" rel="stylesheet"> -->
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>


        <style>

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
            .container_contact:nth-child(1) {
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <table id="pending-verification-table" class="table table-lightborder new-table-design">
                                    <thead>
                                        <tr>
                                            <th>
                                                Verification Pending 
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <th><div class="clone-box">
                                                    <div class="container_contact " id="clone-container">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p><span class="heading_ver">Created Date:</span>2019-07-30 21:50:02.0</p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <h5 class="person-detail"></h5>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Email: <span class="heading_ver username"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Name: <span class="heading_ver name"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>D.O.B: <span class="heading_ver dob5"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Address: <span class="heading_ver address"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Source of Fund:  <span class="heading_ver src"></span>Savings</p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>How much to invest:- <span class="heading_ver how-invest"></span>Less than $1,000</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5>Tax Details</h5>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Resident of Countries:<span class="heading_ver country"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Tax Resident of Countries:<span class="heading_ver taxCountry"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Tax No:<span class="heading_ver taxno"></span></p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5 class="id_type"></h5>
                                                            </div>  
                                                            <div class="col-md-12">
                                                                <p>Issue Country: <span class="heading_ver issue_country"></span></p>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="data-insert">
                                                                    <p class="text-change">Licence No: </p><span class="heading_ver id_number"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <p><span class="heading_ver verification"></span></p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>Expiry Date: <span class="heading_ver licence_expiry_Date"></span></p>
                                                            </div>                                                
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5 class="">Politically exposed person(PEP)</h5>
                                                            </div>  
                                                            <div class="col-md-3">
                                                                <p><span class="heading_ver PEPverification"></span></p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 new-design-btn">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <a href="javascript:void(0)" class="btn-task">
                                                                            <div class="btn btn-primary btn-info profile-btns ">
                                                                                View
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <a href="javascript:void(0)" class="btn-task">
                                                                            <div class="btn btn-primary btn-info profile-btns approved">
                                                                                Approved
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <!--                                                                        <a href="javascript:void(0)" class="btn-task" edit-data>
                                                                                                                                                    <div class="btn btn-primary btn-info profile-btns ">
                                                                                                                                                        Edit
                                                                                                                                                    </div>
                                                                                                                                                </a>-->
                                                                        <button type="button" class=" btn btn-primary btn-info profile-btns documentCheck" data-toggle="modal" data-target=".documentVerify" onclick="documentVerify(this);"> Edit</button>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <a href="javascript:void(0)" class="btn-task">
                                                                            <div class="btn btn-primary btn-info profile-btns">
                                                                                Cancel
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="footer.jsp"/>
        <jsp:include page = "../views/models/documentVerify.jsp"></jsp:include>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script type="text/javascript" src="./resources/js/countries.js"></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>

        <script>
                                                                            function documentVerify(ele) {
                                                                                var docObj = $(ele).data("json");
                                                                                if (docObj.reg_type === "INDIVIDUAL_ACCOUNT" || docObj.reg_type === "MINOR_ACCOUNT" || docObj.reg_type === "JOINT_ACCOUNT") {
                                                                                    $('#id').val(docObj.id);
                                                                                    $('#regId').val(docObj.reg_id);
                                                                                    $('#regType').val(docObj.reg_type);
                                                                                    $('#first_name').val(docObj.firstName);
                                                                                    $('#middle_name').val(docObj.middleName);
                                                                                    $('#last_name').val(docObj.lastName);
                                                                                    $('#dateofBirth').val(docObj.date_of_Birth);
                                                                                    $('#Passportnumber').val(docObj.passport_number);
                                                                                    $('#passportExpire').val(docObj.passport_expiry);
                                                                                    $('#licenseNumber').val(docObj.license_number);
                                                                                    $('#Versionnumber').val(docObj.licence_verson_number);
                                                                                } else if (docObj.reg_type === "COMPANY_ACCOUNT" || docObj.reg_type === "TRUST_ACCOUNT") {
                                                                                    $('#id').val(docObj.id);
                                                                                    $('#regType').val(docObj.reg_type);
                                                                                    $('#first_name').val(docObj.firstName);
                                                                                    $('#middle_name').val(docObj.middleName);
                                                                                    $('#last_name').val(docObj.lastName);
                                                                                    $('#dateofBirth').val(docObj.dateOfBirth);
                                                                                    $('#Passportnumber').val(docObj.passportNumber);
                                                                                    $('#passportExpire').val(docObj.passportExpiryDate);
                                                                                    $('#licenseNumber').val(docObj.licenseNumber);
                                                                                    $('#Versionnumber').val(docObj.versionNumber);
                                                                                }
                                                                            }
                                                                            ;
                                                                            $('.clickinput').on("click", function () {
                                                                                var recemt = $(this).closest('.funds-deatil');
                                                                                recemt.find(".offer-input").toggle();
                                                                            });
                                                                            $(document).ready(function () {
                                                                                $(".loader").fadeOut("slow");
                                                                                $('.hidediv1').hide();
                                                                                $('.hidediv2').hide();
                                                                                $('.hidediv3').hide();
                                                                            });
                                                                            $.ajax({
                                                                                type: 'GET',
                                                                                url: './rest/groot/db/api/get-pending-registration',
                                                                                headers: {"Content-Type": 'application/json'},
                                                                                success: function (data, textStatus, jqXHR) {
                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                    console.log(obj);

                                                                                    //                  document.getElementsByClassName("container_contact")[0].style.display = "none";    
                                                                                    for (var i = 0; i < obj.length; i++) {
                                                                                        if (obj[i].status === "SUBMISSION") {
                                                                                            var url = './rest/groot/db/api/get-pending-verification-' + obj[i].token;
                                                                                            $.ajax({
                                                                                                type: 'GET',
                                                                                                url: url,
                                                                                                headers: {"Content-Type": 'application/json'},
                                                                                                success: function (data, textStatus, jqXHR) {
                                                                                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                                                                                                    console.log(obj);
                                                                                                    console.log("ghdhdhgf---" + data);
                                                                                                    var x = document.getElementById("clone-container").cloneNode(true);
                                                                                                    x.getElementsByClassName("documentCheck")[0].dataset.target = '.documentVerify';
                                                                                                    x.getElementsByClassName("documentCheck")[0].dataset.json = data;

                                                                                                    if (obj.reg_type === "INDIVIDUAL_ACCOUNT" || obj.reg_type === "MINOR_ACCOUNT") {
                                                                                                        if (obj.reg_type === "INDIVIDUAL_ACCOUNT") {
                                                                                                            x.getElementsByClassName("person-detail")[0].innerHTML = 'Individual Details';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("person-detail")[0].innerHTML = 'Minor Details';
                                                                                                        }
                                                                                                        x.getElementsByClassName("username")[0].innerHTML = obj.email;
                                                                                                        x.getElementsByClassName("name")[0].innerHTML = obj.fullName;
                                                                                                        x.getElementsByClassName("dob5")[0].innerHTML = obj.date_of_Birth;
                                                                                                        x.getElementsByClassName("address")[0].innerHTML = obj.homeAddress;
                                                                                                        x.getElementsByClassName("country")[0].innerHTML = obj.country_residence;
                                                                                                        //                                x.getElementsByClassName("taxCountry")[0].innerHTML = obj.countryOfTaxResidence;
                                                                                                        x.getElementsByClassName("taxno")[0].innerHTML = obj.tin;
                                                                                                        x.getElementsByClassName("id_type")[0].innerHTML = obj.id_type;
                                                                                                        if (obj.investor_idverified === "true") {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Verified';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Not Verified';
                                                                                                        }
                                                                                                        if (obj.pepStatus === "true") {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'Yes';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'No';
                                                                                                        }
                                                                                                        if (obj.id_type === "NZ Driver Licence") {
                                                                                                            //                                    alert(obj.id_type);
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Licence No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.license_number;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.licence_expiry_Date;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = 'New Zealand';
                                                                                                            //                                x.getElementsByClassName("licence_verson_number")[0].innerHTML = obj.licence_verson_number;
                                                                                                        } else if (obj.id_type === "NZ Passport") {
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Passport No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.passport_number;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.passport_expiry;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.passport_issue_by;
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.id_type;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.other_id_expiry;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.other_id_issueBy;
                                                                                                        }

                                                                                                    } else if (obj.reg_type === "JOINT_ACCOUNT") {
                                                                                                        x.getElementsByClassName("person-detail")[0].innerHTML = 'Joint Details';
                                                                                                        x.getElementsByClassName("username")[0].innerHTML = obj.email;
                                                                                                        x.getElementsByClassName("name")[0].innerHTML = obj.fullName;
                                                                                                        x.getElementsByClassName("dob5")[0].innerHTML = obj.date_of_Birth;
                                                                                                        x.getElementsByClassName("address")[0].innerHTML = obj.homeAddress;
                                                                                                        x.getElementsByClassName("country")[0].innerHTML = obj.country_residence;
                                                                                                        //                                x.getElementsByClassName("taxCountry")[0].innerHTML = obj.countryOfTaxResidence;
                                                                                                        x.getElementsByClassName("taxno")[0].innerHTML = obj.tin;
                                                                                                        x.getElementsByClassName("id_type")[0].innerHTML = obj.id_type;
                                                                                                        if (obj.investor_idverified === "true") {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Verified';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Not Verified';
                                                                                                        }
                                                                                                        if (obj.pepStatus === "true") {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'Yes';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'No';
                                                                                                        }
                                                                                                        if (obj.id_type === "NZ Driver Licence") {
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Licence  No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.license_number;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.licence_expiry_Date;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = 'New Zealand';
                                                                                                            //                                x.getElementsByClassName("licence_verson_number")[0].innerHTML = obj.licence_verson_number;
                                                                                                        } else if (obj.id_type === "NZ Passport") {
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Passport No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.passport_number;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.passport_expiry;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.passport_issue_by;
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.id_type;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.other_id_expiry;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.other_id_issueBy;
                                                                                                        }
                                                                                                    } else if (obj.reg_type === "COMPANY_ACCOUNT" || obj.reg_type === "TRUST_ACCOUNT") {
                                                                                                        if (obj.reg_type === "COMPANY_ACCOUNT") {
                                                                                                            x.getElementsByClassName("person-detail")[0].innerHTML = 'Company Details';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("person-detail")[0].innerHTML = 'Trust Details';
                                                                                                        }
                                                                                                        x.getElementsByClassName("username")[0].innerHTML = obj.email;
                                                                                                        x.getElementsByClassName("name")[0].innerHTML = obj.companyName;
                                                                                                        x.getElementsByClassName("dob5")[0].innerHTML = obj.dateOfBirth;
                                                                                                        x.getElementsByClassName("address")[0].innerHTML = obj.companyAddress;
                                                                                                        x.getElementsByClassName("country")[0].innerHTML = obj.holderCountryOfResidence;
                                                                                                        //                                x.getElementsByClassName("taxCountry")[0].innerHTML = obj.countryOfTaxResidence;
                                                                                                        //                                x.getElementsByClassName("taxno")[0].innerHTML = obj.companyTaxIdentityNumber;
                                                                                                        x.getElementsByClassName("id_type")[0].innerHTML = obj.srcOfFund;
                                                                                                        if (obj.isInvestor_idverified === "true") {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Verified';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("verification")[0].innerHTML = 'Not Verified';
                                                                                                        }
                                                                                                        if (obj.pepStatus === "true") {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'Yes';
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("PEPverification")[0].innerHTML = 'No';
                                                                                                        }
                                                                                                        if (obj.srcOfFund === "NZ Driver Licence") {
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Licence No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.licenseNumber;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.licenseExpiryDate;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = 'New Zealand';
                                                                                                            //                                x.getElementsByClassName("licence_verson_number")[0].innerHTML = obj.licence_verson_number;
                                                                                                        } else if (obj.srcOfFund === "NZ Passport") {
                                                                                                            x.getElementsByClassName("text-change")[0].innerHTML = 'Passport No:';
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.passportNumber;
                                                                                                            x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.passportExpiryDate;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.countryOfIssue;
                                                                                                        } else {
                                                                                                            x.getElementsByClassName("id_number")[0].innerHTML = obj.typeOfID;
                                                                                                            //                                    x.getElementsByClassName("licence_expiry_Date")[0].innerHTML = obj.other_id_expiry;
                                                                                                            x.getElementsByClassName("issue_country")[0].innerHTML = obj.typeCountryOfIssue;
                                                                                                        }
                                                                                                    }
                                                                                                    $(".clone-box").append(x);
                                                                                                },
                                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    console.log(jqXHR);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                        ;
                                                                                    }
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    console.log(jqXHR);
                                                                                }
                                                                            });
                                                                            $('.democlick').click(function () {
                                                                                var data = $(this).data("target");
                                                                                $('.showclick').hide();
                                                                                $(data).show();
                                                                            });
        </script>
    </body>
</html>