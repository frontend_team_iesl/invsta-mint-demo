<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>



        <style>

            img.logo {
                width: 40%;
            }


            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Investment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row1">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Investment Options </h6>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar1">
                                                                                    <div class="line-dot">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-8">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <h5 class="text-center" style="text-align:left!important">How would you like to pay?</h5>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="body-bank radoi-set">
                                                                                                    <label class="bank-radio  ">
                                                                                                        <i class="fa fa-university" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Poli</span>
                                                                                                        <p class="bank-card-text">4.71 NZD in total fees.</p>
                                                                                                        <input type="radio" checked="checked" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio ">
                                                                                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Debit Card</span>
                                                                                                        <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio">
                                                                                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Credit Card</span>
                                                                                                        <p class="bank-card-text">5.20 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                    <label class="bank-radio continue">
                                                                                                        <i class="fa fa-university" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Bank Transfer</span>
                                                                                                        <p class="bank-card-text">3.44 NZD in total fees.</p>
                                                                                                        <input type="radio" name="radio">
                                                                                                        <span class="checkmark"></span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-bank trans-space">
                                                                                                    <h6 class="body-data">Transfer details</h6>
                                                                                                </div>
                                                                                                <div class="body-trans-space">
                                                                                                    <label class="balance-result">
                                                                                                        <i class="fa fa-bolt" aria-hidden="true"></i>
                                                                                                        <span class="bank-card">Your balance</span>
                                                                                                        <p class="bank-card-text">Open a borderless account and add funds to instantly pay for your transfers.</p>
                                                                                                    </label>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="trans-detail">
                                                                                                    <div class="trans-bank">
                                                                                                        <h6 class="body-data">Transfer details</h6>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">You send</span> 
                                                                                                        <span class="second-span">100 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Fee (included):</span> 
                                                                                                        <span class="second-span">-4.71 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Amount we'll convert</span> 
                                                                                                        <span class="second-span">95.29 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Guaranteed rate (48 hours)</span> 
                                                                                                        <span class="second-span">0.66075</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">You get</span> 
                                                                                                        <span class="second-span">62.96 NZD</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Should arrive</span> 
                                                                                                        <span class="second-span">by Aug 1</span>
                                                                                                    </div>
                                                                                                    <div class="trans-bank trans-space">
                                                                                                        <h6 class="body-data">Recipient details</h6>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Name</span> 
                                                                                                        <span class="second-span">Kayla White</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Email</span> 
                                                                                                        <span class="second-span">white.kaylaj@ gmail.com</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Routing number</span> 
                                                                                                        <span class="second-span">031176110</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Account number</span> 
                                                                                                        <span class="second-span">157040704</span>
                                                                                                    </div>
                                                                                                    <div class="trans-data">
                                                                                                        <span class="first-span">Account type</span> 
                                                                                                        <span class="second-span">Checking</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <!-- <script src="resources/bower_components/moment/moment.js" ></script>
        
        <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script type="text/javascript">
            $(window).load(function () {

                $(".loader").fadeOut("slow");
            });
        </script>

        <!--        <script>
                    //        $('#viewmore1').click(function () {
                    //            $('.hidediv1').show();
                    //            $('#viewmore1').hide();
                    //        });
                    //        $('#viewmore2').click(function () {
                    //            $('.hidediv2').show();
                    //            $('#viewmore2').hide();
                    //        });
                    //        $('#viewmore3').click(function () {
                    //            $('.hidediv3').show();
                    //            $('#viewmore3').hide();
                    //        });
                    $('.clickinput').on("click", function () {
                        var recemt = $(this).closest('.funds-deatil');
                        recemt.find(".offer-input").toggle();
                    });
                    $(document).ready(function () {
                        $(".loader").fadeOut("slow");
                        $('.hidediv1').hide();
                        $('.hidediv2').hide();
                        $('.hidediv3').hide();
                    });
                    $('.democlick').click(function () {
                        var data = $(this).data("target");
                        $('.showclick').hide();
                        $(data).show();
                    });
                </script>-->

        <script>
            $(document).ready(function () {
                $(".loader").fadeOut("slow");
                $(".bank-radio ").click(function () {
                    $(".bank-radio ").removeClass("intro");
                    $(this).addClass("intro");
                });
                $(".continue").click(function () {
                    window.location.href = './banktransfer';
                });
            });

        </script>
    </body>
</html>