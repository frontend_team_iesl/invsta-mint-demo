<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>

       
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"https://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <!-- <script>
                window.intercomSettings = {
                app_id: "q28x66d9"
                };
                </script>
                <script>
                (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>  -->
        <style>
            .el-chart-w {
                overflow: visible!important;
            }
            .portfolio_overlay {
                height: 75%;
            }
            .more_fund tr:nth-child(odd) {
                background-color: #dddddd;
            }
            .row.performance-inline {
                display: flex;
            }

            .col-md-3.col-sm-3.five-div {
                flex: 1;
            }

            span.risk-btns .active {
                background: red;
            }
            span.risk-btns.active {
                /*                background: #4564d0;*/
                color: #fff;
                background: #65b9ac;
            }
            .data-funds::-webkit-scrollbar {
                width: 7px;
            }
            a.fund-detail-btn {
                /*                background: #2e5ea5;*/
                background: #65b9ac;
                padding: 8px 10px;
                color: #fff;
                border-radius: 3px;
                font-weight: 400;
                font-size: 13px;
            }
            .bootstrap3 .fixed-table-toolbar {
                display: none;
            }
            .value.percentage.founder-icon-color {
                color: #0f9d58;
                font-size: 18px;
                margin-left: 10px;
                margin-right: 24px;
            }

            .value.marketValue {
                float: initial!important;
            }
            table.table-value-footer th {
                padding: 0.75rem;
                width: 35%;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
        <style>
            .selectedbank{
                background-color: #65b9ac;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Investment</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <p>Hi Claudia, here's a more detailed look at this particular investment. </p>
                                        <br>
                                        <h6 class="element-header" id="fundName"></h6>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <div class="element-box " style="display: flex; justify-content: space-between;     background: initial; box-shadow: initial;">
                                            <!--<a href="#" data-target=".add-funds-to-portfolio" id="add-funds-to-portfolio-action" data-toggle="modal" class="color3 btn btn-success btn-hover clickmodal" data-id="INV" style="width:40%">Add Funds</a>-->
                                            <a href="./make-transaction-${pc}?ic=${ic}"  id="add_fund"  class="color3 btn btn-success btn-hover clickmodal"  style="width:40%">Add Funds</a>
                                            <a href="#" data-target=".sell-funds-to-portfolio" id="sell-funds-to-portfolio-action" data-toggle="modal" class="color3 btn btn-danger btn-hover clickmodal" data-id="WDW" style="width:40%">Sell Funds</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-box">
                                        <div class="os-tabs-w">
                                            <div class="os-tabs-controls">
                                                <ul class="nav nav-tabs smaller">
                                                    <!--  <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab-live">Live</a>
                                                    </li> -->
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab-latestweek">Investment Performance</a>
                                                    </li>
                                                </ul>
                                                <ul class="nav nav-pills smaller hidden-sm-down">
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane" id="tab-balance" style="">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Current Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">
                                                    </div>
                                                </div>
                                                <!-- <div class="tab-pane active" id="tab-live">
                                                        <div class="">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                        Live Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png" data-toggle="tooltip" title="The live balance chart provides an indication of the real-time balance of this portfolio. It is reliant on external data sources which be different to what is reported on exchanges. The actual daily closing value and withdrawal value of your portfolio will be different.">
                                                        </div>
                                                        <div id="livebalance1" style="min-width: 100%; height: 400px; margin: 0 auto;"></div> 
                                                </div> -->
                                                <div class="tab-pane active" id="tab-latestweek" style="">
                                                    <span style="display: flex; justify-content: space-between;">


                                                        <div class="label">
                                                            <!--                                                            Performance-->
                                                            <!--                                                            <img src="https://backoffice.invsta.io/pocv/resources/img/info-icon.png" style="vertical-align:text-bottom;   margin-left: 5px;" data-toggle="tooltip" >-->
                                                        </div>
                                                    </span>
                                                    <div class="" style="padding:0 0 10px;width: 100%;float: left; display:flex; justify-content:space-between" >
                                                        <div class="contribution_value">
                                                            <div class="label lbl-main">
                                                                Contributions 
                                                                <!--<div class="pulsating-circle new-circle-1" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                            </div>
                                                            <div class="value contributions add-value">$90,688 
                                                            </div>
                                                        </div>
                                                        <div class="current_value">
                                                            <div class="label  lbl-main">
                                                                CURRENT Value
                                                                <!--<div class="pulsating-circle new-circle-2" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom"></div>-->
                                                            </div>
                                                            <div class="value marketValue add-value">
                                                                $29,493 
                                                                <!-- <span class="founder-icon-color">16.46%  <i class="fa fa-arrow-up" aria-hidden="true"></i></span> -->
                                                            </div>
                                                            <div class="value percentage founder-icon-color">
                                                                45.17%<div class="pulsating-circle new-circle-1 check-info" data-toggle="tooltip" title="This is an annualised* money weighted** return after fees and tax, since you have been invested***.The return weights how long each amount has been invested for by how much has been invested to work out the average years each dollar has been invested for. If you have been invested for less than one year, then the return is not annualised.**The return takes into account when your cash flows occur and how big they are.***The return is calculated on all funds that you are currently invested in and any funds that you previously held.Past performance is not a reliable indicator of future returns." data-placement="right" style="vertical-align:text-bottom"></div> 
                                                                <!-- <span class="founder-icon-color">16.46%  <i class="fa fa-arrow-up" aria-hidden="true"></i></span> -->
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="stockbalance" style="min-width: 100%; height: 270px; margin: 0 auto;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row performance-inline">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">Investment Details </h6>
                                    </div>
                                    <div id="elementboxcontent" class="">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller ajust-div">
                                                    <div class="label">
                                                        Current Price
                                                    </div>
                                                    <div class="value setval">
                                                        $2.4069 
                                                    </div>
                                                    <!-- <div class="trending trending-up">
                                                            <span>16.86%</span>
                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div">
                                                    <div class="label">
                                                        Units
                                                    </div>
                                                    <div class="value" id="units">
                                                        253.8487 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div">
                                                    <div class="label ">
                                                        Distribution Method
                                                    </div>
                                                    <div class="value " id="currentInvSqr">
                                                        Reinvested
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div">
                                                    <div class="label">
                                                        Tax owed
                                                    </div>
                                                    <div class="arrow-text">
                                                        <div class="value per- per-color- taxOwed">
                                                            -$15.40 
                                                        </div>
                                                        <!-- <i class=" os-icon os-icon-arrow-up6"></i> -->
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-12">
                                        <div id="elementboxcontent" class="element-box">
                                        <div class="element-wrapper">
                                        <h6 class="element-header">Latest Fund Update <a style="float:right;" href="resources/fund-investor-update-pdf.pdf" class="update-pdf-btn">Download Full PDF Update</a></h6>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12 update-fund-pera">
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>
                                        </div>
                                        </div>
                                        </div>
                                </div> -->

                                <!--  <div class="col-md-6">
                                        <div class="element-box">
                                        <div class="element-wrapper">
                                        <h6 class="element-header">Fund Breakdown</h6>
                                        </div>
                                        <div class="el-chart-w" id="container23" style="width:100%"></div>
                                        </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">Fund Details: </h6>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="elementboxcontent" class="element-box table-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header" id="heading-fund">Month in review</h6>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 " id="dis-fund">
                                                <div class="data-funds">
                                                    <h6>${portfolioDetail.getMonthlyReturnValue()}</h6>
                                                    <p>${portfolioDetail.getMonthlyFundUpdate()}</p>                    
                                                    <p> Established in 1994, Aditya Birla Sun Life Mutual Fund (ABSLMF) is co-sponsored by Aditya Birla Capital Limited (ABCL) and Sun Life (India) AMC Investments Inc.</p>
                                                    <p>
                                                        Having total domestic assets under management (AUM) of close to Rs. 2423 billion for the quarter ended December 31st, 2018, ABSLMF is one of the leading Fund Houses in India based on domestic average AUM as published by the Association of Mutual Funds of India (AMFI). ABSLMF has an impressive mix of reach, a wide range of product offerings across equity, debt, balanced as well as structured asset classes and sound investment performance, and around 6.8 million investor folios as of December 31st, 2018.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="element-box">

                                                <div class="risk-indicator">
                                                    <div class="element-wrapper">
                                                        <h6 class="element-header">Risk Indicator 
                                                            <div class="pulsating-circle circle-1" data-toggle="tooltip" title="Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund." data-placement="right" style="vertical-align:text-bottom"></div></h6>
                                                    </div>
                                                    <div class="Higher">
                                                        <span class="risk-set">Lower risk</span>
                                                        <span>Higher risk</span>
                                                    </div>
                                                    <span class="indicator-box">
                                                        <span class="risk-btns ">1</span>
                                                        <span class="risk-btns">2</span>
                                                        <span class="risk-btns">3</span>
                                                        <span class="risk-btns active">4</span>
                                                        <span class="risk-btns">5</span>
                                                        <span class="risk-btns">6</span>
                                                        <span class="risk-btns">7</span>
                                                    </span>
                                                    <div class="Higher">
                                                        <span class="risk-set">Potentially <br>lower returns</span>
                                                        <span>Potentially <br>higher returns</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-12">

                                            <div class="element-box">
                                                <div class="element-wrapper">
                                                    <h6 class="element-header">Asset Allocation </h6>
                                                </div>
                                                <div class="el-chart-w" id="assetAllocation" style="height:283px"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--                                <div class="col-md-12">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Recent Fund Update: </h6>
                                                                    </div>
                                                                </div>-->
                                <div class="col-md-12">
                                    <div id="elementboxcontent" class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Fund description </h6>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" id="fundupdate">
                                                <p>${portfolioDetail.getFundOverview()}</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 more_fund">
                                    <div class="more-fund-data">
                                        <div class="risk-indicator">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Top 10 Assets </h6>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="bene-transactions-table" data-toggle="table" data-pagination="false" data-search="false" data-show-columns="true" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                           data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                        <thead>
                                                            <tr>
                                                                <!--<th data-field="id" data-checkbox="true"></th>-->
                                                                <th class="teble-design" data-field="investmentName" ><span style="color: black;">Asset Name</span></th>
                                                                <!--<th data-field="AssetClass"><span style="color: black;">Sector Name</span></th>-->
                                                                <!--<th data-field="InvestorAssetValue" ><span style="color: black;">Investor Asset Value</span></th>-->
                                                                <th data-formatter="PercentageFormatter" ><span style="color: black;"> Percentage</span></th>                                                                  
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <table class="table-value-footer totaltable">
                                                        <thead>
                                                            <tr>
                                                                <!--<th data-field="id" data-checkbox="true"></th>-->
                                                                <th class="teble-design"><span style="color: black;">Total</span></th>
                                                                <!--<th><span style="color: black;"></span></th>-->
                                                                <!--<th data-field="InvestorAssetValue" ><span style="color: black;">Investor Asset Value</span></th>-->

                                                                <th ><span class="total-value" style="color: black;">100%</span></th>                                                                  
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade sell-funds-to-portfolio add-funds-to-portfolio" id="add-funds-to-portfolio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Invsta Diversified Income Fund </h5>

                                </div>
                                <div class="modal-body bank-model">
                                    <div class="row">



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Beneficiary Name :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <c:forEach items="${beneficiaryDetails}" var="category">
                                                    <input type="text" value="${category.getName()}" id="beneficiaryId" data-id="${category.getId()}">
                                                </c:forEach>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Transaction Amount :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="number" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount" min="0"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12 bankaccounthide">
                                            <div class="form-group">
                                                <label>Bank Account Details:</label><br>
                                            </div>
                                        </div>
                                    </div>
                                    <c:forEach items="${bankAccountDetails}" var="bank">
                                        <div class="bankaccountdiv bank-status">
                                            <input type="hidden" name="id" class="bankid" value=" ${bank.getId()}">
                                            <input type="hidden" name="id" class="beneficiarid" value="${bank.getBeneficiaryId()}">
                                            <div class="row">
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Account Name
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group accountname">
                                                            ${bank.getAccountName()}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Bank
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group bankname">
                                                            ${bank.getBank()}  
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Branch
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group branchname">
                                                            ${bank.getBranch()} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Account 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group account">
                                                            ${bank.getAccount()} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="btn-pay">
                                                        <a type="button" class="btn selectBank" data-id="${bank.getId()}">Select</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary close-model btn-new" data-dismiss="modal">Close</button>
                                    <button type="button" id="saveInvestmentbtn" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="display-type"></div>
                </div>
                 <jsp:include page="footer.jsp"/>
                <script src="resources/bower_components/jquery/dist/jquery.min.js" defer></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
                <!--  <script src="resources/bower_components/moment/moment.js" defer></script>
                        <script src="resources/bower_components/ckeditor/ckeditor.js" defer></script>
                        <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" defer></script>
                <script src="resources/bower_components/dropzone/dist/dropzone.js" defer></script> -->
                <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
                <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/util.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/alert.js" defer></script>
                        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/button.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/carousel.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/collapse.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/modal.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
                        <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/popover.js" defer></script> -->
                <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
                <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
                <script src="https://code.highcharts.com/highcharts.js" ></script> 
                <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
                <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
                <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
                <script src="https://code.highcharts.com/modules/cylinder.js"></script>
                <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script>
                <script src="./resources/js/sweetalert.min.js"></script>
             
                <script>
                    var type = "";
                    var totalfmcaTopAssetsesPrice = 0;
                    
                    function PercentageFormatter(value, row, index) {
                        
                    var num = (row.price);
                    var per = ((num * 100) /totalfmcaTopAssetsesPrice).toFixed(2) ;
                    return  per + '%';
                    }
                    $(document).ready(function () {
                    $('.totaltable').hide();
//                    $('.bankaccountdiv').hide();
//                    $('.bankaccounthide').hide();
                    var amount = 0;
                    var value = 0;
                    var units = 0;
                    var price = 0;
                    var TaxOwed = 0;
                    var priceArr = [];
                    var dateArr = [];
                    var assetArr = [];
                    var ic = '${ic}';
                    var pc = '${pc}';
                    $('#heading-fund').html('<span>Month in review</span> <a href="" class="fund-detail-btn">Download Full Update</a>');
                    $.ajax({
                    type: 'GET',
                            url: './rest/groot/db/api/show-me-more?ic=${ic}&pc=${pc}',
                            headers: {"Content-Type": 'application/json'},
                            success: function (data, textStatus, jqXHR) {
                            $(".loader").hide();
                            console.log(data);
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            
                            $.each(obj.fmcaTopAssetsesByTable, function (idx, val) {
                            totalfmcaTopAssetsesPrice=  totalfmcaTopAssetsesPrice + val.price;
                            });
                            var investmentHolding = obj.investmentHoldings[0];
                            var portfolioName = investmentHolding.PortfolioName;
                            $('#fundName').text(portfolioName);
                            $.each(obj.pricePortfolioUnitPrices, function (idx, val) {
                            priceArr.push(val.Prices);
                            dateArr.push(val.Date);
                            });
                            var year = new Date(dateArr[0]).getFullYear();
                            var month = new Date(dateArr[0]).getMonth();
                            var day = new Date(dateArr[0]).getDay();
                            perfomancechart(priceArr, year, month, day);
                            $('.contributions').text('$' + investmentHolding.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                            $('.marketValue').text('$' + investmentHolding.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                            $('#units').text(investmentHolding.Units.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                            $('#currentInvSqr').text(investmentHolding.DistMethod);
                            $('.setval').text('$' + investmentHolding.Price.toFixed(4));
                            $('.taxOwed').text('$' + investmentHolding.TaxOwed.toFixed(2));
                            //                $.each(obj.fmcaInvestmentMix, function (idx, val) {
                            //                    assetArr.push([val.FMCAAssetClass, Math.round(val.SectorValueBase)]);
                            //                });
                            $.each(obj.fmcaInvestmentMixByTable, function (idx, val) {
                            assetArr.push([val.InvestmentName, Math.round(val.Price)]);
                            });
                            piechart1('assetAllocation', assetArr);
                            $('#bene-transactions-table').bootstrapTable('load', obj.fmcaTopAssetsesByTable);
                            $('.totaltable').show();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                            }
                    });
                    });
//            if (pc === '290002') {
//            //            $('#dis-fund').html('<h6>Our portfolio returned 3.21% for the month.</h6><p>The main positive contributions came from the interest rate sensitive holdings in the portfolio, with Auckland Airport, Contact Energy and Meridian Energy leading the way for us. The main negative contributors were the a2 Milk Company, Fletcher Building and Link Administration Holdings.</p><p>During the month, we increased holdings in Afterpay Touch Group and Lend Lease Group, and we added Arvida Group. We exited Link Administration Holdings (having lost conviction in the name), and eased back a little on Mercury and Mainfreight into very strong share prices.Arvida raised new equity during the month to acquire three villages, two in Tauranga and one in Queenstown, for $180m. </p><p>The acquisition adds 326 independent living units to Arvida`s existing portfolio of 3,677 units and beds. The acquisition also provides Arvida with additional brownfield development opportunities.</p>');
//            $('#fundupdate').html('<p>This is a single asset class Fund, investing predominantly in Australasian equities, and targeting medium to long-term growth. Investors should expect returns and risk commensurate with the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX50 Gross Index (which is the Fund`s relevant market index) by 3% per annum, before fees, over the medium to long-term.</p>');
//            $('#month-value').html('3.21%');
//            $('#3month-value').html('5.63%');
//            $('#year-value').html('14.08%');
//            $('#5year-value').html('14.61%');
//            $('.percentage').html('16.22%');
//            $('.total-value').html('67.20%');
//            } else {
//            $('#heading-fund').html('<span>Monthly Fund Update</span> <a href="" class="fund-detail-btn">Download Full Update</a>');
//            $('#dis-fund').html('<h6>Our portfolio returned 5.01% for the month.</h6><p>The top positive contributors were holdings in Precinct Property and Kiwi Property Group. The key detractors were holdings in Aveo Group and Lend Lease Group.</p><p>Over the month, there was a flurry of capital raisings and we participated in the Arvida, Charter Hall Long WALE REIT and GPT issues. We also increased exposure to Stride and Lend Lease. We reduced exposure to Kiwi Property Group, Mirvac and Investore as they reached price targets. Arvida (ARV) announced during June that it was acquiring three villages, two in Tauranga and one in Queenstown, for $180m - partly funded by new equity. The acquisition also provides ARV with additional development opportunities.</p><p>Charter Hall and Abacus together launched a takeover offer for Australian Unity Office (AOF) after acquiring a 20% stake on market. GPT raised $0.8bn to acquire two Sydney office assets. With retail REITs still trading below NTA, Scentre Group sold a large asset and launched a share buyback.</p>');
//            $('#fundupdate').html('<p>This is a single asset class Fund, investing predominantly in Australasian listed property securities. Investors should expect returns and risk commensurate with the listed property sector of the New Zealand and Australian share markets.</p><p>Our objective is to outperform the S&P/NZX All Real Estate (Industry Group) Gross Index (which is the Fund`s relevant market index) by 1% per annum, before fees, over the medium to long-term.</p>');
//            $('#month-value').html('5.01%');
//            $('#3month-value').html('10.19%');
//            $('#year-value').html('25.00%');
//            $('#5year-value').html('10.16%');
//            $('.percentage').html('16.31%');
//            $('.total-value').html('84.96%');
//            }
                    //        $.ajax({
                    //            type: 'GET',
                    //            url: './rest/groot/db/api/portfolio-dashboa
                    //            headers: {"Content-Type": 'application/json'},
                    //            success: function (data, textStatus, jqXHR) {
                    //                console.log(data);
                    //                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    //                console.log(obj.fmcaTopAssets);
                    //                $('#bene-transactions-table').bootstrapTable('load', obj.fmcaTopAssets);
                    //                //                $.each(obj.pricePortfolioUnitPrices, function (idx, val) {
                    //                //                    priceArr.push(val.Prices);
                    //                //                    dateArr.push(val.Date);
                    //                //                });
                    //                //                $.each(obj.fmcaInvestmentMix, function (idx, val) {
                    //                //                    assetArr.push([val.FMCAAssetClass, Math.round(val.SectorValueBase)]);
                    //                //                });
                    //                //                var year = new Date(dateArr[0]).getFullYear();
                    //                //                var month = new Date(dateArr[0]).getMonth();
                    //                //                var day = new Date(dateArr[0]).getDay();
                    //                //                piechart1('assetAllocation', assetArr);
                    //                //                perfomancechart(priceArr, year, month, day);
                    //            },
                    //            error: function (jqXHR, textStatus, errorThrown) {
                    //            }
                    //        });

                    //                            $.ajax({
                    //                            type: 'GET',
                    //                                    url: './rest/groot/db/api/beneficiary-dashboard?
                    //                                    headers: {"Content-Type": 'application/json'},
                    //                                    success: function (data, textStatus, jqXHR) {
                    //                                    alert(data);
                    //                                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    //                                    $.each(obj.transactionSummary, function (idx, val) {
                    //                                    if (val.Category === "Contribution" && val.PortfolioCode === pc) {
                    //                                    $('#fund-name').text(val.PortfolioName);
                    //                                    amount = amount + val.Amount;
                    //                                    }
                    //                                    if (val.PortfolioCode === pc) {
                    //                                    value = value + val.Amount;
                    //                                    }
                    //                                    });
                    //                                    $('.contributions').text('$' + amount.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                    //                                    $('.marketValue').text('$' + value.toFixed(0).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                    //                                    var inc = value - amount;
                    //                                    var per = inc * 100 / amount;
                    //                                    per = per.toFixed(2);
                    //                //                    $('.percentage').text(per + '%');
                    //                                    //                                               var PortfolioHoldings = obj.investmentSummary.PortfolioHoldings;
                    //                                    $.each(obj.investmentSummary.PortfolioHoldings, function (idx, val) {
                    //                                    if (val.PortfolioCode === pc) {
                    //                                    units = units + val.Units;
                    //                                    price = price + val.Price;
                    //                                    }
                    //                                    });
                    //                                    $.each(obj.investmentHoldings, function (idx, val) {
                    //                                    if (val.PortfolioCode === pc) {
                    //                                    TaxOwed = TaxOwed + val.TaxOwed;
                    //                                    }
                    //                                    });
                    //                                    TaxOwed = Math.abs(TaxOwed);
                    //                                    $('#units').text(units.toFixed(0));
                    //                                    $('.setval').text('$' + price.toFixed(4));
                    //                                    $('.taxOwed').text('-$' + TaxOwed.toFixed(0));
                    //                                    },
                    //                                    error: function (jqXHR, textStatus, errorThrown) {
                    //                                    }
                    //                            });
//                    $('#beneficiaryId').change(function () {
//                    $('.bankaccountdiv').hide();
//                    $('.bankaccounthide').hide();
//                    var beneficiaryid = $(this).val();
//                    var beneficiaryval = $('#beneficiaryId option:selected').text();
//                    $('#investmentName').val(beneficiaryval);
//                    var beneficiarvals = document.getElementsByClassName('beneficiarid');
//                    for (var i = 0; i < beneficiarvals.length; i++) {
//                    var beneficiarval = beneficiarvals[i];
//                    var root = $(beneficiarval).closest('.bankaccountdiv');
//                    if (beneficiarval.value === beneficiaryid) {
//                    root.show();
//                    $('.bankaccounthide').show();
//                    }
//                    }
//                    });
                    $('.clickmodal').click(function(){
                    type = $(this).attr("data-id")

                    });
                    $('.selectBank').click(function(){
                    $('.selectBank').removeClass('selectedbank');
                    $(this).addClass('selectedbank');
                    });
                    $('#saveInvestmentbtn').click(function () {
                    var bankAccountId = $('.selectedbank').data("id");
//                    var investmentName = $('#investmentName').val();
                    var investmentAmount = $('#investmentAmount').val();
                    var beneficiaryId = $('#beneficiaryId').data("id");
                    var obj = {investmentcode:'${ic}', amount: investmentAmount, portfolioCode: '${pc}', beneficiaryId: beneficiaryId, bankAccountId: bankAccountId, type:type};
                    swal({
                    title: "Proceed",
                            text: "Transaction is progress.",
                            type: "info",
                            timer: 2500,
                            showConfirmButton: true
                    });
                    $.ajax({
                    type: 'POST',
                            url: './rest/groot/db/api/admin/pending-transaction',
                            data: JSON.stringify(obj),
                            headers: {"Content-Type": 'application/json'},
                            success: function (data, textStatus, jqXHR) {
                            swal({
                            title: "Success",
                                    text: "Transaction is successfully.",
                                    type: "success",
                                    timer: 2500,
                                    showConfirmButton: true
                            });
                            location.reload(true);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                            alert("error");
                            }
                    });
                    });
                </script>                
                <script>
                    function piechart1(idc, assetArr) {
                    Highcharts.setOptions({
                    colors: ['#93dbd0', '#606065', '#799fc3', '#c64b38']
                    });
                    Highcharts.chart(idc, {

                    chart: {
                    plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                    },
                            credits: {
                            enabled: false,
                            },
                            exporting: {
                            enabled: false,
                            },
                            title: {
                            text: ''
                            },
                            tooltip: {
                            pointFormat: '<b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                            pie: {
                            innerSize: 70,
                                    depth: 45,
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                    distance: 2,
                                            connectorWidth: 0,
                                            enabled: false,
                                            format: '{point.percentage:.0f} %'
                                    },
                                    showInLegend: true
                            }
                            },
                            series: [{
                            name: 'Brands',
                                    colorByPoint: true,
                                    data: assetArr
                            }]
                    });
                    }
                </script>
                <script>
                    // Create a timer
                    function perfomancechart(arr, year, month, day) {
                    // Create the chart
                    Highcharts.stockChart('stockbalance', {
                    chart: {

                    type: 'spline',
                            zoomType: 'x',
                            events: {
                            load: function () {
                            if (!window.TestController) {
                            this.setTitle(null, {
                            text: ''
                            });
                            }
                            }
                            },
                            zoomType: 'x'
                    },
                            rangeSelector: {

                            buttons: [{
                            type: 'day',
                                    count: 3,
                                    text: '3d'
                            }, {
                            type: 'week',
                                    count: 1,
                                    text: '1w'
                            }, {
                            type: 'month',
                                    count: 1,
                                    text: '1m'
                            }, {
                            type: 'month',
                                    count: 6,
                                    text: '6m'
                            }, {
                            type: 'year',
                                    count: 1,
                                    text: '1y'
                            }, {
                            type: 'all',
                                    text: 'All'
                            }],
                                    selected: 3
                            },
                            yAxis: {
                            title: {
                            text: ''
                            }
                            },
                            title: {
                            text: ''
                            },
                            credits: {
                            enabled: false
                            },
                            exporting: {
                            enabled: false
                            },
                            subtitle: {
                            text: ''
                            },
                            series: [{
                            name: '$',
                                    data: arr,
                                    pointStart: Date.UTC(year, month, day),
                                    pointInterval: 1200 * 60 * 60 * 24,
                                    tooltip: {
                                    valueDecimals: 1,
                                    xDateFormat: '%Y-%m-%d',
                                            valueSuffix: ''
                                    }
                            }]

                    });
                    }
                </script>
                <script>
                    Highcharts.chart('stockbalance1', {
                    chart: {
                    type: 'areaspline'
                    },
                            accessibility: {
                            description: ''
                            },
                            title: {
                            text: ''
                            },
                            subtitle: {
                            text: ''
                            },
                            legend: {
                            enabled: false
                            },
                            xAxis: [{
                            categories: ['21 Oct', '28 Oct', '4 Nov', '11 Nov', '18 Nov', '25 Nov',
                                    '1 Dec', '8 Dec']

                            }],
                            yAxis: {
                            min: 5000, max: 25000,
                                    title: {
                                    text: ''
                                    },
                                    labels: {
                                    formatter: function () {
                                    return this.value / 1000 + 'k';
                                    }
                                    }
                            },
                            navigation: {
                            buttonOptions: {
                            enabled: false
                            }
                            },
                            credits: {
                            enabled: false
                            },
                            tooltip: {
                            pointFormat: '{series.name} $ <b>{point.y:,.0f}</b><br/> {point.x}'
                            },
                            plotOptions: {
                            area: {
                            marker: {
                            enabled: false,
                                    symbol: 'circle',
                                    radius: 2,
                                    states: {
                                    hover: {
                                    enabled: true
                                    }
                                    }
                            }
                            }
                            },
                            series: [{
                            name: '',
                                    data: [
                                            10000, 10400, 11000, 11800, 12500, 13294
                                    ]
                            }]
                    });
                </script>
                <script>
                    Highcharts.chart('livebalance1', {
                    chart: {
                    type: 'spline',
                            animation: Highcharts.svg, // don't animate in old IE
                            marginRight: 10,
                            events: {
                            load: function () {

                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                            series.addPoint([x, y], true, true);
                            }, 1000);
                            }
                            }
                    },
                            time: {
                            useUTC: false
                            },
                            credits: {
                            enabled: false
                            },
                            plotOptions: {
                            series: {
                            color: '#03A9F4',
                                    shadow: true,
                                    lineWidth: 3,
                                    marker: {
                                    enabled: false
                                    }
                            }
                            },
                            title: {
                            text: ''
                            },
                            xAxis: {
                            type: 'datetime',
                                    tickPixelInterval: 150
                            },
                            yAxis: {

                            title: {
                            text: 'Portfolio Investments ($)'
                            },
                                    plotLines: [{
                                    value: 0,
                                            width: 1,
                                            color: '#808080'
                                    }]
                            },
                            tooltip: {
                            headerFormat: '<b>{series.name}</b><br/>',
                                    pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
                            },
                            legend: {
                            enabled: false
                            },
                            exporting: {
                            enabled: false
                            },
                            series: [{
                            name: 'Random data',
                                    data: (function () {
                                    // generate an array of random data
                                    var data = [],
                                            time = (new Date()).getTime(),
                                            i;
                                    for (i = - 19; i <= 0; i += 1) {
                                    data.push({
                                    x: time + i * 1000,
                                            y: Math.random()
                                    });
                                    }
                                    return data;
                                    }())
                            }]
                    });
                </script>

                <script>
                    $.getJSON('https://www.highcharts.com/samples/data/aapl-c.json', function (data) {

                    // Create the chart
                    Highcharts.stockChart('container', {

                    rangeSelector: {
                    selected: 1
                    },
                            title: {
                            text: 'AAPL Stock Price'
                            },
                            navigator: {
                            enabled: false
                            },
                            series: [{
                            name: 'AAPL Stock Price',
                                    data: data,
                                    tooltip: {
                                    valueDecimals: 4
                                    }
                            }]
                    });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                    $("#view_transaction").hide();
                    $("#view_transaction_status").click(function () {
                    $("#view_transaction").toggle();
                    });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                    $(".company-show1").hide();
                    $(".company-show2").hide();
                    $(".company-show3").hide();
                    $(".company-show4").hide();
                    $(".company-show5").hide();
                    $(".company-show6").hide();
                    $(".company-show7").hide();
                    $(".company-show8").hide();
                    $(".company-show9").hide();
                    $(".companyshow1").click(function () {
                    $(".company-show1").toggle();
                    });
                    $(".companyshow2").click(function () {
                    $(".company-show2").toggle();
                    });
                    $(".companyshow3").click(function () {
                    $(".company-show3").toggle();
                    });
                    $(".companyshow4").click(function () {
                    $(".company-show4").toggle();
                    });
                    $(".companyshow5").click(function () {
                    $(".company-show5").toggle();
                    });
                    $(".companyshow6").click(function () {
                    $(".company-show6").toggle();
                    });
                    $(".companyshow7").click(function () {
                    $(".company-show7").toggle();
                    });
                    $(".companyshow8").click(function () {
                    $(".company-show8").toggle();
                    });
                    $(".companyshow9").click(function () {
                    $(".company-show9").toggle();
                    });
                    });
                </script>


                </body>
                </html>									