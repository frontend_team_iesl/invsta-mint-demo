<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>



        <style>

            img.logo {
                width: 40%;
            }

            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Payment</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active p-31" id="one" role="tabpanel" aria-labelledby="one-tab">
                                                <div class="row1">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">Payment</h6>
                                                        </div>

                                                        <div class="form-data">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12 box-payment-space">
                                                                        <div class="content-body payment-box-space">
                                                                            <div class="line-progress">
                                                                                <div class="line-bar1 ">
                                                                                    <div class="line-dot ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="line-data">
                                                                                    <span>Amount</span>
                                                                                    <span>You</span>
                                                                                    <span>Recipient</span>
                                                                                    <span>Review</span>
                                                                                    <span>Pay</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="header-border">

                                                                            </div>
                                                                            <div class="input-content-text">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h5 class="text-center">Bank transfer Instructions?</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="currency-page1">
                                                                                    <div class="trans-detail">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="body-section">

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Payee name</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title=""></a>-->
                                                                                                    <div class="tooltip">TransferWise Limited <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Use the reference</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">P2980490</a>-->
                                                                                                    <div class="tooltip">P2980490 <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Amount to send</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">90 NZD</a>-->
                                                                                                    <div class="tooltip">90 NZD <span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="instruction-body">
                                                                                                    <h5>Account number</h5>
                                                                                                    <!--<a href="" data-toggle="tooltip" data-placement="auto" title="Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.">02-0108-0344040-000</a>-->
                                                                                                    <div class="tooltip">02-0108-0344040-000<span class="tooltiptext">Don't forget your reference This code id unique to you and hels us process your bank tranfer fater. When you make your bank transfer to us, please make sure you write it in the reference field.</span></div>
                                                                                                   
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="instruction-body">
                                                                                                    <p>TransferWise never takes money automatically from your account.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div class="instruction-body">
                                                                                                    <p>You can use your bank's online banking or mobile app to make your bank transfer to TransferWise.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="instruction-btn">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="tree-btn">
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl">I've made my bank transfer</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">I'll transfer my money later</a>
                                                                                                    <a href="javascript:void(0)" class="btn btn-primary btn-styl transparent-style">Cancel this transfer</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="btn btn-primary" id="viewmore1">View More</a>       -->        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
        </script>
         <script>
            $(document).ready(function () {
                $(".loader").fadeOut("slow");
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>


        <!--        <script>
                    //        $('#viewmore1').click(function () {
                    //            $('.hidediv1').show();
                    //            $('#viewmore1').hide();
                    //        });
                    //        $('#viewmore2').click(function () {
                    //            $('.hidediv2').show();
                    //            $('#viewmore2').hide();
                    //        });
                    //        $('#viewmore3').click(function () {
                    //            $('.hidediv3').show();
                    //            $('#viewmore3').hide();
                    //        });
                    $('.clickinput').on("click", function () {
                        var recemt = $(this).closest('.funds-deatil');
                        recemt.find(".offer-input").toggle();
                    });
                    $(document).ready(function () {
                        $(".loader").fadeOut("slow");
                        $('.hidediv1').hide();
                        $('.hidediv2').hide();
                        $('.hidediv3').hide();
                    });
                    $('.democlick').click(function () {
                        var data = $(this).data("target");
                        $('.showclick').hide();
                        $(data).show();
                    });
                </script>-->
       
    </body>
</html>