<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>All Beneficiaries</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
       
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                         <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                             <a href="./home">Beneficiaries</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container emp-profile newone">
                                        <table id="benificary-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <!--<th data-checkbox="true"></th>-->
                                                    <!--<th data-field="title" data-formatter=""><b>Title</b></th>-->
                                                    <th data-field="Id" ><span >Id</span></th>
                                                    <th data-field="IrdNumber" ><span >Ird Number</span></th>
                                                    <th data-field="Name" ><span>Name</span></th>
                                                    <th data-field="AMLEntityType" ><span>AML Entity Type</span></th>
                                                    <th data-field="Status" ><span>Status</span></th>
                                                    <th data-formatter="viewButtonFormatter" >Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
                <jsp:include page="footer.jsp"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>






<!--        <script>
            $('#viewmore1').click(function () {
                $('.hidediv1').show();
                $('#viewmore1').hide();
            });
            $('#viewmore2').click(function () {
                $('.hidediv2').show();
                $('#viewmore2').hide();
            });
            $('#viewmore3').click(function () {
                $('.hidediv3').show();
                $('#viewmore3').hide();
            });
            $('.clickinput').on("click", function () {
                var recemt = $(this).closest('.funds-deatil');
                recemt.find(".offer-input").toggle();
            });
            $(document).ready(function () {

                $('.hidediv1').hide();
                $('.hidediv2').hide();
                $('.hidediv3').hide();

            });
        </script>-->
        <script>
            function viewButtonFormatter(value, row, index) {
                if(parseInt(row.total)>1){
                    return '<a href="./home-beneficiary-db?id=' + row.UserId + '&bi=' + row.Id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>View</strong></span></a> <a href="./advisor-dashboard-' + row.UserId + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Dashboard</strong></span></a>';           
                }else{
                return '<a href="./home-beneficiary-db?id=' + row.UserId + '&bi=' + row.Id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>View</strong></span></a> <a href="./home-beneficiary-dashboard-db?id=' + row.UserId + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Dashboard</strong></span></a>';
            }
        }
        </script>
        <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/beneficiaries',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        $('#benificary-table').bootstrapTable('load', obj.beneficiaries);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR);
                    }
                });
            });
        </script>

    </body>
</html>