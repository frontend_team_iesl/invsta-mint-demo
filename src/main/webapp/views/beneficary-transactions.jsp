
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        <style>
            .btn-group {
    width: auto!important;
    justify-content: space-between;
    flex-wrap: inherit;
}
.columns.columns-right.btn-group.float-right {
    display: none;
}
.btn-secondary {
    background-color: #93dbd0;
    border-color: #e9eaed;
}
.page-item.active .page-link {
    background-color: #93dbd0;
    border-color: #e9eaed;
}
.page-link {
    color: #93dbd0;
}
.page-link:focus, .page-link:hover {
     color: #93dbd0;
}
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                             <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Transactions</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">

                                <div class="col-md-12 invst-option">
                                    <div class="card mt-3 tab-card">
                                        <!-- <div class="card-header tab-card-header">
                                                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item">
                                                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Funds</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Direct Companies</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">New Offers</a>
                                                        </li>
                                                </ul>
                                        </div> -->

                                        <table id="bene-transactions-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="fasle" data-show-pagination-switch="fasle" data-show-refresh="false" data-key-events="fasle" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="fasle" data-click-to-select="fasle" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="id" data-checkbox="true"></th>
                                                    <!--<th data-field="Id" ><span >Id</span></th>-->
                                                    <!--<th data-field="InvestmentCode" ><span>Investment Code</span></th>-->
                                                    <th data-field="EffectiveDate" ><span>Effective Date</span></th>
                                                    <!--<th data-field="Type" ><span>Type</span></th>-->
                                                    <th data-field="TypeDisplayName"><span>Type Display Name</span></th>
                                                    <th data-field="PortfolioName"><span>Portfolio Name</span></th>
                                                    <th data-field="TransactionTypeDescription" ><span>Transaction Type Description</span></th>
                                                    <th data-field="SubType" ><span>Sub Type</span></th>
                                                    <th data-field="Units" ><span>Units</span></th>
                                                    <th data-field="Price" ><span>Price</span></th>
                                                    <th data-field="Value" ><span>Value</span></th>
                                                    <th data-field="Tax" ><span>Tax</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter">Action</th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 

    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: './rest/groot/db/api/all-funds',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    //                        alert(data);
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    $.each(obj, function (idx, val) {
                        if (val.Code === "290002") {
                            var id1 = './investment-fund-' + val.Code;
                            $('.showmemore1').attr('href', id1);
                        }
                        if (val.Code === "290004") {
                            var id1 = './investment-fund-' + val.Code;
                            $('.showmemore2').attr('href', id1);
                        }
                        if (val.Code === "290006") {
                            var id1 = './investment-fund-' + val.Code;
                            $('.showmemore3').attr('href', id1);
                        }
                        if (val.Code === "290012") {
                            var id1 = './investment-fund-' + val.Code;
                            $('.showmemore4').attr('href', id1);
                        }

                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        });
    </script>
    <script>
            $(document).ready(function () {

                $.ajax({
                    type: 'GET',
                    url: './rest/3rd/party/api/bene-transactions-${code}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        var transactions = obj.investmentTransactions.Items;
                        $('#bene-transactions-table').bootstrapTable('load', transactions);
                        var TotalItemCount = obj.investmentTransactions.TotalItemCount;
                        var FieldTotals = obj.investmentTransactions.FieldTotals;
                        var Units = FieldTotals.Units;
                        var Value = FieldTotals.Value;
                        var Cash = FieldTotals.Cash;
                        var Tax = FieldTotals.Tax;
                        var UnitsScale = FieldTotals.UnitsScale;
                        var TotalContributions = FieldTotals.TotalContributions;
                        var TotalWithdrawals = FieldTotals.TotalWithdrawals;
                        var TotalTransferIn = FieldTotals.TotalTransferIn;
                        var Total = FieldTotals.Total;
                        setTransactionsStatus(TotalItemCount, Units, Value, Cash, Tax, UnitsScale, TotalContributions, TotalWithdrawals, TotalTransferIn, Total);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });


            });
        </script>

</body>
</html>