<style>
    @media (min-width:320px) and (max-width:576px){
        header {
            padding: 10px !important;
        }
        header a.logo {
            width: 70px;
        }
        .breadcrumb {
            margin-top: 0.1rem!important;
        }
        .content-w, .menu-side .content-w {
            padding-top: 3rem!important;
        }
        .bar1, .bar2, .bar3 {
            width: 25px!important;
            height: 4px!important;
            margin: 4px 0!important;
        }
        .change .bar1 {
            transform: rotate(-45deg) translate(-4px, 3px)!important;
        }
    }
</style>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!--<div class="onloader">
<div class="infinityChrome">
    <div></div>
    <div></div>
    <div></div>
</div>
    </div>-->
<div class="header-bar">
    <div class="container">
        <header>
            <div class="mobile-logo">
                <a class="logo" href="./home">
                    <img class="main-logo" src="resources/images/mint.png">
                </a>
                <div class="hamburger">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
            </div>
            <nav class="nav-responsive">
                <div class="logged-user-w">
                    <div class="avatar-w">
                        <c:choose>
                            <c:when test="${not empty user.userLogoPath}">
                                <img class="" src="${user.userLogoPath}" alt="" />
                            </c:when>
                            <c:otherwise>
                                <img alt="" src="./resources/images/avtar.png"> 
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">                 
                            ${user.name}
                        </div>
                        <div class="logged-user-role">
                            <sec:authorize access="!hasAuthority('BENEFICIARY')">
                                ${user.role}
                            </sec:authorize>
                        </div>
                        <div class="logged-user-role coustmer-Id">
                            <sec:authorize access="hasAuthority('BENEFICIARY')">
                                ${user.investmentCode}
                            </sec:authorize>
                        </div>
                    </div>
                    <div class="logged-user-menu">
                        <ul>
                            <li>
                                <a href="javascript:void(0)" class="menu-profile"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                            </li>
                            <li>
                                <a href="./login?logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul>                    
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li>
                            <a href="./admin-dashboard">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="fa fa-address-card-o"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>Admin Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-beneficiaries-db">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="os-icon os-icon-window-content"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>All Users Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-notes">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="icon-name">
                                        <span>Notes</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-verify">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <i class="fa fa-support" aria-hidden="true"></i>
                                    </div>
                                    <div class="icon-name">
                                        <span>Verify</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasAuthority('ADVISOR')">
                        <li>
                            <a href="./advisor-dashboard">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="fa fa-address-card-o"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>Advisor Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-beneficiaries-db">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="os-icon os-icon-window-content"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>All Users Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./home-notes">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="icon-name">
                                        <span>Notes</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasAuthority('BENEFICIARY')">
                        <li>
                            <a href="./home">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="os-icon os-icon-window-content"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>User Dashboard</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="!hasAuthority('ADVISOR')">
                        <li>
                            <a href="./investment">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <div class="os-icon os-icon-hierarchy-structure-2"></div>
                                    </div>
                                    <div class="icon-name">
                                        <span>Investment Options</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <sec:authorize access="hasAuthority('ADMIN')">
                            <li>
                                <a href="./home-pendingtransaction">
                                    <div class="icon-data">
                                        <div class="icon-w">
                                            <i class="fa fa-line-chart"></i>
                                        </div>
                                        <div class="icon-name">
                                            <span>Pending Options</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javaScript:void(0)">
                                    <div class="icon-data">
                                        <div class="icon-w">
                                            <i class="fa fa-user-o"></i> 
                                        </div>
                                        <div class="icon-name">
                                            <span>Verification Options</span>
                                        </div>
                                    </div>
                                </a>
                                <a href="./home-pendingverification">
                                    <div class="icon-data">
                                        <div class="icon-w">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div class="icon-name">
                                            <span>Pending Verification</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="menu-payment">
                                    <div class="icon-data">
                                        <div class="icon-w">
                                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                        </div>
                                        <div class="icon-name">
                                            <span>Payment</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </sec:authorize>
                        <li>
                            <a href="javascript:void(0)" class="menu-txns">
                                <div class="icon-data">
                                    <div class="icon-w">
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                    </div>
                                    <div class="icon-name">
                                        <span>Cash &amp; Transactions</span>
                                    </div>
                                </div>
                            </a>
                        </li>

                    </sec:authorize>
                    <li>
                        <a href="./home-newsResearch">
                            <div class="icon-data">
                                <div class="icon-w">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                </div>
                                <div class="icon-name">
                                    <span>News &amp; Research</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="./home-support">
                            <div class="icon-data">
                                <div class="icon-w">
                                    <i class="fa fa-support" aria-hidden="true"></i>
                                </div>
                                <div class="icon-name">
                                    <span>Support</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <c:if test="${not empty allNotification}">
                        <li class="sub-menu">
                            <a href="javascript:void(0)" class="notify-pop " onmouseover="myaddtion1()" onmouseout="myaddtion1()">
                                <div class="icon-w">
                                    <!--<i class="fa fa-support" aria-hidden="true"></i>-->
                                    <i class="fa fa-bell"></i>
                                </div>
                                <span>Notification</span>
                                <span class="popuptext" id="add-Popup1">
                                    <c:forEach items="${allNotification}" var="notify">
                                        <div class="pop-main child-clone" >
                                            <i class="fa fa-times delete-notify" data-id="${notify.id}" onclick="deleteNotify(this)" aria-hidden="true"></i>
                                            <div class="section-pop-1 section-pop-2 " id="parent-notify">

                                                <i class="fa fa-dot-circle-o"></i>
                                                <div class="notify-data " data-id="${notify.id}"  onclick="investment(this)">
                                                    <c:choose>
                                                        <c:when test="${'INVESTMENT_PAYMENT' eq notify.type}">
                                                            <div class="notify-type" data-id="${notify.type}" style="dispaly:none"> </div>
                                                            <div class="notify-code" data-id="${notify.portfolioCode}" style="dispaly:none"> </div>
                                                            <h6 class="notify">Pending Investment</h6>
                                                            <p class="notify-desc">You have a Pending Investment name ${notify.investmentName} of amount ${notify.investedAmount} in portfolio ${notify.name}</p>
                                                        </c:when>
                                                        <c:when test="${'TRANSACTION_PAYMENT' eq notify.type}">
                                                            <div class="notify-type" data-id="${notify.type}" style="dispaly:none"> </div>
                                                            <div class="notify-code" data-id="${notify.portfolioCode}" style="dispaly:none"> </div>
                                                            <h6 class="notify">Pending Transaction</h6>
                                                            <p class="notify-desc">You have a Pending Transaction name ${notify.investmentName} of amount ${notify.investedAmount} in portfolio ${notify.name}</p>
                                                        </c:when>
                                                    </c:choose>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </span>
                            </a> 
                        </li>
                    </c:if>

                </ul>

            </nav>
        </header>
    </div>
</div>
<div id="menys" class="desktop-menu menu-side-w menu-activated-on-click">
    <div class="logo-w">
        <a class="logo" href="./home">
            <img class="main-logo" src="resources/images/mint.png"> 
        </a>
        <a class="logo1" href="./home" style="display:none">
            <img class="main-logo" src="resources/images/mint.png">
        </a>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="logged-user-i">
                <div class="avatar-w">
                    <c:choose>
                        <c:when test="${not empty user.userLogoPath}">
                            <img class="" src="${user.userLogoPath}" alt="" />
                        </c:when>
                        <c:otherwise>
                            <img alt="" src="./resources/images/avtar.png">
                        </c:otherwise>
                    </c:choose>                              
                </div>
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        ${user.name}
                    </div>
                    <div class="logged-user-role">
                        <sec:authorize access="!hasAuthority('BENEFICIARY')">
                            ${user.role}
                        </sec:authorize>
                    </div>
                    <div class="logged-user-role coustmer-Id">
                        <sec:authorize access="hasAuthority('BENEFICIARY')">
                            ${user.investmentCode}
                        </sec:authorize>
                    </div>
                </div>
                <div class="logged-user-menu">
                    <div class="logged-user-avatar-info">
                        <div class="logged-user-info-w">
                            <div class="logged-user-name">
                                ${user.name}
                            </div>
                            <div class="logged-user-role">
                                <sec:authorize access="!hasAuthority('BENEFICIARY')">
                                    ${user.role}
                                </sec:authorize>
                            </div>
                            <div class="logged-user-role coustmer-Id">
                                <sec:authorize access="hasAuthority('BENEFICIARY')">
                                    ${user.investmentCode}
                                </sec:authorize>
                            </div>
                        </div>
                    </div>
                    <div class="bg-icon">
                        <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="javascript:void(0)" class="menu-profile"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                        </li>
                        <li>
                            <a href="./login?logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="main-menu">            
            <sec:authorize access="hasAuthority('ADMIN')">
                <li class="sub-menu">
                    <a href="./admin-dashboard" id="menu-dash">
                        <div class="icon-w">
                            <div class="fa fa-address-card-o"></div>
                        </div>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-beneficiaries-db" >
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>All Users Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-notes" class="menu-notes">
                        <div class="icon-w">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <span>Notes</span>
                    </a> 
                </li>
                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <i class="fa fa-user-o"></i>                           
                        </div>
                        <span>Verification Options</span>
                    </a>    
                    <a href="./home-pendingverification" class="drop_show">
                        <div class="icon-w">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <span>Pending Verification</span>
                    </a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('ADVISOR')">
                <li class="sub-menu">
                    <a href="./advisor-dashboard" id="menu-dash">
                        <div class="icon-w">
                            <div class="fa fa-address-card-o"></div>
                        </div>
                        <span>Advisor Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-beneficiaries-db" >
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>All Users Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./home-notes" class="menu-notes">
                        <div class="icon-w">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <span>Notes</span>
                    </a> 
                </li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('BENEFICIARY')">
                <li class="sub-menu">
                    <a href="./home" class="menu-dash">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>User Dashboard</span>
                    </a>
                </li>
            </sec:authorize>
            <%--  <sec:authorize access="hasAuthority('BENEFICIARY')">
                  <li class="sub-menu">
                      <a href="./home-beneficiary-new-dashboard" class="menu-dash-new">
                          <div class="icon-w">
                              <div class="os-icon os-icon-window-content"></div>
                          </div>
                          <span>New User Dashboard</span>
                      </a>
                  </li> 
              </sec:authorize>--%>
            <sec:authorize access="!hasAuthority('ADVISOR')">
                <li class="sub-menu">
                    <a href="./investment" id="menu-options">
                        <div class="icon-w">
                            <div class="os-icon os-icon-hierarchy-structure-2"></div>
                        </div>
                        <span>Investment Options</span>
                    </a>
                </li>
                <%-- <li class="sub-menu">
                    <a href="./home-pendingtransaction">
                        <div class="icon-w">
                            <div class="">
                                <i class="fa fa-line-chart"></i>
                            </div>
                        </div>
                        <span>Pending Options</span>
                    </a>
                </li>--%>
                <!--                <li class="sub-menu drop1">
                    <a href="javaScript:Void(0)">
                        <div class="icon-w">
                            <i class="fa fa-user-o"></i>                           
                        </div>
                        <span>Verification Options</span>
                    </a>    
                    <a href="./home-pendingverification" class="drop_show">
                        <div class="icon-w">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <span>Pending Verification</span>
                    </a>
                                </li>-->

                <li class="sub-menu">
                    <a href="javascript:void(0)" class="menu-txns">
                        <!--<a href="./beneficary-transactions" id="menu-txns">-->
                        <div class="icon-w">
                            <i class="fa fa-money" aria-hidden="true"></i>
                        </div>
                        <span>Cash &amp; Transactions</span>
                    </a> 
                </li>
                <%--    <li class="sub-menu">
                     <a href="javascript:void(0)" class="menu-txns-new">
                         <!--<a href="./beneficary-transactions" id="menu-txns">-->
                         <div class="icon-w">
                             <i class="fa fa-money" aria-hidden="true"></i>
                         </div>
                         <span>New Cash &amp; Transactions</span>
                     </a> 
                 </li>--%>
                <!--                <li class="sub-menu">
                                    <a href="javascript:void(0)" class="menu-payment">
                                        <a href="./beneficary-transactions" id="menu-txns">
                                        <div class="icon-w">
                                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                        </div>
                                        <span>Payment</span>
                                    </a> 
                                </li>-->
            </sec:authorize>

            <li class="sub-menu">
                <a href="./home-newsResearch" class="menu-news">
                    <div class="icon-w">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    </div>
                    <span>News &amp; Research</span>
                </a> 
            </li>
            <li class="sub-menu">
                <a href="./home-support" class="menu-support">
                    <div class="icon-w">
                        <i class="fa fa-support" aria-hidden="true"></i>
                    </div>
                    <span>Support</span>
                </a> 
            </li>
            <c:if test="${not empty allNotification}">
                <li class="sub-menu">
                    <a href="javascript:void(0)" class="notify-pop "  onmouseover="myaddtion()" onmouseout="myaddtion()">
                        <div class="icon-w">
                            <!--<i class="fa fa-support" aria-hidden="true"></i>-->
                            <i class="fa fa-bell"></i>
                        </div>
                        <span>Notification</span>
                        <span class="popuptext" id="add-Popup">
                            <c:forEach items="${allNotification}" var="notify">
                                <div class="pop-main child-clone" >
                                    <i class="fa fa-times delete-notify" data-id="${notify.id}" onclick="deleteNotify(this)" aria-hidden="true"></i>
                                    <div class="section-pop-1 section-pop-2 " id="parent-notify">
                                        <i class="fa fa-dot-circle-o"></i>
                                        <div class="notify-data" data-id="${notify.id}"  onclick="investment(this)">
                                            <div class="notify-type" data-id="${notify.type}" style="dispaly:none"> </div>
                                            <div class="notify-code" data-id="${notify.portfolioCode}" style="dispaly:none"> </div>
                                            <div class="notify-icode" data-id="${notify.investmentCode}" style="dispaly:none"> </div>
                                            <c:choose>
                                                <c:when test="${'INVESTMENT_PAYMENT' eq notify.type}">
                                                    <h6 class="notify">Pending Investment</h6>
                                                    <p class="notify-desc">You have a Pending Investment name ${notify.investmentName} of amount ${notify.investedAmount} in portfolio ${notify.name}</p>
                                                </c:when>
                                                <c:when test="${'TRANSACTION_PAYMENT' eq notify.type}">
                                                    <h6 class="notify">Pending Transaction</h6>
                                                    <p class="notify-desc">You have a Pending Transaction of amount ${notify.investedAmount} in portfolio ${notify.name}</p>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </span>
                    </a> 
                </li>
            </c:if>
            <sec:authorize access="hasAuthority('ADMIN')">
                <li class="sub-menu">
                    <a href="./home-verify" class="menu-support">
                        <div class="icon-w">
                            <i class="fa fa-support" aria-hidden="true"></i>
                        </div>
                        <span>Verify</span>
                    </a> 
                </li>
            </sec:authorize>
            <!--            <li class="sub-menu">
                            <a href="./home-document" class="menu-support">
                                <div class="icon-w">
                                    <i class="fa fa-support" aria-hidden="true"></i>
                                </div>
                                <span>Documents</span>
                            </a> 
                        </li>-->
        </ul>
        <form id="typeform" action="./notification" method="post">
            <input type="hidden" name="id"/>
            <input type="hidden" name="type"/>
            <input type="hidden" name="code"/>
            <input type="hidden" name="ic"/>
        </form>
        <!--        <form id="typeDelete" action="./delete-notification" method="post">
                    <input type="hidden" name="id"/>
                   
                </form>-->
    </div>

</div>
<link href="./resources/css/main1.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
<script>
                                            $(document).ready(function () {
//                                       
//                                        $.ajax({
//                                            type: 'GET',
//                                            url: './rest/groot/db/api/admin/notification?userId=' + userId,
//                                            headers: {"Content-Type": 'application/json'},
//                                            success: function (data, textStatus, jqXHR) {
//                                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                                                obj = obj.allNotification;
//                                                alert(obj.length);
//                                                var slide = document.getElementById("parent-notify").cloneNode(true);
//                                                for (var i = 0; obj.length > 0; i++) {
//                                                    $(".child-clone").append(slide);
//                                                }
//                                                console.log(data);
//                                            }, error: function (jqXHR, textStatus, errorThrown) {
//                                                alert(textStatus);
//                                            }
//                                        });
                                                $('.menu-dash').attr('href', './home');
                                                $('.menu-dash-new').attr('href', './home-beneficiary-new-dashboard-db?id=${id}');
                                                $('.menu-txns').attr('href', './transactions');
                                                $('.menu-txns-new').attr('href', './home-beneficary-new-transactions-db?id=${id}');
                                                $('.menu-profile').attr('href', './profile');
                                                $('.menu-payment').attr('href', './home-payment-enterpayment?id=${id}');

                                                $(".hamburger").click(function () {
                                                    $(this).toggleClass("change");
                                                    $("nav").toggle("1000");
                                                });
                                            });
</script>
<script>



    function investment(eve) {
        var id = $(eve).data("id");
        var type = $(eve).find(".notify-type").data("id");
        var pcode = $(eve).find(".notify-code").data("id");
        var ic = $(eve).find(".notify-icode").data("id");
        $('#typeform input[name=id]').val(id);
        $('#typeform input[name=type]').val(type);
        $('#typeform input[name=code]').val(pcode);
        $('#typeform input[name=ic]').val(ic);
        $('#typeform').submit();

    }
    function deleteNotify(eve) {
        var id = $(eve).data("id");
        var child = $(eve).closest(".child-clone");
//        var obj ={id: id};
        $.ajax({
            type: 'GET',
            url: './rest/groot/db/api/admin/delete-notification?id=' + id,
            headers: {"Content-Type": 'application/json'},
//            data: JSON.stringfy(obj),
            success: function (data, textStatus, jqXHR) {
                $(child).html("");
                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                console.log(data);
            }, error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }

    // When the user clicks on div, open the popup
    function myaddtion() {
        var popup = document.getElementById("add-Popup");
        popup.classList.toggle("show");
    }
    function myaddtion1() {
        var popup = document.getElementById("add-Popup1");
        popup.classList.toggle("show");
    }
</script>