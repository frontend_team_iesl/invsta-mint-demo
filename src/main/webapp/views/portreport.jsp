<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>




        <style>
            .report-charts {
                margin-bottom: 20px;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('./resources/images/Preloader_3.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
            }
            div#myModal {
    max-width: 100%;
}
.modal-header {
    display: flex;
}
        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="javascript:void(0)" id="home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Portfolio Report</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">Portfolio Holding Report</h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>                      
                                                            <th style="width:110px">
                                                                Date 
                                                            </th>
                                                            <th style="width:110px" class="text-right">
                                                                Total Users
                                                            </th>
                                                            <th class="text-right">
                                                                Total Invested Value
                                                            </th>
                                                            <th class="text-right">
                                                                 Portfolio Units
                                                            </th>
                                                            <th class="text-right">
                                                                Total Investment Value
                                                            </th>
                                                            <th class="text-right">
                                                                Net Asset Value
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>                      
                                                            <td class="date">2019-09-26</td>
                                                            <td class="user_count text-right">59</td>
                                                            <td class="invested_usd  text-right">14728.12</td>
                                                            <td class="total_units  text-right">15410.0617</td>
                                                            <td class="text-right">
                                                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:#3e7aba;">
                                                                    <span class="total_usd" style="display:inline-block; margin-right:10px">13285.660027255846</span><i class="os-icon os-icon-pencil-2" style="font-size:20px"></i>
                                                                </a>
                                                            </td>
                                                            <td class="pull-right">
                                                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:#3e7aba;">
                                                                    <span class="nav" style="display:inline-block; margin-right: 10px">0.86214191</span><i class="os-icon os-icon-pencil-2" style="font-size:20px;"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal fade" id="myModal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Portfolio</h4>
                                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <div class="modal-body">
                                                            <form method="post" action="./admin-update-fund-price">
                                                                <div class="row">
                                                                    <input type="hidden" name="fund_id" value="20">
                                                                    <div class="col-md-6">
                                                                        <label class="label">Date</label>
                                                                        <input type="date" class="form-control date" name="date">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Users</label>
                                                                        <input type="text" class="form-control text-right user_count" name="user_count" readonly="true">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Units</label>
                                                                        <input type="text" class="form-control text-right total_units" id="total_units" name="total_units" readonly="true">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Value</label>
                                                                        <input type="text" class="form-control text-right total_usd" id="total_usd" name="total_usd">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">N.A.V.</label>
                                                                        <input type="text" class="form-control text-right nav" id="nav" name="nav">
                                                                    </div>
                                                                    <div class="col-md-6 pull-right" style="margin-top: 26px">
                                                                        <input type="submit" class="form-control btn btn-primary btn-style" value="Submit">
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-chart-1"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-chart-2"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-chart-3"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-chart-4"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-pie-1"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="report-charts">
                                        <div id="report-pie-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!--    <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/all-funds',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $(".loader").hide();
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $.each(obj, function (idx, val) {
                            if (val.Code === "290002") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore1').attr('href', id1);
                            }
                            if (val.Code === "290004") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore2').attr('href', id1);
                            }
                            if (val.Code === "290006") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore3').attr('href', id1);
                            }
                            if (val.Code === "290012") {
                                var id1 = './portfolio-' + val.Code;
                                $('.showmemore4').attr('href', id1);
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
        </script>-->
    <script>
//        $('#viewmore1').click(function () {
//            $('.hidediv1').show();
//            $('#viewmore1').hide();
//        });
//        $('#viewmore2').click(function () {
//            $('.hidediv2').show();
//            $('#viewmore2').hide();
//        });
//        $('#viewmore3').click(function () {
//            $('.hidediv3').show();
//            $('#viewmore3').hide();
//        });
        $('.clickinput').on("click", function () {
            var recemt = $(this).closest('.funds-deatil');
            recemt.find(".offer-input").toggle();
        });
        $(document).ready(function () {
            $(".loader").fadeOut("slow");
            $('.hidediv1').hide();
            $('.hidediv2').hide();
            $('.hidediv3').hide();
        });
        $('.democlick').click(function () {
            var data = $(this).data("target");
            $('.showclick').hide();
            $(data).show();
        });
    </script>
    <script>
        Highcharts.chart('report-chart-1', {

            title: {
                text: 'Total Users'
            },
            subtitle: {
                text: 'Total Users'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            xAxis: {
                categories: ['2019-09-19', '2019-09-20', '2019-09-21', '2019-09-22', '2019-09-23', '2019-09-24', '2019-09-25', '2019-09-26']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: [{
                    name: 'Total Users',
                    data: [59, 59, 59, 59, 59, 59, 59, 59]
                }],

            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }

        });
    </script>
    <script>
        Highcharts.chart('report-chart-2', {

            title: {
                text: 'Total Units'
            },
            subtitle: {
                text: 'Total Units'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            xAxis: {
                categories: ['2019-09-19', '2019-09-20', '2019-09-21', '2019-09-22', '2019-09-23', '2019-09-24', '2019-09-25', '2019-09-26']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: [{
                    name: 'Total Units',
                    data: [15410.0617, 15410.0617, 15410.0617, 15410.0617, 15410.0617, 15410.0617, 15410.0617, 15410.0617]
                }],

            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }

        });
    </script>
    <script>
        Highcharts.chart('report-chart-3', {

            title: {
                text: 'Net Asset Value'
            },
            subtitle: {
                text: 'Net Asset Value'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            xAxis: {
                categories: ['2019-09-19', '2019-09-20', '2019-09-21', '2019-09-22', '2019-09-23', '2019-09-24', '2019-09-25', '2019-09-26']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: [{
                    name: 'Unit NAV value Daily',
                    data: [0.86214191, 0.86214191, 0.86214191, 0.86214191, 0.86214191, 0.86214191, 0.86214191, 0.86214191]
                }],

            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }

        });
    </script>
    <script>
        Highcharts.chart('report-chart-4', {

            title: {
                text: 'Portfolio Investment Value'
            },
            subtitle: {
                text: 'Portfolio Investment Value'
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            xAxis: {
                categories: ['2019-09-19', '2019-09-20', '2019-09-21', '2019-09-22', '2019-09-23', '2019-09-24', '2019-09-25', '2019-09-26']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: [{
                    name: 'Daily NAV * Units',
                    data: [13285.660027255846, 13285.660027255846, 13285.660027255846, 13285.660027255846, 13285.660027255846, 13285.660027255846, 13285.660027255846, 13285.660027255846]
                }],

            responsive: {
                rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
            }

        });
    </script>
    <script>
        Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
        Highcharts.chart('report-pie-1', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Portfolio Holding By User',
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 150
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },

            series: [{
                    type: 'pie',
                    name: '% age',
                    innerSize: '75%',
                    data: [
                        ['Abhy', 3],
                        ['Alisha', 5],
                        ['Rachel Strevens', 15],
                        ['Udhav', 16],
                        ['Jon', 10],
                        ['Vic', 5],
                    ]
                }]
        });

    </script>
    <script>
        Highcharts.chart('report-pie-2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Total fund holding breakdown',
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 150
            },
            credits: {
                enabled: false,
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>value:{series.value}'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },

            series: [{
                    type: 'pie',
                    name: '% age',
                    innerSize: '75%',
                    data: [
                        ['Slice', 96.20],
                        ['Fund', 3.80],
                    ]
                }]
        });

    </script>
</body>
</html>