<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./resources/favicon.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./resources/css/signup/style.css">
        <link rel="stylesheet" href="./resources/css/signup/custom.css">
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css'>
        <link href="./resources/css/sweetalert.css" rel="stylesheet" />
        <style>
            img.logo {
                width: 30%;
            }
            .intl-tel-input .flag-dropdown .selected-flag .down-arrow {
                top: 6px;
                position: relative;
                left: 20px;
                width: 0px;
                height: 0;
                border-left: 3px solid transparent;
                border-right: 3px solid transparent;
                border-top: 6px solid black;
            }
            input.verify-dl {
                background: ba;
                background: #65b9ac!important;
                z-index: 999999999;
                color: #fff!important;
                max-width: 103px;
                height: 26px;
                padding: 2px!important;
                font-size: 11px!important;
                cursor: pointer;
            }
            @media(max-width:575px){
                .this-space {
                    margin: 10px 0!important;
                }
            }
            div#detectcompany {
                position: absolute;
                background: white;
                z-index: 999;
                height: 163px;
                overflow: auto;
                border: 1px solid #ccc;
                padding: 4px 5px;
                border-top: 0;
                display: none;
            }

            .checknear {
                text-align: left;
                font-size: 12px;
                border-bottom: 1px solid #ccc;
                padding-bottom: 6px;
            }

            input#companyName {
                margin: 0;
            }
            .autocomplete {
                position: relative;
                display: inline-block;
            }

            .autocomplete-items {
                position: absolute;
                border: 1px solid #d4d4d4;
                border-bottom: none;
                border-top: none;
                z-index: 99;
                /*position the autocomplete items to be the same width as the container:*/
                top: 100%;
                left: 0;
                right: 0;
            }

            .autocomplete-items div {
                padding: 10px;
                cursor: pointer;
                background-color: #fff; 
                border-bottom: 1px solid #d4d4d4; 
            }

            /*when hovering an item:*/
            .autocomplete-items div:hover {
                background-color: #e9e9e9; 
            }

            /*when navigating through the items using the arrow keys:*/
            .autocomplete-active {
                background-color: DodgerBlue !important; 
                color: #ffffff; 
            }

            .mm-dropdown.signup-flag .error {
                position: absolute;
                top: 33px;
                z-index: -1;
            }
            .signup-btn-west a {
                color: #fff;
                font-size: 14px;
            }
            .signup-btn-west p {
                background: linear-gradient(to right, #2c94ec, #304bb7);
                color: #fff;
                padding: 7px 10px;
                border-radius: 10px;
                overflow: hidden;
                font-size: 14px;
                border: 1px solid #2f5fc5;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="loged_user signup-btn-west">
                <a href="./login?logout"><p>Already have an Account Login</p></a>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="content_body">
                        <form id="msform" name="msRegform" class="form-signup" action="/chelmer" method="POST" autocomplete="off">
                            <div class="w3-light-grey">
                                <!--<div id="myBar" class="progress-bar" style="width:0%"></div>-->
                            </div>
                            <div class="logo-w">
                                <a href="#"><img class="logo" alt="" src="./resources/images/mint.png" ></a>
                            </div>
                            <div class="save-new-btn">
                                <input type="hidden" name="register_type" id="register_type" value="TRUST_ACCOUNT">
                                <input type="hidden" name="step" id="step" value="1">
                                <button id="" class=" saveExit director-btn all-btn-color save-font">Save & Exit</button>
                                <!--<span class="info-icon"><a href="#" data-toggle="tooltip" title="You can save and exit this application at any time by clicking on this Save & Exit button. We will then send you an email from which you can resume your application when you are ready. "><img src="./resources/images/i-icon.png"></a></span>-->
                                <div class="pulsating-circle4 pulsating-around6" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                    <span class="tooltiptext">You can save and exit this application at any time by clicking on this Save &amp; Exit button. We will then send you an email from which you can resume your application when you are ready.</span>
                                </div>
                            </div>
                            <fieldset id="step1">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide the below Trust details.  
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Name of Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field " id="companyName" value="${company.companyName}" name="companyName"  placeholder="Enter Trust name" onkeyup="myFunction()" />
                                                <div id="detectcompany"></div>
                                                <span class="error" id="spanCompanyName"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Trust address 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <input type="text" class="form-control input-field " value="${company.companyAddress}" placeholder="Trust address" name="companyAddress" id="companyAddress" autocomplete="off">
                                                <span class="error" id="spanCompanyAddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of registration
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control countryname" id="Country_of_incorporation"   value="${company.countryofincorporation}" name="countryCode"  placeholder="Enter Country Code" readonly="readonly">
                                                <span class="error" id="spanCountryCode"></span>
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of establishment
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" id="Date_of_incorporation" placeholder="dd/mm/yyyy" class="input-field doc" value="${company.dateOfInco}"  data-lang="en"/>
                                                <span class="error" id="spanCompanyDate"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Type of Trust
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group selectType typeOfTrust">
                                                    <option value="1">-Select-</option>
                                                    <option <c:if test="${company.typeOfTrust eq 'Family'}">Selected </c:if> value="2">Family</option>
                                                    <option <c:if test="${company.typeOfTrust eq 'Charitable'}">Selected </c:if>value="3">Charitable</option>
                                                    <option <c:if test="${company.typeOfTrust eq 'Other'}">Selected </c:if>value="4">Other</option>
                                                    </select>
                                                    <span class="error" id="spanTypeTrust"></span>
                                                    <!--                                            </div>
                                                                                                <div class="col-sm-6 inputtype1">
                                                                                                    <label class="label_input">
                                                                                                        Enter type
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="col-sm-6 ">-->
                                                    <input type="text" class="form-control input-field otherType" id="" name="trustType" placeholder="Enter type"/>
                                                    <span class="error" id="spanTypeTrustother"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Trust registration number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group country-set flag-drop">
                                                    <input type="text" class="form-control input-field" id="registrationNumber" name="registrationNumber" value="${company.registerNumber}"  placeholder="Enter registration number" onkeyup="myFunction()" />
                                                <span class="error" id="spanRegistrationNumber"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you working with an Investment Adviser?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group avisor-click" id="investmentAdviser" >
                                                    <c:if test = "${company.investAdviser == ''}">
                                                        <option value="">${company.investAdviser}</option>
                                                    </c:if>
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>
                                            <div class="row advisor-show">
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Select Advisery Company
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption advisor_company form-group" id="companyAdvisor" >
                                                        <c:if test = "${company.companyAdvisor  eq ''}">
                                                            <option value="">${company.companyAdvisor}</option>
                                                        </c:if>
                                                        <c:forEach items="${advisories}" var="advisor">
                                                            <option value="${advisor.login_id}">${advisor.companyName}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Select an Adviser 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption advisor form-group" id="selectAnAdviser" >
                                                        <c:if test = "${company.selectAdviser eq ''}">
                                                            <option value="">${company.selectAdviser}</option>
                                                        </c:if>
                                                        <c:forEach items="${advisories}" var="advisor">
                                                            <option value="${advisor.login_id}">${advisor.advisorName}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Please upload a copy of trust deed
                                                </label>
                                            </div>
                                            <div class="col-sm-6 closestcls">
                                                <input type="file" name="myDeedFile" accept="image/x-png,image/jpeg,image/*,.txt,.pdf" onchange="documentFileUpload(this);" class="attach-btn2 checkname" id="attachdeedfile" >
                                                <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                <span class="error error_attachdeedfile"></span>
                                            </div>
                                        </div>


                                    </div> 
                                </div>
                                <div class="footer_form">

                                </div>
                                <a href="#" name="previous" class="previous action-button-previous privous-signup" value="Previous" >Previous</a>
                                <input type="button" name="next" class="next1 action-button"  value="Continue"/>
                            </fieldset>
                            <fieldset id="step2">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Trustees and beneficial owners.
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <small class="verification-otp1">
                                                Please enter the details for all trustees, beneficial owners, and other people with effective control.
                                            </small>
                                            <div class="col-sm-12">
                                                <input type="hidden" class="input-field counter" id="counter" >
                                                <input type="hidden" id="directors-index"/>
                                                <input type="hidden" id="directors-value"/>
                                            </div>
                                        </div>
                                        <div class="row director-section">

                                            <div class="row director-add">
                                                <div class="col-sm-4">
                                                    <label class="label_input" style="text-align:left">
                                                        Full name of trustee/beneficial owner
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 ">
                                                    <select class="selectoption option-add adjust-option mainDirectorTitle" id="shareholder">
                                                        <option value="Mr">Mr</option>
                                                        <option value="Mrs">Mrs</option>
                                                        <option value="Miss">Miss</option>
                                                        <option id="other" value="Other">Master</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="fname input-field tags" id="fname" name="fullName" value="${company.fname}" placeholder="Enter full name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
                                                    <span class="error spanfname" id="spanfname"></span>
                                                    <input type='hidden' class='cls-me' name='me' value='Y'>
                                                </div>
                                                <div class="col-sm-2 this-space">
                                                    <a href="javascript:void(0)" class="this-btn this-is-me check-this-btn"  onclick='addbtnfn(this);'>This is me</a>
                                                </div>
                                            </div> 
                                            <c:choose>
                                                <c:when test="${not empty company.moreInvestorList}">
                                                    <c:forEach items="${company.moreInvestorList}" var="moreInvestor" varStatus="loop">
                                                        <div class="row director-add">
                                                            <div class="col-sm-4">
                                                                <label class="label_input" style="text-align:left">
                                                                    Full name of trustee/beneficial owner
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 ">
                                                                <select class="selectoption mainDirectorTitle directorTitle " id="shareholder">
                                                                    <option value="Mr">Mr</option>
                                                                    <option value="Mrs">Mrs</option>
                                                                    <option value="Miss">Miss</option>
                                                                    <option id="other" value="Other">Master</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="fname input-field" id="fname" name="fullName" value="${moreInvestor.fname}" placeholder="Enter full name" >
                                                                <span class="error spanfname" id="spanfname"></span>
                                                                <input type='hidden' class='cls-me' name='me' value='N'>
                                                            </div>
                                                            <div class="col-sm-2 this-space">
                                                                <a href="javascript:void(0)" class="this-btn this-is-me "  onclick='addbtnfn(this);'>This is me</a>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </c:when>


                                            </c:choose>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button id="btn20" class="director-btn all-btn-color">Add another trustee/beneficial owner</button>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="focus-border" style="display:block;" id="validationId" ></span>
                                </div>

                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous1 action-button-previous" value="Previous" />
                                <input type="button" name="next"  class=" next2 action-button"  value="Continue"/>
                            </fieldset>

                            <fieldset id="step3">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please provide the following information for  <span class="director-name this-name"></span>
                                        </h5>
                                    </div>

                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Preferred first name (if applicable) 
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob"  id="preffered_name" value="" placeholder="Enter preferred name (optional)" class="input-field continer3Dob" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Date of birth
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="dob" class="dob" id="Date_of_Birth" value="${company.dateOfBirth}" placeholder="dd/mm/yyyy" class="input-field continer3Dob" data-format="DD/MM/YY" data-lang="en" />
                                                <span class="error" id="spanDate_of_Birth"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Occupation
                                                </label>
                                            </div>
                                            <div class="col-sm-6" >
                                                <select name="occupation" class="Occupation selectoption selectOcc form-group OccupationOption">
                                                    <option value="-Select-">-Select-</option>
                                                    <c:forEach items="${occupation}" var="occ">
                                                        <option value="${occ.mmc_occu_id}" 
                                                                <c:if test="${occ.mmc_occu_name eq company.occupation}">
                                                                    selected                                                                     
                                                                </c:if> >${occ.mmc_occu_name}</option>
                                                    </c:forEach>
                                                    <option value="0">Other</option>
                                                </select>
                                                <input type="text" name="Occupation" id="cOccupation" placeholder="Enter occupation " value="${company.occupation}" class="Occupation otherOcc input-field" onkeyup="myFunction()"/>
                                                <span class="error" id="spanOccupation"></span>
                                            </div>
                                            <!--                                            <div class="col-sm-6">
                                                                                            <input type="text" class="form-control input-field" id="cOccupation" value="${company.occupation}" name="Occupation" placeholder="Enter occupation" onkeyup="myFunction()" />
                                                                                            <span class="error" id="spanOccupation"></span>
                                                                                        </div>-->
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Country of residence 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 form-group country-set flag-drop">
                                                <input type="text" class="form-control countryname"  id="holderCountryOfResidence" value="${company.holderCountryOfResidence}" name="holderCountryOfResidence"  placeholder="Enter Country Code" readonly="readonly">                                                                                                                                                                                                   
                                                <span class="error" id="spanHolderCountryOfResidence"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Position in Trust 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 trust-signup">
                                                <select class="selectoption form-group" id="positionInCompany">
                                                    <option value="-Select-">-Select-</option>
                                                    <option value="Independent trustee" <c:if test = "${company.positionInCompany eq 'Independent trustee'}">selected</c:if>>Independent trustee</option>
                                                    <option value="Interview" <c:if test = "${company.positionInCompany eq 'Interview'}">selected</c:if>>Beneficial owner</option>
                                                    <option value="Effective" <c:if test = "${company.positionInCompany eq 'Effective'}">selected</c:if>> Effective control</option>
                                                    </select>
                                                    <span class="error" id="error-positionInCompany"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>
                                    <input type="button" name="previous" class="previous2 action-button-previous" value="Previous" />
                                    <input type="button" name="next" class="next3 action-button" value="Continue" />
                                </fieldset>
                                <fieldset id="step4">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Please enter contact details for  <span class="director-name this-name"></span>
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <!-- <div class="col-sm-12">
                                                        <p><span class="director-name this-name"></span></p>
                                                </div> -->
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Home address
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" id="address" value="${company.address}" class="form-control input-field"  placeholder="Enter home address" />
                                                <span class="error" id="spanHomeaddress"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    Mobile number (required)
                                                </label>
                                                <div class="pulsating-circle4 pulsating-around6 circler" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                    <span class="tooltiptext minor-tooltip-2 ">Mobile number is mandatory while  the other number is optional.</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mobile_number first-mobile top-show">
                                                <input type="text" class="form-control codenumber mobile_country_code" value="${company.countryCode3}" id="countryCode3" name="countryCode" placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename mobile_number1" id="mobileNo" name="mobileNo" value="${company.mobileNo}" placeholder="Enter mobile number" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error" id="spanOPmobileNo"></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="label_input" style="text-align:left">
                                                    Other number (optional) 
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="selectoption form-group" id="otherNumber" >
                                                    <option value="Home">Home</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div> 
                                            <div class="col-sm-6 mobile_number first-mobile mobile-index1">
                                                <input type="text" class="form-control codenumber"  id="countryCode" value="${company.otherCountryCode}" name="countryCode"  placeholder="Enter Country Code" readonly="readonly">
                                                <input type="tel" class="form-control error codename" id="mobileNo2" value="${company.otherMobileNo}" name="otherNumber"  placeholder="Enter number " onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                <span class="error" id=""></span>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                    <label class="label_input">
                                                    Work phone number (optional) 
                                                    </label>
                                                    </div>
                                                    <div class="col-sm-6 mobile_number first-mobile mobile-index2">
                                                    <input type="text" class="form-control codenumber"  id="countryCode2" name="countryCode" required="required" placeholder="Enter Country Code" readonly="readonly">
                                                    <input type="tel" class="form-control error codename" name="mobileNo" required="required" placeholder="Enter work number (optional)">
                                                    
                                            </div> -->

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous3 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next4 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step5">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Account security: mobile verification for  <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <small class="verification-otp1">
                                                        To verify your mobile number, please choose to receive a unique verification code via text message or phone call. 
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 otp-icon">
                                                    <ul class="radio_button">
                                                        <li>
                                                            <input type="radio" class="senderType" id="f-option"  name="senderType" value="sms" style="width: auto;">
                                                            <img src="./resources/images/icon/message-icon.png" style="width: 20px; margin-left: 5px;">
                                                            <label for="f-option"></label>
                                                            <div class="check"></div>
                                                        </li>
                                                        <li>
                                                            <input type="radio" class="senderType" id="s-option" name="senderType" value="sms" style="width: auto;">
                                                            <i class="fas fa-phone"></i>
                                                            <label for="s-option"></label>
                                                            <div class="check"><div class="inside"></div></div>
                                                        </li>
                                                    </ul>
                                                    <span class="error" id="error-generateOtp"></span>
                                                </div>
                                                <!--                                                <div class="col-sm-6">
                                                        <input type="button" name="next" id="generate-otp" class="action-button" value="Generate OTP" />
                                                </div>-->                                              


                                            </div>
                                            <div class="row otp-area">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Verification code 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" id="verificationa" name="verification" placeholder="Enter unique code  " class="input-field otpkey" maxlength="6"/>
                                                    <span class=" spanverification" id="spanverificationa" ></span>
                                                </div>
                                            </div>
                                            <!-- <div class="face" id="error-loader" style="display:none" >
                                                    <div class="hand" ></div>
                                            </div> -->
                                            <small class="verification-otp"> If you have not received your unique verification code within one minute, you can request another by  <a href="#">clicking here.</a>.</small>
                                        </div>
                                    </div>



                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->
                                </div>
                                <input type="button" name="previous" class="previous4 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next5 action-button otpNext" value="Continue" />
                            </fieldset>
                            <fieldset id="step6">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the identification details for <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <input type="hidden" class="which_container abc" value="#Date_of_Birth"/>
                                    <div class="input-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-12">
                                                    <p><span class="director-name this-name"></span></p>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <label class="label_input detail-2" style="text-align:left">
                                                    Which type of ID are you providing
                                                </label>
                                            </div>
                                            <div class="col-sm-6 selDiv">
                                                <select class="selectoption form-group option Id_Type" id="src_of_fund1">
                                                    <option value="1">NZ Driver Licence</option>
                                                    <option value="2">NZ Passport</option>
                                                    <option value="3">Other </option>

                                                </select>
                                            </div>
                                            <div class="row drivery-licence">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="licence_first_name"  value="${company.firstName}" placeholder="Enter first name" id="licence_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="licence_middle_name" value="${company.middleName}"id="licence_middle_name" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="licence_last_name" placeholder="Enter last name" value="${company.lastName}" id="licence_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_licence_last_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Licence number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field" id="licenseNumber" value="${company.licenseNumber}" name="license_number"  placeholder="Enter licence number " onkeyup="myFunction()" />
                                                    <span class="error" id="spanlicenseNumber" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="Expirydate" placeholder="dd/mm/yyyy" value="${company.licenseExpiryDate}" class="input-field dob1 lic_expiry_Date" data-format="DD/MM/YY" data-lang="en" />                                 
                                                    <span class="error" id="spanExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Version number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="college" id="versionNumber" value="${company.versionNumber}" placeholder="Enter version number" class="input-field lic_verson_number" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                                    <span class="error" id="spanVersionnumber"></span>
                                                </div>
                                            </div>
                                            <div class="row passport-select">

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" name="passport_first_name" placeholder="Enter first name"  value="${company.firstName}" id="passport_first_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name"  id="passport_middle_name"name="passport_middle_name" value="${company.middleName}" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" name="passport_last_name" placeholder="Enter last name" value="${company.lastName}" id="passport_last_name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_passport_last_name"></span>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Passport number
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field" id="Passportnumber" name="passport_number" value="${company.passportNumber}"  placeholder="Enter passport number" onkeyup="myFunction()" />
                                                    <span class="error" id="spanPassportnumber" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="PExpirydate" placeholder="dd/mm/yyyy" value="${company.passportExpiryDate}" class="input-field dob1  pass_expiry" data-format="DD/MM/YY" data-lang="en" />                                 
                                                    <span class="error" id="spanPExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="Countryofissue" name="countryCode"  value="${company.countryOfIssue}" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="spanCountryofissue"></span>
                                                </div>
                                            </div>
                                            <div class="row other-select">
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Type of ID   
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-field TypeofID"  value="${company.typeOfID}" id="TypeofID" name="fullName"  placeholder="Enter ID type" onkeyup="myFunction()" />
                                                    <span class="error" id="spanTypeofID" ></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        First name
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="first_name" id="other_first_name" value="${company.firstName}" name="other_first_name" placeholder="Enter first name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_first_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Middle name
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="middle_name" name="other_middle_name"id="other_middle_name" value="${company.middleName}" placeholder="Enter middle name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_middle_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Last name  
                                                    </label>
                                                </div>                                                    
                                                <div class="col-sm-6">
                                                    <input type="text" class="last_name" id="other_last_name" name="other_last_name" value="${company.lastName}"  placeholder="Enter last name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'/>
                                                    <span class="error" id="error_other_last_name"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Expiry date 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <input type="text" name="dob" id="typeExpirydate" placeholder="dd/mm/yyyy" class="input-field dob1 otherIdExp" value="${company.typeExpiryDate}" data-format="dd/mm/yyyy" data-lang="en" />                                 
                                                    <span class="error" id="spantypeExpirydate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Country of issue  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 flag-drop">
                                                    <input type="text" class="form-control countryname"  id="countryname1" name="countryCode"  value="${company.otherIdIssuedBy}" placeholder="Enter Country Code" readonly="readonly">    
                                                    <span class="error" id="span_countryname1"></span>
                                                </div>
                                                <div class="col-sm-129 closestcls">
                                                    <input type="file" name="myFile" id="otherDocument" accept="image/x-png,image/jpeg,image/*,.txt,.pdf" onchange="documentFileUpload(this);" class="attach-btn checkname" >
                                                    <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                    <span class="error" id="span-otherDocument"></span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="investor_verify" id="investor_verify" value="false">
                                            <input type="hidden" name="" id="mainDirectorPep_verify" value="false">

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous5 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next6 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step7">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the tax details for <span class="director-name this-name"></span>
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">


                                            <div class="col-sm-6">
                                                <label class="label_input">
                                                    IRD Number 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 validate-ird">
                                                <input type="text" class="form-control input-field" id="irdNumber" value="${company.irdNumber}" name="IRDNumber"  placeholder="XXX-XXX-XXX" onkeypress="checkird(this, event)"  onchange="checkird(this, event)"/>
                                                <a class="ird-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                <span class="error" id="spanIRDNumber"></span>
                                            </div> 
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Are you a US citizen or US tax resident, or a tax resident in any other country?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption selectoption1 form-group" id="USCitizen">
                                                    <option value="1">No</option>
                                                    <option value="2">Yes</option>
                                                </select>
                                            </div>

                                            <div class="row yes-option">
                                                <div class="row yes-new checktindata">
                                                    <div class="col-sm-12">
                                                        <h5 class="element-header aml-text">
                                                            Please enter all of the countries (excluding NZ) of which you are a tax resident.
                                                        </h5>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input" style="text-align:left">
                                                            Country of tax residence:
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 details-pos flag-drop">
                                                        <input type="text" class="form-control countrynameexcludenz tex_residence_Country"  id="countryOfTaxResidence" value="${company.companyReasonTIN}" name="countryCode"  placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN) 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field TIN" id="taxIdenityNumber" value="${company.taxIdentityNumber}" name="taxIdenityNumber"  placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error tin-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available  
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field removedata resn_unavailable" name="reasonTIN" value="${company.countryOfResidence}"  placeholder=""/>
                                                        <span class="error resn_unavailable-error" id="reason_tin_error"></span>
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another">Add another country </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous6 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next7 action-button" value="Continue" />
                            </fieldset>
                            <jsp:include page="./more-trustee-view.jsp"></jsp:include>
                                <fieldset id="step12">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Please enter the below tax information for the Trust.
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        What is the PIR 
                                                    </label>
                                                </div>
                                                <div class="col-sm-3 ">
                                                    <select class="selectoption form-group selectoption3" id="pir">
                                                        <option value="1">28%</option>
                                                        <option value="2">17.5%</option>
                                                        <option value="3">10.5%</option>
                                                        <!--                                                        <option value="4">0%</option>-->
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a href="https://www.ird.govt.nz/roles/portfolio-investment-entities/using-prescribed-investor-rates" id="pir" target="_blank" class="aml-link">What’s my PIR?</a>
                                                    <span class="error" id="error_Investors_Rate"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input">
                                                        Trust IRD number 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 validate-ird">
                                                    <input type="text" class="form-control input-field" id="comIRDNumber" value="${company.companyIRDNumber}" name="companyIRDNumber"  placeholder="XXX-XXX-XXX" onkeypress="checkird(this, event)" onchange="checkird(this, event)"/>
                                                <a class="ird-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                <span class="error" id="spancomIRDNumber"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    What is the source of funds or wealth for this account?
                                                </label>
                                            </div>
                                            <div class="col-sm-6 ">
                                                <select class="selectoption form-group selectoption3" id="sourceOfFunds">
                                                    <option value="1">-Select-</option>
                                                    <option value="2" <c:if test = "${company.sourceOfFunds eq '2'}">selected</c:if>>Business earnings</option>
                                                    <option value="3" <c:if test = "${company.sourceOfFunds eq '3'}">selected</c:if>>Personal employment</option>
                                                    <option value="4" <c:if test = "${company.sourceOfFunds eq '4'}">selected</c:if>>Financial investments</option>
                                                    <option value="5" <c:if test = "${company.sourceOfFunds eq '5'}">selected</c:if>>Inheritance</option>
                                                    <option value="6" <c:if test = "${company.sourceOfFunds eq '6'}">selected</c:if>>Gift</option>
                                                    <option value="7" <c:if test = "${company.sourceOfFunds eq '7'}">selected</c:if>>Other</option>
                                                    </select>
                                                    <span class="error" id="sourceOfFunds-error"></span>
                                                </div>
                                                <div class="col-sm-6 des-togle">
                                                    <label class="label_input" style="text-align:left">
                                                        Description 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 des-togle">
                                                    <input type="text" class="" id="sourceOfFunds_other" name="other_description" placeholder="Enter description"/>
                                                    <span class="error" id="error_sourceOfFunds_other"></span>
                                                </div>
                                                <div class="col-sm-6 inputtype">
                                                    <label class="label_input">
                                                        Enter Detail
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 inputtype">
                                                    <input type="text" class="form-control input-field" id="detail" name="detail"  placeholder="Enter detail" onkeyup="myFunction()" />

                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Is the Trust a listed issuer?  
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption form-group" id="isCompanyListed">
                                                        <option value="1" <c:if test = "${company.isCompanyListed eq '1'}">selected</c:if>>No</option>
                                                    <option value="2" <c:if test = "${company.isCompanyListed eq '2'}">selected</c:if>>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input" style="text-align:left">
                                                        Is the Trust a Government Department named in Schedule 1 of the State Sector Act 1998, or a local authority as defined in Section 5(2) of the Local Government Act 2002?
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption form-group" id="isCompanyGovernment">
                                                        <option value="1" <c:if test = "${company.isCompanyGovernment eq '1'}">selected</c:if>>No</option>
                                                    <option value="2" <c:if test = "${company.isCompanyGovernment eq '2'}">selected</c:if>>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input police-text" style="text-align:left">
                                                        Is the Trust the New Zealand Police? 
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption form-group police-text" id="isCompanyNZPolice">
                                                        <option value="1" <c:if test = "${company.isCompanyNZPolice eq '1'}">selected</c:if>>No</option>
                                                    <option value="2" <c:if test = "${company.isCompanyNZPolice eq '2'}">selected</c:if>>Yes</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer_form">
                                        <!--<img src="./resources/images/red.png">-->	
                                    </div>
                                    <input type="button" name="previous" class="previous11 action-button-previous" value="Previous" />
                                    <input type="button" name="next" class="next12 action-button" value="Continue" />
                                </fieldset>
                                <fieldset id="step13">
                                    <div class="content-section">
                                        <div class="element-wrapper">
                                            <h5 class="element-header">
                                                Please enter the following CRS & FATCA information for the Trust: 
                                                <div class="pulsating-circle4 pulsating-around4 btn-chng-1 trust-info" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                    <span class="tooltiptext tool-ad-2">New Zealand has implemented rules which require financial institutions, including Invsta, to collect certain information abouttheir clients’ foreign tax residency. For further information about the Foreign Account Tax Compliance Act (FATCA) or theCommon Reporting Standard (CRS) you can visit the Inland Revenue website at www.ird.govt.nz/international/exchange orspeak to a tax adviser. For further information about international tax residency rules you can visit the OECD website atwww.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-residency. An ‘entity’ includes a company,trust, partnership, association, registered co-operative, or government body.</span></div>
                                            </h5>
                                        </div>
                                        <div class="input-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="label_input " style="text-align:left">
                                                        Is the Trust a Financial Institution for FATCA or CRS purposes?
                                                        <div class="pulsating-circle4  pulsating-around4 btn-chng-2 trust-info-2 " data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                            <span class="tooltiptext tool-ad">The term Financial Institution as defined by FATCA and CRS includes custodial institutions, depository institutions,investment entities or specified insurance companies. A family trust is likely to be a Financial Institution if 50% or more of the trust’s income is from financial assets or if the trust is managed by another financial institution.</span>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption selectoption6 form-group" id="isCompanyFinancialInstitution">
                                                        <option value="0">-Select-</option>
                                                        <option value="1" <c:if test = "${company.isCompanyFinancialInstitution eq 'Yes'}">selected</c:if>>Yes</option>
                                                    <option value="2" <c:if test = "${company.isCompanyFinancialInstitution eq 'No'}">selected</c:if>>No</option>
                                                    </select>
                                                    <span class="error" id="isCompanyFinancialInstitution-error"></span>
                                                </div>
                                                <div class="col-sm-6 selectno6">
                                                    <label class="label_input police-text" style="text-align:left">
                                                        Is the Trust an Active or Passive Non-Financial Entity for FATCA and CRS purposes?
                                                        <div class="pulsating-circle4 pulsating-around5 btn-chng-3" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                            <span class="tooltiptext">An Entity will be an Active NFE where less than 50% of the NFE’s income for the preceding calendar year or other appropriate reporting period is passive income and less than 50% of the NFE’s assets for the preceding calendar year or other appropriate reporting period produce, or are held for the production of, passive income. Passive income generally include non-trading investment income in the form of: interest, dividends, annuities, other financial arrangements income, rents and royalties.The Entity may also be characterized as an Active NFE under other criteria.Generally, an Entity will be a Passive NFE if it is not an active NFE.</span>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 selectno6">
                                                    <select class="selectoption form-group police-text" id="isCompanyActivePassive">
                                                        <option value="0">-Select-</option>
                                                        <option value="2">Active</option>
                                                        <option value="2">Passive</option>
                                                    </select>
                                                    <span class="error" id="isCompanyActivePassive-error"></span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="label_input police-text" style="text-align:left">
                                                        Is the Trust a US citizen or US tax resident, or a tax resident in any other country?
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 ">
                                                    <select class="selectoption selectoption4 form-group police-text" id="isCompanyUSCitizen">
                                                        <option value="0">-Select-</option>
                                                        <option value="1" <c:if test = "${company.isCompanyUSCitizen eq 'No'}">selected</c:if>>No</option>
                                                    <option value="2" <c:if test = "${company.isCompanyUSCitizen eq 'Yes'}">selected</c:if>>Yes</option>
                                                    </select>
                                                    <span class="error" id="isCompanyUSCitizen-error"></span>
                                                </div>

                                                <div class="row yes-option4">
                                                    <div class="row yes-new4 checktin4data">
                                                        <div class="col-sm-12">
                                                            <h5 class="element-header aml-text">
                                                                Please enter all countries (excluding NZ) of which the Trust is a tax resident.
                                                            </h5>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label class="label_input" style="text-align:left">
                                                                Country of tax residence
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6 details-pos flag-drop">
                                                            <input type="text" class="form-control countrynameexcludenz"  id="companyCountryOfTaxResidence" name="countryOfTaxResidence"  value="${company.companyCountryOfTaxResidence}" placeholder="Enter Country Code" readonly="readonly">    
                                                        <span class="error countrynameexcludenz-error" id="countrynameexcludenz-error1"></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Tax Identification Number (TIN)
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field companyTaxIdentityNumber" id="companyTaxIdentityNumber" name="companyTaxIdentityNumber"  value="${company.companyTaxIdentityNumber}"  placeholder="Enter TIN " onkeyup="myFunction()" />
                                                        <span class="error companyTaxIdentityNumber-error" ></span>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="label_input">
                                                            Reason if TIN not available 
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-field companyReasonTIN" id="companyReasonTIN" value="${company.companyReasonTIN}" name="companyReasonTIN"  placeholder=""/>
                                                        <span class="error companyReasonTIN-error" ></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 add-another">
                                                    <a class="add-another2 all-btn-color" href="javascript:void(0)" id="add-country-another4">Add another country</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous12 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next13 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step14">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please enter the Trust bank account details (for distributions and/or redemptions).
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="row">
                                            <div class="col-sm-6 mb-2">
                                                <label class="label_input">
                                                    Bank name 
                                                </label>
                                            </div>
                                            <div class="col-sm-6 bank-margin1 mb-2">
                                                <div class="mm-dropdown signup-flag">
                                                    <c:choose>
                                                        <c:when test="${not empty company.bankName}">
                                                            <div class="textfirst">${company.bankName}<img class="drop-img" style="width:10px" src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" /></div>
                                                            </c:when >
                                                            <c:otherwise>
                                                            <div class="textfirst">-Select-<img class="drop-img" style="width:10px" src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" /></div>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    <ul class="select">
                                                        <c:forEach items="${allBankCode}" var="bankCode" varStatus="idx">
                                                            <li class="input-option" data-id="${bankCode.bankCode}" data-value="${bankCode.bankName}"><img src="${bankCode.bankLogo}" />${bankCode.bankName}</li>
                                                            </c:forEach>
                                                        <li class="input-option"  data-id= "" data-value="Other">
                                                            Other
                                                        </li>
                                                    </ul>
                                                    <span class="error" id="error_bank_name"></span> 
                                                    <input type="hidden" class="option" name="namesubmit" value="" />
                                                    <input type="hidden" class="bank_name" name="bank_name1" id="bank_name" value="${company.bankName}" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <label class="label_input" style="text-align:left">
                                                    Other Bank name
                                                </label>
                                            </div>
                                            <div class="col-sm-6 toggal_other">
                                                <input type="text" class="Other_Bank_name form-control input-field" name="other_bank_name" id="other_other_name" value=""  placeholder="Enter your Bank name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' />
                                                <span class="error" id="error_other_bank_name"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account name
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control input-field" id="nameOfAccount" name="nameOfAccount" value="${company.nameOfAccount}"  placeholder="Enter account name"  />
                                                <span class="error" id="spannameOfAccount"></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label_input" style="text-align:left">
                                                    Account number
                                                </label>
                                            </div>
                                            <div class="col-sm-6 validate-ird">
                                                <input type="tel" class="form-control input-field" id="accountNumber" name="accountNumber" value="${company.accountNumber}"  placeholder="xx-xxxx-xxxxxxx-xxx" onkeypress="checkAccountNO()" />
                                                <a class="account-check" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                <input type="hidden" class="isAccount_number_verified " />
                                                <span class="error" id="spanaccountNumber"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="new-attach-info">
                                                    <div class="col-sm-129 closestcls">
                                                        <input type="file" name="myFile" accept="image/x-png,image/jpeg,image/*,.txt,.pdf" onchange="documentFileUpload(this);" class="attach-btn2 checkname" id="attachbankfile" >
                                                        <span class="shownamedata"></span><a href="javascript:void(0);" onclick="removedatafile(this);" class="removefile"><i class="far fa-times-circle"></i></a>
                                                        <span class=" error error_attachbankfile"></span>
                                                    </div>
                                                    <div class="pulsating-circle5 pulsating-around6 btn-info-chng" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                        <span class="tooltiptext tooltip-1 ">To confirm your account details, attach one of the following (must show account number and name and be less than 3 months old): Copy of bank or credit card statement, copy of electronic statement, screenshot from online banking.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous13 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next14 action-button" value="Continue" />
                            </fieldset>
                            <fieldset id="step15">
                                <div class="content-section">
                                    <div class="element-wrapper">
                                        <h5 class="element-header">
                                            Please read and accept the below Terms and Conditions to continue: 
                                        </h5>
                                    </div>
                                    <div class="input-content">
                                        <div class="input-content-text">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="text-center">Agreement of Terms</h4>
                                                    <div class="terms_cond" style="overflow:auto;height: 200px;word-break: break-word;">
                                                        <ol>
                                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                                <ol>
                                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions. Capitalised words are defined in clause 22.</li>
                                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition, tolerance to risk and trading experience.
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>SERVICES PROVIDED</b>
                                                                <ol>
                                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services") in relation to digital currencies (such as, but not limited to, Bitcoin and Ethereum) selected by us from time to time (?Digital Currency?):
                                                                        <ol>
                                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                                            <li>Chat and user support services.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We do not deal in digital currencies or tokens which are or may be Financial Products under New Zealand law on the
                                                                        Platform. For this reason, we are not required to hold any licence or authorization in relation to portfolio management of
                                                                        the Digital Currency we do support on the Platform. If we become aware that any Digital Currency we have supported on
                                                                        the Platform is or may be (in our sole discretion) a Financial Product, you authorize and instruct us to take immediate steps
                                                                        to divest any such holdings at the price applying at the time of disposal.</li>
                                                                </ol>
                                                            </li>
                                                            <li> <b>USER PROFILE</b>
                                                                <ol>
                                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                                        <ol>
                                                                            <li>be 18 years of age or older;</li>
                                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                                </ol>
                                                            </li><li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                                <ol>
                                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                                    <li>For lump sum investments, you may give instructionstoexecuteaTransactionviaour Platform usingyour User Profile.</li>
                                                                    <li>For regular monthly investments, you may select a monthly contribution amount when you create your User Profile.
                                                                        This amount may be amended from time to time via our Platform.</li>
                                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy
                                                                        for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the
                                                                        maximum number of units (including fractions of units, if available) of the relevant investments in accordance with
                                                                        your selected Portfolio. Any remaining money after a Transaction has been executed will be held in your Wallet. You
                                                                        acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.
                                                                        You appoint us, and we accept the appointment, to act as your agent for the execution of any Transaction, in
                                                                        accordance with these Terms and Conditions.</li>
                                                                    <li>We will use reasonable endeavors to execute Transactions within 24 hours of receiving allocated funds into your
                                                                        Wallet (subject to these Terms and Conditions, including clause 4.10). The actual price of a particular investment is
                                                                        set by the issuer, provider or the market (as applicable) at the time the order is executed by us. We may set a
                                                                        minimum transaction amount at any time. We may choose not to process any orders below the minimum transaction
                                                                        amount at our discretion. </li>
                                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                                    <li>After a Transaction has been executed, we reserve the right to void any Transaction which we consider contains any
                                                                        manifest error or is contrary to any law or these Terms and Conditions. In the absence of our fraud or willful default,
                                                                        we will not be liable to you for any loss, cost, claim, demand or expense following any such action on our part or
                                                                        otherwise in relation to the manifest error or Transaction.  </li>
                                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                                    <li>Any Transaction via the Platform is deemed to take place in New Zealand and you are deemed to take possession
                                                                        of your Portfolio in New Zealand
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>PORTFOLIO REBALANCING</b>
                                                                <ol>
                                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                                </ol>
                                                            </li>
                                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                                <ol>
                                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                                    <li>We will notify you of any changes to the Target Asset Allocation promptly following these changes taking effect.
                                                                        Notification of any such changes to the Target Asset Allocation are made via the Platform by way of an update to
                                                                        the Target Asset Allocation details provided for each Portfolio. </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                                <ol>
                                                                    <li><b>Digital Currency Transactions:</b> Invsta processes supported Digital Currency according to the instructions received
                                                                        from its users and we do not guarantee the identity of any party to a Transaction. It is your responsibility to verify all
                                                                        transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a
                                                                        Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the
                                                                        Transaction by the Digital Currency network. A Transaction is not complete while it is in a pending state. Funds
                                                                        associated with Transactions that are in a pending state will be designated accordingly, and will not be included in
                                                                        your Invsta Account balance or be available to conduct Transactions. We may charge network fees (miner fees) to
                                                                        process a Transaction on your behalf. We will calculate the network fee in our sole discretion by reference to the 
                                                                        cost to us. </li>
                                                                    <li><b>Digital Currency Storage &amp; Transmission Delays:</b> Invsta securely stores all Digital Currency private keys in our control
                                                                        in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information
                                                                        from offline storage in order to facilitate a Transaction in accordance with your instructions, which may delay the
                                                                        initiation or crediting of a Transaction for 48 hours or more. You accept that a Digital Currency Transaction facilitated
                                                                        by Invsta may be delayed.</li>
                                                                    <li><b>Operation of Digital Currency Protocols.</b> Invsta does not own or control the underlying software protocols which
                                                                        govern the operation of Digital Currencies supported on our platform. By using the Invsta platform, you acknowledge
                                                                        and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no
                                                                        guarantee of their functionality, security, or availability; and (ii) that the underlying protocols may be subject to
                                                                        sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function,
                                                                        and/or essential nature of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that
                                                                        Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its
                                                                        sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely.
                                                                        You acknowledge and agree that Invsta assumes absolutely no responsibility in respect of an unsupported branch
                                                                        of a forked Digital Currency and/or protocol.</li>
                                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                                    <li>You agree that Digital Curreny investments and any other assets held in your Wallet <b>("Client Property")</b> may be
                                                                        pooled with the investments and other assets of other clients and therefore your holdings may not be individually
                                                                        identifiable within the Wallet. </li>
                                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>("Client Money")</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                                    <li>Money received from you or a third party for your benefit, which includes your Client Money held pending
                                                                        investment, payment to you as the proceeds and income from selling investments, may attract interest from the
                                                                        bank at which it is deposited. You consent to such interest being deducted from that bank account and being
                                                                        retained by us as part of the fees for using the Services.
                                                                    </li>
                                                                    <li>We will keep detailed records of all your Client Property and Client Money in your Wallet at all times. </li>
                                                                    <li>Client Property and Client Money received or purchased by us on your behalf will be held on bare trust as your
                                                                        nominee in the name or to the order of Invsta or any other approved third party custodian to our order. </li>
                                                                    <li>You confirm that you are the beneficial owner of all of the Client Property and Client Money in your account, or you
                                                                        are the sole trustee on behalf of the beneficial owner, and that they are and will remain free from any encumbrance.</li>
                                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?.
                                                                        Withdrawals will be processed on a best endeavors basis as outlined in clause 4.6. Withdrawals of Client Money will
                                                                        be processed and paid into a bank account nominated by you in the same name of your User Profile upon
                                                                        settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn
                                                                        from your Wallet. </li>
                                                                    <li>It is up to you to understand whether and to what extent, any taxes apply to any Transactions through the Services
                                                                        or the Platform, or in relation to your Portfolio. We accept no responsibility for, nor make any representation in respect
                                                                        of, your tax liability.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>OUR WEBSITE POLICY</b>
                                                                <ol>
                                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>RISK WARNINGS</b>
                                                                <ol>
                                                                    <li>You acknowledge and agree that:
                                                                        <ol>
                                                                            <li>investing in Digital Currency is unlike investing in traditional currencies, goods or commodities: it involves
                                                                                significant and exceptional risks and the losses can be substantial. You should carefully consider and assess
                                                                                whether trading or holding of digital currency is suitable for you depending upon your financial condition,
                                                                                tolerance to risk and trading experience and consider whether you should take independent financial
                                                                                advice; </li>
                                                                            <li>we have not advised you to, or recommended that you should, use the Platform and/or Services, or trade
                                                                                and/or hold Digital Currency;</li>
                                                                            <li>unlike other traditional forms of currency, Digital Currency is decentralised and is not backed by a central
                                                                                bank, government or legal entities. As such, the value of Digital Currency can be extremely volatile and may
                                                                                swing depending upon the market, confidence of investors, competing currencies, regulatory
                                                                                announcements or changes, technical problems or any other factors. We give no warranties or
                                                                                representations as to the future value of any Digital Currency and accept no liability for any change in value
                                                                                of your Portfolio; and
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You acknowledge that we do not issue or deal in Financial Products or provide any financial advice and no offer or
                                                                        other disclosure document has been, or will be, prepared in relation to the Services, the Platform and/or any of the
                                                                        Digital Currencies, under the Financial Markets Conduct Act 2013, the Financial Advisers Act 2008 or any other similar
                                                                        legislation in any jurisdiction.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DISPUTE RESOLUTION</b>
                                                                <ol>
                                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:<br>
                                                                        <b>Financial Services Complaints Limited (FSCL)</b><br>
                                                                        PO Box 5967, Lambton Quay<br>
                                                                        Wellington, 6145<br> 
                                                                        Email: info@fscl.org.nz<br>
                                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You authorize us to:
                                                                        <ol>
                                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                                            <li>Record all telephone conversations with you;</li>
                                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                                        <ol>
                                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                                        <ol>
                                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                                        <ol>
                                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                                <ol>
                                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                                <ol>
                                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                                    <li>
                                                                        You accept that there may be circumstances where we are required to suspend or disable your User Profile to meet
                                                                        our anti-money laundering obligations.
                                                                    </li>
                                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>LIMITATION OF LIABILITY</b>
                                                                <ol>
                                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                                    <li>You acknowledge that:
                                                                        <ol>
                                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                                <ol>
                                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest in Crypto Currency Portfolios.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                                    <li>The provisions of this clause 14 will extend to all our employees, agents and contractors, and to all corporate entities
                                                                        in which we may have an interest and to all entities which may distribute our publications.</li>
                                                                    <li>Despite anything else in these Terms and Conditions, if we are found to be liable for any loss, cost, damage or
                                                                        expense arising out of or in connection with your use of the Platform or the Services or these Terms and Conditions,
                                                                        our maximum aggregate liability to you will be limited to two times the total amount of fees and charges that you
                                                                        have paid to us in the previous twelve months in accordance with clause 15 below.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>FEES AND CHARGES FOR SERVICES</b>
                                                                <ol>
                                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges ("Fees"):
                                                                        <ol>
                                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                                            <table class="table-bordered">
                                                                                <tbody><tr>
                                                                                        <td>Portfolio Management Fee</td>
                                                                                        <td>Description</td>
                                                                                        <td>Other Expenses</td>
                                                                                        <td>Performance Fee</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Up to 3% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                                        <td>Based on total investment value, charged monthly</td>
                                                                                        <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                                        <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </ol>
                                                                    </li>
                                                                    <li>You must also pay for any other fees and charges for any add on services you obtain via the Platform or as specified
                                                                        in these Terms and Conditions.</li>
                                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                                </ol>
                                                            </li>

                                                            <li>
                                                                <b>TERMINATION OF YOUR USER PROFILE</b>
                                                                <ol>
                                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to) where:
                                                                        <ol>
                                                                            <li>you are insolvent or in liquidation or bankruptcy; or</li>
                                                                            <li>you have not paid Fees or other amounts due under these Terms and Conditions by the due date</li>
                                                                            <li>you gain or attempt to gain unauthorised access to the Platform or another member?s User Profile or Wallet;</li>
                                                                            <li>we consider any conduct by you (whether or not that conduct is related to the Platform or the Services) puts
                                                                                the Platform, the Services or other users at risk; </li>
                                                                            <li>you use or attempt to use the Platform in order to perform illegal or criminal activities;</li>
                                                                            <li>your use of the Platform is subject to any pending investigation, litigation or government proceeding;
                                                                            </li>
                                                                            <li>you fail to pay or fraudulently pay for any transactions;</li>
                                                                            <li>you breach these Terms and Conditions and, where capable of remedy, fail to remedy such breach within 30
                                                                                days? written notice from us specifying the breach and requiring it to be remedied;</li>
                                                                            <li>your conduct may, in our reasonable opinion, bring us into disrepute or adversely affect our reputation or
                                                                                image; and/or</li>
                                                                            <li>we receive a valid request from a law enforcement or government agency to do so.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>We may also terminate these Terms and cease to provide the Services and the Platform if we undergo an insolvency
                                                                        event, meaning that where that party becomes unable to pay its debts as they fall due, or a statutory demand is
                                                                        served, a liquidator, receiver or manager (or any similar person) is appointed, or any insolvency procedure under
                                                                        the Companies Act 1993 is instituted or occurs. </li>
                                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the
                                                                        proceeds of sale (less any applicable Fees) to a bank account nominated by you. </li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>ASSIGNMENT</b>
                                                                <ol>
                                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                                </ol>
                                                            </li>
                                                            <li><b>INDEMNITY</b>
                                                                <ol>
                                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                                        <ol>
                                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                                            <li>which they may incur or suffer as a result of:
                                                                                <ol>
                                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                                </ol>
                                                                            </li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>AMENDMENTS</b>
                                                                <ol>
                                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                                        <ol>
                                                                            <li>Notice on our website; or</li>
                                                                            <li>Direct communication with you via email,</li>
                                                                        </ol>
                                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                                    </li>
                                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                                </ol>
                                                            </li>
                                                            <li>
                                                                <b>NOTICES</b>
                                                                <ol>
                                                                    <li>Any notice or other communication ("Notice") given for the purposes of these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Must be in writing; and</li>
                                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                                        <ol>
                                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>                
                                                            </li>

                                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                                <ol>
                                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                                </ol>
                                                            </li>

                                                            <li><b>DEFINITIONS</b><br>
                                                                <b>"Account"</b> means the Client Property and Client Money we hold for you and represents an entry in your name on the
                                                                general ledger of ownership of Digital Currency and money maintained and held by<br> 

                                                                <!-- <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br> -->

                                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.<br>

                                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.<br>
                                                                <b>"Client Money"</b> has the meaning given to it in clause 7.6.<br>
                                                                <b>"Client Property"</b> has the meaning given to it in clause 7.5.<br>
                                                                <b>"Digital Currency"</b> means the supported tokens or cryptocurrencies offered on the Platform.<br>

                                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.<br> 

                                                                <!-- <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br> -->

                                                                <!-- <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br>  -->
                                                                <!-- <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br>  -->
                                                                <b>"Minor"</b> means a person under the age of 18.<br>
                                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website <br>

                                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. <br>
                                                                <b>"Portfolio Manager"</b> means Invsta and associated people responsible for managing your Portfolio.<br>

                                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services <br>

                                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.<br>

                                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.<br>

                                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.<br>

                                                            </li>

                                                            <li><b>GENERAL INTERPRETATION</b>
                                                                <ol>
                                                                    <li>In these Terms and Conditions:
                                                                        <ol>
                                                                            <li>Unless the context otherwise requires, references to:
                                                                                <ol>
                                                                                    <li>"we", "us", Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                                </ol>
                                                                            </li>
                                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                                        </ol>
                                                                    </li>
                                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                                </ol>
                                                            </li>

                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:20px">
                                                <div class="col-sm-12" style="text-align: center">
                                                    <!--                                                    <a href="./resources/images/pdf/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                                                                           text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>-->
                                                    <a href="javascript:void(0)" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                                       text-decoration: underline;"> Download Client Terms and Conditions</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 condition-details">
                                                    <!--                                                    <input class="checkbox-check ch1"  type="checkbox" name="term_condition">
                                                                                                        <label class="label_input" style="text-align:left">
                                                                                                            I have read and agree to the Terms and Conditions
                                                                                                        </label>-->
                                                    <label class="last-check">I have read and agree to the Terms and Conditions
                                                        <input type="checkbox" class="checkbox-check ch1" name="term_condition">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <span class="error" id="error_term_condition" ></span>
                                                <div class="col-sm-12 condition-details">
                                                    <!--                                                    <input class="checkbox-check ch2 " type="checkbox" name="authorized_condition">
                                                                                                        <label class="label_input" style="text-align:left">
                                                                                                            I am authorised to accept and act on behalf of all account holders
                                                                                                        </label>-->
                                                    <label class="last-check">I am authorized to accept and act on behalf of all account holders
                                                        <input type="checkbox" class="checkbox-check ch2" name="authorized_condition">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <span class="error" id="error_authorized_condition" ></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>
                                <input type="button" name="previous" class="previous14 action-button-previous" value="Previous" />
                                <input type="button" name="next" class="next15 action-button" value="Accept" />
                            </fieldset>
                            <fieldset id="step16">
                                <div class="content-section">
                                    <div class="row">
                                        <div class="col-md-12 thanku">
                                            <div class="element-wrapper">
                                                <h5 class="element-header">
                                                    Thank for submitting your application details, we'll let you know if we require anything further.
                                                </h5>
                                            </div>
                                            <h5 class="element-header3">
                                                To continue to your account and make an investment, please verify your email address by clicking on the link in the email we have sent.
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer_form">
                                    <!--<img src="./resources/images/red.png">-->	
                                </div>

                                <div class="col-md-12" style="max-width: 113px;margin: auto;">
                                    <div class="ok-submit">
                                        <input type="button" name="previous" id="submit" class=" action-button-previous new-ok-btn " value="OK" />
                                    </div>
                                </div>
                                <!-- <input type="button" name="next" class="next12 action-button" value="Accept" /> -->
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
        <script src="./resources/js/index.js"></script>        
        <script src="./resources/js/intlTelInput_1.js"></script>
        <script src="./resources/js/intlTelInput_2.js"></script>
        <script src="./resources/js/intlTelInput_3.js"></script>
        <script src="./resources/js/user-main.js"></script>
        <script type="text/javascript" src="./resources/js/countries.js"></script>
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js'></script>
        <script src="./resources/js/sweetalert.min.js">

        </script>
        <script src="./resources/js/bankValidator/bankValidator.js"></script>
        <script>
                                                            var bankFile = '';
                                                            var otherFile = '';
                                                            var deedFile = '';
                                                            function documentFileUpload(selectedfile) {
//                                                        var selectedfile = document.getElementById("bank_document").files;

//                                                        $(".loadingcircle").show();
                                                                var id = selectedfile.id;
                                                                var value = selectedfile.value;
                                                                id = id.trim();
                                                                selectedfile = selectedfile.files;
                                                                if (selectedfile.length > 0) {
                                                                    var imageFile = selectedfile[0];
                                                                    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
                                                                    if (allowedExtensions.exec(value)) {
                                                                        var size = selectedfile[0].size;
                                                                        if (size < 2008576) {
                                                                            var fileReader = new FileReader();
                                                                            fileReader.onload = function (fileLoadedEvent) {
                                                                                if (id === "bank_document") {
                                                                                    bankFile = fileLoadedEvent.target.result;
                                                                                    console.log("bankFileSRC" + bankFile);
//                                                                            $(".loadingcircle").hide();
                                                                                }
                                                                                if (id === "other_id_myFile") {
                                                                                    otherFile = fileLoadedEvent.target.result;
                                                                                    console.log("otherFileSRC" + otherFile);
//                                                                            $(".loadingcircle").hide();
                                                                                }
                                                                                if (id === "attachdeedfile") {
                                                                                    deedFile = fileLoadedEvent.target.result;
                                                                                    console.log("deedFileSRC" + deedFile);
//                                                                            $(".loadingcircle").hide();
                                                                                }

                                                                            };
                                                                            fileReader.readAsDataURL(imageFile);
                                                                        }
                                                                    }

                                                                }
                                                            }

                                                            $(function () {
                                                                $('.chosen-select').chosen();
                                                                $('.chosen-select-deselect').chosen({allow_single_deselect: true});
                                                            });

                                                            $('#accountNumber').keyup(function () {
                                                                var accountnumber = $('#accountNumber').val();
                                                                $('.account-check').hide();
                                                                $('.isAccount_number_verified').val('false');
//                                                        alert(accountnumber);
                                                                if (accountnumber.length === 19) {

                                                                    var AccountArray = accountnumber.split('-');
//                                                            alert(AccountArray);
                                                                    var bk = AccountArray[0];
                                                                    var brch = AccountArray[1];
                                                                    var acct = AccountArray[2];
                                                                    var suf = AccountArray[3];
//                                                            alert(bk + "  --" + brch + "  " + acct + "   " + suf);
                                                                    var resl = isValidNZBankNumber(bk, brch, acct, suf);
//                                                            alert(resl);
                                                                    if (resl) {
                                                                        $('.account-check').show();
                                                                        $('.isAccount_number_verified').val('true');
                                                                    } else {
                                                                        $('.isAccount_number_verified').val('false');
//                                $('.account-check').hide();
//                                                                        $('#spanaccountNumber').text('account number is invalid');
                                                                    }
                                                                }

                                                            });



                                                            $(document).ready(function () {
//                $(".otpNext").attr("disabled", true);
                                                                $(" .ird-check").hide();
                                                                $('.account-check').hide();
                                                                $(".toggal_other").hide();
                                                                $('.save-new-btn').show();
                                                                $('#cOccupation').hide();
                                                                var arrName = [];
                                                                var thisName;
                                                                $(window).keydown(function (event) {
                                                                    if (event.keyCode === 13) {
                                                                        event.preventDefault();
                                                                        return false;
                                                                    }
                                                                });
                                                            });
                                                            function initAutocomplete() {
                                                                autocomplete = new google.maps.places.Autocomplete(
                                                                        (document.getElementById('address')),
                                                                        {types: ['address'], componentRestrictions: {country: 'nz'}});
                                                                autocomplete1 = new google.maps.places.Autocomplete(
                                                                        (document.getElementById('address1')),
                                                                        {types: ['address'], componentRestrictions: {country: 'nz'}});
                                                            }
                                                            var date = new Date();
                                                            var day = date.getDate();
                                                            var month = date.getMonth();
                                                            var year = date.getFullYear();
                                                            var adultDOB = date.setFullYear(year - 18, month, day);
                                                            $("#countryCode").intlTelInput_1();
                                                            $("#countryCode2").intlTelInput_1();
                                                            $("#countryCode3").intlTelInput_1();
                                                            $("#countryCode4").intlTelInput_1();
                                                            $("#countryCode5").intlTelInput_1();
                                                            $(".countryname").intlTelInput_2();
                                                            $(".countrynameexcludenz").intlTelInput_3();
                                                            $(".countryCode").intlTelInput_1();
                                                            $('#otp-block').hide();
                                                            $('.removefile').hide();
                                                            $('.inputtype').hide();
                                                            $('.otherType').hide();
                                                            $(document).ready(function () {
                                                                $('.passport-select').hide();
                                                                $('.other-select').hide();
                                                                $('.passport-select1').hide();
                                                                $('.other-select1').hide();
                                                                $('.yes-option').hide();
                                                                $('.yes-option1').hide();
                                                                $('.yes-option4').hide();
                                                                $('.selectno6').hide();
                                                                $('.des-togle').hide();
                                                                $('#mobileNo').keyup(function () {
                                                                    $('#error-generateOtp').text('');
                                                                });
                                                                $('#otp').keyup(function () {
                                                                    $('#error-generateOtp').text('');
                                                                });
                                                                $("#dob").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(adultDOB),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".dob1").datepicker({
                                                                    yearRange: (year) + ':' + (year + 80),
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'minDate': new Date(),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".dob").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(adultDOB),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
                                                                $(".doc").datepicker({
                                                                    yearRange: (year - 80) + ':' + year,
                                                                    changeMonth: true,
                                                                    changeYear: true,
                                                                    'maxDate': new Date(),
                                                                    dateFormat: 'dd/mm/yy'
                                                                }).datepicker().attr('readonly', 'readonly');
//                $('input:radio[name="senderType"]').change(function () {
//                    var mNo = $('.mobile_number1').val();
//                    var cCo = $('.mobile_country_code').val();
//                    var sTy = $('.senderType:checked').val();
//                    cCo = cCo.replace(/\+/g, "");
//                    genrateotp(mNo, cCo, sTy);
//
//                    $('#otp-block').show();
//                    document.getElementById('error-loader').style.display = 'none';
//                    document.getElementById('error-generateOtp').innerHTML = "Please check your messages";
//                    $('input:radio[name="senderType"]').prop("checked", false);
//                });
                                                                $('.otpkey').keyup(function () {
                                                                    $('.spanverification').html("");
                                                                    if ($('#verificationa').val().length === 6) {
                                                                        var mNo = $('.mobile_number1').val();
                                                                        var cCo = $('.mobile_country_code').val();
                                                                        var otp = $('#verificationa').val();
                                                                        cCo = cCo.replace(/\+/g, "");
                                                                        validateOTP(mNo, cCo, otp);
                                                                    }
                                                                });
                                                                $('input[name="term_condition"]:checked').change(function () {
//                        alert("hello");
                                                                });
                                                                $('#fourthfs-continue').click(function () {
                                                                    var ele = $(this);
                                                                    moveNextProcess(ele);
                                                                });
                                                            });
                                                            clearValidationId3 = function () {
                                                                $("#validationId3").html('');
                                                            };
                                                            isInviteCodeUsed = function (ele) {
                                                                var inviteCode = $('#inviteCode').val();
                                                                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                                                                $.ajax({
                                                                    url: url,
                                                                    type: "GET",
                                                                    async: false,
                                                                    success: function (response) {
//                                                                        if (response === 'true') {
//                                                                            $("#error-inviteCode").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
//                                                                            $("#error-inviteCode").focus();
//                                                                            moveNextProcess(ele);
//                                                                            return true;
//                                                                        } else {
//                                                                            $("#error-inviteCode").css({'color': 'red', 'margin-left': '10px'}).html('Invite Code is used or invalid.');
//                                                                            $("#error-inviteCode").focus();
//                                                                            return false;
//                                                                        }
                                                                    },
                                                                    error: function (e) {
                                                                        //handle error
                                                                    }
                                                                });
                                                            };
        </script>

        <script>
            $('.senderType').click(function () {
                //                var mNo = $("#mobileNo3").val();
                //                var cCo = $("#countryCode3").val();
                var mNo = $('.mobile_number1').val();
                var cCo = $('.mobile_country_code').val();
                var sTy = $('.senderType:checked').val();
                cCo = cCo.replace(/\+/g, "");
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                //document.getElementById('error-loader').style.display = 'block';
                document.getElementById('error-generateOtp').innerHTML = "";
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json",
                    success: function (data) {
//                        $('#error-generateOtp').html("Please check your messages");
                        $('#otp-block').show();
                        //            document.getElementById('error-loader').style.display = 'none';
                        $('input:radio[name="senderType"]').prop("checked", false);
                    },
                    error: function (e) {
                        var mobileNo = document.getElementsByClassName('mobile_number1');
                        var result = true;
                        $('#otp-block').show();
                        if (mobileNo.value === '') {
                            //document.getElementById('error-loader').style.display = 'none';
                            //                 $('#error-generateOtp').text("Mobile number is required.");
                            $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        } else {
                            //document.getElementById('error-loader').style.display = 'none';

                            //                 $('input:radio[name="senderType"]').prop("checked", false);
                            result = result && false;
                        }
                        return result;
                    }
                });
                return sTy;
            });
        </script>

        <script>
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") === "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
            var moreInvestorArr = [];
            var investors = [];
            var current_investor = {};
            var directors = [];
            var x = 0, y = 0, idx = 0;
            var checkfirstdir = 0;

            getMoreInvestorArr = function () {
                var moreInvestorArray = new Array();
                var moreInvestorInfoArr = document.getElementsByClassName('morestep1');
                for (var i = 0; i < moreInvestorInfoArr.length - 1; i++) {
                    var s6x = 'step7' + i;
                    var s6xh = '#step7' + i;
                    var s6xh1 = s6xh + 1;
                    var s6xh2 = s6xh + 2;
                    var s6xh3 = s6xh + 3;
                    var s6xh4 = s6xh + 4;



                    var index = $(s6xh3 + " .src_of_fund2 option:selected").val();
                   
//                alert(index);
                    var cls = "";
                    if (index === "1") {
                        cls = ' .drivery-licence1  ';
                    }
                    if (index === "2") {
                        cls = ' .passport-select1 ';
                    }
                    if (index === "3") {
                        cls = ' .other-select1 ';
                    }
//                alert(cls + "2");
                    var firstName = "", middleName = "", lastName = "";
                    firstName = $(s6xh3 + cls + '.first_name').val();
                    middleName = $(s6xh3 + cls + '.middle_name').val();
                    lastName = $(s6xh3 + cls + '.last_name').val();
//                alert(firstName + " ---" + middleName + " -- " + lastName);



//                    var title = $(s6xh1 + ' .more-investor-title option:selected').text();
                    var fname = $(s6xh1 + ' .director-name').text();
                    var email = $(s6xh1 + ' .more-director-email').val();
                    var send_email = $(s6xh1 + ' .checkradio:checked').val();
                    var dob = $(s6xh2 + ' .more-director-dob').val();
                    var position = $(s6xh2 + ' .more-director-positionInCompany').val();
                    if (position === "-Select-" || position === " -Select-") {
                        position = "No";
                    }
                    var countryname = $(s6xh4 + ' .more-director-countryname').val();
//                    alert(countryname);
                    var OccupationOption = $(s6xh2 + ' .more-director-select-occu option:selected').val();
                    var occupation = $(s6xh2 + '.otherOcc').text();
                    if (OccupationOption === "0") {
                        occupation = $(s6xh2 + '.otherOcc').text();
                    } else if (OccupationOption === " -Select-" || OccupationOption === "-Select-") {
                        occupation = "No";
                        OccupationOption = "No";
                    } else {
                        occupation = $(s6xh2 + ' .more-director-select-occu option:selected').text();
                    }
                    var address = $(s6xh2 + ' .more-director-address').val();
                    var countrycode = $(s6xh2 + ' .more-director-countrycode').val();
                    var mobile = $(s6xh2 + ' .more-director-mobile').val();
                    if (mobile === "") {
                        countrycode = "";
                    }
                    var srcOfFund = $(s6xh3 + ' .src_of_fund2 option:selected').text();
                    if (send_email === '2') {
                        srcOfFund = "";
                    }
//                    var more_licence_first_name = $(s6xh3 + ' .first_name').val();
//                    var more_licence_middle_name = $(s6xh3 + ' .middle_name').val();
//                    var more_licence_last_name = $(s6xh3 + ' .last_name').val();
                    var licenseNumber = $(s6xh3 + ' .more-director-licenseNumber').val();
                    var licenseExpiryDate = $(s6xh3 + ' .more-director-licenseExpiryDate').val();
                    var versionNumber = $(s6xh3 + ' .more-director-versionNumber').val();
                    var passportNumber = $(s6xh3 + ' .more-director-passportNumber').val();
                    var passportExpiryDate = $(s6xh3 + ' .more-director-passportExpiryDate').val();
                    var passportCountryOfIssue = $(s6xh3 + ' .more-director-passportCountryOfIssue').val();
                    if (passportCountryOfIssue === " –Select–" || passportCountryOfIssue === "–Select–") {
                        passportCountryOfIssue = "";
                    }
                    var typeOfId = $(s6xh3 + ' .more-director-typeOfId').val();
                    var typeOfIdExpiryDate = $(s6xh3 + ' .more-director-typeOfIdExpiryDate').val();
                    var typeOfIdCountryOfIssue = $(s6xh3 + ' .more-director-typeOfIdCountryOfIssue').val();
//                    alert(typeOfIdExpiryDate +"exp date and more director  typeof id n country " + typeOfIdCountryOfIssue);
                    if (typeOfIdCountryOfIssue === " –Select–" || typeOfIdCountryOfIssue === "–Select–") {
                        typeOfIdCountryOfIssue = "";
                    }
                    var more_idverified = $(s6xh3 + ' .more_director_verify').val();
                    var more_directorPep_verify = $(s6xh3 + ' .more_directorPep_verify').val();
                    var Title = $(s6xh3 + ' .hidden-title').val();
                    var irdNumber = $(s6xh4 + ' .more-director-irdNumber').val();
//                    irdNumber = irdNumber.replace(/-/g, '');
                    var usCitizen = $(s6xh4 + ' .more-director-usCitizen').val();
                    var fs = document.getElementById(s6x + '4');
                    if (fs !== null && typeof fs !== "null") {
                        var countryArr = fs.getElementsByClassName('more-director-tex_residence_Country');
                        var TINArr = fs.getElementsByClassName('more-director-TIN');
                        var reasonArr = fs.getElementsByClassName('more-director-resn_tin_unavailable');
                        var countryTINList = new Array();
                        for (var j = 0; j < countryArr.length; j++) {
                            var country = countryArr[j].value;
                            if (country === " -Select-" || country === "-Select-") {
                                country = "";
                            }
                            var tin = TINArr[j].value;
                            var reason = reasonArr[j].value;
                            if (reason === " –Select–" || reason === "–Select–") {
                                reason = "";
                            }
                            countryTINList.push({country: country, tin: tin, reason: reason});
                        }
                    }

                    var moreInvestor = {usCitizen: usCitizen, fname: fname, fullName: fname, email: email, send_email: send_email, dateOfBirth: dob, holderCountryOfResidence: countryname, occupation: occupation, OccupationOption: OccupationOption, pepStatus: more_directorPep_verify,
                        address: address, firstName: firstName, lastName: middleName, middleName: lastName, countryCode: countrycode, mobileNo: mobile, srcOfFund: srcOfFund, licenseNumber: licenseNumber,
                        licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber, title: Title,
                        passportExpiryDate: passportExpiryDate, countryOfIssue: passportCountryOfIssue, typeOfID: typeOfId, typeExpiryDate: typeOfIdExpiryDate,
                        otherIdIssuedBy: typeOfIdCountryOfIssue, irdNumber: irdNumber, countryTINList: countryTINList, investor_idverified: more_idverified, positionInCompany: position};
                    console.log('[' + i + '] ' + JSON.stringify(moreInvestor));
                    moreInvestorArray.push(moreInvestor);
                }
                console.log(JSON.stringify(moreInvestorArray));
                return moreInvestorArray;

            };
            function prev(ele) {
                var currfs = $(ele).parent();
                var prevfs = $(ele).parent().prev();
                var cidc = currfs.attr('id');
                var pidc = prevfs.attr('id');
                var hasCls = prevfs.hasClass('more-director-fs');
                if (cidc === 'step12' || cidc === 'morestep1' || cidc === 'morestep2' || cidc === 'morestep3' || cidc === 'morestep4') {
                    var idx = document.getElementsByClassName('morestep1').length - 2;//2=>1
                    var s6x = '#step7' + idx;
                    var chk = $(s6x + '1').find('input[type="radio"]:checked').val();
                    if (chk === '2') {
                        $(currfs).hide();
                        var btn = prevfs.find('input[name="previous"]');
                        $(s6x + '1').show();
                    } else if (chk === '1') {
                        $(currfs).hide();
                        $(s6x + '4').show();
                    } else {
                        $(currfs).hide();
                        $('#step7').show();
                    }
                } else if (hasCls === true) {
                    pidc = pidc.substring(0, pidc.length - 1);
                    var chk = $('#' + pidc + '1').find('input[type="radio"]:checked').val();
                    if (chk === '2') {
                        var btn = prevfs.find('input[name="previous"]');
                        $(currfs).hide();
                        $('#' + pidc + '1').show();
                    } else {
                        $(currfs).hide();
                        $(prevfs).show();
                    }
                } else {
                    $(currfs).hide();
                    $(prevfs).show();
                }
            }
            function next(ele) {
                var currfs = $(ele).parent();
                var nextfs = $(ele).parent().next();
                var idc = nextfs.attr('id');
                var hasCls = currfs.hasClass('more-director-fs');
                if (idc === 'morestep1') {
                    $(currfs).hide();
                    $('#step12').show();
                } else if (hasCls === true) {
//                    alert(1);
                    idc = idc.substring(0, idc.length - 1);
                    var chk = $('#' + idc + '1').find('input[type="radio"]:checked').val();
                    if (chk === '2') {
                        var btn = nextfs.find('input[type="button"]');
                        $(currfs).hide();
                        next(btn);
                    } else {
                        $(currfs).hide();
                        $(nextfs).show();
                    }
                } else {
                    $(currfs).hide();
                    $(nextfs).show();
                }
            }
            function prev7(ele) {
                $("#step").attr("value", 7);
                prev(ele);
            }
            function prev8(ele) {
                $("#step").attr("value", 8);
                prev(ele);
            }
            function prev9(ele) {
                $("#step").attr("value", 9);
                prev(ele);
            }
            function prev10(ele) {
                $("#step").attr("value", 10);
                prev(ele);
            }
            function next8(ele) {
//                        alert("next8");
                var curr = $(ele).parent();
                var idc = '#' + curr.attr('id');
                var email = $(idc + ' .more-director-email').val();
                var emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//                if (email === "" || !emailExpression.test(email)) {
//                    $(idc + ' .more-director-email-error').text("Email must be 'example@email.com' format");
//                } else {
                    $("#step").attr("value", 9);

                    next(ele);

//                }
            }
            function next9(ele) {
//                         alert("step9");
                var curr = $(ele).parent();
                var idc = '#' + curr.attr('id');
                var address = $(idc + ' .more-director-address').val();
                var dob = $(idc + ' .more-director-dob').val();
                var occupationSel = $(idc + ' .more-director-select-occu option:selected').val();
                var occupation = $(idc + ' .more-director-input-occu').val();
                var mobile = $(idc + ' .more-director-mobile').val();
                var position = $(idc + ' .more-director-positionInCompany option:selected').val();
//                if (address === "") {
//                    $(idc + ' .more-director-address-error').text("This field is required");
//                } else if (mobile === "") {
//                    $(idc + ' .more-director-mobile-error').text("This field is required");
//                } else if (dob === "") {
//                    $(idc + ' .more-director-dob-error').text("This field is required");
//                } else if (occupationSel === "-Select-" || occupationSel === "0" && occupation === "") {
//                    $(idc + ' .spanOccupation').text("This field is required");
//                } else if (position === "-Select-" || position === " -Select-") {
//                    $(idc + ' .more-director-positionInCompany-error').text("This field is required");
//                } else {
                    $("#step").attr("value", 9);
                    next(ele);
//                }
            }
            function next10(ele) {
                var curr = $(ele).parent();
                var prevfs = $(ele).parent().prev();
                var idc = '#' + curr.attr('id');
                var pid = '#' + prevfs.attr('id');
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var License_number = '';
                var licence_verson_number = '';
                var licence_expiry_Date = '';
                var passport_number = '';
                var passport_expiry = '';
                var index = $(idc + " .src_of_fund2 option:selected").val();
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = $(idc + " .more_licence_first_name").val();
                    middleName = $(idc + " .more_licence_middle_name").val();
                    lastName = $(idc + " .more_licence_last_name").val();
                    License_number = $(idc + " .more-director-licenseNumber").val();
                    licence_expiry_Date = $(idc + " .more-director-licenseExpiryDate").val();
                    licence_verson_number = $(idc + " .more-director-versionNumber").val();
//                    if (firstName === "") {
//                        $(idc + " .more_error_licence_first_name").text("This field is required ");
//                    } else if (lastName === "") {
//                        $(idc + " .more_error_licence_last_name").text("This field is required ");
//                    } else if (License_number === "") {
//                        $(idc + " .more-director-licenseNumber-error").text("This field is required ");
//                    } else if (Expirydate === "") {
//                        $(idc + " .more-director-licenseExpiryDate-error").text("This field is required ");
//                    } else if (licence_verson_number === "") {
//                        $(idc + " .more-director-versionNumber-error").text("This field is required ");
//                    } else {
                        $("#step").attr("value", 11);
                        next(ele);
//                    }
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = $(idc + ' .more_passport_first_name').val();
                    middleName = $(idc + ' .more_passport_middle_name').val();
                    lastName = $(idc + ' .more_passport_last_name').val();
                    passport_number = $(idc + " .more-director-passportNumber").val();
                    passport_expiry = $(idc + ' .more-director-passportExpiryDate').val();
                    var passport_country = $(idc + ' .more-director-passportCountryOfIssue').val();
//                    if (passport_number === "") {
//                        $(idc + " .more-director-passportNumber-error").text("This field is required ");
//                    } else if (passport_expiry === "") {
//                        $(idc + " .more-director-passportExpiryDate-error").text("This field is required ");
//                    } else if (passport_country === " -Select-") {
//                        $(idc + " .more-director-passportCountryOfIssue-error").text("This field is required ");
//                    } else {
                        $("#step").attr("value", 11);
                        next(ele);
//                    }
                } else {
                    var TypeofID = $(idc + " .more-director-typeOfId").val();
                    firstName = $(idc + " .more-director-first_name").val();
                    middleName = $(idc + " .more-director-middle_name").val();
                    lastName = $(idc + " .more-director-last_name").val();
                    var Expirydate = $(idc + " .more-director-typeOfIdExpiryDate").val();
                    var country = $(idc + " .more-director-typeOfIdCountryOfIssue").val();
                    var IdDocument = $(idc + " .more-director-file").val();

//                    if (TypeofID === "") {
//                        $(idc + " .more-director-typeOfId-error").text("This field is required ");
//                    } else if (firstName === "") {
//                        $(idc + " .error_other_first_name").text("This field is required");
//                    } else if (lastName === "") {
//                        $(idc + " .error_other_last_name").text("This field is required");
//                    } else if (Expirydate === "") {
//                        $(idc + " .more-director-typeOfIdExpiryDate-error").text("This field is required");
//                    } else if (country === " -Select-") {
//                        $(idc + " .more-director-typeOfIdCountryOfIssue-error").text("This field is required ");
//                    } else if (IdDocument === "") {
//                        $(idc + " .error-more-file").text("Please attach the Document");
//                    } else {
                        $("#step").attr("value", 11);
                        next(ele);
//                    }
                }

                var Date_of_Birth = $(pid + ' .more-director-dob').val();
                var Title = $(idc + ' .hidden-title').val();

                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date, title: Title,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                if (index === "1" || index === "2") {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        headers: {"Content-Type": 'application/json'},
                        data: JSON.stringify(DataObj),
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                            var obj = JSON.parse(data);
                            if (index === "1") {
                                if (obj.driversLicence.verified) {
                                    $(idc + ' .more_director_verify').val('true');
                                } else {
                                    $(idc + ' .more_director_verify').val('false');
                                }
                            } else if (index === "2") {
                                if (obj.passport.verified) {
                                    $(idc + ' .more_director_verify').val('true');
                                } else {
                                    $(idc + ' .more_director_verify').val('false');
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                        }
                    });
                }
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/pep-verification',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var obj = JSON.parse(data);
//                        alert(JSON.stringify(  obj.watchlistAML[0].verified));
//                        if (index === "1") {
                        if (obj.watchlistAML[0].verified) {
//                              
                            $(idc + ' .more_directorPep_verify').val('true');
                        } else {

                            $(idc + ' .more_directorPep_verify').val('false');
                        }
//                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });

            }
            function next11(ele) {
                var curr = $(ele).parent();
                var idc = '#' + curr.attr('id');
                var idr = $(idc + ' .more-director-irdNumber').val();
                var errorIRD = $(idc + ' .more-director-irdNumber-error');
                var country = $(idc + ' .more-director-countryname').val();
                var USCitizen = $(idc + " .more-director-usCitizen option:selected").val();
                var j = 0;
                var validate = moreValidateIrd(idc, idr, errorIRD);
                if (validate) {
                    if (country === " –Select–") {
                        $(idc + ' .more-director-countryname-error').text("This field is required");
                    } else if (USCitizen === "2") {
                        var tindivs = $(curr).find('.checktin3data');
                        for (var i = 0; i < tindivs.length; i++) {
                            var tindiv = tindivs[i];
                            var tinnum = tindiv.getElementsByClassName('more-director-TIN')[0];
                            var tinerror = tindiv.getElementsByClassName('more-director-TIN-error')[0];
                            var country = tindiv.getElementsByClassName('excludenz')[0];
                            var countryerror = tindiv.getElementsByClassName('excludenz-error')[0];
                            var reason = tindiv.getElementsByClassName('more-director-resn_tin_unavailable')[0];
                            var reasonerror = tindiv.getElementsByClassName('more-director-resn_tin_unavailable-error')[0];
//                            if (country.value === " -Select-") {
//                                countryerror.innerHTML = "This field is required ";
//                            } else if (tinnum.value === "" && reason.value === "") {
//                                reasonerror.innerHTML = "This field is required ";
//                                tinerror.innerHTML = "This field is required ";
//                            } else {
                                j++;
//                            }
                        }
                        if (tindivs.length > j) {

                        } else {
                            $("#step").attr("value", 12);
                            next(ele);
                        }
                    } else {
                        $("#step").attr("value", 12);
                        next(ele);
                    }
                }

            }
            $('.selectoption2').change(function () {
                var val = $(".selectoption2 option:selected").val();
                if (val === "1") {
                    $('.yes-option1').hide();
                }
                if (val === "2") {
                    $('.yes-option1').show();
                }
            });


        </script>
        <script>
            var x = 0;
            $(".previous1").click(function () {
                $("#step").attr("value", 1);
                $("#step2").hide();
                $("#step1").show();
            });
            $(".previous2").click(function () {
                $("#step").attr("value", 2);
                $("#step3").hide();
                $("#step2").show();
            });
            $(".previous3").click(function () {
                $("#step").attr("value", 3);
                $("#step4").hide();
                $("#step3").show();
            });
            $(".previous4").click(function () {
                $("#step").attr("value", 4);
                $("#step5").hide();
                $("#step4").show();
            });
            $(".previous5").click(function () {
                $("#step").attr("value", 5);
                $("#step6").hide();
                $("#step5").show();
            });
            $(".previous6").click(function () {
                $("#step").attr("value", 6);
                $("#step7").hide();
                $("#step6").show();
            });
            $(".previous11").click(function (event) {
                $("#step").attr("value", 11);
                prev(event.target);
            });
            $(".previous12").click(function () {
                $("#step").attr("value", 12);
                $("#step13").hide();
                $("#step12").show();
            });
            $(".previous13").click(function () {
                $("#step").attr("value", 13);
                $("#step14").hide();
                $("#step13").show();
            });
            $(".previous14").click(function () {
                $("#step").attr("value", 14);
                $("#step15").hide();
                $("#step14").show();
            });
            $(".previous15").click(function () {
                $("#step").attr("value", 15);
                $("#step16").hide();
                $("#step15").show();
            });
            $(".next1").click(function () {
                var companyName = $("#companyName").val();
                companyName = companyName.trim();
                var countryname = $("#Country_of_incorporation").val();
                countryname = countryname.trim();
                var companyDate = $("#Date_of_incorporation").val();
                companyDate = companyDate.trim();
                var companyRegistration = $("#registrationNumber").val();
                companyRegistration = companyRegistration.trim();
                var companyAddress = $("#companyAddress").val();
                companyAddress = companyAddress.trim();
                var type = $(".selectType option:selected").val();
                var otherType = $(".otherType").val();
                var file = $("#attachdeedfile").val();
//                if (companyName === "") {
//                    $("#spanCompanyName").text("This field is required ");
//                } else if (companyAddress === "") {
//                    $("#spanCompanyAddress").text("This field is required ");
//                } else if (countryname === "–Select–") {
//                    $("#spanCountryCode").text("This field is required ");
//                } else if (companyDate === "") {
//                    $("#spanCompanyDate").text("This field is required ");
//                } else if (type === "1" || type === "4" && otherType === "") {
//                    if (type === "1") {
//                        $("#spanTypeTrust").text("This field is required ");
//                    } else {
//                        $("#spanTypeTrustother").text("This field is required ");
//                    }
//                } else if (companyRegistration === "") {
//                    $("#spanRegistrationNumber").text("This field is required ");
//                } else if (file === "") {
//                    $(".error_attachdeedfile").text("Please attach a copy of document");
//                } else {
                    $("#step").attr("value", 2);
                    $("#step2").show();
                    $("#step1").hide();
//                }
            });
            $(".next2").click(function () {
                var directorDivs1 = document.getElementsByClassName('director-add');
//                  alert("1");
                var j = 0;
                var k = 0;
                for (var i = 0; i < directorDivs1.length; i++) {
                    var directorDiv1 = directorDivs1[i];

                    var fnameInput1 = directorDiv1.getElementsByClassName('fname')[0];
                    var spanfname = directorDiv1.getElementsByClassName('spanfname')[0];
//                    if (fnameInput1.value.trim() === "") {
//                        spanfname.innerHTML = "This field is required ";
//                    } else {
                        k++;
//                    }
                }
                if (directorDivs1.length <= k) {
                    for (var i = 0; i < directorDivs1.length; i++) {
                        var moreInvestor1 = directorDivs1[i];
                        var titleInput1 = moreInvestor1.getElementsByClassName('directorTitle')[0];
                        var fnameInput1 = moreInvestor1.getElementsByClassName('fname')[0];
                        var meInput = moreInvestor1.getElementsByClassName('cls-me')[0];
                        if (meInput.value === 'Y') {
                            var fn = fnameInput1.value;
                            $('.this-name').text(fn);
                            fn = fn.replace(/(^[\s]*|[\s]*$)/g, '');
                            fn = fn.replace(/\s+/g, ' ');
                            var fnarr = fn.split(" ");
//                            alert(fnarr);
                            $('#step6 .first_name').val(fnarr[0]);
                            if (fnarr.length >= 3) {
                                $('#step6  .last_name').val(fnarr[fnarr.length - 1]);
                                fnarr.shift();
                                fnarr.pop();
                                fnarr = fnarr.toString().replace(/,/g, ' ');
                                $('#step6  .middle_name').val(fnarr);
                            } else if (fnarr.length === 3) {
                                $('#step6 .middle_name').val(fnarr[1]);
                                $('#step6 .last_name').val(fnarr[2]);
                            } else if (fnarr.length === 2) {
                                $('#step6 .last_name').val(fnarr[1]);
                            }
                        } else {
//                    var emailInput1 = moreInvestor1.getElementsByClassName('emailAddress')[0];
//                    var checkradio = moreInvestor1.querySelector('input[type="radio"]:checked');
                            var m1 = document.getElementById('morestep1');
                            var m2 = document.getElementById('morestep2');
                            var m3 = document.getElementById('morestep3');
                            var m4 = document.getElementById('morestep4');
                            var s6x = 'step7' + j;
                            var idc = '#' + s6x;
                            var s6x1 = s6x + 1;
                            var s6x1ele = document.getElementById(s6x1);
                            if (s6x1ele === null || typeof s6x1ele === 'undefined') {
                                var m1c = m1.cloneNode(true);
                                var m2c = m2.cloneNode(true);
                                var m3c = m3.cloneNode(true);
                                var m4c = m4.cloneNode(true);
                                m1c.id = s6x + 1;//601
                                m2c.id = s6x + 2;//602
                                m3c.id = s6x + 3;//603
                                m4c.id = s6x + 4;//604
                                var changeid1 = "firstid" + j;
                                var changeid2 = "secondid" + j;
                                m1c.getElementsByClassName("checkradio")[0].removeAttribute("id");
                                m1c.getElementsByClassName("radio1")[0].setAttribute("id", changeid1);
                                m1c.getElementsByClassName("radio2")[0].setAttribute("id", changeid2);
                                m1c.getElementsByClassName("radio1")[0].setAttribute("name", changeid1);
                                m1c.getElementsByClassName("radio2")[0].setAttribute("name", changeid1);
                                m1c.getElementsByClassName("forlabel1")[0].setAttribute("for", changeid1);
                                m1c.getElementsByClassName("forlabel2")[0].setAttribute("for", changeid2);
                                m1c.classList.add("more-director-info");
                                m1c.classList.add("more-director-fs");
                                m2c.classList.add("more-director-info");
                                m2c.classList.add("more-director-fs");
                                m3c.classList.add("more-director-info");
                                m3c.classList.add("more-director-fs");
                                m4c.classList.add("more-director-info");
                                m4c.classList.add("more-director-fs");
                                if (j === 0) {
                                    $("#step7").after(m1c);
                                } else {
                                    var s6x4 = "#step7" + (j - 1) + "4";
                                    $(s6x4).after(m1c);
                                }
                                $(m1c).after(m2c);
                                $(m2c).after(m3c);
                                $(m3c).after(m4c);
//                        alert("4");
                                var fn = fnameInput1.value;
                                fn = fn.replace(/(^[\s]*|[\s]*$)/g, '');
                                fn = fn.replace(/\s+/g, ' ');
                                var fnarr = fn.split(" ");
//                                alert("fnarr" + fnarr);
                                $(idc + 3 + ' .hidden-title').val(titleInput1.options[titleInput1.selectedIndex].value);
                                $(idc + 3 + ' .first_name').val(fnarr[0]);
                                if (fnarr.length >= 3) {
                                    $(idc + 3 + ' .last_name').val(fnarr[fnarr.length - 1]);
                                    fnarr.shift();
                                    fnarr.pop();
                                    fnarr = fnarr.toString().replace(/,/g, ' ');
                                    $(idc + 3 + ' .middle_name').val(fnarr);
                                } else if (fnarr.length === 3) {
                                    $(idc + 3 + ' .middle_name').val(fnarr[1]);
                                    $(idc + 3 + ' .last_name').val(fnarr[2]);
                                } else if (fnarr.length === 2) {
                                    $(idc + 3 + ' .last_name').val(fnarr[1]);
                                }
                            }
                            $(idc + 1 + " .director-name").text(fnameInput1.value);
                            $(idc + 2 + " .director-name").text(fnameInput1.value);
                            $(idc + 3 + " .director-name").text(fnameInput1.value);
                            $(idc + 4 + " .director-name").text(fnameInput1.value);
//                    alert("5");
//                    $(idc + 1 + " .more-investor-fname").val(fnameInput1.value);
//                    $(idc + 1 + " .more-investor-email").val(emailInput1.value);
//                    $(idc + 1 + " .more-investor-radio").val(checkradio.value);
//                    $(idc + 1 + " .more-investor-preferred-name").val(fnameInput1.value);
                            $(idc + 2 + " .more-director-dob").datepicker({
                                yearRange: (year - 80) + ':' + year,
                                changeMonth: true,
                                changeYear: true,
                                'maxDate': new Date(adultDOB),
                                dateFormat: 'dd/mm/yy'
                            }).datepicker().attr('readonly', 'readonly');
                            $(idc + 3 + " .more-director-exp").datepicker({
                                yearRange: (year) + ':' + (year + 80),
                                changeMonth: true,
                                changeYear: true,
                                'minDate': new Date(),
                                dateFormat: 'dd/mm/yy'
                            }).datepicker().attr('readonly', 'readonly');
                            $(idc + 2 + " .more-director-countrycode").intlTelInput_1();
                            $(idc + 4 + " .more-director-countryname").intlTelInput_2();
                            $(idc + 3 + " .more-director-countryname").intlTelInput_2();
                            $(idc + 4 + " .excludenz").intlTelInput_3();
                            new google.maps.places.Autocomplete(
                                    ($(idc + 2 + " .more-director-address")[0]),
                                    {types: ['address'], componentRestrictions: {country: 'nz'}});
                            j++;
                        }
                    }
                    $("#step").attr("value", 3);
                    $("#step3").show();
                    $("#step2").hide();

                }
            });
            $(".next3").click(function () {
                var Date_of_Birth = $("#Date_of_Birth").val();
                Date_of_Birth = Date_of_Birth.trim();
                var OccupationOption = $('#step3 .OccupationOption option:selected').val();
                var Occupation = $("#cOccupation").val();
                Occupation = Occupation.trim();
                var holderCountryOfResidence = $("#holderCountryOfResidence").val();
                holderCountryOfResidence = holderCountryOfResidence.trim();
                var positionInCompany = $("#positionInCompany").val();
                positionInCompany = positionInCompany.trim();
                //                holderCountryOfResidence = holderCountryOfResidence.trim();
//                if (Date_of_Birth === "") {
//                    $("#spanDate_of_Birth").text("This field is required ");
//                } else if (OccupationOption === "-Select-" || OccupationOption === "0" && Occupation === "") {
//                    $("#spanOccupation").text("This field is required ");
//                } else if (holderCountryOfResidence === "–Select–") {
//                    $("#spanHolderCountryOfResidence").text("This field is required ");
//                } else if (positionInCompany === "-Select-") {
//                    $("#error-positionInCompany").text("This field is required ");
//                } else {
                    $("#step").attr("value", 4);
                    $("#step4").show();
                    $("#step3").hide();
//                }
            });
            $(".next4").click(function () {
                var address = $("#address").val();
                address = address.trim();
                var OPmobileNo = $("#mobileNo").val();
                var value = $(".mobile_number1").val();
                OPmobileNo = OPmobileNo.trim();
//                if (address === "") {
//                    $("#spanHomeaddress").text("This field is required ");
//                } else if (OPmobileNo === "") {
//                    $("#spanOPmobileNo").text("This field is required ");
//                } else if (value.length < 7) {
//                    $("#spanOPmobileNo").text("Please enter valid number ");
//                } else {
                    $("#step").attr("value", 5);
                    $("#step5").show();
                    $("#step4").hide();
//                }
            });
            $(".next5").click(function () {
                var verificationa = $("#verificationa").val();
                var text = $('.spanverification').html();
                verificationa = verificationa.trim();
//                if (verificationa === "") {
//                    $("#spanverificationa").text("This field is required ");
//                }
////                else if (text !== "Verification successful") {
////                    $("#spanverificationa").text("Invalid code");
//                }
//                else {
                    $("#step").attr("value", 6);
                    $("#step6").show();
                    $("#step5").hide();
//                }
            });
            $('#typeExpirydate').change(function () {
                $('#spanPExpirydate').text('');
            });
            $("#Countryofissue").change(function () {
                $('#spanCountryofissue').text('');
            });
            $(".next6").click(function () {
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var License_number = '';
                var licence_verson_number = '';
                var licence_expiry_Date = '';
                var passport_number = '';
                var passport_expiry = '';
                var index = $("#src_of_fund1 option:selected").val();
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = $("#licence_first_name").val().trim();
                    middleName = $("#licence_middle_name").val().trim();
                    lastName = $("#licence_last_name").val().trim();
                    License_number = $("#licenseNumber").val();
                    License_number = License_number.trim();
                    var Expirydate = $("#Expirydate").val();
                    Expirydate = Expirydate.trim();
                    licence_verson_number = $("#versionNumber").val();
                    licence_verson_number = licence_verson_number.trim();
//                    if (firstName === "") {
//                        $("#error_licence_first_name").text("This field is required ");
//                    } else if (lastName === "") {
//                        $("#error_licence_last_name").text("This field is required ");
//                    } else if (License_number === "") {
//                        $("#spanlicenseNumber").text("This field is required ");
//                    } else if (Expirydate === "") {
//                        $("#spanExpirydate").text("This field is required ");
//                    } else if (licence_verson_number === "") {
//                        $("#spanVersionnumber").text("This field is required ");
//                    } else {
                        $("#step").attr("value", 7);
                        $("#step7").show();
                        $("#step6").hide();
//                    }
                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = $("#passport_first_name").val().trim();
                    middleName = $("#passport_middle_name").val().trim();
                    lastName = $("#passport_last_name").val().trim();
                    passport_number = $("#Passportnumber").val();
                    passport_expiry = $("#PExpirydate").val();
                    var CountryOfIssue = $("#Countryofissue").val();
                    passport_number = passport_number.trim();
//                    if (firstName === "") {
//                        $("#error_passport_first_name").text("This field is required ");
//                    } else if (lastName === "") {
//                        $("#error_passport_last_name").text("This field is required ");
//                    } else if (passport_number === "") {
//                        $("#spanPassportnumber").text("This field is required ");
//                    } else if (passport_expiry === "") {
//                        $("#spanPExpirydate").text("This field is required ");
//                    } else if (CountryOfIssue === " –Select–") {
//                        $("#spanCountryofissue").text("This field is required ");
//                    } else {
                        $("#step").attr("value", 7);
                        $("#step7").show();
                        $("#step6").hide();
//                    }
                } else {

                    firstName = $("#other_first_name").val().trim();
                    middleName = $("#other_middle_name").val().trim();
                    lastName = $("#other_last_name").val().trim();
//                    alert(firstName +"  =="+ middleName+" " + lastName);
                    var TypeofID = $(".TypeofID").val();
                    var ExpirydateOther = $("#typeExpirydate").val();
                    var countryname1 = $("#countryname1").val();
                    var otherDocument = $("#otherDocument").val();
                    TypeofID = TypeofID.trim();
//                    if (TypeofID === "") {
//                        $("#spanTypeofID").text("This field is required ");
//                    } else if (firstName === "") {
//                        $("#error_other_first_name").text("This field is required ");
//                    } else if (lastName === "") {
//                        $("#error_other_last_name").text("This field is required ");
//                    } else if (ExpirydateOther === "") {
//                        $("#spantypeExpirydate").text("This field is required ");
//                    } else if (countryname1 === " –Select–") {
//                        $("#span_countryname1").text("This field is required ");
//                    } else if (otherDocument === "") {
//                        $("#span-otherDocument").text("Please attach the Document");
//                    } else {
                        $("#step").attr("value", 7);
                        $("#step7").show();
                        $("#step6").hide();
//                    }
                }
                var Title = $('.mainDirectorTitle option:selected').val();
//                 alert(Title);
                var Date_of_Birth = $('#Date_of_Birth').val();
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date, title: Title,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(DataObj);
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                $('#investor_verify').val('true');
                            } else {
                                $('#investor_verify').val('false');
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                $('#investor_verify').val('true');
                            } else {
                                $('#investor_verify').val('false');
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/pep-verification',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);

                        var obj = JSON.parse(data);
//                        alert(JSON.stringify(  obj.watchlistAML[0].verified));
//                        if (index === "1") {
                        if (obj.watchlistAML[0].verified) {
//                              
                            $('#mainDirectorPep_verify').val('true');
                        } else {
                            $('#mainDirectorPep_verify').val('false');
                        }
//                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });


            });
            $(".next7").click(function (event) {
                var j = 0;
                var i = $('#counter').val();
                var IRDNumber = $("#irdNumber").val().trim();
                var USCitizen = $("#USCitizen option:selected").val();
                var validate = validateIrd(IRDNumber);
                if (validate) {
                    if (USCitizen === "2") {
                        var tindivs = document.getElementsByClassName('checktindata');
                        for (var i = 0; i < tindivs.length; i++) {
                            var tindiv = tindivs[i];
                            var tinnum = tindiv.getElementsByClassName('TIN')[0];
                            var tinerror = tindiv.getElementsByClassName('tin-error')[0];
                            var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                            var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
                            var reason = tindiv.getElementsByClassName('resn_unavailable')[0];
                            var reasonerror = tindiv.getElementsByClassName('resn_unavailable-error')[0];
//                            if (country.value === "-Select-".trim().replace('-', '')) {
//                                countryerror.innerHTML = "This field is required ";
////                            alert("country span");
//                            } else if (tinnum.value === "" && reason.value === "") {
//                                tinerror.innerHTML = "This field is required ";
//                                reasonerror.innerHTML = "This field is required ";
//                            } else {
                                j++;
//                            }
                        }
                        if (tindivs.length > j) {
//                            alert("lenght grater");
                        } else {
                            tinfornext7(event.target);
                        }
                    } else {
                        tinfornext7(event.target);
                    }
                }

            });

            $(".next12").click(function () {

                var IRDNumber = $("#comIRDNumber").val().trim();
                var source = $("#sourceOfFunds option:selected").val();
                var sourceother = $("#sourceOfFunds_other").val();
//                var validate = companyValidateIrd(IRDNumber);
//                if (validate) {
//                    if (source === "1" || source === "7" && sourceother === "") {
//                        if (source === "1") {
//                            $("#sourceOfFunds-error").text("This field is required ");
//                        } else {
//                            $("#error_sourceOfFunds_other").text("This field is required ");
//                        }
//                    } else {
                        $("#step").attr("value", 13);
                        $("#step13").show();
                        $("#step12").hide();
//                    }
//                }

            });
            $(".next13").click(function () {

                var firstselect = $('#isCompanyFinancialInstitution option:selected').val();
                var secondselect = $('#isCompanyUSCitizen option:selected').val();
                var activepasive = $('#isCompanyActivePassive option:selected').val();
                var j = 0;
//                if (firstselect === "0" || firstselect === "1" && activepasive === "0") {
//                    if (firstselect === "0") {
//                        $("#isCompanyFinancialInstitution-error").text("This field is required ");
//                    } else {
//                        $("#isCompanyActivePassive-error").text("This field is required ");
//                    }
//                } else if (secondselect === "0") {
//                    $("#isCompanyUSCitizen-error").text("This field is required ");
//                } else 
                    if (secondselect === "2") {
                    var tindivs = document.getElementsByClassName('checktin4data');
                    for (var i = 0; i < tindivs.length; i++) {
                        var tindiv = tindivs[i];
                        var tinnum = tindiv.getElementsByClassName('companyTaxIdentityNumber')[0];
                        var tinerror = tindiv.getElementsByClassName('companyTaxIdentityNumber-error')[0];
                        var reason = tindiv.getElementsByClassName('companyReasonTIN')[0];
                        var reasonerror = tindiv.getElementsByClassName('companyReasonTIN-error')[0];
                        var country = tindiv.getElementsByClassName('countrynameexcludenz')[0];
                        var countryerror = tindiv.getElementsByClassName('countrynameexcludenz-error')[0];
//                        if (country.value === " -Select-") {
//                            countryerror.innerHTML = "This field is required ";
//                        } else if (tinnum.value === "" && reason.value === "") {
//                            tinerror.innerHTML = "This field is required ";
//                            reasonerror.innerHTML = "This field is required ";
//
//                        } else {
                            j++;
//                        }
                    }
                    if (tindivs.length > j) {

                    } else {
                        $("#step").attr("value", 14);
                        $("#step14").show();
                        $("#step13").hide();
                    }
                } else {
                    $("#step").attr("value", 14);
                    $("#step14").show();
                    $("#step13").hide();
                }

            });
            $('.bank_name').change(function () {
                var data = $('.bank_name option:selected').data('id');
                $('#accountNumber').val(data);
            });
            $(".next14").click(function () {
                var bank_name = $('.bank_name').val();
                bank_name = bank_name.trim();
                var other_name = $("#other_other_name").val();
                var accountNumber = $("#accountNumber").val().trim();
                var nameOfAccount = $("#nameOfAccount").val().trim();
                var attachbankfile = $("#attachbankfile").val().trim();
                var ifAcVErified = $('.isAccount_number_verified').val();
//                if (bank_name === "" || bank_name === "–Select–" || bank_name === "Other" && other_name === "") {
//                    if (bank_name === "–Select–" || bank_name === "") {
//                        $("#error_bank_name").text("This field is required ");
//                    } else {
//                        $("#error_other_bank_name").text("This field is required ");
//                    }
//                } else if (nameOfAccount === "") {
//                    $("#spannameOfAccount").text("This field is required ");
//                } else if (accountNumber === "") {
//                    $("#spanaccountNumber").text("This field is required ");
//                } else if (accountNumber.length !== 19 || ifAcVErified === "false") {
//                    $("#spanaccountNumber").text("Account number is invalid");
//                } else if (attachbankfile === "") {
//                    $(".error_attachbankfile").text("Please attach verification of your bank account.");
//                } else {
                    $("#step").attr("value", 15);
                    $("#step15").show();
                    $("#step14").hide();
//                }
            });
            $(".next15").click(function () {
                var y = $('input[name=authorized_condition]').is(':checked');
                var x = $('input[name=term_condition]').is(':checked');
//                if (!x) {
//                    $("#error_term_condition").text("Please read and agree to the Terms and Conditions");
//                } else if (!y) {
//                    $("#error_authorized_condition").text("I am authorised to act and accept on behalf of all account holders");
//                } else {
                    $("#step").attr("value", 15);
                    $("#step16").show();
                    $("#step15").hide();
//                }
            });
            $(".companyaccount").click(function () {
                x = $('[name="step"]').val();
                $("#step").attr("value", parseInt(x) + 1);
                $("#step10").show();
                $("#step2").hide();
            });
            $('.bank_name').change(function () {
                //                alert('other');
                if ($('.bank_name option:selected').val() === 'Other') {
                    $(".toggal_other").show();
                } else {
                    $(".toggal_other").hide();
                }
            });
        </script>
        <script>
            $('#sourceOfFunds').change(function () {
                var val = $("#sourceOfFunds option:selected").val();
                if (val === "7") {
                    $('.des-togle').show();
                }
                if (val === "6") {
                    $('.des-togle').hide();
                }
                if (val === "5") {
                    $('.des-togle').hide();
                }
                if (val === "4") {
                    $('.des-togle').hide();
                }
                if (val === "3") {
                    $('.des-togle').hide();
                }
                if (val === "2") {
                    $('.des-togle').hide();
                }
                if (val === "1") {
                    $('.des-togle').hide();
                }
                if (val === "0") {
                    $('.des-togle').hide();
                }
            });
            $('#src_of_fund1').change(function () {
                var val = $("#src_of_fund1 option:selected").val();
                if (val === "1") {
                    $('.verifybtn').show();
                    $('.passport-select').hide();
                    $('.other-select').hide();
                    $('.drivery-licence').show();
                }
                if (val === "2") {
                    $('.verifybtn').show();
                    $('.passport-select').show();
                    $('.other-select').hide();
                    $('.drivery-licence').hide();
                }
                if (val === "3") {
                    $('.verifybtn').hide();
                    $('.passport-select').hide();
                    $('.other-select').show();
                    $('.drivery-licence').hide();
                }
            });

            $('.selectoption1').change(function () {
                var val = $(".selectoption1 option:selected").val();
                if (val === "1") {
                    $('.yes-option').hide();
                }
                if (val === "2") {
                    $('.yes-option').show();
                }
            });

            $('.selectoption3').change(function () {
                var val = $(".selectoption3 option:selected").val();
                if (val === "7") {
                    $('.inputtype').show();
                } else {
                    $('.inputtype').hide();
                }

            });
            $('.selectoption6').change(function () {
                var val = $(".selectoption6 option:selected").val();
                if (val === "1") {
                    $('.selectno6').show();
                } else {
                    $('.selectno6').hide();
                }

            });
            $('.selectoption4').change(function () {
                var val = $(".selectoption4 option:selected").val();
                if (val === "1") {
                    $('.yes-option4').hide();
                }
                if (val === "2") {
                    $('.yes-option4').show();
                }

            });
        </script>
        <script>
            $(document).ready(function () {
                $("#add-country-another").click(function () {
                    $(".yes-new").append("<div class='row checktindata removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'> <span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='fullName' placeholder='Enter TIN' onkeyup='removetinspan()' ><span class='error tin-error' ></span> </div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field resn_unavailable' name='fullName' placeholder='' onkeyup='removetinspan();'/><span class='error resn_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
                $("#add-country-another4").click(function () {
                    $(".yes-new4").append("<div class='row checktin4data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN) </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyTaxIdentityNumber' name='fullName'  placeholder='Enter TIN' onkeyup='removetinspan()' /><span class='error companyTaxIdentityNumber-error' ></span></div><div class='col-sm-6'><label class='label_input'> Reason if TIN not available </label></div><div class='col-sm-6'><input type='text' class='form-control input-field companyReasonTIN' name='fullName'  placeholder='' onkeyup='removetinspan();'/><span class='error companyReasonTIN-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
                $("#add-country-another3").click(function () {
                    $(".yes-new3").append("<div class='row yes-new3-clone checktin3data removecountry'><div class='col-sm-6' ><label class='label_input' style='text-align:left'>Country of tax residence:</label></div><div class='col-sm-6 details-pos flag-drop' onclick='changecoutry(this);'><input type='text' class='form-control countrynameexcludenz more-investor-tex_residence_Country'  name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error countrynameexcludenz-error' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number (TIN)</label></div><div class='col-sm-6 '><input type='text' class='form-control input-field more-investor-TIN' name='fullName'  placeholder='Enter TIN' onkeyup='removetinspan()'/><span class='error more-investor-TIN-error' ></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available :</label></div><div class='col-sm-6'><input type='text' class='form-control input-field more-investor-resn_tin_unavailable' name='fullName'  placeholder='' onkeyup='removetinspan();'/><span class='error more-investor-resn_tin_unavailable-error' ></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    $(".countrynameexcludenz").intlTelInput_3();
                });
            });
        </script>
        <script>

            var current_director = {};
            $(document).ready(function () {
                var count = 1;
                $('#counter').val(count);
                $("#btn20").click(function () {
                    $(".director-section").append("<div class='row director-add removedirector'><div class='col-sm-4'><label class='label_input' style='text-align:left'>Full name of trustee/beneficial owner</label></div><div class='col-sm-2'><select class='selectoption option-add adjust-option directorTitle ' id=''><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option id='other' value='Other'>Master</option></select></div><div class='col-sm-4'><input type='text' class='fname input-field' id='fname' name='fullName' placeholder='Enter full name' onkeyup='removespan();'><span class='error spanfname' id='spanfname'></span><input type='hidden' class='cls-me' name='me' value='N'></div><div class='col-sm-2 this-space'><a href='javascript:void(0)' class='this-btn' onclick='addbtnfn(this);'>This is me</a><a href='javascript:void(0);' onclick='removedatadiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                    counter = $('#counter').val();
                    counter++;
                    $('#counter').val(counter);
                });
            });
            function removedatadiv(ele) {
                ele.closest('.removedirector').remove();
            }
            function removecountrydiv(ele) {
                ele.closest('.removecountry').remove();
            }
            function addbtnfn(ele) {
                var close = ele.closest('.director-add');
                var find = $(close).find('.fname');
                var btn = $(close).find('.this-btn');
                var cls_me = $(close).find('.cls-me');
                var name = find.val();
                $("a.this-btn").removeClass("check-this-btn");
                $('.cls-me').val('N');
                btn.addClass("check-this-btn");
                cls_me.val('Y');
            }
        </script>
        <script>
//            function checkird(obj) {
//                str = obj.value.replace('', '');
//                if (str.length > 10) {
//                    str = str.substr(0, 10);
//                } else
//                {
//                    str = str.replace(/\D+/g, "").replace(/([0-9]{3})([0-9]{3})([0-9]{2}$)/gi, "$1-$2-$3");
//                }
//                obj.value = str;
//                $('.error').text("");
//            }
            function checkird(obj, event) {
                $(" .ird-check").hide();
                if (event.charCode === 8 || event.charCode >= 48 && event.charCode <= 57) {
                    var ac = $(obj).val();
                    ac = ac.replace(/-/g, '');
                    if (ac.length > 2) {
                        var ac20 = ac.substr(0, 3);
                        var ac21 = ac.substr(3);
                        $(obj).val(ac20 + '-' + ac21);
                    }
                    if (ac.length > 5) {
                        var ac60 = ac.substr(0, 3);//4
                        var ac61 = ac.substr(3, 3);//4
                        var ac62 = ac.substr(6, 2);//3
                        $(obj).val(ac60 + '-' + ac61 + '-' + ac62);
                    }
                } else {
//                    alert(event.charCode);
                    event.preventDefault();
                }
                $('.error').text("");
            }
        </script>
        <script>
//            function checkAccountNO(obj) {
//                str = obj.value.replace('', '');
//                if (str.length > 18) {
//                    str = str.substr(0, 18);
//                } else
//                {
//                    str = str.replace(/([A-Za-z0-9]{2})([[A-Za-z0-9]{4})([[A-Za-z0-9]{7})([[A-Za-z0-9]{4}$)/gi, "$1-$2-$3-$4"); //mask numbers (xxx) xxx-xxxx    
//
//                }
//                obj.value = str;
//            }
            function checkAccountNO() {
                var ac = document.getElementById("accountNumber").value;
                if (ac.length === 2) {
                    var ac20 = ac.substr(0, 2);
                    var a = ac20 + '-';
                    $("#accountNumber").val(a);
                }
                if (ac.length === 7) {
                    var ac21 = ac.substr(7);
                    var b = ac + '-' + ac21;
                    $("#accountNumber").val(b);
                }
                if (ac.length === 15) {
                    var ac212 = ac.substr(15);
                    var c = ac + '-' + ac212;
                    $("#accountNumber").val(c);
                }
                if (ac.length > 18) {
                    var ac23 = ac.substr(0, 18);
                    var d = ac23;
                    $("#accountNumber").val(d);
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.advisor-show').hide();
            });
            $('.avisor-click').change(function () {
                var val = $(".avisor-click option:selected").val();
                if (val === "1") {
                    $('.advisor-show').hide();
                }
                if (val === "2") {
                    $('.advisor-show').show();
                }

            });
            function removespan() {
                $('.spanfname').text('');
            }
        </script>
        <script>

            $('#submit').click(function () {
                $(this).off();
                var status = "SUBMISSION";
//                saveData(status);
//                location.href = "https://www.mintasset.co.nz/";

            });
            $('.saveExit').click(function () {
                var status = "PENDING";
//                saveData(status);
                  window.location.href = "./login?logout";
            });
            saveData = function (status) {



                var index = $("#src_of_fund1 option:selected").val();
//                alert(index);
                var cls = "";
                if (index === "1") {
                    cls = '.drivery-licence ';
                }
                if (index === "2") {
                    cls = '.passport-select ';
                }
                if (index === "3") {
                    cls = '.other-select ';
                }
//                alert(cls + "2");
                var firstName = "", middleName = "", lastName = "";
                firstName = $(cls + '.first_name').val();
                middleName = $(cls + '.middle_name').val();
                lastName = $(cls + '.last_name').val();
//                alert(firstName + " ---" + middleName + " -- " + lastName);

//                  var firstName = $(".first_name").val().trim();
//                var middleName = $(".middle_name").val().trim();
//                var lastName = $(".last_name").val().trim();
//                


                var companyName = $('#companyName').val();
                var countryCode = $('#countryCode').val();
                var countryofincorporation = $('#Country_of_incorporation').val();
                var date_of_incorporation = $('#Date_of_incorporation').val();
                var registrationNumber = $('#registrationNumber').val();
                var companyAddress = $('#companyAddress').val();
                var postalAddress = $('#postalAddress').val();
                var typeOfTrust = $('.typeOfTrust option:selected').text();
                var investmentAdviser = $('#investmentAdviser option:selected').text();
                var companyAdvisor = $('#companyAdvisor option:selected').text();
                var selectAdviser = $('#selectAnAdviser option:selected').text();
                if (investmentAdviser === "No") {
                    companyAdvisor = "No";
                    selectAdviser = "No";
                } else {
                    companyAdvisor = $('#companyAdvisor option:selected').text();
                    selectAdviser = $('#selectAnAdviser option:selected').text();
                }
                var fname = $('#fname').val(); // it may be multiple name
                var holder_email = $('#email').val(); // it may be multiple name
                var preferred_name = $('#preffered_name').val();
                var date_of_Birth = $('#Date_of_Birth').val();
                var OccupationOption = $('#step3 .Occupation option:selected').val();
                var occupation = $('#cOccupation').text();
                if (OccupationOption !== "0") {
                    occupation = $('#step3 .Occupation option:selected').text();
                }
                if (occupation === " –Select–" || occupation === "–Select–") {
                    occupation = "";
                }

                var holderCountryOfResidence = $('#holderCountryOfResidence').val();
                var positionInCompany = $('#positionInCompany option:selected').val();
//                    alert(positionInCompany);
                var address = $('#address').val();
                var countryCode3 = $('#countryCode3').val();
                var mobileNo = $('#mobileNo').val();
                var otherNumber = $('#otherNumber option:selected').text();
//                    alert("otherNumber" + otherNumber);
                var otherCountryCode = $('#countryCode').val();
                var otherMobileNo = $('#mobileNo2').val();
                if (otherMobileNo === "") {
                    otherCountryCode = "";
                    otherNumber = "";
                }
                var src_of_fund2 = $('#src_of_fund1 option:selected').text();
                var licenseNumber = $('#licenseNumber').val(); //for license ID
                var licenseExpiryDate = $('#Expirydate').val(); //for license ID
//                alert(licenseExpiryDate);
                var versionNumber = $('#versionNumber').val();
//                    alert("versionNumber" + versionNumber);//for license ID
                var passportNumber = $('#passportNumber').val(); //for passport ID
                var passportExpiryDate = $('#passportExpiryDate').val(); //for passport ID
                var countryOfIssue = $('#countryOfIssue').val(); //for passport ID
                var investor_verify = $('#investor_verify').val();
                var mainDirectorPep_verify = $('#mainDirectorPep_verify').val();
//                var typeOfID = $('#TypeOfID').val(); 
                var typeOfID = $(".TypeofID").val();//for Other ID
//                alert(typeOfID);
                var typeExpiryDate = $('.otherIdExp').val(); //for Other ID
                var typeCountryOfIssue = $('#typeCountryOfIssue').val(); //for Other ID 
                var myFile = $('#myFile').val();
                //  Please enter the tax details
                var countryOfResidence = $('#holderCountryOfResidence').val();
                if (countryOfResidence === " –Select–" || countryOfResidence === "–Select–") {
                    countryOfResidence = "";
                }
                var irdNumber = $('#irdNumber').val();
//                irdNumber = irdNumber.replace(/-/g, '');
                var citizenUS = $('#citizenUS option:selected').text(); // if citizen yes
                var countryOfTaxResidence = $('#countryOfTaxResidence').val(); //repeat multiple time 

                if (countryOfTaxResidence === " -Select-" || countryOfTaxResidence === "-Select-") {
                    countryOfTaxResidence = "";
                }
                var taxIdenityNumber = $('#taxIdenityNumber').val();                              //repeat multiple time
                //                var reasonTIN = $('#reasonTIN').val();                          //repeat multiple time
                var pir = $('#pir option:selected').text();
                var companyIRDNumber = $('#comIRDNumber').val();
//                 companyIRDNumber = companyIRDNumber.replace(/-/g, '');
                var sourceOfFunds = $('#sourceOfFunds').val();
                var sourceOfFundsText = $('#sourceOfFunds option:selected').text();
                var detail = $('#detail').val();
                var isCompanyListed = $('#isCompanyListed option:selected').text();
                var isCompanyGovernment = $('#isCompanyGovernment option:selected').text();
                var isCompanyNZPolice = $('#isCompanyNZPolice option:selected').text();
                var isCompanyFinancialInstitution = $('#isCompanyFinancialInstitution option:selected').text();
                var isCompanyActivePassive = $('#isCompanyActivePassive option:selected').text();
                if (isCompanyFinancialInstitution === " –Select–" || isCompanyFinancialInstitution === "–Select–") {
                    isCompanyFinancialInstitution = "";
                } else if (isCompanyFinancialInstitution === "No") {
                    isCompanyActivePassive = "No";
                } else {
                    isCompanyActivePassive = $('#isCompanyActivePassive option:selected').text();
                }
                var isCompanyUSCitizen = $('#isCompanyUSCitizen option:selected').text(); // if citizen yes
                if (isCompanyUSCitizen === " –Select–" || isCompanyUSCitizen === "–Select–") {
                    isCompanyUSCitizen = "";
                }
                var companyCountryOfTaxResidence = $('#companyCountryOfTaxResidence').val(); //repeat multiple time 
                if (companyCountryOfTaxResidence === " -Select-" || companyCountryOfTaxResidence === "-Select-") {
                    companyCountryOfTaxResidence = "";
                }
                var companyTaxIdentityNumber = $('#companyTaxIdentityNumber').val(); //repeat multiple time
                var companyReasonTIN = $('#companyReasonTIN').val(); //repeat multiple time
                var bankName = $('#bank_name').val();
//                alert(bankName);
                if (bankName === "Other") {
                    bankName = $('.Other_Bank_name').val();
                }
                var nameOfAccount = $('#nameOfAccount').val();
                var accountNumber = $('#accountNumber').val();
                var step = $("#step").val();
                var reg_type = $('[name="register_type"]').val();
                var reg_id = '${company.reg_id}';
                var id = '${company.id}';
                var otherIdIssuedBy = $("#countryname1").val();
//                  alert(otherIdIssuedBy +"------------- and expiry is "+typeExpiryDate );
                var email = '${company.email}';
                var raw_password = '${company.raw_password}';

                var Title = $('.mainDirectorTitle option:selected').val();
//                var firstName = $(".first_name").val().trim();
//                var middleName = $(".middle_name").val().trim();
//                var lastName = $(".last_name").val().trim();
                var countryArr = document.getElementsByClassName('tex_residence_Country');
                var TINArr = document.getElementsByClassName('TIN');
//                var reasonArr = document.getElementsByClassName('resn_tin_unavailable');
                var countryTINList = new Array();
                for (var i = 0; i < countryArr.length; i++) {
                    var country = countryArr[i].value;
                    if (country === " -Select-" || country === "-Select-") {
                        country = "";
                    }
                    var tin = TINArr[i].value;
//                    var reason = reasonArr[i].value;
                    countryTINList.push({country: country, tin: tin});
                }
                var moreInvestorInfos = getMoreInvestorArr();
                var DataObj = {id: id, raw_password: raw_password, email: email, companyName: companyName, countryCode: countryCode, dateOfInco: date_of_incorporation, otherIdIssuedBy: otherIdIssuedBy, title: Title,
                    registerNumber: registrationNumber, companyAddress: companyAddress, postalAddress: postalAddress, investAdviser: investmentAdviser,
                    companyAdvisor: companyAdvisor, selectAdviser: selectAdviser, fname: fname, holder_email: holder_email, dateOfBirth: date_of_Birth,
                    occupation: occupation, holderCountryOfResidence: holderCountryOfResidence, positionInCompany: positionInCompany, address: address,
                    countryCode3: countryCode3, mobileNo: mobileNo, otherNumber: otherNumber, otherCountryCode: otherCountryCode, typeOfTrust: typeOfTrust,
                    otherMobileNo: otherMobileNo, srcOfFund: src_of_fund2, licenseNumber: licenseNumber, sourceOfFundsText: sourceOfFundsText,
                    licenseExpiryDate: licenseExpiryDate, versionNumber: versionNumber, passportNumber: passportNumber, preferred_name: preferred_name,
                    passportExpiryDate: passportExpiryDate, countryOfIssue: countryOfIssue, typeOfID: typeOfID, typeExpiryDate: typeExpiryDate,
                    typeCountryOfIssue: typeCountryOfIssue, myFile: myFile, countryOfResidence: countryOfResidence, irdNumber: irdNumber, citizenUS: citizenUS, isInvestor_idverified: investor_verify, pepStatus: mainDirectorPep_verify,
                    countryOfTaxResidence: countryOfTaxResidence, taxIdentityNumber: taxIdenityNumber, pir: pir, firstName: firstName, lastName: lastName, middleName: middleName,
                    companyIRDNumber: companyIRDNumber, sourceOfFunds: sourceOfFunds, detail: detail, bankFile: bankFile, otherFile: otherFile, deedFile: deedFile,
                    isCompanyListed: isCompanyListed, isCompanyGovernment: isCompanyGovernment, isCompanyNZPolice: isCompanyNZPolice,
                    isCompanyFinancialInstitution: isCompanyFinancialInstitution, isCompanyActivePassive: isCompanyActivePassive,
                    isCompanyUSCitizen: isCompanyUSCitizen, companyCountryOfTaxResidence: companyCountryOfTaxResidence, companyTaxIdentityNumber: companyTaxIdentityNumber,
                    companyReasonTIN: companyReasonTIN, countryofincorporation: countryofincorporation, bankName: bankName,
                    nameOfAccount: nameOfAccount, accountNumber: accountNumber, step: step, status: status, reg_type: reg_type, reg_id: reg_id, countryTINList: countryTINList, moreInvestorList: moreInvestorInfos};
//                alert(JSON.stringify(DataObj));
//                swal({title: "",
//                    text: "Application data being saved...  ",
//                    type: "success",
//                    timer: 3500,
//                    showConfirmButton: false
//                });
                console.log(JSON.stringify(DataObj));
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/company-registeration',
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        swal({
                            title: "Complete",
                            text: "Your application data has been saved. To resume your application, please see the email we have now sent you. ",
                            type: "success",
//                            timer: 2500,
                            showConfirmButton: false
                        }, function () {
                            if (status === "SUBMISSION") {
                                location.href = "https://www.mintasset.co.nz/";
                            } else {
                                location.href = "./login?logout";
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            };
        </script>
        <script>
            function tinfornext7(ele) {
                var directorDivs1 = document.getElementsByClassName('director-add');
                if (directorDivs1.length > 1) {
                    $("#step").attr("value", 8);
                    next(ele);
                } else {
                    $("#step").attr("value", 12);
                    $("#step12").show();
                    $("#step7").hide();
                }
            }

            function removetinspan() {
                $('.error').text('');
            }
        </script>
        <script>
            $('.checkname').change(function () {
                console.log("daya");
                var root = $(this).closest('.closestcls');
                var name = $(this).val();
                var a = this.files[0].size;
                var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
//                if (!allowedExtensions.exec(name)) {
//                    root.find('.error').text("File format not allowed (only images and pdf files are allowed)");
//                    root.find(".removefile").hide();
//                    root.find('.shownamedata').text("");
//                    root.find('.checkname').val('');
//                } else {
//                    if (a < 2008576) {
                        var filename = name.split('\\').pop().split('/').pop();
                        filename = filename.substring(0, filename.lastIndexOf('.'));
                        console.log(name);
                        root.find('.shownamedata').text("File: " + filename);
                        root.find(".removefile").show();
//                    } else {
//                        root.find('.error').text("Max. File size 2 Mb");
//                        root.find(".removefile").hide();
//                        root.find('.shownamedata').text("");
//                        root.find('.checkname').val('');
//                    }
//                }
            });
            function checkname(ele) {
                console.log("daya");
                var root = ele.closest('.closestcls');
                var name = ele.value;
                var filename = name.split('\\').pop().split('/').pop();
                filename = filename.substring(0, filename.lastIndexOf('.'));
                console.log(name);
                var a = ele.files[0].size;
                var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
//                if (!allowedExtensions.exec(name)) {
//                    $(root).find('.error').text("File format not allowed (only images and pdf files are allowed)");
//                    $(root).find(".removefile").hide();
//                    $(root).find('.shownamedata').text("");
//                    $(root).find('.checkname').val('');
//                } else {
//                    if (a < 2008576) {
                        $(root).find('.shownamedata').text("File: " + filename);
                        $(root).find(".removefile").show();
                        $(root).find('.error').text("");
//                    } else {
//                        $(root).find('.error').text("Max. File size 2 Mb");
//                        $(root).find(".removefile").hide();
//                        $(root).find('.shownamedata').text("");
//                        $(root).find('.checkname').val('');
//                    }
//                }
            }
            function  removedatafile(ele) {
                var root = ele.closest('.closestcls');
                $(root).find('.shownamedata').text("");
                $(root).find('.checkname').val("");
                $(root).find(".removefile").hide();
            }
            $("#step3 .Occupation").change(function () {
                var OccupationOption = $('#step3 .Occupation option:selected').val();
                if (OccupationOption === "0") {
                    $("#step3 .selectOcc").show();
                    $("#step3 .otherOcc").show();
                } else {
                    $("#step3 .selectOcc").show();
                    $("#step3 .otherOcc").hide();
                }
            });
        </script>

        <script>
            $(".verify-dl").click(function () {
                var x = $(this).closest(".content-section");
                var index = x.find(".Id_Type option:selected").val();
                var s = $(x).find('.which_container').val();
                var url = '';
                var firstName = '';
                var middleName = '';
                var lastName = '';
                var Date_of_Birth = '';
                if (index === "1") {
                    url = './rest/groot/db/api/dl-verification';
                    firstName = x.find('input[name=licence_first_name]').val();
                    middleName = x.find('input[name=licence_middle_name]').val();
                    lastName = x.find('input[name="licence_last_name"]').val();
                    var License_number = x.find('input[name="license_number"]').val();
//                    var License_number = $('.License_number').val();
                    var licence_expiry_Date = x.find($('.lic_expiry_Date')).val();
                    var licence_verson_number = x.find($('.lic_verson_number')).val();
//                    if (firstName === "") {
//
//                        if (s === "#Date_of_Birth") {
//
//                            $("#error_licence_first_name").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more_error_licence_first_name").text("This field is required ");
//                            return false;
//                        }
//                    } else if (License_number === "") {
//                        if (s === "#Date_of_Birth") {
//                            $("#spanlicenseNumber").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more-investor-licenseNumber-error").text("This field is required ");
//                            return false;
//                        }
//                    } else if (licence_verson_number === "") {
//                        if (s === "#Date_of_Birth") {
//                            $("#spanVersionnumber").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more-investor-versionNumber-error").text("This field is required ");
//                            return false;
//                        }
//
//                    } else if (lastName === "") {
//                        if (s === "#Date_of_Birth") {
//                            $("#error_licence_last_name").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more_error_licence_last_name").text("This field is required ");
//                            return false;
//                        }
//                    }

                } else if (index === "2") {
                    url = './rest/groot/db/api/pp-verification';
                    firstName = x.find('input[name=passport_first_name]').val();
                    middleName = x.find('input[name=passport_middle_name]').val();
                    lastName = x.find('input[name="passport_last_name"]').val();
                    var passport_number = x.find('input[name="passport_number"]').val();
//                    var passport_expiry =  "2017-10-10";
                    var passport_expiry = x.find('.pass_expiry').val();
                    var dob2 = x.find('.pass_expiry').val();
                    dob2 = dob2.trim();
                    passport_number = passport_number.trim();
//                    if (firstName === "") {
//                        if (s === "#Date_of_Birth") {
//
//                            $("#error_passport_first_name").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more_error_passport_first_name").text("This field is required ");
//                            return false;
//                        }
//                    } else if (passport_number === "") {
//                        if (s === "#Date_of_Birth") {
//
//                            $("#spanPassportnumber").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more-investor-passportNumber-error").text("This field is required ");
//                            return false;
//                        }
//
//                    } else if (passport_expiry === "") {
//                        if (s === "#Date_of_Birth") {
//
//                            $("#spanPExpirydate").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more-investor-passportExpiryDate-error").text("This field is required ");
//                            return false;
//                        }
//
//                    } else if (lastName === "") {
//                        if (s === "#Date_of_Birth") {
//
//                            $("#error_passport_last_name").text("This field is required ");
//                            return false;
//                        } else if (s === "#more-investor-dob") {
//                            $("#more_error_passport_last_name").text("This field is required ");
//                            return false;
//                        }
//                    }
                }
                Date_of_Birth = $(s).val();
//                alert(Date_of_Birth);
                var DataObj = {license_number: License_number, licence_expiry_Date: licence_expiry_Date,
                    licence_verson_number: licence_verson_number, date_of_Birth: Date_of_Birth, firstName: firstName,
                    middleName: middleName, lastName: lastName, passport_number: passport_number, passport_expiry: passport_expiry};
                console.log(JSON.stringify(DataObj));
//                alert(JSON.stringify(DataObj));
                $.ajax({
                    type: 'POST',
                    url: url,
                    headers: {"Content-Type": 'application/json'},
                    data: JSON.stringify(DataObj),
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        var obj = JSON.parse(data);
                        if (index === "1") {
                            if (obj.driversLicence.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
//                                alert("wrong data");
                            }
                        } else if (index === "2") {
                            if (obj.passport.verified) {
                                swal({
                                    title: "Success",
                                    text: "Data is Verified.",
                                    type: "info",
                                    timer: 3500,
                                    showConfirmButton: true
                                });
                            } else {
//                                alert("wrong data");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(" inside error" + jqXHR);
                    }
                });
            });
            $('.country').click(function () {
                $('.error').text("");
            });
            function changecoutry(ele) {
                ele.getElementsByClassName('error')[0].innerHTML = "";
            }
            function changeFund(ele) {
                var root = $(ele).closest('.morestep3');
                console.log(root);
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .other-select1').hide();
                    $(idc + ' .drivery-licence1').show();
                    $(idc + ' .passport-select1').hide();
                } else if (val === "2") {
                    $(idc + ' .passport-select1').show();
                    $(idc + ' .other-select1').hide();
                    $(idc + ' .drivery-licence1').hide();
                } else if (val === "3") {
                    $(idc + ' .passport-select1').hide();
                    $(idc + ' .other-select1').show();
                    $(idc + ' .drivery-licence1').hide();
                }
                $('.error').text("");
            }
            function changeCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var val = ele.value;
                var idc = "#" + (root.attr("id"));
                if (val === "1") {
                    $(idc + ' .yes-option1').hide();
                }
                if (val === "2") {
                    $(idc + ' .yes-option1').show();
                }
                $('.error').text("");
            }
            function addAnotherCountry(ele) {
                var root = $(ele).closest('.morestep4');
                var idc = "#" + (root.attr("id"));
                $(idc + " .yes-new3").append("<div class='row checktindata removecountry'><div class='col-sm-6'><label class='label_input' style='text-align:left'>Country of tax residence</label></div><div class='col-sm-6 details-pos flag-drop'><input type='text' class='form-control excludenz tex_residence_Country'  id='countryname' name='countryCode' placeholder='Enter Country Code' readonly='readonly'><span class='error error-tex-residence-Country' ></span></div><div class='col-sm-6'><label class='label_input'>Tax Identification Number </label></div><div class='col-sm-6'><input type='text' class='form-control input-field TIN' name='tin' required='required' placeholder='Enter TIN' onkeyup='removerror()' /><span class='error error-TIN'></span></div><div class='col-sm-6'><label class='label_input'>Reason if TIN not available </label></div><div class='col-sm-6'> <input type='text' class='form-control input-field resn_tin_unavailable'  name='tin' required='required' onkeyup='removerror()'/><span class='error error-resn-tin-unavailable' id='error-resn-tin-unavailable'></span><a href='javascript:void(0);' onclick='removecountrydiv(this);'><i class='far fa-times-circle'></i></a></div></div>");
                $(idc + " .excludenz").intlTelInput_3();
                $('.error').text("");
            }
            function chnageoccupation(ele) {
                var root = $(ele).closest('.morestep2');
                var idc = "#" + (root.attr("id"));
                var val = $(idc + ' .selectOcc option:selected').val();
                if (val === "0") {
                    $(idc + ' .otherOcc').show();
                } else {
                    $(idc + ' .otherOcc').hide();
                }
                $('.error').text("");

            }
            $("#step1 .selectType").change(function () {
                var trustType = $("#step1 .selectType option:selected").val();
                if (trustType === "4") {
                    $("#step1 .selectType").show();
                    $("#step1 .otherType").show();
                } else {
                    $("#step1 .selectType").show();
                    $("#step1 .otherType").hide();
                }
                $('.error').text("");
            });
        </script>
        <!----------------------bank btn start------------------------->
        <script>
            function moreValidateIrd(idc, irdNumber, errorIrdNumber) {
                const irdToUse = irdNumber.replace(/-/g, '');
                var returnval = false;
//                if (irdToUse.length !== 9) {
//                    errorIrdNumber.text("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
//                } else if (irdToUse < 10000000 || irdToUse > 150000000) {
//                    errorIrdNumber.text("IRD number must be in the range of 10000000 - 150000000");
//                } else {
//                var originalIrd = irdToUse.padStart(9, '0');
                    var originalIrd = irdToUse;
                    var count = 0;
                    var irdFirstWeight = '32765432';
                    getCheckDigit(originalIrd, irdFirstWeight);
                    function getCheckDigit(originalIrd, irdWeight) {
                        var ird = originalIrd.substr(0, originalIrd.length - 1);
                        var lastDigit = originalIrd.substr(-1);
                        var total = 0;
                        for (var i = 0; i < ird.length; i++) {
                            var j = i + 1;
                            var irdDigit = ird.substring(i, j);
                            var irdWeightDigit = irdWeight.substring(i, j);
                            total = total + (irdDigit * irdWeightDigit);
                        }
                        var remainder = total % 11;
//                        if (remainder === 0) {
//                            $(idc + " .ird-check").show();
//                            returnval = true;
//                        } else {
//                            var calCheck = 11 - remainder;
//                            if (calCheck >= 0 && calCheck <= 9) {
//                                if (calCheck === parseInt(lastDigit)) {
//                                    $(idc + " .ird-check").show();
//
//                                    returnval = true;
//
//                                } else {
//                                    errorIrdNumber.text("IRD Number is Invalid");
//                                    returnval = false;
//                                }
//                            } else {
//                                var irdSecondWeight = '74325276';
//                                count++;
//                                if (count === 1) {
//                                    getCheckDigit(originalIrd, irdSecondWeight);
//                                }
//                                errorIrdNumber.text("IRD Number is Invalid");
//                                returnval = false;
//                            }
//                        }
//                    }
                }
                return returnval;
            }
            function validateIrd(irdNumber) {
                const irdToUse = irdNumber.replace(/-/g, '');
//                var returnval = false;
////                if (irdToUse.length !== 9) {
////                    $("#spanIRDNumber").text("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
////                } else if (irdToUse < 10000000 || irdToUse > 150000000) {
////                    $("#spanIRDNumber").text("IRD number must be in the range of 10000000 - 150000000");
////                } else {
////                var originalIrd = irdToUse.padStart(9, '0');
//                    var originalIrd = irdToUse;
//                    var count = 0;
//                    var irdFirstWeight = '32765432';
//                    getCheckDigit(originalIrd, irdFirstWeight);
//                    function getCheckDigit(originalIrd, irdWeight) {
//                        var ird = originalIrd.substr(0, originalIrd.length - 1);
//                        var lastDigit = originalIrd.substr(-1);
//                        var total = 0;
//                        for (var i = 0; i < ird.length; i++) {
//                            var j = i + 1;
//                            var irdDigit = ird.substring(i, j);
//                            var irdWeightDigit = irdWeight.substring(i, j);
//                            total = total + (irdDigit * irdWeightDigit);
//                        }
//                        var remainder = total % 11;
//                        if (remainder === 0) {
//                            $("#step7 .ird-check").show();
//                            returnval = true;
//                        } else {
//                            var calCheck = 11 - remainder;
//                            if (calCheck >= 0 && calCheck <= 9) {
//                                if (calCheck === parseInt(lastDigit)) {
//                                    $("#step7 .ird-check").show();
//                                    returnval = true;
//                                } else {
//                                    $("#spanIRDNumber").text("IRD Number is Invalid");
//                                    returnval = false;
//                                }
//                            } else {
//                                var irdSecondWeight = '74325276';
//                                count++;
//                                if (count === 1) {
//                                    getCheckDigit(originalIrd, irdSecondWeight);
//                                }
//                                $("#spanIRDNumber").text("IRD Number is Invalid");
//                                returnval = false;
//                            }
//                        }
//                    }
//                }
                return true;

            }
            function companyValidateIrd(irdNumber) {
                const irdToUse = irdNumber.replace(/-/g, '');
                var returnval = false;
//                if (irdToUse.length !== 9) {
//                    $("#spancomIRDNumber").text("IRD number must contain 9 digits (if less than this, enter a 0 for the first number on the left)");
//                } else if (irdToUse < 10000000 || irdToUse > 150000000) {
//                    $("#spancomIRDNumber").text("IRD number must be in the range of 10000000 - 150000000");
//                } else {
////                var originalIrd = irdToUse.padStart(9, '0');
//                    var originalIrd = irdToUse;
//                    var count = 0;
//                    var irdFirstWeight = '32765432';
//                    getCheckDigit(originalIrd, irdFirstWeight);
//                    function getCheckDigit(originalIrd, irdWeight) {
//                        var ird = originalIrd.substr(0, originalIrd.length - 1);
//                        var lastDigit = originalIrd.substr(-1);
//                        var total = 0;
//                        for (var i = 0; i < ird.length; i++) {
//                            var j = i + 1;
//                            var irdDigit = ird.substring(i, j);
//                            var irdWeightDigit = irdWeight.substring(i, j);
//                            total = total + (irdDigit * irdWeightDigit);
//                        }
//                        var remainder = total % 11;
//                        if (remainder === 0) {
//                            $("#step12 .ird-check").show();
//                            returnval = true;
//                        } else {
//                            var calCheck = 11 - remainder;
//                            if (calCheck >= 0 && calCheck <= 9) {
//                                if (calCheck === parseInt(lastDigit)) {
//                                    $("#step12 .ird-check").show();
//                                    returnval = true;
//                                } else {
//                                    $("#spancomIRDNumber").text("IRD Number is Invalid");
//                                    returnval = false;
//                                }
//                            } else {
//                                var irdSecondWeight = '74325276';
//                                count++;
//                                if (count === 1) {
//                                    getCheckDigit(originalIrd, irdSecondWeight);
//                                }
//                                $("#spancomIRDNumber").text("IRD Number is Invalid");
//                                returnval = false;
//                            }
//                        }
//                    }
//                }
                return returnval;

            }
            $(".select li").click(function () {

                $("#error_bank_name").text('');
                var a = $(this).data('id');
                var b = $(this).data('value');
                $('#accountNumber').val(a);
                $('#bank_name').val(b);
//                alert(b);
                if (b === 'Other') {
                    $('.toggal_other').show();
                } else {
                    $('.toggal_other').hide();
                }
            });

            $('.textfirst').click(function () {
                $('#bank_name').val("");

            });





            $(function () {
                // Set
                var main = $('div.mm-dropdown .textfirst')
                var li = $('div.mm-dropdown > ul > li.input-option')
                var inputoption = $("div.mm-dropdown .option")
                var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="10" height="10" class="down" />';

                // Animation
                main.click(function () {
                    main.html(default_text);
                    li.toggle('fast');
                });

                // Insert Data
                li.click(function () {
                    // hide
                    li.toggle('fast');
                    var livalue = $(this).data('value');
                    var lihtml = $(this).html();
                    main.html(lihtml);
                    inputoption.val(livalue);
                });
            });
        </script>
        <!----------------------bank btn end------------------------->


        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
    </body>
</html>														