
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
        <style>
            .profile-weight {
    font-weight: 500;
}
            .tabpill-margin {
                margin-top: 20px;
            }
            .image-select {
                width: 100%;
                font-size: 13px;
                position: relative;
                top: -5px;
            }
            .profile-pic {
    max-width: 200px;
    max-height: 200px;
    display: block;
    width: 100%;
        height: 100%;
    object-fit: cover;
}

.file-upload {
    display: none;
}
.profile-circle {
    border-radius: 1000px !important;
    overflow: hidden;
    width: 200px;
    height: 200px;
    border: 8px solid rgb(147, 219, 208);
        max-width: 200px;
    margin: auto;
   
}
.p-image {
    position: relative;
    top: -36px;
    left: 209px;
    color: #666666;
    transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
  font-size: 1.2em;
}

.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
.emp-profile {
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                         <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Profile</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="height:100vh">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container emp-profile">
                                        <form method="post">
                                            <div class="row">

                                                <div class="col-md-4">
                                                    
                                                    <div class="profile-circle">
                                                        <!-- User Profile Image -->
                                                        <img class="profile-pic" src="https://backoffice.invsta.io/pocv/resources/img/man.jpg">

                                                        <!-- Default Image -->
                                                        <!-- <i class="fa fa-user fa-5x"></i> -->
                                                    </div>
                                                    <div class="p-image">
                                                        <i class="fa fa-camera upload-button"></i>
                                                        <input class="file-upload" type="file" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="profile-head">
                                                        <h5 class="ben-Name">
                                                        </h5>
                                                        <!--                                                        <h6 class="ben-AMLEntityType">
                                                                                                                </h6>-->
                                                        <!--<p class="proile-rating">RANKINGS : <span>10/10</span></p>-->
                                                        <ul class="nav nav-tabs profile-weight" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Address</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="bank-tab" data-toggle="tab" href="#bank" role="tab" aria-controls="bank" aria-selected="false">Bank Details</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content profile-tab tabpill-margin profile-weight" id="myTabContent">
                                                            <div class="tab-pane fade  active in" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>User Id :</label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p id="ben-Id"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Name :</label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p class="ben-Name"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="email-row">
                                                                    <div class="col-sm-6">
                                                                        <label>Email :</label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p id="ben-Email"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="phone-row">
                                                                    <div class="col-sm-6">
                                                                        <label>Phone :</label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p id="ben-PhoneNumber"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="phone-row">
                                                                    <div class="col-sm-6">
                                                                        <label>Date Of Birth :</label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <p id="ben-DateOfBirth"></p>
                                                                    </div>
                                                                </div>
                                                                <!--                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Access Level :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="ben-AccessLevel"></p>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>AML Entity Type :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p class="ben-AMLEntityType"></p>
                                                                                                                                </div>
                                                                                                                            </div>-->
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Status :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="ben-Status"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                                <!--                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Address ID :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="AddressID"></p>
                                                                                                                                </div>
                                                                                                                            </div>-->
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Country :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="Country"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Country Code :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="CountryCode"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Type :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="Type"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Address Line 1 :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="AddressLine1"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Address Line 2 :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="AddressLine2"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Address Line 3 :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="AddressLine3"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Address Line 4 :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="AddressLine4"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>City :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="City"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Region :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="Region"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>State :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="State"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Postal Code :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="PostalCode"></p>
                                                                    </div>
                                                                </div>
                                                                <!--                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Formatted Address :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="FormattedAddress"></p>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Is Primary :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="IsPrimary"></p>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                -->                                                            <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Effective Date :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="EffectiveDate"></p>
                                                                    </div>
                                                                </div><!--
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>In Effective Date :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="InEffectiveDate"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <label>Your Bio</label><br/>
                                                                        <p>Your detail description</p>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                            <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Account Name :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-AccountName"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Bank :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-Bank"> </p>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="email-row">
                                                                    <div class="col-md-6">
                                                                        <label>Branch :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-Branch"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="phone-row">
                                                                    <div class="col-md-6">
                                                                        <label>Account :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-Account"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Suffix :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-Suffix"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label>Currency :</label>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p id="bank-Currency"></p>
                                                                    </div>
                                                                </div>
                                                                <!--                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Status :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="bank-Status"></p>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Type :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="bank-Type"></p>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <label>Is Primary :</label>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-6">
                                                                                                                                    <p id="bank-IsPrimary"></p>
                                                                                                                                </div>
                                                                                                                            </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-2">
                                                                                                    <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
                                                                                                </div>-->
                                            </div>
                                            <div class="row">

                                                <div class="col-md-8">

                                                </div>
                                            </div>
                                        </form>   
                                        <!--                                        <table id="beni-table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                                                       data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th data-checkbox="true"></th>
                                                                                            <th data-field="title" data-formatter=""><b>Title</b></th>
                                                                                            <th data-field="RegistryAccountId" ><span >Registry Account Id</span></th>
                                                                                            <th data-field="Code" ><span >Code</span></th>
                                                                                            <th data-field="ContractDate" ><span>Contract Date</span></th>
                                                                                            <th data-field="InvestmentType" ><span>Investment Type</span></th>
                                                                                            <th data-field="InvestmentName" ><span>Investment Name</span></th>
                                                                                            <th data-field="Status">Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
    <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
    <!-- <script src="resources/bower_components/moment/moment.js" ></script>
    
    <script src="resources/bower_components/ckeditor/ckeditor.js" ></script>
    <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" ></script>
    <script src="resources/bower_components/dropzone/dist/dropzone.js" ></script> -->

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
    <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/alert.js" ></script>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/button.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/carousel.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/modal.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tab.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" ></script>
    <script src="resources/bower_components/bootstrap/js/dist/popover.js" ></script> -->
    <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
    <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>

    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
    <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
    <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
    <script src="https://code.highcharts.com/highcharts.js" ></script> 
    <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
    <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>

    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'GET',
                url: './rest/3rd/party/api/beneficiary-${id}',
                headers: {"Content-Type": 'application/json'},
                success: function (data, textStatus, jqXHR) {
                    var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                    var Investment = obj.Investments;
//                        $('#beni-table').bootstrapTable('load', Investment);

                    //Bank Detail
                    var BankAccountDetail = Investment[0].BankAccountDetail;
                    var AccountName = BankAccountDetail[0].AccountName;
                    var Bank = BankAccountDetail[0].Bank;
                    var Branch = BankAccountDetail[0].Branch;
                    var Account = BankAccountDetail[0].Account;
                    var Suffix = BankAccountDetail[0].Suffix;
                    var Currency = BankAccountDetail[0].Currency;
                    var bankStatus = BankAccountDetail[0].Status;
                    var bankType = BankAccountDetail[0].Type;
                    var bankIsPrimary = BankAccountDetail[0].IsPrimary;
                    setBankDetail(AccountName, Bank, Branch, Account, Suffix, Currency, bankStatus, bankType, bankIsPrimary);

                    //Address Detail
                    var Address = obj.Addresses;
                    var AddressID = Address[0].AddressID;
                    var Country = Address[0].Country;
                    var CountryCode = Address[0].CountryCode;
                    var Type = Address[0].Type;
                    var AddressLine1 = Address[0].AddressLine1;
                    var AddressLine2 = Address[0].AddressLine2;
                    var AddressLine3 = Address[0].AddressLine3;
                    var AddressLine4 = Address[0].AddressLine4;
                    var City = Address[0].City;
                    var Region = Address[0].Region;
                    var State = Address[0].State;
                    var PostalCode = Address[0].PostalCode;
                    var FormattedAddress = Address[0].FormattedAddress;
                    var IsPrimary = Address[0].IsPrimary;
                    var EffectiveDate = Address[0].EffectiveDate;
                    setAddress(AddressID, Country, CountryCode, Type, AddressLine1, AddressLine2, AddressLine3, AddressLine4, City, Region, State, PostalCode, FormattedAddress, IsPrimary, EffectiveDate);

                    // Personal Detail
                    var PhoneNumbers = obj.PhoneNumbers;
                    var Emails = obj.Emails;
                    var Email = Emails[0].Email;
                    var Id = obj.Id;
                    var Name = obj.Name;
                    var PhoneNumber = PhoneNumbers[0].Number;
                    var IrdNumber = obj.IrdNumber;
                    var AccessLevel = obj.AccessLevel;
                    var AMLEntityType = obj.AMLEntityType;
                    var Status = obj.Status;
                    var DateOfBirth = obj.DateOfBirth;
                    setAbout(Id, Name, Email, PhoneNumber, IrdNumber, AccessLevel, AMLEntityType, Status, DateOfBirth);

                }, error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        });


    </script>
    <script>
        function setAbout(Id, Name, Email, PhoneNumber, IrdNumber, AccessLevel, AMLEntityType, Status, DateOfBirth) {
            $("#ben-Id").html(Id);
            $(".ben-Name").html(Name);
            $("#ben-Email").html(Email);
            $("#ben-PhoneNumber").html(PhoneNumber);
            $("#ben-IrdNumber").html(IrdNumber);
//                $("#ben-AccessLevel").html(AccessLevel);
//                $(".ben-AMLEntityType").html(AMLEntityType);
            $("#ben-Status").html(Status);
            $("#ben-DateOfBirth").html(DateOfBirth);
        }

        function setAddress(AddressID, Country, CountryCode, Type, AddressLine1, AddressLine2, AddressLine3, AddressLine4, City, Region, State, PostalCode, FormattedAddress, IsPrimary, EffectiveDate) {
            $("#AddressID").html(AddressID);
            $("#Country").html(Country);
            $("#CountryCode").html(CountryCode);
            $("#Type").html(Type);
            $("#AddressLine1").html(AddressLine1);
            $("#AddressLine2").html(AddressLine2);
            $("#AddressLine3").html(AddressLine3);
            $("#AddressLine4").html(AddressLine4);
            $("#City").html(City);
            $("#Region").html(Region);
            $("#State").html(State);
            $("#PostalCode").html(PostalCode);
            $("#FormattedAddress").html(FormattedAddress);
            $("#IsPrimary").html(IsPrimary);
            $("#EffectiveDate").html(EffectiveDate);
        }

        function setBankDetail(AccountName, Bank, Branch, Account, Suffix, Currency, bankStatus, bankType, bankIsPrimary) {
            $("#bank-AccountName").html(AccountName);
            $("#bank-Bank").html(Bank);
            $("#bank-Branch").html(Branch);
            $("#bank-Account").html(Account);
            $("#bank-Suffix").html(Suffix);
            $("#bank-Currency").html(Currency);
            $("#bank-Status").html(bankStatus);
            $("#bank-Type").html(bankType);
            $("#bank-IsPrimary").html(bankIsPrimary);

        }

    </script>
    <script>
        $(document).ready(function() {
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
    </script>


</body>
</html>