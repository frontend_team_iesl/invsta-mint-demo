<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Admin Dashboard</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
      
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Investment</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box admin-new-one">
                            <div class="row">                                  
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">

                                        </div>
                                        <h6 class="element-header">
                                            Admin Dashboard
                                            <img src="https://s3-ap-southeast-2.amazonaws.com/invsta.com/public_resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-12 advisor-btn">
                                                    <button type="button" class="btn btn-info btn-lg advisorModel " data-toggle="modal" data-target="#advisorModel" onclick="advisorModel(this);">Add Advisor </button>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Customers
                                                        </div>
                                                        <div class="value totaluser">
                                                            181

                                                        </div>
                                                        <div class="trending trending-up">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Investments
                                                        </div>
                                                        <div class="value totalinvestment">
                                                            425

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Current Balance
                                                        </div>
                                                        <div class="value currentBalance">
                                                            152,963.02

                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            <!--                                                            Crypto Traded Portfolio (CTP) Investment Options -->
                                            Portfolio Investment
                                        </h6>
                                        <h6 id="error-message">

                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <c:forEach items="${allPortfolios}" var="portfolio">
                                                    <div class="col-md-3 col-sm-4">
                                                        <a href="./portfolio-${portfolio.getCode()}">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background: #fff">
                                                                <div class="portfolio_overlay">
                                                                    <h5 class="color3 portfolio_name">${portfolio.getName()}</h5>
                                                                </div>
                                                            </div>
                                                            <div class="parent-row-action">
                                                                <div class="row-actions"> 
                                                                    <!--<a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>-->
                                                                    <a href="./home-portreport" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                                </div>
                                                            </div>
                                                        </a>    
                                                    </div>
                                                </c:forEach>
                                                <!--                                                <div class="col-md-3 col-sm-4">
                                                                                                    <a href="./portfolio-290006">
                                                                                                        <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a;">
                                                                                                            <div class="portfolio_overlay">
                                                                                                                <h5 class="color3 portfolio_name"> Mint Diversified Income Fund </h5>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="parent-row-action">
                                                                                                            <div class="row-actions"> 
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </a>    
                                                                                                </div>
                                                
                                                                                                <div class="col-md-3 col-sm-4">
                                                                                                    <a href="./portfolio-290002">
                                                                                                        <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a;">
                                                                                                            <div class="portfolio_overlay">
                                                                                                                <h5 class="color3 portfolio_name"> Mint Australasian Equity Fund </h5>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="parent-row-action">
                                                                                                            <div class="row-actions"> 
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </a>    
                                                                                                </div>
                                                
                                                                                                <div class="col-md-3 col-sm-4">
                                                                                                    <a href="./portfolio-290004">
                                                                                                        <div class="ilumony-box text-center" style="cursor: pointer; background: #54565a;">
                                                                                                            <div class="portfolio_overlay">
                                                                                                                <h5 class="color3 portfolio_name">    Mint Australasian Property  Fund    </h5>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="parent-row-action">
                                                                                                            <div class="row-actions"> 
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>
                                                                                                                <a href="javascript:void(0)" style="color:white"><i class="fa fa-line-chart" style="font-size:20px"></i></a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </a>    
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-inline" action="./xls/transectionInvestment-sheet" method="get" >
                                        <button class="xls-file" type="submit" ><i class="fa fa-file-excel-o"></i></button>
                                    </form>


                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Investment Requests:</h5></div>
                                        <table id="pending-invetsment" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="created_ts" ><span >Date</span></th>
                                                    <!--<th data-field="investmentId" ><span >Investment Id</span></th>-->
                                                    <th data-field="name" ><span >Name</span></th>
                                                    <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                    <th data-field="portfolioName" ><span>Portfolio Name</span></th>
                                                    <th data-field="investedAmount" ><span >Invested Amount</span></th>
                                                    <th data-field="paymentMethod" ><span >Payment Amount</span></th>
                                                    <th data-field="investmentType" ><span >Investment Type</span></th>
                                                    <th data-field="status" ><span>status</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter">Action</th>-->
                                                    <th data-formatter="cancelInvestmentFormatter">Cancel</th>

                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <form class="form-inline" action="./xls/transection-sheet" method="get" >
                                        <button class="xls-file" type="submit" title="Sell fund file"><i class="fa fa-file-excel-o"></i></button>
                                    </form>
                                    <form class="form-inline" action="./xls/transectionMaster-sheet" method="get" >
                                        <button class="xls-file" type="submit" title="Master file"><i class="fa fa-file-excel-o"></i></button>
                                    </form>
                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Sell Requests:</h5></div>
                                        <table id="pending-transaction" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="created_ts" ><span >Date</span></th>
                                                    <th data-field="investmentcode" ><span >Investment Code</span></th>
                                                    <th data-field="name" ><span>Name</span></th>
                                                    <th data-field="beneficiaryName" ><span >Beneficiary Name</span></th>
                                                    <th data-field="portfolioName" ><span>Portfolio Name</span></th>
                                                    <th data-field="amount" ><span>Amount</span></th>
                                                    <th data-field="type" ><span>Type</span></th>
                                                    <!--<th data-formatter="viewButtonFormatter1">Action</th>-->
                                                    <th data-formatter="cancelTransectionFormatter">Cancel</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Bank Accounts:</h5></div>
                                        <table id="pending-bankAccount" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="accountHolderName" ><span >Account Holder Name</span></th>
                                                    <th data-field="bankName" ><span >Bank Name</span></th>
                                                    <th data-field="accountNumber" ><span>Account Number</span></th>
                                                    <th data-field="pincodeinput" ><span >Pincode Input</span></th>
                                                    <th data-field="action" ><span>status</span></th>
                                                    <!--<th data-field="amount" ><span>Amount</span></th>-->
                                                    <!--<th data-field="type" ><span>Type</span></th>-->
                                                    <th data-formatter="viewbankButtonFormatter">Action</th>
                                                    <!--<th data-formatter="cancelTransectionFormatter">Cancel</th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <form class="form-inline" action="./xls/company-sheet" method="get" >
                                        <button class="xls-file" type="submit" ><i class="fa fa-file-excel-o"></i></button>
                                    </form>
                                    <div class="container emp-profile">
                                        <div class="pendingstate">
                                            <h5>Pending Registrations:</h5>
                                            <div class="search-data-table">
                                                <input type="text" placeholder="Search..">
                                                <a href=""> <i class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                        <table id="pending-registration" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <!--<th data-field="id" ><span>Id</span></th>-->
                                                    <th data-field="username"><span>Username</span></th>
                                                    <th data-field="name"><span>Full Name</span></th>
                                                    <th data-field="dob"><span>D.O.B</span></th>
                                                    <th data-field="reg_type"><span>Reg. Type</span></th>
                                                    <th data-field="step" ><span>Step</span></th>
                                                    <th data-formatter="showDetailsButtonFormatter">Show Details</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="container emp-profile">
                                        <div class="pendingstate"><h5>Pending Application:</h5></div>
                                        <table id="pending-application" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="false" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                               data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <!--<th data-field="id" ><span>Id</span></th>-->
                                                    <th data-field="ApplicationId"><span>Application Id</span></th>
                                                    <th data-field="ExternalReference"><span>External Reference</span></th>
                                                    <th data-field="PrimaryBeneficiaryId"><span>Primary Beneficiary Id</span></th>
                                                    <th data-field="ApplicationType"><span>Application Type</span></th>
                                                    <th data-field="ApplicationSource" ><span>Application Source</span></th>
                                                    <!--<th data-field="CreatedDate" ><span>Created Date</span></th>-->
                                                    <th data-formatter="BeneficiariesLenght">Beneficiaries</th>
                                                    <th data-formatter="deleteapplicationformatter">Delete (Unavailable)</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
            <jsp:include page = "../views/models/addAdvisor.jsp"></jsp:include>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script> 
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
            <script src="./resources/js/sweetalert.min.js"></script>
            <jsp:include page="footer.jsp" />
        <script>

                                                        function viewButtonFormatter(value, row, index) {
                                                            return '<a href="./investment-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                                                        }
                                                        function viewbankButtonFormatter(value, row, index) {
                                                            return '<a href="./acceptbackaccount-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                                                        }
                                                        function cancelInvestmentFormatter(value, row, index) {
                                                            return '<a href="./cancelInvestment-' + row.id + '" class="btn btn-info cancel-btn "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Cancel</strong></span></a>';
                                                        }
                                                        function viewButtonFormatter1(value, row, index) {
                                                            return '<a href="./transection-' + row.id + '" class="btn btn-info a-btn-slide-text "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Accept</strong></span></a>';
                                                        }
                                                        function cancelTransectionFormatter(value, row, index) {
                                                            return '<a href="./cancelTransection-' + row.id + '" class="btn btn-info cancel-btn "><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Cancel</strong></span></a>';
                                                        }
                                                        function showDetailsButtonFormatter(value, row, index) {
                                                            return '<a href="./reg-details-db-' + row.token + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Show Details</strong></span></a>';
                                                        }
                                                        function deleteapplicationformatter(value, row, index) {
                                                            return '<a href="./delete-application-' + row.ApplicationId + '" class="btn btn-info a-btn-slide-text show-details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <span><strong>Delete</strong></span></a>';
                                                        }
                                                        function BeneficiariesLenght(value, row, index) {
                                                            var length = row.Beneficiaries.length;
                                                            return length;
                                                        }
        </script>
        <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/admin/get-pending-transaction',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $('#pending-transaction').bootstrapTable('load', obj);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/admin/get-pending-investment',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $('#pending-invetsment').bootstrapTable('load', obj);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/get-pending-registration',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
                        $('#pending-registration').bootstrapTable('load', obj);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/admin/get-pending-bankAccount',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(obj);
//                        alert(JSON.stringify(obj));
                        $('#pending-bankAccount').bootstrapTable('load', obj);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
//                $.ajax({
//                    type: 'GET',
//                    url: './rest/3rd/party/api/currentApplications',
//                    headers: {"Content-Type": 'application/json'},
//                    success: function (data, textStatus, jqXHR) {
//                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                        console.log(obj);
//                        $('#pending-application').bootstrapTable('load', obj);
//                    },
//                    error: function (jqXHR, textStatus, errorThrown) {
//                        console.log(jqXHR);
//                    }
//                });
                $.ajax({
                    type: 'GET',
                    url: './get-user-information',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        $('.totaluser').text(obj.user);
                        $('.totalinvestment').text(obj.investment);
                        $('.currentBalance').text(obj.amount);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
            });
        </script>

    </body>
</html>