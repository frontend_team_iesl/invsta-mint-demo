<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <jsp:include page="header_url.jsp"></jsp:include>
    </head>
    <body>
        <!--<div class="loader"></div>-->
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Investment</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span></span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box show-more-page">
                            <div class="show-header row">
                                <div class="col-md-9">
                                    <div class="element-wrapper">
                                        <p>Hi <span class="logged-user-name first-name">${user.name}</span>, here's a more detailed look at this particular investment. </p>
                                        <br>
                                        <h6 class="element-header" id="fundName"></h6>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="element-wrapper">
                                        <div class="element-box add-fund-btn" >
                                            <!--<a href="#" data-target=".add-funds-to-portfolio" id="add-funds-to-portfolio-action" data-toggle="modal" class="color3 btn btn-success btn-hover clickmodal" data-id="INV" style="width:40%">Add Funds</a>-->
                                            <form action="./make-transaction" method="post">
                                                <input type="hidden" name="pc" value="${pc}">
                                                <input type="hidden" name="ic" value="${ic}">
                                                <button type="submit"  id="add_fund"  class="btn-success clickmodal mint-btn-design"  style="width:100%">Add Funds</button>
                                            </form>
                                            <form action="./sell-transaction" method="post">
                                                <input type="hidden" name="pc" value="${pc}">
                                                <input type="hidden" name="ic" value="${ic}">
                                                <button type="submit" id="sell-funds-to-portfolio-action"  class="btn-width sell-btn-style mint-btn-design mint-transparent" style="width:100%">Sell Funds</button>

                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                                                <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Investment overview </h6>

                                    </div>
                                    <div class="element-box" style="display: none;">
                                        <div class="new-invest-chart">
                                            <div class="start-amnt">
                                                <div class="tab-pane active custom-add-box" id="tab_overview">
                                                    <div class="" style="">
                                                        Contributions
                                                    </div>
                                                    <div class="value add-value currentBalance contributions">$0.0</div>
                                                </div>
                                                <div class="custom-add-box-1" style="">
                                                    <div class=" current-label add-label">
                                                        Current Value
                                                    </div>
                                                    <div class="value add-value sumWalletBalance clr-add" style="">
                                                        <div class="sumWalletBal marketValue">$0.0</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="month-section">
                                                <div class="month-section">
                                                    <span class="mont-drop">Year </span> 
                                                    <select class="date-option">
                                                        <option>2019</option>
                                                        <option>2018</option>
                                                        <option>2017</option>
                                                        <option>2016</option>
                                                    </select>
                                                </div>
                                                <div class="month-section">
                                                    <span class="mont-drop">Month </span> 
                                                    <select class="date-option">
                                                        <option>Feb</option>
                                                        <option>Jan</option>
                                                        <option>Dec</option>
                                                        <option>Nov</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div id="fundPerformanceChart1" style="height: 400px; overflow: hidden;" data-highcharts-chart="1"></div>-->
                                        <div id="stockbalance5" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                    </div>
                                    <div class="element-box">
                                        <div class="new-invest-chart">
                                            <div class="start-amnt second-historical">
                                                <div class="tab-pane active custom-add-box">
                                                    <div class="" style="">
                                                        Contributions
                                                    </div>
                                                    <div class="value add-value currentBalance contributions">$0.0</div>
                                                </div>
                                                <div class="custom-add-box-1 custom-add-box" style="">
                                                    <div class=" current-label add-label">
                                                        Current Value
                                                    </div>
                                                    <div class="value add-value sumWalletBalance clr-add" style="">
                                                        <div class="sumWalletBal marketValue">$0.0</div>
                                                    </div>
                                                </div>
                                                <div class="custom-add-box-1" style="">
                                                    <div class=" current-label add-label">
                                                        Performance
                                                    </div>
                                                    <div class="value add-value sumWalletBalance clr-add" style="">
                                                        <div class="founder-icon-color" id="percentageid">0.0%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="stockhistorical" style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row performance-inline">
                                <!--                               <div class="col-md-12 mt-3">
                                                                                        <div class="element-wrapper">
                                                                                            <h6 class="new-design-hearder">Fund Performance </h6>
                                                                                        </div>
                                                                                        <div id="elementboxcontent" class="new-perform">
                                                                                            <div class="row">
                                                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                                                        <div class="label">
                                                                                                            1 Month
                                                                                                        </div>
                                                                                                        <div class="value">
                                                                                                            <div class="down-data" id="1-month">3.21%</div>
                                                                                                            <p class="date_upd"></p>
                                                                                                        </div>
                                
                                                                                                        <div class="trending trending-up 1-monthTrending" hidden="">
                                                                                                            <span>12.95</span>
                                                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                                                        </div> 
                                
                                
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                                                        <div class="label">
                                                                                                            3 Months
                                                                                                        </div>
                                                                                                        <div class="value">
                                                                                                            <div class="down-data" id="3-month">5.63%</div>
                                                                                                            <p class="date_upd"></p>
                                                                                                        </div>
                                
                                
                                
                                                                                                        <div class="trending trending-up 3-monthTrending" hidden="">
                                                                                                            <span>10.78</span>
                                                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                                                        </div> 
                                
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                                                        <div class="label">
                                                                                                            1 Year
                                                                                                        </div>
                                                                                                        <div class="value">
                                                                                                            <div class="down-data" id="1-year">14.08%</div>
                                                                                                            <p class="date_upd"></p>
                                                                                                        </div>
                                
                                
                                                                                                        <div class="trending trending-up 1-yearTrending" hidden="">
                                                                                                            <span>24.04</span>
                                                                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                                                                        </div> 
                                
                                
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-3 col-sm-3 mb-4">
                                                                                                    <div class="element-box el-tablo centered trend-in-corner smaller performance-sec">
                                                                                                        <div class="label">
                                                                                                            5 Years
                                                                                                        </div>
                                                                                                        <div class="value">
                                                                                                            <div class="down-data" id="5-year">14.61%</div>
                                                                                                            <p class="date_upd"></p> 
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>-->

                                <div class="col-md-12 mt-3">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder">Investment Details </h6>
                                    </div>
                                    <div id="elementboxcontent" class="invest-detail-box">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller ajust-div ">
                                                    <div class="label">
                                                        Current Price
                                                    </div>
                                                    <div class="value setval">
                                                        $2.4069 
                                                    </div>
                                                    <!-- <div class="trending trending-up">
                                                            <span>16.86%</span>
                                                            <i class="os-icon os-icon-arrow-up6"></i>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div ">
                                                    <div class="label">
                                                        Units
                                                    </div>
                                                    <div class="value" id="units">
                                                        253.8487 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div ">
                                                    <div class="label ">
                                                        Distribution Method 
                                                    </div>
                                                    <div class="value " id="currentInvSqr">
                                                        Reinvested
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 ">
                                                <div class="element-box el-tablo centered trend-in-corner smaller element-new ajust-div ">
                                                    <div class="label">
                                                        Tax Owed
                                                    </div>
                                                    <div class="arrow-text">
                                                        <div class="value per- per-color- taxOwed">
                                                            -$15.40 
                                                        </div>
                                                        <!-- <i class=" os-icon os-icon-arrow-up6"></i> -->
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mt-3">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder dot-position">Risk Indicator <div class="pulsating-circle pulsating-around2 risk-dot" data-toggle="tooltip" title="" data-placement="right" style="vertical-align:text-bottom">
                                                <span class="tooltiptext tolltip-1">Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund.</span>
                                            </div></h6>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="element-box risk-radius">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!--                                                                    <div class="risk-indicator">
                                                                                                                       <div class="element-wrapper">
                                                                                                                            <h6 class="element-header">Risk Indicator   <div class="pulsating-circle circle-2" data-toggle="tooltip" title="Managed funds in New Zealand must have a standard risk indicator. The risk indicator is designed to help investors understand the uncertainties both for loss and growth that may affect their investment. You can compare funds using the risk indicator. The risk indicator is rated from 1 (low) to 7 (high).The rating reflects how much the value of the fund's assets goes up and down (volatility). A higher risk generally means higher potential returns over time, but more ups and downs along the way. To help you clarify your own attitude to risk, you can seek financial advice or work out your risk profile at www.sorted.org.nz/tools/investor-kickstarter. Note that even the lowest category does not mean a risk-free investment, and there are other risks(described under the heading 'Other specific risks') that are not captured by this rating. This risk indicator is not a guarantee of a fund's future performance. The risk indicator is based on the returns data for the five years to 30 June 2019. While risk indicators are usually relatively stable, they do shift from time to time. You can see the most recent risk indicator in the latest fund update for each Fund." data-placement="right" style="vertical-align:text-bottom"></div></h6>
                                                                                                                        </div>
                                                                                                                        <div class="Higher">
                                                                                                                            <span class="risk-set">Lower risk</span>
                                                                                                                            <span>Higher risk</span>
                                                                                                                        </div>
                                                                                                                        <span class="indicator-box">
                                                                                                                            <span class="risk-btns ">1</span>
                                                                                                                            <span class="risk-btns addactive-cls">2</span>
                                                                                                                            <span class="risk-btns">3</span>
                                                                                                                            <span class="risk-btns">4</span>
                                                                                                                            <span class="risk-btns">5</span>
                                                                                                                            <span class="risk-btns">6</span>
                                                                                                                            <span class="risk-btns">7</span>
                                                                                                                        </span>
                                                                                                                        <div class="Higher">
                                                                                                                            <span  class="risk-set">Potentially <br>lower returns</span>
                                                                                                                            <span>Potentially <br>higher returns</span>
                                                                                                                        </div>
                                                                                                                    </div>-->
                                                <div class="new-risk-section">
                                                    <div class="low-risk">
                                                        <h6>Lower risk</h6>
                                                    </div>
                                                    <div class="low-risk risk-width">
                                                        <div class="risk-point">
                                                            <span class="risk">1</span>
                                                            <span class="risk">2</span>
                                                            <span class="risk">3</span>
                                                            <span class="risk">4</span>
                                                            <span class="risk">5</span>
                                                            <span class="risk">6</span>
                                                            <span class="risk">7</span>
                                                            <span class="risk">8</span>
                                                            <span class="risk">9</span>
                                                        </div>
                                                    </div>
                                                    <div class="low-risk ml-3">
                                                        <h6>Higher risk</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="risk-data">
                                        <h6>Potentially lower returns</h6>
                                        <h6 class="risk-color">Potentially higher returns</h6>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder" id="heading-fund">Month in review</h6>
                                    </div>
                                    <div id="elementboxcontent" class="element-box table-box">
                                        <!--                                        <div class="element-wrapper">
                                                                                    <h6 class="element-header" id="heading-fund"></h6>
                                                                                </div>-->
                                        <div class="row">
                                            <div class="col-md-12 " id="dis-fund">
                                                <div class="data-funds">




                                                    <c:choose>
                                                        <c:when test="${not empty  portfolioDetail.getMonthlyFundUpdate()}">
                                                            <h6>${portfolioDetail.getMonthlyReturnValue()}</h6><br>
                                                            <p>${portfolioDetail.getMonthlyFundUpdate()}</p> 
                                                        </c:when>
                                                        <c:otherwise>
                                                            <h6>Our portfolio returned -0.44% for the month.</h6>
                                                            <p>The top positive contributions were holdings in LendleaseGroup, CSL Limited and Mirvac Group. Charter Communications and SAP SE were the best performing stocks in the portfolio in the month after reporting strong Q3 results.</p><p>Global Equities had a positive contribution to the total return of the fund (+0.15%). This was offset by holdings within Fixed Income (-0.22%), Australasian Equities (-0.12%). The largest detractors were New Zealand Housing 2026 and 2028 bonds, Kiwi Property Group (sell-off after a $180m placement to raise capital to fund ongoing development projects across the portfolio) and Meridian Energy (following the announcement of the Smelter strategic review).</p><p>During the month, we added Cleanaway, Ingenia Communities, PPG Industries and Argosy 2026 senior bond. We sold our holding in Metlifecare.</p>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="element-wrapper">
                                        <h6 class="new-design-hearder aad-may"> Asset Allocation </h6>
                                    </div>
                                    <div class="element-box">
                                        <div class="el-chart-w" id="container30" style="height:400px; width:100%"></div>
                                    </div>
                                </div>
                                <!--                                <div class="col-md-12">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Recent Fund Update: </h6>
                                                                    </div>
                                                                </div>-->
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class=" new-design-hearder">Fund description </h6>
                                    </div>
                                    <div id="elementboxcontent" class="element-box">

                                        <div class="row">
                                            <div class="col-md-12" id="fundupdate">




                                                <c:choose>
                                                    <c:when test="${not empty  portfolioDetail.getFundOverview()}">
                                                        <p> ${portfolioDetail.getFundOverview()}</p>
                                                    </c:when>
                                                    <c:otherwise>

                                                        This is a multi-asset class Fund that invests across a range of asset types which includes: cash, fixed interest (including credit products), listed property and equities.The objective of the Fund is to deliver moderate capital growth in excess of the Consumer Price Index (CPI) by 3% per annum, before fees, over the medium to long-term, while also providing income on a quarterly basis. This Fund is invsta’s lowest risk strategy and investors should expect returns and risk to sit between the risk profiles of the fixed interest and property asset classes.
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 more_fund">
                                    <div class="element-wrapper">
                                        <h6 class=" new-design-hearder">Top 10 Assets </h6>
                                    </div>
                                    <div class="more-fund-data">
                                        <div class="risk-indicator">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="element-wrapper" id="topassests">
                                                        <!--                                                        <div class="mint-Diversified">
                                                                                                                   <h5>Health Care   </h5>
                                                                                                                   <p><span>33.66%</span></p>
                                                                                                               </div>
                                                                                                               <div class="mint-Diversified">
                                                                                                                   <h5>Industrials </h5>
                                                                                                                   <p><span>37.66%</span></p>
                                                                                                               </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade sell-funds-to-portfolio add-funds-to-portfolio" id="add-funds-to-portfolio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ria-hiddena="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Mint Diversified Income Fund </h5>

                                </div>
                                <div class="modal-body bank-model">
                                    <div class="row">



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Beneficiary Name :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <c:forEach items="${beneficiaryDetails}" var="category">
                                                    <input type="text" value="${category.getName()}" id="beneficiaryId" data-id="${category.getId()}">
                                                </c:forEach>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Transaction Amount :</label><br>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="number" id="investmentAmount" name ="investmentAmount" pattern="Enter Investment Amount" min="0"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12 bankaccounthide">
                                            <div class="form-group">
                                                <label>Bank Account Details:</label><br>
                                            </div>
                                        </div>
                                    </div>
                                    <c:forEach items="${bankAccountDetails}" var="bank">
                                        <div class="bankaccountdiv bank-status">
                                            <input type="hidden" name="id" class="bankid" value=" ${bank.getId()}">
                                            <input type="hidden" name="id" class="beneficiarid" value="${bank.getBeneficiaryId()}">
                                            <div class="row">
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Account Name
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group accountname">
                                                            ${bank.getAccountName()}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Bank
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group bankname">
                                                            ${bank.getBank()}  
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Branch
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group branchname">
                                                            ${bank.getBranch()} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 account-info">
                                                    <div class="col-sm-6">
                                                        <div class="form-group model-name">
                                                            Account 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group account">
                                                            ${bank.getAccount()} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="btn-pay">
                                                        <a type="button" class="btn selectBank" data-id="${bank.getId()}">Select</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary close-model btn-new" data-dismiss="modal">Close</button>
                                    <button type="button" id="saveInvestmentbtn" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="display-type"></div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"/>
        <script src="resources/bower_components/jquery/dist/jquery.min.js" defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
        <!--  <script src="resources/bower_components/moment/moment.js" defer></script>
                <script src="resources/bower_components/ckeditor/ckeditor.js" defer></script>
                <script src="resources/bower_components/bootstrap-validator/dist/validator.min.js" defer></script>
        <script src="resources/bower_components/dropzone/dist/dropzone.js" defer></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" defer></script>
        <!-- <script src="resources/bower_components/tether/dist/js/tether.min.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/util.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/alert.js" defer></script>
                <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/button.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/carousel.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/collapse.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/dropdown.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/modal.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
                <script src="resources/bower_components/bootstrap/js/dist/tooltip.js" defer></script>
        <script src="resources/bower_components/bootstrap/js/dist/popover.js" defer></script> -->
        <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" defer></script>
        <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
        <script src="https://code.highcharts.com/highcharts.js" ></script> 
        <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
        <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.15.3/bootstrap-table.js"></script>
        <script src="./resources/js/sweetalert.min.js"></script>
        <script>
            var type = "";
            var performanceChartDate = [];
            var performanceChartData = [];
            $(document).ready(function () {
                var assetArr = [];
                var ic = '${ic}';
                var pc = '${pc}';

                $('#heading-fund').html('<span>Month in review</span> <a href="javascript:void(0)" class="mint-btn-design low-padding">Download Full Update</a>');
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/show-me-more?ic=${ic}&pc=${pc}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        $('.loader').hide();
                        console.log(data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        var investmentHolding = obj.investmentHoldings[0];
                        console.log(JSON.stringify(investmentHolding));
                      var portfolioName = performances.PortfolioName.replace("Mint","Invsta");
                        if (!portfolioName.toString().includes("Invsta")) {
                            portfolioName = "Invsta " + portfolioName; 
                        }
                        $('#fundName').text(portfolioName);
                        $('.contributions').text('$' + investmentHolding.Contributions.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        $('.marketValue').text('$' + investmentHolding.MarketValue.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        $('#units').text(investmentHolding.Units.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        $('#currentInvSqr').text(investmentHolding.DistMethod);
                        $('.setval').text('$' + investmentHolding.Price.toFixed(4));
                        $('.taxOwed').text('$' + investmentHolding.TaxOwed.toFixed(2));
                        $('#bene-transactions-table').bootstrapTable('load', obj.fmcaTopAssetsesByTable);
                        callChart(investmentHolding.Units);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/fmca-investment-mix?id=${id}&ic=${ic}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log('success' + data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        if (obj.fmcaInvestmentMix.length > 0) {
                            fmcaInvestmentMix(obj);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/3rd/party/api/portfolio-dashboard?ic=${ic}&pc=${pc}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log('success' + data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        if (obj.fmcaTopAssets.length > 0) {
                            $.each(obj.fmcaTopAssets, function (idx, val) {
                                $('#topassests').append("<div class='mint-Diversified'><h5>" + val.AssetName + "</h5><p><span>" + ((val.Percentage)).toFixed(2) + "%</span></p></div> ");
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/get-FundPerformance-${pc}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log("data-->" + data);
                        var obj = JSON.parse(data);

                        $('.risk').each(function (i) {
                            var statsValue = $(this).text();
                            if (statsValue === obj.riskIndicator) {
                                $(this).addClass("risk-point-radius");
                            }
                        });
                        console.log('here is end');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/3rd/party/api/beneficiary-${user.beneficiairyId}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log('success' + data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        console.log(JSON.stringify(obj));
                        if (obj.FirstName !== "" && obj.FirstName !== null) {
                            $('.first-name').text(obj.FirstName);
                        } else {
                            $('.first-name').text('${user.name}');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/investment-return-rate?ic=${ic}&pc=${pc}',
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        console.log('success' + data);
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                        var returnrateper = obj.investmentReturnRate;
                        $('#percentageid').text((returnrateper * 100).toFixed(2) + "%");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('error');
                    }
                });
                function fmcaInvestmentMix(obj) {
                    var assetAllocation = [];
                    var fmcaInvestmentMix = obj.fmcaInvestmentMix;
                    $.each(fmcaInvestmentMix, function (idx, val) {
                        assetAllocation.push([val.FMCAAssetClass, val.SectorValueBase]);
                    });
                    pieChart(assetAllocation, false);
                }
                function callChart(units) {
                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/getUnitsPerformancechart?ic=${ic}&portfolios=${pc}',
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            console.log('success' + data);
                            var obj = JSON.stringify(data);
                            var obj = JSON.parse(obj.replace(/[\n\t\r]/g, ' '));
                            console.log('success' + JSON.stringify(obj));
                            allPortfolioChart(obj.valueByDate);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('error');
                        }
                    });

                }
                function allPortfolioChart(obj) {

                    var i = 0;
                    var firstdate;
                    for (var firstKey in obj) {
                        if (i === 0) {
                            firstdate = firstKey;
                            i++;
                        } else {
                            break;
                        }
                    }
                    var startdate = new Date(firstdate);
                    var year = startdate.getFullYear();
                    var month = startdate.getMonth();
                    var day = startdate.getDate();
                    $.each(obj, function (idx, val) {
                        performanceChartData.push(val);
                    });
                    splinechart2(year, month, day);
                }
            });
            $('.clickmodal').click(function () {
                type = $(this).attr("data-id");

            });
            $('.selectBank').click(function () {
                $('.selectBank').removeClass('selectedbank');
                $(this).addClass('selectedbank');
            });
            $('#saveInvestmentbtn').click(function () {
                var bankAccountId = $('.selectedbank').data("id");
                //                    var investmentName = $('#investmentName').val();
                var investmentAmount = $('#investmentAmount').val();
                var beneficiaryId = $('#beneficiaryId').data("id");
                var obj = {investmentcode: '${ic}', amount: investmentAmount, portfolioCode: '${pc}', beneficiaryId: beneficiaryId, bankAccountId: bankAccountId, type: type};
                swal({
                    title: "Proceed",
                    text: "Transaction is progress.",
                    type: "info",
                    timer: 2500,
                    showConfirmButton: true
                });
                $.ajax({
                    type: 'POST',
                    url: './rest/groot/db/api/admin/pending-transaction',
                    data: JSON.stringify(obj),
                    headers: {"Content-Type": 'application/json'},
                    success: function (data, textStatus, jqXHR) {
                        swal({
                            title: "Success",
                            text: "Transaction is successfully.",
                            type: "success",
                            timer: 2500,
                            showConfirmButton: true
                        });
                        location.reload(true);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });
            });
        </script>                

        <script>
            function splinechart() {
                Highcharts.chart('stockbalance5', {
                    chart: {
                        type: 'spline',
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {

                    },
                    xAxis: {
                        categories: performanceChartDate,
                        tickInterval: 100,
                        labels: {
                            style: {
                                color: '#333',
                                fontSize: '12px',
                                textTransform: 'uppercase'
                            },
                            y: 20,
                            x: 10
                        },
                        lineColor: '#2c94ec'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'center',
                        verticalAlign: 'bottom',
                        enabled: false

                    },
                    plotOptions: {
                        series: {
                            color: '#2c94ec',
                            shadow: true,
                            lineWidth: 3,
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        pointFormatter: function () {
                            var isNegative = this.y < 0 ? '-' : '';
                            var value = isNegative + '$' + Math.abs(this.y.toFixed(2));
                            var formated = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            return formated;
                            //        return  isNegative + '$' + Math.abs(this.y.toFixed(0));
                        }
                    },
                    series: [{
                            name: '',
                            data: performanceChartData
                        }],
                    responsive: {
                        rules: [{
                                condition: {
                                    maxWidth: 500
                                }
                            }]
                    }

                });
            }
        </script>        
        <script>

            function pieChart(arr, mktval) {

                Highcharts.setOptions({
                    lang: {
                        thousandsSep: ','
                    }
                });
                Highcharts.setOptions({
                colors: ['#2d373e', '#575f65', '#2c94ec', '#d7d2cb']
            });
                var ptfmt = '';
                if (mktval) {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} % <br>Market Value: {point.y}';
                } else {
                    ptfmt = '{series.name} <br>{point.percentage:.2f} %';
                }
                Highcharts.chart('container30', {
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 0
                        }
                    },
                    credits: {
                        enabled: false,
                    },
                    exporting: {
                        enabled: false,
                    },
                    legend: {
                        enabled: true,
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    tooltip: {
                        valueDecimals: 2,
                        pointFormat: ptfmt,
                        valuePrefix: '$',
                    },
                    //                    tooltip: {
                    //                    valueDecimals:2,
                    //                    pointFormatter: function () {
                    //                        var isNegative = this.y < 0 ? '-' : '';
                    //                        return  isNegative + '$' + Math.abs(this.y.toFixed(4).toString());
                    //                    }
                    //                },
                    plotOptions: {
                        pie: {
                            innerSize: 100,
                            depth: 45,
                            dataLabels: {enabled: false,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %<br>Value: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            },
                        },
                    },
                    series: [{
                            showInLegend: true,
                            name: '',
                            data: arr
                        }]
                });
            }

        </script>
        <script>
            function splinechart2(year, month, day) {

                // Create the chart
                Highcharts.stockChart('stockhistorical', {
                    chart: {
                        events: {
                            load: function () {
                                if (!window.TestController) {
                                    this.setTitle(null, {
                                        text: ''
                                    });
                                }
                            }
                        },
                        zoomType: 'x',
                    },
                    credits: {
                        enabled: false
                    },
                    navigation: {
                        buttonOptions: {
                            enabled: false
                        }
                    },

                    rangeSelector: {

                        buttons: [{
                                type: 'day',
                                count: 3,
                                text: '3d'
                            }, {
                                type: 'week',
                                count: 1,
                                text: '1w'
                            }, {
                                type: 'month',
                                count: 1,
                                text: '1m'
                            }, {
                                type: 'month',
                                count: 6,
                                text: '6m'
                            }, {
                                type: 'year',
                                count: 1,
                                text: '1y'
                            }, {
                                type: 'all',
                                text: 'All'
                            }],
                        selected: 5
                    },
                    plotOptions: {
                        series: {
                            color: '#2c94ec',
                            lineWidth: 2,
                        }
                    },

                    yAxis: {
                        title: {
                            text: ''
                        }
                    },

                    title: {
                        text: ''
                    },

                    subtitle: {
                        text: '' // dummy text to reserve space for dynamic subtitle
                    },
                    navigator: {
                        enabled: true
                    },
                    scrollbar: {
                        barBackgroundColor: '#2c94ec',
                        barBorderRadius: 7,
                        barBorderWidth: 0,
                        barBorderHeight: 1,
                        buttonBackgroundColor: '#2c94ec',
                        buttonBorderWidth: 0,
                        buttonBorderRadius: 7,
                        trackBackgroundColor: 'none',
                        trackBorderWidth: 1,
                        trackBorderRadius: 8,
                        trackBorderColor: '#CCC'
                    },
                    series: [{
                            name: 'Value',
                            data: performanceChartData,
                            type: 'spline',
                            pointStart: Date.UTC(year, month, day),
                            pointInterval: 3600 * 1000 * 24,
                            tooltip: {
                                xDateFormat: '%d-%m-%Y',
                                valueDecimals: 1,
                                valuePrefix: '$'
                            }
                        }]

                });

            }
        </script>

    </body>
</html>			  						