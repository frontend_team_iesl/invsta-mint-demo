
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<c:set var="home" value="${pageContext.request.contextPath}"/>--%>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link href="https://backoffice.invsta.io/pocv/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="./resources/favicon.png" rel="shortcut icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>
        <link href="./resources/backoffice/style.css" rel="stylesheet"/>
        <!--<link href="https://backoffice.invsta.io/pocv/resources/css/main.css?version=3.5.1" rel="stylesheet"/>-->
        <link href="https://backoffice.invsta.io/pocv/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <link href="./resources/css/main.css" rel="stylesheet"/>
        <link href="./resources/css/custom.css" rel="stylesheet"/>
        <link href="./resources/css/bootstrap-pincode-input.css" rel="stylesheet"/>
        <link rel="stylesheet" href="./resources/css/signup/intlTelInput.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">-->
        <!--<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css'>-->
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->


        <style>
            .submit-btn input {
                width: 20%;
                margin-top: 20px;
                background: #65b9ac!important;
                color: white;
                border: none;
                padding: 4px 6px;
                border-radius: 5px;
            }
            .profile-weight {
                font-weight: 500;
            }
            .tabpill-margin {
                margin-top: 20px;
            }
            .image-select {
                width: 100%;
                font-size: 13px;
                position: relative;
                top: -5px;
            }
            .profile-pic {
                max-width: 200px;
                max-height: 200px;
                display: block;
                width: 100%;
                height: 100%;
                object-fit: cover;
                border-radius: 50%;
            }

            .file-upload {
                display: none;
            }
            .profile-circle {
                border-radius: 1000px !important;
                overflow: hidden;
                width: 200px;
                height: 200px;
                border: 8px solid #2f55be;
                max-width: 200px;
                margin: auto;

            }

            .upload-button {
                font-size: 1.2em;
            }

            .upload-button:hover {
                transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                color: #999;
            }
            .emp-profile {
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            /*start setting checkbox*/
            .new-notification .custom-check {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 16px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
            .new-notification .custom-check input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }
            .new-notification .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 23px;
                width: 23px;
                background-color: #eee;
            }
            .new-notification .custom-check:hover input ~ .checkmark {
                background-color: #ccc;
            }
            .new-notification .custom-check input:checked ~ .checkmark {
                background-color: #2c94ec;
            }
            .new-notification .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }
            .new-notification .custom-check input:checked ~ .checkmark:after {
                display: block;
            }
            .new-notification .custom-check .checkmark:after {
                left: 8px;
                top: 4px;
                width: 8px;
                height: 12px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            .color-take {
                background: #e9eaed;
                width: 50px;
                height: 45px;
                cursor: pointer;
                max-width: 90%;
            }
            .color-line {
                display: flex;
                margin-bottom: 15px;
            }
            .color-red {
                background: red;
            }
            .color-green {
                background: green;
            }
            .color-blue {
                background: blue;
            }
            .color-yellow-red {
                background: #dc7b17;
            }
            .color-purple {
                background: purple;
            }
            .color-pink {
                background: pink;
            }
            .vc-chrome {
                position: absolute;
                top: 35px;
                right: 0;
                z-index: 9;
                width: 100%
            }
            .current-color {
                display: inline-block;
                width: 16px;
                height: 16px;
                background-color: #000;
                cursor: pointer;
            }
            .submit_btn_new {
                display: table;
                margin-left: auto;
            }
            /*end setting checkbox*/
            .night-mode{
                filter: invert(100%);
                background-color: #000;
            }

            .night-mode img {
                filter: invert(100%);
            }
            .day-night-mode a {
                color: #fff!important;
                cursor: pointer;
            }
            .pincode-input-container {
                display: inline-block;
                display: flex;

            }
            .pincode-input-container textarea:focus, input:focus {
                border-color: #676767 !important;
            }

        </style>
    </head>
    <body>
        <div class="loader">
            <ul class="circle-loader">
                <li class="center"></li>
                <li class="item item-1"></li>
                <li class="item item-2"></li>
                <li class="item item-3"></li>
                <li class="item item-4"></li>
                <li class="item item-5"></li>
                <li class="item item-6"></li>
                <li class="item item-7"></li>
                <li class="item item-8"></li>
            </ul>
        </div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp" />  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">Profile</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)"></a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
            -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="user-slider">
                                        <div class="slideshow-container">
                                            <!--                                            <a class="prev zidex" onclick="plusSlides(-1)">&#10094;</a>
                                                                                        <a class="next zidex" onclick="plusSlides(1)">&#10095;</a>-->
                                            <div class="mySlides fade" id="myslide-clone">
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <div class="container emp-profile">
                                                            <div class="row">

                                                                <div class="col-md-4">

                                                                    <div class="profile-circle profile-images">
                                                                        <!-- User Profile Image -->
                                                                        <c:choose>
                                                                            <c:when test="${not empty user.userLogoPath}">
                                                                                <img class="profile-pic Imgurl" src="${user.userLogoPath}" alt="" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <img alt="" class="profile-pic Imgurl" src="./resources/images/avtar.png">
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                        <!--<img class="profile-pic" src="https://backoffice.invsta.io/pocv/resources/img/man.jpg">-->
                                                                    </div>
                                                                    <!--                                                                <div class="p-image">
                                                                                                                                        <form action ="./profilePic" enctype="multipart/form-data" method="POST">
                                                                                                                                            <i class="fa fa-camera upload-button"></i>
                                                                                                                                            <input class="file-upload" type="file" name ="userLogo" onchange="readURL(this);" accept="image/*"/>
                                                                                                                                            <div class="submit-btn">
                                                                                                                                                <input class="" type="submit" value="Submit"><a href=""><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                                                                                                                            </div>
                                                                                                                                        </form>
                                                                                                                                    </div>-->
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="profile-head">
                                                                        <h5 class="ben-Name">
                                                                        </h5>
                                                                        <ul class="nav nav-tabs profile-weight" id="myTab" role="tablist">
                                                                            <li class="nav-item">
                                                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href=".home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" id="profile-tab" data-toggle="tab" href=".profile" role="tab" aria-controls="profile" aria-selected="false">Address</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" id="bank-tab" data-toggle="tab" href=".bank" role="tab" aria-controls="bank" aria-selected="false">Bank Details</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content profile-tab tabpill-margin profile-weight" id="myTabContent">
                                                                            <div class="tab-pane fade active in home" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Account Id :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-Id"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Name :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-Name"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="email-row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Email :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-Email"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="phone-row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Mobile Number :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-PhoneNumber"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row homeNum" id="phone-row ">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Home Number :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-HomeNumber"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row BusinessNum" id="phone-row ">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Business Number :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-BusinessNumber"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="phone-row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Date Of Birth :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-DateOfBirth"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Status :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-Status"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>PIR Rate :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="ben-PIR"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="edit-profile">
                                                                                            <!-- Trigger the modal with a button -->
                                                                                            <button type="button" class="phone-data phoneModel"  data-toggle="modal" data-target="#phoneModel" onclick="phoneModel(this);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tab-pane fade profile" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Country :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="Country"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>Country Code :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="CountryCode"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>Type :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="Type"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>-->
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Address</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="AddressLine1"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>Address Line 2 :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="AddressLine2"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>Address Line 3 :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="AddressLine3"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>Address Line 4 :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="AddressLine4"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>-->
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>City :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="City"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Region :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="Region"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--                                                                                <div class="row">
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <label>State :</label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="col-sm-6">
                                                                                                                                                                        <p class="State"></p>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>-->
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Postal Code :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="PostalCode"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Effective Date :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="EffectiveDate"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="edit-profile">
                                                                                            <!-- Trigger the modal with a button -->
                                                                                            <button type="button" class="addressModel" data-toggle="modal" data-target="#addressModel" onclick="addressModel(this);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tab-pane fade bank" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Account Name :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-AccountName"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Bank :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-Bank"> </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="email-row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Branch :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-Branch"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="phone-row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Account :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-Account"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Suffix :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-Suffix"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <label>Currency :</label>
                                                                                    </div>
                                                                                    <div class="col-sm-6 col-6">
                                                                                        <p class="bank-Currency"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="edit-profile">
                                                                                            <!-- Trigger the modal with a button -->
                                                                                            <button type="button" class="bankModel" data-toggle="modal" data-target="#bankModel" onclick="bankModel(this);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                        <!--                                        <div class="slider-dot" style="text-align:center">
                                                                                    <span class="dot" id="currentslide" onclick="currentSlide(1)"></span> 
                                                                                                                                <span class="dot" onclick="currentSlide(2)"></span> 
                                                                                                                                <span class="dot" onclick="currentSlide(3)"></span> 
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Setting
                                        </h6>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <!--                                        <h6 class="element-header">
                                                                                    Setting
                                                                                </h6>-->
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs profile-border smaller">
                                                        <li class="nav-item">
                                                            <a class="profile-tab active" data-toggle="tab" href="#tab_all_days">Notifications</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="new-notification">
                                                <!--                                                <label class="custom-check">Signup Notifications
                                                                                                    <input type="checkbox" checked="checked">
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>-->
                                                <label class="custom-check">Login Notifications
                                                    <input type="checkbox" class="notificationCheck">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="custom-check">Add fund Notifications
                                                    <input type="checkbox" class="notificationCheck">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="custom-check">Signup Notifications
                                                    <input type="checkbox" class="notificationCheck">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="submit_btn_new noti_submit">
                                                <a class="" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs profile-border smaller">
                                                        <li class="nav-item">
                                                            <a class="profile-tab active" data-toggle="tab" href="#tab_all_days">2FA Verification</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="2faStatus">2FA is enabled</label>
                                                </div>
                                                <div class="col-md-6 switch-btn-fa">
                                                    <label class="switch1">
                                                        <input type="checkbox"  class ="enable2FA" onchange="enable2FA();">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div  class="col-md-6 mt-3">
                                                    <div  class="tofa-style">
                                                        <div class="enableield">
                                                            <input type='text' id='country-code ' class='conty-box enableield c_code' name='' placeholder=""   readonly="">
                                                            <input type='text' id='sec_number' class='mobile-box enableield' readonly="" >
                                                            <input type='text' id='mob_number' class=' enableield' name=''placeholder=" Enter Mobile number " hidden>
                                                        </div>
                                                        <div class="opthide">
                                                            <input type='text' id='otp-qr' class='  opthide otpkey' name=''placeholder=" enter OTP "    maxlength="6">
                                                        </div>
                                                        <span class="msg"></span>
                                                    </div>
                                                </div>
                                                <div  class="col-md-6 mt-3">
                                                    <div class="generate-otp-btn">
                                                        <input type='button' id='genrate_code' class='btn btn-primary btn-style enableield' name='enableield'placeholder="" onclick="otp();" value="Generate OTP">
                                                        <input type='button' id='' class='btn btn-primary btn-style opthide refres-validate' name=''placeholder="" onclick="valOtp();" value="Validate OTP">
                                                        <a href="javascript:void(0)" class="opthide" onclick="otp();"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                                <div  class="col-md-12 mt-3 continue">
                                                    <input type='button' id='' class='btn btn-primary btn-style continue' name=''placeholder="" onclick="genrateQr();" value="Continue">

                                                </div>


                                                <div class="col-md-12 barcodeDiv" id="barcode">
                                                    <div  class="modal-body">
                                                        <p>Scan this Barcode using Google Authenticator app on your phone to use it later in login
                                                            <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Android</a> and
                                                            <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605">iPhone</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                            <div class="submit_btn_new">
                                                                                            <a class="btn btn-primary btn-style" href="javascript:void(0)">Submit</a>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <!--                                        <h6 class="element-header">
                                                                                    Setting
                                                                                </h6>-->
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs profile-border smaller">
                                                        <li class="nav-item">
                                                            <a class="profile-tab active" data-toggle="tab" href="#tab_all_days">Change Password</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>
                                                        Enter Previous Password
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-form-style cls">
                                                        <input type="text" id="oldpassword" class="dataerror pwd" name="oldpassword" title="Must contain at least one number and one uppercase and lowercase letter and one specail character, and at least 8 or more characters">
                                                        <span class="error-grey pass" id='error-oldpassword'></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>
                                                        Enter New  Password
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-form-style cls">
                                                        <input type="text" id="newpassword" class="dataerror pwd" name="newpassword">
                                                        <span class="error-grey pass" id='error-newmessage'></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>
                                                        ReEnter New Password
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-form-style">
                                                        <input type="text" id="confirmpassword" class="dataerror" name="confirmpassword">
                                                        <span class="error" id='error-confirmmessage'></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="submit_btn_new submitChangePass">
                                                <a class="setPassword" onclick="Validate()" href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <!--                                        <div class="element-box">
                                                                                    <div class="os-tabs-w">
                                                                                        <div class="os-tabs-controls">
                                                                                            <ul class="nav nav-tabs smaller">
                                                                                                <li class="nav-item">
                                                                                                    <a class="nav-link active" data-toggle="tab" href="#tab_all_days">Setting</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="color-section">
                                                                                                                                        <div class="color-line">
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-red"></div></a>
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-green"></div></a>
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-blue"></div></a>
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-yellow-red"></div></a>
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-purple"></div></a>
                                                                                                                                            <a href="javascript:void(0)"><div class="color-take color-pink"></div></a>
                                                                                                                                        </div>
                                                                                                                                        <div class="custom-color-line">
                                                                                                                                            <p class="element-header">
                                                                                                                                                Custom Color
                                                                                                                                            </p> 
                                                                                                                                            <div class="custm-clr">
                                                                                                                                                <div id="app" class="form-horizontal">
                                                                                                                                                    <div class="row">
                                                                                                                                                        <div class="col-md-12 col-md-offset-12">
                                                                                                                                                            <div class="form-group">
                                                                                                                                                                <label class="col-sm-4 control-label">Custom color</label>
                                                                                                                                                                <div class="col-sm-8">
                                                                                                                                                                    <colorpicker :color="defaultColor" v-model="defaultColor" />
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                        <div class="custom-color-line">
                                                                                            <p class="element-header">
                                                                                                Color Text Size
                                                                                            </p> 
                                                                                            <div class="custm-size">
                                                                                                                                                        <div class="font-large">
                                                                                                                                                            <p style="font-size : 20px">Color Text Size 20px</p>
                                                                                                                                                        </div>
                                                                                                <button id="btn-decrease" class="btn btn-style">A-</button>
                                                                                                <button id="btn-orig">A</button>
                                                                                                <button id="btn-increase" class="btn btn-style">A+</button>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="day-night-mode mt-3">
                                                                                            <p class="element-header">
                                                                                                Day and Night
                                                                                            </p> 
                                        
                                                                                            <a class="btn btn-style night-button"> Night Mode</a>
                                                                                            <label class="switchnight">
                                                                                                <input type="checkbox">
                                                                                                <span class="slidernight roundnight"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="submit_btn_new">
                                                                                            <a class="btn btn-primary btn-style" href="javascript:void(0)">Submit</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page = "../views/models/updateBankAccount.jsp"></jsp:include>
        <jsp:include page = "../views/models/updatePhoneNumber.jsp"></jsp:include>
        <jsp:include page = "../views/models/updateAddress.jsp"></jsp:include>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>
            <script src="./resources/js/intlTelInput_1.js"></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" ></script>
            <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js" ></script>
            <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/util.js" defer></script>
            <script src="https://backoffice.invsta.io/ui-fund/resources/bower_components/bootstrap/js/dist/tab.js" defer></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/collapse.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/bower_components/bootstrap/js/dist/util.js" ></script>
            <script src="https://backoffice.invsta.io/pocv/resources/js/main.js?version=3.5.1" ></script>
            <script src="https://code.highcharts.com/stock/highstock.js" ></script> 
            <script src="https://code.highcharts.com/highcharts.js" ></script> 
            <script src="https://code.highcharts.com/stock/highcharts-3d.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/exporting.js" ></script>
            <script src="https://code.highcharts.com/stock/modules/export-data.js" ></script>
            <script src="https://code.highcharts.com/modules/cylinder.js"></script>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.4/vue.min.js'></script>
            <script src='https://unpkg.com/vue-color/dist/vue-color.min.js'></script>
            <script src="./resources/js/intlTelInput_1.js"></script>
            <script src="./resources/js/intlTelInput_2.js"></script>
            <script src="./resources/js/intlTelInput_3.js"></script>
            <script type="text/javascript" src="./resources/js/countries.js"></script>
            <script src="./resources/js/bootstrap-pincode-input.js"></script>

            <script>
//                                                    $('.pwd').keyup(function () {
//                                                        var password = $(this).val();
//                                                        var passwordExpression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*[\s]).{8,16}$/;
//                                                        if (password === "" || passwordExpression.test(password)) {
//                                                           var cls = $(this).closest('.cls');
//                                                            cls.find(".pass").text('');
//                                                        } else {
//                                                            var passwordTitle = "Password must contain at least one number, one uppercase and lowercase letter, one special character  and be between 8-16 characters";
////                                                            $("#error-password").html(passwordTitle);
//                                                            var cls = $(this).closest('.cls');
//                                                            cls.find(".pass").html(passwordTitle);
//                                                        }
//                                                    });
            </script>
            <script>
//                $("#oldpassword").keydown(function () {
//
//
//
//                });
                $("#newpassword").keyup(function () {

                    $("#error-oldpassword").text('');
                    $("#error-newmessage").text('');


                });





                $(".notificationCheck").change(function () {
                    var checkedNum = $('.notificationCheck:checked').length;
                    if (checkedNum > 0) {
                        $(".noti_submit").show();
                    } else {
                        $(".noti_submit").hide();
                    }
                });

                $("#oldpassword").keyup(function () {
                    var checkedNum = $("#oldpassword").val();
                    $("#error-oldpassword").text('');
                    $("#error-newmessage").text('');
//                    alert(checkedNum);
                    if (checkedNum.length > 0) {
                        $(".submitChangePass").show();
                    } else {
                        $(".submitChangePass").hide();
                    }
                });



                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $(input).closest('.profile-images').find('.Imgurl').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);

                    }
                }
            </script>


            <script>

                var cCode = new Map();
                $(document).ready(function () {
                    $(".noti_submit").hide();
                    $(".submitChangePass").hide();


                    $(".codenumber").intlTelInput_1();
                    $(".countryname").intlTelInput_2();
                    $(".countrynameoutnz").intlTelInput_3();
                });


                $.ajax({
                    type: 'GET',
                    url: './rest/groot/db/api/getAllCountry-Code',
                    success: function (data, textStatus, jqXHR) {
                        var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));

                        console.log("Country data=----------" + data);
                        for (var i = 0; i < obj.length; i++) {
                            cCode[obj[i].country] = obj[i].codeISO;
                        }


                    }, error: function (jqXHR, textStatus, errorThrown) {
//                        alert("code error" + textStatus);
                    }
                });




//                                                          
                function Validate() {
                    var response;
                    var password = document.getElementById("newpassword").value;
                    var oldpassword = document.getElementById("oldpassword").value;
                    var confirmPassword = document.getElementById("confirmpassword").value;
                    var passwordExpression = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*[\s|%|#|&]).{8,16}$/;
                    var passwordTitle = "Password must contain at least one number, one uppercase and lowercase letter, one special character  and be between 8-16 characters";
                    if (oldpassword.trim() === "") {
                        $("#error-oldpassword").text(passwordTitle);
                        response = false;

                    } else if (password !== confirmPassword) {
                        $("#error-confirmmessage").text("Passwords do not match.");
                        response = false;
                    } else if (password === "" || !passwordExpression.test(password)) {
                        $("#error-newmessage").text(passwordTitle);
                        response = false;
                    } else if (confirmPassword.trim() === "") {
                        $("#error-confirmmessage").text("Password can not be blank.");
                        response = false;
                    } else {
                        response = true;
                    }
                    if (response) {
                        var oldPassword = $('#oldpassword').val();
                        var newPassword = $('#newpassword').val();
                        obj = {old_password: oldPassword, new_password: newPassword};
//                        swal({
//                            title: "Progress",
//                            text: "Password Change request is in progress.",
//                            type: "info",
//                            timer: 3500,
//                            showConfirmButton: true
//                        });
                        $.ajax({
                            type: 'POST',
                            url: './rest/groot/db/api/password-registeration',
                            headers: {"Content-Type": 'application/json'},
                            data: JSON.stringify(obj),
                            success: function (data, textStatus, jqXHR) {

                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));

                                if (obj.message === "true") {
                                    swal({
                                        title: "Sucess",
                                        text: "Your password change request is accepted successfully.",
                                        type: "info",
                                        timer: 3500,
                                        showConfirmButton: true
                                    });
                                } else {
                                    swal({
                                        title: "Failed",
                                        text: "Previous  Password did not match.",
                                        type: "warning",
                                        timer: 3500,
                                        showConfirmButton: true
                                    });
                                }
                                console.log(data);
                            }, error: function (jqXHR, textStatus, errorThrown) {
//                                alert(textStatus);
                            }
                        });
                    }

                }
                function initAutocomplete() {
                    var input = document.getElementById('all-address');
                    var options = {
                        types: ['address'],
                        componentRestrictions: {
                            country: 'nz'
                        }
                    };
                    autocomplete = new google.maps.places.Autocomplete(input, options);
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var place = autocomplete.getPlace();
                        console.log(JSON.stringify(place));
//                        alert(place);
                        for (var i = 0; i < place.address_components.length; i++) {
                            for (var j = 0; j < place.address_components[i].types.length; j++) {
//                                 alert(place.address_components[i].types[j]);
                                if (place.address_components[i].types[j] === "postal_code") {
//                                     alert(place.address_components[i].long_name);
                                    document.getElementById('postal_code').value = place.address_components[i].long_name;

                                }
                            }
                        }
                    });
                }
            </script>
            <script>
                var phoneNum = new Array();
                var BankCode = new Map();
                var BankNameByNcNum = new Map();
                $(document).ready(function () {
                    var id = '${id}';
                    var count = 1;
//                    $('.enable2FA').prop('disabled', false);
            <%--     console.log('hello data' + '${allBankCode}');
        <c:forEach items="${allBankCode}" var="bankCode" varStatus="idx">
                BankCode[${bankCode.bankCode}] = '${bankCode.bankName}';
             BankNameByNcNum[${bankCode.ncNumber}] = '${bankCode.bankName}';
        </c:forEach>--%>
            <c:forEach items="${beneficiaryDetails}" var="ben" varStatus="idx">

                    $.ajax({
                        type: 'GET',
                        url: './rest/groot/db/api/beneficiary?UserId=${id}&BeneficiaryId=' +${ben.getBeneficiaryId()},
                        headers: {"Content-Type": 'application/json'},
                        success: function (data, textStatus, jqXHR) {
                            $(".loader").hide();
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            //                    var Investment = obj.Investments;
                            //                        $('#beni-table').bootstrapTable('load', Investment);
                            console.log(" all data  -------------------->" + data);
                            console.log(JSON.stringify(obj.phoneNumber));
                            phoneNum.push(obj.phoneNumber[0]);

                            //Bank Detail
                            if (count === 1) {

                                //Address Detail
                                var Address = obj.addresses;
                                var AddressID = Address[0].AddressID;
                                var Country = Address[0].Country;
                                var CountryCode = Address[0].CountryCode;
                                var Type = Address[0].Type;
                                var AddressLine1 = Address[0].AddressLine1;
                                var AddressLine2 = Address[0].AddressLine2;
                                var AddressLine3 = Address[0].AddressLine3;
                                var AddressLine4 = Address[0].AddressLine4;
                                var City = Address[0].City;
                                var Region = Address[0].Region;
                                var State = Address[0].State;
                                var PostalCode = Address[0].PostalCode;
                                var FormattedAddress = Address[0].FormattedAddress;
                                var IsPrimary = Address[0].IsPrimary;
                                var EffectiveDate = Address[0].EffectiveDate;
                                $('.addressModel').attr("data-json", JSON.stringify(obj.addresses[0]));
                                setAddress(AddressID, Country, CountryCode, Type, AddressLine1, AddressLine2, AddressLine3, AddressLine4, City, Region, State, PostalCode, FormattedAddress, IsPrimary, EffectiveDate);
                                // Personal Detail
                                var beneficiary = obj.beneficiary;
                                var PhoneNumbers = obj.phoneNumber;
                                var Emails = obj.emails;
                                var Email = Emails[0].user_name;
                                var Id = beneficiary.Id;
                                var Name = beneficiary.Name;
                                var PhoneNumber = PhoneNumbers[0].TelMobile;

                                var IrdNumber = beneficiary.IrdNumber;
                                var AccessLevel = beneficiary.AccessLevel;
                                var AMLEntityType = beneficiary.AMLEntityType;
                                var Status = beneficiary.Status;
                                var DateOfBirth = beneficiary.DateOfBirth;
                                var pirRate = "";
                                if (beneficiary.pirRate !== null || beneficiary.pirRate !== "") {
                                    pirRate = (beneficiary.pirRate * 100) + "%";
                                }
                                $('.phoneModel').attr("data-json", JSON.stringify(obj));
//                               alert("data--"+JSON.stringify(obj));
                                setAbout(Id, Name, Email, PhoneNumbers[0], IrdNumber, AccessLevel, AMLEntityType, Status, DateOfBirth, pirRate);
                                if (obj.bankAccountDetails.length > 0) {
                                    var BankAccountDetail = obj.bankAccountDetails;

                                    var AccountName = BankAccountDetail[0].AccountName;
                                    var BankName = BankAccountDetail[0].BankName;
                                    var Branch = BankAccountDetail[0].Branch;
                                    var Account = BankAccountDetail[0].Account;
                                    var Suffix = BankAccountDetail[0].Suffix;
                                    var Currency = BankAccountDetail[0].Currency;
                                    var bankStatus = BankAccountDetail[0].Status;
                                    var bankType = BankAccountDetail[0].Type;
                                    var bankIsPrimary = BankAccountDetail[0].IsPrimary;
                                    $('.bankModel').attr("data-json", JSON.stringify(obj.bankAccountDetails[0]));
                                    setBankDetail(AccountName, BankName, Branch, Account, Suffix, Currency, bankStatus, bankType, bankIsPrimary);
                                }
                            } else {

                                var x = document.getElementById("myslide-clone").cloneNode(true);
                                var slide = document.getElementById("currentslide").cloneNode(true);

                                $(x).closest('.mySlides').hide();
                                $(x).closest('.mySlides').removeAttr("id");
                                //                                x.getElementsByClassName("myslides")[0].style.display="none";
                                //                                x.getElementsByClassName("myslides")[0].removeAttribute()('id');
                                $(x).find(".ben-Id").html(obj.beneficiary.Id);
                                $(x).find(".ben-Name").html(obj.beneficiary.Name);
                                $(x).find(".ben-Email").html(obj.emails[0].Email);
                                $(x).find(".ben-PhoneNumber").html(obj.phoneNumber[0].Number);
                                $(x).find(".ben-DateOfBirth").html(obj.beneficiary.DateOfBirth);
                                $(x).find(".Country").html(obj.addresses[0].Country);
                                $(x).find(".CountryCode").html(obj.addresses[0].CountryCode);
                                $(x).find(".Type").html(obj.addresses[0].Type);
                                $(x).find(".AddressLine1").html(obj.addresses[0].AddressLine1);
                                $(x).find(".AddressLine2").html(obj.addresses[0].AddressLine2);
                                $(x).find(".AddressLine3").html(obj.addresses[0].AddressLine3);
                                $(x).find(".AddressLine4").html(obj.addresses[0].AddressLine4);
                                $(x).find(".City").html(obj.addresses[0].City);
                                $(x).find(".Region").html(obj.addresses[0].Region);
                                $(x).find(".PostalCode").html(obj.addresses[0].PostalCode);
                                $(x).find(".FormattedAddress").html(obj.addresses[0].FormattedAddress);
                                $(x).find(".IsPrimary").html(obj.addresses[0].IsPrimary);
                                $(x).find(".bank-AccountName").html(obj.bankAccountDetails[0].AccountName);
                                $(x).find(".bank-Bank").html(obj.bankAccountDetails[0].Bank);
                                $(x).find(".bank-Account").html(obj.bankAccountDetails[0].Account);
                                $(x).find(".bank-Suffix").html(obj.bankAccountDetails[0].Suffix);
                                $(x).find(".bank-Currency").html(obj.bankAccountDetails[0].Currency);
                                $(x).find(".bank-Status").html(obj.bankAccountDetails[0].Status);
                                $(x).find(".bank-Type").html(obj.bankAccountDetails[0].Type);
                                $(x).find(".bank-IsPrimary").html(obj.bankAccountDetails[0].IsPrimary);
                                $(x).find(".beneId").attr('value', obj.beneficiary.Id);
                                $(x).find(".phoneModel").attr("data-json", JSON.stringify(obj.beneficiary));
                                $(x).find(".addressModel").attr("data-json", JSON.stringify(obj.addresses[0]));
                                $(x).find(".bankModel").attr("data-json", JSON.stringify(obj.bankAccountDetails[0]));
                                //                                console.log(x.innerHTML); $(".AddressID").html(AddressID);
                                $(".slideshow-container").append(x);
                                $(slide).closest('.dot').removeAttr("id");
                                $(slide).closest('.dot').removeAttr("onclick");
                                $(slide).closest('.dot').attr("onclick", "currentSlide(" + count + ")");
                                $('.slider-dot').append(slide);
                            }

                            count++;
                        }, error: function (jqXHR, textStatus, errorThrown) {
//                            alert(textStatus);
                        }
                    });
            </c:forEach>


                });
                function phoneModel(ele) {
//                     alert(ele);
                    var phoneobj = $(ele).data('json');
                    console.log("===========================+++++++++++++++++++++++++++++++++++++" + JSON.stringify(phoneobj));
//                  alert(JSON.stringify(phoneobj.phoneNumber[0].TelMobile));
                    $('#mobileNo').val(phoneobj.phoneNumber[0].TelMobile);
                    $('#beneId').val(phoneobj.beneficiary.Id);
                    $('#HomeNumber').val(phoneobj.phoneNumber[0].TelHome);
                    $('#BusinessNumber').val(phoneobj.phoneNumber[0].TelBusiness);
                }
                function addressModel(ele) {
                    var addressobj = $(ele).data("json");
//                  alert(JSON.stringify(addressobj.BeneficiaryId));
                    $('#addressId').val(addressobj.AddressID);
                    $('#beneIdAdd').val(addressobj.BeneficiaryId);
                    $('#countryname2').val(" New Zealand");
//                  $('#address').val(addressobj.AddressLine1);
//                  $('#address1').val(addressobj.AddressLine2);
//                  $('#address2').val(addressobj.AddressLine3);
//                  $('#address3').val(addressobj.AddressLine4);
//                  $('#city').val(addressobj.City);
//                  $('#region').val(addressobj.Region);
////                  $('#state').val(addressobj.);
//                  $('#potalcode').val(addressobj.PostalCode);
//                  $('#effectivedate').val(addressobj.);

                }
                function bankModel(ele) {
                    var bankobj = $(ele).data("json");
//alert(bankobj.Bank);
                    $("#other_other_name").val('');
                    $(".toggal_other").hide();
                    $(".bank_name1").val('');
//                    $("div.mm-dropdown .option").val('-Select-');
                    $('div.mm-dropdown .textfirst').html('-Select-');

                    console.log("===========================+++++++++++++++++++++++++++++++++++++" + JSON.stringify(BankCode));
//                    $("#bank_name").val(bankobj.Bank);
                    var bankName = BankCode[parseInt(bankobj.Bank)];
//                    $("#bank_name").data(bankobj.Bank,'id');
                    $("#acount_holder_name").val(bankobj.AccountName);
//                    $("#account_number").val(bankobj.Account);
//                    $("#Branch").val(bankobj.Branch);
//                    $("#Suffix").val(bankobj.Suffix);
//                    $("#Currency").val(bankobj.Currency);
                    $("#investIdBank").val(bankobj.InvestmentCode);
                    $("#beneIdBank").val(bankobj.BeneficiaryId);
                    if (bankobj.Bank !== "" || bankobj.Bank !== null) {
                        var objSelect = document.getElementById("bank_name");
//                    function setSelectedValue(selectObj, valueToSet) {

                        for (var i = 0; i < objSelect.options.length; i++) {
//                            alert();
//                            alert(objSelect.options[i].text);
//                            alert(objSelect.options[i].values() +"--------"+bankobj.Bank);
                            if (objSelect.options[i].text === bankName) {
//                                alert(objSelect.options[i].values);
                                objSelect.options[i].selected = true;
                                return;
                            }
                        }
                    }


                }

        </script>
        <script>
//            function setAbout(Id, Name, Email, PhoneNumber, IrdNumber, AccessLevel, AMLEntityType, Status, DateOfBirth) {
//                $(".ben-Id").html(Id);
//                $(".ben-Name").html(Name);
//                $(".ben-Email").html(Email);
//                $(".ben-PhoneNumber").html(PhoneNumber);
//                $(".ben-IrdNumber").html(IrdNumber);
//                //                $(".ben-AccessLevel").html(AccessLevel);
//                //                $(".ben-AMLEntityType").html(AMLEntityType);
//                $(".ben-Status").html(Status);
//                $(".ben-DateOfBirth").html(DateOfBirth);
//            }

            function setAbout(Id, Name, Email, PhoneNumber, IrdNumber, AccessLevel, AMLEntityType, Status, DateOfBirth, PIRRate) {
                $(".ben-Id").html(Id);
                $(".ben-Name").html(Name);
                $(".ben-Email").html(Email);
                $(".ben-PhoneNumber").html(PhoneNumber.TelMobile);
                if (PhoneNumber.TelHome === null || PhoneNumber.TelHome.trim() === "") {
                    $(".homeNum").hide();
                }

                if (PhoneNumber.TelBusiness === null || PhoneNumber.TelBusiness.trim() === "") {
                    $(".BusinessNum").hide();
                }
                $(".ben-HomeNumber").html(PhoneNumber.TelHome);
                $(".ben-BusinessNumber").html(PhoneNumber.TelBusiness);
                $(".ben-IrdNumber").html(IrdNumber);
                //                $(".ben-AccessLevel").html(AccessLevel);
                //                $(".ben-AMLEntityType").html(AMLEntityType);
                $(".ben-Status").html(Status);
                $(".ben-PIR").html(PIRRate);
                $(".ben-DateOfBirth").html(DateOfBirth);
            }

            function setAddress(AddressID, Country, CountryCode, Type, AddressLine1, AddressLine2, AddressLine3, AddressLine4, City, Region, State, PostalCode, FormattedAddress, IsPrimary, EffectiveDate) {
                $(".AddressID").html(AddressID);
                $(".Country").html(Country);
                $(".CountryCode").html(CountryCode);
                $(".Type").html(Type);
                $(".AddressLine1").html(AddressLine1);
                $(".AddressLine2").html(AddressLine2);
                $(".AddressLine3").html(AddressLine3);
                $(".AddressLine4").html(AddressLine4);
                $(".City").html(City);
                $(".Region").html(Region);
                $(".State").html(State);
                $(".PostalCode").html(PostalCode);
                $(".FormattedAddress").html(FormattedAddress);
                $(".IsPrimary").html(IsPrimary);
                $(".EffectiveDate").html(EffectiveDate);
            }

            function setBankDetail(AccountName, BankName, Branch, Account, Suffix, Currency, bankStatus, bankType, bankIsPrimary) {
                $(".bank-AccountName").html(AccountName);
                $(".bank-Bank").html(BankName);
                $(".bank-Branch").html(Branch);
                $(".bank-Account").html(Account);
                $(".bank-Suffix").html(Suffix);
                $(".bank-Currency").html(Currency);
                $(".bank-Status").html(bankStatus);
                $(".bank-Type").html(bankType);
                $(".bank-IsPrimary").html(bankIsPrimary);
            }

        </script>
        <script>
            $(document).ready(function () {


                var readURL = function (input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.profile-pic').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }


                $(".file-upload").on('change', function () {
                    readURL(this);

                });
                $(".upload-button").on('click', function () {
                    $(".file-upload").click();
                });
            });
        </script>
        <script>
            var Chrome = VueColor.Chrome;
            Vue.component('colorpicker', {
                components: {
                    'chrome-picker': Chrome,
                },
                template: `
    <div class="input-group color-picker" ref="colorpicker">
            <input type="text" class="form-control" v-model="colorValue" @focus="showPicker()" @input="updateFromInput" />
            <span class="input-group-addon color-picker-container">
                    <span class="current-color" :style="'background-color: ' + colorValue" @click="togglePicker()"></span>
                    <chrome-picker :value="colors" @input="updateFromPicker" v-if="displayPicker" />
            </span>
    </div>`,
                props: ['color'],
                data() {
                    return {
                        colors: {
                            hex: '#000000',
                        },
                        colorValue: '',
                        displayPicker: false,
                    }
                },
                mounted() {
                    this.setColor(this.color || '#000000');
                },
                methods: {
                    setColor(color) {
                        this.updateColors(color);
                        this.colorValue = color;
                    },
                    updateColors(color) {
                        if (color.slice(0, 1) === '#') {
                            this.colors = {
                                hex: color
                            };
                        } else if (color.slice(0, 4) === 'rgba') {
                            var rgba = color.replace(/^rgba?\(|\s+|\)$/g, '').split(','),
                                    hex = '#' + ((1 << 24) + (parseInt(rgba[0]) << 16) + (parseInt(rgba[1]) << 8) + parseInt(rgba[2])).toString(16).slice(1);
                            this.colors = {
                                hex: hex,
                                a: rgba[3],
                            }
                        }
                    },
                    showPicker() {
                        document.addEventListener('click', this.documentClick);
                        this.displayPicker = true;
                    },
                    hidePicker() {
                        document.removeEventListener('click', this.documentClick);
                        this.displayPicker = false;
                    },
                    togglePicker() {
                        this.displayPicker ? this.hidePicker() : this.showPicker();
                    },
                    updateFromInput() {
                        this.updateColors(this.colorValue);
                    },
                    updateFromPicker(color) {
                        this.colors = color;
                        if (color.rgba.a === 1) {
                            this.colorValue = color.hex;
                        } else {
                            this.colorValue = 'rgba(' + color.rgba.r + ', ' + color.rgba.g + ', ' + color.rgba.b + ', ' + color.rgba.a + ')';
                        }
                    },
                    documentClick(e) {
                        var el = this.$refs.colorpicker,
                                target = e.target;
                        if (el !== target && !el.contains(target)) {
                            this.hidePicker();
                        }
                    }
                },
                watch: {
                    colorValue(val) {
                        if (val) {
                            this.updateColors(val);
                            this.$emit('input', val);
                            //document.body.style.background = val;
                        }
                    }
                },
            });
            new Vue({
                el: '#app',
                data: {
                    defaultColor: '#FF0000'
                }
            });
        </script>
        <script>
            var $affectedElements = $("h6, h5, h4, h3, h2, h1, a, label, p"); // Can be extended, ex. $("div, p, span.someClass")

            // Storing the original size in a data attribute so size can be reset
            $affectedElements.each(function () {
                var $this = $(this);
                $this.data("orig-size", $this.css("font-size"));
            });
            $("#btn-increase").click(function () {
                changeFontSize(1);
            });

            $("#btn-decrease").click(function () {
                changeFontSize(-1);
            });

            $("#btn-orig").click(function () {
                $affectedElements.each(function () {
                    var $this = $(this);
                    $this.css("font-size", $this.data("orig-size"));
                });
            });

            function changeFontSize(direction) {
                $affectedElements.each(function () {
                    var $this = $(this);
                    $this.css("font-size", parseInt($this.css("font-size")) + direction);
                });
            }
        </script>
        <script>
            $(".slidernight").click(function () {
                $("body").toggleClass('night-mode');
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".color-red").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "red");
                });
                $(".color-green").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "green");
                });
                $(".color-blue").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "blue");
                });
                $(".color-yellow-red").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "#dc7b17");
                });
                $(".color-purple").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "purple");
                });
                $(".color-pink").click(function () {
                    $(".breadcrumb, .btn-style, .btn-style:hover, .new-notification .checkmark").css("background", "pink");
                });
            });
        </script>

        <script>
            var slideIndex = 1;
            showSlides(slideIndex);
            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function currentSlide(n) {
                showSlides(slideIndex = n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) {
                    slideIndex = 1;
                }
                if (n < 1) {
                    slideIndex = slides.length;
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
        </script>
        <script>

            function  genrateotp(mNo, cCo, sTy) {
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/generateotp?pn=' + mNo + '&cc=' + cCo + '&sTy=' + sTy;
                $.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        $('.opthide').show();
                        $('#otp-qr').hide();
                        $('.enableield').hide();
                        if ($('.enable2FA')[0].checked === true) {
                            $('.continue').val("Enable 2FA");
                        } else {
                            $('.continue').val("Disable 2FA");
                        }
                        $('.continue').show();
                        $('.continue').prop('disabled', true);

//                                               alert("otp genrated" + data);
                    },
                    error: function (e) {
//                        alert("otp failed");
                    }
                });

            }

            function validateOTP(mNo, cCo, otp) {
                var url = 'https://jj1stbnzb3.execute-api.ap-southeast-2.amazonaws.com/otp/verifyotp?pn=' + mNo + '&cc=' + cCo + '&code=' + otp;
                $.ajax({
                    url: url,
                    type: 'POST',
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        console.log(" sucess" + JSON.stringify(data.status));
                        if (data.status === "approved") {
                            $('.enableield').hide();
//                            $('#otp-qr').hide();
//                            alert('validated');
//                            $('.continue').show();
                            $('.continue').prop('disabled', false);
                        } else {
                            $('.msg').html("Wrong OTP").css({
                                color: 'red',
                                fontSize: "9px"
                            });

                        }
                    },
                    error: function (e) {
//                            alert();
//                            $('.msg').html("Wrong OTP");
//                        alert("otp failed" + JSON.stringify(e));
                    }
                });
            }



        </script>

        <script>
            var x = "";
            $(document).ready(function () {
//                $('.enable2FA').prop('disabled', true);

                $('#otp-qr').pincodeInput({hidedigits: false, inputs: 6});
                $('.opthide').hide();
                $('.enableield').hide();
                $('.continue').hide();
                $('.barcodeDiv').hide();
                $('.2faStatus').text('2FA is disabled');
                if (('${user.secret}') !== null && ('${user.secret}') !== "") {
                    $('.enable2FA').prop("checked", true);
                    $('.barcodeDiv').show();
                    $('.2faStatus').text('2FA is enabled');
                    $.get("./genrateCode", function (data) {
                        $("#barcode").append('<img id="barcodeurl" src="' + data.url + '" />');
                        $("#barcode").append('<span class="barcodelabel"> 16-Digit key: </span>');
                        $("#barcode").append('<span class="barcode_digits">' + '${user.secret}' + '</span>');
                    });
                }
            });

//            $('.otpDigits').change(function () {
//
//                var length = document('otpDigits').length;
//
//                for (var i = 0; i < length; i++) {
//                    ot = ot + $('.otpDigits'.val();
//                    alert(ot);
//                }
//                $('.msg').html("");
//                if (ot.length === 6) {
//                    var mNo = $('#mob_number').val();
//                    var cCo = $('.c_code').val();
////                    var otp = $('#otp-qr').val();
//                    cCo = cCo.replace(/\+/g, "");
//                    validateOTP(mNo, cCo, ot);
//                    ot = "";
//                }
//            });


            function  otp() {
                var mNo = $('#mob_number').val();
                var cCo = $('.c_code').val();
                var sTy = "sms";
                cCo = cCo.replace(/\+/g, "");
                genrateotp(mNo, cCo, sTy);
//                x = setInterval(function () {
//                  valOtp() ;
//                }, 1000);
            }

            function valOtp() {
//            $('.otpkey').keyup(function () {
                $('.msg').html("");

                if ($('#otp-qr').val().length === 6) {
                    var mNo = $('#mob_number').val();
                    var cCo = $('.c_code').val();
                    var otp = $('#otp-qr').val();
                    cCo = cCo.replace(/\+/g, "");
                    validateOTP(mNo, cCo, otp);
                } else {
                    $('.msg').html("Wrong OTP").css({
                        color: 'red',
                        fontSize: "9px"
                    });
                }
            }
//            });
    function removeerror(){
       alert();
         $('.msg').text("");
   };


            function enable2FA() { 
                if ($('.enable2FA')[0].checked   &&  $('.2faStatus').text() === '2FA is disabled' || $('.2faStatus').text() ===  '2FA is enabled'  &&  !$('.enable2FA')[0].checked ) {
//                    alert(JSON.stringify($('.enable2FA')[0].checked));
                    $('.enableield').show();
                } else {
                    $('.enableield').hide();
                }
                $('.opthide   .pincode-input-container .pincode-input-text').val("");
                $('.msg').text("");
                console.log(JSON.stringify(phoneNum));
                var CountryCode;
                var number;
                for (var i = 0; i < phoneNum.length; i++) {
                    var numb = phoneNum[i].TelMobile;
                    var CntCode = phoneNum[i].Country.trim();
                    console.log("0new----------" + JSON.stringify(cCode));
                    CntCode = cCode[CntCode].toLowerCase();
                    if (numb !== null && parseInt(numb) > 0) {
                        number = numb;
                        CountryCode = CntCode;
                        break;
                    } else {
                        number = numb;
                        CountryCode = CntCode;
                    }
                }
//                 CountryCode = phoneNum[0].CountryCode.toLowerCase();
                $('.c_code').intlTelInput_1({
                    preferredCountries: [CountryCode]
                });
                var lastThree = number.substr(number.length - 3);
                var secretNum = "";
                for (var i = 0; i < number.length - 3; i++) {
                    secretNum = secretNum + "*";
                }
                $('#mob_number').val(number);
                $('#sec_number').val(secretNum + lastThree);
                $('.continue').hide();
                $('.opthide').hide();
            }
            ;
            function  genrateQr() {
                $('.enable2FA').prop('disabled', true);
                if ($('.enable2FA')[0].checked === true) {
                    getQRCode('${id}');
                    $('#otp-qr').val("");

                } else {
                    $('#otp-qr').val("");
                    remove2fa('${id}');
                }
            }
            ;
            function remove2fa(user) {
                $.get("./disbale2fa?username=" + user, function (data) {
                    $("#barcode").html("");
                    $('.2faStatus').text('2FA is disabled');
                    $("#barcode").html("");
                    $('.enableield').hide();
                    $('.barcodeDiv').hide();
                    $('.enable2FA').prop('disabled', false);
                });
                $('.opthide').hide();
                $('.continue').hide();
            }
            function getQRCode(email) {
                $.get("./qrcode?username=" + email, function (data) {
                    console.log(data);
                    $('.barcodeDiv').show();
                    $("#barcode").append('<img id="barcodeurl" src="' + data.url + '" />');
                    $("#barcode").append('<span class="barcodelabel"> 16-Digit key: </span>');
                    $("#barcode").append('<span class="barcode_digits">' + data.key + '</span>');
                    $('.enableield').hide();
                    $('.2faStatus').text('2FA is enabled');
                    $('.enable2FA').prop('disabled', false);
                });
                $('.opthide').hide();
                $('.continue').hide();
            }
            $('.dataerror').keyup(function () {
                $('.error').text("");
            });
        </script>

        <script>
            $('.phone-submit').click(function () {
                var code = $("#countryCode").val();
                var mNumber = $("#mobileNo").val();
                var HomeNumber = $("#HomeNumber").val();
                var BusinessNumber = $("#BusinessNumber").val();

                var type = $("#Type").val();
                var beneId = $("#beneId").val();

                if (mNumber === "") {
                    $(".mobileUpdate-error").text('This field is required');
                } else {
                    //        alert(beneId);
                    var obj = {country: "", countryCode: code, number: mNumber, type: type, beneficiaryId: beneId, telHome: HomeNumber, telBusiness: BusinessNumber};
                    //        alert(JSON.stringify(obj));
                    console.log(JSON.stringify(obj));
                    $.ajax({
                        type: 'POST',
                        url: './rest/groot/db/api/phoneNumber-registeration',
                        headers: {"Content-Type": 'application/json'},
                        data: JSON.stringify(obj),
                        success: function (data, textStatus, jqXHR) {
                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            console.log(data);
                            location.reload();
                        }, error: function (jqXHR, textStatus, errorThrown) {
//                        alert(textStatus);
                        }
                    });
                }
            });

        </script>
        <script>
            $('.digit-group').find('input').each(function () {
                $(this).attr('maxlength', 1);
                $(this).on('keyup', function (e) {
                    var parent = $($(this).parent());

                    if (e.keyCode === 8 || e.keyCode === 37) {
                        var prev = parent.find('input#' + $(this).data('previous'));

                        if (prev.length) {
                            $(prev).select();
                        }
                    } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                        var next = parent.find('input#' + $(this).data('next'));

                        if (next.length) {
                            $(next).select();
                        } else {
                            if (parent.data('autosubmit')) {
                                parent.submit();
                            }
                        }
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".nav-link").click(function () {
                    $(".nav-link").removeClass("active");
                    $(this).addClass("active");
                });
            });

        </script>  

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
    </body>
</html>