<%-- 
    Document   : temp2FA
    Created on : 27 Sep, 2019, 9:39:33 AM
    Author     : innovative002
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<c:set var="home" value="${pageContext.request.contextPath}"/>--%>
<!DOCTYPE html>
<html xmlns:layout="https://www.thymeleaf.org" xmlns:th="https://www.thymeleaf.org">
    <head>
        <title>Invsta</title>
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
       <jsp:include page="header_url.jsp"></jsp:include>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        
       


        <style>
            .profile-weight {
                font-weight: 500;
            }
            .tabpill-margin {
                margin-top: 20px;
            }
            .image-select {
                width: 100%;
                font-size: 13px;
                position: relative;
                top: -5px;
            }
            .profile-pic {
                max-width: 200px;
                max-height: 200px;
                display: block;
                width: 100%;
                height: 100%;
                object-fit: cover;
                border-radius: 50%;
            }

            .file-upload {
                display: none;
            }
            .profile-circle {
                border-radius: 1000px !important;
                overflow: hidden;
                width: 200px;
                height: 200px;
                border: 8px solid rgb(147, 219, 208);
                max-width: 200px;
                margin: auto;

            }

            .upload-button {
                font-size: 1.2em;
            }

            .upload-button:hover {
                transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
                color: #999;
            }
            .emp-profile {
                padding: 3%;
                margin-top: 3%;
                margin-bottom: 3%;
                border-radius: 0.5rem;
                background: #fff;
            }
            /*start setting checkbox*/
            .new-notification .custom-check {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 16px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
            .new-notification .custom-check input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }
            .new-notification .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 23px;
                width: 23px;
                background-color: #eee;
            }
            .new-notification .custom-check:hover input ~ .checkmark {
                background-color: #ccc;
            }
            .new-notification .custom-check input:checked ~ .checkmark {
                background-color: #93dbd0;
            }
            .new-notification .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }
            .new-notification .custom-check input:checked ~ .checkmark:after {
                display: block;
            }
            .new-notification .custom-check .checkmark:after {
                left: 8px;
                top: 4px;
                width: 8px;
                height: 12px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            .color-take {
                background: #e9eaed;
                width: 50px;
                height: 45px;
                cursor: pointer;
                max-width: 90%;
            }
            .color-line {
                display: flex;
                margin-bottom: 15px;
            }
            .color-red {
                background: red;
            }
            .color-green {
                background: green;
            }
            .color-blue {
                background: blue;
            }
            .color-yellow-red {
                background: #dc7b17;
            }
            .color-purple {
                background: purple;
            }
            .color-pink {
                background: pink;
            }
            .vc-chrome {
                position: absolute;
                top: 35px;
                right: 0;
                z-index: 9;
                width: 100%
            }
            .current-color {
                display: inline-block;
                width: 16px;
                height: 16px;
                background-color: #000;
                cursor: pointer;
            }
            .submit_btn_new {
                display: table;
                margin-left: auto;
            }
            /*end setting checkbox*/
            .night-mode{
                filter: invert(100%);
                background-color: #000;
            }

            .night-mode img {
                filter: invert(100%);
            }
            .day-night-mode a {
                color: #fff!important;
                cursor: pointer;
            }

        </style>
        <style>

            .topnav {
                overflow: hidden;
                background-color: #e9e9e9;
            }

            .topnav a {
                width: 100%;
                float: left;
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
                text-align: left;
            }
            .reslut h4 {
                font-size: 16px;
                padding: 10px;
                background: #65b9ac;
                color: white;
                text-align: center;
            }
            .topnav a:hover {
                background-color: #ddd;
                color: black;
            }

            .topnav a.active {
                background-color: #2196F3;
                color: white;
            }



            .topnav input[type=text] {
                padding: 6px;
                margin-top: 8px;
                font-size: 17px;
                border: none;
            }

            .topnav .search-container button {
                background: no-repeat;
                border: none;
                position: absolute;
                top: 20px;
                right: 20px;
            }

            .topnav .search-container button:hover {
                background: white;
                color: #65b9ac;
            }

            @media screen and (max-width: 600px) {
                .topnav .search-container {
                    float: none;
                }
                .topnav a, .topnav input[type=text], .topnav .search-container button {
                    float: none;
                    display: block;
                    text-align: left;
                    width: 100%;
                    margin: 0;
                    padding: 14px;
                }
                .topnav input[type=text] {
                    border: 1px solid #ccc;  
                }
            }
            .search-container {
                display: flex;
                max-width: 100%;
                /* display: table; */
                position: relative;
            }
            .topnav input[type=text] {
                margin: 0;
                padding: 20px;
                font-size: 17px;
                height: 61px;
                outline: none;
                border-radius: 50px;
                border: none;
                box-shadow: 0px 0px 6px 2px #00000073;
            }
            .topnav {
                width: 100%;
                float: left;
            }

            fieldset {
                float: left;
                width: 100%;
            }

            .company-reg h1 {
                margin: 0;
            }
            .company-reg lable {
                font-size: 16px;
                padding: 0px 6px;
                font-weight: 500;
                color: white;
            }

            .topnav {
                background: #93dbd0;
                padding: 42px;
                margin-bottom: 15px;
            }
            .search-container1{
                max-width: 85%;
                margin: auto;
            }
            div#detectcompany {
                background: white;
                float: left;
                width: 100%;
            }
            .checknear {
                width: 100%;
                float: left;
                border-bottom: 1px solid;
            }
            .reslut {
                width: 100%;
                float: left;
                border: 1px solid #65b9ac;
                height: 400px;
                overflow: auto;
                margin-bottom: 20px;
                    background: #f9f9f9;
            }
            .showshareholderdiv p {
                display:none;
                margin-left: 20px;
            }
            .showshareholderdiv.intro p {
                display:block;
            }
            .resulld {
                margin: auto;
                max-width: 80%;
                width: 100%;
                color: black;
                display: table;
                background: red;
                padding: 16px;
                 box-shadow: 0px 0px 6px 2px #00000073;
            }
            .resulld {
                background: #fff;
            }
            .showshareholderdiv.intro h3:after {

                /* background: red; */

                display: block;

                height: 10px;

                font-family: FontAwesome;

                content: "\f056";

                font-family: FontAwesome;

                content: "\f056";

                position: absolute;

                right: 45px;

                top: 0;

                font-size: 20px;
                font-size: 25px;
                color: #93dbd0;
            }
            .showshareholderdiv h3:after {

                /* background: red; */

                display: block;

                height: 10px;

                font-family: FontAwesome;

                content: "\f055";

                font-family: FontAwesome;

                content: "\f055";

                position: absolute;

                right: 45px;

                top: 0;

                font-size: 25px;
                color: #93dbd0;
            }
            /*            .search-container:hover {
                            border: 6px solid #65b9ac;
                            box-shadow: 0px 0px 7px 4px #249180;
                        }*/
            .search-container button {
                outline: none;
            }
            .showdirector.intro h3:after {

                /* background: red; */

                display: block;

                height: 10px;

                font-family: FontAwesome;

                content: "\f056";

                font-family: FontAwesome;

                content: "\f056";

                position: absolute;

                right: 45px;

                top: 0;

                font-size: 20px;
            }
            .showdirector h3:after {

                /* background: red; */

                display: block;

                height: 10px;

                font-family: FontAwesome;

                content: "\f055";

                font-family: FontAwesome;

                content: "\f055";

                position: absolute;

                right: 45px;

                top: 0;

                font-size: 20px;
            }
            h3.showshareholder {
                position: relative;
                padding: 10px;
                font-size: 16px;
            }

            .showshareholderdiv {
                padding: 14px;
            }


            .showshareholderdiv {
                border-top: 1px solid #ccc;
              
            }
            .totalnumbershare {
    padding: 0px 20px;
}
.totalnumbershare {
    padding: 0px 130px;
    display: flex;
    justify-content: space-between;
}

            div#resultdata {}

            h6.showdiradd {
                display: inline-block;
                margin-right: 17px;
                font-size: 14px;
            }
            h3.showdirector {
                position: relative;
                font-size: 16px;
                padding: 10px;
            }
            div#detectcompany {
                max-width: 74%;
                background: white;
                float: left;
                width: 100%;
                position: absolute;
                z-index: 1000000014;
                margin-left: 40px;
            }
            .search-container1 {
                max-width: 85%;
                margin: auto;
                /* position: relative; */
                z-index: 111111111111;
            }
            .add-one-1 {
                height: auto;
            }
            .head-breadc {
                margin-bottom: 10px;
            }
            .company-reg img {
                width: 60px;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                font-size: 10px;
                z-index: 9999;
                background: url('./resources/images/spinner-icon-gif-10.jpg') 50% 50% no-repeat rgb(249,249,249);
                opacity: .8;
                background-size: 150px;
            }
            .company-reg {
                text-align: center;
            }
.under-line-companies {
    background: white;
    height: 4px;
    width: 240px;
    display: table;
    margin: auto;
    border-radius: 6px;
    margin-bottom: 20px;
}
.company-name h3 {
    padding: 0px 30px;
    font-weight: 400;
    font-size: 20px;
    border-bottom: 1px solid;
    padding-bottom: 10px;
    border-bottom-width: 3px;
}
.showcodediv p {
    padding: 10px 30px;
}

        </style>
    </head>
    <body>
        <div class="loader"></div>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <%--<jsp:include page="header.jsp" />--%>  
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
            -------------------->
                    <ul class="breadcrumb head-breadc">
                        <li class="breadcrumb-item ">
                            <a href="./home">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">search-company</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)"></a>
                        </li>
                    </ul>




                    <div class="content-i">

                        <div class="topnav">
                            <div class="company-reg">
                                <h1>COMPANIES</h1>
                                <!--<img src="./resources/images/arroww.gif"></img>-->
                                <!--<lable>REGISTER</lable>-->
                            </div>
                            <div class="under-line-companies"></div>
                            <div class="search-container1">
                                <div class="search-container">

                                    <input type="text" placeholder="Search.." class="search" name="search">
                                    <button type="submit" class="details"><i class="fa fa-search"></i></button>

                                </div>
                                <div id="detectcompany"></div>
                            </div>

                        </div>

                        <div class="resulld">
                            <div class="row">
                                <div class="col-md-12">
                                   <div id="" class="reslut add-one-1">
                                         <h4>Company Summary</h4>
                                         <div class="company-name companyName">                                             
                                         </div>

                                    <div class="showcodediv" >
                                        <!--<p class="showshareholder">-->
<!--                                        <h6 class="showdiradd">comapany number:</h6>2345672</p>
                                        <p class="showshareholder"><h6 class="showdiradd">Incorporation Date: </h6>30 Sep 2009</p>
                                        <p class="showshareholder"><h6 class="showdiradd">Company Status:</h6>Removed</p>-->
                                        <p class="showcode" id="showcode"></p>
                                    </div>
                                       </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="resultdata2" class="reslut add-one-1">
                                        <h4>Beneficial Owner</h4>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="resultdata" class="reslut add-one-2">
                                        <h4>Shareholders</h4>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="resultdata1" class="reslut add-one-3">
                                        <h4>Director</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

 <jsp:include page="footer.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://backoffice.invsta.io/pocv/resources/bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" ></script>

    <script>
        var data;
        var token;
        $(document).ready(function () {
            $.ajax({
                url: 'https://toyfjejt90.execute-api.ap-southeast-2.amazonaws.com/prod/companysearch',
                headers: {"Content-Type": 'application/json'},
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    token = data;
                }, error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        });

        $('.search').keyup(function () {
            data = $('.search').val();
            if(data.trim()===""){
                $('#detectcompany').html('');
            }
             $('.companyName').html("");
            $('#showcode').html("");
            getData();
        });
        function getData() {
            $('#resultdata').html(' <h4>ShareHolder</h4>');
            $('#resultdata1').html('<h4>Director</h4>');
            $('#resultdata2').html('<h4>Beneficial Owner</h4>');
            $.ajax({
                url: 'https://api.business.govt.nz/services/v4/nzbn/entities?search-term=' + data,
                headers: {
                    'Authorization': `Bearer ` + token
                },
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    $('#detectcompany').html('');
                    var obj = JSON.stringify(data);
                    console.log(obj);
                    obj = JSON.parse(obj);


                    for (var i = 0; i < obj.items.length; i++) {
                        var div = document.createElement('div');
                        div.classList.add("checknear");
                        document.getElementById('detectcompany').appendChild(div);
                        var item = obj.items[i];
                        console.log(item.entityName);

                        var a = document.createElement('a');

                        a.innerHTML = item.entityName;
                        a.classList.add("clickcompany");
                        a.setAttribute("href", "javascript:void(0)");
                        a.setAttribute("onclick", "clickcompany(this)");
                        div.appendChild(a);
                        var span = document.createElement('span');
                        var code = item.nzbn;
                        span.innerHTML = code;
                        span.classList.add("spancode");
                        span.style.display = "none";
                        div.appendChild(span);


                    }
                }, error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        }

        function clickcompany(ele) {
            $('#detectcompany').html('');
            $(".loader").fadeIn("slow");
            var code = $(ele).closest('.checknear').find('.spancode').text();
           var compoanyname= $(ele).text();
           $('.companyName').html("<h3>"+compoanyname+"</h3>");
           $('.search').val(compoanyname);
            $('#showcode').html("<h6 class='showdiradd' >NZBN:</h6>"+code);
//                alert(code);
            getdirector(code);
            var link = 'https://api.business.govt.nz/services/v4/nzbn/entities/' + code + '/company-details';
            $.ajax({
                url: link,
                headers: {
                    'Authorization': `Bearer ` + token
                },
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    $(".loader").fadeOut("fast");
                    var obj = JSON.stringify(data);
                    console.log(obj);
                    obj = JSON.parse(obj);
                  
                    var numberOfShares = obj.shareholding.numberOfShares;
                    var div = document.createElement('div');
                    div.classList.add("totalnumbershare");
                    div.innerHTML="<h6>Total number of share: </h6>"+numberOfShares;
                    document.getElementById('resultdata').appendChild(div);
                    for (var i = 0; i < obj.shareholding.shareAllocation.length; i++) {
                        var div = document.createElement('div');
                        div.classList.add("showshareholderdiv");
                        div.setAttribute("onclick", "showshareholderdiv(this)");
                        document.getElementById('resultdata').appendChild(div);
                        var shareAllocation = obj.shareholding.shareAllocation[i];
                        var allocation = shareAllocation.allocation;
                        var percentage = parseInt(allocation) / parseInt(numberOfShares) * 100;
                        var p2 = document.createElement('h3');
                        var k = i + 1;
                        p2.innerHTML = "shareHolder " + k;
                        p2.classList.add("showshareholder");
                        div.appendChild(p2);

                        var p = document.createElement('p');
                        p.innerHTML = "<h6 class='showdiradd'>Allocation :</h6> " + allocation;
                        p.classList.add("showshareholder");
                        div.appendChild(p);
                        var p1 = document.createElement('p');
                            p1.innerHTML = "<h6 class='showdiradd'>percentage  : </h6> " + percentage + "%";
                        p1.classList.add("showshareholder");
                        div.appendChild(p1);
                        for (var j = 0; j < shareAllocation.shareholder.length; j++) {
                            var shareholder = shareAllocation.shareholder[j];
                            var fullName = shareholder.individualShareholder.fullName;
                            if (typeof fullName !== 'undefined') {
                                var p = document.createElement('p');
                                p.innerHTML = "<h6 class='showdiradd'>Full Name : </h6> " + fullName;
                                p.classList.add("showshareholder");
                                div.appendChild(p);
                            }
                        }


                    }
                      getbeneficiaryowner(obj);

                }, error: function (jqXHR, textStatus, errorThrown) {
                    $(".loader").fadeOut("fast");


                }
            });

        }
        function getdirector(code) {
            var link = 'https://api.business.govt.nz/services/v4/nzbn/entities/' + code + '/roles';

            $.ajax({
                url: link,
                headers: {
                    'Authorization': `Bearer ` + token
                },
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    var obj = JSON.stringify(data);
                    console.log(obj);
                    obj = JSON.parse(obj);
                    var j=0;
                    for (var i = 0; i < obj.length; i++) {
                        var director = obj[i];
                        if(director.endDate!==null || typeof director.endDate!==null){ 
                            j++;
                        var div = document.createElement('div');
                        div.classList.add("showshareholderdiv");
                        div.setAttribute("onclick", "showshareholderdiv(this)");
                        document.getElementById('resultdata1').appendChild(div);
                        console.log(JSON.stringify(director));
                        var p = document.createElement('h3');
                        p.innerHTML = "Director  " + j;
                        p.classList.add("showdirector");
                        div.appendChild(p);
                        var firstName = director.rolePerson.firstName;
                        var middleNames = director.rolePerson.middleNames;
                        var lastName = director.rolePerson.lastName;
                        var p1 = document.createElement('p');
                        p1.classList.add("showdirector");
                        p1.innerHTML = "<h6 class='showdiradd'>Name  :</h6>" + firstName + " " + middleNames + " " + lastName;
                        div.appendChild(p1);

                        var address1 = director.roleAddress[0].address1;
                        var address2 = director.roleAddress[0].address2;
                        var address3 = director.roleAddress[0].address3;
                        var p2 = document.createElement('p');
                        p2.classList.add("showdirector");
                        p2.innerHTML = "<h6 class='showdiradd'>Address  :</h6>" + address1 + " " + address2 + " " + address3;
                        div.appendChild(p2);
                    }
                    }


                }, error: function (jqXHR, textStatus, errorThrown) {


                }
            });
        }

        function getbeneficiaryowner(obj) {
            var numberOfShares = obj.shareholding.numberOfShares;

            for (var i = 0; i < obj.shareholding.shareAllocation.length; i++) {
                var div = document.createElement('div');
                div.classList.add("showshareholderdiv");
                div.setAttribute("onclick", "showshareholderdiv(this)");
                document.getElementById('resultdata2').appendChild(div);
                var shareAllocation = obj.shareholding.shareAllocation[i];
                var allocation = shareAllocation.allocation;
                var percentage = parseInt(allocation) / parseInt(numberOfShares) * 100;
                if (percentage > 19) {
                    var p2 = document.createElement('h3');
                    var k = i + 1;
                    p2.innerHTML = "Beneficial Owner" + k;
                    p2.classList.add("showshareholder");
                    div.appendChild(p2);

                    var p = document.createElement('p');
                    p.innerHTML = "<h6 class='showdiradd'>Allocation :</h6> " + allocation;
                    p.classList.add("showshareholder");
                    div.appendChild(p);
                    var p1 = document.createElement('p');
                    p1.innerHTML = "<h6 class='showdiradd'>percentage  : </h6> " + percentage + "%";
                    p1.classList.add("showshareholder");
                    div.appendChild(p1);
                    for (var j = 0; j < shareAllocation.shareholder.length; j++) {
                        var shareholder = shareAllocation.shareholder[j];
                        var fullName = shareholder.individualShareholder.fullName;
                        if (typeof fullName !== 'undefined') {
                            var p = document.createElement('p');
                            p.innerHTML = "<h6 class='showdiradd'>Full Name : </h6> " + fullName;
                            p.classList.add("showshareholder");
                            div.appendChild(p);
                        }
                    }
                }


            }
        }
    </script>      


    <script>
        function showshareholderdiv(ele) {
            $('.showshareholderdiv ').removeClass("intro");
            $(ele).addClass("intro");

        }

    </script>


</body>
</html>