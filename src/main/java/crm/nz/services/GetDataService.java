/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.google.gson.Gson;
import crm.nz.authorization.InvestmentsService;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.UpdateUnitsHolding;
import crm.nz.table.bean.SecuredUser;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class GetDataService {

    @Autowired
    private Gson gson;
    @Autowired
    private InvestmentsService investmentService;
    @Autowired
    private GetS3ObjectService s3ObjectService;

    public String getUserId(SecuredUser user, String UserId) {
        if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
            UserId = user.getUser_id();
        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            if (UserId == null || UserId != null && UserId.isEmpty()) {
                UserId = user.getUser_id();
            }
        } else if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            if (UserId == null || UserId != null && UserId.isEmpty()) {
                UserId = user.getUser_id();
            }
        }
        return UserId;
    }

    public Beneficiary beneficiaryByInvestmentCode(String bearerToken, String investmentCode) {
        StringBuilder builder = investmentService.investmentSummary(bearerToken, investmentCode);
        Investment investment = gson.fromJson(builder.toString(), Investment.class);
        return investment.getPrimaryBeneficiary();
    }

    public List<UpdateUnitsHolding> fetchUpdateUnits(String filename) {
        File fetchUpdateUnitsExcelFromS3 = s3ObjectService.fetchUpdateUnitsExcelFromS3(filename);
        try {
            FileInputStream file = new FileInputStream(fetchUpdateUnitsExcelFromS3);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            List<UpdateUnitsHolding> updateUnitsHoldings = new LinkedList<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                System.out.print(row.getRowNum() + ", ");
                UpdateUnitsHolding uuh = new UpdateUnitsHolding();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getColumnIndex()) {
                        case 1:
                            uuh.setIRDNo(cell.getStringCellValue());
                            System.out.print(cell.getStringCellValue() + ", ");
                            break;
                        case 2:
                            uuh.setId(cell.getStringCellValue());
                            System.out.print(cell.getStringCellValue());
                            break;
                    }
                }
                updateUnitsHoldings.add(uuh);
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
