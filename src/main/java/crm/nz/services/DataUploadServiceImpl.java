/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.amazonaws.services.s3.model.S3Object;
import crm.nz.components.FileMethods;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class DataUploadServiceImpl implements DataUploadService {

    @Autowired
    FileMethods fileMethods;

    @Override
    public S3Object storeTos3PicsByBase64(String type, String ImageSource, String irdNumber) {
        try {
            String[] split = ImageSource.split(";base64,");
            String extension = split[0].split("/")[1];
            String imageByte = split[1];
            String idType = null;
            int count = 1;
            if (type.trim().equalsIgnoreCase("NZ Driver Licence")) {
                idType = "DL";
            } else if (type.trim().equalsIgnoreCase("NZ Passport")) {
                idType = "PP";
            } else if (type.trim().equalsIgnoreCase("Birth Certificate")) {
                idType = "BC";
            } else if (type.trim().equalsIgnoreCase("Other")) {
                idType = "Add";
            } else if (type.trim().equalsIgnoreCase("DeedFile")) {
                idType = "DF";
            }
            String fileName = irdNumber + "-" + count + "-" + idType + "." + extension;
            String folderName = "image";
            count++;
            return fileMethods.storeTos3ByBase64(imageByte, extension, fileName, folderName);
        } catch (IOException ex) {
            Logger.getLogger(DataUploadServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public S3Object storeTos3BankDocByBase64(String type, String ImageSource, String irdNumber) {
        try {
            String[] split = ImageSource.split(";base64,");
            String extension = split[0].split("/")[1];
            String imageByte = split[1];
            int count = 1;
            String fileName = irdNumber + "-" + count + "-" + "Bank" + "." + extension;
            String folderName = "Bank_doc";
            count++;
            return fileMethods.storeTos3ByBase64(imageByte, extension, fileName, folderName);
        } catch (IOException ex) {
            Logger.getLogger(DataUploadServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
