/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import crm.nz.components.FileMethods;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class GetS3ObjectService {

    @Autowired
    private FileMethods fileMethods;

    public File fetchUpdateUnitsExcelFromS3(String fileName) {
        File flatFile = fileMethods.fetchDataFromExcelFromS3(fileName);
        return flatFile;
    }
    
}
