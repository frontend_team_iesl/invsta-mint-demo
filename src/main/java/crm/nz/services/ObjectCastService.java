/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import java.io.IOException;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface ObjectCastService {

    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException;

    public <T> T jSONcast(Class<T> clazz, Map map) throws IOException;

    public <T> T jSONcast(Class<T> clazz, Object obj) throws IOException, InstantiationException, IllegalAccessException;
}
