/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import crm.nz.beans.acc.api.UpdateUnitsHolding;
import crm.nz.components.RandomStringGenerator;
import crm.nz.repository.UserUpdatedUnitsRepository;
import crm.nz.security.CurrentUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class UserUpdatedUnitsService implements CurrentUserService {

    @Autowired
    private UserUpdatedUnitsRepository userUpdatedUnitsRepository;
    @Autowired
    private RandomStringGenerator generator;
    @Autowired
    private GetDataService dataService;

    public void executeFlatFile(String fileName) {
        List<UpdateUnitsHolding> fetchUpdateUnits = dataService.fetchUpdateUnits(fileName);
        System.out.println(fetchUpdateUnits.toString());
    }

    public void method2() {
        List<UpdateUnitsHolding> investments = userUpdatedUnitsRepository.getNotExistInvestments(null, null);
        userUpdatedUnitsRepository.saveInvestments(investments);
        List<UpdateUnitsHolding> beneficiaries = userUpdatedUnitsRepository.getNotExistBeneficiaries(null, null);
        userUpdatedUnitsRepository.saveBeneficiaries(beneficiaries);
        List<UpdateUnitsHolding> usernames = userUpdatedUnitsRepository.getNotExistUsernames(null, null);
        usernames.forEach((UpdateUnitsHolding un) -> {
            String Password = generator.generate(12);
            un.setPassword(Password);
        });
        userUpdatedUnitsRepository.saveUsernames(usernames);
        List<UpdateUnitsHolding> userBeneficiaryRelationships = userUpdatedUnitsRepository.getNotExistUserBeneficiaryRelationships(null, null);
        userUpdatedUnitsRepository.saveUserBeneficiaryRelationships(userBeneficiaryRelationships);
    }

}
