/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import crm.nz.beans.acc.api.PortfolioPrice;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author TOSHIBA R830
 */
@Service
public class DataChartService {

    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    PortfolioRepository portfolioRepository;

    public final Map<String, Map<String, TransactionItem>> unitsByDate(String Portfolios[], String ic) {
        Map<String, Map<String, TransactionItem>> units = new HashMap<>();
        if (units.isEmpty()) {
            Map<String, List<TransactionItem>> trans = new HashMap<String, List<TransactionItem>>();
            for (String port : Portfolios) {
                List<TransactionItem> transactions = transactionRepository.getTransactionsbycode(null, null, ic, port);
                trans.put(port, transactions);
                ListIterator<TransactionItem> listIterator = trans.get(port).listIterator();
                Map<String, TransactionItem> temp = new HashMap<String, TransactionItem>();
                while (listIterator.hasNext()) {
                    TransactionItem bean = listIterator.next();
                    temp.put(bean.getEffectiveDate(), bean);
                }
                units.put(port, temp);
            }

        }
        return units;
    }

    public final Map<String, Map<String, PortfolioPrice>> portfolioPriceByDate(String Portfolios[], String min) {
        Map<String, Map<String, PortfolioPrice>> portfolioPrice = new HashMap<>();
        if (portfolioPrice.isEmpty()) {
            Map<String, List<PortfolioPrice>> trans = new HashMap<String, List<PortfolioPrice>>();
            for (String port : Portfolios) {
                List<PortfolioPrice> performancePrices = portfolioRepository.getPerformancePrices(port, min);
                trans.put(port, performancePrices);
                ListIterator<PortfolioPrice> listIterator = trans.get(port).listIterator();
                Map<String, PortfolioPrice> temp = new HashMap<String, PortfolioPrice>();
                while (listIterator.hasNext()) {
                    PortfolioPrice bean = listIterator.next();
                    temp.put(bean.getCreatedDate(), bean);

                }
                portfolioPrice.put(port, temp);
            }
        }
        return portfolioPrice;
    }

    public final Map<String, TransactionItem> TotalUnitswithprice(String Portfolios, String ic) {
        Map<String, TransactionItem> transactionsMap = new TreeMap<String, TransactionItem>();
        if (transactionsMap.isEmpty()) {
            List<TransactionItem> transactions = transactionRepository.getTotalUnitsWithprice(null, null, ic, Portfolios);
            ListIterator<TransactionItem> listIterator = transactions.listIterator();

            while (listIterator.hasNext()) {
                TransactionItem bean = listIterator.next();
                transactionsMap.put(bean.getCreatedDate(), bean);
            }
            return transactionsMap;
        }
        return null;
    }

}