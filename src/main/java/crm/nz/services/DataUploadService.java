/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.amazonaws.services.s3.model.S3Object;

/**
 *
 * @author ADMIN
 */
public interface DataUploadService {

    public S3Object storeTos3PicsByBase64(String type, String ImageSource, String irdNumber);

    public S3Object storeTos3BankDocByBase64(String type, String ImageSource, String irdNumber);
}
