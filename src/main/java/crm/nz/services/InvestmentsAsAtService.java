/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import crm.nz.beans.acc.api.ApiCall;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.PricePortfoliosService;
import crm.nz.authorization.TransactionsService;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioPrice;
import crm.nz.beans.acc.api.Transaction;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.components.CommonMethods;
import crm.nz.repository.CommonRepository;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class InvestmentsAsAtService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private InvestmentsService investmentsService;
    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private InvestmentRepository investmentsRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private PricePortfoliosService pricePortfoliosService;
    @Autowired
    private CommonRepository commonRepository;
    @Autowired
    private PortfolioRepository portfolioRepository;
    @Autowired
    private Gson gson;
    @Autowired
    private CommonMethods common;

    public List<InvestmentHolding> getInvestmentHoldings(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String CalledDate = common.format3(common.today());
        List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, null);
//        if (investmentHoldings.isEmpty()) {
//            List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
//            List<ApiCall> ApiCalls = new LinkedList<>();
//            if (!investments.isEmpty()) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials2();
//                for (Investment investment : investments) {
//                    StringBuilder result2 = investmentsService.investmentHoldingSummaryValues(clientCredentials, investment.getCode());
////we need request for this investment holding summary
//                    Type type = new TypeToken<LinkedList<InvestmentHolding>>() {
//                    }.getType();
//                    List<InvestmentHolding> list = gson.fromJson(result2.toString(), type);
//                    list.forEach((InvestmentHolding inv) -> {
//                        inv.setBeneficiaryId(investment.getBeneficiaryId());
//                        inv.setAsAt(inv.getAsAt() != null && inv.getAsAt().contains("T") ? inv.getAsAt().split("T")[0] : inv.getAsAt());
//                        inv.setDistMethod(inv.getDistMethod() != null ? inv.getDistMethod() : "Reinvested");
//                    });
//                    investmentHoldings.addAll(list);
//                }
//                investmentsRepository.saveInvestmentHolding(investmentHoldings);
//                investmentHoldings.forEach((inv) -> {
//                    ApiCall apiCall = new ApiCall();
//                    apiCall.setActionName("INVESTMENT_HOLDING");
//                    apiCall.setCalledDate(CalledDate);
//                    apiCall.setInvestmentCode(inv.getInvestmentCode());
//                    apiCall.setPortfolioCode(inv.getPortfolioCode());
//                    ApiCalls.add(apiCall);
//                });
//                commonRepository.saveApiCall(ApiCalls);
//            }
//            investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode,null);
//        }
        return investmentHoldings;
    }

    public List<InvestmentPerformance> getInvestmentPerformances(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String CalledDate = common.format3(common.today());
        List<InvestmentPerformance> investmentPerformances = investmentsRepository.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, null);
//        if (investmentPerformances.isEmpty()) {
//            List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
//            List<ApiCall> ApiCalls = new LinkedList<>();
//            if (!investments.isEmpty()) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials2();
//                for (Investment investment : investments) {
//                    StringBuilder result2 = investmentsService.investmentPerformance(clientCredentials, investment.getCode());
////we need request for this investment performances
//                    Type type = new TypeToken<LinkedList<InvestmentPerformance>>() {
//                    }.getType();
//                    List<InvestmentPerformance> list = gson.fromJson(result2.toString(), type);
//                    list.forEach((InvestmentPerformance inv) -> {
//                        inv.setBeneficiaryId(investment.getBeneficiaryId());
//                        inv.setInvestmentCode(investment.getCode());
//                    });
//                    investmentPerformances.addAll(list);
//                }
//                investmentsRepository.saveInvestmentPerformance(investmentPerformances);
//                HashMap<String, Boolean> invMap = new HashMap<>();
//                for (InvestmentPerformance inv : investmentPerformances) {
//                    Boolean get = invMap.get(inv.getInvestmentCode() + ":" + inv.getPortfolio());
//                    if (get == null) {
//                        ApiCall apiCall = new ApiCall();
//                        apiCall.setActionName("INVESTMENT_PERFORMANCES");
//                        apiCall.setCalledDate(CalledDate);
//                        apiCall.setInvestmentCode(inv.getInvestmentCode());
//                        apiCall.setPortfolioCode(inv.getPortfolio());
//                        ApiCalls.add(apiCall);
//                        invMap.put(inv.getInvestmentCode() + ":" + inv.getPortfolio(), true);
//                    }
//                }
//                commonRepository.saveApiCall(ApiCalls);
//            }
//            investmentPerformances = investmentsRepository.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, CalledDate);
//        }
        return investmentPerformances;
    }

    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String CalledDate = common.format3(common.today());
        List<FmcaInvestmentMix> fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, null);
//        if (fmcaInvestmentMix.isEmpty()) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials2();
//            Type type = new TypeToken<LinkedList<FmcaInvestmentMix>>() {
//            }.getType();
//            List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, CalledDate);
//            List<ApiCall> ApiCalls = new LinkedList<>();
//            for (InvestmentHolding investment : investmentHoldings) {
//                StringBuilder result3 = investmentsService.fmcaInvestmentMix(clientCredentials, investment.getInvestmentCode(), investment.getPortfolioCode());
//                fmcaInvestmentMix = gson.fromJson(result3.toString(), type);
//                fmcaInvestmentMix.forEach((FmcaInvestmentMix inv) -> {
//                    inv.setBeneficiaryId(investment.getBeneficiaryId());
//                    inv.setInvestmentCode(investment.getInvestmentCode());
//                });
//                investmentsRepository.saveFmcaInvestmentMix((List<FmcaInvestmentMix>) fmcaInvestmentMix);
//            }
//            HashMap<String, Boolean> invMap = new HashMap<>();
//            for (FmcaInvestmentMix inv : fmcaInvestmentMix) {
//                Boolean get = invMap.get(inv.getInvestmentCode() + ":" + inv.getPortfolio());
//                if (get == null) {
//                    ApiCall apiCall = new ApiCall();
//                    apiCall.setActionName("FMCA_INVESTMENT_MIX");
//                    apiCall.setCalledDate(CalledDate);
//                    apiCall.setInvestmentCode(inv.getInvestmentCode());
//                    apiCall.setPortfolioCode(inv.getPortfolio());
//                    ApiCalls.add(apiCall);
//                    invMap.put(inv.getInvestmentCode() + ":" + inv.getPortfolio(), true);
//                }
//            }
//            commonRepository.saveApiCall(ApiCalls);
//            fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, null);
//        }
        return fmcaInvestmentMix;
    }

    public List<TransactionItem> getInvestmentTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String CalledDate = common.format3(common.today());
        List<TransactionItem> investmentTransactions = transactionRepository.getTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, null);
//        if (investmentTransactions.isEmpty()) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials2();
//            List<Investment> investments = investmentsRepository.getInvestments(UserId, BeneficiaryId);
//            List<TransactionItem> transactionList = new LinkedList<>();
//            List<ApiCall> ApiCalls = new LinkedList<>();
//            for (Investment investment : investments) {
//                TransactionItem investmentTransactions2 = transactionRepository.getTransactions(UserId);
//                boolean resultEqualsTo = true;
//                Integer page = 1;
//                StringBuilder r = transactionsService.investmentTransactions(clientCredentials, investment.getCode(), page);
//                Transaction txn = gson.fromJson(r.toString(), Transaction.class);
//                if (txn.getTotalItemCount() > investmentTransactions2.getTotalItemCount()) {
//                    if (investmentTransactions2.getTotalItemCount() > 0) {
//                        while (resultEqualsTo) {
//                            StringBuilder result4 = transactionsService.investmentTransactions(clientCredentials, investment.getCode(), page, investmentTransactions2.getEffectiveDate());
//                            Transaction transaction = gson.fromJson(result4.toString(), Transaction.class);
//                            transactionList.addAll(transaction.getItems());
//                            if (transaction.getItems().size() == 50) {
//                                resultEqualsTo = true;
//                            } else {
//                                resultEqualsTo = false;
//                            }
//                            page++;
//
//                        }
//                        transactionRepository.saveTransactions(transactionList);
//                    } else {
//                        while (resultEqualsTo) {
//                            StringBuilder result4 = transactionsService.investmentTransactions(clientCredentials, investment.getCode(), page);
//                            Transaction transaction = gson.fromJson(result4.toString(), Transaction.class);
//                            transactionList.addAll(transaction.getItems());
//                            if (transaction.getItems().size() == 50) {
//                                resultEqualsTo = true;
//                            } else {
//                                resultEqualsTo = false;
//                            }
//                            page++;
//
//                        }
//                        transactionRepository.saveTransactions(transactionList);
//                    }
//                }
//            }
//            HashMap<String, Boolean> invMap = new HashMap<>();
//            for (TransactionItem inv : transactionList) {
//                Boolean get = invMap.get(inv.getInvestmentCode() + ":" + inv.getPortfolioCode());
//                if (get == null) {
//                    ApiCall apiCall = new ApiCall();
//                    apiCall.setActionName("INVESTMENT_TRANSACTION");
//                    apiCall.setCalledDate(CalledDate);
//                    apiCall.setInvestmentCode(inv.getInvestmentCode());
//                    apiCall.setPortfolioCode(inv.getPortfolioCode());
//                    ApiCalls.add(apiCall);
//                    invMap.put(inv.getInvestmentCode() + ":" + inv.getPortfolioCode(), true);
//                }
//            }
//            commonRepository.saveApiCall(ApiCalls);
//            investmentTransactions = transactionRepository.getTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//        }
        return investmentTransactions;
    }

    public void saveLatestPortfoliosPrices() {
        String CalledDate = common.format3(common.today());
        List<PortfolioPrice> listPortfolioPrices = new LinkedList<PortfolioPrice>();
        List<ApiCall> ApiCalls = new LinkedList<>();
        List<Portfolio> allPortfolios = portfolioRepository.getAllPortfoliosList();
        String clientCredentials = oAuth2TokenService.getClientCredentials2();
        for (Portfolio portfolio : allPortfolios) {
            StringBuilder result3 = pricePortfoliosService.pricePortfolioLatest(clientCredentials, portfolio.getCode());
            PortfolioPrice listprice = gson.fromJson(result3.toString(), PortfolioPrice.class);
            listprice.setDate(CalledDate);
            if (listprice != null) {
                listPortfolioPrices.add(listprice);
            }
        }
        investmentsRepository.saveLatestPortfoliosPrices(listPortfolioPrices);
        HashMap<String, Boolean> invMap = new HashMap<>();
        for (PortfolioPrice inv : listPortfolioPrices) {
            Boolean get = invMap.get(inv.getPortfolio());
            if (get == null) {
                ApiCall apiCall = new ApiCall();
                apiCall.setActionName("PORTFOLIO_PERFORMANCES");
                apiCall.setCalledDate(CalledDate);
                apiCall.setInvestmentCode("MIN000");
                apiCall.setPortfolioCode(inv.getPortfolio());
                ApiCalls.add(apiCall);
                invMap.put(inv.getPortfolio(), true);
            }
        }
        commonRepository.saveApiCall(ApiCalls);
    }

    public List<PortfolioPrice> getLatestPortfoliosPrices(String PortfolioCode) throws ParseException {
        String CalledDate = common.format3(common.today());
        List<PortfolioPrice> portfolioPrice = investmentsRepository.getLatestPortfolioPrice(null, CalledDate);
        if (portfolioPrice.isEmpty()) {
            saveLatestPortfoliosPrices();
            portfolioPrice = investmentsRepository.getLatestPortfolioPrice(PortfolioCode, CalledDate);
        }
        return portfolioPrice;
    }

    public List<FmcaTopAssets> getFmcaAssets(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String CalledDate = common.format3(common.today());
        List<FmcaTopAssets> FmcaAssets = investmentsRepository.getFmcaAssets(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, CalledDate);

//        if (FmcaAssets.isEmpty()) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials2();
//            Type type = new TypeToken<LinkedList<FmcaTopAssets>>() {
//            }.getType();
//            List<InvestmentHolding> investmentHoldings = investmentsRepository.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, CalledDate);
//            List<ApiCall> ApiCalls = new LinkedList<>();
//            for (InvestmentHolding investment : investmentHoldings) {
//                StringBuilder result3 = investmentsService.fmcaTopAssets(clientCredentials, investment.getInvestmentCode(), investment.getPortfolioCode());
//                FmcaAssets = gson.fromJson(result3.toString(), type);
//                FmcaAssets.forEach((FmcaTopAssets inv) -> {
//                    inv.setInvestmentCode(investment.getInvestmentCode());
//                });
//                investmentsRepository.saveFmcaTopAssets((List<FmcaTopAssets>) FmcaAssets);
//            }
//            HashMap<String, Boolean> invMap = new HashMap<>();
//            for (FmcaTopAssets inv : FmcaAssets) {
//                Boolean get = invMap.get(inv.getInvestmentCode() + ":" + inv.getPortfolio());
//                if (get == null) {
//                    ApiCall apiCall = new ApiCall();
//                    apiCall.setActionName("FMCA_TOP_ASSETS");
//                    apiCall.setCalledDate(CalledDate);
//                    apiCall.setInvestmentCode(inv.getInvestmentCode());
//                    apiCall.setPortfolioCode(inv.getPortfolio());
//                    ApiCalls.add(apiCall);
//                    invMap.put(inv.getInvestmentCode() + ":" + inv.getPortfolio(), true);
//                }
//            }
//            commonRepository.saveApiCall(ApiCalls);
//            FmcaAssets = investmentsRepository.getFmcaAssets(UserId, BeneficiaryId, InvestmentCode, PortfolioCode, CalledDate);
//        }
        return FmcaAssets;
    }

}
