/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;

/**
 *
 * @author IESL
 */
public interface ThirdAPIRegisterService {

    PersonDetailsBean createIndividual(PersonDetailsBean person);

    JointDetailBean createJoint(JointDetailBean joint);

     String updateAddress(Address address);
}
