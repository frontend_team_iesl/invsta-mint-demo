/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import com.google.gson.Gson;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.Application;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.CountryISOCodeBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.components.CommonMethods;
import crm.nz.repository.CommonRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.Optional.empty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class ThirdAPIRegisterServiceImpl implements ThirdAPIRegisterService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private Gson gson;
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public PersonDetailsBean createIndividual(PersonDetailsBean individualAccount) {
        System.out.println("2");
        String clientCredentials = oAuth2TokenService.getClientCredentials();
        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
        Application application = gson.fromJson(createApplication.toString(), Application.class);
        Map<String, CountryISOCodeBean> countryISOCodeMap = common.countryISOCode();
        if (application != null) {
            List<Beneficiary> beneficiaries = application.getBeneficiaries();
            Beneficiary beneficiary = beneficiaries.get(0);
            if (individualAccount.getCountry_residence() != null && !individualAccount.getCountry_residence().isEmpty()) {
                CountryISOCodeBean countryISOCode = countryISOCodeMap.get(individualAccount.getCountry_residence().trim());
                individualAccount.setCodeISO(countryISOCode.getCodeISO());
            }
            onboardingsServices.updateBeneficiary(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPIUpdateDetails(individualAccount));
            StringBuilder AddressId = onboardingsServices.createBeneficiaryAddress(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPICreateAddress(individualAccount));
            StringBuilder PhoneId = onboardingsServices.createBeneficiaryPhone(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPICreatePhone(individualAccount));
            StringBuilder IdentificationId = onboardingsServices.createBeneficiaryIdentification(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPICreateIdentity(individualAccount));
            onboardingsServices.fetchBeneficiaryByIds(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId());
            System.out.println("createIndividual" + application);
            individualAccount.setApplicationId(application.getApplicationId());
            individualAccount.setBeneficiaryId(application.getPrimaryBeneficiaryId());
            if (!AddressId.toString().contains("Status Code: 404")) {
                individualAccount.setAddressId(AddressId.toString());
            }
            System.out.println("AddressId");
            if (!PhoneId.toString().contains("Status Code: 404")) {
                individualAccount.setPhoneId(PhoneId.toString());
            }
            if (!IdentificationId.toString().contains("Status Code: 404")) {
                individualAccount.setIdentificationId(IdentificationId.toString());
            }
        }
        System.out.println("3");
        System.out.println(individualAccount);
        return individualAccount;
    }

    @Override
    public JointDetailBean createJoint(JointDetailBean jointAccount) {
        String clientCredentials = oAuth2TokenService.getClientCredentials();
        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
        Application application = gson.fromJson(createApplication.toString(), Application.class);
        if (application != null) {
            jointAccount.setApplicationId(application.getApplicationId());
            jointAccount.setBeneficiaryId(application.getPrimaryBeneficiaryId());
            List<Beneficiary> beneficiaries = application.getBeneficiaries();
            Beneficiary beneficiary = beneficiaries.get(0);
            String thirdAPIUpdateDetails = beneficiary.getThirdAPIUpdateDetails(jointAccount);
            System.out.println("thirdAPIUpdateDetails------------>" + thirdAPIUpdateDetails);
            onboardingsServices.updateBeneficiary(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId(), beneficiary.getThirdAPIUpdateDetails(jointAccount));
//            onboardingsServices.fetchBeneficiaryByIds(clientCredentials, beneficiary.getApplicationId(), beneficiary.getBeneficiaryId());
            for (JointDetailBean moreInvestor : jointAccount.getMoreInvestorList()) {
                if (!moreInvestor.getSend_email().equals("2")) {
                    moreInvestor.setApplicationId(application.getApplicationId());
                    moreInvestor.setBeneficiaryId("0");

                    String date = common.changeFormat(moreInvestor.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
                    moreInvestor.setDate_of_Birth(date);
                    System.out.println("---" + moreInvestor.getThirdAPICreateDetails(moreInvestor));
                    StringBuilder createMoreInvestorBeneficiary = onboardingsServices.createBeneficiary(clientCredentials, application.getApplicationId(), moreInvestor.getThirdAPICreateDetails(moreInvestor));
                    Beneficiary moreInvestorBeneficiary = gson.fromJson(createMoreInvestorBeneficiary.toString(), Beneficiary.class);
                    moreInvestor.setBeneficiaryId(moreInvestorBeneficiary.getBeneficiaryId());
                }
            }
        }
        return jointAccount;
    }

    @Override
    public String updateAddress(Address address) {
        System.out.println("2");
        Map<String, CountryISOCodeBean> countryISOCodeMap = common.countryISOCode();
        if (address.getCountry() != null) {
            String country = address.getCountry();
            if (country != null && !country.isEmpty()) {
                CountryISOCodeBean countryISOCode = countryISOCodeMap.get(country.trim());
                String codeISO = countryISOCode.getCodeISO();
                country = codeISO;

            }
            address.setCountryCode(country);
        }
        String clientCredentials = oAuth2TokenService.getClientCredentials();
        StringBuilder AddressId = onboardingsServices.updateBeneficiaryAddress(clientCredentials, address.getApplicationId(), address.getBeneficiaryId(), address.getAddressID(), address.getThirdAPIUpdateAddress(address));

        if (!AddressId.toString().contains("Status Code: 404")) {
            return AddressId.toString();
        }
        return null;
    }
}
