/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.services;

import java.util.Date;

/**
 *
 * @author Maninderjit Singh
 */
public interface DateTimeService {

    public Date today0000();

    public Date today1200();

    public Date now();

    public long timeDate();

}
