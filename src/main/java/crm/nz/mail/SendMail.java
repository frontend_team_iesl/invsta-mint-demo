/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.mail;

import java.io.IOException;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.components.FileUtility;
import crm.nz.components.MailTemplate;
import crm.nz.table.bean.CsrfToken;
import crm.nz.table.bean.SecuredUser;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;

/**
 *
 * @author TOSHIBA R830
 */
@Component
public class SendMail {

    @Autowired
    MailTemplate mailsTemp;
    @Autowired
    private FileUtility fileUtility;
    final private String SENDGRID_APIKEY = "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";

    static String from = "investments@invsta.io";
    static String fromName = "Mint Asset Management ";

    public void sendMail(String toEmail, String fromEmail, String subject, String htmlMessage) {
        try {
            Response response = null;
            String[] emailArr = toEmail.split(",");
            if (emailArr.length > 1) {
                Mail mail = new Mail();
                Email formEmail = new Email(fromEmail);
                mail.setFrom(formEmail);
                Personalization personalization = new Personalization();
                for (String to : emailArr) {
                    Email to2 = new Email(to);
                    personalization.addTo(to2);
                }
                mail.addPersonalization(personalization);
                mail.setSubject(subject);
                Content content = new Content("text/html", htmlMessage);
                mail.addContent(content);
                SendGrid sg = new SendGrid(SENDGRID_APIKEY);
                Request request = new Request();
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                response = sg.api(request);
            } else {
                Email from = new Email(fromEmail, fromName);
                Email to = new Email(toEmail);
                Content content = new Content("text/html", htmlMessage);
                Mail mail = new Mail(from, subject, to, content);
                SendGrid sg = new SendGrid(SENDGRID_APIKEY);
                Request request = new Request();
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                response = sg.api(request);
            }
            LoggerFactory.getLogger(SendMail.class).debug("Status Code", response.getStatusCode());
            LoggerFactory.getLogger(SendMail.class).debug(response.getBody());
            LoggerFactory.getLogger(SendMail.class).debug("Headers Map", response.getHeaders());
        } catch (IOException ex) {
            LoggerFactory.getLogger(SendMail.class).error("", ex);
        }
    }

    public void sendResetPasswordMail(String email, String username, String id, CsrfToken csrfToken, String domain) {
        StringBuilder textMessage = new StringBuilder();
        String link = domain.replaceAll("(?<!:)//", "/") + "resetpwd?&token=" + csrfToken.getToken();
//        String link = domain.replaceAll("(?<!:)//", "/") + "resetpwd?id=" + id + "&token=" + csrfToken.getToken();
        String Message = "";
//        textMessage.append("Dear ").append(username).append("\n");
//        textMessage.append("Please Click on below link to reset your Password \n");
//        textMessage.append(domain).append("resetpwd?id=").append(id).append("&token=").append(csrfToken.getToken());
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "ChangePassword.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstName", username);
            Message = Message.replaceAll("varlink", link);

        } catch (FileNotFoundException ex) {
        }
        sendMail(email, from, mailsTemp.PasswordChangeSubject, Message);
    }

    public void sendChangePassword(String email, String username, String domain) {
        StringBuilder textMessage = new StringBuilder();
        String link = domain.replaceAll("(?<!:)//", "/") + "login";
        String Message = "";
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "resetPassword.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstName", username);
            Message = Message.replaceAll("varlink", link);

        } catch (FileNotFoundException ex) {
        }
        sendMail(email, from, mailsTemp.PasswordResetSubject, Message);
    }

    public void sendVerifyNewPersonToEmail(PersonDetailsBean person, String token, String domain) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
//        textMessage.append("Hi ").append(person.getFullName()).append("\n");
//        textMessage.append("Your Login Credentials are : \nEmail ID :").append(person.getEmail()).append("\nPassword :").append(person.getPassword()).append("\n");
//        textMessage.append("Please Click on below link to complete your verfication ").append("\n");
//        textMessage.append(domain).append("login");
        if ("PENDING".equalsIgnoreCase(person.getStatus())) {
            Subject = mailsTemp.pendingRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "pendingRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                Message = textMessage.toString();
                Message = Message.replace("var firstname", person.getFirstName());
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
                System.out.println("domain name----------------" + domain.replaceAll("(?<!:)//", "/"));
                Message = Message.replace("var email", person.getEmail());
                Message = Message.replace("var password", person.getRaw_password());

            } catch (FileNotFoundException ex) {
            }
        } else if ("SUBMISSION".equalsIgnoreCase(person.getStatus())) {
            Subject = mailsTemp.FinalRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "finalRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                domain = domain + "verifyEmail-" + token;
                Message = textMessage.toString();
                Message = Message.replace("firstname", person.getFullName());
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
            } catch (FileNotFoundException ex) {

            }
        }
        sendMail(person.getEmail(), from, Subject, Message);
    }

    public void sendVerifyNewCompanyToEmail(CompanyDetailBean company, String token, String domain) {

        company.getMoreInvestorList().forEach((inv) -> {
            StringBuilder textMessage = new StringBuilder();
            File file;
            String Message = "";
            String NewSub = "New account for " + company.getCompanyName();
            String link = "";
            if ("2".equalsIgnoreCase(inv.getSend_email())) {
                if ("COMPANY_ACCOUNT".equalsIgnoreCase(company.getReg_type())) {
                    link = domain.replaceAll("(?<!:)//", "/").concat("outside-company-account").concat("?token=").concat(inv.getToken());
                } else {
                    link = domain.replaceAll("(?<!:)//", "/").concat("outside-trust-account").concat("?token=").concat(inv.getToken());
                }
                file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "moreInvestor.html");
                try {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNextLine()) {
                        textMessage.append(sc.nextLine());
                    }
                    Message = textMessage.toString();
                    Message = Message.replace("firstname", inv.getFname().split(" ")[0]);
                    Message = Message.replace("creatorName", company.getFname());
                    Message = Message.replace("Joint Account", company.getCompanyName());
                    Message = Message.replace("detailsLink", link);

                } catch (FileNotFoundException ex) {

                }
            } else {
//                NewSub = mailsTemp.FinalRegistrationSubject;
                file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "moreInvestorCompleted.html");
                try {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNextLine()) {
                        textMessage.append(sc.nextLine());
                    }
                    Message = textMessage.toString();
                    Message = Message.replace("firstname", inv.getFname().split(" ")[0]);
                    Message = Message.replace("creatorName", company.getFname().split(" ")[0]);
                    Message = Message.replace("Joint Account", company.getCompanyName());
                    Message = Message.replace("detailsLink", link);
                } catch (FileNotFoundException ex) {

                }
            }

            sendMail(inv.getEmail(), from, NewSub, Message);
        });

        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        if ("PENDING".equalsIgnoreCase(company.getStatus())) {
            Subject = mailsTemp.pendingRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "pendingRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                Message = textMessage.toString();
                Message = Message.replace("var firstname", company.getFname().split(" ")[0]);
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
                Message = Message.replace("var email", company.getEmail());
                Message = Message.replace("var password", company.getRaw_password());

            } catch (FileNotFoundException ex) {
            }
        } else if ("SUBMISSION".equalsIgnoreCase(company.getStatus())) {
            Subject = mailsTemp.FinalRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "finalRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                Message = textMessage.toString();
                Message = Message.replace("firstname", company.getFname().split(" ")[0]);
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
            } catch (FileNotFoundException ex) {

            }
        }

//        textMessage.append("Welcome , ").append(company.getCompanyName()).append("\n");
//        textMessage.append("Your Login Credentials are : \nEmail ID :").append(company.getEmail()).append("\nPassword :").append(company.getPassword()).append("\n");
//        textMessage.append("Please Click on below link to complete your verfication ").append("\n");
//        textMessage.append(domain).append("login");
        sendMail(company.getEmail(), from, Subject, Message);
    }

    public void sendVerifyNewJointToEmail(JointDetailBean joint, String token, String domain) {
        joint.getMoreInvestorList().forEach((inv) -> {
            StringBuilder textMessage = new StringBuilder();
            File file;
            String Message = "";
            String NewSub = "New account for " + joint.getFullName();
            String link = domain.replaceAll("(?<!:)//", "/").concat("outside-joint-account").concat("?token=").concat(inv.getToken());
            if ("2".equalsIgnoreCase(inv.getSend_email())) {
//                Subject = mailsTemp.pendingRegistrationSubject;
                file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "moreInvestor.html");
                try {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNextLine()) {
                        textMessage.append(sc.nextLine());
                    }
                    Message = textMessage.toString();
                    Message = Message.replace("firstname", inv.getFirstName());
                    Message = Message.replace("creatorName", joint.getFullName().split(" ")[0]);
                    Message = Message.replace("detailsLink", link);

                } catch (FileNotFoundException ex) {

                }
            } else {
//                NewSub = mailsTemp.FinalRegistrationSubject;
                file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "moreInvestorCompleted.html");
                try {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNextLine()) {
                        textMessage.append(sc.nextLine());
                    }
                    Message = textMessage.toString();
                    Message = Message.replace("firstname", inv.getFirstName());
                    Message = Message.replace("creatorName", joint.getFullName().split(" ")[0]);
                    Message = Message.replace("detailsLink", link);
                } catch (FileNotFoundException ex) {

                }
            }

//            textMessage.append("Welcome , ").append(inv.getFullName()).append("</br>");
//            textMessage.append("Your Login Credentials are :</br>");
//            textMessage.append("Email Id :").append(inv.getEmail()).append("</br>");
//            textMessage.append("Password :").append(inv.getEmail()).append("</br>");
//            textMessage.append("Please Click on below link to complete your verification ").append("</br>");
//            textMessage.append(domain).append("home-outside-joint-account?token=").append(inv.getToken());
            sendMail(inv.getEmail(), from, NewSub, Message);
        });
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        if ("PENDING".equalsIgnoreCase(joint.getStatus())) {
            Subject = mailsTemp.pendingRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "pendingRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                Message = textMessage.toString();
                Message = Message.replace("var firstname", joint.getFirstName());
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
                Message = Message.replace("var email", joint.getEmail());
                Message = Message.replace("var password", joint.getRaw_password());

            } catch (FileNotFoundException ex) {
            }
        } else if ("SUBMISSION".equalsIgnoreCase(joint.getStatus())) {
            Subject = mailsTemp.FinalRegistrationSubject;
            file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "finalRegistration.html");
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNextLine()) {
                    textMessage.append(sc.nextLine());
                }
                Message = textMessage.toString();
                Message = Message.replace("firstname", joint.getFirstName());
                Message = Message.replaceAll("var link", domain.replaceAll("(?<!:)//", "/"));
            } catch (FileNotFoundException ex) {

            }

        }

//        textMessage.append("Welcome , ").append(joint.getAcount_holder_name()).append("\n");
//        textMessage.append("Your Login Credentials are : \nEmail ID :").append(joint.getEmail()).append("\n");
////        textMessage.append("Password :").append(joint.getPassword()).append("\n");
//        textMessage.append("Please Click on below link to complete your verification ").append("\n");
//        textMessage.append(domain).append("login");
        sendMail(joint.getEmail(), from, Subject, Message);
    }

    public void sendOnEmailVerificationMail(String name, String email) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.emailVerificationSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "sendOnEmailVerification.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", name);

        } catch (FileNotFoundException ex) {

        }
        sendMail(email, from, Subject, Message);
    }

    public void sendInvestmentPaymentMail(PendingInvestmentBean bean, SecuredUser user) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.InvestmentPaymentMailSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "investmentPayment.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", bean.getInvestmentName());
            Message = Message.replace("fundName", bean.getPortfolioName());
            Message = Message.replace("$Amount", bean.getInvestedAmount());
            Message = Message.replace("url_link", "link");

        } catch (FileNotFoundException ex) {

        }
        sendMail(user.getUsername(), from, Subject, Message);
    }

    public void sendTransactionPaymentMail(PendingTransactionBean bean, SecuredUser user) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.InvestmentPaymentMailSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "investmentPayment.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", bean.getInvestmentName());
            Message = Message.replace("fundName", bean.getPortfolioName());
            Message = Message.replace("$Amount", bean.getInvestedAmount());
            Message = Message.replace("url_link", "link");

        } catch (FileNotFoundException ex) {

        }
        sendMail(user.getUsername(), from, Subject, Message);
    }

    public void sendTransactionPaymentPayNowMail(PendingTransactionBean bean, SecuredUser user) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.InvestmentPaymentMailSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "investmentPayment.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", bean.getInvestmentName());
            Message = Message.replace("fundName", bean.getPortfolioName());
            Message = Message.replace("$Amount", bean.getInvestedAmount());
            Message = Message.replace("url_link", "link");

        } catch (FileNotFoundException ex) {

        }
        sendMail(user.getUsername(), from, Subject, Message);
    }

    public void sendConfirmingInvestmentMail(String name, String email) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.ConfirmingInvestment;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "confirmingInvestment.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", name);
            Message = Message.replace("fundName", "fund");
            Message = Message.replace("$Amount", "Amount");
            Message = Message.replace("url_link", "link");

        } catch (FileNotFoundException ex) {

        }
        sendMail(email, from, Subject, Message);
    }

    public void sendInvestmentPaymentPayNowMail(PendingInvestmentBean bean, SecuredUser user) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.InvestmentPaymentMailSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "investmentPaymentPayNow.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", user.getName());
            Message = Message.replace("Bankname", bean.getBankName());
            Message = Message.replace("AccountName", bean.getBankAccountName());
            Message = Message.replace("AccountNumber", bean.getBankAccountNumber());
            Message = Message.replace("PayReference", "83214901890821");
            Message = Message.replace("fundName", bean.getPortfolioName());
            Message = Message.replace("InvAmount", "$" + bean.getInvestedAmount());
        } catch (FileNotFoundException ex) {

        }
        sendMail(user.getUsername(), from, Subject, Message);
    }

    public void sendSellFundsMail(SecuredUser user, PendingTransactionBean bean) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.SellFundsSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "sellFund.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", bean.getBeneficiaryName());
            Message = Message.replace("$amount", "$" + bean.getInvestedAmount());
            Message = Message.replace("fundname", bean.getPortfolioName());

        } catch (FileNotFoundException ex) {

        }
        sendMail(user.getUsername(), from, Subject, Message);
    }

    public void Withdrawal(String name, String email) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file;
        String Message = "";
        Subject = mailsTemp.WithdrawalToBankSubject;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "Withdrawal.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("firstname", name);
            Message = Message.replace("$amount", name);

        } catch (FileNotFoundException ex) {

        }
        sendMail(email, from, Subject, Message);
    }

    public void sendSingleAccountRegistrationMail(String otherInvName, String accountname, String mainInv, String Link, String mainEmail, String OtherEmail) {
        StringBuilder textMessage = new StringBuilder();
        String Subject = "";
        File file, file2;
        String Message = "";
        String Message2 = "";
        Subject = "Details received for " + accountname;
        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "mainInv-company-joint-trust.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message = textMessage.toString();
            Message = Message.replace("<Firstname>", mainInv.split(" ")[0]);
            Message = Message.replace("<jointaccountname>", accountname);
            Message = Message.replace("SecondpersonName", otherInvName.split(" ")[0]);
            Message = Message.replace("url_link", Link.replaceAll("(?<!:)//", "/"));

        } catch (FileNotFoundException ex) {

        }
        sendMail(mainEmail, from, Subject, Message);
        textMessage.setLength(0);
//          Subject = "Details received for "+ name;
        file2 = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "anotherInv-company-joint-trust.html");
        try {
            Scanner sc = new Scanner(file2);
            while (sc.hasNextLine()) {
                textMessage.append(sc.nextLine());
            }
            Message2 = textMessage.toString();
            Message2 = Message2.replace("<Firstname>", otherInvName.split(" ")[0]);
            Message2 = Message2.replace("AccountCreator", mainInv.split(" ")[0]);
            Message2 = Message2.replace("<jointaccountname>", accountname);
            Message2 = Message2.replace("url_link", Link.replaceAll("(?<!:)//", "/"));

        } catch (FileNotFoundException ex) {

        }
        sendMail(OtherEmail, from, Subject, Message2);
    }

//    public void companyTrustSingleRegisteration(CompanyDetailBean company, String email) {
//        StringBuilder textMessage = new StringBuilder();
//        String Subject = "";
//        File file;
//        String Message = "";
//        Subject = "New account for  ";
//        file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(), "finalRegistration.html");
//    
//            try {
//                Scanner sc = new Scanner(file);
//                while (sc.hasNextLine()) {
//                    textMessage.append(sc.nextLine());
//                }
//                Message = textMessage.toString();
//                Message = Message.replace("firstname", company.getCompanyName());
////                Message = Message.replace("var link", domain.replaceAll("(?<!:)//", "/"));
//            } catch (FileNotFoundException ex) {
//
//            }
//        }
}
