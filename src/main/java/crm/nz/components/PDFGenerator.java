/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Owner
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PDFGenerator {

    final private static DecimalFormat df6 = new DecimalFormat("##,##,###.00");


    public ByteArrayInputStream getDataZooPdf(String pendingInvestmentRequests) throws DocumentException {
        Document doc = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(doc, out);
            doc.setPageSize(PageSize.A3);
            doc.open();
            BaseColor backgroundColor = new BaseColor(46, 94, 165, 1);
            Font font = FontFactory.getFont(FontFactory.COURIER, 14, backgroundColor);
            Paragraph para = new Paragraph("User Investment Requests", font);
            para.setAlignment(Element.ALIGN_CENTER);
            doc.add(para);
            doc.add(Chunk.NEWLINE);
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            Stream.of("Date", "Transaction Id")
                    .forEach(headerTitle -> {
                        PdfPCell header = new PdfPCell();
                        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD,9,Font.NORMAL,BaseColor.WHITE);
                        header.setBackgroundColor(backgroundColor);
                        header.setBorderWidth(1);
                        header.setPadding(12);
                        header.setPhrase(new Phrase(headerTitle, headFont));
                        table.addCell(header);
                    });
                PdfPCell dateCell = new PdfPCell(new Phrase("User Name"));
                dateCell.setPadding(12);
                table.addCell(dateCell);
                  PdfPCell tranIdCell = new PdfPCell(new Phrase("2"));
                tranIdCell.setPadding(12);
                table.addCell(tranIdCell);
                dateCell = new PdfPCell(new Phrase("Data Source(s) Selected:"));
                dateCell.setPadding(12);
                table.addCell(dateCell);
                  tranIdCell = new PdfPCell(new Phrase("2"));
                tranIdCell.setPadding(12);
                table.addCell(tranIdCell);
                dateCell = new PdfPCell(new Phrase("Enquiry Reference:"));
                dateCell.setPadding(12);
                table.addCell(dateCell);
                   tranIdCell = new PdfPCell(new Phrase("2"));
                tranIdCell.setPadding(12);
                table.addCell(tranIdCell);
                dateCell = new PdfPCell(new Phrase("Enquiry Date and Time"));
                dateCell.setPadding(12);
                table.addCell(dateCell);
                  tranIdCell = new PdfPCell(new Phrase("2"));
                tranIdCell.setPadding(12);
                table.addCell(tranIdCell);
                dateCell = new PdfPCell(new Phrase("Client Reference"));
                dateCell.setPadding(12);
                table.addCell(dateCell);

                 tranIdCell = new PdfPCell(new Phrase("2"));
                tranIdCell.setPadding(12);
                table.addCell(tranIdCell);
                
            doc.add(table);
            doc.close();
        } catch (DocumentException ex) {
            Logger.getLogger(PDFGenerator.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

}
    
    
    
    

