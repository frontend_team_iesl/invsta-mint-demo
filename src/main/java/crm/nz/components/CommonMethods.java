/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.CountryISOCodeBean;
import crm.nz.beans.acc.api.PortfolioPrice;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.repository.CommonRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.services.DateTimeServiceImpl;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CommonMethods {

    @Autowired
    CommonRepository commonRepository;

   

    private final Map<String, String> modelIds = new HashMap<>();
    private final Map<String, String> userCurrency = new HashMap<>();
    private final Map<String, String> currSymbol = new HashMap<>();
    private final Map<String, String> reportTypes = new HashMap<>();
    private final Map<String, String> fundColors = new HashMap<>();
    private final Map<String, CountryISOCodeBean> countryCode = new HashMap<>();
    private final Map<String, BankCodeBean> bankCode = new HashMap<>();
    private final Map<String, DateFormat> dateFormats = new HashMap<>();
    private static final long ONE_SECOND = 1000;
    public static final long ONE_MINUTE = 60 * ONE_SECOND;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    private static final String DATE_FORMAT1 = "yyyy/MM/dd HH:mm:ss";
    private static final String DATE_FORMAT2 = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT3 = "yyyy-MM-dd";
    private static final String DATE_FORMAT4 = "dd-MM-yyyy";
    private static final String DATE_FORMAT5 = "MM/dd/yyyy";
    private static final String DATE_FORMAT6 = "dd-MM-yyyy";
    private static final String DATE_FORMAT7 = "yyyy/MM/dd";
    private static final String DATE_FORMAT8 = "dd/MM/yyyy";
    private static final String DATE_FORMAT_dd_MM = "dd/MM";
    private static final String DISPLAY_FORMAT = "dd-MMM-yyyy";
    private static final String DISPLAY_FORMAT_2 = "dd-MMM-yyyy HH:mm:ss";
    private static final String DISPLAY_FORMAT_3 = "MMM-yyyy";
    private static final String TIME_FORMAT1 = "HH:mm:ss";
    private static final String TIME_FORMAT2 = "HH:mm";
    public static final SimpleDateFormat format1 = new SimpleDateFormat(DATE_FORMAT1);
    public static final SimpleDateFormat format2 = new SimpleDateFormat(DATE_FORMAT2);
    public static final SimpleDateFormat format3 = new SimpleDateFormat(DATE_FORMAT3);
    public static final SimpleDateFormat format4 = new SimpleDateFormat(DATE_FORMAT4);
    public static final SimpleDateFormat format5 = new SimpleDateFormat(DATE_FORMAT5);
    public static final SimpleDateFormat format6 = new SimpleDateFormat(DATE_FORMAT6);
    public static final SimpleDateFormat format7 = new SimpleDateFormat(DATE_FORMAT7);
    public static final SimpleDateFormat format8 = new SimpleDateFormat(DATE_FORMAT8);
    public static final SimpleDateFormat format_dd_MM = new SimpleDateFormat(DATE_FORMAT_dd_MM);
    public static final SimpleDateFormat display_format = new SimpleDateFormat(DISPLAY_FORMAT);
    public static final SimpleDateFormat display_format2 = new SimpleDateFormat(DISPLAY_FORMAT_2);
    public static final SimpleDateFormat display_format3 = new SimpleDateFormat(DISPLAY_FORMAT_3);
    public static final SimpleDateFormat time_format1 = new SimpleDateFormat(TIME_FORMAT1);
    public static final SimpleDateFormat time_format2 = new SimpleDateFormat(TIME_FORMAT2);

    public String format1(Date date) {
        if (date == null) {
            return "";
        } else {
            return format1.format(date);
        }
    }

    public String format2(Date date) {
        if (date == null) {
            return "";
        } else {
            return format2.format(date);
        }
    }

    public String format3() {
        return DATE_FORMAT3;
    }

    public String format4() {
        return DATE_FORMAT4;
    }

    public String format3(Date date) {
        if (date == null) {
            return "";
        } else {
            return format3.format(date);
        }
    }

    public String format4(Date date) {
        if (date == null) {
            return "";
        } else {
            return format4.format(date);
        }
    }

    public String format5(Date date) {
        if (date == null) {
            return "";
        } else {
            return format5.format(date);
        }
    }

    public String format_dd_MM(Date date) {
        if (date == null) {
            return "";
        } else {
            return format_dd_MM.format(date);
        }
    }

    public String display_format(Date date) {
        if (date == null) {
            return "";
        } else {
            return display_format.format(date);
        }
    }

    public String display_format2(Date date) {
        if (date == null) {
            return "";
        } else {
            return display_format2.format(date);
        }
    }

    public String display_format3(Date date) {
        if (date == null) {
            return "";
        } else {
            return display_format3.format(date);
        }
    }

    public String date_format_map(Date date) {
        if (date == null) {
            return "";
        } else {
            return display_format3.format(date);
        }
    }

    public String getId(Date date) {
        String id = date.toString().toLowerCase();
        id = id.replaceAll(" ", "");
        id = id.replaceAll(":", "");
        return id;
    }

    public int getRandomNumber() {
        Random random = new Random();
        int number = 100000 + random.nextInt(900000);
        return number;
    }

    public Date parseDate(String date) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            return format1.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date parseDate(String date, String format) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date parseDate(String date, DateFormat dateFormat) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            return dateFormat.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public String ts2date(String timestamp) {
        Date date = parseDate(timestamp, format2);
        return format3.format(date);
    }

    public String dateTimeIn2ndZone(String date_format3, String time_format2_3, String first_tz_id) {
        return dateTimeIn2ndZone(date_format3, time_format2_3, first_tz_id, DateTimeServiceImpl.TOZONEID.getId());
    }

    public String dateTimeIn2ndZone(String date_format3, String time_format2_3, String first_tz_id, String second_tz_id) {
        try {
            Date date = format3.parse(date_format3);
            String[] time = time_format2_3.split(":");

            TimeZone firstTimeZone = TimeZone.getTimeZone(first_tz_id);
            TimeZone secondTimeZone = TimeZone.getTimeZone(second_tz_id);

            Calendar calendar = Calendar.getInstance(firstTimeZone);
            calendar.setTime(date);
            if (time.length > 0) {
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
            } else {
                calendar.set(Calendar.HOUR_OF_DAY, 0);
            }
            if (time.length > 1) {
                calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
            } else {
                calendar.set(Calendar.MINUTE, 0);
            }
            if (time.length > 2) {
                calendar.set(Calendar.SECOND, Integer.parseInt(time[2]));
            } else {
                calendar.set(Calendar.SECOND, 0);
            }
            System.out.println(calendar);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            dateFormat.setTimeZone(firstTimeZone);
            System.out.println("time in " + firstTimeZone + " " + dateFormat.format(calendar.getTime()));

            dateFormat.setTimeZone(secondTimeZone);
            System.out.println("time in " + secondTimeZone + " " + dateFormat.format(calendar.getTime()));
            return dateFormat.format(calendar.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(CommonMethods.class.getName()).log(Level.SEVERE, null, ex);
            return date_format3 + " " + time_format2_3;
        } finally {

        }
    }

    public Date dateTimeInLondon(String date_format3, String time_format2_3, String time_zone_id) {
        Date date = parseDate(date_format3, format3);
        String[] time = time_format2_3.split(":");
        TimeZone workTimeZone = TimeZone.getTimeZone(time_zone_id);
        Calendar calendar = Calendar.getInstance(workTimeZone);
        calendar.setTime(date);
        if (time.length > 0) {
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
        }
        if (time.length > 1) {
            calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        } else {
            calendar.set(Calendar.MINUTE, 0);
        }
        if (time.length > 2) {
            calendar.set(Calendar.SECOND, Integer.parseInt(time[2]));
        } else {
            calendar.set(Calendar.SECOND, 0);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("time in " + time_zone_id);
        dateFormat.setTimeZone(workTimeZone);
        System.out.println(dateFormat.format(calendar.getTime()));

        System.out.println("time in " + DateTimeServiceImpl.TOZONEID.getId());
        dateFormat.setTimeZone(TimeZone.getTimeZone(DateTimeServiceImpl.TOZONEID));
        System.out.println(dateFormat.format(calendar.getTime()));
        return calendar.getTime();
    }

    public BigDecimal parseBigDecimal(String bigDecimal) {
        if (bigDecimal == null) {
            bigDecimal = "0";
        }
        return new BigDecimal(bigDecimal);
    }

    public Integer parseInt(String numeric) {
        if (numeric == null) {
            numeric = "0";
        }
        return isNumeric(numeric) ? Integer.parseInt(numeric) : 0;
    }

    public boolean isNumeric(String maybeNumeric) {
        maybeNumeric = maybeNumeric.replaceAll(",", "");
        return maybeNumeric != null && maybeNumeric.matches("[0-9]+");
    }

    public Date today() {
        return DateTimeServiceImpl.current();
    }

    public String dateFormat(java.util.Date date) {
        return format1.format(date);
    }

    public String dateFormat(java.util.Date date, DateFormat format) {
        return format.format(date);
    }

    public String dateFormat(java.util.Date date, String DATE_FORMAT) {
        DateFormat format = new SimpleDateFormat(DATE_FORMAT);
        return format.format(date);
    }

    public String changeFormat(String date, DateFormat format1, DateFormat format2) {
        Date parseDate = parseDate(date, format1);
        return dateFormat(parseDate, format2);
    }

    public String changeSqlDateToFormat(String date, String format) {
        DateFormat dateFormat = dateFormats().get(format);
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat(format);
            dateFormats().put(format, dateFormat);
        }
        return changeFormat(date, format3, dateFormat);
    }

    public String changeCreatedTsToFormat(String ts) {
        Date createdDate = parseDate(ts, format2);
        return timeDiff(today(), createdDate);
    }

    public String checkNull(String string, String defaultString) {
        return string != null || string != null && string.equalsIgnoreCase("null") ? string : defaultString;
    }

    public boolean checkToken(String token) {
        return token != null;
    }

    public String timeDiff(Date d1, Date d2) {
        //in milliseconds
        long diff = Math.abs(d1.getTime() - d2.getTime());
        long diffSeconds = diff / 1000;
        if (diffSeconds < 60) {
            return "Just Now";
        }
        long diffMinutes = diff / (60 * 1000);
        if (diffMinutes < 60) {
            if (diffMinutes == 1) {
                return "a minute ago";
            }
            return diffMinutes + " minutes ago";
        }
        long diffHours = diff / (60 * 60 * 1000);
        if (diffHours < 24) {
            if (diffHours == 1) {
                return "an hour ago";
            }
            return diffHours + " hours ago";
        }
        long diffDays = diff / (24 * 60 * 60 * 1000);
        if (diffDays == 1) {
            return "a day ago";
        }
        return diffDays + " days ago";
    }

    public BigDecimal round(BigDecimal bd, int places) {
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd;
    }

    public BigDecimal round(String value, int places) {
        if (value == null || value != null && value.trim().isEmpty()) {
            value = "0.00";
        }
        return round(new BigDecimal(value), places);
    }

    public String round2String(String value, int places) {
        double d = round(value, places).doubleValue();
        return String.valueOf(d);
    }

    public String dayDateOfMonth(java.util.Date dt) {
        String day = day(dt.getDay());
        String date = date(dt.getDate());
        String month = month(dt.getMonth() + 1);
        return date + "/" + month;
    }

    public String dateOfMonth(java.util.Date dt) {
        return format_dd_MM.format(dt);
    }

    private String day(int day) {
        String dy = null;
        switch (day) {
            case 1:
                dy = "Monday";
                break;
            case 2:
                dy = "Tuesday";
                break;
            case 3:
                dy = "Wednesday";
                break;
            case 4:
                dy = "Thursday";
                break;
            case 5:
                dy = "Friday";
                break;
            case 6:
                dy = "Saturday";
                break;
            case 7:
                dy = "Sunday";
                break;
            default:
                break;
        }
        return dy;
    }

    private String date(int date) {
        String dt = null;
        if (1 == date) {
            dt = "1st";
        } else if (2 == date) {
            dt = "2nd";
        } else if (3 == date) {
            dt = "3rd";
        } else if (4 <= date) {
            dt = date + "th";
        }
        return dt;
    }

    public String month(int month) {
        String mth = null;
        switch (month) {
            case 1:
                mth = "January";
                break;
            case 2:
                mth = "February";
                break;
            case 3:
                mth = "March";
                break;
            case 4:
                mth = "April";
                break;
            case 5:
                mth = "May";
                break;
            case 6:
                mth = "June";
                break;
            case 7:
                mth = "July";
                break;
            case 8:
                mth = "August";
                break;
            case 9:
                mth = "September";
                break;
            case 10:
                mth = "October";
                break;
            case 11:
                mth = "November";
                break;
            case 12:
                mth = "December";
                break;
            default:
                break;
        }
        return mth;
    }

    public List<String> hours() {
        List<String> hours = new ArrayList<>();
        for (int i = 0; i <= 23; i++) {
            String hour = "";
            if (i < 10) {
                hour = "0";
            }
            hour += i;
            hours.add(hour);
        }
        return hours;
    }

    public List<String> minutes() {
        List<String> minutes = new ArrayList<>();
        for (int i = 0; i <= 59; i++) {
            String minute = "";
            if (i < 10) {
                minute = "0";
            }
            minute += i;
            minutes.add(minute);
        }
        return minutes;
    }

    public final Map<String, String> userCurrency() {
        return userCurrency;
    }

    public final Map<String, DateFormat> dateFormats() {
        return dateFormats;
    }

    public final Map<String, String> fundColors() {
        return fundColors;
    }

    public final Map<String, String> currSymbol() {
        return currSymbol;
    }

    public final Map<String, String> modelIds() {
        return modelIds;
    }

    public final Map<String, String> reportTypes() {
        return reportTypes;
    }

    public final Map<String, CountryISOCodeBean> countryISOCode() {
        if (countryCode.isEmpty()) {
            List<CountryISOCodeBean> allCountryCode = commonRepository.getAllCountryCode();
            ListIterator<CountryISOCodeBean> listIterator = allCountryCode.listIterator();
            while (listIterator.hasNext()) {
                CountryISOCodeBean bean = listIterator.next();
                countryCode.put(bean.getCountry(), bean);
            }
        }
        return countryCode;
    }

    public final Map<String, CountryISOCodeBean> countryFromDialingCode() {
        if (countryCode.isEmpty()) {
            List<CountryISOCodeBean> allCountryCode = commonRepository.getAllCountryCode();
            ListIterator<CountryISOCodeBean> listIterator = allCountryCode.listIterator();
            while (listIterator.hasNext()) {
                CountryISOCodeBean bean = listIterator.next();
                countryCode.put(bean.getDialingCode(), bean);
            }
        }
        return countryCode;
    }

    public final Map<String, BankCodeBean> bankNameByCode() {
        if (bankCode.isEmpty()) {
            List<BankCodeBean> allBankCode = commonRepository.getAllBankCode();
            ListIterator<BankCodeBean> listIterator = allBankCode.listIterator();
            while (listIterator.hasNext()) {
                BankCodeBean bean = listIterator.next();
                bankCode.put(bean.getBankCode(), bean);
            }
        }
        return bankCode;
    }

    public String toString(List list) {
        if (list == null) {
            return null;
        }
        String objects = "";
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);
            objects = objects + ((i > 0 && i != list.size()) ? "," : "") + obj.toString();
        }
        return "[" + objects + "]";
    }

    public List<String> getAllDatesOfLatestWeek() {
        Calendar now = Calendar.getInstance();
        now.setTime(today());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<String> days = new ArrayList<>();
        now.add(Calendar.DAY_OF_MONTH, -7);
        for (int i = 0; i < 7; i++) {
            days.add(format.format(now.getTime()));
            now.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    public List<String> getAllTimes(int start_hh, int start_mm, int end_hh, int end_mm, int duration_mm) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        cal.set(Calendar.HOUR_OF_DAY, end_hh);
        cal.set(Calendar.MINUTE, end_mm);
        cal.set(Calendar.SECOND, 0);
        Date endTime = cal.getTime();
        cal.set(Calendar.HOUR_OF_DAY, start_hh);
        cal.set(Calendar.MINUTE, start_mm);
        List<String> times = new LinkedList<>();
        while (cal.getTime().before(endTime) || cal.getTime().equals(endTime)) {
            times.add(format.format(cal.getTime().getTime()));
            cal.add(Calendar.MINUTE, duration_mm);
        }
        return times;
    }


    public List<String> getAllDates(String fDate, String tDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = parseDate(fDate, format);
        Date toDate = parseDate(tDate, format);
        cal.setTime(fromDate);
        List<String> days = new ArrayList<>();
        while (cal.getTime().before(toDate) || cal.getTime().equals(toDate)) {
            days.add(format.format(cal.getTime().getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    public List<Date> getAllDates(Date fromDate, Date toDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        List<Date> days = new LinkedList<>();
        while (cal.getTime().before(toDate) || cal.getTime().equals(toDate)) {
            days.add(cal.getTime());
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    public Map<Date, List<Date>> getAllDatesMonthWise(Date fromDate, Date toDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        Date lastDateOfMonth = cal.getTime();
        cal.setTime(fromDate);
        Map<Date, List<Date>> allDatesMonthWise = new TreeMap<>();
        while (cal.getTime().before(toDate) || cal.getTime().equals(toDate)) {
            List<Date> allDates = new LinkedList<>();
            while (cal.getTime().before(lastDateOfMonth) || cal.getTime().equals(lastDateOfMonth)) {
                allDates.add(cal.getTime());
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
            allDatesMonthWise.put(lastDateOfMonth, allDates);
            cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
            lastDateOfMonth = cal.getTime();
            cal.set(Calendar.DATE, 1);
            fromDate = cal.getTime();
        }
        return allDatesMonthWise;
    }

}
