/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import java.io.File;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PathDirector {

    @Autowired
    private ServletContext servletContext;

    public String rootDirectoryPath() {
        String path = servletContext.getRealPath("/resources");
        return path;
    }

    public String imagesRootDirectoryPath() {
        return rootDirectoryPath() + File.separator + "images";
    }

    public String usersDirectoryPath() {
        return rootDirectoryPath() + File.separator + "users";
    }

    public String userRootDirectoryPath(long userid) {
        return usersDirectoryPath() + File.separator + userid;
    }

    public String imagesDirectoryPath(long userid) {
        return userRootDirectoryPath(userid) + File.separator + "images";
    }
    public String pdfDirectoryPath(long userid) {
        return userRootDirectoryPath(userid) + File.separator + "pdf";
    }

    public File rootDirectory() {
        // Creating or fetch the directory to store file
        File root = new File(rootDirectoryPath());
        if (!root.exists()) {
            root.mkdirs();
        }
        return root;
    }

    public File imagesRootDirectory() {
        // Creating or fetch the directory to store file
        File root_images = new File(imagesRootDirectoryPath());
        if (!root_images.exists()) {
            root_images.mkdirs();
        }
        return root_images;
    }

    public File userRootDirectory(long userid) {
        // Creating or fetch the directory to store file
        File userrootdir = new File(userRootDirectoryPath(userid));
        if (!userrootdir.exists()) {
            userrootdir.mkdirs();
        }
        return userrootdir;
    }

    public File imagesDirectory(long userid) {
        // Creating or fetch the directory to store file
        File user_images = new File(imagesDirectoryPath(userid));
        if (!user_images.exists()) {
            user_images.mkdirs();
        }
        return user_images;
    }
}
