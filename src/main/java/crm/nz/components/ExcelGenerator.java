package crm.nz.components;

import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.CountryTINBean;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Owner
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ExcelGenerator {

    private static String Reg_id = "";
    private static String Company_name = "";
    private static int i = 2;

    public ByteArrayInputStream getCompanydata(List<CompanyDetailBean> companyDetails) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Company data");
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFCellStyle redstyle = workbook.createCellStyle();
            HSSFCellStyle greenstyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            HSSFFont redFont = workbook.createFont();
            redFont.setColor(IndexedColors.RED.getIndex());
            redstyle.setFont(redFont);
            HSSFFont greenFont = workbook.createFont();
            greenFont.setColor(IndexedColors.GREEN.getIndex());
            greenstyle.setFont(greenFont);
            for (Row row : sheet) {
                sheet.removeRow(row);
            }
            int rowIndex = 2;
            HSSFRow titlerow = sheet.getRow(0) != null ? sheet.getRow(0) : sheet.createRow(0);
            HSSFCell titlecell = titlerow.createCell(3);
            titlecell.setCellStyle(style);
            sheet.autoSizeColumn(3);
            String[] heading = {"Title", "FirstName", "MiddleName", "Surname", "DateofBirth", "PreferredName", "Gender", "AddressLine1", "AddressLine2", "AddressLine3", "Suburb", "City", "Postcode", "IDType", "DriversLicenceNumber", "DriversLicenceVersion", "PassportNumber", "PassportExpiryDate", "BirthCertificateNumber", "CitizenCertificateNumber", "CountryofBirth", "Watchlist", "CountryofCitizenship", "CountryofResidency", "Consent", "Link", "IRDNumber", "Email", "Mobile", "Phone", "WorkPhone", "PIRRate", "USResident", "TINNumber1", "TINCountry1", "TINNumber2", "TINCountry2", "TINNumber3", "TINCountry3", "InvestmentExRef", "FundName", "FundName2", "FundName3", "InvestmentStrategy", "IsPrimary", "InvestorType", "Relationship", "CompanyNumber", "BankCode", "BankBranch", "Account", "Suffix", "BankAccountName", "UserDefined1", "InvestmentDefined1"};
            int rowIndex2 = 1;
            HSSFRow row = sheet.getRow(rowIndex2) != null ? sheet.getRow(rowIndex2) : sheet.createRow(rowIndex2);
            int cellval = 1;
            for (int j = 0; j < heading.length; j++) {
                String headValue = heading[j];
                HSSFCell cell = row.createCell(cellval);
                cell.setCellValue(headValue);
                cell.setCellStyle(style);
                sheet.autoSizeColumn(j);
                cellval++;
            }

            for (int j = 0; j < companyDetails.size(); j++) {
                CompanyDetailBean bean = companyDetails.get(j);
                HSSFRow fstRow = sheet.getRow(rowIndex + j) != null ? sheet.getRow(rowIndex + j) : sheet.createRow(rowIndex + j);
                HSSFCell cell = fstRow.createCell(1);
                cell.setCellValue("");
                cell = fstRow.createCell(2);

                String person = "";
                if (bean.getReg_id() == null ? Reg_id == null : bean.getReg_id().equals(Reg_id)) {
                    if ("COMPANY_ACCOUNT".equals(bean.getReg_type())) {
                        person = "Director " + i;

                        i++;
                    } else if ("TRUST_ACCOUNT".equals(bean.getReg_type())) {
                        person = "Trustee " + i;
                        i++;
                    }
                } else {
                    if ("COMPANY_ACCOUNT".equals(bean.getReg_type())) {
                        person = "Director";
                        i = 2;
                    } else if ("TRUST_ACCOUNT".equals(bean.getReg_type())) {
                        person = "Trustee";
                        i = 2;
                    }
                }

                if (bean.getFname() != null || bean.getReg_id().equals(Reg_id)) {
                    String fname[] = bean.getFname().split(" ", 2);
                    String F = "", M = "";
                    if (fname.length == 1) {
                        F = "( " + person + ")";
                    } else {
                        M = "( " + person + " )";;
                    }
                    cell.setCellValue(fname[0] + " " + F);
                    if (fname.length == 2) {
                        cell = fstRow.createCell(3);
                        if (fname[1] != null) {
                            cell.setCellValue(fname[1] + " " + M);
                        } else {
                            cell.setCellValue(fname[2] + " " + M);
                        }
                    }
                }

                if (bean.getCompanyName() != null || bean.getReg_id().equals(Reg_id)) {
                    if (bean.getReg_id().equals(Reg_id)) {
                        bean.setCompanyName(Company_name);
                    }
                    cell = fstRow.createCell(4);
                    cell.setCellValue(bean.getCompanyName());

                }
                Reg_id = bean.getReg_id();
                Company_name = bean.getCompanyName();
                person = "";
                if (bean.getDateOfBirth() != null) {
                    cell = fstRow.createCell(5);
                    cell.setCellValue(bean.getDateOfBirth());
                }
                if (bean.getFname() != null) {
                    cell = fstRow.createCell(6);
                    cell.setCellValue(bean.getFname());
                }
                cell = fstRow.createCell(7);
                cell.setCellValue("");
                if (bean.getAddress() != null) {
//                    String address[] = bean.getAddress().split(" ");
                    HSSFCell cell2 = fstRow.createCell(8);
                    HSSFCell cell3 = fstRow.createCell(9);
                    HSSFCell cell4 = fstRow.createCell(10);
                    cell2.setCellValue(bean.getAddress());
//                    cell2.setCellValue(address[0]);
//                    if (address.length == 2) {
//                        cell3.setCellValue(address[1]);
//                    } else if (address.length == 3) {
//                        cell3.setCellValue(address[1]);
//                        cell4.setCellValue(address[2]);
//                    }
                }
                cell = fstRow.createCell(11);
                cell.setCellValue("");
                if (bean.getCitizenUS() != null) {
                    cell = fstRow.createCell(12);
                    cell.setCellValue(bean.getCitizenUS());
                }
                cell = fstRow.createCell(13);
                cell.setCellValue("");

                if (bean.getSrcOfFund() != null) {
                    cell = fstRow.createCell(14);
                    cell.setCellValue(bean.getSrcOfFund());
                }
                if (bean.getLicenseNumber() != null) {
                    cell = fstRow.createCell(15);
                    cell.setCellValue(bean.getLicenseNumber());
                }
                if (bean.getVersionNumber() != null) {
                    cell = fstRow.createCell(16);
                    cell.setCellValue(bean.getVersionNumber());
                }
                if (bean.getPassportNumber() != null) {
                    cell = fstRow.createCell(17);
                    cell.setCellValue(bean.getPassportNumber());
                }
                if (bean.getPassportExpiryDate() != null) {
                    cell = fstRow.createCell(18);
                    cell.setCellValue(bean.getPassportExpiryDate());
                }
                cell = fstRow.createCell(19);
                cell.setCellValue("");
                cell = fstRow.createCell(20);
                cell.setCellValue("");
                cell = fstRow.createCell(21);
                cell.setCellValue("");
                cell = fstRow.createCell(22);
                cell.setCellValue("");

                if (bean.getCountryOfResidence() != null) {
                    cell = fstRow.createCell(23);
                    cell.setCellValue(bean.getCountryOfResidence());
                }
                if (bean.getCountryOfResidence() != null) {
                    cell = fstRow.createCell(24);
                    cell.setCellValue(bean.getCountryOfResidence());
                }
                cell = fstRow.createCell(25);
                cell.setCellValue("");
                cell = fstRow.createCell(26);
                cell.setCellValue("");
                if (bean.getIrdNumber() != null) {
                    cell = fstRow.createCell(27);
                    cell.setCellValue(bean.getIrdNumber());
                }
                if (bean.getUsername() != null) {
                    cell = fstRow.createCell(28);
                    if (bean.getEmail() != null) {
                        cell.setCellValue(bean.getEmail());
                    } else {
                        cell.setCellValue(bean.getUsername());
                    }
                }
                if (bean.getMobileNo() != null) {
                    cell = fstRow.createCell(29);
                    cell.setCellValue(bean.getMobileNo());
                }
                if (bean.getMobileNo() != null) {
                    cell = fstRow.createCell(30);
                    cell.setCellValue(bean.getMobileNo());
                }
                cell = fstRow.createCell(31);
                cell.setCellValue("");
                if (bean.getPir() != null) {
                    cell = fstRow.createCell(32);
                    cell.setCellValue(bean.getPir());
                }
                if (bean.getCitizenUS() != null) {
                    cell = fstRow.createCell(33);
                    cell.setCellValue(bean.getCitizenUS());
                }
                List<CountryTINBean> countryTINList = bean.getCountryTINList();
                if (countryTINList != null && !countryTINList.isEmpty()) {
                    for (int i = 0; i < countryTINList.size(); i++) {
                        CountryTINBean countrybean = countryTINList.get(i);
                        if (i == 0) {
                            if (countrybean.getCountry() != null) {
                                cell = fstRow.createCell(34);
                                cell.setCellValue(countrybean.getCountry());
                            }
                            if (countrybean.getTin() != null) {
                                cell = fstRow.createCell(35);
                                cell.setCellValue(countrybean.getTin());
                            }
                        }
                        if (i == 1) {
                            if (countrybean.getCountry() != null) {
                                cell = fstRow.createCell(36);
                                cell.setCellValue(countrybean.getCountry());
                            }
                            if (countrybean.getTin() != null) {
                                cell = fstRow.createCell(37);
                                cell.setCellValue(countrybean.getTin());
                            }
                        }
                        if (i == 2) {
                            if (countrybean.getCountry() != null) {
                                cell = fstRow.createCell(38);
                                cell.setCellValue(countrybean.getCountry());
                            }
                            if (countrybean.getTin() != null) {
                                cell = fstRow.createCell(39);
                                cell.setCellValue(countrybean.getTin());
                            }
                        }
                    }
                }
                cell = fstRow.createCell(40);
                cell.setCellValue("");
                cell = fstRow.createCell(41);
                cell.setCellValue("");
                cell = fstRow.createCell(42);
                cell.setCellValue("");
                cell = fstRow.createCell(43);
                cell.setCellValue("");
                cell = fstRow.createCell(44);
                cell.setCellValue("");
                cell = fstRow.createCell(45);
                cell.setCellValue("");
                cell = fstRow.createCell(46);
                cell.setCellValue("");
                cell = fstRow.createCell(47);
                cell.setCellValue("");
                if (bean.getRegisterNumber() != null) {
                    cell = fstRow.createCell(48);
                    cell.setCellValue(bean.getRegisterNumber());
                }
                if (bean.getBankCode() != null) {
                    cell = fstRow.createCell(49);
                    cell.setCellValue(bean.getBankCode());
                }
                cell = fstRow.createCell(50);
                cell.setCellValue("");
                if (bean.getAccountNumber() != null) {
                    cell = fstRow.createCell(51);
                    cell.setCellValue(bean.getAccountNumber());
                }
                cell = fstRow.createCell(52);
                cell.setCellValue("");
                if (bean.getBankName() != null) {
                    cell = fstRow.createCell(53);
                    cell.setCellValue(bean.getBankName());
                }
                cell = fstRow.createCell(54);
                cell.setCellValue("");
                cell = fstRow.createCell(55);
                cell.setCellValue("");
                sheet.autoSizeColumn(j);
            }

            workbook.write(out);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ExcelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public ByteArrayInputStream getTransectiondata(List<PendingTransactionBean> pendingTransaction) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("transection data");
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFCellStyle redstyle = workbook.createCellStyle();
            HSSFCellStyle greenstyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            HSSFFont redFont = workbook.createFont();
            redFont.setColor(IndexedColors.RED.getIndex());
            redstyle.setFont(redFont);
            HSSFFont greenFont = workbook.createFont();
            greenFont.setColor(IndexedColors.GREEN.getIndex());
            greenstyle.setFont(greenFont);
            for (Row row : sheet) {
                sheet.removeRow(row);
            }
            int rowIndex = 2;
            HSSFRow titlerow = sheet.getRow(0) != null ? sheet.getRow(0) : sheet.createRow(0);
            HSSFCell titlecell = titlerow.createCell(3);
            titlecell.setCellStyle(style);
            sheet.autoSizeColumn(3);
            String[] heading = {"InvestmentNumber", "Type", "Date", "FromFund", "ToFund", "Amount", "AmountType", "Comments", "ExternalReference"};
            int rowIndex2 = 1;
            HSSFRow row = sheet.getRow(rowIndex2) != null ? sheet.getRow(rowIndex2) : sheet.createRow(rowIndex2);
            int cellval = 1;
            for (int j = 0; j < heading.length; j++) {
                String headValue = heading[j];
                HSSFCell cell = row.createCell(cellval);
                cell.setCellValue(headValue);
                cell.setCellStyle(style);
                sheet.autoSizeColumn(j);
                cellval++;
            }
            for (int j = 0; j < pendingTransaction.size(); j++) {
                PendingTransactionBean bean = pendingTransaction.get(j);
                HSSFRow fstRow = sheet.getRow(rowIndex + j) != null ? sheet.getRow(rowIndex + j) : sheet.createRow(rowIndex + j);
                HSSFCell cell = fstRow.createCell(1);
                if (bean.getInvestmentcode() != null) {
                    cell.setCellValue(bean.getInvestmentcode());
                }
                if (bean.getType() != null) {
                    cell = fstRow.createCell(2);
                    cell.setCellValue(bean.getType());
                }
                if (bean.getCreated_ts() != null) {
                    cell = fstRow.createCell(3);
                    cell.setCellValue(bean.getCreated_ts());
                }
                if (bean.getPortfolioName() != null) {
                    cell = fstRow.createCell(4);
                    cell.setCellValue(bean.getPortfolioName());
                }
                cell = fstRow.createCell(5);
                cell.setCellValue("");

                if (bean.getAmount() != null) {
                    cell = fstRow.createCell(6);
                    cell.setCellValue(bean.getAmount());
                }
                cell = fstRow.createCell(7);
                cell.setCellValue("N");
                cell = fstRow.createCell(8);
                cell.setCellValue("");
                cell = fstRow.createCell(9);
                cell.setCellValue("");

                sheet.autoSizeColumn(j);
            }

            workbook.write(out);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ExcelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public ByteArrayInputStream getDirectDebitdata(List<PendingInvestmentBean> pendingInvestment) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("transection data");
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFCellStyle redstyle = workbook.createCellStyle();
            HSSFCellStyle greenstyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            HSSFFont redFont = workbook.createFont();
            redFont.setColor(IndexedColors.RED.getIndex());
            redstyle.setFont(redFont);
            HSSFFont greenFont = workbook.createFont();
            greenFont.setColor(IndexedColors.GREEN.getIndex());
            greenstyle.setFont(greenFont);
            for (Row row : sheet) {
                sheet.removeRow(row);
            }
            int rowIndex = 2;
            HSSFRow titlerow = sheet.getRow(0) != null ? sheet.getRow(0) : sheet.createRow(0);
            HSSFCell titlecell = titlerow.createCell(3);
            titlecell.setCellStyle(style);
            sheet.autoSizeColumn(3);
            String[] heading = {"CustomerNumber", "InvestmentCode", "InvestmentScheme", "InvestmentExternalReference",
                "PortfolioCode", "StandingInstructionAmount",
                "StandingInstructionUnits", "StandingInstructionFrequency", "StandingInstructionType",
                "StandingInstructionStartDate", "StandingInstructionEndDate", "BankCode",
                "BankBranch", "Account", "Suffix", "BankAccountName", "StandingInstructionAmountType", "StandingInstructionExternalReference",
                "OneOffContribution"};
            int rowIndex2 = 1;
            HSSFRow row = sheet.getRow(rowIndex2) != null ? sheet.getRow(rowIndex2) : sheet.createRow(rowIndex2);
            int cellval = 1;
            for (int j = 0; j < heading.length; j++) {
                String headValue = heading[j];
                HSSFCell cell = row.createCell(cellval);
                cell.setCellValue(headValue);
                cell.setCellStyle(style);
                sheet.autoSizeColumn(j);
                cellval++;
            }
            for (int j = 0; j < pendingInvestment.size(); j++) {
                PendingInvestmentBean bean = pendingInvestment.get(j);
                HSSFRow fstRow = sheet.getRow(rowIndex + j) != null ? sheet.getRow(rowIndex + j) : sheet.createRow(rowIndex + j);
                HSSFCell cell = fstRow.createCell(1);
                if (bean.getBeneficiaryId() != null) {
                    cell.setCellValue(bean.getBeneficiaryId());
                }
                if (bean.getInvestmentCode() != null) {
                    cell = fstRow.createCell(2);
                    cell.setCellValue(bean.getInvestmentCode());
                }

                cell = fstRow.createCell(3);
                cell.setCellValue("KS");

                if (bean.getInvestmentCode() != null) {
                    cell = fstRow.createCell(4);
                    cell.setCellValue(bean.getInvestmentCode());
                }

                if (bean.getPortfolioCode() != null) {
                    cell = fstRow.createCell(5);
                    cell.setCellValue(bean.getPortfolioCode());
                }

                if (bean.getInvestedAmount() != null) {
                    cell = fstRow.createCell(6);
                    cell.setCellValue(bean.getInvestedAmount());
                }

                cell = fstRow.createCell(7);
                cell.setCellValue("");

                if (bean.getSelectfrequecy() != null) {
                    cell = fstRow.createCell(8);
                    cell.setCellValue(bean.getSelectfrequecy());
                }

                if ("DEBIT".equals(bean.getPaymentMethod())) {
                    bean.setPaymentMethod("DD");
                }
                if (bean.getPaymentMethod() != null) {
                    cell = fstRow.createCell(9);
                    cell.setCellValue(bean.getPaymentMethod());
                }
                if (bean.getStartDate() != null) {
                    cell = fstRow.createCell(10);
                    cell.setCellValue(bean.getStartDate());
                }
                if (bean.getEndDate() != null) {
                    cell = fstRow.createCell(11);
                    cell.setCellValue(bean.getEndDate());
                }
                if (bean.getBankCode() != null) {
                    cell = fstRow.createCell(12);
                    cell.setCellValue(bean.getBankCode());
                }
                if (bean.getBranch() != null) {
                    cell = fstRow.createCell(13);
                    cell.setCellValue(bean.getBranch());
                }
                if (bean.getBankAccountNumber() != null) {
                    cell = fstRow.createCell(14);
                    cell.setCellValue(bean.getBankAccountNumber());
                }
                if (bean.getSuffix() != null) {
                    cell = fstRow.createCell(15);
                    cell.setCellValue(bean.getSuffix());
                }
                if (bean.getBankAccountName() != null) {
                    cell = fstRow.createCell(16);
                    cell.setCellValue(bean.getBankAccountName());
                }

                cell = fstRow.createCell(17);
                cell.setCellValue("N");

                if (bean.getInvestmentCode() != null) {
                    cell = fstRow.createCell(18);
                    cell.setCellValue(bean.getInvestmentCode());
                }
                cell = fstRow.createCell(19);
                cell.setCellValue("");

                sheet.autoSizeColumn(j);
            }

            workbook.write(out);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ExcelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public ByteArrayInputStream getMasterTransectiondata(List<PendingTransactionBean> pendingTransaction) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("transection data");
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFCellStyle redstyle = workbook.createCellStyle();
            HSSFCellStyle greenstyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            HSSFFont redFont = workbook.createFont();
            redFont.setColor(IndexedColors.RED.getIndex());
            redstyle.setFont(redFont);
            HSSFFont greenFont = workbook.createFont();
            greenFont.setColor(IndexedColors.GREEN.getIndex());
            greenstyle.setFont(greenFont);
            for (Row row : sheet) {
                sheet.removeRow(row);
            }
            int rowIndex = 2;
            HSSFRow titlerow = sheet.getRow(0) != null ? sheet.getRow(0) : sheet.createRow(0);
            HSSFCell titlecell = titlerow.createCell(3);
            titlecell.setCellStyle(style);
            sheet.autoSizeColumn(3);
            String[] heading = {"Type", "Date", "Fund", "Client Number", "Units", "Gross Amount", "Net Amount", "Extra", "Comments", "Price", "Date", "ExternalReference", "Destination"};
            int rowIndex2 = 1;
            HSSFRow row = sheet.getRow(rowIndex2) != null ? sheet.getRow(rowIndex2) : sheet.createRow(rowIndex2);
            int cellval = 1;
            for (int j = 0; j < heading.length; j++) {
                String headValue = heading[j];
                HSSFCell cell = row.createCell(cellval);
                cell.setCellValue(headValue);
                cell.setCellStyle(style);
                sheet.autoSizeColumn(j);
                cellval++;
            }
            for (int j = 0; j < pendingTransaction.size(); j++) {
                PendingTransactionBean bean = pendingTransaction.get(j);
                HSSFRow fstRow = sheet.getRow(rowIndex + j) != null ? sheet.getRow(rowIndex + j) : sheet.createRow(rowIndex + j);
                HSSFCell cell = fstRow.createCell(1);
                cell.setCellValue("RED");

                if (bean.getCreated_ts() != null) {
                    cell = fstRow.createCell(2);
                    cell.setCellValue(bean.getCreated_ts());
                }
                if (bean.getPortfolioName() != null) {
                    cell = fstRow.createCell(3);
                    cell.setCellValue(bean.getPortfolioName());
                }
                if (bean.getInvestmentcode() != null) {
                    cell = fstRow.createCell(4);
                    cell.setCellValue(bean.getInvestmentcode());
                }
                if (bean.getUnits() != null) {
                    cell = fstRow.createCell(5);
                    cell.setCellValue(bean.getUnits());
                }
                if (bean.getAmount() != null) {
                    cell = fstRow.createCell(6);
                    cell.setCellValue(bean.getAmount());
                }
                if (bean.getAmount() != null) {
                    cell = fstRow.createCell(7);
                    cell.setCellValue(bean.getAmount());
                }

                cell = fstRow.createCell(8);
                cell.setCellValue("");

                cell = fstRow.createCell(9);
                cell.setCellValue("");
                
                if (bean.getUnitprice() != null) {
                    cell = fstRow.createCell(10);
                    cell.setCellValue(bean.getUnitprice());
                }
                if (bean.getCreated_ts() != null) {
                    cell = fstRow.createCell(11);
                    cell.setCellValue(bean.getCreated_ts());
                }
                if (bean.getInvestmentcode() != null) {
                    cell = fstRow.createCell(12);
                    cell.setCellValue(bean.getInvestmentcode());
                }
                cell = fstRow.createCell(13);
                cell.setCellValue("");
                sheet.autoSizeColumn(j);
            }

            workbook.write(out);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ExcelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
