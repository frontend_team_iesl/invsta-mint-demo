/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.components;

import crm.nz.services.DateTimeServiceImpl;
import crm.nz.services.InvestmentsAsAtService;
import crm.nz.services.UserUpdatedUnitsService;
import java.util.Calendar;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
//@Component
//@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class TimeManager {

    @Autowired
    private UserUpdatedUnitsExecuter executer;
    @Autowired
    private SaveLatestPortfolioPriceExecuter executer1;

    public TimeManager() {
        executer = new UserUpdatedUnitsExecuter();
        executer.start();
        executer1 = new SaveLatestPortfolioPriceExecuter();
        executer1.start();
    }

    class SaveLatestPortfolioPriceExecuter extends Thread {

        @Autowired
        private InvestmentsAsAtService service;
        private Date targetDate;
        private Logger logger = LoggerFactory.getLogger(SaveLatestPortfolioPriceExecuter.class);

        public SaveLatestPortfolioPriceExecuter() {
//            service = new InvestmentsAsAtService();
            Date now = DateTimeServiceImpl.current();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.set(Calendar.HOUR, 11);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.AM_PM, Calendar.PM);
            targetDate = cal.getTime();
        }

        @Override
        public void run() {
            while (true) {
                Date now = DateTimeServiceImpl.current();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.set(Calendar.SECOND, 59);
                cal.set(Calendar.MILLISECOND, 0);
                now = cal.getTime();
                logger.debug(service + "Mint Right Now:-" + now + ", Target DateTime:-" + targetDate);
                System.err.println(service + "Mint Right Now:-" + now + ", Target DateTime:-" + targetDate);
                try {
                    if (now.after(targetDate)) {
                        service.saveLatestPortfoliosPrices();
                        cal.setTime(targetDate);
                        cal.add(Calendar.DAY_OF_YEAR, 1);
                        targetDate = cal.getTime();
                    }
                    Thread.sleep(CommonMethods.ONE_MINUTE);
                } catch (InterruptedException ex) {
                    logger.debug(" Right Now:-" + now, ex);
                }
            }
        }
    }

    class UserUpdatedUnitsExecuter extends Thread {

        @Autowired
        private UserUpdatedUnitsService service;
        private Date targetDate;
        private Logger logger = LoggerFactory.getLogger(UserUpdatedUnitsExecuter.class);

        public UserUpdatedUnitsExecuter() {
//            service = new UserUpdatedUnitsService();
            Date now = DateTimeServiceImpl.current();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.set(Calendar.HOUR, 11);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.AM_PM, Calendar.PM);
            targetDate = cal.getTime();
        }

        @Override
        public void run() {
            while (true) {
                Date now = DateTimeServiceImpl.current();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.set(Calendar.SECOND, 59);
                cal.set(Calendar.MILLISECOND, 0);
                now = cal.getTime();
                logger.debug(service + "Mint Right Now:-" + now + ", Target DateTime:-" + targetDate);
                System.err.println(service + "Mint Right Now:-" + now + ", Target DateTime:-" + targetDate);
                try {
                    if (now.after(targetDate)) {
                        //        String fileName = "UpdateUnits".concat(common.format3(new Date())).concat(".xlsx");
                        String fileName = "UpdateUnits.xlsx";
                        service.executeFlatFile(fileName);
                        cal.setTime(targetDate);
                        cal.add(Calendar.DAY_OF_YEAR, 1);
                        targetDate = cal.getTime();
                    }
                    Thread.sleep(CommonMethods.ONE_MINUTE);
                } catch (InterruptedException ex) {
                    logger.debug(" Right Now:-" + now, ex);
                }
            }
        }
    }
}
