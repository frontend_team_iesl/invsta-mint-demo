/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.lambda.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class NotificationDetailsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void deleteNotification(String id) throws SQLException {
        Connection con = jdbcTemplate.getDataSource().getConnection();
        String updateNotifySql = "UPDATE `notification` SET `active` = 'N' WHERE `id` = ?;";
        PreparedStatement ps = con.prepareStatement(updateNotifySql);
        ps.setString(1, id);
        ps.execute();

    }

}
