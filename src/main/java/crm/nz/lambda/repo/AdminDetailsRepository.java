/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.lambda.repo;

import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.Advisor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64.Encoder;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class AdminDetailsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BCryptPasswordEncoder encoder;

    public void saveAdvisorDetails(Advisor advisor, HashMap response) {
        try {
//            getUpdByEmail(phone);
            if (advisor != null) {
                Connection con = jdbcTemplate.getDataSource().getConnection();
                try {
                    addAdvisorDetails(con, advisor, response);
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                response.put("address", "Your address change request accept successfully.");
            } else {
                response.put("address", "You have Submited info Successfully.");
            }
            response.put("message", "You have Submited info Successfully.");
            response.put("status", 200);
        } catch (SQLException ex) {
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addAdvisorDetails(Connection con, Advisor advisor, HashMap response) {
        try {
            String innerSql = "INSERT INTO `advisors`\n"
                    + " (`advisorName`,`companyName`,`advisorDob`,`active`,`created_ts`,`created_by`,`username`,`password`)\n"
                    + " VALUES (?,?,?,'y',?,?,?,?);";

            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, advisor.getAdvisorName());
            preparedStatement.setString(2, advisor.getCompanyName());
            preparedStatement.setString(3, advisor.getAdvisorDob());
            preparedStatement.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
            preparedStatement.setString(5, advisor.getCreated_by());
            preparedStatement.setString(6, advisor.getUsername());
            String password = encoder.encode(advisor.getPassword());
            preparedStatement.setString(7, password);
            int i = preparedStatement.executeUpdate();

            String innerSql1 = "INSERT INTO `login_master`\n"
                    + "(`username`,`password`,`created_ts`,`active`,`created_by`,`role`)\n"
                    + "VALUES (?,?,?,'y',?,'ADVISOR');";

            PreparedStatement preparedStatement1 = con.prepareStatement(innerSql1, Statement.RETURN_GENERATED_KEYS);
            preparedStatement1.setString(1, advisor.getUsername());
            String encode = encoder.encode(advisor.getPassword());
            preparedStatement1.setString(2, encode);
            preparedStatement1.setDate(3, new java.sql.Date(new java.util.Date().getTime()));
            preparedStatement1.setString(4, advisor.getCreated_by());
            preparedStatement1.executeUpdate();
            con.close();

            response.put("message", "You have created Advisor");
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(SettingDetailsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
