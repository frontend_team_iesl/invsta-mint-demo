/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.table.bean;

import crm.nz.constants.Constants;
import java.lang.reflect.Field;

/**
 *
 * @author IESL
 */
public interface ToString {

    default String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                builder.append("\"").append(fieldName).append("\"");
                builder.append(":");
                builder.append(ToObjectConverter.checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    builder.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                getLogger().error("", ex);
            }
        }
        builder.append("}");
        return builder.toString();
    }

    public org.slf4j.Logger getLogger();
}
