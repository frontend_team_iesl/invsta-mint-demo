/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.table.bean;

import crm.nz.constants.Constants;
import static crm.nz.table.bean.ToObjectConverter.checkNull;
import java.util.Collection;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author TOSHIBA R830
 */
public class SecuredUser extends org.springframework.security.core.userdetails.User {

    public SecuredUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    private String investmentCode,beneficiairyId;

    public String getBeneficiairyId() {
        return beneficiairyId;
    }

    public void setBeneficiairyId(String beneficiairyId) {
        this.beneficiairyId = beneficiairyId;
    }

    public String getInvestmentCode() {
        return investmentCode;
    }

    public void setInvestmentCode(String investmentCode) {
        this.investmentCode = investmentCode;
    }
    private String created_ts;
    private String created_by;
    private String role;
    private String name;
    private String active;
    private String userLogoPath;
    private String client_id, application_id;
    private String user_id, step, token, secret;
    private Boolean verification;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SecuredUser.class);

    public String getUserLogoPath() {
        return userLogoPath;
    }

    public void setUserLogoPath(String userLogoPath) {
        this.userLogoPath = userLogoPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getVerification() {
        return verification;
    }

    public void setVerification(Boolean verification) {
        this.verification = verification;
    }

    public String getCreated_ts() {
        return created_ts;
    }

    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(Login login) {
        this.created_ts = login.getCreated_ts();
        this.created_by = login.getCreated_by();
        this.role = login.getRole();
        this.active = login.getActive();
        this.client_id = login.getClient_id();
        this.user_id = login.getUser_id();
        this.application_id = login.getApplication_id();
        this.name = login.getName();
        this.setToken(login.getToken());
        this.setStep(login.getStep());
        this.setSecret(login.getSecret());
        this.verification = false;
        this.userLogoPath = login.getUserLogoPath();
        this.investmentCode = login.getInvestmentCode();
        this.beneficiairyId = login.getBeneficiairyId();
    }

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
//                        checkNull = encrypt();
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the step
     */
    public String getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

}
