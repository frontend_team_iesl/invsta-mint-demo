/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.DataZooServicebean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class DataZooService extends HttpService {

    {
        System.out.println("DataZooService");
    }

//    final static String username = "Invsta_Test";
//    final static String password = "4SnTBx*89sLm";
//    final static String apiUrl = "https://resttest.datazoo.com/api";
//    @Autowired
//    private String dataZooPassword;
//    @Autowired
//    private String dataZooApiUrl;
    @Autowired
    private DataZooServicebean dataZooServicebean;

//    private String auth = dataZooServicebean.getDataZooUsername() + ":" + dataZooServicebean.getDataZooPassword();
//    private String authentication = Base64.getEncoder().encodeToString(auth.getBytes());

    public String sessionToken() {
        String auth = dataZooServicebean.getDataZooUsername() + ":" + dataZooServicebean.getDataZooPassword();
        String authentication = Base64.getEncoder().encodeToString(auth.getBytes());
        try {
            URL url = new URL(dataZooServicebean.getDataZooApiUrl().concat("/Authenticate.json"));
            String body = "{\n"
                    + "    \"UserName\": \"" + dataZooServicebean.getDataZooUsername() + "\",\n"
                    + "    \"Password\": \"" + dataZooServicebean.getDataZooPassword() + "\"\n"
                    + "}";
            HashMap<String, String> hm = new HashMap();
            hm.put("Authorization", "Basic " + authentication);
            StringBuilder sessionToken = postResponse(url, null, body, hm, null);
            System.out.println("sessionToken : " + sessionToken);
            return sessionToken.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder verifyNZTADL(String sessionToken, PersonDetailsBean dlPerson) {
        try {
            URL url = new URL(dataZooServicebean.getDataZooApiUrl().concat("/NewZealand/Verify.json"));
            String body = "{\n"
                    + "    \"dataSources\": [\n"
                    + "        \"NZTA Drivers License\"\n"
                    + "    ],\n"
                    + "    \"reportingReference\": null,\n"
                    + "    \"firstName\": \"" + dlPerson.getFirstName() + "\",\n"
                    + "    \"middleName\": \"" + dlPerson.getMiddleName() + "\",\n"
                    + "    \"lastName\": \"" + dlPerson.getLastName() + "\",\n"
                    + "    \"dateOfBirth\": \"" + dlPerson.getDate_of_Birth() + "\",\n"
                    + "    \"driversLicenceNo\": \"" + dlPerson.getLicense_number() + "\",\n"
                    + "    \"driversLicenceVersion\": \"" + dlPerson.getLicence_verson_number() + "\",\n"
                    + "    \"driversLicenceConsentObtained\": true\n"
                    + "}";
            HashMap<String, String> hMap = new HashMap();
            hMap.put("SessionToken", sessionToken);
            hMap.put("UserName", dataZooServicebean.getDataZooUsername());
            System.out.println("insiede dl verify datazooo----------------------------------");
            StringBuilder response = postResponse(url, null, body, hMap, null);
            System.out.println("response : " + response);
            System.out.println("insiede dl verify datazooo----------------------------------" + response);

            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder verifyNZDIAPP(String sessionToken, PersonDetailsBean dlPerson) {
        try {
            URL url = new URL(dataZooServicebean.getDataZooApiUrl().concat("/NewZealand/Verify.json"));
            String body = "{\n"
                    + "    \"dataSources\": [\n"
                    + "        \"DIA-Passport\"\n"
                    + "    ],\n"
                    + "    \"reportingReference\": null,\n"
                    + "    \"firstName\": \"" + dlPerson.getFirstName() + "\",\n"
                    + "    \"middleName\": \"" + dlPerson.getMiddleName() + "\",\n"
                    + "    \"lastName\": \"" + dlPerson.getLastName() + "\",\n"
                    + "    \"dateOfBirth\": \"" + dlPerson.getDate_of_Birth() + "\",\n"
                    + "    \"passportNo\": \"" + dlPerson.getPassport_number() + "\",\n"
                    + "    \"passportExpiryDate\": \"" + dlPerson.getPassport_expiry() + "\",\n"
                    + "    \"passportConsentObtained\": true\n"
                    + "}";
            HashMap<String, String> hMap = new HashMap();
            hMap.put("SessionToken", sessionToken);
            hMap.put("UserName", dataZooServicebean.getDataZooUsername());
            System.out.println("insiede pp verify datazooo    header ----------------------------------" + hMap + "--------------body" + body);
            StringBuilder response = postResponse(url, null, body, hMap, null);
            System.out.println("response : " + response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
     public StringBuilder verifyNZPEP(String sessionToken, PersonDetailsBean dlPerson) {
        try {
            URL url = new URL(dataZooServicebean.getDataZooApiUrl().concat("/Australia/Verify.json"));
            String Title=dlPerson.getTitle().equals("Mr")? "male"  : "female";
            String body = "{\n"
                    + "    \"dataSources\": [\n"
                    + "        \"Watchlist AML\"\n"
                    + "    ],\n"
                    + "    \"reportingReference\": null,\n"
                    + "    \"firstName\": \"" + dlPerson.getFirstName() + "\",\n"
                    + "    \"lastName\": \"" + dlPerson.getLastName() + "\",\n"
                     + "    \"gender\":\""+ Title+"\",\n"
                    + "    \"dateOfBirth\": \"" + dlPerson.getDate_of_Birth() + "\"\n"
                    + "}";
            HashMap<String, String> hMap = new HashMap();
            hMap.put("SessionToken", sessionToken);
            hMap.put("UserName", dataZooServicebean.getDataZooUsername());
            System.out.println("insiede dl verify datazooo----------------------------------");
            StringBuilder response = postResponse(url, null, body, hMap, null);
            System.out.println("response : " + response);
            System.out.println("insiede dl verify datazooo----------------------------------" + response);

            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    

}
