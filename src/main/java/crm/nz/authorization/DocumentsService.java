/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.io.IOException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class DocumentsService extends HttpService{

//    @Autowired
//     private  String oAuth2TokenApiUrl;
    
    
      @Autowired
 OAuth2TokenServiceBean oAuth2TokenServiceBean;
// url/document/listofdocuments?documentType=csv
    public StringBuilder listOfDocuments(String bearerToken) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/document/listofdocuments?documentType=csv"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("listOfDocuments : " + builder);
            return builder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
