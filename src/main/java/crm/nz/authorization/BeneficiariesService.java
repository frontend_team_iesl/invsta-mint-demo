/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import crm.nz.services.HttpService;
import java.net.MalformedURLException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class BeneficiariesService extends HttpService {

// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken) {
        return beneficiaries(bearerToken, 1);
    }
    @Autowired
    OAuth2TokenServiceBean oAuth2TokenServiceBean;

//    @Autowired
//    private String oAuth2TokenApiUrl;
// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken, int pageIndex) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiaries/" + pageIndex + "?page=1&pageSize=200"));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaries : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiaries/{page}
    public StringBuilder beneficiaries(String bearerToken, int pageIndex, int pageSize) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiaries/" + pageIndex + "?page=1&pageSize=" + pageSize));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaries : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiary/{id}
    public StringBuilder beneficiaryById(String bearerToken, String id) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/" + id));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiaryById : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

// url/beneficiary/external/{customerNumber}
    public StringBuilder beneficiaryByCustomerNumber(String bearerToken, String customerNumber) {
        try {
            URL url = new URL(oAuth2TokenServiceBean.getoAuth2TokenApiUrl().concat("/beneficiary/external/" + customerNumber));
            StringBuilder builder = getResponse(url, bearerToken);
            System.out.println("beneficiary : " + builder);
            return builder;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //    @RequestMapping(value = {"/beneficiary-dashboard"}, method = {RequestMethod.GET})
//    @ResponseBody
//    public String beneficiaryDashboardDB(
//            @RequestParam(value = "id", required = false) String id) {
//        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            if (id == null || id != null && id.isEmpty()) {
//                id = user.getUser_id();
//            }
//            if (id != null) {
//                Beneficiary beneficiary = beneficiaryrepository.getBeneficiary(id);
//                Type type = null;
//                List<InvestmentHolding> investmentHoldings = null;
//                List<InvestmentPerformance> investmentPerformances = null;
//                List<FmcaInvestmentMix> fmcaInvestmentMix = null;
//                if (beneficiary != null) {
//                    investmentHoldings = investrepository.getInvestmentHoldings(id, null);
//                    investmentPerformances = investrepository.getInvestmentPerformances(id, null);
//                    fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(id, null, null);
//                } else {
//                    final String BeneficiaryId = id;
//                    String clientCredentials = oAuth2TokenService.getClientCredentials();
//                    List<Investment> investments = investrepository.getInvestments(BeneficiaryId);
//                    if (investments == null || investments.isEmpty()) {
//                        StringBuilder result0 = beneficiariesService.beneficiaryById(clientCredentials, id);
//                        beneficiary = gson.fromJson(result0.toString(), Beneficiary.class);
////                    beneficiaryrepository.saveBeneficiary(beneficiary);
//                        investments = beneficiary.getInvestments();
//                        investments.forEach((Investment inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                        });
//                        investrepository.saveInvestment(investments);
//                    }
//                    for (Investment investment : investments) {
//                        final String InvestmentCode = investment.getCode();
//                        StringBuilder result2 = investmentsService.investmentHoldingSummaryValues(clientCredentials, InvestmentCode);//we need request for this investment holding summary
//                        type = new TypeToken<LinkedList<InvestmentHolding>>() {
//                        }.getType();
//                        investmentHoldings = gson.fromJson(result2.toString(), type);
//                        investmentHoldings.forEach((InvestmentHolding inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                        });
//                        investrepository.saveInvestmentHolding(investmentHoldings);
//                        StringBuilder result1 = investmentsService.investmentPerformance(clientCredentials, InvestmentCode);
//                        type = new TypeToken<LinkedList<InvestmentPerformance>>() {
//                        }.getType();
//                        investmentPerformances = gson.fromJson(result1.toString(), type);
//                        investmentPerformances.forEach((InvestmentPerformance inv) -> {
//                            inv.setBeneficiaryId(BeneficiaryId);
//                            inv.setInvestmentCode(InvestmentCode);
//                        });
//                        investrepository.saveInvestmentPerformance(investmentPerformances);
//                        type = new TypeToken<LinkedList<FmcaInvestmentMix>>() {
//                        }.getType();
//                        for (InvestmentHolding holding : investmentHoldings) {
//                            StringBuilder result3 = investmentsService.fmcaInvestmentMix(clientCredentials, holding.getInvestmentCode(), holding.getPortfolioCode());
//                            fmcaInvestmentMix = gson.fromJson(result3.toString(), type);
//                            fmcaInvestmentMix.forEach((FmcaInvestmentMix inv) -> {
//                                inv.setBeneficiaryId(BeneficiaryId);
//                                inv.setInvestmentCode(InvestmentCode);
//                            });
//                            investrepository.saveFmcaInvestmentMix((List<FmcaInvestmentMix>) fmcaInvestmentMix);
//                        }
//                    }
//                    fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(id, null, null);
//                }
//                if (beneficiary != null) {
//                    return "{"
//                            + "\"investmentHoldings\":" + investmentHoldings.toString()
//                            + ",\"investmentPerformances\":" + investmentPerformances.toString()
//                            + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
//                            + "}";
//                }
//            }
//        }
//        return null;
//    }    
//    @RequestMapping(value = {"/investments"}, method = {RequestMethod.GET})
//    @ResponseBody
//    public String investmentsDB(@RequestParam(value = "id", required = false) String beneficiary_id) {
//        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            if (beneficiary_id == null || beneficiary_id != null && beneficiary_id.isEmpty()) {
//                beneficiary_id = user.getUser_id();
//            }
//            if (beneficiary_id != null) {
//                List<Investment> investments = investrepository.getInvestments(beneficiary_id);
//                if (investments.isEmpty()) {
//                    final String BeneficiaryId = beneficiary_id;
//                    String clientCredentials = oAuth2TokenService.getClientCredentials();
//                    StringBuilder result0 = beneficiariesService.beneficiaryById(clientCredentials, BeneficiaryId);
//                    Beneficiary beneficiary = gson.fromJson(result0.toString(), Beneficiary.class);
////                    beneficiaryrepository.saveBeneficiary(beneficiary);
//                    investments = beneficiary.getInvestments();
//                    investments.forEach((Investment inv) -> {
//                        inv.setBeneficiaryId(BeneficiaryId);
//                    });
//                    investrepository.saveInvestment(investments);
//                }
//                return "{"
//                        + "\"investments\":" + investments.toString()
//                        + "}";
//            }
//        }
//        return "{}";
//    }
}
