/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import com.google.gson.Gson;
//import crm.nz.beans.acc.api.AegisBean;

public class TestMintNZAPI {

    public static void main(String args[]) {
//        GetAegisTokenService aegisTokenService = new GetAegisTokenService();
//        String clientCredentials = aegisTokenService.getClientCredentials();
//        Gson gson = new Gson();
//        AegisBean bean = gson.fromJson(clientCredentials, AegisBean.class);
//        AegisService aegisService = new AegisService();
//        System.out.println("\n\nIndividual Client");
//        aegisService.ClientDetails(bean, "990012702");
//        System.out.println("\n\nCurrency Exchange");
//        aegisService.CurrencyExchange(bean);
//        System.out.println("\n\nAssociated Parties");
//        aegisService.AssociatedParties(bean, "990012702");
//        System.out.println("\n\nPortfolio Valuation");
//        aegisService.PortfolioValuation(bean, "990012702");
//        System.out.println("\n\nPerformance Money Weighted");
//        aegisService.PerformanceMoneyWeighted(bean, "990012702");
//        System.out.println("\n\nAsset Holdings");
//        aegisService.AssetHoldings(bean, "990012702");
//        System.out.println("\n\nCash Holdings");
//        aegisService.CashHoldings(bean, "990012702");
//        System.out.println("\n\nHoldings");
//        aegisService.Holdings(bean, "990012702");
//        System.out.println("\n\nCash Transactions");
//        aegisService.CashTransactions(bean, "990012702");
//        System.out.println("\n\nCash And Income Transactions");
//        aegisService.CashAndIncomeTransactions(bean, "990012702");
//        System.out.println("\n\nCash And Asset Transactions");
//        aegisService.CashAndAssetTransactions(bean, "990012702");
//        System.out.println("\n\nAsset Transactions");
//        aegisService.AssetTransactions(bean, "990012702");
//        System.out.println("\n\nIncome Transactions");
//        aegisService.IncomeTransactions(bean, "990012702");
//        System.out.println("\n\nAsset Maturities");
//        aegisService.AssetMaturities(bean, "990012702");
//        System.out.println("\n\nBank Accounts");
//        aegisService.BankAccounts(bean, "990012702");
//        System.out.println("\n\nFee With Service Level");
//        aegisService.FeeWithServiceLevel(bean, "990012702");
//        System.out.println("\n\nService Level");
//        aegisService.ServiceLevel(bean, "990012702");
//        System.out.println("\n\nReport List");
//        aegisService.ReportList(bean, "990012702");
//        System.out.println("\n\nPDF Report Generation");
//        aegisService.PDFReportGeneration(bean, "990012702", "B3A8883A-CC0F-40E1-8B28-80E78A88A2F5");
//        System.out.println("\n\nAsset For Trading");
//        aegisService.AssetForTrading(bean, "990012702");
//        System.out.println("\n\nNon Auto Reweight Default Investment Options");
//        aegisService.NonAutoReweightDefaultInvestmentOptions(bean, "990012702");
//        System.out.println("\n\nClient Portal Users");
//        aegisService.ClientPortalUsers(bean, "990012702");
//        System.out.println("\n\nUpcoming Standing Orders");
//        aegisService.UpcomingStandingOrders(bean, "990012702");
//        System.out.println("\n\nAssociated Party Country Details");
//        aegisService.AssociatedPartyCountryDetails(bean, "990012702");
//        System.out.println("\n\nInvestment Strategy");
//        aegisService.InvestmentStrategy(bean, "990012702");
//        System.out.println("\n\nDims Fee Schedule");
//        aegisService.DimsFeeSchedule(bean, "990012702");
//        System.out.println("\n\nDims Fee Schedule MER");
//        aegisService.DimsFeeScheduleMER(bean, "990012702");
//        System.out.println("\n\nDims Cash Transactions");
//        aegisService.DimsCashTransactions(bean, "990012702");
        System.out.println("\n\nFixed Interest Portfolio Report");
//        aegisService.FixedInterestPortfolioReport(bean, "580000060");
//        System.out.println("\n\nDims Portfolio Income Report");
//        aegisService.DimsPortfolioIncomeReport(bean, "990012702");
    }
//        System.out.println("\n\nJoint Client");
//        aegisService.ClientDetails(bean, "600000090");
//        System.out.println("\n\nPartnership Client");
//        aegisService.ClientDetails(bean, "990038988");
//        System.out.println("\n\nTrust Client");
//        aegisService.ClientDetails(bean, "580000060");

    public static void main1(String args[]) {
//        DataZooService abc = new DataZooService();
//        String ver = abc.sessionToken();
//        AWSSecretsManagerConfig config = new AWSSecretsManagerConfig();
//        OAuth2TokenServiceBean oAuth2Token = config.getOAuth2Token1();
//        OAuth2TokenServiceBean oAuth2Token = config.getOAuth2Token2();
//        GetOAuth2TokenService oAuth2TokenService = new GetOAuth2TokenService(oAuth2Token);
//        String clientCredentials = oAuth2TokenService.getClientCredentials();
//        OnboardingsServices onboardingsServices = new OnboardingsServices();
//        onboardingsServices.currentApplications(clientCredentials);
//        StringBuilder createApplication = onboardingsServices.createApplication(clientCredentials);
//        System.out.println("createApplication" + createApplication);
//        onboardingsServices.currentApplicationById(clientCredentials, "16356");
//        onboardingsServices.deleteApplicationById(clientCredentials, "16327");
//        onboardingsServices.currentApplicationByRef(clientCredentials, "15067");
        String updateBeneficiayBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"Title\": \"Mr\",\n"
                + "\"FirstName\": \"Maninderjit\",\n"
                + "\"SecondName\": \"Ajeet\",\n"
                + "\"LastName\": \"Singh\",\n"
                + "\"MiddleName\": \"M\",\n"
                + "\"PreferredName\": \"Mr. Maninderjit\",\n"
                + "\"Gender\": \"Male\",\n"
                + "\"DateOfBirth\": \"1987-01-19\",\n"
                + "\"PlaceOfBirth\": \"NZ\",\n"
                + "\"Nation\": \"NZ\",\n"
                + "\"Occupation\": \"21\",\n"
                + "\"OccupationStatus\": \"Software Engineer\",\n"
                + "\"IRDNo\": \"049091850\",\n"
                + "\"IsUSTaxResident\": false,\n"
                + "\"PIRRate\": 0000,\n"
                + "\"ConsentToCheckId\": true,\n"
                + "\"Email\": \"maninderjit@iesl.co\",\n"
                + "\"SecondaryEmail\": \"manish@iesl.co\",\n"
                + "\"PreferredComm\": \"Email\",\n"
                + "\"TINNumber\": null,\n"
                + "\"TINNumber2\": null,\n"
                + "\"TINNumber3\": null,\n"
                + "\"TINCountry\": null,\n"
                + "\"TINCountry2\": null,\n"
                + "\"TINCountry3\": null,\n"
                + "\"CountryOfResidency\": \"NZ\",\n"
                + "\"ExternalReference\": \"INVB1001\",\n"
                + "\"UserDefined1\": null,\n"
                + "\"UserDefined2\": null,\n"
                + "\"UserDefined3\": null,\n"
                + "\"UserDefined4\": null,\n"
                + "\"UserDefined5\": null,\n"
                + "\"UserDefined6\": null,\n"
                + "\"UserDefined7\": null,\n"
                + "\"UserDefined8\": null,\n"
                + "\"UserDefined9\": null,\n"
                + "\"UserDefined10\":null\n"
                + "} ";
        System.out.println("updateBeneficiayBody---->" + updateBeneficiayBody);
//StringBuilder updateBeneficiary = onboardingsServices.updateBeneficiary(clientCredentials, "15068", "14193", updateBeneficiayBody);
        String createBeneficiayBody = "{\n"
                + "\"ApplicationId\": 17853,\n"
                + "\"Title\": \"Mr\",\n"
                + "\"FirstName\": \"Mamu\",\n"
                + "\"SecondName\": \"Mamu\",\n"
                + "\"LastName\": \"Khan\",\n"
                + "\"MiddleName\": \"S\",\n"
                + "\"PreferredName\": \"Mr. Mamu\",\n"
                + "\"Gender\": \"Male\",\n"
                + "\"DateOfBirth\": \"2007-01-19\",\n"
                + "\"PlaceOfBirth\": \"NZ\",\n"
                + "\"Nation\": \"NZ\",\n"
                + "\"Occupation\": \"21\",\n"
                + "\"OccupationStatus\": \"Software Engineer\",\n"
                + "\"IRDNo\": \"049091850\",\n"
                + "\"IsUSTaxResident\": false,\n"
                + "\"PIRRate\": 0000,\n"
                + "\"ConsentToCheckId\": true,\n"
                + "\"Email\": \"mamu@iesl.co\",\n"
                + "\"SecondaryEmail\":null,\n"
                + "\"PreferredComm\": \"Email\",\n"
                + "\"TINNumber\": null,\n"
                + "\"TINNumber2\": null,\n"
                + "\"TINNumber3\": null,\n"
                + "\"TINCountry\": null,\n"
                + "\"TINCountry2\": null,\n"
                + "\"TINCountry3\": null,\n"
                + "\"CountryOfResidency\": \"NZ\",\n"
                + "\"ExternalReference\": \"INVB1003\",\n"
                + "\"UserDefined1\": null,\n"
                + "\"UserDefined2\": null,\n"
                + "\"UserDefined3\": null,\n"
                + "\"UserDefined4\": null,\n"
                + "\"UserDefined5\": null,\n"
                + "\"UserDefined6\": null,\n"
                + "\"UserDefined7\": null,\n"
                + "\"UserDefined8\": null,\n"
                + "\"UserDefined9\": null,\n"
                + "\"UserDefined10\":null\n"
                + "}";
        System.out.println("createBeneficiayBody---->" + createBeneficiayBody);
        //StringBuilder createBeneficiay = onboardingsServices.createBeneficiary(clientCredentials, "15068", createBeneficiayBody);
        String createBeneficiaryAddressBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"AddressType\": \"Residential\",\n" // it can be [ Residential, Postal ] but not more than 2
                + "\"AddressLine1\": \"445 Mount Eden Road\",\n"
                + "\"AddressLine2\": \"Mount Eden\",\n"
                + "\"AddressLine3\": \"Auckland\",\n"
                + "\"AddressLine4\": null,\n"
                + "\"City\": \"Auckland\",\n"
                + "\"Region\": \"Auckland Region\",\n"
                + "\"State\": \"Auckland\",\n"
                + "\"Nation\": \"NZ\",\n"
                + "\"PostCode\": \"1024\",\n"
                + "\"IsPrimary\": true,\n"
                + "\"SameAsResidential\": true\n"
                + "}";
        System.out.println("createBeneficiaryAddressBody--->" + createBeneficiaryAddressBody);
//onboardingsServices.createBeneficiaryAddress(address, "15068", "14193", BeneficiaryAddressBody);
        String createBeneficiaryIdentificationBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"Nation\": \"NZ\",\n"
                + "\"Number\": \"AH669158\",\n"
                + "\"IssueDate\": \"2011-06-28\",\n"
                + "\"ExpiryDate\": \"2021-06-28\",\n"
                + "\"IdentificationType\": \"DriversLicence\",\n"// it can be [ Passport, DriversLicence, BirthCertificate, Citizenship, CertifiedDocs ]
                + "\"Version\": \"2\",\n"
                + "\"IsPrimary\": false\n"
                + "}";
        System.out.println("createBeneficiaryIdentificationBody--->" + createBeneficiaryIdentificationBody);
//onboardingsServices.createBeneficiaryIdentification(address, "15068", "14193", BeneficiaryAddressBody);
        String createBeneficiaryPhoneBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"Nation\": \"NZ\",\n"
                + "\"PhoneType\": \"Home\",\n"
                + "\"IsPrimary\": true,\n"
                + "\"PhoneNumber\": \"21675099\"\n"
                + "}";
        System.out.println("createBeneficiaryPhoneBody--->" + createBeneficiaryPhoneBody);
//onboardingsServices.createBeneficiaryPhone(address, "15068", "14193", BeneficiaryPhoneBody);        
        String createInvestmentBody = "{\n"
                + "  \"ApplicationId\": 17845,\n"
                + "  \"BeneficiaryId\": 18197,\n"
                + "  \"InvestmentName\": \"Maninderjit Singh Investment\",\n"
                + "  \"InvestmentType\": \"Unit_Registry\",\n"
                + "  \"InvestmentFunds\": [\n"
                + "    {\n"
                + "      \"ApplicationId\": 17845,\n"
                + "      \"BeneficiaryId\": 18197,\n"
                + "      \"FundCode\": \"290006\",\n"
                + "  	  \"FundName\": \"Diversified Income Fund\",\n"
                + "      \"Amount\": 10000,\n"
                + "      \"Percentage\": 100,\n"
                + "      \"ReinvestPercentage\": 0\n"
                + "    }\n"
                + "  ],\n"
                + "  \"InvestmentStrategy\": \"Mint Diversified Income Fund Investment Strategy\",\n"
                + "  \"InvestmentStrategyBand\": 0,\n"
                + "  \"ExternalReference\": \"INVI1001\",\n"
                + "  \"ReinvestPercentage\": 0,\n"
                + "  \"AdvisorCode\": \"BR0001\",\n"
                + "  \"AdvisorRate\": 1,\n"
                + "  \"AmountToInvest\": 10000,\n"
                + "  \"InvestmentContribution\": \"InvestmentContribution\",\n"
                + "  \"FeeGroup\": \"FeeGroup\",\n"
                + "  \"UserDefined1\": \"UserDefined1\",\n"
                + "  \"UserDefined2\": null,\n"
                + "  \"UserDefined3\": null,\n"
                + "  \"UserDefined4\": null,\n"
                + "  \"UserDefined5\": null,\n"
                + "  \"UserDefined6\": null,\n"
                + "  \"UserDefined7\": null,\n"
                + "  \"UserDefined8\": null,\n"
                + "  \"UserDefined9\": null,\n"
                + "  \"UserDefined10\": null\n"
                + "}";
        System.out.println("createInvestmentBody--->" + createInvestmentBody);
//onboardingsServices.createNewInvestment(clientCredentials, "17845", InvestmentBody);
        String updateInvestmentBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"InvestmentId\": 20158,\n"
                + "\"InvestmentName\": \"Maninderjit Singh Investment\",\n"
                + "\"InvestmentType\": \"Unit_Registry\",\n"
                + "\"InvestmentFunds\": [\n"
                + "    {\n"
                + "        \"ApplicationId\": 17845,\n"
                + "        \"BeneficiaryId\": 18197,\n"
                + "        \"InvestmentId\": 20158,\n"
                + "        \"InvestmentFundId\": 22644,\n"
                + "        \"FundCode\": \"290006\",\n"
                + "        \"FundName\": \"MMC Diversied Income Fund\",\n"
                + "        \"Amount\": 10000.0000,\n"
                + "        \"Percentage\": 50.00,\n"
                + "        \"ReinvestPercentage\": 0.00\n"
                + "    },\n"
                + "    {\n"
                + "        \"ApplicationId\": 17845,\n"
                + "        \"BeneficiaryId\": 18197,\n"
                + "        \"InvestmentId\": 20158,\n"
                + "        \"InvestmentFundId\": 22645,\n"
                + "        \"FundCode\": \"290002\",\n"
                + "        \"FundName\": \"MMC Australasian Equity Fund\",\n"
                + "        \"Amount\": 10000.0000,\n"
                + "        \"Percentage\": 50.00,\n"
                + "        \"ReinvestPercentage\": 0.00\n"
                + "    }\n"
                + "],\n"
                + "\"InvestmentStrategy\": null,\n"
                + "\"InvestmentStrategyBand\": 0,\n"
                + "\"ExternalReference\": \"INVI1001\",\n"
                + "\"ReinvestPercentage\": 0.0000,\n"
                + "\"AdvisorCode\": \"BR0001\",\n"
                + "\"AdvisorRate\": 1.0000,\n"
                + "\"AmountToInvest\": 10000.00,\n"
                + "\"InvestmentContribution\": null,\n"
                + "\"FeeGroup\": \"FeeGroup\",\n"
                + "\"UserDefined1\": \"UserDefined1\",\n"
                + "\"UserDefined2\": null,\n"
                + "\"UserDefined3\": null,\n"
                + "\"UserDefined4\": null,\n"
                + "\"UserDefined5\": null,\n"
                + "\"UserDefined6\": null,\n"
                + "\"UserDefined7\": null,\n"
                + "\"UserDefined8\": null,\n"
                + "\"UserDefined9\": null,\n"
                + "\"UserDefined10\": null\n"
                + "}";
        System.out.println("updateInvestmentBody--->" + updateInvestmentBody);
//https://mintnztestapi.mmcnz.co.nz/onboarding/applications/17845/investments/20158
        String createInvestmentFundsBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"InvestmentId\": 20158,\n"
                + "\"FundCode\": \"290002\",\n"
                + "\"FundName\": \"MMC Australasian Equity Fund \",\n"
                + "\"Amount\": 10000,\n"
                + "\"Percentage\": 100,\n"
                + "\"ReinvestPercentage\": 0\n"
                + "}";
        System.out.println("createInvestmentFundsBody--->" + createInvestmentFundsBody);
//onboardingsServices.createInvestmentFunds(clientCredentials,  17845,  20158, InvestmentFundsBody);
        String updateInvestmentFundsBody = "	{\n"
                + "      \"ApplicationId\": 17845,\n"
                + "      \"BeneficiaryId\": 18197,\n"
                + "      \"InvestmentId\": 20158,\n"
                + "      \"InvestmentFundId\":22644,\n"
                + "      \"FundCode\": \"290006\",\n"
                + "  	  \"FundName\": \"MMC Diversied Income Fund\",\n"
                + "      \"Amount\": 10000,\n"
                + "      \"Percentage\": 50,\n"
                + "      \"ReinvestPercentage\": 0\n"
                + "    }";
        System.out.println("updateInvestmentFundsBody--->" + updateInvestmentFundsBody);
        //        https://mintnztestapi.mmcnz.co.nz/onboarding/applications/17845/investments/20158/investmentFunds/22644
        String createBeneficiayBankAccountBody = "{\n"
                + "\"ApplicationId\": 17845,\n"
                + "\"BeneficiaryId\": 18197,\n"
                + "\"BankCode\": 1,\n"
                + "\"BranchCode\": 108,\n"
                + "\"Account\": 3924123,\n"
                + "\"Suffix\": 0,\n"
                + "\"BankAccountName\": \"Abhy Singla\",\n"
                + "\"BankAccountCurrency\": \"NZD\",\n"
                + "\"IsPrimary\": true\n"
                + "}";
        System.out.println("createBeneficiayBankAccountBody--->" + createBeneficiayBankAccountBody);
//onboardingsServices.createBeneficiaryBankAccount(clientCredentials, "15067", createBeneficiayBankAccountBody);
        String createDirectDebitBody = "{\n"
                + "  \"ApplicationId\": 17847,\n"
                + "  \"BeneficiaryId\": 18199,\n"
                + "  \"InvestmentId\": 20160,\n"
                + "  \"BankAccountId\": 11518,\n"
                + "  \"Frequency\": \"Weekly\",\n"
                + "  \"Amount\": 20000,\n"
                + "  \"Comments\": \"Comments\",\n"
                + "  \"IsActive\": true,\n"
                + "  \"StartDate\": \"2019-11-09\",\n"
                + "  \"EndDate\": \"2029-11-09\",\n"
                + "  \"Type\": \"DD\"\n"
                + "}";
        System.out.println("createDirectDebitBody--->" + createDirectDebitBody);
//        onboardingsServices.createInvestmentDirectDebits(bearerToken, applicationId,investmentId,bankAccountId, DirectDebitBody);
        String createBeneficiayRelationshipBody = "{\n"
                + "    \"ApplicationId\": 17853,\n"
                + "    \"BeneficiaryId\": 18208,\n"
                + "    \"SecondaryBeneficiaryId\": 18209,\n"
                + "    \"RelationshipType\": \"Parent\"\n"
                + "}";
        System.out.println("createBeneficiayRelationshipBody--->" + createBeneficiayRelationshipBody);
//        onboardingsServices.createBeneficiaryRelationship(clientCredentials, "15067", createBeneficiayRelationshipBody);

    }

//public static void insertBeneficiaryList(List<Beneficiary> beneficiaryList) {
//Connection con = null;
//String url = "jdbc:mysql://invsta-staging.cdiah0tmq8hx.ap-southeast-2.rds.amazonaws.com/groot",
//username = "admin", password = "11$O7PDjAcsstm$";
//BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
//try {
//con = DriverManager.getConnection(url, username, password);
//String sql = " INSERT INTO `login_master` (`username`,`password`, `name`, `active`, `created_ts`)\n"
//+ "VALUES (?, ?, ?,'Y', now()) ";
//PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//for (Beneficiary beneficiary : beneficiaryList) {
//preparedStatement.setString(1, beneficiary.getIrdNumber());
//String irdNumber = beneficiary.getIrdNumber();
//preparedStatement.setString(2, encoder.encode(irdNumber));
//preparedStatement.setString(3, beneficiary.getName());
//preparedStatement.setLong(4, beneficiary.getId());
//preparedStatement.addBatch();
//}
//preparedStatement.executeUpdate();
//ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
//if (generatedKeys.next()) {
//String user_id = generatedKeys.getString(1);
//String sql1 = " INSERT INTO `user_beneficiairy_relationship`\n"
//+ "(`user_name`,`beneficiairy_id`,`user_id`)\n"
//+ "VALUES (?,?,?);";
//PreparedStatement preparedStatement1 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
//for (Beneficiary beneficiary : beneficiaryList) {
//preparedStatement1.setString(1, beneficiary.getName());
//preparedStatement1.setLong(2, beneficiary.getId());
//preparedStatement1.setString(3, user_id);
//preparedStatement1.addBatch();
//}
//
//}
//} catch (SQLException ex) {
//ex.printStackTrace();
//} finally {
//if (con != null) {
//try {
//con.close();
//} catch (SQLException ex) {
//Logger.getLogger(TestMintNZAPI.class.getName()).log(Level.SEVERE, null, ex);
//}
//}
//}
//}
//AddressId = 17310 for 18197
//BankId = 11515 for 18197
//MessageService messageService = new MessageService();
//messageService.getAllMessages(clientCredentials);
//AdvisorService advisorService = new AdvisorService();
//advisorService.advisor(clientCredentials);
//BeneficiariesService beneficiariesService = new BeneficiariesService();
//boolean resultEqualsTo = true;
//Integer page = 1, clientSize = 200;//max Client Size
//Type type = new TypeToken<LinkedList<Beneficiary>>() {
//}.getType();
//List<Beneficiary> beneficiaryList = new LinkedList<>();
//Gson gson = new Gson();
//while (resultEqualsTo) {
//StringBuilder beneficiaries = beneficiariesService.beneficiaries(clientCredentials, page, clientSize);
//try {
//LinkedList<Beneficiary> list = gson.fromJson(beneficiaries.toString(), type);
//System.out.println(page + " @ pageIndex List.size" + list.size());
//beneficiaryList.addAll(list);
//if (clientSize != list.size()) {
//resultEqualsTo = false;
//}
//} catch (JsonSyntaxException ex) {
//ex.printStackTrace();
//}
//page++;
//}
//for (Beneficiary beneficiary : beneficiaryList) {
//beneficiariesService.beneficiaryById(clientCredentials, "4721");
//beneficiariesService.beneficiaryById(clientCredentials, beneficiary.getBeneficiaryId());
//}
//insertBeneficiaryList(beneficiaryList);
//beneficiariesService.beneficiaryByCustomerNumber(clientCredentials, "MIN626");
//PortfoliosService portfoliosService = new PortfoliosService();
//portfoliosService.allPortfolios(clientCredentials);
//portfoliosService.portfolio(clientCredentials, "290002");
//portfoliosService.portfolioSectorValuations(clientCredentials, "290004");
//portfoliosService.portfolioTopAssets(clientCredentials, "290004");
//portfoliosService.portfolioAssets(clientCredentials, "290004");
//PricePortfoliosService pricePortfoliosService = new PricePortfoliosService();
//pricePortfoliosService.pricePortfolioLatest(clientCredentials, "290002");
//pricePortfoliosService.pricePortfolioLatest(clientCredentials, "290002", "2019-06-25");
//pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, "290003");
//TransactionsService transactionsService = new TransactionsService();
//transactionsService.investmentTransactionsSummary(clientCredentials, "MIN793");
//transactionsService.investmentTransactions(clientCredentials, "MIN793");
//DocumentsService documentsService = new DocumentsService();
//documentsService.listOfDocuments(clientCredentials);
//InvestmentsService investmentsService = new InvestmentsService();
//investmentsService.investment(clientCredentials, "MIN793");//already
//investmentsService.investmentAssets(clientCredentials, "MIN793", "290004");// wrong 
//investmentsService.investmentTopAssets(clientCredentials, "MIN793", "290004");// wrong
//investmentsService.investments(clientCredentials);// bad request exception from api server
//investmentsService.investmentHoldings(clientCredentials, "MIN793");
//investmentsService.investmentHoldingSummaryValues(clientCredentials, "MIN793");//we need request for this investment holding summary
//investmentsService.investmentHoldingTransactionSummary(clientCredentials, "MIN793");
//investmentsService.investmentHoldingTransactionSummaryByPortfolio(clientCredentials, "MIN793", "290002");
//investmentsService.investmentSummary(clientCredentials, "MIN793");//it is very good investmnet summary with portfolio break down
//investmentsService.investmentPerformance(clientCredentials, "MIN793");
//investmentsService.kiwisaverReasonTotals(clientCredentials, "MIN793");
//investmentsService.marketvaluetimeseries(clientCredentials);// bad request exception from api server
//investmentsService.fmcaInvestmentMix(clientCredentials, "MIN793");
//investmentsService.fmcaInvestmentMix(clientCredentials, "MIN793", "290004");
//investmentsService.fmcaTopAssets(clientCredentials, "MIN793");
//investmentsService.fmcaTopAssets(clientCredentials, "MIN793", "290002");
//investmentsService.investment(clientCredentials, "MIN876");//already
//AllConnectionService.credentialsProvider();
//AllConnectionService.getSecret();
//AllConnectionService allConnectionService = new AllConnectionService();
//investmentsService.fmcaTopAssets(clientCredentials, "MIN793");
}
