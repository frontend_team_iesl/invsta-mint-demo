/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.authorization;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.services.rekognition.model.InvalidParameterException;
import com.amazonaws.services.rekognition.model.ResourceNotFoundException;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import crm.nz.services.HttpService;
import java.util.Base64;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class AllConnectionService extends HttpService {

//    private final AWSCredentialsProvider credentialsProvider = new StaticCredentialsProvider(basicAWSCredentials);

    public static AWSCredentialsProvider credentialsProvider() {
        String awsAccessKey = "AKIAI5GUSB5HL2LJPIVA";
        String awsSecretKey = "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";
        BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        AWSCredentialsProvider credentialsProvider = new StaticCredentialsProvider(basicAWSCredentials);
        return credentialsProvider;
    }

    public static String secretName = "AllTestingSchemas";
    public static String region = "ap-southeast-2";

    public static AWSSecretsManager createClient() {
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setConnectionTimeout(30000);
        clientConfig.setRequestTimeout(60000);
        clientConfig.setProtocol(Protocol.HTTPS);
        AWSCredentialsProvider credentialsProvider = credentialsProvider();
        return AWSSecretsManagerClientBuilder
                .standard()
                .withRegion(region)
                .build();
    }
    static AWSSecretsManager client = createClient();

    public static void getSecret() {

        String secretName = "AllTestingSchemas";
        String region = "ap-southeast-2";

        // Create a Secrets Manager client
//        AWSSecretsManager client = AWSSecretsManagerClientBuilder
//                .standard()
//                .withRegion(region)
//                .build();
        // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        // We rethrow the exception by default.
        String secret, decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException | InternalServiceErrorException | InvalidParameterException | InvalidRequestException | ResourceNotFoundException e) {

            throw e;
        }

        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
            System.out.println("secret--" + secret);
        } else {
            decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
            System.out.println("decodedBinarySecret" + decodedBinarySecret);

        }

    }

}
