/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.constants;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

/**
 *
 * @author Administrator
 */
@Component
public class Constants {
    
    public static String CODE_NEW_LINE = " ";
    public static String url;
    public static String username;
    public static String password;
    public static String driver;

    @Autowired
    public void setUrl(String url) {
        Constants.url = url;
        System.out.println("Constants.url---->" + Constants.url);
    }

    @Autowired
    public void setUsername(String username) {
        Constants.username = username;
        System.out.println("Constants.username---->" + Constants.username);
    }

    @Autowired
    public void setPassword(String password) {
        Constants.password = password;
        System.out.println("Constants.password---->" + Constants.password);
    }

    @Autowired
    public void setDriver(String driver) {
        Constants.driver = driver;
        System.out.println("Constants.driver---->" + Constants.driver);
    }

    public static String depositParticulars = "Deposit-(Purchased Investments)";
    public static String withdrawlParticulars = "Withdrawal-(Sold Investments)";

    public static String purchasedParticulars = "Client Portfolio Buy";
    public static String soldParticulars = "Client Portfolio Sell";

    public static BigDecimal HUNDRED = new BigDecimal(100);
    public static BigDecimal BD365 = new BigDecimal(365);
    public static Integer PERCENTANGE_DECIMAL_PLACES = new Integer(8);//don't modify it
    public static Integer MAXIMUM_DECIMAL_PLACES = new Integer(16);//don't modify it
    public static Integer DISPLAY_DECIMAL_PLACES = new Integer(2);//don't modify it
    public static String ENV_VAL = "DEV";
    public static String ENV_DEV = "DEV";
    public static String ENV_PROD = "PROD";
    
    public static GrantedAuthority ManagerRole = new SimpleGrantedAuthority("ROLE_MANAGER");
    public static GrantedAuthority HrRole = new SimpleGrantedAuthority("ROLE_HR");
    public static GrantedAuthority EmployeeRole = new SimpleGrantedAuthority("ROLE_EMPLOYEE");
    public static GrantedAuthority DirectorRole = new SimpleGrantedAuthority("ROLE_DIRECTOR");

    public static String blockedName = "ERROR BLOCKED PAGE";
    public static String blockedMessage = "<h3 style=\"color:#011c53; font-size: 25px\">ERROR BLOCKED PAGE</h3></br>"
            + "<p>THE ACCESS TO THIS PAGE/ EVENT HAS BEEN BLOCKED BY THE SUPER ADMIN.</p></br>";
//            + "<p>KINDLY CONTACT THE  SUPER ADMIN ON THE EMAIL ADDRESS GIVEN BELOW FOR ANY QUERIES.</p></br>";
    public static LinkedHashMap<String, ModelMap> userModelMap = new LinkedHashMap<>();
    public static LinkedHashMap<String, ModelMap> adminModelMap = new LinkedHashMap<>();
//    backgroundImage, logo, textColor, primaryButtonColor, menuColor, profileColor, backgroundColor;

    public static GrantedAuthority ManagerRole() {
        return ManagerRole;
    }

    public static GrantedAuthority HrRole() {
        return HrRole;
    }

    public static GrantedAuthority EmployeeRole() {
        return EmployeeRole;
    }

    public static GrantedAuthority DirectorRole() {
        return DirectorRole;
    }

    public static void userModelMapEntry(String key, ModelMap modelMap) {
        modelMap.addAttribute("key", key);
        userModelMap.put(key, modelMap);
    }

    public static ModelMap userModelMap(String key) {
        return userModelMap.get(key);
    }

    public static HashMap<String, Object> userMap(String key) {
        ModelMap modelMap = userModelMap.get(key);
        if (modelMap != null && !modelMap.isEmpty()) {
            HashMap<String, Object> userMap = new HashMap<>();
            Set<Map.Entry<String, Object>> entrySet = modelMap.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Object value = entry.getValue();
                if (value != null && !org.springframework.validation.BeanPropertyBindingResult.class.isInstance(value)) {
                    userMap.put(entry.getKey(), value);
                }
            }
            return userMap;
        }
        return null;
    }

    public static void adminModelMapEntry(String key, ModelMap modelMap) {
        adminModelMap.put(key, modelMap);
    }

    public static ModelMap adminModelMap(String key) {
        return adminModelMap.get(key);
    }

    public static HashMap<String, Object> adminMap(String key) {
        ModelMap modelMap = adminModelMap.get(key);
        if (modelMap != null && !modelMap.isEmpty()) {
            HashMap<String, Object> adminMap = new HashMap<>();
            Set<Map.Entry<String, Object>> entrySet = modelMap.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Object value = entry.getValue();
                if (value != null && !org.springframework.validation.BeanPropertyBindingResult.class.isInstance(value)) {
                    adminMap.put(entry.getKey(), value);
                }
            }
            return adminMap;
        }
        return null;
    }

}
