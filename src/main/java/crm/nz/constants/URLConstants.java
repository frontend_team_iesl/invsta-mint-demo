/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.constants;

/**
 *
 * @author ADMIN
 */
public abstract interface URLConstants {

    String STAGE = "https://3ntd6jykm6.execute-api.ap-southeast-2.amazonaws.com/prod";
    String TASK_REPO_URL = STAGE.concat("/tasks");   //tasks repository stage
    String FETCH_TASKS_URL = TASK_REPO_URL.concat("/fetch");
    String NEW_TASK_URL = TASK_REPO_URL.concat("/new");

    String CONTACTS_REPO_URL = STAGE.concat("/contacts");  //contacts repository stage
    String NEW_PERSON_URL = CONTACTS_REPO_URL + "/new-person";
    String NEW_COMPANY_URL = CONTACTS_REPO_URL + "/new-company";
    String NEW_TRUST_URL = CONTACTS_REPO_URL + "/new-trust";
    String FETCH_CONTACTS_URL = CONTACTS_REPO_URL + "/fetch";
    String FETCH_PERSONS_URL = CONTACTS_REPO_URL + "/fetch/persons";
    String FETCH_COMPANIES_URL = CONTACTS_REPO_URL + "/fetch/companies";
    String FETCH_TRUSTS_URL = CONTACTS_REPO_URL + "/fetch/trusts";

    String EVENTS_REPO_URL = STAGE.concat("/events");    //events repository stage
    String FETCH_EVENTS_URL = EVENTS_REPO_URL.concat("/fetch");
    String NEW_EVENT_URL = EVENTS_REPO_URL.concat("/new");

    String OPPORTUNITIES_REPO_URL = STAGE.concat("/opps");    //Opportunity repository stage
    String FETCH_OPPORTUNITY_URL = OPPORTUNITIES_REPO_URL.concat("/fetch");
    String NEW_OPPORTUNITY_URL = OPPORTUNITIES_REPO_URL.concat("/new");

    String ACTIVITIES_REPO_URL = STAGE.concat("/activities");    //Activities repository stage
    String FETCH_ACTIVITIES_URL = ACTIVITIES_REPO_URL.concat("/fetch");

    String NOTES_REPO_URL = STAGE.concat("/notes");    //Notes repository stage
    String FETCH_NOTES_URL = NOTES_REPO_URL.concat("/fetch");
    String NEW_NOTES_URL = NOTES_REPO_URL.concat("/new");

}
