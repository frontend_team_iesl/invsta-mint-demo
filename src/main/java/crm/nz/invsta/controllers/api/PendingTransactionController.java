/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.components.CommonMethods;
import crm.nz.mail.SendMail;
import crm.nz.repository.PendingTransactionRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author TOSHIBA R830
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api/admin")
public class PendingTransactionController implements CurrentUserService {

    @Autowired
    private PendingTransactionRepository repository;
    @Autowired
    private CommonMethods common;
    @Autowired
    private SendMail sendmail;
    @Autowired
    private PortfolioRepository portRepository;

    @RequestMapping(value = {"/pending-transaction"}, method = {RequestMethod.POST})
    @ResponseBody
    public String pendingInvestment(ModelMap modelMap, @RequestBody PendingTransactionBean bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            bean.setUserId(user.getUser_id());
            System.out.println(bean);
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(bean.getPortfolioCode());
            bean.setPortfolioName(portfolioDetail.getFundName());
            bean.setCreated_ts(common.format2(common.today()));
            if (!"".equals(bean.getId()) && !bean.getId().isEmpty()) {
                repository.setUpdateTransaction(bean);
            } else {
               repository.setPendingTransaction(bean);
            }
           
            if (bean.getType().equals("WDW")) {
//                sendmail.sendSellFundsMail(user, bean);
            } else {
                if (!"PAYMENT_LATER".equalsIgnoreCase(bean.getStatus())) {
                    if ("REGULAR".equalsIgnoreCase(bean.getInvestmentType())  && "DEBIT".equalsIgnoreCase(bean.getPaymentMethod())) {
//                        sendmail.sendTransactionPaymentMail(bean, user);
                    } else  {
//                        sendmail.sendTransactionPaymentPayNowMail(bean, user);
                    }
                }
            }
            return "data";
        }

        return "error";
    }

    @RequestMapping(value = {"/get-pending-transaction"}, method = {RequestMethod.GET})
    @ResponseBody
    public String getPendingInvestment(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                System.out.println(user.getRole());
                List<PendingTransactionBean> pendingTransaction = repository.getPendingsSellTransaction();
                return pendingTransaction.toString();
            }
        }
        return "[]";
    }

}
