/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.authorization.PortfoliosService;
import crm.nz.authorization.PricePortfoliosService;
import crm.nz.authorization.TransactionsService;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.repository.InvestmentRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author IESL
 */
@Deprecated
@RestController
@RequestMapping(value = "/rest/3rd/party/api")
public class ThirdPartyAPIController implements CurrentUserService {

    @Autowired
    private BeneficiariesService beneficiariesService;
    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private InvestmentsService investmentsService;
    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private PortfoliosService portfoliosService;
    @Autowired
    private PricePortfoliosService pricePortfoliosService;
    @Autowired
    private  InvestmentRepository investmentRepository;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private Gson gson;
/**
 * 
 */
    @RequestMapping(value = {"/currentApplications"}, method = {RequestMethod.GET})
    @ResponseBody
    public String getAllMessages(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder currentApplications = onboardingsServices.currentApplications(clientCredentials);
            return "";
        }
        return "[]";
    }
    
    @RequestMapping(value = {"/beneficiaries"}, method = {RequestMethod.GET})
    @ResponseBody
    public String age(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder beneficiaries = beneficiariesService.beneficiaries(clientCredentials);
//            System.out.println("Beneficary of All User" + beneficiaries.toString());
//            return beneficiaries.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/beneficiary-{Id}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryById(ModelMap modelMap, @PathVariable(value = "Id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder beneficiary = beneficiariesService.beneficiaryById(clientCredentials, id);
//            System.out.println("Beneficary of User" + beneficiary.toString());
//            return beneficiary.toString();
        }
        return "{}";
    }

    @RequestMapping(value = {"/beneficiary-dashboard-{id}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryDashboard(ModelMap modelMap, @PathVariable(value = "id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder beneficiary = beneficiariesService.beneficiaryById(clientCredentials, id);
            Gson gson = new Gson();
//            HashMap beneficiaryMap = gson.fromJson(beneficiary.toString(), HashMap.class);
//            List Investments = (List) beneficiaryMap.get("Investments");
//            Map Investment = (Map) Investments.get(0);
//            String Code = (String) Investment.get("Code");
//            System.out.println("Code" + Code);
//            StringBuilder transactionSummary = investmentsService.investmentHoldingTransactionSummary(clientCredentials, Code);
//            StringBuilder investmentPerformance = investmentsService.investmentPerformance(clientCredentials, Code);
//            StringBuilder investmentSummary = investmentsService.investmentSummary(clientCredentials, Code);
//            StringBuilder investmentHoldings = investmentsService.investmentHoldings(clientCredentials, Code);
//            return "{"
//                    + "\"transactionSummary\":" + transactionSummary.toString()
//                    //                    + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
//                    + ",\"investmentPerformance\":" + investmentPerformance.toString()
//                    + ",\"investmentSummary\":" + investmentSummary.toString()
//                    + ",\"investmentHoldings\":" + investmentHoldings.toString()
//                    + "}";

        }
        return "{}";
    }

    @RequestMapping(value = {"/beneficiary-dashboard"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryDashboard(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            return beneficiaryDashboard(modelMap, "4721");
        }
        return "{}";
    }

    /**
     *
     */
    @RequestMapping(value = {"/beneficiary-dashboard-db"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryDashboardDB(ModelMap modelMap, @RequestParam(value = "id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = "4721";
            }
            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder result0 = beneficiariesService.beneficiaryById(clientCredentials, id);
//            Beneficiary beneficiary = gson.fromJson(result0.toString(), Beneficiary.class);
//            List<Investment> investments = beneficiary.getInvestments();
//            Investment inv = investments.get(0);
//            StringBuilder result2 = investmentsService.investmentHoldingSummaryValues(clientCredentials, inv.getCode());//we need request for this investment holding summary
//            Type type = new TypeToken<LinkedList<InvestmentHolding>>() {
//            }.getType();
//            LinkedList<InvestmentHolding> investmentHoldings = gson.fromJson(result2.toString(), type);
//            modelMap.addAttribute("investmentHoldings", investmentHoldings);
//
//            return "{"
//                    + "\"investmentHoldings\":" + investmentHoldings.toString()
//                    + "}";

        }
        return "{}";
    }

    @RequestMapping(value = {"/investemnt-summary"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentSummary(ModelMap modelMap,
            @RequestParam("ic") String investmentCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (investmentCode == null) {
                investmentCode = "MIN793";
            }
//            String clientCredentials = oAuth2TokenService.getClientCredentials();
////            StringBuilder investmentSummary = investmentsService.investmentSummary(clientCredentials, investmentCode);
////            return investmentSummary.toString();
        }
        return "{}";
    }

    @RequestMapping(value = {"/top-Holdings-{Id}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String TopHoldings(ModelMap modelMap, @PathVariable(value = "Id") String id) {
        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder portfolioTopAssets = portfoliosService.portfolioTopAssets(clientCredentials, id);
//            StringBuilder pricePortfolioUnitPrices = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, id);
//            return "{"
//                    + "\"portfolioTopAssets\":" + portfolioTopAssets.toString()
//                    + ",\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
//                    + "}";
//        }
        return "{}";
    }

    @RequestMapping(value = {"/portfolio-dashboard"}, method = {RequestMethod.GET})
    @ResponseBody
    public String portfolioDashboard(ModelMap modelMap,
            @RequestParam(value = "ic") String ic,
            @RequestParam(value = "pc") String pc
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            
            List<FmcaTopAssets> fmcaTopAssets = investmentRepository.getFmcaTopAssets(user.getUser_id(), null, ic, pc.equals("290007")?"290006":pc);
//            String clientCredentials = oAuth2TokenService.getClientCredentials2();
//            StringBuilder fmcaTopAssets = investmentsService.fmcaTopAssets(clientCredentials, ic, pc);
//            StringBuilder fmcaInvestmentMix = investmentsService.fmcaInvestmentMix(clientCredentials, ic, pc);
//            StringBuilder pricePortfolioUnitPrices = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, pc);
            return "{"
                    + "\"fmcaTopAssets\":" + fmcaTopAssets.toString()
                    //                    + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
//                    + ",\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/bene-transactions-{code}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String BeneTransactionHistory(ModelMap modelMap, @PathVariable(value = "code") String code) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials2();
            StringBuilder investmentTransactions = transactionsService.investmentTransactions(clientCredentials, code, 1);
            return "{"
                    + "\"investmentTransactions\":" + investmentTransactions.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/all-funds"}, method = {RequestMethod.GET})
    @ResponseBody
    public String Allfunds(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials();
//            StringBuilder allPortfolios = portfoliosService.allPortfolios(clientCredentials);
//            return allPortfolios.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/fund-details-{Id}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String FundDetails(ModelMap modelMap, @PathVariable(value = "Id") String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
//            String clientCredentials = oAuth2TokenService.getClientCredentials2();
//            StringBuilder pricePortfolioUnitPrices = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, id);
//            return "{"
//                    + "\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
//                    //                    + ",\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
//                    + "}";
        }
        return "{}";
    }

}
