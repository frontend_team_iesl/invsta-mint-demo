/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.components.CommonMethods;
import crm.nz.mail.SendMail;
import crm.nz.repository.CommonRepository;
import crm.nz.repository.PendingInvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author TOSHIBA R830
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api/admin")
public class PendingInvestmentController implements CurrentUserService {

    @Autowired
    private PendingInvestmentRepository repository;
    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private CommonMethods common;
    @Autowired
    private SendMail sendMail;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private CommonRepository commonRepository;

    @RequestMapping(value = {"/pending-investment"}, method = {RequestMethod.POST})
    @ResponseBody
    public String pendingInvestment(ModelMap modelMap, @RequestBody PendingInvestmentBean bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            bean.setUserId(user.getUser_id());
            bean.setCreated_ts(common.format2(common.today()));
            String clientCredentials = oAuth2TokenService.getClientCredentials();
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(bean.getPortfolioCode());
            bean.setPortfolioName(portfolioDetail.getFundName());
            String createBeneficiaryInvestmentBody = "{\n"
                    + "	\"ApplicationId\": " + bean.getApplicationId() + ",\n"
                    + "	\"BeneficiaryId\": " + bean.getBeneficiaryId() + ",\n"
                    + "	\"InvestmentName\": \"" + bean.getInvestmentName() + "\",\n"
                    + "	\"InvestmentType\": \"Unit_Registry\",\n"
                    + "	\"InvestmentFunds\": [\n"
                    + "    	{\n"
                    + "		\"ApplicationId\": " + bean.getApplicationId() + ",\n"
                    + "         \"BeneficiaryId\": " + bean.getBeneficiaryId() + ",\n"
                    + "    	\"FundCode\": \"" + bean.getPortfolioCode() + "\",\n"
                    + "    	\"FundName\": \"" + bean.getPortfolioName() + "\",\n"
                    + "    	\"Amount\": " + bean.getInvestedAmount() + "\n"
                    + "    	}"
                    + "    	\n"
                    + "	],\n"
                    + "	\"ReinvestPercentage\": 0,\n"
                    + "	\"AdvisorCode\": \"BR011\",\n"
                    + "	\"AmountToInvest\": " + bean.getInvestedAmount() + ",\n"
                    + "	\"InvestmentContribution\": \"" + bean.getInvestedAmount() + "\"\n"
                    + "}";
            System.out.println("--->" + createBeneficiaryInvestmentBody);
            if (bean.getApplicationId() != null && !bean.getApplicationId().isEmpty() && !"PAYMENT_LATER".equalsIgnoreCase(bean.getPaymentMethod())) {
                StringBuilder investmentId = onboardingsServices.createNewInvestment(clientCredentials, bean.getApplicationId(), createBeneficiaryInvestmentBody);
                if (!investmentId.toString().contains("Status Code: 404")) {
                    bean.setInvestmentId(investmentId.toString());
                }
            }

//            bean.setInvestmentId("1");
            String createDirectDebitBody = "{\n"
                    + "  \"ApplicationId\": " + bean.getApplicationId() + ",\n"
                    + "  \"BeneficiaryId\": " + bean.getBeneficiaryId() + ",\n"
                    + "  \"InvestmentId\":  " + bean.getInvestmentId() + ",\n"
                    + "  \"BankAccountId\": " + bean.getBankApiId() + ",\n"
                    + "  \"Frequency\": \"" + bean.getSelectfrequecy() + "\",\n"
                    + "  \"Amount\": " + bean.getInvestedAmount() + ",\n"
                    + "  \"Comments\": " + bean.getComments() + ",\n"
                    + "  \"IsActive\": true,\n"
                    + "  \"StartDate\": \"" + bean.getStartDate() + "\",\n"
                    + "  \"EndDate\": \"" + bean.getEndDate() + "\",\n"
                    + "  \"Type\": \"DD\"\n"
                    + "}";
            System.out.println("createDirectDebitBody-->" + createDirectDebitBody);
            StringBuilder directDebitId = null;
            if (bean.getApplicationId() != null && !bean.getApplicationId().isEmpty() && "REGULAR".equalsIgnoreCase(bean.getInvestmentType()) && "DEBIT".equalsIgnoreCase(bean.getPaymentMethod()) && bean.getBankAccountId()!= null && !bean.getBankAccountId().isEmpty()) {
                directDebitId = onboardingsServices.createInvestmentDirectDebits(clientCredentials, bean.getApplicationId(), bean.getInvestmentId(), bean.getBankAccountId(), createDirectDebitBody);
                System.out.println("directDebitId" + directDebitId);
                if (!directDebitId.toString().contains("Status Code: 404")) {
                    bean.setDirectDebitId(directDebitId.toString());
                }
            }
            if (!bean.getBankCode().equals("") && !bean.getBankCode().isEmpty()) {
                Map<String, BankCodeBean> bankNameByCode = common.bankNameByCode();
                BankCodeBean bank = bankNameByCode.get(bean.getBankCode().trim());
                bank.setBankName(bank.getBankName());
            }
            if (!"".equals(bean.getId()) && !bean.getId().isEmpty()) {
                repository.setUpdateInvestment(bean);
            } else {
                repository.setPendingInvestment(bean);
            }

            if (!"PAYMENT_LATER".equalsIgnoreCase(bean.getStatus())) {
                if ("REGULAR".equalsIgnoreCase(bean.getInvestmentType() )  && "DEBIT".equalsIgnoreCase(bean.getPaymentMethod())) {
//                    sendMail.sendInvestmentPaymentMail(bean, user);
                } else  {
//                    sendMail.sendInvestmentPaymentPayNowMail(bean, user);
                }

            }

            return "data";
        }

        return "error";
    }

    @RequestMapping(value = {"/get-pending-investment"}, method = {RequestMethod.GET})
    @ResponseBody
    public String pendingInvestment(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                List<PendingInvestmentBean> pendingInvestment = repository.getPendingInvestment();
                return pendingInvestment.toString();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                List<PendingInvestmentBean> pendingInvestment = repository.getAdvisorPendingInvestment(user.getUser_id());
                return pendingInvestment.toString();
            }
        }
        return "error";
    }

}
