/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.security.CurrentUserService;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author IESL
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class CheckCommunicationAPIController implements CurrentUserService {

    @RequestMapping(value = {"/check-communication"}, method = {RequestMethod.GET})
    @ResponseBody
    public long checkCommunication(HttpSession session) {
        System.out.println(session);
        return session.getLastAccessedTime();
    }
}
