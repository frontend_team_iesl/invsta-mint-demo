package crm.nz.invsta.controllers.api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import crm.nz.authorization.DataZooService;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.CountryISOCodeBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.Register;
import crm.nz.beans.acc.api.ResponseBean;
import crm.nz.components.CommonMethods;
import crm.nz.components.FileMethods;
import crm.nz.components.ObjectCastManager;
import crm.nz.components.RandomStringGenerator;
import crm.nz.mail.SendMail;
import crm.nz.repository.CommonRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.DataUploadService;
import crm.nz.services.ThirdAPIRegisterService;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class RegisterAPIController implements CurrentUserService {

    @Autowired
    private SendMail sendMail;
    @Autowired
    private RandomStringGenerator generator;
    @Autowired
    private ThirdAPIRegisterService registerService;
    @Autowired
    private FormDetailsRepository formDetailsRepository;
    @Autowired
    private DataZooService datazooservice;
    @Autowired
    CommonMethods common;
    @Autowired
    FileMethods fileMethods;
    @Autowired
    DataUploadService dataUploadService;
    @Autowired
    private CommonRepository commonRepository;

    Gson gson = new Gson();

    @RequestMapping(value = {"/get-pending-registration"}, method = RequestMethod.GET)
    @ResponseBody
    public String getPendingRegisterDetails(HttpServletRequest request, ModelMap model) throws IOException {
        SecuredUser user = getCurrentUser();
        if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails();
            return pendingRegisterDetails.toString();
        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails(user.getUser_id());
            return pendingRegisterDetails.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/get-pending-verification-{token}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getPendingRegisterVerificationDetails(HttpServletRequest request,
            ModelMap modelMap, @PathVariable(value = "token") String token) throws IOException {
        SecuredUser user = getCurrentUser();
        if ("ADMIN".equalsIgnoreCase(user.getRole())) {
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null) {
                return person.toString();
            } else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null) {
                    return joint.toString();
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null) {
                        return company.toString();
                    }
                }
            }

        } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
            List<Register> pendingRegisterDetails = formDetailsRepository.getPendingRegisterDetails(user.getUser_id());
            return pendingRegisterDetails.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/dl-verification"}, method = RequestMethod.POST)
    @ResponseBody
    public String verifyDlDetail(HttpServletRequest request, ModelMap model, @RequestBody PersonDetailsBean dlPerson) throws IOException {
        String sessiontokenObj = datazooservice.sessionToken();
        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
        String token = (String) sessionToken.get("sessionToken");
        String format3 = common.changeFormat(dlPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setDate_of_Birth(format3);
        String verifyNZTADL = datazooservice.verifyNZTADL(token, dlPerson).toString();
        HashMap<String, String> response = new HashMap<>();
        HashMap<String, Object> sessionToken1 = gson.fromJson(verifyNZTADL, HashMap.class);
        LinkedTreeMap<String, Object> sessionToken2 = (LinkedTreeMap) sessionToken1.get("driversLicence");
        Object verified = sessionToken2.get("verified");
        String verify = verified.toString();
        dlPerson.setInvestor_idverified(verify);
        if (verify.equalsIgnoreCase("true")) {
            formDetailsRepository.updateVerification(dlPerson, response);
        }
        System.out.println("verifyNZTADL--->" + verifyNZTADL);
        return verifyNZTADL;
    }

    @RequestMapping(value = {"/pp-verification"}, method = RequestMethod.POST)
    @ResponseBody
    public String verifyPPDetail(HttpServletRequest request, ModelMap model, @RequestBody PersonDetailsBean dlPerson) throws IOException {
        String sessiontokenObj = datazooservice.sessionToken();
        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
        String token = (String) sessionToken.get("sessionToken");
        String Dob = common.changeFormat(dlPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setDate_of_Birth(Dob);
        String exp = common.changeFormat(dlPerson.getPassport_expiry(), CommonMethods.format8, CommonMethods.format3);
        dlPerson.setPassport_expiry(exp);
        String verifyNZDIAPP = datazooservice.verifyNZDIAPP(token, dlPerson).toString();
        HashMap<String, String> response = new HashMap<>();
        HashMap<String, Object> sessionToken1 = gson.fromJson(verifyNZDIAPP, HashMap.class);
        LinkedTreeMap<String, Object> sessionToken2 = (LinkedTreeMap) sessionToken1.get("passport");
        Object verified = sessionToken2.get("verified");
        String toString = verified.toString();
        dlPerson.setInvestor_idverified(toString);
        if (toString.equalsIgnoreCase("true")) {
            formDetailsRepository.updateVerification(dlPerson, response);
        }
        System.out.println("verifyNZDIAPP--->" + verifyNZDIAPP);

        return verifyNZDIAPP;
    }

    @RequestMapping(value = {"/pep-verification"}, method = RequestMethod.POST)
    @ResponseBody
    public String verifyNZPEPDetail(HttpServletRequest request, ModelMap model, @RequestBody PersonDetailsBean pepPerson) throws IOException {
        String sessiontokenObj = datazooservice.sessionToken();
        String format3 = common.changeFormat(pepPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
        pepPerson.setDate_of_Birth(format3);
//        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
//        String token = (String) sessionToken.get("sessionToken");
////        String Dob = common.changeFormat(dlPerson.getDate_of_Birth(), CommonMethods.format8, CommonMethods.format3);
////        dlPerson.setDate_of_Birth(Dob);
////          String sessiontokenObj = datazooservice.sessionToken();
        HashMap<String, Object> sessionToken = gson.fromJson(sessiontokenObj, HashMap.class);
        String PPtoken = (String) sessionToken.get("sessionToken");
        String verifyNZPEP = datazooservice.verifyNZPEP(PPtoken, pepPerson).toString();
//        HashMap<String, Object> sessionToken1 = gson.fromJson(verifyNZPEP, HashMap.class);
//        ArrayList get = (ArrayList) sessionToken1.get("watchlistAML");
//        LinkedTreeMap<String, Object> get1 = (LinkedTreeMap) get.get(0);
//        Object get2 =  get1.get("verified");
//        String toString = get2.toString();
//        person.setPepStatus(toString);
        System.out.println("verifyNZDIAPP--->" + verifyNZPEP);

        return verifyNZPEP;
    }

    @RequestMapping(value = {"/person-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String savePersonDetail(HttpServletRequest request, ModelMap model,
            @RequestBody PersonDetailsBean person) throws IOException {
        String token = generator.generate(30);
        person.setTokan(token);

//        
        HashMap response = new HashMap<>();
        System.out.println("here");
        Map<String, CountryISOCodeBean> countryISOCodeMap = common.countryISOCode();
        if (person.getBankFile() != null && !person.getBankFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3BankDocByBase64(person.getId_type(), person.getBankFile(), person.getIrd_number());
            String bankFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("bankFileUrl" + bankFileUrl);
            person.setBankFile(bankFileUrl);
        }
        if (person.getOtherFile() != null && !person.getOtherFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3PicsByBase64(person.getId_type(), person.getOtherFile(), person.getIrd_number());
            String otherFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("otherFileUrl" + otherFileUrl);
            person.setOtherFile(otherFileUrl);
        }
        if (person.getCountry_residence() != null && !person.getCountry_residence().isEmpty()) {
            CountryISOCodeBean countryISOCode = countryISOCodeMap.get(person.getCountry_residence().trim());
            person.setCodeISO(countryISOCode.getCodeISO());
        }
        if (person.getTaxCountries() != null && person.getTaxCountries().size() > 0) {
            List<String> taxCountries = person.getTaxCountries();
            for (int i = 0; i < taxCountries.size(); i++) {
                if (taxCountries.get(i) != null && !taxCountries.get(i).isEmpty()) {
                    CountryISOCodeBean countryISOCode = countryISOCodeMap.get(taxCountries.get(i).trim());
                    String codeISO = countryISOCode.getCodeISO();
                    taxCountries.set(i, codeISO);
                }
            }
            person.setTaxCountries(taxCountries);
        }
        boolean licence = GenericValidator.isDate(person.getLicence_expiry_Date(), "yyyy-MM-dd", false);
        boolean passport = GenericValidator.isDate(person.getPassport_expiry(), "yyyy-MM-dd", false);
        if (licence) {
            if (person.getLicence_expiry_Date() != null && !person.getLicence_expiry_Date().isEmpty() && person.getLicence_issue_date() != null && !person.getLicence_issue_date().isEmpty()) {
                String licenceExpiry = common.changeFormat(person.getLicence_expiry_Date(), CommonMethods.format8, CommonMethods.format3);
                String issueDate = common.changeFormat(person.getLicence_issue_date(), CommonMethods.format8, CommonMethods.format3);
                person.setLicence_expiry_Date(licenceExpiry);
                person.setLicence_issue_date(issueDate);
            }
        }
        if (passport) {
            if (person.getPassport_expiry() != null && !person.getPassport_expiry().isEmpty() && person.getPassport_issue_date() != null && !person.getPassport_issue_date().isEmpty()) {
                String passportExpiry = common.changeFormat(person.getPassport_expiry(), CommonMethods.format8, CommonMethods.format3);
                String issueDate = common.changeFormat(person.getPassport_issue_date(), CommonMethods.format8, CommonMethods.format3);
                person.setPassport_expiry(passportExpiry);
                person.setPassport_issue_date(issueDate);
            }
        }

        if ("SUBMISSION".equalsIgnoreCase(person.getStatus())) {
//            person = registerService.createIndividual(person);
        }
        formDetailsRepository.saveFormDetails(person, response);
//        sendMail.sendVerifyNewPersonToEmail(person, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/company-trust-single-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveCompanyTrustDetail(HttpServletRequest request, ModelMap model,
            @RequestBody CompanyDetailBean company) throws IOException {
        CompanyDetailBean companyMaininv = formDetailsRepository.getCompanyMaininvByRegId(company.getReg_id());

//        String token = generator.generate(30);
//        company.setTokan(token);
        HashMap response = new HashMap<>();
        formDetailsRepository.saveSingleCompanyTrustDetails(company, response);
//        sendMail.sendSingleAccountRegistrationMail(company.getFname(), companyMaininv.getCompanyName(), companyMaininv.getFname(), requestedDomain(request), companyMaininv.getEmail(), company.getEmail());
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/company-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveSingleCompanyTrustDetail(HttpServletRequest request, ModelMap model,
            @RequestBody CompanyDetailBean company) throws IOException {
        String token = generator.generate(30);
        company.setToken(token);
        if (company.getMoreInvestorList() != null) {
            company.getMoreInvestorList().forEach((inv) -> {
                String generate = generator.generate(30);
                inv.setToken(generate);
            });
        }
        HashMap response = new HashMap<>();
        if (company.getBankFile() != null && !company.getBankFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3BankDocByBase64(company.getSrcOfFund(), company.getBankFile(), company.getIrdNumber());
            String bankFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("bankFileUrl" + bankFileUrl);
            company.setBankFile(bankFileUrl);
        }
        if (company.getOtherFile() != null && !company.getOtherFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3PicsByBase64(company.getSrcOfFund(), company.getOtherFile(), company.getIrdNumber());
            String otherFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("otherFileUrl" + otherFileUrl);
            company.setOtherFile(otherFileUrl);
        }
        if (company.getDeedFile() != null && !company.getDeedFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3PicsByBase64("DeedFile", company.getDeedFile(), company.getIrdNumber());
            String deedFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("deedFileUrl" + deedFileUrl);
            company.setDeedFile(deedFileUrl);
        }
        formDetailsRepository.saveCompanyDetails(company, response);
//        sendMail.sendVerifyNewCompanyToEmail(company, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/joint-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveJointDetail(HttpServletRequest request, ModelMap model,
            @RequestBody JointDetailBean joint) throws IOException {
        String token = generator.generate(30);
        joint.setToken(token);
        Map<String, CountryISOCodeBean> countryISOCodeMap = common.countryISOCode();
        if (joint.getBankFile() != null && !joint.getBankFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3BankDocByBase64(joint.getId_type(), joint.getBankFile(), joint.getIrd_Number());
            String bankFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("bankFileUrl" + bankFileUrl);
            joint.setBankFile(bankFileUrl);
        }
        if (joint.getOtherFile() != null && !joint.getOtherFile().isEmpty()) {
            S3Object s3Object = dataUploadService.storeTos3PicsByBase64(joint.getId_type(), joint.getOtherFile(), joint.getIrd_Number());
            String otherFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("otherFileUrl" + otherFileUrl);
            joint.setOtherFile(otherFileUrl);
        }
        if (joint.getMoreInvestorList() != null) {
            for (JointDetailBean inv : joint.getMoreInvestorList()) {
                String generate = generator.generate(30);
                inv.setToken(generate);
                if (inv.getCountry_residence() != null && !inv.getCountry_residence().isEmpty()) {
                    CountryISOCodeBean countryISOCode = countryISOCodeMap.get(inv.getCountry_residence().trim());
                    inv.setCodeISO(countryISOCode.getCodeISO());
                }
            }
        }
        if (joint.getCountry_residence() != null && !joint.getCountry_residence().isEmpty()) {
            CountryISOCodeBean countryISOCode = countryISOCodeMap.get(joint.getCountry_residence().trim());
            joint.setCodeISO(countryISOCode.getCodeISO());
        }
        HashMap response = new HashMap<>();
        if ("SUBMISSION".equalsIgnoreCase(joint.getStatus())) {
//            joint = registerService.createJoint(joint);
        }
        formDetailsRepository.saveJointDetails(joint, response);
//        sendMail.sendVerifyNewJointToEmail(joint, token, requestedDomain(request));
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

    @RequestMapping(value = {"/joint-single-registeration"}, method = RequestMethod.POST)
    @ResponseBody
    public String saveSingleJointDetail(HttpServletRequest request, ModelMap model,
            @RequestBody JointDetailBean joint) throws IOException, SQLException {

        JointDetailBean mainInvDt = formDetailsRepository.getJointMaininvByRegId(joint.getReg_id());
        HashMap response = new HashMap<>();
//        if ("SUBMISSION".equalsIgnoreCase(joint.getStatus())) {
//            System.out.println("1");
//           
////            joint = registerService.createJoint(joint);
//            System.out.println("2");
//        }
        formDetailsRepository.saveSingleJointDetails(joint, response);
//        sendMail.sendSingleAccountRegistrationMail(joint.getFullName(), "Joint Account", mainInvDt.getFullName(), requestedDomain(request), mainInvDt.getEmail(), joint.getEmail());
        ResponseBean responseBean = ObjectCastManager.getInstance().jSONcast(ResponseBean.class, response);
        return responseBean.toString();
    }

}
