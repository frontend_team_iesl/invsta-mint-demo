/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.userInfobean;
import crm.nz.repository.UserInformationRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInformationController implements CurrentUserService {
    @Autowired
    private UserInformationRepository repository;
    
    @RequestMapping(value = {"/get-user-information"}, method = {RequestMethod.GET})
     @ResponseBody
    public String pendingInvestment() {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                userInfobean userdetailsByAdmin = repository.getUserdetailsByAdmin();
                return userdetailsByAdmin.toString();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                userInfobean userdetailsByAdvisor = repository.getUserdetailsByAdvisor(user.getUser_id());
                return userdetailsByAdvisor.toString();
            }
        }
        return "error";
    }
}
