/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.amazonaws.services.s3.model.S3Object;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.OnboardingsServices;
import crm.nz.beans.acc.api.PendingBankAccount;
import crm.nz.components.FileMethods;
import crm.nz.repository.BankAccountRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.DataUploadService;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author TOSHIBA R830
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api/admin")
public class PendingbankAccountController implements CurrentUserService {

    @Autowired
    private BankAccountRepository repository;
    @Autowired
    private OnboardingsServices onboardingsServices;
    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    DataUploadService dataUploadService;
    @Autowired
    FileMethods fileMethods;

    @RequestMapping(value = {"/pending-bankAccount"}, method = {RequestMethod.POST})
    @ResponseBody
    public String pendingbankAccount(ModelMap modelMap, @RequestBody PendingBankAccount bean) throws IOException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            bean.setUserId(user.getUser_id());
            bean.setApplicationId(user.getApplication_id());

            if (bean.getBankFile() != null && !bean.getBankFile().isEmpty()) {
                S3Object s3Object = dataUploadService.storeTos3BankDocByBase64(bean.getBeneficiaryId(), bean.getBankFile(), bean.getAccountHolderName());
                String bankFileUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
                System.out.println("bankFileUrl" + bankFileUrl);
                bean.setBankFile(bankFileUrl);
            }

            if (bean != null && bean.getBankName() != null && bean.getAccountNumber() != null && !bean.getAccountNumber().isEmpty() && !bean.getBankName().isEmpty()) {
                 if (bean.getApplicationId() != null) {
                String clientCredentials = oAuth2TokenService.getClientCredentials();
                String createBeneficiayBankAccountBody = "{\n"
                        + "\"ApplicationId\": \"" + bean.getApplicationId() + "\",\n"
                        + "\"BeneficiaryId\": \"" + bean.getBeneficiaryId() + "\",\n"
                        + "\"BankCode\": \"" + Integer.parseInt(bean.getAccountNumber().split("-")[0]) + "\",\n"
                        + "\"BranchCode\": \"" + Integer.parseInt(bean.getAccountNumber().split("-")[1]) + "\",\n"
                        + "\"Account\": \"" + Integer.parseInt(bean.getAccountNumber().split("-")[2]) + "\",\n"
                        + "\"Suffix\": \"" + Integer.parseInt(bean.getAccountNumber().split("-")[3]) + "\",\n"
                        + "\"BankAccountName\":\"" + bean.getBankName() + "\",\n"
                        + "\"BankAccountCurrency\":\"" + "NZD" + "\",\n"
                        + "\"IsPrimary\": true\n"
                        + "}";
               
                    StringBuilder bankAccountId = onboardingsServices.createBeneficiaryBankAccount(clientCredentials, bean.getApplicationId(), createBeneficiayBankAccountBody);
                    int parseInt = Integer.parseInt(bankAccountId.toString());
                    bean.setBankAccountId(bankAccountId.toString());
                }
                repository.setPendingBankAccount(bean);
                System.out.println("bean----------------------->" + bean);
                return "success";
            }

        }

        return "error";
    }

    @RequestMapping(value = {"/get-pending-bankAccount"}, method = {RequestMethod.GET})
    @ResponseBody
    public String GetBankAccount(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                List<PendingBankAccount> pendingankAccount = repository.getPendingankAccount();
                return pendingankAccount.toString();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
//                List<PendingInvestmentBean> pendingInvestment = repository.getAdvisorPendingInvestment(user.getUser_id());
                return "";
            }
        }
        return "error";
    }
}
