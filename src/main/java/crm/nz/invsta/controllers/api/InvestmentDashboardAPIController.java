/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.PricePortfoliosService;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.services.InvestmentsAsAtService;
import crm.nz.table.bean.SecuredUser;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class InvestmentDashboardAPIController implements CurrentUserService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private GetDataService dataService;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private Gson gson;
    @Autowired
    private InvestmentsAsAtService investmentsAsAtService;
    @Autowired
    private InvestmentsService investmentsService;

    @RequestMapping(value = {"/investment-holdings"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentHoldingsDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<InvestmentHolding> investmentHoldings = investmentsAsAtService.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            investmentsAsAtService.getInvestmentTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            return "{"
                    + "\"investmentHoldings\":" + investmentHoldings.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-performances"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentPerformancesDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<InvestmentPerformance> investmentPerformances = investmentsAsAtService.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            return "{"
                    + "\"investmentPerformances\":" + investmentPerformances.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/fmca-investment-mix"}, method = {RequestMethod.GET})
    @ResponseBody
    public String fmcaInvestmentMixDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
//            List<FmcaInvestmentMix> fmcaInvestmentMix = investmentsRepository.getTotalFmcaInvestmentMixByTable(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            List<FmcaInvestmentMix> fmcaInvestmentMix = investmentsAsAtService.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            return "{"
                    + "\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/show-me-more"}, method = {RequestMethod.GET})
    @ResponseBody
    public String portfolioDashboard(ModelMap modelMap,
            @RequestParam(value = "ic") String ic,
            @RequestParam(value = "pc") String pc
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
//             UserId = dataService.getUserId(user, UserId);
            List<InvestmentHolding> investmentHoldings = investmentsAsAtService.getInvestmentHoldings(null, null, ic, pc);
//            List<InvestmentTransection> investmnetTransection = investmentsRepository.getInvestmnetTransection(null, null, ic, pc);
//            List<FmcaInvestmentMix> fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(null, ic, pc);
            List<FmcaInvestmentMix> fmcaInvestmentMixByTable = portRepository.getTotalFmcaInvestmentMix(pc);
//            List<FmcaTopAssets> fmcaTopAssetses = investrepository.getFmcaTopAssets(null, ic, pc);
            List<FmcaTopAssets> fmcaTopAssetsesByTable = portRepository.getFmcaTopAssets(pc);

//            String clientCredentials = null;
//            clientCredentials = oAuth2TokenService.getClientCredentials();
//            if (fmcaTopAssetses != null) {
//                fmcaTopAssets = investmentsService.fmcaTopAssets(clientCredentials, ic, pc).toString();
//            }  else {
//                fmcaTopAssets = fmcaTopAssetses.toString();
//            }
//            StringBuilder pricePortfolioUnitPrices = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, pc);
            return "{"
                    //                    + "\"fmcaTopAssets\":" + fmcaTopAssets
                    + "\"fmcaTopAssetsesByTable\":" + fmcaTopAssetsesByTable.toString()
                    //                    + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                    + ",\"fmcaInvestmentMixByTable\":" + fmcaInvestmentMixByTable.toString()
                    + ",\"investmentHoldings\":" + investmentHoldings.toString()
                    //                    + ",\"pricePortfolioUnitPrices\":" + pricePortfolioUnitPrices.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-transactions"}, method = {RequestMethod.GET})
    @ResponseBody
    public String BeneTransactionHistory(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<PendingTransactionBean> pendingTransactions = transactionRepository.getPendingTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            List<TransactionItem> investmentTransactions = investmentsAsAtService.getInvestmentTransactions(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
            return "{"
                    + "\"investmentTransactions\":" + investmentTransactions.toString()
                    + ",\"pendingTransactions\":" + pendingTransactions.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-return-rate"}, method = {RequestMethod.GET})
    @ResponseBody
    public String investmentReturnRate(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            String clientCredentials = oAuth2TokenService.getClientCredentials2();
            StringBuilder investmentReturnRate = null;
            if (InvestmentCode != null && PortfolioCode != null) {
                investmentReturnRate = investmentsService.investmentReturnRate(clientCredentials, InvestmentCode, PortfolioCode);
            } else if (InvestmentCode != null) {
                investmentReturnRate = investmentsService.investmentReturnRate(clientCredentials, InvestmentCode);
            }
            return "{"
                    + "\"investmentReturnRate\":" + investmentReturnRate.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/investment-return-rate"}, method = {RequestMethod.POST})
    @ResponseBody
    public String investmentReturnRatePost(@RequestBody Beneficiary bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials2();
            List<String> investmentReturnlist = new LinkedList<>();
            System.err.println("InvestmentHolding--- " + bean);
            for (InvestmentHolding beandata : bean.getInvestmentHoldings()) {
                StringBuilder investmentReturnRate = investmentsService.investmentReturnRate(clientCredentials, beandata.getInvestmentCode(), beandata.getPortfolioCode());
                investmentReturnlist.add(investmentReturnRate.toString());
                System.err.println("Invest Rate----------- " + investmentReturnRate);
            }
            return "{"
                    + "\"investmentReturnRate\":" + investmentReturnlist
                    + "}";
        }
        return "{}";
    }

}
