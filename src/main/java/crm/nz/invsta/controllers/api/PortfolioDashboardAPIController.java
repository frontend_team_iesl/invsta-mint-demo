/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.PerformanceBean;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class PortfolioDashboardAPIController implements CurrentUserService {

    @Autowired
    private PortfolioRepository portRepository;

    @RequestMapping(value = {"/all-funds"}, method = {RequestMethod.GET})
    @ResponseBody
    public String Allfunds(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            List<Portfolio> allPortfolios = portRepository.getAllPortfolios();
            return allPortfolios.toString();
        }
        return "[]";
    }

    @RequestMapping(value = {"/investment-fund-details-{pc}", "/portfolio-details-{pc}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String Investmentfund(ModelMap modelMap,
            @PathVariable(value = "pc") String portfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(portfolioCode);
            return "{"
                    + "\"portfolioDetail\":" + portfolioDetail.toString()
                    + "}";
        }
        return "{}";
    }

    @RequestMapping(value = {"/update-FundPerformance"}, method = {RequestMethod.POST})
    @ResponseBody
    public String updateFundPerformance(ModelMap modelMap, @RequestBody PerformanceBean bean) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            portRepository.setFundPerformance(bean);
            return "/update-portfolio-" + bean.getPortfolio();
        }
        return "error";
    }

    @RequestMapping(value = {"/get-FundPerformance-{pc}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String getFundPerformance(ModelMap modelMap, @PathVariable(value = "pc") String portfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            List<PerformanceBean> fundPerformanceBeans = portRepository.getFundPerformance(!portfolioCode.equals("290007")?portfolioCode:"290006");
            PerformanceBean fundPerformance = fundPerformanceBeans != null && !fundPerformanceBeans.isEmpty() ? fundPerformanceBeans.get(0) : null;
            return fundPerformance != null ? fundPerformance.toString() : null;
        }
        return null;
    }

}
