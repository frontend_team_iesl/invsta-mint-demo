/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import com.google.gson.Gson;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.authorization.TransactionsService;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.Identification;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.beans.acc.api.PortfolioPrice;
import crm.nz.beans.acc.api.TransactionItem;
import crm.nz.components.CommonMethods;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.repository.TransactionRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.DataChartService;
import crm.nz.services.GetDataService;
import crm.nz.services.InvestmentsAsAtService;
import crm.nz.table.bean.SecuredUser;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api")
public class BeneficiaryDashboardAPIController implements CurrentUserService {

    @Autowired
    private GetDataService dataService;
    @Autowired
    private InvestmentsAsAtService investmentsAsAtService;
    @Autowired
    private BeneficiaryRepository beneficiaryrepository;
    @Autowired
    private InvestmentRepository investrepository;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DataChartService dataservice;

    @RequestMapping(value = {"/beneficiaries"}, method = {RequestMethod.GET})
    @ResponseBody
    public String allBeneficiariesToDB() {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                List<Beneficiary> beneficiaries = beneficiaryrepository.getBeneficiaryDetails(null, null, null);
                return "{"
                        + "\"beneficiaries\":" + beneficiaries.toString()
                        + "}";
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                List<Beneficiary> beneficiaries = beneficiaryrepository.getBeneficiaryDetails(null, null, user.getUser_id());
                return "{"
                        + "\"beneficiaries\":" + beneficiaries.toString()
                        + "}";
            } else if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                return "{\"message\":\"Sorry\"}";
            }

//            if (beneficiaries.size() < 5) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials();
//                Type type = null;
//                boolean resultEqualsTo = true;
//                Integer page = 1, clientSize = 25;
//                type = new TypeToken<LinkedList<Beneficiary>>() {
//                }.getType();
//                LinkedList<Beneficiary> beneficiaryList = new LinkedList<>();
//                while (resultEqualsTo) {
//                    try {
//                        StringBuilder result0 = beneficiariesService.beneficiaries(clientCredentials, page, clientSize);
//                        LinkedList<Beneficiary> list = gson.fromJson(result0.toString(), type);
//                        System.out.println(page + " @ pageIndex List.size" + list.size());
//                        beneficiaryList.addAll(list);
//                        if (clientSize != list.size()) {
//                            resultEqualsTo = false;
//                        }
//                    } catch (JsonSyntaxException ex) {
//                        ex.printStackTrace();
//                    }
//                    page++;
//                }
//                beneficiaryrepository.saveBeneficiaries(beneficiaryList);
//                beneficiaries = beneficiaryList;
//            }
        }
        return "{}";
    }

    @RequestMapping(value = {"/a12314646446446446464546454122497845167987416789464574596764641789764679845676465f4df4s7fsd46f74asdf46sdf4f894sdf52634f896sd4f6sd7f64s6f4sd564f6sd7f8sd4f6f564sd6f776d51f6sd7fsd67456sd4f7sd65f41sd6f7sd57f56sd46f-{UId}-{BId}"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneById(ModelMap modelMap,
            @PathVariable(value = "UId") String UserId,
            @PathVariable(value = "BId") String BeneficiaryId) {
        return "forward:/beneficiary-".concat(UserId).concat("-" + UserId);
    }

    @RequestMapping(value = {"/beneficiary"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryById(ModelMap modelMap,
            @RequestParam(value = "UserId", required = false) String UserId,
            @RequestParam(value = "BeneficiaryId") String BeneficiaryId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            Beneficiary beneficiary = beneficiaryrepository.getBeneficiary(BeneficiaryId);
            System.out.println("beneficiary-------------" + beneficiary);
            List<Address> addresses = beneficiaryrepository.getAddresses(UserId, BeneficiaryId);
            List<Email> emails = beneficiaryrepository.getEmails(UserId, BeneficiaryId);
            List<PhoneNumber> phoneNumber = beneficiaryrepository.getPhoneNumbers(UserId, BeneficiaryId);
            List<Identification> identifications = beneficiaryrepository.getIdentifications(UserId, BeneficiaryId);
            List<BankAccountDetail> bankAccountDetails = beneficiaryrepository.getBankAccountDetails(UserId, BeneficiaryId);
//            if (identifications == null || identifications.isEmpty()) {
//                String clientCredentials = oAuth2TokenService.getClientCredentials();
//                StringBuilder beneficiary1 = beneficiariesService.beneficiaryById(clientCredentials, id);
//                beneficiary = gson.fromJson(beneficiary1.toString(), Beneficiary.class);
//                beneficiaryrepository.saveEmails(beneficiary.getEmails(), beneficiary.getId());
//                beneficiaryrepository.savePhoneNumbers(beneficiary.getPhoneNumbers(), beneficiary.getId());
//                beneficiaryrepository.saveAddresses(beneficiary.getAddresses(), beneficiary.getId());
//                beneficiaryrepository.saveIdentifications(beneficiary.getIdentification(), beneficiary.getId());
//                addresses = beneficiary.getAddresses();
//                emails = beneficiary.getEmails();
//                identifications = beneficiary.getIdentification();
//                phoneNumber = beneficiary.getPhoneNumbers();
//            } 
            if (BeneficiaryId != null) {
                return "{"
                        + "\"addresses\":" + addresses.toString()
                        + ",\"beneficiary\":" + beneficiary.toString()
                        + ",\"emails\":" + emails.toString()
                        + ",\"bankAccountDetails\":" + bankAccountDetails.toString()
                        + ",\"phoneNumber\":" + phoneNumber.toString()
                        + ",\"identifications\":" + identifications.toString()
                        + "}";
            }
        }

        return "{}";
    }

    @RequestMapping(value = {"/beneficiary-dashboard"}, method = {RequestMethod.GET})
    @ResponseBody
    public String beneficiaryDashboardDB(
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String BeneficiaryId,
            @RequestParam(value = "ic", required = false) String InvestmentCode,
            @RequestParam(value = "pc", required = false) String PortfolioCode
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            if (UserId != null) {
                System.out.println("1");
                List<InvestmentHolding> investmentHoldings = investmentsAsAtService.getInvestmentHoldings(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                List<InvestmentPerformance> investmentPerformances = investmentsAsAtService.getInvestmentPerformances(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                List<FmcaInvestmentMix> fmcaInvestmentMix = investrepository.getTotalFmcaInvestmentMix(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
//                List<FmcaInvestmentMix> fmcaInvestmentMixByTable = investrepository.getTotalFmcaInvestmentMixByTable(UserId, BeneficiaryId, InvestmentCode, PortfolioCode);
                return "{"
                        + "\"investmentHoldings\":" + investmentHoldings.toString()
                        + ",\"investmentPerformances\":" + investmentPerformances.toString()
                        + ",\"fmcaInvestmentMixByTable\":" + fmcaInvestmentMix.toString()
                        + ",\"fmcaInvestmentMix\":" + fmcaInvestmentMix.toString()
                        + "}";
            }
        }
        return null;
    }

    @RequestMapping(value = {"/getPerformanceforchart"}, method = {RequestMethod.GET})
    @ResponseBody
    public Map getPerformance(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "portfolios", required = false) String Portfolios) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            Investment startInvestmentDate = investrepository.getStartInvestmentDate(UserId, null);
            String[] portfolioArr = null;
            Map<String, List<PortfolioPrice>> performancePricesMap = new HashMap<String, List<PortfolioPrice>>();
            if (Portfolios.contains(",")) {
                portfolioArr = Portfolios.split(",");
            } else {
                portfolioArr = new String[1];
                portfolioArr[0] = Portfolios;
            }

            List<PortfolioPrice> performancePrices = portRepository.getPerformancePrices("290004", startInvestmentDate.getContractDate());
            performancePricesMap.put("A290004", performancePrices);

            return performancePricesMap;

        }

        return null;
    }

    @RequestMapping(value = {"/getallPerformanceforchart"}, method = {RequestMethod.GET})
    @ResponseBody
    public Map getAllDatesPerformance(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId, @RequestParam(value = "ic", required = false) String ic,
            @RequestParam(value = "portfolios", required = false) String Portfolios[]) throws ParseException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            Investment startInvestmentDate = investrepository.getStartInvestmentDate(UserId, null);
            String min = startInvestmentDate.getContractDate();
            Date minDate = common.parseDate(min, CommonMethods.format3);
            Date today = common.today();
            List<Date> allDates = common.getAllDates(minDate, today);
            Map<String, Map<String, TransactionItem>> unitsByDate = dataservice.unitsByDate(Portfolios, ic);
            Map<String, Map<String, PortfolioPrice>> portfolioPriceByDate = dataservice.portfolioPriceByDate(Portfolios, min);
            Map<String, Double> allPortfolioChart = new TreeMap<>();
//            Map<String, Map<String, Double>> portfolioChart = new TreeMap<>();
            Map<String, Double> valueByDate = new TreeMap<>();
            Iterator<Map.Entry<String, Map<String, TransactionItem>>> iterator = unitsByDate.entrySet().iterator();
            Iterator<Map.Entry<String, Map<String, PortfolioPrice>>> iterator1 = portfolioPriceByDate.entrySet().iterator();
            while (iterator.hasNext() && iterator1.hasNext()) {
                Double preUnits = 0.00;
                Double prePrice = 0.00;
                Double balance = 0.00;
                Map.Entry<String, Map<String, TransactionItem>> next = iterator.next();
                Map.Entry<String, Map<String, PortfolioPrice>> next1 = iterator1.next();
                String key = next.getKey();
                String key1 = next1.getKey();
                Map<String, TransactionItem> value = next.getValue();
                Map<String, PortfolioPrice> value1 = next1.getValue();
                for (Date a : allDates) {
                    String dateFormat = common.dateFormat(a, CommonMethods.format3);
                    TransactionItem item = value.get(dateFormat);
                    PortfolioPrice price = value1.get(dateFormat);
                    if (item != null) {
                        preUnits += item.getUnits();
                    }
                    if (price != null) {
                        prePrice = price.getLastPrice();
                    }
                    if (item == null && price != null) {
                        balance = preUnits * price.getLastPrice();
                    } else if (price == null && item != null) {
                        balance = prePrice * preUnits;
                    } else if (price == null && item == null) {
                        balance = preUnits * prePrice;
                    } else if (price != null && item != null) {
                        balance = preUnits * price.getLastPrice();
                    }
                    Double tempval = allPortfolioChart.get(dateFormat);
                    Double totalbal = 0.0;
                    if (tempval != null) {
                        totalbal = balance + tempval;
                    } else {
                        totalbal = balance;
                    }
                    allPortfolioChart.put(dateFormat, totalbal);
                }
                valueByDate.putAll(allPortfolioChart);
//                portfolioChart.put("A" + key, valueByDate);

            }
            return valueByDate;
        }
        return null;
    }

    @RequestMapping(value = {"/getPerformanceOfLastWeek"}, method = {RequestMethod.GET})
    @ResponseBody
    public Map getPerformanceOfLastWeek(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId, @RequestParam(value = "ic", required = false) String ic,
            @RequestParam(value = "portfolios", required = false) String Portfolios[]) throws ParseException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            Date today = common.today();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            calendar.add(Calendar.DAY_OF_YEAR, -7);
            Date minDate = calendar.getTime();
            List<Date> allDates = common.getAllDates(minDate, today);
            Map<String, Map<String, TransactionItem>> unitsByDate = dataservice.unitsByDate(Portfolios, ic);
            if (unitsByDate != null) {
                return unitsByDate;
            }
            Map<String, Map<String, PortfolioPrice>> portfolioPriceByDate = dataservice.portfolioPriceByDate(Portfolios, common.format3(minDate));
//            Map<String, Double> allPortfolioChart = new TreeMap<>();
            Map<String, Map<String, Double>> portfolioChart = new TreeMap<>();

            Iterator<Map.Entry<String, Map<String, TransactionItem>>> iterator = unitsByDate.entrySet().iterator();
            Iterator<Map.Entry<String, Map<String, PortfolioPrice>>> iterator1 = portfolioPriceByDate.entrySet().iterator();
            while (iterator.hasNext() && iterator1.hasNext()) {
                Map<String, Double> valueByDate = new TreeMap<>();
                Double preUnits = 0.00;
                Double prePrice = 0.00;
                Double balance = 0.00;
                Map.Entry<String, Map<String, TransactionItem>> next = iterator.next();
                Map.Entry<String, Map<String, PortfolioPrice>> next1 = iterator1.next();
                String key = next.getKey();
                Map<String, TransactionItem> value = next.getValue();
                Map<String, PortfolioPrice> value1 = next1.getValue();
                for (Date a : allDates) {
                    String dateFormat = common.dateFormat(a, CommonMethods.format3);
                    TransactionItem item = value.get(dateFormat);
                    PortfolioPrice price = value1.get(dateFormat);
                    if (item != null) {
                        preUnits += item.getUnits();
                    }
                    if (price != null) {
                        prePrice = price.getLastPrice();
                    }
                    if (item == null && price != null) {
                        balance = preUnits * price.getLastPrice();
                    } else if (price == null && item != null) {
                        balance = prePrice * preUnits;
                    } else if (price == null && item == null) {
                        balance = preUnits * prePrice;
                    } else if (price != null && item != null) {
                        balance = preUnits * price.getLastPrice();
                    }
                    valueByDate.put(dateFormat, balance);
                }
                portfolioChart.put("A" + key, valueByDate);
            }
            return portfolioChart;
        }
        return null;
    }

    @RequestMapping(value = {"/getUnitsPerformancechart"}, method = {RequestMethod.GET})
    @ResponseBody
    public Map getUnitsPerformancechart(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId, @RequestParam(value = "ic", required = false) String ic,
            @RequestParam(value = "portfolios", required = false) String Portfolios[]) throws ParseException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            Map<String, Double> valueByDate = new TreeMap<>();
            Map<String, Double> allPortfolioChart = new TreeMap<>();
//            Map<String,  Map<String, Double>> weekMapPort  = new TreeMap<>();
            Map<String, Map<String, Double>> returnMap = new TreeMap<>();
            List<String> allDatesOfLatestWeek = common.getAllDatesOfLatestWeek();
            for (String port : Portfolios) {
                Map<String, Double> temp = new TreeMap<>();
                Map<String, Double> weekMap = new TreeMap<>();
                Map<String, TransactionItem> unitprices = dataservice.TotalUnitswithprice(port, ic);
                System.out.println("unitprices  " + unitprices);
                Iterator<Map.Entry<String, TransactionItem>> iterator = unitprices.entrySet().iterator();
                if (iterator.hasNext()) {
                    Map.Entry<String, TransactionItem> next = iterator.next();
                    System.out.println("next  " + next);
                    String key = next.getKey();
                    Date today = common.today();
                    Double preprice = 0.00;
                    Double balance = 0.00;
                    Date min = common.parseDate(key, CommonMethods.format3);
                    System.out.println("min date  " + min);
                    List<Date> allDates = common.getAllDates(min, today);
                    for (Date date : allDates) {
                        String dateFormat = common.dateFormat(date, CommonMethods.format3);
                        TransactionItem item = unitprices.get(dateFormat);
                        if (item != null) {
                            balance = item.getPrice();
                            preprice = item.getPrice();
                        }
                        if (item == null) {
                            balance = preprice;
                        }
                        Double tempval = allPortfolioChart.get(dateFormat);
                        Double totalbal = 0.0;
                        if (tempval != null) {
                            totalbal = balance + tempval;
                        } else {
                            totalbal = balance;
                        }
                        allPortfolioChart.put(dateFormat, totalbal);
                        temp.put(dateFormat, balance);
                    }
                    for (String oneday : allDatesOfLatestWeek) {
                        weekMap.put(oneday, temp.get(oneday));
                    }
//                    weekMapPort.put(port, weekMap);
                    returnMap.put(port, weekMap);
                    valueByDate.putAll(allPortfolioChart);

                }

            }
            returnMap.put("valueByDate", valueByDate);
            return returnMap;
        }
        return null;
    }
}
