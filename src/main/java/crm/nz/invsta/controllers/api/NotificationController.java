/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.api;

import crm.nz.beans.acc.api.Notification;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.components.CommonMethods;
import crm.nz.lambda.repo.NotificationDetailsRepository;
import crm.nz.repository.NotificationRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN/rest/groot/db/api/admin/delete-notification
 */
@RestController
@RequestMapping(value = "/rest/groot/db/api/admin")
public class NotificationController implements CurrentUserService {

    @Autowired
    NotificationDetailsRepository notificationdetailsRepository;

    @Autowired
    CommonMethods common;

    @RequestMapping(value = {"/notification"}, method = {RequestMethod.GET})
    @ResponseBody
    public String allNotification(ModelMap modelMap, @RequestParam(value = "id", required = false) String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
            }
//            return "{"
//                    + "\"allNotification\":" + allNotification.toString()
//                    + "}";

        }
        return "{}";
    }

    @RequestMapping(value = {"/delete-notification"}, method = RequestMethod.GET)
    @ResponseBody
    public String deleteNotification(ModelMap modelMap,
//            @RequestBody Notification notify,
             @RequestParam(value = "id") String id) throws SQLException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            notificationdetailsRepository.deleteNotification(id);
        }
        return "data";
    }

}
