/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.PortfoliosService;
import crm.nz.authorization.PricePortfoliosService;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.Identification;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.PIRRate;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.beans.acc.api.PortfolioPrice;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.InvestmentRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.table.bean.SecuredUser;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author IESL
 */
@Deprecated
@Controller
public class DeprecatedMappingController implements CurrentUserService {

    @Autowired
    private GetOAuth2TokenService oAuth2TokenService;
    @Autowired
    private BeneficiariesService beneficiariesService;
    @Autowired
    private PortfoliosService portfoliosService;
    @Autowired
    private PricePortfoliosService pricePortfoliosService;
    @Autowired
    private PortfolioRepository portRepository;
    @Autowired
    private BeneficiaryRepository beneficiaryRepository;
    @Autowired
    private InvestmentRepository investmentRepository;
    @Autowired
    private Gson gson;
    @Autowired
    private GetDataService dataService;

    @RequestMapping(value = {"/post-all-funds-and-navs"}, method = {RequestMethod.GET})
    public String postAllFundsAndNavs(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
            StringBuilder result = portfoliosService.allPortfolios(clientCredentials);
            Type type = new TypeToken<LinkedList<Portfolio>>() {
            }.getType();
            List<Portfolio> portfolioDetails = gson.fromJson(result.toString(), type);
            portRepository.savePortfolios(portfolioDetails);
            Type type2 = new TypeToken<LinkedList<PortfolioPrice>>() {
            }.getType();
            for (Portfolio pD : portfolioDetails) {
                StringBuilder result2 = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, pD.getCode());
                List<PortfolioPrice> portfolioDetail = gson.fromJson(result2.toString(), type2);
                portRepository.savePortfolioPrices(portfolioDetail);
            }
            return "beneficary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/post-all-funds"}, method = {RequestMethod.GET})
    public String postAllFunds(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
            StringBuilder result = portfoliosService.allPortfolios(clientCredentials);
            Type type = new TypeToken<LinkedList<Portfolio>>() {
            }.getType();
            List<Portfolio> portfolioDetails = gson.fromJson(result.toString(), type);
            portRepository.savePortfolios(portfolioDetails);
            return "beneficary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/post-all-fund-navs-{pc}"}, method = {RequestMethod.GET})
    public String postAllFundUnitPrices(ModelMap modelMap,
            @PathVariable("pc") final String PortfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            String clientCredentials = oAuth2TokenService.getClientCredentials2();
            Type type2 = new TypeToken<LinkedList<PortfolioPrice>>() {
            }.getType();
            StringBuilder result2 = pricePortfoliosService.pricePortfolioUnitPrices(clientCredentials, PortfolioCode);
            if (result2 != null) {
                List<PortfolioPrice> portfolioDetail = gson.fromJson(result2.toString(), type2);
                portRepository.savePortfolioPrices(portfolioDetail);
            }
            return "beneficary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/post-all-beneficiaries"}, method = {RequestMethod.GET})
    public String postAllBeneficiaries(ModelMap modelMap,
            @RequestParam(required = true, name = "pg") Integer page,
            @RequestParam(required = true, name = "sz") Integer pageSize
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null && "ADMIN".equalsIgnoreCase(user.getRole())) {
            String clientCredentials = oAuth2TokenService.getClientCredentials();
            boolean resultEqualsTo = true;
//            Integer page = 1, pageSize = 10;//200 is maximum client size
            Type type = new TypeToken<LinkedList<Beneficiary>>() {
            }.getType();
            List<Beneficiary> apiBeneficiaryList = new LinkedList<>();
//            while (resultEqualsTo) 
            {
                StringBuilder beneficiaries = beneficiariesService.beneficiaries(clientCredentials, page, pageSize);
                try {
                    LinkedList<Beneficiary> list = gson.fromJson(beneficiaries.toString(), type);
                    System.out.println(page + " @ pageIndex List.size" + list.size());
                    apiBeneficiaryList.addAll(list);
                    if (pageSize != list.size()) {
                        resultEqualsTo = false;
                    }
                } catch (JsonSyntaxException ex) {
                    ex.printStackTrace();
                }
                page++;
            }
//            List<Beneficiary> beneficiaryList = new LinkedList<>();
//            List<Email> emailList = new LinkedList<>();
//            List<PhoneNumber> phoneNumberList = new LinkedList<>();
//            List<Address> addressList = new LinkedList<>();
//            List<Identification> identificationList = new LinkedList<>();
//            List<BankAccountDetail> bankAccountDetailList = new LinkedList<>();
//            List<PIRRate> pirRateList = new LinkedList<>();
//            List<Investment> investmentList = new LinkedList<>();
//            for (Beneficiary b : apiBeneficiaryList) {
//                try {
//                    final String BeneficiaryId = String.valueOf(b.getId());//                String BeneficiaryId = "4721";
//                    if (!BeneficiaryId.isEmpty()) {
//                        StringBuilder result = beneficiariesService.beneficiaryById(clientCredentials, BeneficiaryId);
//                        Beneficiary bean = gson.fromJson(result.toString(), Beneficiary.class);
//                        beneficiaryList.add(bean);
//                        if (bean.getEmails() != null && !bean.getEmails().isEmpty()) {
//                            bean.getEmails().forEach((ele) -> {
//                                ele.setBeneficiaryId(BeneficiaryId);
//                            });
//                            emailList.addAll(bean.getEmails());
//                        }
//                        if (bean.getPhoneNumbers() != null && !bean.getPhoneNumbers().isEmpty()) {
//                            bean.getPhoneNumbers().forEach((ele) -> {
//                                ele.setBeneficiaryId(BeneficiaryId);
//                            });
//                            phoneNumberList.addAll(bean.getPhoneNumbers());
//                        }
//                        if (bean.getAddresses() != null && !bean.getAddresses().isEmpty()) {
//                            bean.getAddresses().forEach((ele) -> {
//                                ele.setBeneficiaryId(BeneficiaryId);
//                            });
//                            addressList.addAll(bean.getAddresses());
//                        }
//                        if (bean.getIdentification() != null && !bean.getIdentification().isEmpty()) {
//                            bean.getIdentification().forEach((ele) -> {
//                                ele.setBeneficiaryId(BeneficiaryId);
//                            });
//                            identificationList.addAll(bean.getIdentification());
//                        }
//                        if (bean.getPIRRates() != null && !bean.getPIRRates().isEmpty()) {
//                            bean.getPIRRates().forEach((ele) -> {
//                                ele.setBeneficiaryId(BeneficiaryId);
//                            });
//                            pirRateList.addAll(bean.getPIRRates());
//                        }
//                        if (bean.getInvestments() != null && !bean.getInvestments().isEmpty()) {
//                            bean.getInvestments().forEach((inv) -> {
//                                inv.setBeneficiaryId(BeneficiaryId);
//                                if (inv.getBankAccountDetail() != null && !inv.getBankAccountDetail().isEmpty()) {
//                                    inv.getBankAccountDetail().forEach((acc) -> {
//                                        acc.setBeneficiaryId(BeneficiaryId);
//                                        acc.setInvestmentCode(inv.getCode());
//                                    });
//                                    bankAccountDetailList.addAll(inv.getBankAccountDetail());
//                                }
//                            });
//                            investmentList.addAll(bean.getInvestments());
//                        }
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
            try {
                beneficiaryRepository.saveBeneficiaries(apiBeneficiaryList);
//                beneficiaryRepository.saveEmails(emailList);
//                beneficiaryRepository.savePhoneNumbers(phoneNumberList);
//                beneficiaryRepository.saveAddresses(addressList);
//                beneficiaryRepository.saveIdentifications(identificationList);
//                beneficiaryRepository.saveBankAccountDetails(bankAccountDetailList);
//                beneficiaryRepository.savePIRRates(pirRateList);
//                investmentRepository.saveInvestments(investmentList);
//                beneficiaryRepository.saveUsernames(emailList);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return "beneficary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/home-beneficiary-dashboard"}, method = {RequestMethod.GET})
    public String beneficiaryDashboard(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
            }
            modelMap.addAttribute("id", id);
            return "beneficiary-dashboard";

        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/beneficiary"}, method = {RequestMethod.GET})
    public String profile(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
            }
            modelMap.addAttribute("id", id);
            return "beneficary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/home-beneficiary-transactions"}, method = {RequestMethod.GET})
    public String beneficaryTransactions(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String id) {//bounce
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
            }
            modelMap.addAttribute("id", id);
            return "beneficary-transactions-db";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/show-me-more", "/show-me-more-db"}, method = {RequestMethod.POST})
    public String showmemore(ModelMap modelMap,
            @RequestParam("pc") String portfolioCode,
            @RequestParam("ic") String investmentCode,
            @RequestParam(value = "id", required = false) String UserId
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(portfolioCode);
            List<Beneficiary> beneficiaryDetails = beneficiaryRepository.getBeneficiaryDetailsByInvestmentCode(investmentCode);
            List<BankAccountDetail> bankAccountDetails = beneficiaryRepository.getBankAccountsByInvestmentCode(investmentCode);
            Investment startInvestmentDate = investmentRepository.getStartInvestmentDate(UserId, investmentCode);
            List<PortfolioPrice> performancePrices = portRepository.getPerformancePrices(portfolioCode, startInvestmentDate.getContractDate());
            modelMap.addAttribute("performancePrices", performancePrices);
            modelMap.addAttribute("beneficiaryDetails", beneficiaryDetails);
            modelMap.addAttribute("bankAccountDetails", bankAccountDetails);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("ic", investmentCode);
            modelMap.addAttribute("pc", portfolioCode);
            modelMap.addAttribute("portfolioDetail", portfolioDetail);
            return "new-show-me-more-db";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/new-show-me-more-{ic}"}, method = {RequestMethod.GET})
    public String newShowmemore(ModelMap modelMap,
            @PathVariable("ic") String investmentCode,
            @RequestParam("pc") String portfolioCode,
            @RequestParam(value = "id", required = false) String UserId
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            PortfolioDetail portfolioDetail = portRepository.getPortfolioDetail(portfolioCode);
            List<Beneficiary> beneficiaryDetails = beneficiaryRepository.getBeneficiaryDetailsByInvestmentCode(investmentCode);
            List<BankAccountDetail> bankAccountDetails = beneficiaryRepository.getBankAccountsByInvestmentCode(investmentCode);
            modelMap.addAttribute("beneficiaryDetails", beneficiaryDetails);
            modelMap.addAttribute("bankAccountDetails", bankAccountDetails);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("ic", investmentCode);
            modelMap.addAttribute("pc", portfolioCode);
            modelMap.addAttribute("portfolioDetail", portfolioDetail);
            return "show-me-more-db";
        }
        return "redirect:/login";
    }

}
