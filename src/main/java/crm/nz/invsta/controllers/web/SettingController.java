/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.amazonaws.services.s3.model.S3Object;
import crm.nz.components.CommonMethods;
import crm.nz.components.FileMethods;
import crm.nz.lambda.repo.NotificationDetailsRepository;
import crm.nz.mail.SendMail;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.sql.SQLException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ADMIN
 */
@Controller
public class SettingController implements CurrentUserService {

    @Autowired
    FileMethods fileMethods;

    @Autowired
    CommonMethods common;

    @Autowired
    BeneficiaryRepository beneficiaryRepository;

    @Autowired
    NotificationDetailsRepository notification;

    @RequestMapping(value = {"/profilePic"}, method = RequestMethod.POST)
    public String profilePicChange(ModelMap modelMap, @ModelAttribute Login login) {
        SecuredUser user = getCurrentUser();
        login.setUser_id(user.getUser_id());
        if (login.getUserLogo() != null && !login.getUserLogo().isEmpty()) {
            MultipartFile multipartFile = login.getUserLogo();
            String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename()) + common.today().getTime();
            S3Object s3Object = fileMethods.storeTos3Pics(multipartFile, login.getUser_id(), name, ext);
            String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            login.setUserLogoPath(url);
        }
        beneficiaryRepository.updateProfile(login);
        return "redirect:/profile-db";
    }

    @RequestMapping(value = {"/notification"}, method = RequestMethod.POST)
    public String notification(ModelMap modelMap,
            @RequestParam(value = "id") String notifyId,
            @RequestParam(value = "code") String pc,
            @RequestParam(value = "ic" , required = false) String ic,
            @RequestParam(value = "type") String type) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("INVESTMENT_PAYMENT".equalsIgnoreCase(type)) {
                return "redirect:/make-investment-" + pc + "?notifyId=" + notifyId;
            } else if ("TRANSACTION_PAYMENT".equalsIgnoreCase(type)) {
//                make-transaction-290002?ic=MIN476
//                make-transaction-MIN476?=icMIN476&notifyId=33
                return "redirect:/make-transaction-" + pc + "?ic=" + ic + "&notifyId=" +notifyId;
            }

        }
        return "redirect:/profile-db";
    }

//    @RequestMapping(value = {"/delete-notification"}, method = RequestMethod.POST)
//    public String deleteNotification(ModelMap modelMap,
//            @RequestParam(value = "id") String notifyId) throws SQLException {
//        SecuredUser user = getCurrentUser();
//        if (user != null) {
//            notification.deleteNotification(notifyId);
//        }
//        return "data";
//    }
}
