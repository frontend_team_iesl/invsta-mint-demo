/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.security.CurrentUserService;
import datadog.trace.api.Trace;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class SearchCompanycontroller implements CurrentUserService {

    @Trace
    @RequestMapping(value = {"/search-company"}, method = {RequestMethod.GET})
    public String searchcompany(ModelMap modelMap) {
        modelMap.addAttribute("token", "3c595b269b2d0d5c382c86037de0b9d7");
        return "search-company";

    }

    @Trace
    @RequestMapping(value = {"/test-request"}, method = {RequestMethod.GET})
    @ResponseBody
    public String test(ModelMap modelMap,
            @RequestHeader(value = "asb-token", required = false) String token,
            @RequestHeader(value = "Authorization", required = false) String auth
    ) {
        return "[" + token + ", " + auth + "]";
    }
}
