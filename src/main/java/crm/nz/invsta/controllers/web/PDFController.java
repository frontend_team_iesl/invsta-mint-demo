/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.itextpdf.text.DocumentException;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.components.PDFGenerator;
import crm.nz.lambda.repo.FormDetailsRepository;
import java.io.ByteArrayInputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Dell
 */
@Controller
@RequestMapping("/pdf")
public class PDFController {
    
    
      @Autowired
    private FormDetailsRepository repository;
      @Autowired
    private PDFGenerator pdfGenerator;

    
    
@RequestMapping(value = "/pdfRequests", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> customersReport() throws DocumentException { 
       
//        List<InvestmentBean> pendingInvestmentRequests = databaseService.pendingInvestmentRequests(null, null, null, null);
        ByteArrayInputStream bis = pdfGenerator.getDataZooPdf("abc");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=PendingInvestmentRequests.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(new InputStreamResource(bis));
    }
    
    
    
    
    
}
