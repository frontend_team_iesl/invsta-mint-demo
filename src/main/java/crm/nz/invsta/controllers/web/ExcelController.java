package crm.nz.invsta.controllers.web;

import crm.nz.components.ExcelGenerator;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.repository.PendingTransactionRepository;
import org.springframework.stereotype.Controller;
import java.io.ByteArrayInputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/xls")
public class ExcelController {

    @Autowired
    private FormDetailsRepository repository;
    @Autowired
    private ExcelGenerator excelGenerator;
    @Autowired
    private PendingTransactionRepository Pendingrepository;

    @RequestMapping(value = "/company-sheet", method = RequestMethod.GET, produces = "application/xls")
    public ResponseEntity<InputStreamResource> CompanyExcel() {

        List<CompanyDetailBean> companyDetails = repository.getCompanyDetails();
        ByteArrayInputStream bis = excelGenerator.getCompanydata(companyDetails);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=companyxls.xls");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/transection-sheet", method = RequestMethod.GET, produces = "application/xls")
    public ResponseEntity<InputStreamResource> TransectionExcel() {

        List<PendingTransactionBean> pendingTransaction = Pendingrepository.getPendingsSellTransaction();
        ByteArrayInputStream bis = excelGenerator.getTransectiondata(pendingTransaction);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=transection.xls");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/transectionInvestment-sheet", method = RequestMethod.GET, produces = "application/xls")
    public ResponseEntity<InputStreamResource> directDebitExcel() {

        List<PendingInvestmentBean> pendingTransaction = Pendingrepository.getDirectDebitTransaction();
        ByteArrayInputStream bis = excelGenerator.getDirectDebitdata(pendingTransaction);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=InvestmentTransection.xls");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/transectionMaster-sheet", method = RequestMethod.GET, produces = "application/xls")
    public ResponseEntity<InputStreamResource> transectionMasterExcel() {

        List<PendingTransactionBean> pendingsSellTransaction = Pendingrepository.getPendingsSellTransaction();
        ByteArrayInputStream bis = excelGenerator.getMasterTransectiondata(pendingsSellTransaction);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=InvestmentTransection.xls");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(new InputStreamResource(bis));
    }

}
