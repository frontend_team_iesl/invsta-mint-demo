/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.google.gson.Gson;
import crm.nz.authorization.BeneficiariesService;
import crm.nz.authorization.GetOAuth2TokenService;
import crm.nz.authorization.InvestmentsService;
import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.Notification;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.CommonRepository;
import crm.nz.repository.NotificationRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeControllerDB implements CurrentUserService {

    @Autowired
    GetOAuth2TokenService auth2TokenService;
    @Autowired
    BeneficiariesService beneficiariesService;
    @Autowired
    InvestmentsService investmentsService;
    @Autowired
    BeneficiaryRepository beneficiaryRepository;
    @Autowired
    FormDetailsRepository formDetailsRepository;
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    Gson gson;
    @Autowired
    private GetDataService dataService;
    @Autowired
    CommonRepository commonRepository;

    @Trace
    @RequestMapping(value = {"/home-beneficiary-db"}, method = {RequestMethod.GET})
    public String beneficiaryProfileFromDB(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
                modelMap.addAttribute("allNotification", allNotification);
                UserId = user.getUser_id();
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                UserId = dataService.getUserId(user, UserId);
            }
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "beneficary-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-beneficiary-dashboard-db"}, method = {RequestMethod.GET})
    public String beneficiaryDashboardFromDB(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "name", required = false) String userName,
            @RequestParam(value = "ic", required = false) String ic
    ) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
            modelMap.addAttribute("allNotification", allNotification);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("userName", userName);
            modelMap.addAttribute("ic", ic);
            return "beneficiary-dashboard-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/show-me-more-db-{ic}"}, method = {RequestMethod.GET})
    public String showmemore(ModelMap modelMap,
            @PathVariable("ic") String InvestmentCode,
            @RequestParam("pc") String PortfolioCode) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("ic", InvestmentCode);
            modelMap.addAttribute("pc", PortfolioCode);
            return "show-me-more-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/profile-db","/profile"}, method = {RequestMethod.GET})
    public String getBenficaryIDs(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<BankCodeBean> allBankCode = commonRepository.getAllBankCode();
            modelMap.addAttribute("allBankCode", allBankCode);

            List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
            modelMap.addAttribute("allNotification", allNotification);
            System.out.println("userId" + UserId);
            List<Beneficiary> beneficiaryDetails = beneficiaryRepository.getBeneficiaryDetails(UserId, null, null);
            modelMap.addAttribute("beneficiaryDetails", beneficiaryDetails);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            return "beneficary-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-beneficiary-transactions-db","/transactions"}, method = {RequestMethod.GET})
    public String beneficaryTransactions(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId) {//bounce
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
            modelMap.addAttribute("allNotification", allNotification);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            return "beneficary-transactions-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-reg-details-db-{tkn}", "/reg-details-db-{tkn}"}, method = {RequestMethod.GET})
    public String details(ModelMap modelMap,
            @PathVariable("tkn") String token) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("token", token);
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null) {
                modelMap.addAttribute("person", person);
            } else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null) {
                    modelMap.addAttribute("joint", joint);
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null) {
                        modelMap.addAttribute("company", company);
                    }
                }
            }
            return "reg-details-db";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-enterpayment-new"}, method = {RequestMethod.GET})
    public String paymentEnterpayment(ModelMap modelMap) {

        return "payment-enterpayment-new";

    }

}
