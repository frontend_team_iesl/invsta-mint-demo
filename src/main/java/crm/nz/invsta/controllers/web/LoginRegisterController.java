/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.beans.acc.api.Advisor;
import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.PersonDetailsBean;
import crm.nz.beans.acc.api.Register;
import crm.nz.components.RandomStringGenerator;
import crm.nz.beans.acc.api.Occupation;
import crm.nz.components.CommonMethods;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.mail.SendMail;
import crm.nz.repository.CommonRepository;
import crm.nz.repository.OutsideRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.security.SecurityService;
import crm.nz.services.DateTimeServiceImpl;
import crm.nz.table.bean.CsrfToken;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author TOSHIBA R830
 */
@Controller
public class LoginRegisterController implements CurrentUserService {

    @Autowired
    private CommonRepository repository;
    @Autowired
    private RandomStringGenerator generator;
    @Autowired
    private FormDetailsRepository formDetailsRepository;
    @Autowired
    private SecurityService securityService;
    @Autowired
    OutsideRepository outsideRepository;
    @Autowired
    private DateTimeServiceImpl dateTimeServiceImpl;
    @Autowired
    private SendMail sendmail;

    private static final Logger logger = LoggerFactory.getLogger(LoginRegisterController.class);

    @Trace
    @RequestMapping(value = {"", "/", "/haveAccount"}, method = RequestMethod.GET)
    public String empty() {
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/session"}, method = RequestMethod.GET)
    public String session(HttpServletRequest request, ModelMap modelMap,
            @RequestParam(value = "invalid", required = false) String invalid,
            @RequestParam(value = "expired", required = false) String expired,
            @RequestParam(value = "timeout", required = false) String timeout,
            @RequestParam(value = "auth-error", required = false) String autherror) {
        System.out.println("Request URL :: " + request.getRequestURL().toString() + "?" + request.getQueryString());
        System.out.println(request);
        System.out.println(request.getSession(false));
        if (invalid != null) {
            logout(repository);
            modelMap.addAttribute("title", "Invalid session");
            modelMap.addAttribute("message", "You have requested for an invalid session!");
        }
        if (expired != null) {
            logout(repository);
            modelMap.addAttribute("title", "Expired session");
            modelMap.addAttribute("message", "You have requested for an expired session!");
        }
        if (timeout != null) {
            logout(repository);
            modelMap.addAttribute("title", "Expired session");
            modelMap.addAttribute("message", "Your session has been expired due to timeout. "
                    + " No activity done from 15 mintues in this session.");
        }
        logger.debug("Request URL :: " + request.getRequestURL().toString() + " :: Method Execute Start Time= " + System.currentTimeMillis());
        return "login";
    }

    @Trace
    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request, ModelMap modelMap,
            @RequestParam(value = "authpass", required = false) String authpass,
            @RequestParam(value = "authfail", required = false) String authfail,
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {
        System.out.println("Request URL :: " + request.getRequestURL().toString());
        System.out.println(request);
        logger.debug("Request URL :: " + request.getRequestURL().toString() + " :: Method Execute Start Time= " + System.currentTimeMillis());
        if (error != null) {
            logout(repository);
            modelMap.addAttribute("title", "Invalid credential");
            modelMap.addAttribute("message", "Invalid username and password!");
        }
        if (logout != null) {
            logout(repository);
            modelMap.addAttribute("title", "Logout");
            modelMap.addAttribute("message", "You have been logged out successfully.");
        }
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (user.getSecret() != null && !user.getSecret().isEmpty() && !user.getVerification()) {
                return "use2fa";
            } else {
                return "redirect:/home";
            }
        }
        return "login";
    }

    @Trace
    @RequestMapping(value = {"/loggedin"}, method = RequestMethod.GET)
    public String loggedin() {
        return "redirect:/home-beneficiary-dashboard";
    }

    @Trace
    @RequestMapping(value = {"/personal-signup"}, method = RequestMethod.GET)
    public String personalsignup(ModelMap modelMap) {
        return "personal-signup";
    }

    @Trace
    @RequestMapping(value = {"/joint-account"}, method = RequestMethod.GET)
    public String jointAccount(ModelMap modelMap) {
        return "joint-account-new";
    }

    @Trace
    @RequestMapping(value = {"/minor-signup"}, method = RequestMethod.GET)
    public String minorSignup(ModelMap modelMap) {
        return "minor-signup";
    }

    @Trace
    @RequestMapping(value = {"/company-signup"}, method = RequestMethod.GET)
    public String companySignup(ModelMap modelMap,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "password", required = false) String password) {
//        if (email != null && password != null) {
        modelMap.addAttribute("email", email);
        modelMap.addAttribute("password", password);
        return "company-signup-new";
//        }
//        return "redirect:/signUpPath";
    }

    @Trace
    @RequestMapping(value = {"/trust-signup"}, method = RequestMethod.GET)
    public String trustSignup(ModelMap modelMap, @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "password", required = false) String password) {
        modelMap.addAttribute("email", email);
        modelMap.addAttribute("password", password);
        return "trust-signup-new";

    }

    @Trace
    @RequestMapping(value = {"/signUpPath"}, method = RequestMethod.POST)//it is for invite code
    public String register(ModelMap modelMap, @ModelAttribute Register register) throws SQLException, IOException {
        String token = generator.generate(30);
        register.setToken(token);
        // testing purpose 
        if ("wheeler.hamse@iiron.us".equalsIgnoreCase(register.getEmail())) {
            List<Occupation> occupation = formDetailsRepository.getOccupation();
            modelMap.addAttribute("occupation", occupation);
            switch (register.getType()) {
                case "JOINT_ACCOUNT":
                    return "joint-account-new";
                case "MINOR_ACCOUNT":
                    return "minor-signup";
                case "COMPANY_ACCOUNT":
                    return "company-signup-new";
                case "TRUST_ACCOUNT":
                    return "trust-signup-new";
                default:
                    break;
            }
        } else {
            int result = repository.register(register);
            if (result == 1) {
                securityService.autologin(register.getEmail(), register.getPassword());
            }
        }
        return "redirect:/signUpPath";
    }

    @Trace
    @RequestMapping(value = {"/signUpPath"}, method = RequestMethod.GET)//it is for invite code
    public String signUpPath(ModelMap modelMap) throws SQLException, IOException {
        SecuredUser user = getCurrentUser();
        List<Advisor> advisories = formDetailsRepository.getAdvisories();
        modelMap.addAttribute("advisories", advisories);
        List<Occupation> occupation = formDetailsRepository.getOccupation();
        modelMap.addAttribute("occupation", occupation);
        List<BankCodeBean> allBankCode = repository.getAllBankCode();
        modelMap.addAttribute("allBankCode", allBankCode);
        if (user != null && user.getToken() != null && user.getToken().length() == 30) {
            final String token = user.getToken();
            modelMap.addAttribute("token", token);
            PersonDetailsBean person = formDetailsRepository.getPersonDetailByToken(token);
            if (person != null && person.getReg_type() != null && "INDIVIDUAL_ACCOUNT".equalsIgnoreCase(person.getReg_type())) {
                modelMap.addAttribute("person", person);
                return "personal-signup";
            } else if (person != null && person.getReg_type() != null && "MINOR_ACCOUNT".equalsIgnoreCase(person.getReg_type())) {
                modelMap.addAttribute("person", person);
                return "minor-signup";
            } else {
                JointDetailBean joint = formDetailsRepository.getJointDetailByToken(token);
                if (joint != null && joint.getReg_type() != null && "JOINT_ACCOUNT".equalsIgnoreCase(joint.getReg_type())) {
                    modelMap.addAttribute("joint", joint);
                    return "joint-account-new";
                } else {
                    CompanyDetailBean company = formDetailsRepository.getCompanyDetailByToken(token);
                    if (company != null && company.getReg_type() != null && "COMPANY_ACCOUNT".equalsIgnoreCase(company.getReg_type())) {
                        modelMap.addAttribute("company", company);
                        return "company-signup-new";
                    } else if (company != null && company.getReg_type() != null && "TRUST_ACCOUNT".equalsIgnoreCase(company.getReg_type())) {
                        modelMap.addAttribute("company", company);
                        return "trust-signup-new";
                    }
                }
                return "signup";
            }
        } else {
            return "signup";
        }
    }

    @Trace
    @RequestMapping(value = {"/signUpPath-{token}"}, method = RequestMethod.GET)//it is from link
    public String verifyNew1(ModelMap modelMap,
            @PathVariable(value = "token") String tkn) throws SQLException, IOException {
        final String token = tkn;
        modelMap.addAttribute("token", token);
        Register register = formDetailsRepository.getRegisterDetailsByToken(token);
        securityService.autologin(register.getEmail(), register.getPassword());
        return "redirect:/signUpPath";
    }

    @Trace
    @RequestMapping(value = {"/forgetPassword"}, method = RequestMethod.POST)//it is from link
    public String forgetPasswor(HttpServletRequest request, ModelMap modelMap,
            @RequestParam("hiddenEmail") String hiddenEmail
    //            @RequestParam("token") String tok
    ) throws SQLException, IOException {
        SecuredUser usera = getCurrentUser();
        List<Email> mail = formDetailsRepository.getEmail(hiddenEmail);
        if (mail == null || mail.isEmpty()) {
            String message = "This Email is not registered";
            modelMap.addAttribute("message", message);
            return "login";
        } else {
            String tok = formDetailsRepository.getTokenbyEmail(hiddenEmail);
//            String token = generator.generate(30);
//            String csrf = generator.generate(24);
            CsrfToken csrfToken = new CsrfToken();
//            csrfToken.setCsrf(csrf);
            csrfToken.setToken(tok);
            Date date = dateTimeServiceImpl.now();
            Login bean = outsideRepository.getUserInfobyToken(tok);
//            String hiddenEmail = bean.getUsername();
            bean.setToken(tok);
            if (bean != null && tok.length() == 30) {
//                sendmail.sendResetPasswordMail(hiddenEmail, bean.getUsername(), bean.getId(), csrfToken, requestedDomain(request));
//                Object session =  request.getSession();
                String a = CommonMethods.time_format1.format(date);
                formDetailsRepository.TimeValidate(a, bean, modelMap);
                modelMap.addAttribute("hiddenEmail", hiddenEmail);
                String message = "We have send you a reset password link at your email";
                modelMap.addAttribute("message", message);

//              return "create-password";
                return "login";
//              return "redirect:/login";
            } else {
                modelMap.addAttribute("message", "Email is not valid");
                return "login";
            }
        }
    }

    @Trace
    @RequestMapping(value = {"/createPassword"}, method = RequestMethod.POST)//it is from link
    public String createPasswor(ModelMap modelMap, @RequestParam("token") String token,
             @RequestParam("password") String password) throws SQLException, IOException {
//           SecuredUser user = getCurrentUser();
//            String tok = formDetailsRepository.getTokenbyEmail(hiddenEmail);
//            Login bean = outsideRepository.getUserInfobyToken(tok);
        Login bean = outsideRepository.getUserInfobyToken(token);
        String Email = bean.getUsername();
        String id = bean.getId();
        String message = "your password is successfully change";
        formDetailsRepository.updatedPassword(Email, password, id);
        modelMap.addAttribute("message", message);
        return "login";
    }

    @Trace
    @RequestMapping(value = {"/resetpwd"}, method = RequestMethod.GET)//it is from link
    public String forgetPasswor1(HttpServletRequest request, ModelMap modelMap,
            @RequestParam("token") String token
    //            @RequestParam("token") String tok
    ) throws SQLException, IOException {

//        String token = generator.generate(30);
        String csrf = generator.generate(24);
        CsrfToken csrfToken = new CsrfToken();
        csrfToken.setCsrf(csrf);
        csrfToken.setToken(token);
//            String tok = formDetailsRepository.getTokenbyEmail(hiddenEmail);
//        Login bean = outsideRepository.getUserInfobyToken(tok);
//            String hiddenEmail = bean.getUsername();
//            bean.setToken(tok);
//            if (bean != null && tok.length() == 30) {
//                  sendmail.sendResetPasswordMail(hiddenEmail, bean.getUsername(), bean.getUser_id(),csrfToken, requestedDomain(request));
//                  String message = "We have send you a reset password link at your email";          
        modelMap.addAttribute("token", token);
// 
//return "create-password";
        return "new-password";
    }
//             String message = "your not a Registered user";          
//                  modelMap.addAttribute("message", message);
//            return "login";
}
//    }
