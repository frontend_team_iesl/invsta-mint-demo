/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.security.CurrentUserService;
import crm.nz.services.GetDataService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PaymentControllerDB implements CurrentUserService {

    @Autowired
    private GetDataService dataService;
    
    @Trace
    @RequestMapping(value = {"/payment-enterpayment", "/enterpayment"}, method = {RequestMethod.GET})
    public String enterpayment(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-enterpayment";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-regularmethod", "/regularmethod"}, method = {RequestMethod.GET})
    public String regularmethod(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-regularmethod";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-banktransfer", "/banktransfer"}, method = {RequestMethod.GET})
    public String bankTransfer(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-banktransfer";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-maketransfer", "/maketransfer"}, method = {RequestMethod.GET})
    public String makeTransfer(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-maketransfer";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-recipient", "/recipient"}, method = {RequestMethod.GET})
    public String recipient(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-recipient";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/payment-review", "/review"}, method = {RequestMethod.GET})
    public String review(ModelMap modelMap,
            @RequestParam(value = "id", required = false) String UserId,
            @RequestParam(value = "bi", required = false) String bi) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            UserId = dataService.getUserId(user, UserId);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", UserId);
            modelMap.addAttribute("bi", bi);
            return "payment-review";
        }
        return "redirect:/login";
    }

}
