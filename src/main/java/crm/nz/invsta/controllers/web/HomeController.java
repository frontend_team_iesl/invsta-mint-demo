/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import crm.nz.beans.acc.api.Notification;
import crm.nz.repository.BeneficiaryRepository;
import crm.nz.repository.NotificationRepository;
import crm.nz.repository.PortfolioRepository;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.SecuredUser;
import datadog.trace.api.Trace;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController implements CurrentUserService {

    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private BeneficiaryRepository beneficiaryRepository;

    @Trace
    @RequestMapping(value = {"/welcome"}, method = {RequestMethod.GET})
    public String welcome(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (user.getSecret() != null && !user.getSecret().isEmpty() && !user.getVerification()) {
                return "use2fa";
            } else {
                modelMap.addAttribute("user", user);
                return "redirect:/home";
            }
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home"}, method = {RequestMethod.GET})
    public String home(ModelMap modelMap) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("user", user);
            if ("ADMIN".equalsIgnoreCase(user.getRole())) {
                return "redirect:/admin-dashboard";
            } else if ("ADVISOR".equalsIgnoreCase(user.getRole())) {
                return "redirect:/advisor-dashboard";
            } else if ("BENEFICIARY".equalsIgnoreCase(user.getRole())) {
                List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
                modelMap.addAttribute("allNotification", allNotification);
                return "beneficiary-dashboard-db";
            } else if ("PENDING_USER".equalsIgnoreCase(user.getRole())) {
                return "redirect:/signUpPath";
            } else if ("SUBMISSION_COMPANY".equalsIgnoreCase(user.getRole()) || "SUBMISSION_TRUST".equalsIgnoreCase(user.getRole())) {
                SecurityContextHolder.getContext().setAuthentication(null);
                return "login";
            }
            return "home";
        }
        return "redirect:/login";
    }

    @Trace
    @RequestMapping(value = {"/home-{page}"}, method = {RequestMethod.GET})
    public String age(ModelMap modelMap, @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "userName", required = false) String userName,
            @PathVariable(value = "page") String page) {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            if (id == null || id != null && id.isEmpty()) {
                id = user.getUser_id();
                userName = user.getUsername();
            }
            List<Notification> allNotification = notificationRepository.getAllNotification(user.getUser_id());
            modelMap.addAttribute("allNotification", allNotification);
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("id", id);
            modelMap.addAttribute("username", userName);

            return page;
        }
        return "redirect:/login";
    }
}
