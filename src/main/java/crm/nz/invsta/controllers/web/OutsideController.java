/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.invsta.controllers.web;

import com.amazonaws.services.s3.model.S3Object;
import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.beans.acc.api.Occupation;
import crm.nz.components.FileMethods;
import crm.nz.lambda.repo.FormDetailsRepository;
import crm.nz.mail.SendMail;
import crm.nz.repository.OutsideRepository;
import crm.nz.repository.OutsideRepositoryImp;
import crm.nz.security.CurrentUserService;
import crm.nz.table.bean.DocumentBean;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author innovative002
 */
@Controller
public class OutsideController implements CurrentUserService {

    @Autowired
    OutsideRepository outsideRepository;
    @Autowired
    FileMethods fileMethods;
    @Autowired
    FormDetailsRepository formDetailsRepository;
    @Autowired
    private SendMail sendmail;

    @RequestMapping(value = {"/home-outside-joint-account", "/outside-joint-account"}, method = RequestMethod.GET)
    public String getJoint(ModelMap modelMap, @RequestParam(value = "token", required = true) String token) {
        JointDetailBean jointHolderDetailsByToken = outsideRepository.getJointHolderDetailsByToken(token);
        List<Occupation> occupation = formDetailsRepository.getOccupation();
        modelMap.addAttribute("occupation", occupation);
        if (jointHolderDetailsByToken == null) {
            modelMap.addAttribute("message", "Data is unavailable");
            return "outside-joint-account";
        }
        modelMap.addAttribute("jointHolder", jointHolderDetailsByToken);
        return "outside-joint-account";
    }

    @RequestMapping(value = {"/home-outside-company-account", "/outside-company-account"}, method = RequestMethod.GET)
    public String getCompany(ModelMap modelMap, @RequestParam(value = "token", required = true) String token) {
        CompanyDetailBean companyHolderDetailsByToken = outsideRepository.getCompanyTrustHolderDetailsByToken(token);
        List<Occupation> occupation = formDetailsRepository.getOccupation();
        modelMap.addAttribute("occupation", occupation);
        if (companyHolderDetailsByToken == null) {
            modelMap.addAttribute("message", "Data is unavailable");
            return "outside-company-account";
        }
        modelMap.addAttribute("company", companyHolderDetailsByToken);
        return "outside-company-account";
    }

    @RequestMapping(value = {"/home-outside-trust-account", "/outside-trust-account"}, method = RequestMethod.GET)
    public String getTrust(ModelMap modelMap, @RequestParam(value = "token", required = true) String token) {
        CompanyDetailBean companyHolderDetailsByToken = outsideRepository.getCompanyTrustHolderDetailsByToken(token);
        List<Occupation> occupation = formDetailsRepository.getOccupation();
        modelMap.addAttribute("occupation", occupation);
        if (companyHolderDetailsByToken == null) {
            modelMap.addAttribute("message", "Data is unavailable");
            return "outside-trust-account";
        }
        modelMap.addAttribute("company", companyHolderDetailsByToken);
        return "outside-trust-account";
    }

    @RequestMapping(value = {"/save-document"}, method = RequestMethod.POST)
    public String viewerdescription(ModelMap model, @ModelAttribute DocumentBean bean) throws IOException {
        SecuredUser user = getCurrentUser();
        if (user != null) {
            bean.getUpload_path();
//            Date today = .today();
//            user.setCreated_ts(common.format2(today));
//            if (user.getUserFile() != null && !user.getUserFile().isEmpty()) {
//                MultipartFile multipartFile = user.getUserFile();
//                String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
//                String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename());
//                S3Object s3Object = fileMethods.storeTos3Pdf(multipartFile, user.getId(),name, ext);
//                String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
//                user.setUpload_path(url);
//
//            }
            if (bean.getDocument_file() != null && !bean.getDocument_file().isEmpty()) {
                MultipartFile multipartFile = bean.getDocument_file();
                String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                String name = FilenameUtils.removeExtension(multipartFile.getOriginalFilename());
                S3Object s3Object = fileMethods.storeTos3Pdf(multipartFile, bean.getId(), name, FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
                String url = s3Object.getObjectContent().getHttpRequest().getURI().toString();
                bean.setUpload_path(url);
            }
            outsideRepository.saveDocumentDetails(bean);
            List<DocumentBean> document = outsideRepository.getDocument();
//            System.out.println("lissssssssssssst--------------------------------------------------------");
//            List<DocumentBean>list = getDocument();
            model.addAttribute("document", document);
            model.addAttribute("info", user);
            return "document";
        } else {
            return "redirect:/joint-holder";
        }
    }

    @RequestMapping(value = {"/verifyEmail-{token}"}, method = RequestMethod.GET)
    public String deleteNotification(ModelMap modelMap,
            @PathVariable(value = "token") String token) throws SQLException {
        Login bean = outsideRepository.getUserInfobyToken(token);
//        sendmail.sendOnEmailVerificationMail(bean.getName(), bean.getUsername());

        return "login";
    }
}
