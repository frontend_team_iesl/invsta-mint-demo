/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.table.bean.DocumentBean;
import crm.nz.table.bean.Login;
import java.sql.Date;
//import crm.nz.table.bean.DocumentBean;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author innovative002
 */
@Repository
public class OutsideRepositoryImp implements OutsideRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BCryptPasswordEncoder encoder;

    @Override
    public JointDetailBean getJointHolderDetailsByToken(String token) {
        String Sql = "select * from `joint_account_details` where `token` ='" + token + "';";
        List<JointDetailBean> joint = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(JointDetailBean.class));
        return !joint.isEmpty() ? joint.get(joint.size() - 1) : null;

    }

    @Override
    public CompanyDetailBean getCompanyTrustHolderDetailsByToken(String token) {
        String Sql = "select * from `company_trust_details` where `token` ='" + token + "';";
        List<CompanyDetailBean> company = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(CompanyDetailBean.class));
        return !company.isEmpty() ? company.get(company.size() - 1) : null;

    }

    @Override
    public Login getUserInfobyToken(String token) {
        String Sql = "SELECT LM.*,B.Name name FROM register R inner join login_master LM on(LM.username=R.username) \n"
                + "inner join user_beneficiairy_relationship UBR on(UBR.user_id=LM.id)\n"
                + "inner join beneficiaries B on (B.Id=UBR.beneficiairy_id)\n"
                + " where R.token='" + token + "'";
        List<Login> list = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(Login.class));
        return !list.isEmpty() ? list.get(list.size() - 1) : null;

    }

    public void saveDocumentDetails(DocumentBean user) {
        String sql = "INSERT INTO `document` \n"
                + " (`name`,`document_file`,`upload_path`)\n"
                + " VALUES (?,?,'yes')\n";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user.getName());
            ps.setString(2, user.getUpload_path());
            return ps;
        });

//        KeyHolder holder = new GeneratedKeyHolder();
//        jdbcTemplate.update((java.sql.Connection con) -> {
//            PreparedStatement pstmnt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            pstmnt.setString(1, user.getName());
//            pstmnt.setString(2, user.getUpload_path());
////            pstmnt.setString(3, bea n.getUpload_path());
////            pstmnt.setString(4, bean.getCreated_by());
////            pstmnt.setString(5, bean.getCreated_ts());
////            pstmnt.setString(6, bean.getReport_id());
//            return pstmnt;
//        }, holder);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DocumentBean> getDocument() {
        String sql = " SELECT * FROM `document`";// where  `email` = ? \n";
        List<DocumentBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DocumentBean.class));
        return list;
    }
}
//@Repository
//public class uploadRepImpl implements uploadRepository {
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Override
//    public void upload(uploadbean bean) {
//        String sql = "INSERT INTO `upload` \n"
//                + " (`id`,`name`,`description`,`upload_path`)\n"
//                + " VALUES (?,?,?,?)\n";
//        KeyHolder holder = new GeneratedKeyHolder();
//        jdbcTemplate.update((java.sql.Connection con) -> {
//            PreparedStatement pstmnt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            pstmnt.setString(1, bean.getId());
//            pstmnt.setString(2, bean.getName());
//            pstmnt.setString(3, bean.getDescription());
//            pstmnt.setString(4, bean.getUpload_path());
//
//
//            return pstmnt;
//        }, holder);
//
//    }
//
//    @Override
//    public List<uploadbean> getfile() {
//        String sql = " SELECT * FROM `upload`  ";
//        List<uploadbean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(uploadbean.class));
//        return list;
//    }
//
//    @Override
//    public void updatuploaded(uploadbean bean) {
//String sql = " UPDATE `upload`\n"
//                + "SET `name` = ?, `description`= ? \n"
//                + "WHERE `id` = ?;";
//        jdbcTemplate.update(sql, new Object[]{bean.getName(), bean.getDescription(), bean.getId()});
//    }    
//}
