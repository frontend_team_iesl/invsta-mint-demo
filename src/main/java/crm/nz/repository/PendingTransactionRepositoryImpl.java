/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.lambda.repo.FormDetailsRepository;
import static crm.nz.repository.PendingInvestmentRepository.getPrice;
import static crm.nz.repository.PendingInvestmentRepository.insertNotifySql;
import static crm.nz.repository.PendingInvestmentRepository.invTranSql;
import static crm.nz.repository.PendingInvestmentRepository.updatetNotifySql;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class PendingTransactionRepositoryImpl implements PendingTransactionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
//id investmentcode, portfolioCode, amount, applicationId, type, status, userId, bankAccountId, 
//    beneficiaryId, created_ts, portfolioName, investmentType, regularAmount, selectfrequecy, paymentMethod, directDebitId

    @Override
    public void setPendingTransaction(PendingTransactionBean bean) {
        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            String table_id = "";

            PreparedStatement ps = con.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bean.getInvestedAmount());
            ps.setString(2, bean.getInvestmentcode());
            ps.setString(3, bean.getPortfolioCode());
            ps.setString(4, bean.getApplicationId());
            ps.setString(5, bean.getUserId());
            ps.setString(6, bean.getBeneficiaryId());
            ps.setString(7, bean.getBankAccountId());
            ps.setString(8, bean.getType());
            ps.setString(9, bean.getCreated_ts());
            ps.setString(10, bean.getPortfolioName());
            ps.setString(11, bean.getInvestmentType());
            ps.setString(12, bean.getRegularAmount());
            ps.setString(13, bean.getSelectfrequecy());
            ps.setString(14, bean.getPaymentMethod());
            ps.setString(15, bean.getDirectDebitId());
            ps.setString(16, bean.getStatus());
            ps.setString(17, bean.getUnits());
            ps.setString(18, bean.getUnitprice());
            ps.execute();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                table_id = generatedKeys.getString(1);
            }

            if ("PAYMENT_LATER".equalsIgnoreCase(bean.getStatus())) {
                PreparedStatement ps2 = con.prepareStatement(insertNotifySql);
                ps2.setString(1, bean.getUserId());
                ps2.setString(2, bean.getBeneficiaryId());
                ps2.setString(3, table_id);
                ps2.setString(4, "pending_transaction");
                ps2.setString(5, bean.getCreated_ts());
                ps2.setString(6, "TRANSACTION_PAYMENT");
                ps2.execute();

            }

        } catch (SQLException ex) {
            Logger.getLogger(PendingTransactionRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void setUpdateTransaction(PendingTransactionBean bean) {
        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            String table_id = "";
            System.out.println("bean--------->" + bean);
            jdbcTemplate.update(updateSql, new Object[]{bean.getStatus(), bean.getId()});
//            PreparedStatement ps = con.prepareStatement(updateSql, Statement.RETURN_GENERATED_KEYS);
//            ps.setString(1, bean.getStatus());
//            ps.setString(2, bean.getId());
//            System.out.println("updateSql------------>"+updateSql);
//            ps.execute();
//            ResultSet generatedKeys = ps.getGeneratedKeys();
//            if (generatedKeys.next()) {
//                table_id = generatedKeys.getString(1);
//            }

            if (!"".equals(bean.getNotifyId()) && bean.getNotifyId() != null && !bean.getNotifyId().isEmpty()) {
                PreparedStatement ps3 = con.prepareStatement(updatetNotifySql);
                ps3.setString(1, bean.getNotifyId());
                ps3.execute();
            }

        } catch (SQLException ex) {
            Logger.getLogger(PendingTransactionRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<PendingTransactionBean> getPendingTransaction() {
        String sql = "SELECT pt.id, investmentcode, portfolioCode, amount, pt.applicationId, (CASE when pt.type = 'WDW'Then 'withdrawal' ELSE  'Investment' END) as type,\n"
                + "  pt.status, userId, bankAccountId, beneficiaryId, pt.created_ts, investmentType, regularAmount, selectfrequecy, paymentMethod, directDebitId,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName FROM pending_transaction pt inner join beneficiaries b on\n"
                + " (b.Id=pt.beneficiaryId) left join portfolio p on(p.Code=pt.portfolioCode and p.status='Y')\n"
                + "  inner join login_master lm on(lm.id=pt.userId) where pt.status='Pending'; ";
        List<PendingTransactionBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingTransactionBean.class));
        return beneficiaryDetail;

    }

    @Override
    public List<PendingTransactionBean> getPendingsSellTransaction() {
        String sql = "SELECT pt.id, investmentcode, portfolioCode, amount, pt.applicationId,pt.type  as type,\n"
                + "                  pt.status, userId, bankAccountId, beneficiaryId, pt.created_ts, investmentType, regularAmount, selectfrequecy, paymentMethod,pt.units,pt.unitprice, directDebitId,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName FROM pending_transaction pt inner join beneficiaries b on\n"
                + "                 (b.Id=pt.beneficiaryId) inner join portfolio p on(p.Code=pt.portfolioCode and p.status='Y')\n"
                + "                  inner join login_master lm on(lm.id=pt.userId) where pt.status='Pending' and pt.type='WDW' order by pt.created_ts desc; ";
        List<PendingTransactionBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingTransactionBean.class));
        return beneficiaryDetail;

    }

    @Override
    public List<PendingInvestmentBean> getDirectDebitTransaction() {
        String sql = "select up.*,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName ,BAD.Bank bankCode,BAD.AccountName as bankAccountName,\n"
                + "BAD.Branch branch,BAD.Account as bankAccountNumber,BAD.Suffix as suffix,b.Id beneficiaryId\n"
                + "   from (\n"
                + "  SELECT pt.id, investmentcode, portfolioCode, amount investedAmount, pt.applicationId,\n"
                + "  CASE when pt.type = 'WDW' Then 'Withdrawal' ELSE  'Investment' END as type,\n"
                + "  pt.status, userId, bankAccountId, beneficiaryId, pt.created_ts, investmentType, regularAmount,\n"
                + "  selectfrequecy, paymentMethod, directDebitId ,'pending_transaction' tabletype,startDate,endDate FROM pending_transaction pt\n"
                + "  union all\n"
                + "  SELECT pI.id, investmentcode, portfolioCode,investedAmount amount, pI.applicationId,\n"
                + "  'Investment' as type,\n"
                + "  pI.status, userId, bankAccountId, beneficiaryId, pI.created_ts,\n"
                + "  investmentType, regularAmount, selectfrequecy, paymentMethod,\n"
                + "  directDebitId  ,'pending_investment' tabletype ,startDate,endDate from pending_investment pI ) up\n"
                + "  left join Bank_account_Detail BAD  on (BAD.id=up.bankAccountId)\n"
//                + "  left join bank_code BC  on (BAD.Bank=BC.bankCode)\n"
                + "  inner join beneficiaries b on (b.Id=up.beneficiaryId)\n"
                + "  inner join portfolio p on(p.Code=up.portfolioCode and p.status='Y')\n"
                + "  inner join login_master lm on(lm.id=up.userId) where up.status='Pending' and up.type='Investment'\n"
                + "  and paymentMethod='DEBIT' order by up.created_ts desc;";
        List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return beneficiaryDetail;

    }

    @Override
    public void cancelTransection(String id) {
        String setstatus = "UPDATE pending_transaction SET status = 'Cancel' WHERE id =?";
        jdbcTemplate.update(setstatus, new Object[]{id});

    }

    @Override
    public void acceptTransaction(String id) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            String sql = "SELECT pt.*,p.Name as name  FROM pending_transaction pt inner join portfolio p  on (p.code=pt.portfolioCode and p.status='Y') where pt.id= ?";
            List<PendingTransactionBean> pedndingTransection = jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean bean = pedndingTransection.get(0);

            List<PendingTransactionBean> pricebeans = jdbcTemplate.query(getPrice, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean pricebean = pricebeans.get(0);
            BigDecimal nav = new BigDecimal(pricebean.getAmount());
            BigDecimal amount = new BigDecimal(bean.getAmount());

            BigDecimal units = amount.divide(nav, 2, RoundingMode.HALF_UP);
            String displayType = "";
            if (bean.getType().equalsIgnoreCase("WDR")) {
                displayType = "withdrawal";
            } else {
                displayType = "Contribution";
            }

            PreparedStatement ps3 = con.prepareStatement(invTranSql, Statement.RETURN_GENERATED_KEYS);
            ps3.setString(1, bean.getInvestmentcode());
            ps3.setDate(2, new Date(new java.util.Date().getTime()));
            ps3.setString(3, "APP");
            ps3.setString(4, displayType);
            ps3.setString(5, bean.getType());
            ps3.setString(6, bean.getType());
            ps3.setString(7, bean.getPortfolioCode());
            ps3.setString(8, bean.getName());
            ps3.setString(9, units.toPlainString());
            ps3.setString(10, nav.toPlainString());
            ps3.setString(11, bean.getAmount());
            ps3.setDouble(12, 0.0);
            ps3.setDouble(13, 0.0);
            ps3.setDouble(14, 0.0);
            ps3.setDouble(15, 0.0);
            ps3.setDouble(16, 0.0);
            ps3.setDouble(17, 0.0);
            ps3.setDouble(18, 0.0);
            ps3.setDouble(19, 0.0);
            ps3.setString(20, "0");
            ps3.setString(21, "Provider");
            ps3.setString(22, "Other");
            ps3.setString(23, "Contribution");
            ps3.setString(24, "Application");
            ps3.executeUpdate();
//            ps3.close();
            String setstatus = "UPDATE pending_transaction SET status = 'Done' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{bean.getId()});

        } catch (SQLException ex) {
            System.out.println("" + ex);
        } finally {
            if (con != null) {
                try {
                    con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
