/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.CompanyDetailBean;
import crm.nz.beans.acc.api.JointDetailBean;
import crm.nz.table.bean.DocumentBean;
import crm.nz.table.bean.Login;
//import crm.nz.table.bean.DocumentBean;
import java.util.List;

/**
 *
 * @author innovative002
 */
public interface OutsideRepository {
    
    public JointDetailBean getJointHolderDetailsByToken(String token);
    
    public CompanyDetailBean getCompanyTrustHolderDetailsByToken(String token);
    
    public Login getUserInfobyToken(String token);
    
//    public void saveDocument(DocumentBean task);
    public void saveDocumentDetails(DocumentBean user);
    
    public List<DocumentBean> getDocument();
}

//public interface uploadRepository {
//
//     public void upload(uploadbean bean);
//
//    public List<uploadbean> getfile();
//        public void updatuploaded(uploadbean bean);

    
//    public List<uploadLinkBean> getReportsLink();
    

//    public void updateReport(String id);
//    
//    public void updateDocument(String id);
//     public void setDocumentation(uploadLinkBean beans);

//    public List<uploadLinkBean> getReports_Link(String link);

//    public void updateEditDocument(uploadbean bean);

    
//}