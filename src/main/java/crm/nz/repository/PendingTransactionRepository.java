/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import java.util.List;

/**
 *
 * @author TOSHIBA R830
 */
public interface PendingTransactionRepository {

//       id, investmentcode, portfolioCode, amount, applicationId, type, status, userId, bankAccountId, 
//               beneficiaryId, created_ts, portfolioName, investmentType, regularAmount, selectfrequecy, paymentMethod, directDebitId
    String insertSql = "INSERT INTO `pending_transaction`\n"
            + "(`amount`,\n"
            + "`investmentcode`,\n"
            + "`portfolioCode`,\n"
            + "`applicationId`,\n"
            + "`userId`,\n"
            + "`beneficiaryId`,\n"
            + "`bankAccountId`,\n"
            + "`type`,\n"
            + "`created_ts`,\n"
            + "`portfolioName`,\n"
            + "`investmentType`,\n"
            + "`regularAmount`,\n"
            + "`selectfrequecy`,\n"
            + "`paymentMethod`,\n"
            + "`directDebitId`,\n"
            + "`status`,\n"
            + "`units`,\n"
            + "`unitprice`)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
   String updateSql = "UPDATE `pending_transaction` SET `status` = ? WHERE `id` = ?;";
   
   
   String updatetNotifySql = "UPDATE `notification` SET `active` = 'N' WHERE `id` = ?;";

    String insertNotifySql = "INSERT INTO `notification`\n"
            + "(`user_id`,`beneficiaryId`,`table_id`,`table_name`,`created_ts`,`active`,`type`)\n"
            + "VALUES (?,?,?,?,?,'Y',?);";

    String invTranSql = "INSERT INTO investment_transaction\n"
            + "(InvestmentCode,\n"
            + "EffectiveDate,\n"
            + "Type,\n"
            + "TypeDisplayName,\n"
            + "SubType,\n"
            + "SubTypeDisplayName,\n"
            + "PortfolioCode,\n"
            + "PortfolioName,\n"
            + "Units,\n"
            + "Price,\n"
            + "Value,\n"
            + "Cash,\n"
            + "Tax,\n"
            + "Fee,\n"
            + "AccruedIncome,\n"
            + "CashClearing,\n"
            + "PaymentClearing,\n"
            + "PortfolioAccount,\n"
            + "TaxRebate,\n"
            + "isPending,\n"
            + "TransactionSourceType,\n"
            + "TransactionMethodType,\n"
            + "TransactionDisplayName,\n"
            + "TransactionTypeDescription)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    public void setPendingTransaction(PendingTransactionBean bean);
    
    public void setUpdateTransaction(PendingTransactionBean bean);

    public void acceptTransaction(String id);

    public List<PendingTransactionBean> getPendingTransaction();

    public List<PendingTransactionBean> getPendingsSellTransaction();
    
    public List<PendingInvestmentBean> getDirectDebitTransaction();

    public void cancelTransection(String id);
}
