/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.authorization.TestMintNZAPI;
import crm.nz.beans.acc.api.Address;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.FundBankAccountDetail;
import crm.nz.beans.acc.api.Identification;
import crm.nz.beans.acc.api.PIRRate;
import crm.nz.beans.acc.api.PhoneNumber;
import crm.nz.table.bean.Login;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author IESL
 */
@Repository
public class BeneficiaryRepositoryImpl implements BeneficiaryRepository {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private InvestmentRepository investmentRepository;

    @Override
    public void saveBeneficiary(Beneficiary bean) {
        String loginSQL = "INSERT INTO `beneficiaries`\n"
                + "(`Id`,`IrdNumber`,`CustomerNumber`,`AccountOf`,`Name`,`DateOfBirth`,`ClientAccountStartDate`,\n"
                + "`FirstName`,`LastName`,`AccessLevel`,`AMLEntityType`,`Status`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(loginSQL);
            ps.setLong(1, bean.getId());
            ps.setString(2, bean.getIrdNumber());
            ps.setString(3, bean.getCustomerNumber());
            ps.setString(4, bean.getAccountOf());
            ps.setString(5, bean.getName());
            ps.setString(6, bean.getDateOfBirth());
            ps.setString(7, bean.getClientAccountStartDate());
            ps.setString(8, bean.getFirstName());
            ps.setString(9, bean.getLastName());
            ps.setString(10, bean.getAccessLevel());
            ps.setString(11, bean.getAMLEntityType());
            ps.setString(12, bean.getStatus());
            return ps;
        });
        if (bean.getEmails() != null && !bean.getEmails().isEmpty()) {
            saveEmails(bean.getEmails());
        }
        if (bean.getPhoneNumbers() != null && !bean.getPhoneNumbers().isEmpty()) {
            savePhoneNumbers(bean.getPhoneNumbers());
        }
        if (bean.getAddresses() != null && !bean.getAddresses().isEmpty()) {
            saveAddresses(bean.getAddresses());
        }
        if (bean.getIdentification() != null && !bean.getIdentification().isEmpty()) {
            saveIdentifications(bean.getIdentification());
        }
        if (bean.getBankAccountDetail() != null && !bean.getBankAccountDetail().isEmpty()) {
            saveBankAccountDetails(bean.getBankAccountDetail());
        }
        if (bean.getPIRRates() != null && !bean.getPIRRates().isEmpty()) {
            savePIRRates(bean.getPIRRates());
        }
        if (bean.getInvestments() != null && !bean.getInvestments().isEmpty()) {
            investmentRepository.saveInvestments(bean.getInvestments());
        }
    }

    @Override
    public void updateProfile(Login login) {
        String loginSQL = "UPDATE `login_master` SET `userLogoPath` = ? WHERE `id` = ?;";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(loginSQL);
            ps.setString(1, login.getUserLogoPath());
            ps.setString(2, login.getUser_id());
            return ps;
        });

    }

    @Override
    public void saveBeneficiaries(List<Beneficiary> beneficiaryList) {
        String loginSQL = "INSERT INTO `beneficiaries`\n"
                + "(`Id`,`IrdNumber`,`CustomerNumber`,`AccountOf`,`Name`,`DateOfBirth`,`ClientAccountStartDate`,\n"
                + "`FirstName`,`LastName`,`AccessLevel`,`AMLEntityType`,`Status`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Beneficiary beneficiary = beneficiaryList.get(i);
                ps.setLong(1, beneficiary.getId());
                ps.setString(2, beneficiary.getIrdNumber());
                ps.setString(3, beneficiary.getCustomerNumber());
                ps.setString(4, beneficiary.getAccountOf());
                ps.setString(5, beneficiary.getName());
                ps.setString(6, beneficiary.getDateOfBirth());
                ps.setString(7, beneficiary.getClientAccountStartDate());
                ps.setString(8, beneficiary.getFirstName());
                ps.setString(9, beneficiary.getLastName());
                ps.setString(10, beneficiary.getAccessLevel());
                ps.setString(11, beneficiary.getAMLEntityType());
                ps.setString(12, beneficiary.getStatus());
            }

            @Override
            public int getBatchSize() {
                return beneficiaryList.size();
            }
        });
    }

    @Override
    public void savePhoneNumbers(List<PhoneNumber> phoneNumbers) {
        String loginSQL = "INSERT INTO `phone_numbers`\n"
                + "(`Country`,`CountryCode`,`Number`,`Type`,`Home`,`IsPrimary`, `BeneficiaryId`)\n"
                + "VALUES (?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PhoneNumber phoneNumber = phoneNumbers.get(i);
                ps.setString(1, phoneNumber.getCountry());
                ps.setString(2, phoneNumber.getCountryCode());
                ps.setString(3, phoneNumber.getNumber());
                ps.setString(4, phoneNumber.getType());
                ps.setString(5, phoneNumber.getHome());
                ps.setBoolean(6, phoneNumber.isIsPrimary());
                ps.setString(7, phoneNumber.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return phoneNumbers.size();
            }

        });

    }

    @Override
    public void saveEmails(List<Email> emails) {
        String loginSQL = "INSERT INTO `Emails`\n"
                + "(`Email`,`IsPrimary`,`BeneficiaryId`)\n"
                + "VALUES (?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Email email = emails.get(i);
                ps.setString(1, email.getEmail());
                ps.setBoolean(2, email.isIsPrimary());
                ps.setString(3, email.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return emails.size();
            }
        });
    }

    @Override
    public void saveBankAccountDetails(List<BankAccountDetail> details) {
        String loginSQL = "INSERT INTO `Bank_account_Detail`\n"
                + "(`AccountName`,`Bank`,`Branch`,`Account`,`Suffix`,`Currency`,`Status`,`Type`,`IsPrimary`,`BeneficiaryId`,`InvestmentCode`)\n"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                BankAccountDetail detail = details.get(i);
                ps.setString(1, detail.getAccountName());
                ps.setString(2, detail.getBank());
                ps.setString(3, detail.getBranch());
                ps.setString(4, detail.getAccount());
                ps.setString(5, detail.getSuffix());
                ps.setString(6, detail.getCurrency());
                ps.setString(7, detail.getStatus());
                ps.setString(8, detail.getType());
                ps.setBoolean(9, detail.isIsPrimary());
                ps.setString(10, detail.getBeneficiaryId());
                ps.setString(11, detail.getInvestmentCode());
            }

            @Override
            public int getBatchSize() {
                return details.size();
            }
        });
    }

    @Override
    public void saveIdentifications(List<Identification> identifications) {
        String loginSQL = "INSERT INTO `Identification`\n"
                + "(`IdentificationType`,`Number`,`ExpiryDate`,`BeneficiaryId`)\n"
                + "VALUES (?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Identification identification = identifications.get(i);
                ps.setString(1, identification.getIdentificationType());
                ps.setString(2, identification.getNumber());
                ps.setString(3, identification.getExpiryDate());
                ps.setString(4, identification.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return identifications.size();
            }
        });
    }

    @Override
    public void saveAddresses(List<Address> addresses) {
        String loginSQL = "INSERT INTO `address`\n"
                + "(`EffectiveDate`,`InEffectiveDate`,`AddressID`,`Country`,`CountryCode`,`Type`,\n"
                + "`AddressLine1`,`AddressLine2`,`AddressLine3`,`AddressLine4`,`City`,`Region`,`State`,\n"
                + "`PostalCode`,`FormattedAddress`,`IsPrimary`,`BeneficiaryId`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Address address = addresses.get(i);
                ps.setString(1, address.getEffectiveDate());
                ps.setString(2, address.getInEffectiveDate());
                ps.setString(3, address.getAddressID());
                ps.setString(4, address.getCountry());
                ps.setString(5, address.getCountryCode());
                ps.setString(6, address.getType());
                ps.setString(7, address.getAddressLine1());
                ps.setString(8, address.getAddressLine2());
                ps.setString(9, address.getAddressLine3());
                ps.setString(10, address.getAddressLine4());
                ps.setString(11, address.getCity());
                ps.setString(12, address.getRegion());
                ps.setString(13, address.getState());
                ps.setString(14, address.getPostalCode());
                ps.setString(15, address.getFormattedAddress());
                ps.setBoolean(16, address.isIsPrimary());
                ps.setString(17, address.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return addresses.size();
            }
        });
    }

    @Override
    public void savePIRRates(List<PIRRate> PIRRates) {
        String SQL = "INSERT INTO `PIRRates`\n"
                + "(`Rate`,\n"
                + "`EffectiveDate`,\n"
                + "`Status`,\n"
                + "`BeneficiaryId`)\n"
                + " VALUES (?, ?, ?, ?);";
        jdbcTemplate.batchUpdate(SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PIRRate pirRate = PIRRates.get(i);
                ps.setDouble(1, pirRate.getRate());
                ps.setString(2, pirRate.getEffectiveDate());
                ps.setString(3, pirRate.getStatus());
                ps.setString(4, pirRate.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return PIRRates.size();
            }
        });
    }

    @Override
    public void saveUsernames(List<Email> Emails) {
        String uniqueEmailsSQL = "SELECT E.* FROM Emails E \n"
                + "WHERE E.email NOT IN(SELECT E.email FROM Emails E INNER JOIN login_master LM ON(E.Email = LM.username))\n"
                + "GROUP BY E.Email;";
        List<Email> uniqueEmails = jdbcTemplate.query(uniqueEmailsSQL, new BeanPropertyRowMapper(Email.class));
        String insertSQL = " INSERT INTO `login_master`\n"
                + "(`username`, `password`, `role`, `created_ts`, `active`)\n"
                + "VALUES (?, ?, ?, now(), 'Y') ";
        jdbcTemplate.batchUpdate(insertSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Email uniqueEmail = uniqueEmails.get(i);
                ps.setString(1, uniqueEmail.getEmail());
                String password = uniqueEmail.getEmail().split("@")[0];
                ps.setString(2, encoder.encode(password));
                ps.setString(3, "BENEFICIARY");
            }

            @Override
            public int getBatchSize() {
                return uniqueEmails.size();
            }
        });
        String beneficiaryEmailsSQL = "SELECT LM.id UserId, username Email, E.BeneficiaryId FROM login_master LM\n"
                + "inner join Emails E ON (LM.username = E.Email)\n"
                + "where LM.username not in (SELECT LM.username FROM login_master LM \n"
                + "inner join user_beneficiairy_relationship UBR ON (LM.id = UBR.user_id))"
                + "group by UserId, Email, BeneficiaryId";
        List<Email> beneficiaryEmails = jdbcTemplate.query(beneficiaryEmailsSQL, new BeanPropertyRowMapper(Email.class));
        String insertSQL2 = " INSERT INTO `user_beneficiairy_relationship`\n"
                + "(`user_name`,`beneficiairy_id`,`user_id`)\n"
                + "VALUES (?,?,?);";
        jdbcTemplate.batchUpdate(insertSQL2, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Email email = beneficiaryEmails.get(i);
                ps.setString(1, email.getEmail());
                ps.setString(2, email.getBeneficiaryId());
                ps.setString(3, email.getUserId());
            }

            @Override
            public int getBatchSize() {
                return beneficiaryEmails.size();
            }
        });
    }

    @Override
    public Beneficiary getBeneficiary(String BeneficiaryId) {
        String sql = "SELECT b.*,pir.Rate as pirRate FROM beneficiaries b\n"
                + "  left JOIN PIRRates pir ON (b.Id = pir.BeneficiaryId)\n"
                + " where b.Id = '" + BeneficiaryId + "' group by b.id;";
        List<Beneficiary> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Beneficiary.class));
        return !query.isEmpty() ? query.get(0) : null;
    }

    @Override
    public List<Beneficiary> getBeneficiaryDetails(String UserId, String BeneficiaryId, String AdvisorId) {
        String sql = " SELECT  B.*,UBR.user_id UserId, count(*) total FROM login_master LM\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (UBR.user_id =LM.id AND (LM.role = 'BENEFICIARY' or LM.role = 'ADVISOR'))\n"
                + " INNER JOIN beneficiaries B ON (B.Id = UBR.beneficiairy_id)  WHERE B.Id = UBR.beneficiairy_id\n";
        if (UserId != null) {
            sql += " AND UBR.user_id = " + UserId;
        }
        if (AdvisorId != null) {
            sql += " AND UBR.advisor_id = " + AdvisorId;
        }
        if (BeneficiaryId != null) {
            sql += " AND B.Id =" + BeneficiaryId;
        }
        sql += " group by UBR.user_id";
        System.out.println("Advisro----" + sql);
        List<Beneficiary> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Beneficiary.class));
        return beneficiaryDetail;
    }

    @Override
    public List<PhoneNumber> getPhoneNumbers(String UserId, String BeneficiaryId) {
        String sql = " SELECT B.*,UBR.user_id UserId  FROM phone_numbers B\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "WHERE B.BeneficiaryId = UBR.beneficiairy_id and  B.IsPrimary = '1' ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += "  and B.BeneficiaryId = " + BeneficiaryId;
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(PhoneNumber.class));
    }

    @Override
    public List<Address> getAddresses(String UserId, String BeneficiaryId) {
        String sql = " SELECT B.*,UBR.user_id UserId  FROM address B\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE B.BeneficiaryId = UBR.beneficiairy_id and  B.IsPrimary = '1'\n";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and B.BeneficiaryId = " + BeneficiaryId;
        }
        System.out.println("---" + sql);
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Address.class));
    }

    @Override
    public List<Email> getEmails(String UserId, String BeneficiaryId) {
        String sql = "SELECT lm.*,UBR.user_id UserId,UBR.user_name  FROM user_beneficiairy_relationship UBR\n"
                + "                 INNER JOIN login_master lm ON (lm.id = UBR.user_id)\n"
                + "              where   lm.id = UBR.user_id";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and UBR.beneficiairy_id = " + BeneficiaryId;
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Email.class));
    }

    @Override
    public List<Identification> getIdentifications(String UserId, String BeneficiaryId) {
        String sql = " SELECT B.*,UBR.user_id UserId  FROM Identification B\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "WHERE B.BeneficiaryId = UBR.beneficiairy_id ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and B.BeneficiaryId = " + BeneficiaryId;
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Identification.class));
    }

    @Override
    public List<BankAccountDetail> getBankAccountDetails(String UserId, String BeneficiaryId) {
//        String sql = "SELECT B.* FROM Bank_account_Detail B\n"
//                + " INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
//                + " WHERE B.BeneficiaryId = UBR.beneficiairy_id and  B.IsPrimary = '1'\n";
        String sql = "SELECT B.*, bc.bankName as BankName FROM Bank_account_Detail B \n"
                + "                INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " left join bank_code bc on(bc.bankCode=B.Bank) "
                //                + "                    INNER JOIN pendingbankAccount cod ON (concat( B.bank,B.Branch) =  concat(cod.Bank,cod.Branch) and cod.action='accept')\n"
                + "                 WHERE B.BeneficiaryId = UBR.beneficiairy_id and  B.IsPrimary = '1'";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and B.BeneficiaryId = " + BeneficiaryId;
        }
        sql += " group by Bank, Branch, Account";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(BankAccountDetail.class));

    }

    @Override
    public FundBankAccountDetail getFundBankAccountDetails(String PortfolioCode) {
        String sql = "SELECT * FROM Fund_bank_account_detail";
        if (PortfolioCode != null) {
            sql += " where portfolioCode = " + PortfolioCode;
        }
        List<FundBankAccountDetail> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FundBankAccountDetail.class));
        return query.size() > 0 ? query.get(0) : null;
    }

    @Override
    public List<BankAccountDetail> getBankAccountsByInvestmentCode(String InvestmentCode) {
        String sql = "SELECT B.*,BC.bankName BankName FROM Bank_account_Detail B\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " LEFT JOIN bank_code BC on (BC.bankCode=B.Bank)\n"
                + "WHERE B.BeneficiaryId IN(SELECT BeneficiaryId FROM investments where Code=? group by BeneficiaryId) group by Account";
        return jdbcTemplate.query(sql, new Object[]{InvestmentCode}, new BeanPropertyRowMapper(BankAccountDetail.class));
    }

    @Override
    public List<Beneficiary> getBeneficiaryDetailsByInvestmentCode(String InvestmentCode) {
        String sql = " SELECT B.*,UBR.user_id UserId FROM beneficiaries B\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (B.Id = UBR.beneficiairy_id)\n"
                + " WHERE B.Id IN(SELECT BeneficiaryId FROM investments where Code=? group by BeneficiaryId) group by Id;";
        List<Beneficiary> beneficiaryDetail = jdbcTemplate.query(sql, new Object[]{InvestmentCode}, new BeanPropertyRowMapper(Beneficiary.class));
        return beneficiaryDetail;
    }

}
