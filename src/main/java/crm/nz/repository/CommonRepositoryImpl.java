/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.ApiCall;
import crm.nz.beans.acc.api.BankAccountDetail;
import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.Beneficiary;
import crm.nz.beans.acc.api.CountryISOCodeBean;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.Register;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class CommonRepositoryImpl implements CommonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private BCryptPasswordEncoder encoder;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CommonRepositoryImpl.class);

    @Override
    public Login findLoginByUsername(String username) {

        String loginSQL = " SELECT LM.* FROM (\n"
                + "  SELECT LM.*,B.Name,I.Code investmentCode,ubr.beneficiairy_id beneficiairyId, count(*) total FROM login_master LM\n"
                + "  INNER JOIN user_beneficiairy_relationship ubr ON (ubr.user_id =LM.id AND (LM.role = 'BENEFICIARY' or LM.role = 'ADVISOR'))\n"
                + "  INNER JOIN beneficiaries B ON (B.Id=ubr.beneficiairy_id) \n"
                + "  LEFT JOIN investments I ON (I.BeneficiaryId= ubr.beneficiairy_id) group by ubr.user_id\n"
                + "  union all\n"
                + "  SELECT LM.*, 'Admin' Name,'' as investmentCode,'' as beneficiairyId,  0 total FROM login_master LM  WHERE LM.role = 'ADMIN'\n"
                + "  ) LM\n"
                + "  WHERE LM.username =? \n"
                + " limit 1;";
        List<Login> loginList = jdbcTemplate.query(loginSQL, new Object[]{username}, (ResultSet rs, int rowNum) -> {
            Login user = new Login();
            int total = rs.getInt("total");
            user.setUsername(rs.getString("username"));
            user.setUser_id(rs.getString("id"));
            user.setPassword(rs.getString("password"));
            user.setCreated_ts(rs.getString("created_ts"));
            if (total > 1) {
                user.setRole("ADVISOR");
            } else {
                user.setRole(rs.getString("role"));
            }
            user.setActive(rs.getString("active"));
            user.setSecret(rs.getString("secret"));
            user.setApplication_id(rs.getString("application_id"));
            user.setName(rs.getString("Name"));
            user.setUserLogoPath(rs.getString("userLogoPath"));
            user.setInvestmentCode(rs.getString("investmentCode"));
            user.setBeneficiairyId(rs.getString("beneficiairyId"));
            return user;
        });
        if (!loginList.isEmpty()) {
            return loginList.get(loginList.size() - 1);
        } else {
            String pendingRegSQL = " select * from `register` where `status` IN ('SUBMISSION' ,'PENDING') and `username` = ?";
            List<Login> pendingRegList = jdbcTemplate.query(pendingRegSQL, new Object[]{username}, (ResultSet rs, int rowNum) -> {
                Login user = new Login();
                user.setUsername(rs.getString("username"));
                user.setUser_id(rs.getString("id"));
                user.setPassword(rs.getString("password"));
                user.setCreated_ts(rs.getString("created_ts"));
                user.setToken(rs.getString("token"));
                user.setStep(rs.getString("step"));
                String regType = rs.getString("reg_type");
                String status = rs.getString("status");
                if ("TRUST_ACCOUNT".equalsIgnoreCase(regType) && "SUBMISSION".equalsIgnoreCase(status)) {
                    user.setRole("SUBMISSION_TRUST");
                } else if ("COMPANY_ACCOUNT".equalsIgnoreCase(regType) && "SUBMISSION".equalsIgnoreCase(status)) {
                    user.setRole("SUBMISSION_COMPANY");
                } else {
                    user.setRole("PENDING_USER");
                }

                user.setActive("Y");
                return user;
            });
            return !pendingRegList.isEmpty() ? pendingRegList.get(pendingRegList.size() - 1) : null;
        }

    }

    @Override
    public int register(Register bean) {
        String regSql = " INSERT INTO `register`\n"
                + "(`username`,\n"
                + "`password`,\n"
                + "`token`,\n"
                + "`status`,\n"
                + "`created_ts`,\n"
                + "`step`,\n"
                + "`reg_type`,\n"
                + "`raw_password`"
                + ")\n"
                + "VALUES\n"
                + "(?, ?, ?, ?, ?, ?, ?, ?); ";
        KeyHolder keyholder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps0 = con.prepareStatement(regSql, Statement.RETURN_GENERATED_KEYS);
                ps0.setString(1, bean.getEmail());
                String encryptedPassword = encoder.encode(bean.getPassword());
                ps0.setString(2, encryptedPassword);
                ps0.setString(3, bean.getToken());
                ps0.setString(4, "PENDING");
                ps0.setDate(5, new Date(new java.util.Date().getTime()));
                ps0.setString(6, "0");
                ps0.setString(7, bean.getType());
                ps0.setString(8, bean.getPassword());
                return ps0;
            }
        }, keyholder);
        long key = keyholder.getKey().longValue();
        if ("JOINT_ACCOUNT".equalsIgnoreCase(bean.getType())) {
            String sql = "INSERT INTO `joint_account_details` (`reg_id` ,`active`) VALUES (?,'Y');";
            return jdbcTemplate.update(sql, new Object[]{key});
        } else if ("COMPANY_ACCOUNT".equalsIgnoreCase(bean.getType())) {
            String sql = "INSERT INTO `company_trust_details` (`reg_id` ,`active`) VALUES (?,'Y');";
            return jdbcTemplate.update(sql, new Object[]{key});
        } else if ("TRUST_ACCOUNT".equalsIgnoreCase(bean.getType())) {
            String sql = "INSERT INTO `company_trust_details` (`reg_id` ,`active`) VALUES (?,'Y');";
            return jdbcTemplate.update(sql, new Object[]{key});
        } else if ("MINOR_ACCOUNT".equalsIgnoreCase(bean.getType())) {
            String sql = "INSERT INTO `individual_person_detail` (`reg_id` ,`active`) VALUES (?,'Y');";
            return jdbcTemplate.update(sql, new Object[]{key});
        } else if ("INDIVIDUAL_ACCOUNT".equalsIgnoreCase(bean.getType())) {
            String sql = "INSERT INTO `individual_person_detail` (`reg_id` ,`active`) VALUES (?,'Y');";
            return jdbcTemplate.update(sql, new Object[]{key});
        }
        return 0;

    }

    @Override
    public void insertLogin(List<Email> emailList) {
        String sql = " INSERT INTO `login_master` (`username`, `password`, `active`, `created_ts`, `beneficiary_id`)\n"
                + "VALUES (?, ?, 'Y', now(), ?) ";
        String password = "$2a$11$iUhUS3PPnFqwMsCLcY37PO2Tn5MlVLkvRI9hgcnMzgpY6B/JOwXBS";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Email login = emailList.get(i);
                ps.setString(1, login.getEmail());
                ps.setString(2, password);
                ps.setString(3, login.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return emailList.size();
            }
        });

        String sql2 = "INSERT INTO `role_user_master`\n" + "(`role`, `beneficiary_id`)\n" + "VALUES ('BENEFICIARY', ?);";
        jdbcTemplate.batchUpdate(sql2, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Email login = emailList.get(i);
                ps.setString(1, login.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return emailList.size();
            }
        });

    }

    @Override
    public void updateSecretCode(SecuredUser user) {

        String setstatus = "UPDATE login_master SET secret = ? WHERE id =?";
        jdbcTemplate.update(setstatus, new Object[]{user.getSecret(), user.getUser_id()});
    }

//    @Override
//    public CountryISOCodeBean getCountryISOCode(String country) {
//        country = country.trim();
//        String codeSQL = "SELECT * FROM CountryISO WHERE `country`=?;";
//        List<CountryISOCodeBean> countryISOCodeBean = jdbcTemplate.query(codeSQL,new Object[]{country},  new BeanPropertyRowMapper(CountryISOCodeBean.class));
//        return !countryISOCodeBean.isEmpty() ? countryISOCodeBean.get(countryISOCodeBean.size() - 1) : null;
//    }
    @Override
    public CountryISOCodeBean getCountryISOCode(String country) {
        String sql = "SELECT * FROM CountryISO where country = '" + country.trim() + "';";
        List<CountryISOCodeBean> countryISOCodeBean = jdbcTemplate.query(sql, new BeanPropertyRowMapper(CountryISOCodeBean.class));
        return !countryISOCodeBean.isEmpty() ? countryISOCodeBean.get(0) : null;
    }

    @Override
    public void removeSecretCode(SecuredUser user) {

        String setstatus = "UPDATE login_master SET secret = '' WHERE id =?";
        jdbcTemplate.update(setstatus, new Object[]{user.getUser_id()});

    }

    @Override
    public List<CountryISOCodeBean> getAllCountryCode() {
        String sql = "SELECT * FROM CountryISO ;";
        List<CountryISOCodeBean> countryISOCodeBean = jdbcTemplate.query(sql, new BeanPropertyRowMapper(CountryISOCodeBean.class));
        return countryISOCodeBean;
    }
    
    @Override
    public List<BankCodeBean> getAllBankCode() {
        String sql = "SELECT * FROM bank_code ;";
        List<BankCodeBean> bankCodeBean = jdbcTemplate.query(sql, new BeanPropertyRowMapper(BankCodeBean.class));
        return bankCodeBean;
    }

    @Override
    public void saveApiCall(ApiCall apiCall) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            con.setAutoCommit(false);
            String inactiveSQL = " UPDATE `ApiCalls` SET `Active` = 'N' WHERE `InvestmentCode` = ? and `PortfolioCode` = ? ";
            PreparedStatement ps = con.prepareStatement(inactiveSQL);
            ps.setString(1, apiCall.getInvestmentCode());
            ps.setString(2, apiCall.getPortfolioCode());
            ps.executeUpdate();
            String sql = "INSERT INTO `ApiCalls`\n"
                    + "(`ActionName`, `CalledDate`, `Active`, `InvestmentCode`)\n"
                    + "VALUES\n"
                    + "(?, ?, 'Y', ?);";
            PreparedStatement ps2 = con.prepareStatement(sql);
            ps2.setString(1, apiCall.getActionName());
            ps2.setString(2, apiCall.getCalledDate());
            ps2.setString(3, apiCall.getInvestmentCode());
            ps2.setString(4, apiCall.getPortfolioCode());
            ps2.executeUpdate();
            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void saveApiCall(List<ApiCall> apiCalls) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            con.setAutoCommit(false);
            String inactiveSQL = " UPDATE `ApiCalls` SET `Active` = 'N' WHERE `InvestmentCode` = ? and `PortfolioCode` = ?  and `ActionName` = ?";
            PreparedStatement ps1 = con.prepareStatement(inactiveSQL);
            for (ApiCall apiCall : apiCalls) {
                ps1.setString(1, apiCall.getInvestmentCode());
                ps1.setString(2, apiCall.getPortfolioCode());
                ps1.setString(3, apiCall.getActionName());
                ps1.addBatch();
            }
            ps1.executeBatch();
            String sql = "INSERT INTO `ApiCalls`\n"
                    + "(`ActionName`, `CalledDate`, `Active`, `InvestmentCode`,`PortfolioCode`)\n"
                    + "VALUES\n"
                    + "(?, ?, 'Y', ?, ?);";
            PreparedStatement ps2 = con.prepareStatement(sql);
            for (ApiCall apiCall : apiCalls) {
                ps2.setString(1, apiCall.getActionName());
                ps2.setString(2, apiCall.getCalledDate());
                ps2.setString(3, apiCall.getInvestmentCode());
                ps2.setString(4, apiCall.getPortfolioCode());
                ps2.addBatch();
            }
            ps2.executeBatch();
            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
