/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.lambda.repo.FormDetailsRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class PendingInvestmentRepositoryImpl implements PendingInvestmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setPendingInvestment(PendingInvestmentBean bean) {
        try {
            String table_id = "";
            Connection con = jdbcTemplate.getDataSource().getConnection();
//            if ("".equals(bean.getId()) && bean.getId().isEmpty()) {
            PreparedStatement ps = con.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bean.getInvestmentName());
            ps.setString(2, bean.getInvestedAmount());
            ps.setString(3, bean.getInvestmentCode());
            ps.setString(4, bean.getPortfolioCode());
            ps.setString(5, bean.getApplicationId());
            ps.setString(6, bean.getUserId());
            ps.setString(7, bean.getBeneficiaryId());
            ps.setString(8, bean.getInvestmentId());
            ps.setString(9, bean.getBankAccountId());
            ps.setString(10, bean.getCreated_ts());
            ps.setString(11, bean.getInvestmentType());
            ps.setString(12, bean.getRegularAmount());
            ps.setString(13, bean.getSelectfrequecy());
            ps.setString(14, bean.getPaymentMethod());
            ps.setString(15, bean.getDirectDebitId());
            ps.setString(16, bean.getStatus());
            ps.setString(17, bean.getStartDate());
            ps.setString(18, bean.getEndDate());
            ps.execute();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                table_id = generatedKeys.getString(1);
            }

            //        user_id`,`beneficiaryId`,`table_id`,`table_name`,`created_ts`,`active
            if ("PAYMENT_LATER".equalsIgnoreCase(bean.getStatus())) {
                PreparedStatement ps2 = con.prepareStatement(insertNotifySql);
                ps2.setString(1, bean.getUserId());
                ps2.setString(2, bean.getBeneficiaryId());
                ps2.setString(3, table_id);
                ps2.setString(4, "pending_investment");
                ps2.setString(5, bean.getCreated_ts());
                ps2.setString(6, "INVESTMENT_PAYMENT");
                ps2.execute();

            }
//            }

            if ("DEBIT".equalsIgnoreCase(bean.getPaymentMethod())) {
                PreparedStatement ps1 = con.prepareStatement(insertDebitSql);
                ps1.setString(1, bean.getApplicationId());
                ps1.setString(2, bean.getBeneficiaryId());
                ps1.setString(3, bean.getInvestmentId());
                ps1.setString(4, bean.getBankAccountId());
                ps1.setString(5, bean.getSelectfrequecy());
                ps1.setString(6, bean.getInvestedAmount());
                ps1.setString(7, bean.getComments());
                ps1.setString(8, bean.getStartDate());
                ps1.setString(9, bean.getEndDate());
                ps1.setString(10, bean.getInvestmentType());
                ps1.setDate(11, new Date(new java.util.Date().getTime()));
                ps1.setString(12, bean.getDirectDebitId());
                ps1.execute();
            }

            if (!"".equals(bean.getNotifyId()) && bean.getNotifyId() != null && !bean.getNotifyId().isEmpty()) {
                PreparedStatement ps3 = con.prepareStatement(updatetNotifySql);
                ps3.setString(1, bean.getNotifyId());
                ps3.execute();

            }
        } catch (SQLException ex) {
            Logger.getLogger(PendingInvestmentRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void setUpdateInvestment(PendingInvestmentBean bean) {
        try {
            String table_id = "";
            Connection con = jdbcTemplate.getDataSource().getConnection();
//            if ("".equals(bean.getId()) && bean.getId().isEmpty()) {
            PreparedStatement ps = con.prepareStatement(updateSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bean.getStatus());
            ps.setString(2, bean.getId());
            ps.execute();
//                ResultSet generatedKeys = ps.getGeneratedKeys();
//                if (generatedKeys.next()) {
//                    table_id = generatedKeys.getString(1);
//                }

            if (!"".equals(bean.getNotifyId()) && bean.getNotifyId() != null && !bean.getNotifyId().isEmpty()) {
                PreparedStatement ps3 = con.prepareStatement(updatetNotifySql);
                ps3.setString(1, bean.getNotifyId());
                ps3.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(PendingInvestmentRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<PendingInvestmentBean> getPendingInvestment() {
        String sql = "select up.*,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName \n"
                + "   from (\n"
                + "  SELECT pt.id, investmentcode, portfolioCode, amount investedAmount, pt.applicationId, \n"
                + "  CASE when pt.type = 'WDW' Then 'Withdrawal' ELSE  'Investment' END as type,\n"
                + "  pt.status, userId, bankAccountId, beneficiaryId, pt.created_ts, investmentType, regularAmount,\n"
                + "  selectfrequecy, paymentMethod, directDebitId ,'pending_transaction' tabletype FROM pending_transaction pt \n"
                + "  union all \n"
                + "  SELECT pI.id, investmentcode, portfolioCode,investedAmount, pI.applicationId,\n"
                + "  'Investment' as type,\n"
                + "  pI.status, userId, bankAccountId, beneficiaryId, pI.created_ts, \n"
                + "  investmentType, regularAmount, selectfrequecy, paymentMethod,\n"
                + "  directDebitId  ,'pending_investment' tabletype  from pending_investment pI ) up\n"
                + "  inner join beneficiaries b on (b.Id=up.beneficiaryId)\n"
                + "  inner join portfolio p on(p.Code=up.portfolioCode and p.status='Y')\n"
                + "  inner join login_master lm on(lm.id=up.userId) where up.status='Pending'  and up.type='Investment'  order by up.created_ts desc ";
        List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return beneficiaryDetail;

    }

    @Override
    public void cancelInvestment(String id) {
        String setstatus = "UPDATE pending_investment SET status = 'Cancel' WHERE id =?";
        jdbcTemplate.update(setstatus, new Object[]{id});

    }

    @Override
    public List<PendingInvestmentBean> getAdvisorPendingInvestment(String id) {
        String sql = " SELECT pt.*,lm.username as name,b.Name as beneficiaryName FROM pending_investment pt\n"
                + "inner join beneficiaries b on (b.Id=pt.beneficiaryId)\n"
                + "inner join login_master lm on(lm.id=pt.userId)\n"
                + "inner join user_beneficiairy_relationship ubr on(lm.id = ubr.user_id)\n"
                + "where pt.status='Pending'\n"
                + "and ubr.advisor_id = " + id;
        List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return beneficiaryDetail;

    }

    @Override
    public int codeGenerator() {

        String sql = "INSERT INTO `codeGenerator` (`code`) VALUES (?);";

        KeyHolder keyholder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps0 = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps0.setString(1, "A");

                return ps0;
            }
        }, keyholder);
        int key = keyholder.getKey().intValue();
        return key;
    }

    @Override
    public void acceptInvestmentRequest(String id) {
        Connection con = null;
        try {
            String sql = "SELECT * FROM pending_investment where id= ?";
            con = jdbcTemplate.getDataSource().getConnection();

            List<PendingInvestmentBean> beneficiaryDetail = jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(PendingInvestmentBean.class));
            PendingInvestmentBean bean = beneficiaryDetail.get(0);
            int codeGenerator = codeGenerator();
            String code = "INV" + codeGenerator;

            String sql1 = "SELECT p.Name fundName FROM portfolio p where code= ? and `status`='Y'";
            List<PortfolioDetail> fundDetails = jdbcTemplate.query(sql1, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PortfolioDetail.class));
            PortfolioDetail fundDetail = fundDetails.get(0);
            String fundName = fundDetail.getFundName();
            PreparedStatement ps = con.prepareStatement(insertcode, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, "0");
            ps.setString(2, "0");
            ps.setString(3, "0");
            ps.setString(4, "0");
            ps.setString(5, "0");
            ps.setString(6, "0");
            ps.setString(7, "0");
            ps.setString(8, "0");
            ps.setString(9, "0");
            ps.setString(10, "0");
            ps.setString(11, code);
            ps.setString(12, "Unit_Registry");
            ps.setString(13, bean.getBeneficiaryId());
            ps.setString(14, bean.getBankAccountId());
            ps.executeUpdate();
            ps.close();

            List<PendingTransactionBean> pricebeans = jdbcTemplate.query(getPrice, new Object[]{bean.getPortfolioCode()}, new BeanPropertyRowMapper(PendingTransactionBean.class));
            PendingTransactionBean pricebean = pricebeans.get(0);
            BigDecimal nav = new BigDecimal(pricebean.getAmount());
            BigDecimal amount = new BigDecimal(bean.getInvestedAmount());

            BigDecimal units = amount.divide(nav, 2, RoundingMode.HALF_UP);
            PreparedStatement ps2 = con.prepareStatement(insertInvestmentHolding, Statement.RETURN_GENERATED_KEYS);
            ps2.setString(1, code);
            ps2.setDate(2, new Date(new java.util.Date().getTime()));
            ps2.setString(3, bean.getPortfolioCode());
            ps2.setString(4, fundName);
            ps2.setString(5, "INVESTED");
            ps2.setString(6, units.toPlainString());
            ps2.setString(7, "0");
            ps2.setString(8, nav.toPlainString());
            ps2.setString(9, bean.getInvestedAmount());
            ps2.setDouble(10, 0.0);
            ps2.setDouble(11, 0.0);
            ps2.setString(12, bean.getInvestedAmount());
            ps2.setDouble(13, 0.0);
            ps2.setDouble(14, 0.0);
            ps2.setString(15, bean.getBeneficiaryId());
            ps2.executeUpdate();
            ps2.close();

            PreparedStatement ps3 = con.prepareStatement(invTranSql, Statement.RETURN_GENERATED_KEYS);
            ps3.setString(1, code);
            ps3.setDate(2, new Date(new java.util.Date().getTime()));
            ps3.setString(3, "APP");
            ps3.setString(4, "Contribution");
            ps3.setString(5, "INV");
            ps3.setString(6, "Contribution");
            ps3.setString(7, bean.getPortfolioCode());
            ps3.setString(8, fundName);
            ps3.setString(9, units.toPlainString());
            ps3.setString(10, nav.toPlainString());
            ps3.setString(11, bean.getInvestedAmount());
            ps3.setDouble(12, 0.0);
            ps3.setDouble(13, 0.0);
            ps3.setDouble(14, 0.0);
            ps3.setDouble(15, 0.0);
            ps3.setDouble(16, 0.0);
            ps3.setDouble(17, 0.0);
            ps3.setDouble(18, 0.0);
            ps3.setDouble(19, 0.0);
            ps3.setString(20, "0");
            ps3.setString(21, "Provider");
            ps3.setString(22, "Other");
            ps3.setString(23, "Contribution");
            ps3.setString(24, "Application");
            ps3.executeUpdate();
//            ps3.close();
            String setstatus = "UPDATE pending_investment SET status = 'Done' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{bean.getId()});

        } catch (SQLException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } catch (NullPointerException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } finally {
            if (con != null) {
                try {
                    con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}
