///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.as.mint.lambda;
//
//import com.amazonaws.services.lambda.runtime.LambdaLogger;
//
//public class BeneficiaryRepo {
//
//    LambdaLogger logger;
//
//    public BeneficiaryRepo(LambdaLogger logger) {
//        this.logger = logger;
//    }
//    public void saveBeneficiary(Connection con, Beneficiary bean) throws SQLException {
//        String loginSQL = "INSERT INTO `beneficiaries`\n"
//                + "(`Id`,`IrdNumber`,`CustomerNumber`,`AccountOf`,`Name`,`DateOfBirth`,`ClientAccountStartDate`,\n"
//                + "`FirstName`,`LastName`,`AccessLevel`,`AMLEntityType`,`Status`)\n"
//                + "VALUES\n"
//                + "(?,?,?,?,?,?,?,?,?,?,?,?);";
//        logger.log(loginSQL);
//        PreparedStatement ps = con.prepareStatement(loginSQL, Statement.RETURN_GENERATED_KEYS);
//        ps.setLong(1, bean.getId());
//        ps.setString(2, bean.getIrdNumber());
//        ps.setString(3, bean.getCustomerNumber());
//        ps.setString(4, bean.getAccountOf());
//        ps.setString(5, bean.getName());
//        ps.setString(6, bean.getDateOfBirth());
//        ps.setString(7, bean.getClientAccountStartDate());
//        ps.setString(8, bean.getFirstName());
//        ps.setString(9, bean.getLastName());
//        ps.setString(10, bean.getAccessLevel());
//        ps.setString(11, bean.getAMLEntityType());
//        ps.setString(12, bean.getStatus());
//        ps.executeUpdate();
//
//        if (bean.getEmails()
//                != null && !bean.getEmails().isEmpty()) {
//            saveEmails(con, bean.getEmails());
//        }
//
//        if (bean.getPhoneNumbers()
//                != null && !bean.getPhoneNumbers().isEmpty()) {
//            savePhoneNumbers(con, bean.getPhoneNumbers());
//        }
//
//        if (bean.getAddresses()
//                != null && !bean.getAddresses().isEmpty()) {
//            saveAddresses(con, bean.getAddresses());
//        }
//
//        if (bean.getIdentification()
//                != null && !bean.getIdentification().isEmpty()) {
//            saveIdentifications(con, bean.getIdentification());
//        }
//
//        if (bean.getBankAccountDetail()
//                != null && !bean.getBankAccountDetail().isEmpty()) {
//            saveBankAccountDetails(con, bean.getBankAccountDetail());
//        }
//
//        if (bean.getPIRRates()
//                != null && !bean.getPIRRates().isEmpty()) {
//            savePIRRates(con, bean.getPIRRates());
//        }
//
//        if (bean.getInvestments()
//                != null && !bean.getInvestments().isEmpty()) {
//            saveInvestments(con, bean.getInvestments());
//        }
//    }
//
//    public void saveBeneficiaries(Connection con, List<Beneficiary> beneficiaryList) throws SQLException {
//        String loginSQL = "INSERT INTO `beneficiaries`\n"
//                + "(`Id`,`IrdNumber`,`CustomerNumber`,`AccountOf`,`Name`,`DateOfBirth`,`ClientAccountStartDate`,\n"
//                + "`FirstName`,`LastName`,`AccessLevel`,`AMLEntityType`,`Status`)\n"
//                + "VALUES\n"
//                + "(?,?,?,?,?,?,?,?,?,?,?,?);";
//        logger.log(loginSQL);
//
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (Beneficiary beneficiary : beneficiaryList) {
//            ps.setLong(1, beneficiary.getId());
//            ps.setString(2, beneficiary.getIrdNumber());
//            ps.setString(3, beneficiary.getCustomerNumber());
//            ps.setString(4, beneficiary.getAccountOf());
//            ps.setString(5, beneficiary.getName());
//            ps.setString(6, beneficiary.getDateOfBirth());
//            ps.setString(7, beneficiary.getClientAccountStartDate());
//            ps.setString(8, beneficiary.getFirstName());
//            ps.setString(9, beneficiary.getLastName());
//            ps.setString(10, beneficiary.getAccessLevel());
//            ps.setString(11, beneficiary.getAMLEntityType());
//            ps.setString(12, beneficiary.getStatus());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//    }
//
//    public void savePhoneNumbers(Connection con, List<PhoneNumber> phoneNumbers) throws SQLException {
//        String loginSQL = "INSERT INTO `phone_numbers`\n"
//                + "(`Country`,`CountryCode`,`Number`,`Type`,`Home`,`IsPrimary`, `BeneficiaryId`)\n"
//                + "VALUES (?,?,?,?,?,?,?);";
//        logger.log(loginSQL);
//
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < phoneNumbers.size(); i++) {
//            PhoneNumber phoneNumber = phoneNumbers.get(i);
//            ps.setString(1, phoneNumber.getCountry());
//            ps.setString(2, phoneNumber.getCountryCode());
//            ps.setString(3, phoneNumber.getNumber());
//            ps.setString(4, phoneNumber.getType());
//            ps.setString(5, phoneNumber.getHome());
//            ps.setBoolean(6, phoneNumber.isIsPrimary());
//            ps.setString(7, phoneNumber.getBeneficiaryId());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//    }
//
//    public void saveEmails(Connection con, List<Email> emails) throws SQLException {
//        String loginSQL = "INSERT INTO `Emails`\n"
//                + "(`Email`,`IsPrimary`,`BeneficiaryId`)\n"
//                + "VALUES (?,?,?);";
//        logger.log(loginSQL);
//
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < emails.size(); i++) {
//            Email email = emails.get(i);
//            ps.setString(1, email.getEmail());
//            ps.setBoolean(2, email.isIsPrimary());
//            ps.setString(3, email.getBeneficiaryId());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//
//    }
//
//    public void saveBankAccountDetails(Connection con, List<BankAccountDetail> details) throws SQLException {
//        String loginSQL = "INSERT INTO `Bank_account_Detail`\n"
//                + "(`AccountName`,`Bank`,`Branch`,`Account`,`Suffix`,`Currency`,`Status`,`Type`,`IsPrimary`,`BeneficiaryId`,`InvestmentCode`)\n"
//                + "VALUES (?,?,?,?,?,?,?,?,?,?,?);";
//        logger.log(loginSQL);
//
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < details.size(); i++) {
//            BankAccountDetail detail = details.get(i);
//            ps.setString(1, detail.getAccountName());
//            ps.setString(2, detail.getBank());
//            ps.setString(3, detail.getBranch());
//            ps.setString(4, detail.getAccount());
//            ps.setString(5, detail.getSuffix());
//            ps.setString(6, detail.getCurrency());
//            ps.setString(7, detail.getStatus());
//            ps.setString(8, detail.getType());
//            ps.setBoolean(9, detail.isIsPrimary());
//            ps.setString(10, detail.getBeneficiaryId());
//            ps.setString(11, detail.getInvestmentCode());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//
//    }
//
//    public void saveIdentifications(Connection con, List<Identification> identifications) throws SQLException {
//        String loginSQL = "INSERT INTO `Identification`\n"
//                + "(`IdentificationType`,`Number`,`ExpiryDate`,`BeneficiaryId`)\n"
//                + "VALUES (?,?,?,?);";
//        logger.log(loginSQL);
//
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < identifications.size(); i++) {
//            Identification identification = identifications.get(i);
//            ps.setString(1, identification.getIdentificationType());
//            ps.setString(2, identification.getNumber());
//            ps.setString(3, identification.getExpiryDate());
//            ps.setString(4, identification.getBeneficiaryId());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//
//    }
//
//    public void saveAddresses(Connection con, List<Address> addresses) throws SQLException {
//        String loginSQL = "INSERT INTO `address`\n"
//                + "(`EffectiveDate`,`InEffectiveDate`,`AddressID`,`Country`,`CountryCode`,`Type`,\n"
//                + "`AddressLine1`,`AddressLine2`,`AddressLine3`,`AddressLine4`,`City`,`Region`,`State`,\n"
//                + "`PostalCode`,`FormattedAddress`,`IsPrimary`,`BeneficiaryId`)\n"
//                + "VALUES\n"
//                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < addresses.size(); i++) {
//            Address address = addresses.get(i);
//            ps.setString(1, address.getEffectiveDate());
//            ps.setString(2, address.getInEffectiveDate());
//            ps.setString(3, address.getAddressID());
//            ps.setString(4, address.getCountry());
//            ps.setString(5, address.getCountryCode());
//            ps.setString(6, address.getType());
//            ps.setString(7, address.getAddressLine1());
//            ps.setString(8, address.getAddressLine2());
//            ps.setString(9, address.getAddressLine3());
//            ps.setString(10, address.getAddressLine4());
//            ps.setString(11, address.getCity());
//            ps.setString(12, address.getRegion());
//            ps.setString(13, address.getState());
//            ps.setString(14, address.getPostalCode());
//            ps.setString(15, address.getFormattedAddress());
//            ps.setBoolean(16, address.isIsPrimary());
//            ps.setString(17, address.getBeneficiaryId());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//
//    }
//
//    public void savePIRRates(Connection con, List<PIRRate> PIRRates) throws SQLException {
//        String loginSQL = "INSERT INTO `PIRRates`\n"
//                + "(`Rate`,\n"
//                + "`EffectiveDate`,\n"
//                + "`Status`,\n"
//                + "`BeneficiaryId`)\n"
//                + " VALUES (?, ?, ?, ?);";
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//        for (int i = 0; i < PIRRates.size(); i++) {
//            PIRRate pirRate = PIRRates.get(i);
//            ps.setDouble(1, pirRate.getRate());
//            ps.setString(2, pirRate.getEffectiveDate());
//            ps.setString(3, pirRate.getStatus());
//            ps.setString(4, pirRate.getBeneficiaryId());
//            ps.addBatch();
//        }
//        ps.executeBatch();
//
//    }
//
//    public void saveInvestments(Connection con, List<Investment> investments) throws SQLException {
//        String insertSQL = "INSERT INTO `investments`\n"
//                + "(`InvestmentName`,`Status`,`ExternalReference`,`FeeGroup`,`AdvisorRate`,`Greeting`,`InvestmentStrategyName`,\n"
//                + "`Contributions`,`Withdrawals`,`Earnings`,`TotalValue`,`ReinvestedDistributions`,`CashDistributions`,`Tax`,`TaxPaid`,\n"
//                + "`InvestmentReturnRate`,`InvestmentStrategyBand`,`RegistryAccountId`,`ContractDate`,`Code`,`InvestmentType`, `BeneficiaryId`)\n"
//                + "VALUES\n"
//                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
//        PreparedStatement ps = con.prepareStatement(insertSQL);
//        for (int i = 0; i < investments.size(); i++) {
//            Investment Inv = investments.get(i);
//            ps.setString(1, Inv.getInvestmentName());
//            ps.setString(2, Inv.getStatus());
//            ps.setString(3, Inv.getExternalReference());
//            ps.setString(4, Inv.getFeeGroup());
//            ps.setString(5, Inv.getAdvisorRate());
//            ps.setString(6, Inv.getGreeting());
//            ps.setString(7, Inv.getInvestmentStrategyName());
//            ps.setDouble(8, Inv.getContributions());
//            ps.setDouble(9, Inv.getWithdrawals());
//            ps.setDouble(10, Inv.getEarnings());
//            ps.setDouble(11, Inv.getTotalValue());
//            ps.setDouble(12, Inv.getReinvestedDistributions());
//            ps.setDouble(13, Inv.getCashDistributions());
//            ps.setDouble(14, Inv.getTax());
//            ps.setDouble(15, Inv.getTaxPaid());
//            double rate = Inv.getInvestmentReturnRate() != null ? Inv.getInvestmentReturnRate() : 0.0;
//            ps.setDouble(16, rate);
//            ps.setDouble(17, Inv.getInvestmentStrategyBand());
//            ps.setLong(18, Inv.getRegistryAccountId());
//            ps.setString(19, Inv.getContractDate());
//            ps.setString(20, Inv.getCode());
//            ps.setString(21, Inv.getInvestmentType());
//            ps.setString(22, Inv.getBeneficiaryId());
//            ps.addBatch();
//        }
//
//    }
//
//    public void saveUsernames(Connection con) throws SQLException {
//        String uniqueEmailsSQL = "SELECT E.* FROM Emails E \n"
//                + "WHERE E.email NOT IN(SELECT E.email FROM Emails E INNER JOIN login_master LM ON(E.Email = LM.username))\n"
//                + "GROUP BY E.Email;";
//        List<Email> uniqueEmails = null;
//
//        String loginSQL = " INSERT INTO `login_master`\n"
//                + "(`username`, `password`, `role`, `created_ts`, `active`)\n"
//                + "VALUES (?, ?, ?, now(), 'Y') ";
//        PreparedStatement ps = con.prepareStatement(loginSQL);
//
//        for (int i = 0; i < uniqueEmails.size(); i++) {
//            Email uniqueEmail = uniqueEmails.get(i);
//            ps.setString(1, uniqueEmail.getEmail());
//            String password = uniqueEmail.getEmail().split("@")[0];
//            ps.setString(2, encoder.encode(password));
//            ps.setString(3, "BENEFICIARY");
//            ps.addBatch();
//        }
//
//        String beneficiaryEmailsSQL = "SELECT LM.id UserId, username Email, E.BeneficiaryId FROM login_master LM\n"
//                + "inner join Emails E ON (LM.username = E.Email)\n"
//                + "where LM.username not in (SELECT LM.username FROM login_master LM \n"
//                + "inner join user_beneficiairy_relationship UBR ON (LM.id = UBR.user_id))"
//                + "group by UserId, Email, BeneficiaryId";
//        List<Email> beneficiaryEmails = null;
//
//        String insertSQL2 = " INSERT INTO `user_beneficiairy_relationship`\n"
//                + "(`user_name`,`beneficiairy_id`,`user_id`)\n"
//                + "VALUES (?,?,?);";
//        PreparedStatement ps2 = con.prepareStatement(insertSQL2);
//        for (int i = 0; i < uniqueEmails.size(); i++) {
//            Email email = beneficiaryEmails.get(i);
//            ps.setString(1, email.getEmail());
//            ps.setString(2, email.getBeneficiaryId());
//            ps.setString(3, email.getUserId());
//            ps.addBatch();
//        }
//    }
//    @Override
//    public Beneficiary getBeneficiary(String BeneficiaryId
//    ) {
//        String sql = "SELECT * FROM beneficiaries where Id = '" + BeneficiaryId + "';";
//        List<Beneficiary> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Beneficiary.class
//        ));
//        return !query.isEmpty() ? query.get(0) : null;
//    }
//
//    @Override
//    public List<Address> getAddresses(String UserId, String BeneficiaryId
//    ) {
//        String sql = "SELECT * FROM address where BeneficiaryId = '" + BeneficiaryId + "';";
//
//        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Address.class
//        ));
//    }
//
//    @Override
//    public List<Email> getEmails(String UserId, String BeneficiaryId
//    ) {
//        String sql = "SELECT * FROM Emails where BeneficiaryId = '" + BeneficiaryId + "';";
//
//        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Email.class
//        ));
//
//    }
//
//    @Override
//    public List<Identification> getIdentifications(String UserId, String BeneficiaryId
//    ) {
//        String sql = "SELECT * FROM Identification where BeneficiaryId = '" + BeneficiaryId + "';";
//
//        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(Identification.class
//        ));
//    }
//
//    @Override
//    public List<BankAccountDetail> getBankAccountDetails(String UserId, String BeneficiaryId
//    ) {
//        String sql = "SELECT B.* FROM Bank_account_Detail B\n"
//                + "INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
//                + "WHERE UBR.user_id = ?";
//
//        return jdbcTemplate.query(sql, new Object[]{UserId}, new BeanPropertyRowMapper(BankAccountDetail.class
//        ));
//
//    }
//
//    @Override
//    public List<BankAccountDetail> getBankAccountsByInvestmentCode(String InvestmentCode
//    ) {
//        String sql = "SELECT B.* FROM Bank_account_Detail B\n"
//                + "INNER JOIN user_beneficiairy_relationship UBR ON (B.BeneficiaryId = UBR.beneficiairy_id)\n"
//                + "WHERE B.BeneficiaryId = (SELECT BeneficiaryId FROM investments where Code=? group by BeneficiaryId);";
//
//        return jdbcTemplate.query(sql, new Object[]{InvestmentCode}, new BeanPropertyRowMapper(BankAccountDetail.class
//        ));
//    }
//
//    @Override
//    public List<PhoneNumber> getPhoneNumbers(String UserId, String BeneficiaryId
//    ) {
//        String sql = "SELECT * FROM phone_numbers where BeneficiaryId = '" + BeneficiaryId + "';";
//
//        return jdbcTemplate.query(sql, new BeanPropertyRowMapper(PhoneNumber.class
//        ));
//    }
//
//    @Override
//    public List<Beneficiary> getBeneficiaryDetails(String UserId, String BeneficiaryId
//    ) {
//        String sql = " SELECT B.* FROM beneficiaries B\n"
//                + " INNER JOIN user_beneficiairy_relationship UBR ON (B.Id = UBR.beneficiairy_id)\n"
//                + " WHERE B.Id = UBR.beneficiairy_id\n";
//        if (UserId != null) {
//            sql += "UBR.user_id =" + UserId;
//        }
//        if (UserId != null) {
//            sql += "B.Id =" + UserId;
//
//        }
//        List<Beneficiary> beneficiaryDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Beneficiary.class
//        ));
//        return beneficiaryDetail;
//    }
//
//    @Override
//    public List<Beneficiary> getBeneficiaryDetailsByInvestmentCode(String InvestmentCode
//    ) {
//        String sql = " SELECT B.* FROM beneficiaries B\n"
//                + " INNER JOIN user_beneficiairy_relationship UBR ON (B.Id = UBR.beneficiairy_id)\n"
//                + " WHERE B.Id IN(SELECT BeneficiaryId FROM investments where Code=? group by BeneficiaryId)";
//        List<Beneficiary> beneficiaryDetail = jdbcTemplate.query(sql, new Object[]{InvestmentCode}, new BeanPropertyRowMapper(Beneficiary.class
//        ));
//        return beneficiaryDetail;
//
//    }
//}
