/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.Investment;
import crm.nz.beans.acc.api.InvestmentHolding;
import crm.nz.beans.acc.api.InvestmentPerformance;
import crm.nz.beans.acc.api.InvestmentTransection;
import crm.nz.beans.acc.api.PortfolioPrice;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author IESL
 */
@Repository
public class InvestmentRepositoryImpl implements InvestmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveInvestments(List<Investment> investments) {
        String insertInvestment = "INSERT INTO `investments`\n"
                + "(`InvestmentName`,`Status`,`ExternalReference`,`FeeGroup`,`AdvisorRate`,`Greeting`,`InvestmentStrategyName`,\n"
                + "`Contributions`,`Withdrawals`,`Earnings`,`TotalValue`,`ReinvestedDistributions`,`CashDistributions`,`Tax`,`TaxPaid`,\n"
                + "`InvestmentReturnRate`,`InvestmentStrategyBand`,`RegistryAccountId`,`ContractDate`,`Code`,`InvestmentType`, `BeneficiaryId`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(insertInvestment, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Investment Inv = investments.get(i);
                ps.setString(1, Inv.getInvestmentName());
                ps.setString(2, Inv.getStatus());
                ps.setString(3, Inv.getExternalReference());
                ps.setString(4, Inv.getFeeGroup());
                ps.setString(5, Inv.getAdvisorRate());
                ps.setString(6, Inv.getGreeting());
                ps.setString(7, Inv.getInvestmentStrategyName());
                ps.setDouble(8, Inv.getContributions());
                ps.setDouble(9, Inv.getWithdrawals());
                ps.setDouble(10, Inv.getEarnings());
                ps.setDouble(11, Inv.getTotalValue());
                ps.setDouble(12, Inv.getReinvestedDistributions());
                ps.setDouble(13, Inv.getCashDistributions());
                ps.setDouble(14, Inv.getTax());
                ps.setDouble(15, Inv.getTaxPaid());
                double rate = Inv.getInvestmentReturnRate() != null ? Inv.getInvestmentReturnRate() : 0.0;
                ps.setDouble(16, rate);
                ps.setDouble(17, Inv.getInvestmentStrategyBand());
                ps.setLong(18, Inv.getRegistryAccountId());
                ps.setString(19, Inv.getContractDate());
                ps.setString(20, Inv.getCode());
                ps.setString(21, Inv.getInvestmentType());
                ps.setString(22, Inv.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return investments.size();
            }
        });

    }

    @Override
    public void saveFmcaTopAssets(List<FmcaTopAssets> fmcaTopAssets) {
        String insertFmcaTopAsset = "INSERT INTO `fmca_top_assest`\n"
                + "(`AssetName`,`AssetType`,`AssetClass`,`Portfolios`,`AsAtDate`,`Percentage`,`InvestorAssetValue`,`investmentCode`)\n"
                + "VALUES (?,?,?,?,?,?,?,?);";
        System.out.println("insertFmcaTopAsset----->" + insertFmcaTopAsset);
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = connection.prepareStatement(insertFmcaTopAsset, Statement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < fmcaTopAssets.size(); i++) {
                FmcaTopAssets fmcaTopAsset = fmcaTopAssets.get(i);
                ps.setString(1, fmcaTopAsset.getAssetName());
                ps.setString(2, fmcaTopAsset.getAssetType());
                ps.setString(3, fmcaTopAsset.getAssetClass());
                ps.setString(4, fmcaTopAsset.getAsAtDate());
                ps.setDouble(5, fmcaTopAsset.getPercentage());
                ps.setDouble(6, fmcaTopAsset.getInvestorAssetValue());
                ps.setString(7, fmcaTopAsset.getInvestmentCode());
                ps.addBatch();
            }
            int[] i = ps.executeBatch();
        } catch (SQLException ex) {
            Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        jdbcTemplate.batchUpdate(insertFmcaTopAsset, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {

            }

            @Override
            public int getBatchSize() {
                return fmcaTopAssets.size();
            }
        });

    }

    @Override
    public void saveLatestPortfoliosPrices(List<PortfolioPrice> portfolioPrices) {
        
        String updatePortfolioPrice = "UPDATE `portfolio_price` SET `active` = 'N' where `PortfolioCode` = ?";
        System.out.println("updatePortfolioPrice----->" + updatePortfolioPrice);

        jdbcTemplate.batchUpdate(updatePortfolioPrice, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PortfolioPrice portfolioPrice = portfolioPrices.get(i);
                ps.setString(1, portfolioPrice.getPortfolio());
            }

            @Override
            public int getBatchSize() {
                return portfolioPrices.size();
            }
        });

        String insertPortfolioPrice = "INSERT INTO `portfolio_price`\n"
                + "(`PortfolioCode`,`CreatedDate`,`BidPrice`,`AskPrice`,`LastPrice`,`MidPrice`,`active`)\n"
                + " VALUES (?,?,?,?,?,?,'Y');";

        jdbcTemplate.batchUpdate(insertPortfolioPrice, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PortfolioPrice portfolioPrice = portfolioPrices.get(i);
                ps.setString(1, portfolioPrice.getPortfolio());
                ps.setString(2, portfolioPrice.getDate());
                ps.setDouble(3, portfolioPrice.getBidPrice());
                ps.setDouble(4, portfolioPrice.getAskPrice());
                ps.setDouble(5, portfolioPrice.getLastPrice());
                ps.setDouble(6, portfolioPrice.getMidPrice());
            }

            @Override
            public int getBatchSize() {
                return portfolioPrices.size();
            }
        });

    }

    @Override
    public void saveFmcaInvestmentMix(List<FmcaInvestmentMix> fmcaInvestmentMixs) {
        String insertFmcaInvestmentMix = "INSERT INTO `fmca_investment_mix`\n"
                + "(`Portfolio`,`FMCAAssetClass`,`SectorValueBase`,`Percentage`,`AsAtDate`, `InvestmentCode`, `BeneficiaryId`)\n"
                + "VALUES (?,?,?,?,?,?,?);";
        System.out.println("insertFmcaInvestmentMix----->" + insertFmcaInvestmentMix);
        jdbcTemplate.batchUpdate(insertFmcaInvestmentMix, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                FmcaInvestmentMix fmcaInvestmentMix = fmcaInvestmentMixs.get(i);
                ps.setString(1, fmcaInvestmentMix.getPortfolio());
                ps.setString(2, fmcaInvestmentMix.getFMCAAssetClass());
                ps.setDouble(3, fmcaInvestmentMix.getSectorValueBase());
                ps.setDouble(4, fmcaInvestmentMix.getPercentage());
                ps.setString(5, fmcaInvestmentMix.getAsAtDate());
                ps.setString(6, fmcaInvestmentMix.getInvestmentCode());
                ps.setString(7, fmcaInvestmentMix.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return fmcaInvestmentMixs.size();
            }
        });

    }

    @Override
    public void saveInvestmentHolding(List<InvestmentHolding> investmentHoldings) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            con.setAutoCommit(false);
            String inactiveIH = "Update investment_holding set active='N' where InvestmentCode=? and PortfolioCode=?";
            PreparedStatement ps1 = con.prepareStatement(inactiveIH);
            String insertInvestmentHolding = "INSERT INTO `investment_holding`\n"
                    + "(`InvestmentCode`,`AsAt`,`PortfolioCode`,`PortfolioName`,`DistMethod`,`Units`,`TaxOwed`,\n"
                    + "`Price`,`MarketValue`,`PortfolioScale`,`Volatility`,`AverageFundReturn`,`Contributions`,\n"
                    + "`Withdrawals`,`Fees`,`CashDistributions`,`ReinvestedDistributions`,`Earnings`,`ReturnRate`,`BeneficiaryId`)\n"
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            PreparedStatement ps2 = con.prepareStatement(insertInvestmentHolding);
            System.out.println("insertInvestmentHolding----->" + insertInvestmentHolding);
            for (int i = 0; i < investmentHoldings.size(); i++) {
                InvestmentHolding InvHolding = investmentHoldings.get(i);
                ps1.setString(1, InvHolding.getInvestmentCode());
                ps1.setString(2, InvHolding.getPortfolioCode());
                ps1.addBatch();
                ps2.setString(1, InvHolding.getInvestmentCode());
                ps2.setString(2, InvHolding.getAsAt());
                ps2.setString(3, InvHolding.getPortfolioCode());
                ps2.setString(4, InvHolding.getPortfolioName());
                ps2.setString(5, InvHolding.getDistMethod());
                ps2.setDouble(6, InvHolding.getUnits());
                ps2.setDouble(7, InvHolding.getTaxOwed());
                ps2.setDouble(8, InvHolding.getPrice());
                ps2.setDouble(9, InvHolding.getMarketValue());
                ps2.setDouble(10, InvHolding.getPortfolioScale());
                ps2.setDouble(11, InvHolding.getVolatility());
                ps2.setDouble(12, InvHolding.getAverageFundReturn());
                ps2.setDouble(13, InvHolding.getContributions());
                ps2.setDouble(14, InvHolding.getWithdrawals());
                ps2.setDouble(15, InvHolding.getFees());
                ps2.setDouble(16, InvHolding.getCashDistributions());
                ps2.setDouble(17, InvHolding.getReinvestedDistributions());
                ps2.setDouble(18, InvHolding.getEarnings());
                ps2.setDouble(19, InvHolding.getReturnRate());
                ps2.setString(20, InvHolding.getBeneficiaryId());
                ps2.addBatch();
            }
            ps1.executeBatch();
            ps2.executeBatch();
            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(InvestmentRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void saveInvestmentPerformance(List<InvestmentPerformance> investmentPerformances) {
        Connection con = null;
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            con.setAutoCommit(false);
            String inactiveIH = "Update investment_performance set Active='N' where InvestmentCode=? and Portfolio=?";
            PreparedStatement ps1 = con.prepareStatement(inactiveIH);
            String insertInvestmentPerformance = "INSERT INTO `investment_performance`\n"
                    + "(`Portfolio`,`PeriodStartDate`,`PeriodEndDate`,`XIRRReturnRate`,`InsufficientData`,`PeriodsCovered`,`IsTotalPeriod`,`BeneficiaryId`, `InvestmentCode`, `Active`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?,'Y');";
            PreparedStatement ps2 = con.prepareStatement(insertInvestmentPerformance);
            System.out.println("insertInvestmentPerformance----->" + insertInvestmentPerformance);
            for (int i = 0; i < investmentPerformances.size(); i++) {
                InvestmentPerformance investmentPerformance = investmentPerformances.get(i);
                ps1.setString(1, investmentPerformance.getInvestmentCode());
                ps1.setString(2, investmentPerformance.getPortfolio());
                ps1.addBatch();
                ps2.setString(1, investmentPerformance.getPortfolio());
                ps2.setString(2, investmentPerformance.getPeriodStartDate());
                ps2.setString(3, investmentPerformance.getPeriodEndDate());
                ps2.setDouble(4, investmentPerformance.getXIRRReturnRate());
                ps2.setBoolean(5, investmentPerformance.isInsufficientData());
                ps2.setDouble(6, investmentPerformance.getPeriodsCovered());
                ps2.setBoolean(7, investmentPerformance.isIsTotalPeriod());
                ps2.setString(8, investmentPerformance.getBeneficiaryId());
                ps2.setString(9, investmentPerformance.getInvestmentCode());
                ps2.addBatch();
            }
            ps1.executeBatch();
            ps2.executeBatch();
            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(InvestmentRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CommonRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public List<Investment> getInvestments(String UserId, String BeneficiaryId) {
        String sql = " SELECT * FROM investments IM "
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IM.BeneficiaryId = UBR.beneficiairy_id ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IM.BeneficiaryId = " + BeneficiaryId;

        }
        List<Investment> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Investment.class
        ));
        return list;
    }

    @Override
    public List<InvestmentHolding> getInvestmentHoldings(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
//        String sql = "SELECT UUH.ClientName, UUH.Id id, Unitholder InvestmentCode, now() AsAt, Portfolio PortfolioCode, FundName PortfolioName, 'Reinvested' DistMethod, Units, 0 TaxOwed, Red Price, LastPrice * Units MarketValue, '' PortfolioScale, '' Volatility, '' AverageFundReturn, Units * Red Contributions, 0 Withdrawals, 0 Fees, 0 CashDistributions, 0 ReinvestedDistributions, 0 Earnings, 0 ReturnRate, UUH.BeneficiaryId  FROM investments IM \n"
//                + "INNER JOIN UpdateUnitsHolding UUH ON(IM.Code = UUH.Unitholder)\n"
//                + "INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
//                + "INNER JOIN (SELECT * FROM portfolio_price WHERE CreatedDate = (SELECT max(CreatedDate) FROM portfolio_price) GROUP BY PortfolioCode) PP ON(UUH.Portfolio = PP.PortfolioCode)\n"
//                + "WHERE IM.BeneficiaryId = UUH.BeneficiaryId\n";
        String sql = "SELECT IH.InvestmentCode, AsAt, IH.PortfolioCode, PortfolioName, DistMethod, Units, TaxOwed, PP.BidPrice*IH.Units AS MarketValue, PortfolioScale, \n"
                + "Volatility, AverageFundReturn, IH.Price, IH.Price * IH.Units AS Contributions, IH.Withdrawals, IH.Fees, IH.CashDistributions, IH.ReinvestedDistributions, IH.Earnings, IH.ReturnRate, IH.BeneficiaryId \n"
                + "FROM investments IM \n"
                + "INNER JOIN investment_holding IH ON(IM.Code = IH.InvestmentCode)\n"
                + "INNER JOIN portfolio_price PP ON(PP.PortfolioCode = IH.PortfolioCode and PP.active ='Y')\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "WHERE IM.BeneficiaryId = IH.BeneficiaryId and IH.active = 'Y'";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and UBR.beneficiairy_id = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and IM.Code = '" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
//            sql += " and UUH.Portfolio = " + PortfolioCode;
            sql += " and IH.PortfolioCode = " + PortfolioCode;
        }
        sql += " group by InvestmentCode, PortfolioCode; ";
        System.out.println("sql---->" + sql);
        List<InvestmentHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentHolding.class
        ));
        return list;
    }

//    'INVESTMENT_HOLDING'
    @Override
    public List<InvestmentHolding> getInvestmentHoldings(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate) {
        String sql = "SELECT IH.InvestmentCode, IH.AsAt, IH.PortfolioCode, IH.PortfolioName, IH.DistMethod, IH.Units, IH.TaxOwed, IH.MarketValue, IH.PortfolioScale, \n"
                + "IH.Volatility, IH.AverageFundReturn, IH.Price, IH.Contributions, IH.Withdrawals, IH.Fees, IH.CashDistributions, IH.ReinvestedDistributions, IH.Earnings, IH.ReturnRate, IH.BeneficiaryId ,B.Name as name\n"
                + "FROM investments IM \n"
                + "INNER JOIN investment_holding IH ON(IM.Code = IH.InvestmentCode)\n"
                + "INNER JOIN beneficiaries B ON (IM.BeneficiaryId = B.Id)\n"
                + "INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "INNER JOIN ApiCalls AC ON (IH.InvestmentCode = AC.InvestmentCode and IH.PortfolioCode = AC.PortfolioCode and AC.ActionName ='INVESTMENT_HOLDING')\n"
                + "WHERE IM.BeneficiaryId = IH.BeneficiaryId and IH.active = 'Y'";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and UBR.beneficiairy_id = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and IM.Code = '" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and IH.PortfolioCode = " + PortfolioCode;
        }
        if (CalledDate != null) {
            sql += "  and AC.CalledDate = '" + CalledDate + "' ";
        }
        sql += " group by InvestmentCode, PortfolioCode; ";
        System.out.println("InvestmentHoldings sql---->" + sql);
        List<InvestmentHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentHolding.class
        ));
        return list;
    }

    @Override
    public List<PortfolioPrice> getLatestPortfolioPrice(String PortfolioCode, String CalledDate) {
        String sql = "SELECT pp.* FROM portfolio_price pp inner join ApiCalls ac on"
                + " (ac.PortfolioCode=pp.PortfolioCode and ac.ActionName='PORTFOLIO_PERFORMANCES' and ac.Active='Y') \n"
                + " where pp.active='Y' ";
        if (CalledDate != null) {
          sql += " and ac.CalledDate = '" + CalledDate + "' ";
        }
        if (PortfolioCode != null) {
            sql += " and pp.PortfolioCode = " + PortfolioCode;
        }
        System.out.println("PortfolioPrice sql---->" + sql);
        List<PortfolioPrice> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PortfolioPrice.class));
        return list;
    }

    @Override
    public List<InvestmentPerformance> getInvestmentPerformances(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        System.out.println("call only");
        String sql = "SELECT IP.* FROM investments IM\n"
                + " INNER JOIN investment_performance IP on(IM.Code = IP.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IM.BeneficiaryId = IP.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        System.out.println("InvestmentPerformances sql---->" + sql);
        List<InvestmentPerformance> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentPerformance.class
        ));
        return list;
    }

    @Override
    public List<InvestmentPerformance> getInvestmentPerformances(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate) {
        System.out.println("call dated");
        String sql = "SELECT IP.* FROM investments IM\n"
                + " INNER JOIN investment_performance IP on(IM.Code = IP.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " INNER JOIN ApiCalls AC ON(IP.InvestmentCode = AC.InvestmentCode and IP.Portfolio = AC.PortfolioCode and  AC.ActionName = 'INVESTMENT_PERFORMANCES')\n"
                + " WHERE IM.BeneficiaryId = IP.BeneficiaryId and IP.Active = 'Y' ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (CalledDate != null) {
            sql += " and AC.CalledDate = '" + CalledDate + "' ";
        }
        System.out.println("InvestmentPerformances sql---->" + sql);
        List<InvestmentPerformance> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentPerformance.class
        ));
        return list;
    }

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = " select FMCAAssetClass, sum(SectorValueBase) SectorValueBase from investments IM\n"
                + " inner join fmca_investment_mix FIM on(IM.Code = FIM.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " where FIM.BeneficiaryId = IM.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and FIM.BeneficiaryId =" + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and FIM.InvestmentCode ='" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and FIM.Portfolio =" + PortfolioCode;
        }
        sql += " group by InvestmentCode, FMCAAssetClass;";
        System.out.println("sql---->" + sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class
        ));
        return list;
    }

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate) {
        String sql = " select FMCAAssetClass, sum(SectorValueBase) SectorValueBase from investments IM\n"
                + " inner join fmca_investment_mix FIM on(IM.Code = FIM.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " INNER JOIN ApiCalls AC ON (FIM.InvestmentCode = AC.InvestmentCode and FIM.Portfolio = AC.PortfolioCode and AC.ActionName ='FMCA_INVESTMENT_MIX')\n"
                + " where FIM.BeneficiaryId = IM.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and FIM.BeneficiaryId =" + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and FIM.InvestmentCode ='" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and FIM.Portfolio =" + PortfolioCode;
        }
        if (CalledDate != null) {
            sql += " and AC.CalledDate = '" + CalledDate + "' ";
        }
        sql += " group by FIM.InvestmentCode, FMCAAssetClass;";
        System.out.println("TotalFmcaInvestmentMix sql---->" + sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class));
        return list;
    }
    @Override
    public List<FmcaTopAssets> getFmcaAssets(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate) {
        String sql = " select FMCAAssetClass, sum(SectorValueBase) SectorValueBase from investments IM\n"
                + " inner join fmca_investment_mix FIM on(IM.Code = FIM.InvestmentCode)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " INNER JOIN ApiCalls AC ON (FIM.InvestmentCode = AC.InvestmentCode and FIM.Portfolio = AC.PortfolioCode and AC.ActionName ='FMCA_INVESTMENT_MIX')\n"
                + " where FIM.BeneficiaryId = IM.BeneficiaryId ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and FIM.BeneficiaryId =" + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and FIM.InvestmentCode ='" + InvestmentCode + "'";
        }
        if (PortfolioCode != null) {
            sql += " and FIM.Portfolio =" + PortfolioCode;
        }
        if (CalledDate != null) {
            sql += " and AC.CalledDate = '" + CalledDate + "' ";
        }
        sql += " group by FIM.InvestmentCode, FMCAAssetClass;";
        System.out.println("TotalFmcaInvestmentMix sql---->" + sql);
        List<FmcaTopAssets> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaTopAssets.class));
        return list;
    }

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMixByTable(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = " SELECT  TFA.InvestmentName FMCAAssetClass, SUM(TFA.Price) SectorValueBase\n"
                + " FROM investments IM INNER JOIN investment_holding IH ON (IM.Code = IH.InvestmentCode)\n"
                + " INNER JOIN target_fund_allocation TFA ON (IH.PortfolioCode = TFA.Portfolio)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " where IH.PortfolioCode = TFA.Portfolio ";
        if (UserId != null) {
            sql += " and UBR.user_id =" + UserId;
        }
        sql += " GROUP BY FMCAAssetClass;";
//        System.out.println("sql---->" + sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class
        ));
        return list;
    }

    @Override
    public List<FmcaTopAssets> getFmcaTopAssets(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
                
        String sql = "SELECT * FROM fmca_top_assest where Portfolios = '"+PortfolioCode+"' order by Percentage desc;";
            List<FmcaTopAssets> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaTopAssets.class));
        return list;
    }

    @Override
    public List<InvestmentTransection> getInvestmnetTransection(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String sql = "SELECT \n"
                + "  sum(CASE when pt.SubType = 'ATT' or pt.SubType = 'INV' Then pt.Value ELSE 0 END) as att_value,\n"
                + "  sum(CASE when pt.SubType = 'WDW' Then pt.Value ELSE 0 END) as wdw_value\n"
                + "  FROM investment_transaction pt\n"
                + "  inner join investments i on (i.Code=pt.InvestmentCode)\n"
                + "  INNER JOIN user_beneficiairy_relationship UBR ON (i.BeneficiaryId = UBR.beneficiairy_id)\n"
                + "  WHERE i.BeneficiaryId = UBR.beneficiairy_id\n";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (BeneficiaryId != null) {
            sql += " and IH.BeneficiaryId = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            sql += " and IH.InvestmentCode = '" + InvestmentCode + "'\n";
        }
        if (PortfolioCode != null) {
            sql += " and IH.PortfolioCode = " + PortfolioCode;

        }
        List<InvestmentTransection> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentTransection.class
        ));
        return list;
    }

    @Override
    public Investment getStartInvestmentDate(String UserId, String InvestmentCode) {
        String sql = " SELECT MIN(IM.ContractDate) as ContractDate FROM investments IM "
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IM.BeneficiaryId = UBR.beneficiairy_id ";
        if (UserId != null) {
            sql += " and UBR.user_id = " + UserId;
        }
        if (InvestmentCode != null) {
               sql += " and IM.Code = '" + InvestmentCode + "'\n";

        }
        sql += " limit 1 \n";
        System.out.println("sql---------"+sql);
        List<Investment> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Investment.class));
        return list.isEmpty() ? null : list.get(0);
    }
}
