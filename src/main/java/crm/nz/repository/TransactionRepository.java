/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.TransactionItem;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface TransactionRepository {

    public void saveTransactions(List<TransactionItem> transactionDetail);

    public List<TransactionItem> getTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public List<TransactionItem> getTransactionsbycode(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

    public TransactionItem getTransactions(String UserId);

    public List<TransactionItem> getTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate);

    public List<PendingTransactionBean> getPendingTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);
  
    public List<TransactionItem> getTotalUnitsWithprice(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode);

}
