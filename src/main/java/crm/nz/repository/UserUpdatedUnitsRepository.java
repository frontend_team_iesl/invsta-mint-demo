/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.UpdateUnitsHolding;
import java.util.List;

/**
 *
 * @author IESL
 */
public interface UserUpdatedUnitsRepository {

    public List<UpdateUnitsHolding> getNotExistInvestments(String UserId, String BeneficiaryId);

    public void saveInvestments(List<UpdateUnitsHolding> investmentList);

    public List<UpdateUnitsHolding> getNotExistBeneficiaries(String UserId, String BeneficiaryId);

    public void saveBeneficiaries(List<UpdateUnitsHolding> beneficiaryList);

    public List<UpdateUnitsHolding> getNotExistUsernames(String UserId, String BeneficiaryId);

    public void saveUsernames(List<UpdateUnitsHolding> UsernameList);

    public List<UpdateUnitsHolding> getNotExistUserBeneficiaryRelationships(String UserId, String BeneficiaryId);

    public void saveUserBeneficiaryRelationships(List<UpdateUnitsHolding> userBeneficiaryRelationships);

}
