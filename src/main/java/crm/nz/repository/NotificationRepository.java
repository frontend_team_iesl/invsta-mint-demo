/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.Notification;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface NotificationRepository {
   
   public List<Notification> getAllNotification(String id);
   
   public  PendingInvestmentBean getInvestmentLaterDetails(String notifyId);
   
   
   public  PendingInvestmentBean getTransactionLaterDetails(String notifyId);
    
    
}
