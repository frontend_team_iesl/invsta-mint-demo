/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.TransactionItem;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveTransactions(List<TransactionItem> transactionDetails) {
        String loginSQL = "INSERT INTO `investment_transaction`\n"
                + "(`InvestmentCode`,`EffectiveDate`,`Type`,`TypeDisplayName`,`SubType`,`SubTypeDisplayName`,`PortfolioCode`,`PortfolioName`,\n"
                + "`Units`,`Price`,`Value`,`Cash`,`Tax`,`Fee`,`AccruedIncome`,`CashClearing`,`PaymentClearing`,`PortfolioAccount`,`TaxRebate`,\n"
                + "`isPending`,`TransactionReasonType`,`TransactionSourceType`,`TransactionMethodType`,`TransactionDescription`,\n"
                + "`TransactionDisplayName`,`TransactionTypeDescription`,`PaymentReference`,`ExternalReference`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                TransactionItem transactionDetail = transactionDetails.get(i);
                ps.setString(1, transactionDetail.getInvestmentCode());
                ps.setString(2, transactionDetail.getEffectiveDate());
                ps.setString(3, transactionDetail.getType());
                ps.setString(4, transactionDetail.getTypeDisplayName());
                ps.setString(5, transactionDetail.getSubType());
                ps.setString(6, transactionDetail.getSubTypeDisplayName());
                ps.setString(7, transactionDetail.getPortfolioCode());
                ps.setString(8, transactionDetail.getPortfolioName());
                ps.setDouble(9, transactionDetail.getUnits());
                double Price = transactionDetail.getPrice() != null ? transactionDetail.getPrice() : 0.0;
                ps.setDouble(10, Price);
                ps.setDouble(11, transactionDetail.getValue());
                ps.setDouble(12, transactionDetail.getCash());
                ps.setDouble(13, transactionDetail.getTax());
                ps.setDouble(14, transactionDetail.getFee());
                ps.setDouble(15, transactionDetail.getAccruedIncome());
                ps.setDouble(16, transactionDetail.getCashClearing());
                ps.setDouble(17, transactionDetail.getPaymentClearing());
                ps.setDouble(18, transactionDetail.getPortfolioAccount());
                ps.setDouble(19, transactionDetail.getTaxRebate());
                ps.setBoolean(20, transactionDetail.getIsPending());
                ps.setString(21, transactionDetail.getTransactionReasonType());
                ps.setString(22, transactionDetail.getTransactionSourceType());
                ps.setString(23, transactionDetail.getTransactionMethodType());
                ps.setString(24, transactionDetail.getTransactionDescription());
                ps.setString(25, transactionDetail.getTransactionDisplayName());
                ps.setString(26, transactionDetail.getTransactionTypeDescription());
                ps.setString(27, transactionDetail.getPaymentReference());
                ps.setString(28, transactionDetail.getExternalReference());
            }

            @Override
            public int getBatchSize() {
                return transactionDetails.size();
            }

        });
    }

    @Override
    public List<TransactionItem> getTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String Sql = "SELECT IT.* FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IT.id > 0 ";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (PortfolioCode != null) {
            Sql += " and IT.PortfolioCode = " + PortfolioCode;
        }
        if (InvestmentCode != null) {
            Sql += " and IM.Code = " + InvestmentCode;
        }
        Sql += " order by EffectiveDate desc";
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions;
    }

    @Override
    public List<TransactionItem> getTransactionsbycode(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String Sql = "SELECT Date(EffectiveDate) EffectiveDate,IT.Units FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IT.id > 0 ";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (PortfolioCode != null) {
            Sql += " and IT.PortfolioCode = " + PortfolioCode;
        }
        if (InvestmentCode != null) {
            Sql += " and IM.Code ='" + InvestmentCode + "' ";
        }
        Sql += " order by EffectiveDate asc";
        System.out.println("Sql" + Sql);
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions;
    }

    @Override
    public TransactionItem getTransactions(String UserId) {
        String Sql = "SELECT count(IT.id) TotalItemCount, max(IT.EffectiveDate) EffectiveDate FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " WHERE IT.id > 0";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId;
        }
        Sql += " limit 1";
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions.get(0);
    }

    @Override
    public List<TransactionItem> getTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode, String CalledDate) {
        String Sql = "SELECT IT.* FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " INNER JOIN ApiCalls AC ON (IT.InvestmentCode = AC.InvestmentCode and IT.PortfolioCode = AC.PortfolioCode and AC.ActionName ='INVESTMENT_TRANSACTION')\n"
                + " WHERE IT.id > 0 ";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            Sql += " and IM.Code = " + InvestmentCode;
        }
        if (CalledDate != null) {
            Sql += " and AC.CalledDate = '" + CalledDate + "' ";
        }
        Sql += " order by EffectiveDate desc";

        System.out.println("Transactions Sql" + Sql);
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions;
    }

    @Override
    public List<PendingTransactionBean> getPendingTransactions(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String Sql = "select up.*,lm.username as name,b.Name as beneficiaryName ,p.Name as portfolioName \n"
                + "  from (\n"
                + " SELECT pt.id, investmentcode, portfolioCode, amount, pt.applicationId, \n"
                + " CASE when pt.type = 'WDW' Then 'Withdrawal' ELSE  'Investment' END as type,\n"
                + " pt.status, userId, bankAccountId, beneficiaryId, pt.created_ts, investmentType, regularAmount,\n"
                + " selectfrequecy, paymentMethod, directDebitId ,'pending_transaction' tabletype FROM pending_transaction pt \n"
                + " union all \n"
                + " SELECT pI.id, investmentcode, portfolioCode,investedAmount amount, pI.applicationId,\n"
                + " 'Investment' as type,\n"
                + " pI.status, userId, bankAccountId, beneficiaryId, pI.created_ts, \n"
                + " investmentType, regularAmount, selectfrequecy, paymentMethod,\n"
                + " directDebitId  ,'pending_investment' tabletype  from pending_investment pI ) up\n"
                + " inner join beneficiaries b on (b.Id=up.beneficiaryId)\n"
                + " inner join portfolio p on(p.Code=up.portfolioCode and p.status='Y')\n"
                + " inner join login_master lm on(lm.id=up.userId) where up.status='Pending'  ";
        if (UserId != null) {
            Sql += " and userid= " + UserId;
        }
        if (BeneficiaryId != null) {
            Sql += " and beneficiaryId = " + BeneficiaryId;
        }
        if (InvestmentCode != null) {
            Sql += " and investmentcode = " + InvestmentCode;
        }
        Sql += " order by created_ts desc ";
        System.out.println("Sql------->" + Sql);
        List<PendingTransactionBean> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(PendingTransactionBean.class));
        return transactions;
    }

    @Override
    public List<TransactionItem> getTotalUnitsWithprice(String UserId, String BeneficiaryId, String InvestmentCode, String PortfolioCode) {
        String Sql = "SELECT Date(PP.CreatedDate) CreatedDate, PP.PortfolioCode, PP.LastPrice, Date(EffectiveDate) EffectiveDate, IT.InvestmentCode, IT.PortfolioCode, IT.Units, sum(IT.Units) TotalUnits,sum(IT.Units)*PP.LastPrice as Price, IT.Type, IT.SubType FROM investment_transaction IT\n"
                + " INNER JOIN investments IM ON(IT.InvestmentCode = IM.Code)\n"
                + " INNER JOIN user_beneficiairy_relationship UBR ON (IM.BeneficiaryId = UBR.beneficiairy_id)\n"
                + " INNER JOIN (select * from portfolio_price group by CreatedDate,PortfolioCode) PP ON(IT.EffectiveDate<=PP.CreatedDate AND IT.PortfolioCode = PP.PortfolioCode)\n"
                + " WHERE IT.id > 0";
        if (UserId != null) {
            Sql += " and UBR.user_id= " + UserId; 
        }
        if (BeneficiaryId != null) {
            Sql += " and IM.BeneficiaryId = " + BeneficiaryId;
        }
        if (PortfolioCode != null) {
            Sql += " and IT.PortfolioCode = " + PortfolioCode;
        }
        if (InvestmentCode != null) {
            Sql += " and IM.Code ='" + InvestmentCode + "' ";
        }
        Sql += " GROUP BY CreatedDate, IT.InvestmentCode, IT.PortfolioCode\n"
                + " ORDER BY CreatedDate asc";
        System.out.println("Sql" + Sql);
        List<TransactionItem> transactions = jdbcTemplate.query(Sql, new BeanPropertyRowMapper(TransactionItem.class));
        return transactions;
    }

}
