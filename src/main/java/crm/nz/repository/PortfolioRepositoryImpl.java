/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.FmcaInvestmentMix;
import crm.nz.beans.acc.api.FmcaTopAssets;
import crm.nz.beans.acc.api.InvestmentPdfBean;
import crm.nz.beans.acc.api.InvestmentTeamBean;
import crm.nz.beans.acc.api.PerformanceBean;
import crm.nz.beans.acc.api.Portfolio;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.beans.acc.api.PortfolioPrice;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class PortfolioRepositoryImpl implements PortfolioRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<FmcaInvestmentMix> getTotalFmcaInvestmentMix(String PortfolioCode) {
        String sql = " SELECT * FROM target_fund_allocation where `status`='Y' and portfolio = " + PortfolioCode;
        System.out.println(sql);
        List<FmcaInvestmentMix> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaInvestmentMix.class));
        return list;
    }

    @Override
    public List<FmcaTopAssets> getFmcaTopAssets(String PortfolioCode) {
        String sql = " SELECT * FROM Acutal_sector_allocation where `active`='Y' and portfolio = " + PortfolioCode;
        System.out.println(sql);
        List<FmcaTopAssets> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(FmcaTopAssets.class));
        return list;
    }

    @Override
    public PortfolioDetail getPortfolioDetail(String PortfolioCode) {
        String sql = "  SELECT pd.*,p.name as fundName FROM portfolio_detail pd inner join portfolio p on(p.code=pd.Portfolio and p.status='Y') where pd.active='Y' and pd.Portfolio=" + PortfolioCode;
        System.out.println(sql);
        List<PortfolioDetail> portfolioDetail = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PortfolioDetail.class));
        return !portfolioDetail.isEmpty() ? portfolioDetail.get(0) : null;
    }

    @Override
    public void setPortfolioDetail(PortfolioDetail bean) {

        String sql = " update  portfolio_detail set `active`='N' where Portfolio =?";
        jdbcTemplate.update(sql, new Object[]{bean.getPortfolio()});

        String sqll = "INSERT INTO `portfolio_detail`\n"
                + "(\n"
                + "`Portfolio_pic`,\n"
                + "`Portfolio`,\n"
                + "`FundOverview`,\n"
                + "`RiskIndicator`,\n"
                + "`MonthlyFundUpdate`,\n"
                + "`FeaturesOftheFund`,\n"
                + "`monthlyReturnValue`,\n"
                + "`active`)\n"
                + "VALUES\n"
                + "(\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "'Y');";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(sqll);

            ps.setString(1, bean.getPortfolio_pic());
            ps.setString(2, bean.getPortfolio());
            ps.setString(3, bean.getFundOverview());
            ps.setString(4, bean.getRiskIndicator());
            ps.setString(5, bean.getMonthlyFundUpdate());
            ps.setString(6, bean.getFeaturesOftheFund());
            ps.setString(7, bean.getMonthlyReturnValue());

            return ps;
        });

    }

    @Override
    public void setPortfolio(PortfolioDetail bean) {

        String sql1 = " update  portfolio set `status`='N' where code =?";
        jdbcTemplate.update(sql1, new Object[]{bean.getPortfolio()});

        String sqll = " INSERT INTO `portfolio`\n"
                + "(\n"
                + "`name`,\n"
                + "`status`,\n"
                + "`code`\n"
                + ")\n"
                + "VALUES\n"
                + "(?,\n"
                + "'Y',\n"
                + "?);";
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(sqll);
            ps.setString(1, bean.getFundName());
            ps.setString(2, bean.getPortfolio());

            return ps;
        });

    }

    @Override
    public void setFmcaTopAssets(FmcaTopAssets bean) {
        String inActiveTargetFundDetail = " UPDATE `Acutal_sector_allocation` SET `active`='N' WHERE `portfolio`=?; ";
        String insertTargetFundDetail = "INSERT INTO `Acutal_sector_allocation`\n(`portfolio`, `investmentName`, `price`,`colorCode`, `active`)\n VALUES (?, ?, ?,?, 'Y');";

        jdbcTemplate.update(inActiveTargetFundDetail, new Object[]{bean.getPortfolio()});

        final String[] investmentName = bean.getInvestmentName() != null ? bean.getInvestmentName().split(",") : new String[]{};
        final String[] price = bean.getPrices().split(",");
        final String[] colorCode = bean.getColorCode().split(",");
        int length = investmentName.length;

        jdbcTemplate.batchUpdate(insertTargetFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, bean.getPortfolio());
                ps.setString(2, investmentName[i]);
                ps.setString(3, price[i]);
                ps.setString(4, colorCode[i]);

            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });

    }

    @Override
    public void setTotalFmcaInvestmentMix(FmcaTopAssets bean) {

        String inActiveTargetFundDetail = " UPDATE `target_fund_allocation` SET `status`='N' WHERE `portfolio`=?; ";
        String insertTargetFundDetail = "INSERT INTO `target_fund_allocation`\n(`portfolio`, `investmentName`, `price`, `colorCode`,`status`)\n VALUES (?, ?, ?,?, 'Y');";

        jdbcTemplate.update(inActiveTargetFundDetail, new Object[]{bean.getPortfolio()});

        final String[] investmentName = bean.getInvestmentName() != null ? bean.getInvestmentName().split(",") : new String[]{};
        final String[] price = bean.getPrices().split(",");
        final String[] colorCode = bean.getColorCode().split(",");
        int length = investmentName.length;

        jdbcTemplate.batchUpdate(insertTargetFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, bean.getPortfolio());
                ps.setString(2, investmentName[i]);
                ps.setString(3, price[i]);
                ps.setString(4, colorCode[i]);

            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });

    }

    @Override
    public void setFundPerformance(PerformanceBean bean) {
        String inActiveTargetFundDetail = " UPDATE `temp_fund_performances` SET `status`='N' WHERE `portfolio`=?; ";

        String sql = "INSERT INTO `temp_fund_performances`\n"
                + "(`onemonth`,\n"
                + "`onemonthTrending`,\n"
                + "`threemonth`,\n"
                + "`threemonthTrending`,\n"
                + "`portfolio`,\n"
                + "`oneyear`,\n"
                + "`oneyeartrending`,\n"
                + "`unit_price`,\n"
                + "`fund_size`,\n"
                + "`one_year_return`,\n"
                + "`riskIndicator`,\n"
                + "`status`,\n"
                + "`fiveyear`)\n"
                + "VALUES\n"
                + "(?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "'Y',\n"
                + "?);";
        jdbcTemplate.update(inActiveTargetFundDetail, new Object[]{bean.getPortfolio()});

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, bean.getOnemonth());
            ps.setString(2, bean.getOnemonthTrending());
            ps.setString(3, bean.getThreemonth());
            ps.setString(4, bean.getThreemonthTrending());
            ps.setString(5, bean.getPortfolio());
            ps.setString(6, bean.getOneyear());
            ps.setString(7, bean.getOneyeartrending());
            ps.setString(8, bean.getUnit_price());
            ps.setString(9, bean.getFund_size());
            ps.setString(10, bean.getOne_year_return());
            ps.setString(11, bean.getRiskIndicator());
            ps.setString(12, bean.getFiveyear());

            return ps;
        });

//        String sql = " update  portfolio_detail set FundOverview=?,MonthlyFundUpdate=?,FeaturesOftheFund=? where Portfolio =?";
//        jdbcTemplate.update(sql, new Object[]{bean.getFundOverview(), bean.getMonthlyFundUpdate(), bean.getFeaturesOftheFund(), bean.getPortfolio()});
    }

    @Override
    public List<PerformanceBean> getFundPerformance(String PortfolioCode) {
        String sql = " SELECT * FROM `temp_fund_performances` where `status` = 'Y' and `portfolio` = " + PortfolioCode;
        List<PerformanceBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PerformanceBean.class));
        return list;
    }

    @Override
    public void savePortfolios(List<Portfolio> list) {
        String sql = "INSERT INTO `portfolio` (`Name`, `Code`) VALUES(?, ?);";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Portfolio p = list.get(i);
                ps.setString(1, p.getName());
                ps.setString(2, p.getCode());
            }

            @Override
            public int getBatchSize() {
                return list.size();
            }
        });
    }

    @Override
    public void savePortfolioPrices(List<PortfolioPrice> list) {
        String sql = " INSERT INTO `portfolio_price`\n"
                + "(`PortfolioCode`,\n"
                + " `CreatedDate`,\n"
                + " `BidPrice`,\n"
                + " `AskPrice`,\n"
                + " `LastPrice`,\n"
                + " `MidPrice`)\n"
                + " VALUES\n"
                + "(?, ?, ?, ?, ?, ?); ";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PortfolioPrice p = list.get(i);
                ps.setString(1, p.getPortfolioCode());
                ps.setString(2, p.getDate());
                ps.setDouble(3, p.getPrices());
                ps.setDouble(4, p.getPrices());
                ps.setDouble(5, p.getPrices());
                ps.setDouble(6, p.getPrices());
            }

            @Override
            public int getBatchSize() {
                return list.size();
            }
        });
    }

    @Override
    public List<PortfolioDetail> getOtherPortfolioDetail(String PortfolioCode) {
        String sql = "  SELECT p.id id,p.name fundName, p.code Portfolio FROM `portfolio` p where    code <> '" + PortfolioCode + "' and p.status ='Y';";
        List<PortfolioDetail> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PortfolioDetail.class));
        return list;
    }

    @Override
    public List<Portfolio> getAllPortfolios() {
        String sql = " SELECT p.id id,p.Name, p.Code ,pd.FundOverview  as fundOverview  FROM `portfolio` p  inner join portfolio_detail pd on(pd.Portfolio=p.Code and pd.active='Y') where p.status ='Y';";
        List<Portfolio> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Portfolio.class));
        return list;
    }

    @Override
    public List<Portfolio> getAllPortfoliosList() {
        String sql = " select T.* from (SELECT PortfolioCode as Code FROM investment_holding \n"
                + "union all\n"
                + "SELECT Code  FROM portfolio)  T\n"
                + " group by Code;";
        List<Portfolio> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Portfolio.class));
        return list;
    }

    @Override
    public void saveOrUpdateInvestmentTeam(List<InvestmentTeamBean> teamBeans, String portfolio) {
        String inActiveTargetFundDetail = " UPDATE `investment_team` SET `status`='N' WHERE `portfolio`=?; ";
        jdbcTemplate.update(inActiveTargetFundDetail, new Object[]{portfolio});

        String isql = "INSERT INTO `investment_team`\n"
                + "(\n"
                + "`name`,\n"
                + "`role`,\n"
                + "`pic_url`,\n"
                + "`place`,\n"
                + "`status`,\n"
                + "`portfolio`,\n"
                + "`team_bio`)\n"
                + "VALUES\n"
                + "(?,\n"
                + "?,\n"
                + "?,\n"
                + "?,\n"
                + "'Y',\n"
                + "?,\n"
                + "?);";

        jdbcTemplate.batchUpdate(isql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                InvestmentTeamBean bean = teamBeans.get(i);
                ps.setString(1, bean.getName());
                ps.setString(2, bean.getRole());
                ps.setString(3, bean.getPic_url());
                ps.setString(4, bean.getPlace());
                ps.setString(5, bean.getPortfolio());
                ps.setString(6, bean.getTeam_bio());
            }

            @Override
            public int getBatchSize() {
                return teamBeans.size();
            }
        });
    }

    @Override
    public List<InvestmentTeamBean> getInvestmentTeam(String PortfolioCode) {
        String sql = "  SELECT * FROM `investment_team`  where    `portfolio`= '" + PortfolioCode + "' and status ='Y';";
        List<InvestmentTeamBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentTeamBean.class));
        return list;
    }

    @Override
    public void saveOrUpdatePdfFile(List<InvestmentPdfBean> teamBeans, String portfolio) {

        String inActiveTargetFundDetail = " UPDATE `investment_pdf` SET `status`='N' WHERE `portfolio`=?; ";
        jdbcTemplate.update(inActiveTargetFundDetail, new Object[]{portfolio});
        String isql = "INSERT INTO `investment_pdf`\n"
                + "(\n"
                + "`pdf_name`,\n"
                + "`pdf_url`,\n"
                + "`status`,\n"
                + "`portfolio`)\n"
                + "VALUES\n"
                + "(\n"
                + "?,\n"
                + "?,\n"
                + "'N',\n"
                + "?);";

        jdbcTemplate.batchUpdate(isql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                InvestmentPdfBean bean = teamBeans.get(i);
                ps.setString(1, bean.getPdf_name());
                ps.setString(2, bean.getPdf_url());
                ps.setString(3, bean.getPortfolio());
            }

            @Override
            public int getBatchSize() {
                return teamBeans.size();
            }
        });

    }

    @Override
    public List<InvestmentPdfBean> getInvestmentPdf(String PortfolioCode) {
        String sql = "  SELECT * FROM `investment_pdf`  where    `portfolio`= '" + PortfolioCode + "' and status ='Y';";
        List<InvestmentPdfBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentPdfBean.class));
        return list;
    }

    @Override
    public List<InvestmentPdfBean> getQuarterlyPdf(String PortfolioCode) {
        String sql = "  SELECT * FROM `quarterly_pdf`  where    `portfolio`= '" + PortfolioCode + "' and status ='Y';";
        List<InvestmentPdfBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(InvestmentPdfBean.class));
        return list;
    }

    @Override
    public List<PerformanceBean> getPerformanceSinceinception(String PortfolioCode) {
        String sql = "  SELECT * FROM `performance_since_inception`  where    `portfolio_code`= '" + PortfolioCode + "' ORDER BY date asc;";
        List<PerformanceBean> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PerformanceBean.class));
        return list;
    }

    @Override
    public List<PortfolioPrice> getPerformancePrices(String PortfolioCode, String startDate) {
        String sql = "SELECT  PortfolioCode, Date(CreatedDate) CreatedDate, BidPrice, \n"
                + "AskPrice, LastPrice, MidPrice, active FROM portfolio_price where PortfolioCode='" + PortfolioCode + "' ";
        if (startDate != null) {
            sql += " and CreatedDate >= '" + startDate + "'\n";
        }
        sql += " group by CreatedDate,PortfolioCode ORDER BY CreatedDate asc;";
        List<PortfolioPrice> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PortfolioPrice.class));
        return list;
    }

}
