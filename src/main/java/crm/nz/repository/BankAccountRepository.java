/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingBankAccount;
import java.util.List;

/**
 *
 * @author TOSHIBA R830
 */
public interface BankAccountRepository {

    String insertSql = "INSERT INTO `pendingbankAccount`\n"
            + "(`accountHolderName`,`bankName`,`accountNumber`,`pincodeinput`,`userId`,\n"
            + "`beneficiaryId`,`action`,`Bank`,`Branch`,`Suffix`,`Currency`,`ApplicationId`,`BankAccountId`,`bankFileUrl`)\n"
            + "VALUES (?,?,?,?,?,?,'Pending',?,?,?,?,?,?,?);";

    String invbankSql = "INSERT INTO `Bank_account_Detail`\n"
            + "(`AccountName`,\n"
            + "`Bank`,\n"
            + "`Branch`,\n"
            + "`Account`,\n"
            + "`Suffix`,\n"
            + "`Currency`,\n"
            + "`Status`,\n"
            + "`Type`,\n"
            + "`IsPrimary`,\n"
            + "`BeneficiaryId`,\n"
            + "`BankAccountDetailId`,\n"
            + "`ApplicationId`,\n"
            + "`BankAccountId`,\n"
            + "`InvestmentCode`)\n"
            + "VALUES\n"
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    public void setPendingBankAccount(PendingBankAccount bean);

    public List<PendingBankAccount> getPendingankAccount();

    public void acceptBankAccount(String id);
}
