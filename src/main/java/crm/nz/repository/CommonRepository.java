/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.ApiCall;
import crm.nz.beans.acc.api.BankCodeBean;
import crm.nz.beans.acc.api.CountryISOCodeBean;
import crm.nz.beans.acc.api.Email;
import crm.nz.beans.acc.api.Register;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface CommonRepository {

    public Login findLoginByUsername(String un);

    public int register(Register register);

    public void insertLogin(List<Email> emailList);

    public void updateSecretCode(SecuredUser user);

    public CountryISOCodeBean getCountryISOCode(String country);

    public List<CountryISOCodeBean> getAllCountryCode();
    
    public List<BankCodeBean> getAllBankCode();

    public void removeSecretCode(SecuredUser user);

    public void saveApiCall(ApiCall apiCall);

    public void saveApiCall(List<ApiCall> apiCalls);
}
