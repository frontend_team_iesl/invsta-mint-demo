/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.PendingBankAccount;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import crm.nz.beans.acc.api.PendingTransactionBean;
import crm.nz.beans.acc.api.PortfolioDetail;
import crm.nz.lambda.repo.FormDetailsRepository;
import static crm.nz.repository.PendingInvestmentRepository.getPrice;
import static crm.nz.repository.PendingInvestmentRepository.insertInvestmentHolding;
import static crm.nz.repository.PendingInvestmentRepository.insertcode;
import static crm.nz.repository.PendingInvestmentRepository.invTranSql;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class BankAccountRepositoryImpl implements BankAccountRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void setPendingBankAccount(PendingBankAccount bean) {
        jdbcTemplate.update((java.sql.Connection con) -> {

            PreparedStatement ps = con.prepareStatement(insertSql);
            ps.setString(1, bean.getAccountHolderName());
            ps.setString(2, bean.getBankName());
            ps.setString(3, bean.getAccountNumber());
            ps.setString(4, bean.getPincodeinput());
            ps.setString(5, bean.getUserId());
            ps.setString(6, bean.getBeneficiaryId());
            ps.setString(7, bean.getAccountNumber().split("-")[0]);
            ps.setString(8, bean.getAccountNumber().split("-")[1]);
            ps.setString(9, bean.getAccountNumber().split("-")[3]);
            ps.setString(10, "NZD");
            ps.setString(11, bean.getApplicationId());
            ps.setString(12, bean.getBankAccountId());
            ps.setString(13, bean.getBankFile());
            return ps;
        });
    }

    @Override
    public List<PendingBankAccount> getPendingankAccount() {
        String sql = "SELECT * FROM `pendingbankAccount` where action='Pending'  ";
        List<PendingBankAccount> PendingBankAccount = jdbcTemplate.query(sql, new BeanPropertyRowMapper(PendingBankAccount.class));
        return PendingBankAccount;

    }

    @Override
    public void acceptBankAccount(String id) {
        Connection con = null;
        try {

            String sql = "SELECT * FROM `pendingbankAccount` where id= ?";
            con = jdbcTemplate.getDataSource().getConnection();
            List<PendingBankAccount> pendingBankAccount = jdbcTemplate.query(sql, new Object[]{id}, new BeanPropertyRowMapper(PendingBankAccount.class));
            PendingBankAccount bean = pendingBankAccount.get(0);
            String setPrimary = "UPDATE Bank_account_Detail SET IsPrimary='0' WHERE BeneficiaryId =?";
            jdbcTemplate.update(setPrimary, new Object[]{bean.getBeneficiaryId()});

            PreparedStatement ps = con.prepareStatement(invbankSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bean.getAccountHolderName());
            ps.setString(2, bean.getAccountNumber().split("-")[0]);
            ps.setString(3, bean.getAccountNumber().split("-")[1]);
            ps.setString(4, bean.getAccountNumber().split("-")[2]);
            ps.setString(5, bean.getAccountNumber().split("-")[3]);
            ps.setString(6, "NZD");
            ps.setString(7, "");
            ps.setString(8, "");
            ps.setString(9, "1");
            ps.setString(10, bean.getBeneficiaryId());
            ps.setString(11, "");
            ps.setString(12, "");
            ps.setString(13, bean.getApplicationId());
            ps.setString(14, bean.getBankAccountId());

            ps.executeUpdate();
//            ps3.close();
            String setstatus = "UPDATE pendingbankAccount SET action='accept' WHERE id =?";
            jdbcTemplate.update(setstatus, new Object[]{bean.getId()});

        } catch (SQLException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } catch (NullPointerException ex) {
            System.out.print("sqlexcp------------------" + ex);
        } finally {
            if (con != null) {
                try {
                    con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(FormDetailsRepository.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
