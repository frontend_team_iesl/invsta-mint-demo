/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.Notification;
import crm.nz.beans.acc.api.PendingInvestmentBean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class NotificationRepositoryImpl implements NotificationRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public PendingInvestmentBean getInvestmentLaterDetails(String notifyId) {
        String innerSql = "SELECT N.id as notifyId , PI.*, P.Name as  portfolioName  FROM notification N \n"
                + "inner join pending_investment PI on (PI.id =N.table_id) \n"
                + "inner join portfolio P on (P.Code = PI.portfolioCode) \n"
                + "where N.id= ?;";
        List<PendingInvestmentBean> notifyInvestment = jdbcTemplate.query(innerSql, new Object[]{notifyId}, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return notifyInvestment.size() > 0 ? notifyInvestment.get(0) : null;
    }

    @Override
    public PendingInvestmentBean getTransactionLaterDetails(String notifyId) {
        String innerSql = "SELECT N.id as notifyId , PT.*, PT.amount as investedAmount, P.Name as  portfolioName  FROM notification N \n"
                + " inner join pending_transaction PT on (PT.id =N.table_id) \n"
                + " inner join portfolio P on (P.Code = PT.portfolioCode) \n"
                + " where N.id= ?;";
        List<PendingInvestmentBean> notifyInvestment = jdbcTemplate.query(innerSql, new Object[]{notifyId}, new BeanPropertyRowMapper(PendingInvestmentBean.class));
        return notifyInvestment.size() > 0 ? notifyInvestment.get(0) : null;
    }

    @Override
    public List<Notification> getAllNotification(String id) {
        String innerSql = " SELECT CT.*, P.Name as name  FROM (\n"
                + " SELECT  N.*, PI.investedAmount, PI.investmentName , PI.investmentCode, PI.portfolioCode FROM pending_investment PI\n"
                + " inner join notification N on (PI.id = N.table_id AND N.table_name ='pending_investment') \n"
                + " union ALL\n"
                + " SELECT N.*, PT.amount as investedAmount, null as investmentName,  PT.investmentCode , PT.portfolioCode FROM pending_transaction PT\n"
                + " inner join notification N on (PT.id = N.table_id AND N.table_name ='pending_transaction')\n"
                + " ) CT\n"
                + " inner join portfolio P on (P.Code = CT.portfolioCode)\n"
                + " where CT.user_id= ? and CT.active = 'Y'; ";
        List<Notification> notify = jdbcTemplate.query(innerSql, new Object[]{id}, new BeanPropertyRowMapper(Notification.class));
        return notify;
    }
}
