/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.userInfobean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author TOSHIBA R830
 */
@Repository
public class UserInformationRepositoryImpl implements UserInformationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public userInfobean getUserdetailsByAdmin() {
        List<userInfobean> userinfobean = jdbcTemplate.query(sqladmin, new BeanPropertyRowMapper(userInfobean.class));
        return userinfobean.get(0);
    }
    @Override
    public userInfobean getUserdetailsByAdvisor(String id) {
        List<userInfobean> userinfobean = jdbcTemplate.query(sqladvisor,new Object[]{id,id,id}, new BeanPropertyRowMapper(userInfobean.class));
        return userinfobean.get(0);
    }

}
