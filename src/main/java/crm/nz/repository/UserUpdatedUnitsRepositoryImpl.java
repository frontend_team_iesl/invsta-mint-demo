/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.repository;

import crm.nz.beans.acc.api.UpdateUnitsHolding;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author IESL
 */
@Service
public class UserUpdatedUnitsRepositoryImpl implements UserUpdatedUnitsRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public List<UpdateUnitsHolding> getNotExistInvestments(String UserId, String BeneficiaryId) {
        String sql = " select UUH.FundName, UUH.ExternalRef, UUH.Greeting, UUH.DateEntered, UUH.Unitholder, UUH.ClientID BeneficiaryId from NewUpdateUnitsHoldings UUH where UUH.ClientID is not null \n"
                + " AND UUH.Unitholder not in (SELECT Code from investments where Code \n"
                + " in(select UUH.Unitholder from NewUpdateUnitsHoldings UUH where UUH.ClientID is not null group by UUH.Unitholder)) \n"
                + " group by UUH.Unitholder;";
        List<UpdateUnitsHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(UpdateUnitsHolding.class));
        return list;
    }

    @Override
    public void saveInvestments(List<UpdateUnitsHolding> investments) {
        String insertInvestment = "INSERT INTO `investments`\n"
                + "(`InvestmentName`,`Status`, `ExternalReference`,`FeeGroup`,`AdvisorRate`,`Greeting`,`InvestmentStrategyName`,\n"
                + "`Contributions`,`Withdrawals`,`Earnings`,`TotalValue`,`ReinvestedDistributions`,`CashDistributions`,`Tax`,`TaxPaid`,\n"
                + "`InvestmentReturnRate`,`InvestmentStrategyBand`,`RegistryAccountId`,`ContractDate`,`Code`,`InvestmentType`, `BeneficiaryId`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(insertInvestment, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UpdateUnitsHolding Inv = investments.get(i);
                ps.setString(1, Inv.getFundName());
                ps.setString(2, null);
                ps.setString(3, Inv.getExternalRef());
                ps.setString(4, null);
                ps.setString(5, null);
                ps.setString(6, Inv.getGreeting());
                ps.setString(7, null);
                ps.setDouble(8, 0.0d);
                ps.setDouble(9, 0.0d);
                ps.setDouble(10, 0.0d);
                ps.setDouble(11, 0.0d);
                ps.setDouble(12, 0.0d);
                ps.setDouble(13, 0.0d);
                ps.setDouble(14, 0.0d);
                ps.setDouble(15, 0.0d);
                ps.setDouble(16, 0.0d);
                ps.setDouble(17, 0.0d);
                ps.setLong(18, 0l);
                ps.setString(19, Inv.getDateEntered());
                ps.setString(20, Inv.getUnitholder());
                ps.setString(21, "Unit_Registry");
                ps.setString(22, Inv.getBeneficiaryId());
            }

            @Override
            public int getBatchSize() {
                return investments.size();
            }
        });
    }

    @Override
    public List<UpdateUnitsHolding> getNotExistBeneficiaries(String UserId, String BeneficiaryId) {
        String sql = "select UUH.BeneficiaryId, UUH.IRDNo, UUH.ClientName, UUH.DateEntered,UUH.InvestorType from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null "
                + "and UUH.BeneficiaryId not in (SELECT Id from beneficiaries where Id  "
                + "in(select UUH.BeneficiaryId from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null group by UUH.BeneficiaryId)) "
                + " group by UUH.BeneficiaryId";
        List<UpdateUnitsHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(UpdateUnitsHolding.class));
        return list;
    }

    @Override
    public void saveBeneficiaries(List<UpdateUnitsHolding> beneficiaryList) {
        String loginSQL = "INSERT INTO `beneficiaries`\n"
                + "(`Id`,`IrdNumber`,`CustomerNumber`,`AccountOf`,`Name`,`DateOfBirth`,`ClientAccountStartDate`,\n"
                + "`FirstName`,`LastName`,`AccessLevel`,`AMLEntityType`,`Status`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?);";
        jdbcTemplate.batchUpdate(loginSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UpdateUnitsHolding beneficiary = beneficiaryList.get(i);
                ps.setString(1, beneficiary.getBeneficiaryId());
                ps.setString(2, beneficiary.getIRDNo());
                ps.setString(3, null);
                ps.setString(4, null);
                ps.setString(5, beneficiary.getClientName());
                ps.setString(6, null);
                ps.setString(7, beneficiary.getDateEntered());
                ps.setString(8, null);
                ps.setString(9, null);
                ps.setString(10, "ListAccess");
                ps.setString(11, beneficiary.getInvestorType());
                ps.setString(12, "OPEN");
            }

            @Override
            public int getBatchSize() {
                return beneficiaryList.size();
            }
        });
    }

    @Override
    public List<UpdateUnitsHolding> getNotExistUsernames(String UserId, String BeneficiaryId) {
        String sql = " select UUH.Email from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null "
                + " and UUH.Email not in (SELECT username from login_master where username "
                + " in (select UUH.Email from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null group by UUH.Email)) "
                + " group by UUH.Email;";
        List<UpdateUnitsHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(UpdateUnitsHolding.class));
        return list;
    }

    @Override
    public void saveUsernames(List<UpdateUnitsHolding> UsernameList) {
        String insertSQL = " INSERT INTO `login_master`\n"
                + "(`username`, `password`, `role`, `created_ts`, `active`, `raw_pwd`)\n"
                + "VALUES (?, ?, ?, now(), 'Y', ?) ";
        jdbcTemplate.batchUpdate(insertSQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UpdateUnitsHolding uniqueEmail = UsernameList.get(i);
                ps.setString(1, uniqueEmail.getEmail());
                String password = uniqueEmail.getPassword();
                ps.setString(2, encoder.encode(password));
                ps.setString(3, "BENEFICIARY");
                ps.setString(4, password);
            }

            @Override
            public int getBatchSize() {
                return UsernameList.size();
            }
        });
    }

    @Override
    public List<UpdateUnitsHolding> getNotExistUserBeneficiaryRelationships(String UserId, String BeneficiaryId) {
        String sql = "select UBR.*, LM.id UserId \n"
                + "from (select UUH.Email, UUH.BeneficiaryId from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null \n"
                + "and UUH.BeneficiaryId \n"
                + "not in \n"
                + "(\n"
                + "select beneficiairy_id from user_beneficiairy_relationship where beneficiairy_id \n"
                + "in (select UUH.BeneficiaryId from UpdateUnitsHolding UUH where UUH.BeneficiaryId is not null group by UUH.BeneficiaryId)\n"
                + ")\n"
                + " group by UUH.BeneficiaryId) UBR\n"
                + " inner join login_master LM on (UBR.Email = LM.username)";
        List<UpdateUnitsHolding> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper(UpdateUnitsHolding.class));
        return list;
    }

    @Override
    public void saveUserBeneficiaryRelationships(List<UpdateUnitsHolding> userBeneficiaryRelationships) {
        String insertSQL2 = " INSERT INTO `user_beneficiairy_relationship`\n"
                + "(`user_name`,`beneficiairy_id`,`user_id`)\n"
                + "VALUES (?,?,?);";
        jdbcTemplate.batchUpdate(insertSQL2, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UpdateUnitsHolding email = userBeneficiaryRelationships.get(i);
                ps.setString(1, email.getEmail());
                ps.setString(2, email.getBeneficiaryId());
                ps.setString(3, email.getUserId());
            }

            @Override
            public int getBatchSize() {
                return userBeneficiaryRelationships.size();
            }
        });
    }

}
