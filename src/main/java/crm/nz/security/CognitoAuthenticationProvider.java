/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.NotAuthorizedException;
import com.amazonaws.services.cognitoidp.model.UserNotConfirmedException;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import crm.nz.beans.acc.api.CognitoUserPoolBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

@Component
public class CognitoAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    public CognitoUserPoolBean userPoolBean;
    
    @Autowired
    private CognitoProvider cognitoProvider;
    
    @Autowired
    private UserDetailsServiceImpl repository;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        System.out.println("Start authenticate");
        String poolId = userPoolBean.getPoolId();
        String clientId = userPoolBean.getClientId();
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        Map<String, String> params = new HashMap<>();
        params.put("USERNAME", username);
        params.put("PASSWORD", password);
//        params.put("SECRET_HASH", calculateSecretHash(userId));
        AdminInitiateAuthRequest request = new AdminInitiateAuthRequest()
                .withUserPoolId(poolId)
                .withClientId(clientId)
                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withAuthParameters(params);
        System.out.println("request------------------" + request);
        try {
            AdminInitiateAuthResult result = cognitoProvider.identityProvider().adminInitiateAuth(request);
            System.out.println("result------------------" + result);
            if ("NEW_PASSWORD_REQUIRED".equals(result.getChallengeName())) {
                System.out.println("Challenge Name------------------NEW_PASSWORD_REQUIRED");
            }
//            SecuredUser securedUser = new SecuredUser(username, password, Collections.emptyList());
            UserDetails securedUser = repository.loadUserByUsername(username);
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(securedUser, password);
            return usernamePasswordAuthenticationToken;
        } catch (UserNotFoundException | NotAuthorizedException | UserNotConfirmedException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private String calculateSecretHash(@Nonnull String userName) {// It is so important for secret key which we can need in make api side
        byte[] clientSecretBytes = null;
        String clientId = userPoolBean.getClientId();
//        clientSecretBytes = clientSecret.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec signingKey = new SecretKeySpec(clientSecretBytes, HmacAlgorithms.HMAC_SHA_256.toString());
        try {
            Mac mac = Mac.getInstance(HmacAlgorithms.HMAC_SHA_256.toString());
            mac.init(signingKey);
            mac.update(userName.getBytes(StandardCharsets.UTF_8));
            byte[] rawHmac = mac.doFinal(clientId.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(rawHmac);
        } catch (Exception ex) {
            throw new RuntimeException("Error calculating secret hash", ex);
        }
    }

}
