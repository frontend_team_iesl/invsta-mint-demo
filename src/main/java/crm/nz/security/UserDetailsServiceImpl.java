package crm.nz.security;

import crm.nz.repository.CommonRepository;
import crm.nz.table.bean.Login;
import crm.nz.table.bean.SecuredUser;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private CommonRepository repository;

    @Override
    @Transactional(readOnly = false)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Login login = repository.findLoginByUsername(username);
        if (login != null) {
            GrantedAuthority authority = new SimpleGrantedAuthority(login.getRole());
            SecuredUser user = new SecuredUser(login.getUsername(), login.getPassword(), Arrays.asList(authority));
            user.setLogin(login);
            return user;
        } else {
            return null;
        }
    }
}
