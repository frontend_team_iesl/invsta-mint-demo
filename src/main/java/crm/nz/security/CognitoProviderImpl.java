/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import org.springframework.stereotype.Component;

@Component
public class CognitoProviderImpl implements CognitoProvider{
    
    private final String awsAccessKey = "AKIAI5GUSB5HL2LJPIVA";
    private final String awsSecretKey = "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";
    private final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
    private final AWSCredentialsProvider credentialsProvider = new StaticCredentialsProvider(basicAWSCredentials);
    private final AWSCognitoIdentityProvider identityProvider = AWSCognitoIdentityProviderClientBuilder
            .standard()
            .withCredentials(credentialsProvider)
            .withRegion(Regions.AP_SOUTHEAST_2)
            .build();

    public AWSCognitoIdentityProvider identityProvider() {
        return identityProvider;
    }

}
