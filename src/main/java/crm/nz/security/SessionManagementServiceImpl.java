/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;

import crm.nz.table.bean.SecuredUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class SessionManagementServiceImpl implements SessionManagementService {

    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @Override
    public List<SecuredUser> getAllOnlineUsers() {
        if (sessionRegistry == null) {
            throw new NullPointerException("The sessionRegistry cannot be null");
        }
        List<SecuredUser> principals = (List) sessionRegistry.getAllPrincipals();
        System.out.println("principals" + principals);
        return principals;
    }

    @Override
    public List<SessionInformation> getAllSessions(SecuredUser principal, boolean includeExpiredSessions) {
        if (sessionRegistry == null) {
            throw new NullPointerException("The sessionRegistry cannot be null");
        }
        List<SessionInformation> sessions = (List) sessionRegistry.getAllSessions(principal, includeExpiredSessions);
        System.out.println("sessions" + sessions);
        return sessions;
    }

    @Override
    public SessionInformation getSessionInformation(String sessionId) {
        if (sessionRegistry == null) {
            throw new NullPointerException("The sessionRegistry cannot be null");
        }
        return sessionRegistry.getSessionInformation(sessionId);
    }
}
