package crm.nz.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import crm.nz.table.bean.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Component
public class UserDetailsServiceAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        System.out.println("Start authenticate");
        String username = authentication.getName();
        String rawPassword = authentication.getCredentials().toString();
        UserDetails securedUser = userDetailsServiceImpl.loadUserByUsername(username);
        if (securedUser == null) {
            return null;
        }
        final String encodedPassword = securedUser.getPassword();
        boolean matches = encoder.matches(rawPassword, encodedPassword);
        System.out.println("Secured authenticate");
        if (matches) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(securedUser, rawPassword, securedUser.getAuthorities());
            return usernamePasswordAuthenticationToken;
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
