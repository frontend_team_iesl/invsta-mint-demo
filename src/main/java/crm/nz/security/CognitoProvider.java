/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.security;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;

/**
 *
 * @author IESL
 */
public interface CognitoProvider {

    public AWSCognitoIdentityProvider identityProvider();

}
