/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADMIN
 */
public class Transaction implements ToObjectConverter {

    List<TransactionItem> Items;
    int TotalItemCount;
    TransactionTotals FieldTotals;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Transaction.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public List<TransactionItem> getItems() {
        return Items;
    }

    public void setItems(List<TransactionItem> Items) {
        this.Items = Items;
    }

    public int getTotalItemCount() {
        return TotalItemCount;
    }

    public void setTotalItemCount(int TotalItemCount) {
        this.TotalItemCount = TotalItemCount;
    }

    public TransactionTotals getFieldTotals() {
        return FieldTotals;
    }

    public void setFieldTotals(TransactionTotals FieldTotals) {
        this.FieldTotals = FieldTotals;
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
