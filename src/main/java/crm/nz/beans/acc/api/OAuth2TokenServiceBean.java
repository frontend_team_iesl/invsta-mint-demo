/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author innovative002
 */
public class OAuth2TokenServiceBean {
     //variables for GetOAuth2TokenService
    private String oAuth2TokenApiUrl, oAuth2TokenClientId, oAuth2TokenClientSecret;

    public OAuth2TokenServiceBean(String oAuth2TokenApiUrl, String oAuth2TokenClientId, String oAuth2TokenClientSecret) {
        this.oAuth2TokenApiUrl = oAuth2TokenApiUrl;
        this.oAuth2TokenClientId = oAuth2TokenClientId;
        this.oAuth2TokenClientSecret = oAuth2TokenClientSecret;
    }

    public String getoAuth2TokenApiUrl() {
        return oAuth2TokenApiUrl;
    }

    public void setoAuth2TokenApiUrl(String oAuth2TokenApiUrl) {
        this.oAuth2TokenApiUrl = oAuth2TokenApiUrl;
    }

    public String getoAuth2TokenClientId() {
        return oAuth2TokenClientId;
    }

    public void setoAuth2TokenClientId(String oAuth2TokenClientId) {
        this.oAuth2TokenClientId = oAuth2TokenClientId;
    }

    public String getoAuth2TokenClientSecret() {
        return oAuth2TokenClientSecret;
    }

    public void setoAuth2TokenClientSecret(String oAuth2TokenClientSecret) {
        this.oAuth2TokenClientSecret = oAuth2TokenClientSecret;
    }
    
    
}
