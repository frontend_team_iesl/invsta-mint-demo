/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * "InvestmentCode":"MIN793", "AsAt":"2019-07-17T00:00:00",
 * "PortfolioCode":"290002", "PortfolioName":"Australasian Equity Fund",
 * "Units":29532.6771, "TaxOwed":-44.9, "TaxPaid":507.53, "Price":3.4667,
 * "MarketValue":102380.93170257, "DistMethod":"Reinvested", "PortfolioScale":4,
 * "Volatility":0, "AverageFundReturn":0, "Contributions":70507.53,
 * "Withdrawals":0, "Fees":0, "CashDistributions":0,
 * "ReinvestedDistributions":0, "Earnings":31365.87170257, "ReturnRate":0
 *
 * @author IESL
 */
public class InvestmentHolding implements ToObjectConverter {

    private String InvestmentCode, AsAt, PortfolioCode, PortfolioName, DistMethod,ApplicationId, BeneficiaryId,name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }
    private Double Units, TaxOwed, TaxPaid, Price, MarketValue, PortfolioScale, Volatility, AverageFundReturn, Contributions,
            Withdrawals, Fees, CashDistributions, ReinvestedDistributions, Earnings, ReturnRate;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InvestmentHolding.class);

    public String getInvestmentCode() {
        return InvestmentCode;
    }

    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }

    public String getAsAt() {
        return AsAt;
    }

    public void setAsAt(String AsAt) {
        this.AsAt = AsAt;
    }

    public String getPortfolioCode() {
        return PortfolioCode;
    }

    public void setPortfolioCode(String PortfolioCode) {
        this.PortfolioCode = PortfolioCode;
    }

    public String getPortfolioName() {
        return PortfolioName;
    }

    public void setPortfolioName(String PortfolioName) {
        this.PortfolioName = PortfolioName;
    }

    public String getDistMethod() {
        return DistMethod;
    }

    public void setDistMethod(String DistMethod) {
        this.DistMethod = DistMethod;
    }

    public Double getUnits() {
        return Units;
    }

    public void setUnits(Double Units) {
        this.Units = Units;
    }

    public Double getTaxOwed() {
        return TaxOwed;
    }

    public void setTaxOwed(Double TaxOwed) {
        this.TaxOwed = TaxOwed;
    }

    public Double getTaxPaid() {
        return TaxPaid;
    }

    public void setTaxPaid(Double TaxPaid) {
        this.TaxPaid = TaxPaid;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }

    public Double getMarketValue() {
        return MarketValue;
    }

    public void setMarketValue(Double MarketValue) {
        this.MarketValue = MarketValue;
    }

    public Double getPortfolioScale() {
        return PortfolioScale;
    }

    public void setPortfolioScale(Double PortfolioScale) {
        this.PortfolioScale = PortfolioScale;
    }

    public Double getVolatility() {
        return Volatility;
    }

    public void setVolatility(Double Volatility) {
        this.Volatility = Volatility;
    }

    public Double getAverageFundReturn() {
        return AverageFundReturn;
    }

    public void setAverageFundReturn(Double AverageFundReturn) {
        this.AverageFundReturn = AverageFundReturn;
    }

    public Double getContributions() {
        return Contributions;
    }

    public void setContributions(Double Contributions) {
        this.Contributions = Contributions;
    }

    public Double getWithdrawals() {
        return Withdrawals;
    }

    public void setWithdrawals(Double Withdrawals) {
        this.Withdrawals = Withdrawals;
    }

    public Double getFees() {
        return Fees;
    }

    public void setFees(Double Fees) {
        this.Fees = Fees;
    }

    public Double getCashDistributions() {
        return CashDistributions;
    }

    public void setCashDistributions(Double CashDistributions) {
        this.CashDistributions = CashDistributions;
    }

    public Double getReinvestedDistributions() {
        return ReinvestedDistributions;
    }

    public void setReinvestedDistributions(Double ReinvestedDistributions) {
        this.ReinvestedDistributions = ReinvestedDistributions;
    }

    public Double getEarnings() {
        return Earnings;
    }

    public void setEarnings(Double Earnings) {
        this.Earnings = Earnings;
    }

    public Double getReturnRate() {
        return ReturnRate;
    }

    public void setReturnRate(Double ReturnRate) {
        this.ReturnRate = ReturnRate;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the BeneficiaryId
     */
    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    /**
     * @param BeneficiaryId the BeneficiaryId to set
     */
    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

}
