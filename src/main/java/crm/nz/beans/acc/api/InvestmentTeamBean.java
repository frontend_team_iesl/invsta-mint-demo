/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author innovative002
 */
public class InvestmentTeamBean {

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the pic_url
     */
    public String getPic_url() {
        return pic_url;
    }

    /**
     * @param pic_url the pic_url to set
     */
    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    /**
     * @return the portfolio
     */
    public String getPortfolio() {
        return portfolio;
    }

    /**
     * @param portfolio the portfolio to set
     */
    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    /**
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    public void setPlace(String place) {
        this.place = place;
    }

    private String id, name, role, pic_url, portfolio, place,team_bio;

    public String getTeam_bio() {
        return team_bio;
    }

    public void setTeam_bio(String team_bio) {
        this.team_bio = team_bio;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        try {
            Field[] declaredFields = THIS.getClass().getDeclaredFields();
            StringBuilder builder = new StringBuilder();
            builder.append("{");
            for (int i = 0; i < declaredFields.length; i++) {
                Field field = declaredFields[i];
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                builder.append("\"").append(fieldName).append("\"");
                builder.append(":");
                builder.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    builder.append(", \n");
                }
            }
            builder.append("}");
            return builder.toString();
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(InvestmentTeamBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "{}";
    }

}
