/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class UpdateUnitsHolding {

    private String Id, Portfolio, FundName, Unitholder, ClientName, IRDNo, Address1, Address2, Address4, Country, Postcode, Email,
            Units, Pirate, Date, Red, Balance, InvestorPercent, ExternalRef, DistMethod, Greeting, RWT, ZeroRatedFlag, InvestorType,
            Nationality, DateEntered, NFIInvestor, BeneficiaryId, AdvisorId, Password, UserId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UpdateUnitsHolding.class);

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    public String getFundName() {
        return FundName;
    }

    public void setFundName(String FundName) {
        this.FundName = FundName;
    }

    public String getUnitholder() {
        return Unitholder;
    }

    public void setUnitholder(String Unitholder) {
        this.Unitholder = Unitholder;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String ClientName) {
        this.ClientName = ClientName;
    }

    public String getIRDNo() {
        return IRDNo;
    }

    public void setIRDNo(String IRDNo) {
        this.IRDNo = IRDNo;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String Address1) {
        this.Address1 = Address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String Address2) {
        this.Address2 = Address2;
    }

    public String getAddress4() {
        return Address4;
    }

    public void setAddress4(String Address4) {
        this.Address4 = Address4;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String Postcode) {
        this.Postcode = Postcode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getUnits() {
        return Units;
    }

    public void setUnits(String Units) {
        this.Units = Units;
    }

    public String getPirate() {
        return Pirate;
    }

    public void setPirate(String Pirate) {
        this.Pirate = Pirate;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getRed() {
        return Red;
    }

    public void setRed(String Red) {
        this.Red = Red;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String Balance) {
        this.Balance = Balance;
    }

    public String getInvestorPercent() {
        return InvestorPercent;
    }

    public void setInvestorPercent(String InvestorPercent) {
        this.InvestorPercent = InvestorPercent;
    }

    public String getExternalRef() {
        return ExternalRef;
    }

    public void setExternalRef(String ExternalRef) {
        this.ExternalRef = ExternalRef;
    }

    public String getDistMethod() {
        return DistMethod;
    }

    public void setDistMethod(String DistMethod) {
        this.DistMethod = DistMethod;
    }

    public String getGreeting() {
        return Greeting;
    }

    public void setGreeting(String Greeting) {
        this.Greeting = Greeting;
    }

    public String getRWT() {
        return RWT;
    }

    public void setRWT(String RWT) {
        this.RWT = RWT;
    }

    public String getZeroRatedFlag() {
        return ZeroRatedFlag;
    }

    public void setZeroRatedFlag(String ZeroRatedFlag) {
        this.ZeroRatedFlag = ZeroRatedFlag;
    }

    public String getInvestorType() {
        return InvestorType;
    }

    public void setInvestorType(String InvestorType) {
        this.InvestorType = InvestorType;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String Nationality) {
        this.Nationality = Nationality;
    }

    public String getDateEntered() {
        return DateEntered;
    }

    public void setDateEntered(String DateEntered) {
        this.DateEntered = DateEntered;
    }

    public String getNFIInvestor() {
        return NFIInvestor;
    }

    public void setNFIInvestor(String NFIInvestor) {
        this.NFIInvestor = NFIInvestor;
    }

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    public String getAdvisorId() {
        return AdvisorId;
    }

    public void setAdvisorId(String AdvisorId) {
        this.AdvisorId = AdvisorId;
    }

}
