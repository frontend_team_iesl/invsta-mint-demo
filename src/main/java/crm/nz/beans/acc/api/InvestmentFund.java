/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import crm.nz.constants.Constants;
import static crm.nz.table.bean.ToObjectConverter.checkNull;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author TOSHIBA R830
 */
public class InvestmentFund {
    private String id, Portfolio, FundOverview, RiskIndicator, MonthlyFundUpdate, FeaturesOftheFund, monthlyReturnValue;
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InvestmentFund.class);
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the Portfolio
     */
    public String getPortfolio() {
        return Portfolio;
    }

    /**
     * @param Portfolio the Portfolio to set
     */
    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    /**
     * @return the FundOverview
     */
    public String getFundOverview() {
        return FundOverview;
    }

    /**
     * @param FundOverview the FundOverview to set
     */
    public void setFundOverview(String FundOverview) {
        this.FundOverview = FundOverview;
    }

    /**
     * @return the RiskIndicator
     */
    public String getRiskIndicator() {
        return RiskIndicator;
    }

    /**
     * @param RiskIndicator the RiskIndicator to set
     */
    public void setRiskIndicator(String RiskIndicator) {
        this.RiskIndicator = RiskIndicator;
    }

    /**
     * @return the MonthlyFundUpdate
     */
    public String getMonthlyFundUpdate() {
        return MonthlyFundUpdate;
    }

    /**
     * @param MonthlyFundUpdate the MonthlyFundUpdate to set
     */
    public void setMonthlyFundUpdate(String MonthlyFundUpdate) {
        this.MonthlyFundUpdate = MonthlyFundUpdate;
    }

    /**
     * @return the FeaturesOftheFund
     */
    public String getFeaturesOftheFund() {
        return FeaturesOftheFund;
    }

    /**
     * @param FeaturesOftheFund the FeaturesOftheFund to set
     */
    public void setFeaturesOftheFund(String FeaturesOftheFund) {
        this.FeaturesOftheFund = FeaturesOftheFund;
    }

    /**
     * @return the monthlyReturnValue
     */
    public String getMonthlyReturnValue() {
        return monthlyReturnValue;
    }

    /**
     * @param monthlyReturnValue the monthlyReturnValue to set
     */
    public void setMonthlyReturnValue(String monthlyReturnValue) {
        this.monthlyReturnValue = monthlyReturnValue;
    }
    
     @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
//                        checkNull = encrypt();
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
