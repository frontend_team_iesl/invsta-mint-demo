/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class Application implements ToObjectConverter{
 
    /**
     * @return the ApplicationId
     */
     private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Application.class);
    
    public String getApplicationId() {
        return ApplicationId;
    }

    /**
     * @param ApplicationId the ApplicationId to set
     */
    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    /**
     * @return the ExternalReference
     */
    public String getExternalReference() {
        return ExternalReference;
    }

    /**
     * @param ExternalReference the ExternalReference to set
     */
    public void setExternalReference(String ExternalReference) {
        this.ExternalReference = ExternalReference;
    }

    /**
     * @return the PrimaryBeneficiaryId
     */
    public String getPrimaryBeneficiaryId() {
        return PrimaryBeneficiaryId;
    }

    /**
     * @param PrimaryBeneficiaryId the PrimaryBeneficiaryId to set
     */
    public void setPrimaryBeneficiaryId(String PrimaryBeneficiaryId) {
        this.PrimaryBeneficiaryId = PrimaryBeneficiaryId;
    }

    /**
     * @return the ApplicationType
     */
    public String getApplicationType() {
        return ApplicationType;
    }

    /**
     * @param ApplicationType the ApplicationType to set
     */
    public void setApplicationType(String ApplicationType) {
        this.ApplicationType = ApplicationType;
    }

    /**
     * @return the ApplicationSource
     */
    public String getApplicationSource() {
        return ApplicationSource;
    }

    /**
     * @param ApplicationSource the ApplicationSource to set
     */
    public void setApplicationSource(String ApplicationSource) {
        this.ApplicationSource = ApplicationSource;
    }

    /**
     * @return the CreatedDate
     */
    public String getCreatedDate() {
        return CreatedDate;
    }

    /**
     * @param CreatedDate the CreatedDate to set
     */
    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /**
     * @return the Beneficiaries
     */
    public List<Beneficiary> getBeneficiaries() {
        return Beneficiaries;
    }

    /**
     * @param Beneficiaries the Beneficiaries to set
     */
    public void setBeneficiaries(List<Beneficiary> Beneficiaries) {
        this.Beneficiaries = Beneficiaries;
    }

    /**
     * @return the BankAccounts
     */
    public List<BankAccountDetail> getBankAccounts() {
        return BankAccounts;
    }

    /**
     * @param BankAccounts the BankAccounts to set
     */
    public void setBankAccounts(List<BankAccountDetail> BankAccounts) {
        this.BankAccounts = BankAccounts;
    }

    /**
     * @return the Investments
     */
    public List<Investment> getInvestments() {
        return Investments;
    }

    /**
     * @param Investments the Investments to set
     */
    public void setInvestments(List<Investment> Investments) {
        this.Investments = Investments;
    }

    /**
     * @return the Documents
     */
    public List<Document> getDocuments() {
        return Documents;
    }

    /**
     * @param Documents the Documents to set
     */
    public void setDocuments(List<Document> Documents) {
        this.Documents = Documents;
    }

    /**
     * @return the Status
     */
    public Status getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(Status Status) {
        this.Status = Status;
    }
    private String ApplicationId;
    private String ExternalReference;
    private String PrimaryBeneficiaryId;
    private String ApplicationType;
    private String ApplicationSource;
    private String CreatedDate;
    private List<Beneficiary> Beneficiaries;
    private List<BankAccountDetail> BankAccounts;
    private List<Investment> Investments;
    private List<Document> Documents;
    private Status Status;

     @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
    
}
