/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class ClientInfo {

    private String Id, InvestmentCode, ClientEmail, ClientName, Type, IRDNo, Address1, Address2, Address3, Address4, Country,
            Postcode, TelHome, TelBusiness, TelMobile, Email, BankAccount, BankAccount2, Units, PIRRate, Date, Red, Balance,
            InvestorPercent, ExternalRef, Advisor, DistMethod, Greeting, RWT, ExemptCert, ZeroRatedFlag, InterfunderCode,
            PreferredComm, DOB, StartDate, InvestorType, Nationality, PlaceOfBirth, DateEntered, NFIInvestor;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ClientInfo.class);

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getInvestmentCode() {
        return InvestmentCode;
    }

    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }

    public String getClientEmail() {
        return ClientEmail;
    }

    public void setClientEmail(String ClientEmail) {
        this.ClientEmail = ClientEmail;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String ClientName) {
        this.ClientName = ClientName;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getIRDNo() {
        return IRDNo;
    }

    public void setIRDNo(String IRDNo) {
        this.IRDNo = IRDNo;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String Address1) {
        this.Address1 = Address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String Address2) {
        this.Address2 = Address2;
    }

    public String getAddress3() {
        return Address3;
    }

    public void setAddress3(String Address3) {
        this.Address3 = Address3;
    }

    public String getAddress4() {
        return Address4;
    }

    public void setAddress4(String Address4) {
        this.Address4 = Address4;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String Postcode) {
        this.Postcode = Postcode;
    }

    public String getTelHome() {
        return TelHome;
    }

    public void setTelHome(String TelHome) {
        this.TelHome = TelHome;
    }

    public String getTelBusiness() {
        return TelBusiness;
    }

    public void setTelBusiness(String TelBusiness) {
        this.TelBusiness = TelBusiness;
    }

    public String getTelMobile() {
        return TelMobile;
    }

    public void setTelMobile(String TelMobile) {
        this.TelMobile = TelMobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getBankAccount() {
        return BankAccount;
    }

    public void setBankAccount(String BankAccount) {
        this.BankAccount = BankAccount;
    }

    public String getBankAccount2() {
        return BankAccount2;
    }

    public void setBankAccount2(String BankAccount2) {
        this.BankAccount2 = BankAccount2;
    }

    public String getUnits() {
        return Units;
    }

    public void setUnits(String Units) {
        this.Units = Units;
    }

    public String getPIRRate() {
        return PIRRate;
    }

    public void setPIRRate(String PIRRate) {
        this.PIRRate = PIRRate;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getRed() {
        return Red;
    }

    public void setRed(String Red) {
        this.Red = Red;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String Balance) {
        this.Balance = Balance;
    }

    public String getInvestorPercent() {
        return InvestorPercent;
    }

    public void setInvestorPercent(String InvestorPercent) {
        this.InvestorPercent = InvestorPercent;
    }

    public String getExternalRef() {
        return ExternalRef;
    }

    public void setExternalRef(String ExternalRef) {
        this.ExternalRef = ExternalRef;
    }

    public String getAdvisor() {
        return Advisor;
    }

    public void setAdvisor(String Advisor) {
        this.Advisor = Advisor;
    }

    public String getDistMethod() {
        return DistMethod;
    }

    public void setDistMethod(String DistMethod) {
        this.DistMethod = DistMethod;
    }

    public String getGreeting() {
        return Greeting;
    }

    public void setGreeting(String Greeting) {
        this.Greeting = Greeting;
    }

    public String getRWT() {
        return RWT;
    }

    public void setRWT(String RWT) {
        this.RWT = RWT;
    }

    public String getExemptCert() {
        return ExemptCert;
    }

    public void setExemptCert(String ExemptCert) {
        this.ExemptCert = ExemptCert;
    }

    public String getZeroRatedFlag() {
        return ZeroRatedFlag;
    }

    public void setZeroRatedFlag(String ZeroRatedFlag) {
        this.ZeroRatedFlag = ZeroRatedFlag;
    }

    public String getInterfunderCode() {
        return InterfunderCode;
    }

    public void setInterfunderCode(String InterfunderCode) {
        this.InterfunderCode = InterfunderCode;
    }

    public String getPreferredComm() {
        return PreferredComm;
    }

    public void setPreferredComm(String PreferredComm) {
        this.PreferredComm = PreferredComm;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public String getInvestorType() {
        return InvestorType;
    }

    public void setInvestorType(String InvestorType) {
        this.InvestorType = InvestorType;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String Nationality) {
        this.Nationality = Nationality;
    }

    public String getPlaceOfBirth() {
        return PlaceOfBirth;
    }

    public void setPlaceOfBirth(String PlaceOfBirth) {
        this.PlaceOfBirth = PlaceOfBirth;
    }

    public String getDateEntered() {
        return DateEntered;
    }

    public void setDateEntered(String DateEntered) {
        this.DateEntered = DateEntered;
    }

    public String getNFIInvestor() {
        return NFIInvestor;
    }

    public void setNFIInvestor(String NFIInvestor) {
        this.NFIInvestor = NFIInvestor;
    }

}
