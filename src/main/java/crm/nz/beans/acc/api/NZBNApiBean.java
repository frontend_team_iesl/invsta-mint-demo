/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author innovative002
 */
public class NZBNApiBean {
    //variables for nzbn api
    private String nzbnTokenUrl, nzbnClientId, nzbnClientSecret;

    public NZBNApiBean(String nzbnTokenUrl, String nzbnClientId, String nzbnClientSecret) {
        this.nzbnTokenUrl = nzbnTokenUrl;
        this.nzbnClientId = nzbnClientId;
        this.nzbnClientSecret = nzbnClientSecret;
    }

    
    public String getNzbnTokenUrl() {
        return nzbnTokenUrl;
    }

    public void setNzbnTokenUrl(String nzbnTokenUrl) {
        this.nzbnTokenUrl = nzbnTokenUrl;
    }

    public String getNzbnClientId() {
        return nzbnClientId;
    }

    public void setNzbnClientId(String nzbnClientId) {
        this.nzbnClientId = nzbnClientId;
    }

    public String getNzbnClientSecret() {
        return nzbnClientSecret;
    }

    public void setNzbnClientSecret(String nzbnClientSecret) {
        this.nzbnClientSecret = nzbnClientSecret;
    }
    
    
}
