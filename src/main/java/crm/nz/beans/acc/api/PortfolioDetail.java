/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ADMIN
 */
public class PortfolioDetail implements ToObjectConverter {

    private String id, portfolio_pic,Portfolio,fundName, FundOverview, RiskIndicator, MonthlyFundUpdate, FeaturesOftheFund,bio, monthlyReturnValue;
private MultipartFile portfolio_image;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PortfolioDetail.class);
    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getPortfolio_pic() {
        return portfolio_pic;
    }

    public void setPortfolio_pic(String portfolio_pic) {
        this.portfolio_pic = portfolio_pic;
    }

    public MultipartFile getPortfolio_image() {
        return portfolio_image;
    }

    public void setPortfolio_image(MultipartFile portfolio_image) {
        this.portfolio_image = portfolio_image;
    }





    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    public String getFundOverview() {
        return FundOverview;
    }

    public void setFundOverview(String FundOverview) {
        this.FundOverview = FundOverview;
    }

    public String getRiskIndicator() {
        return RiskIndicator;
    }

    public void setRiskIndicator(String RiskIndicator) {
        this.RiskIndicator = RiskIndicator;
    }

    public String getMonthlyFundUpdate() {
        return MonthlyFundUpdate;
    }

    public void setMonthlyFundUpdate(String MonthlyFundUpdate) {
        this.MonthlyFundUpdate = MonthlyFundUpdate;
    }

    public String getFeaturesOftheFund() {
        return FeaturesOftheFund;
    }

    public void setFeaturesOftheFund(String FeaturesOftheFund) {
        this.FeaturesOftheFund = FeaturesOftheFund;
    }

    public String getMonthlyReturnValue() {
        return monthlyReturnValue;
    }

    public void setMonthlyReturnValue(String monthlyReturnValue) {
        this.monthlyReturnValue = monthlyReturnValue;
    }
    
    

}
