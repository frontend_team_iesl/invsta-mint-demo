/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import java.util.HashMap;

/**
 *
 * @author IESL
 */
public class DriverLicence {

    private Integer status;
    private Boolean verified;
    private String safeHarbourScore;
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private HashMap<String, String> fields;

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * @param verified the verified to set
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /**
     * @return the safeHarbourScore
     */
    public String getSafeHarbourScore() {
        return safeHarbourScore;
    }

    /**
     * @param safeHarbourScore the safeHarbourScore to set
     */
    public void setSafeHarbourScore(String safeHarbourScore) {
        this.safeHarbourScore = safeHarbourScore;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the fields
     */
    public HashMap<String, String> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(HashMap<String, String> fields) {
        this.fields = fields;
    }
}
