/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author innovative002
 */
public class DataZooServicebean {
    
        //variables for Datazooo services
    private String dataZooUsername, dataZooPassword, dataZooApiUrl;

    public DataZooServicebean(String dataZooUsername, String dataZooPassword, String dataZooApiUrl) {
        this.dataZooUsername = dataZooUsername;
        this.dataZooPassword = dataZooPassword;
        this.dataZooApiUrl = dataZooApiUrl;
    }
    
    public String getDataZooUsername() {
        return dataZooUsername;
    }

    public void setDataZooUsername(String dataZooUsername) {
        this.dataZooUsername = dataZooUsername;
    }

    public String getDataZooPassword() {
        return dataZooPassword;
    }

    public void setDataZooPassword(String dataZooPassword) {
        this.dataZooPassword = dataZooPassword;
    }

    public String getDataZooApiUrl() {
        return dataZooApiUrl;
    }

    public void setDataZooApiUrl(String dataZooApiUrl) {
        this.dataZooApiUrl = dataZooApiUrl;
    }
    
}
