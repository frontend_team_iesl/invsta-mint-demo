/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author innovative002
 */
public class JointDetailBean implements ToObjectConverter {

    private String country, codeISO, codeUN, mobileCode, dialingCode;
    private String email, password, title, fullName, date_of_Birth, country_residence, occupation,pepStatus,
            working_with_adviser, advisor_company, advisor, homeAddress, mobile_country_code, mobile_number,
            optional_num_type, optional_num_code, optional_num, id_type, license_number, licence_expiry_Date,
            licence_verson_number, passport_number, passport_expiry, passport_issue_by, other_id_type, otherFile, bankFile,
            other_id_expiry, other_id_issueBy, pir, ird_Number, reg_id, id, tin, Gender, send_email, raw_password,
            wealth_src, resn_tin_unavailable, bank_name, acount_holder_name, account_number, preferred_name,
            firstName, lastName, middleName, occupationStatus, investor_idverified,
            UserDefined1, UserDefined2, UserDefined3, UserDefined4, UserDefined5, UserDefined6, UserDefined7, UserDefined8, UserDefined9, UserDefined10;
    private boolean ConsentToCheckId;

    public String getOtherFile() {
        return otherFile;
    }

    public void setOtherFile(String otherFile) {
        this.otherFile = otherFile;
    }

    public String getBankFile() {
        return bankFile;
    }

    public void setBankFile(String bankFile) {
        this.bankFile = bankFile;
    }

    public String getPepStatus() {
        return pepStatus;
    }

    public void setPepStatus(String pepStatus) {
        this.pepStatus = pepStatus;
    }

    
    
    
    public boolean isConsentToCheckId() {
        return ConsentToCheckId;
    }

    public void setConsentToCheckId(boolean ConsentToCheckId) {
        this.ConsentToCheckId = ConsentToCheckId;
    }

    public String getUserDefined1() {
        return UserDefined1;
    }

    public void setUserDefined1(String UserDefined1) {
        this.UserDefined1 = UserDefined1;
    }

    public String getUserDefined2() {
        return UserDefined2;
    }

    public void setUserDefined2(String UserDefined2) {
        this.UserDefined2 = UserDefined2;
    }

    public String getUserDefined3() {
        return UserDefined3;
    }

    public void setUserDefined3(String UserDefined3) {
        this.UserDefined3 = UserDefined3;
    }

    public String getUserDefined4() {
        return UserDefined4;
    }

    public void setUserDefined4(String UserDefined4) {
        this.UserDefined4 = UserDefined4;
    }

    public String getUserDefined5() {
        return UserDefined5;
    }

    public void setUserDefined5(String UserDefined5) {
        this.UserDefined5 = UserDefined5;
    }

    public String getUserDefined6() {
        return UserDefined6;
    }

    public void setUserDefined6(String UserDefined6) {
        this.UserDefined6 = UserDefined6;
    }

    public String getUserDefined7() {
        return UserDefined7;
    }

    public void setUserDefined7(String UserDefined7) {
        this.UserDefined7 = UserDefined7;
    }

    public String getUserDefined8() {
        return UserDefined8;
    }

    public void setUserDefined8(String UserDefined8) {
        this.UserDefined8 = UserDefined8;
    }

    public String getUserDefined9() {
        return UserDefined9;
    }

    public void setUserDefined9(String UserDefined9) {
        this.UserDefined9 = UserDefined9;
    }

    public String getUserDefined10() {
        return UserDefined10;
    }

    public void setUserDefined10(String UserDefined10) {
        this.UserDefined10 = UserDefined10;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCodeISO() {
        return codeISO;
    }

    public void setCodeISO(String codeISO) {
        this.codeISO = codeISO;
    }

    public String getCodeUN() {
        return codeUN;
    }

    public void setCodeUN(String codeUN) {
        this.codeUN = codeUN;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getWealth_src() {
        return wealth_src;
    }

    public void setWealth_src(String wealth_src) {
        this.wealth_src = wealth_src;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getSend_email() {
        return send_email;
    }

    public void setSend_email(String send_email) {
        this.send_email = send_email;
    }
    private String token, link, step, status, reg_type, isUSCitizen, created_ts, created_by, curr_idx;
    private String ApplicationId, BeneficiaryId;
    private Boolean IsUSTaxResident;

    public String getInvestor_idverified() {
        return investor_idverified;
    }

    public void setInvestor_idverified(String investor_idverified) {
        this.investor_idverified = investor_idverified;
    }

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    public Boolean getIsUSTaxResident() {
        return IsUSTaxResident;
    }

    public void setIsUSTaxResident(Boolean IsUSTaxResident) {
        this.IsUSTaxResident = IsUSTaxResident;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupationStatus() {
        return occupationStatus;
    }

    public void setOccupationStatus(String occupationStatus) {
        this.occupationStatus = occupationStatus;
    }

    public String getCurr_idx() {
        return curr_idx;
    }

    public void setCurr_idx(String curr_idx) {
        this.curr_idx = curr_idx;
    }
    private List<CountryTINBean> countryTINList;
    private List<JointDetailBean> moreInvestorList;

    public String getId() {
        return id;
    }

    public String getPreferred_name() {
        return preferred_name;
    }

    public void setPreferred_name(String preferred_name) {
        this.preferred_name = preferred_name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReg_id() {
        return reg_id;
    }

    public void setReg_id(String reg_id) {
        this.reg_id = reg_id;
    }

    public List<CountryTINBean> getCountryTINList() {
        return countryTINList;
    }

    public void setCountryTINList(List<CountryTINBean> countryTINList) {
        this.countryTINList = countryTINList;
    }

    public String getIrd_Number() {
        return ird_Number;
    }

    public void setIrd_Number(String ird_Number) {
        this.ird_Number = ird_Number;
    }

    public String getCreated_ts() {
        return created_ts;
    }

    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getIsUSCitizen() {
        return isUSCitizen;
    }

    public void setIsUSCitizen(String isUSCitizen) {
        this.isUSCitizen = isUSCitizen;
    }

    public String getReg_type() {
        return reg_type;
    }

    public void setReg_type(String reg_type) {
        this.reg_type = reg_type;
    }
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JointDetailBean.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate_of_Birth() {
        return date_of_Birth;
    }

    public void setDate_of_Birth(String date_of_Birth) {
        this.date_of_Birth = date_of_Birth;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPir() {
        return pir;
    }

    public void setPir(String pir) {
        this.pir = pir;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCountry_residence() {
        return country_residence;
    }

    public void setCountry_residence(String country_residence) {
        this.country_residence = country_residence;
    }

    public String getWorking_with_adviser() {
        return working_with_adviser;
    }

    public void setWorking_with_adviser(String working_with_adviser) {
        this.working_with_adviser = working_with_adviser;
    }

    public String getAdvisor_company() {
        return advisor_company;
    }

    public void setAdvisor_company(String advisor_company) {
        this.advisor_company = advisor_company;
    }

    public String getAdvisor() {
        return advisor;
    }

    public void setAdvisor(String advisor) {
        this.advisor = advisor;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getMobile_country_code() {
        return mobile_country_code;
    }

    public void setMobile_country_code(String mobile_country_code) {
        this.mobile_country_code = mobile_country_code;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getOptional_num_type() {
        return optional_num_type;
    }

    public void setOptional_num_type(String optional_num_type) {
        this.optional_num_type = optional_num_type;
    }

    public String getOptional_num_code() {
        return optional_num_code;
    }

    public void setOptional_num_code(String optional_num_code) {
        this.optional_num_code = optional_num_code;
    }

    public String getOptional_num() {
        return optional_num;
    }

    public void setOptional_num(String optional_num) {
        this.optional_num = optional_num;
    }

    public String getId_type() {
        return id_type;
    }

    public void setId_type(String id_type) {
        this.id_type = id_type;
    }

    public String getLicense_number() {
        return license_number;
    }

    public void setLicense_number(String license_number) {
        this.license_number = license_number;
    }

    public String getLicence_expiry_Date() {
        return licence_expiry_Date;
    }

    public void setLicence_expiry_Date(String licence_expiry_Date) {
        this.licence_expiry_Date = licence_expiry_Date;
    }

    public String getLicence_verson_number() {
        return licence_verson_number;
    }

    public void setLicence_verson_number(String licence_verson_number) {
        this.licence_verson_number = licence_verson_number;
    }

    public String getPassport_number() {
        return passport_number;
    }

    public void setPassport_number(String passport_number) {
        this.passport_number = passport_number;
    }

    public String getPassport_expiry() {
        return passport_expiry;
    }

    public void setPassport_expiry(String passport_expiry) {
        this.passport_expiry = passport_expiry;
    }

    public String getPassport_issue_by() {
        return passport_issue_by;
    }

    public void setPassport_issue_by(String passport_issue_by) {
        this.passport_issue_by = passport_issue_by;
    }

    public String getOther_id_type() {
        return other_id_type;
    }

    public void setOther_id_type(String other_id_type) {
        this.other_id_type = other_id_type;
    }

    public String getOther_id_expiry() {
        return other_id_expiry;
    }

    public void setOther_id_expiry(String other_id_expiry) {
        this.other_id_expiry = other_id_expiry;
    }

    public String getOther_id_issueBy() {
        return other_id_issueBy;
    }

    public void setOther_id_issueBy(String other_id_issueBy) {
        this.other_id_issueBy = other_id_issueBy;
    }

    public String getResn_tin_unavailable() {
        return resn_tin_unavailable;
    }

    public void setResn_tin_unavailable(String resn_tin_unavailable) {
        this.resn_tin_unavailable = resn_tin_unavailable;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAcount_holder_name() {
        return acount_holder_name;
    }

    public void setAcount_holder_name(String acount_holder_name) {
        this.acount_holder_name = acount_holder_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<JointDetailBean> getMoreInvestorList() {
        return moreInvestorList;
    }

    public void setMoreInvestorList(List<JointDetailBean> moreInvestorList) {
        this.moreInvestorList = moreInvestorList;
    }

    /**
     * @return the tin
     */
    public String getTin() {
        return tin;
    }

    /**
     * @param tin the tin to set
     */
    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getThirdAPICreateDetails(JointDetailBean joint) {
        return "{\n"
                + "  \"ApplicationId\": " + ApplicationId + ",\n"
                + "  \"BeneficiaryId\": " + BeneficiaryId + ",\n"
                + "  \"Title\": \"" + joint.getTitle() + "\",\n"
                + "  \"FirstName\": \"" + joint.getFirstName() + "\",\n"
                + "  \"SecondName\": " + (joint.getFullName() != null ? "\"" + joint.getFullName() + "\"" : null) + ",\n"
                + "  \"LastName\": " + (joint.getLastName() != null ? "\"" + joint.getLastName() + "\"" : null) + ",\n"
                + "  \"MiddleName\": " + (joint.getMiddleName() != null ? "\"" + joint.getMiddleName() + "\"" : null) + ",\n"
                + "  \"PreferredName\": \"" + joint.getPreferred_name() + "\",\n"
                + "  \"Gender\": " + (Gender != null ? "\"" + Gender + "\"" : null) + ",\n"
                + "  \"DateOfBirth\": \"" + joint.getDate_of_Birth() + "\",\n"
                + "  \"PlaceOfBirth\": \"" + joint.getCodeISO() + "\",\n"
                + "  \"Nation\": \"" + joint.getCodeISO() + "\",\n"
                + "  \"Occupation\": " + joint.getOccupation() + ",\n"
                + "  \"OccupationStatus\": \"" + joint.getOccupationStatus() + "\",\n"
                + "  \"IRDNo\": \"" + joint.getIrd_Number().replaceAll("-", "") + "\",\n"
                + "  \"IsUSTaxResident\": " + IsUSTaxResident + ",\n"
                + "  \"PIRRate\": " + joint.getPir().replaceAll("%", "") + ",\n"
                + "  \"ConsentToCheckId\": " + ConsentToCheckId + ",\n"
                + "  \"Email\": \"" + joint.getEmail() + "\",\n"
                + "  \"SecondaryEmail\": \"" + joint.getEmail() + "\",\n"
                + "  \"PreferredComm\": " + null + ",\n"
                + "  \"TINNumber\": " + (joint.getCountryTINList().size() > 0 ? "\"" + joint.getCountryTINList().get(0).getTin() + "\"" : null) + ",\n"
                + "  \"TINNumber2\": " + (joint.getCountryTINList().size() > 1 ? "\"" + joint.getCountryTINList().get(1).getTin() + "\"" : null) + ",\n"
                + "  \"TINNumber3\": " + (joint.getCountryTINList().size() > 2 ? "\"" + joint.getCountryTINList().get(2).getTin() + "\"" : null) + ",\n"
                + "  \"TINCountry\": " + (joint.getCountryTINList().size() > 0 ? "\"" + joint.getCountryTINList().get(0).getCountry() + "\"" : null) + ",\n"
                + "  \"TINCountry2\": " + (joint.getCountryTINList().size() > 1 ? "\"" + joint.getCountryTINList().get(1).getCountry() + "\"" : null) + ",\n"
                + "  \"TINCountry3\": " + (joint.getCountryTINList().size() > 2 ? "\"" + joint.getCountryTINList().get(2).getCountry() + "\"" : null) + ",\n"
                + "  \"CountryOfResidency\": \"" + joint.getCodeISO() + "\",\n"
                + "  \"ExternalReference\": \"INV" + joint.getApplicationId() + "\",\n"
                + "  \"UserDefined1\": " + (UserDefined1 != null ? "\"" + UserDefined1 + "\"" : null) + ",\n"
                + "  \"UserDefined2\": " + (UserDefined2 != null ? "\"" + UserDefined2 + "\"" : null) + ",\n"
                + "  \"UserDefined3\": " + (UserDefined3 != null ? "\"" + UserDefined3 + "\"" : null) + ",\n"
                + "  \"UserDefined4\": " + (UserDefined4 != null ? "\"" + UserDefined4 + "\"" : null) + ",\n"
                + "  \"UserDefined5\": " + (UserDefined5 != null ? "\"" + UserDefined5 + "\"" : null) + ",\n"
                + "  \"UserDefined6\": " + (UserDefined6 != null ? "\"" + UserDefined6 + "\"" : null) + ",\n"
                + "  \"UserDefined7\": " + (UserDefined7 != null ? "\"" + UserDefined7 + "\"" : null) + ",\n"
                + "  \"UserDefined8\": " + (UserDefined8 != null ? "\"" + UserDefined8 + "\"" : null) + ",\n"
                + "  \"UserDefined9\": " + (UserDefined9 != null ? "\"" + UserDefined9 + "\"" : null) + ",\n"
                + "  \"UserDefined10\": " + (UserDefined10 != null ? "\"" + UserDefined10 + "\"" : null) + "\n"
                + "}";
    }

    public String getRaw_password() {
        return raw_password;
    }

    public void setRaw_password(String raw_password) {
        this.raw_password = raw_password;
    }

}
