/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author TOSHIBA R830
 */
public class userInfobean {
   private String user,investment,amount;
          private static final org.slf4j.Logger logger = LoggerFactory.getLogger(userInfobean.class);


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getInvestment() {
        return investment;
    }

    public void setInvestment(String investment) {
        this.investment = investment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
       @Override
    public String toString() {
        return toString(this);
    }
   
    
     public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
     
     
}
