/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author ADMIN
 */
public class CognitoUserPoolBean {
    
    String poolId, clientId;

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public CognitoUserPoolBean(String poolId, String clientId) {
        this.poolId = poolId;
        this.clientId = clientId;
    }
    
    
    
}
