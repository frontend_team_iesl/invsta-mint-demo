/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class InvestmentPerformance implements ToObjectConverter {

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
    
    String Portfolio, PeriodStartDate, PeriodEndDate;
    private String BeneficiaryId, InvestmentCode;
    Double XIRRReturnRate, PeriodsCovered;
    boolean InsufficientData, IsTotalPeriod;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InvestmentPerformance.class);

    public String getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    public String getPeriodStartDate() {
        return PeriodStartDate;
    }

    public void setPeriodStartDate(String PeriodStartDate) {
        this.PeriodStartDate = PeriodStartDate;
    }

    public String getPeriodEndDate() {
        return PeriodEndDate;
    }

    public void setPeriodEndDate(String PeriodEndDate) {
        this.PeriodEndDate = PeriodEndDate;
    }

    public Double getXIRRReturnRate() {
        return XIRRReturnRate;
    }

    public void setXIRRReturnRate(Double XIRRReturnRate) {
        this.XIRRReturnRate = XIRRReturnRate;
    }

    public Double getPeriodsCovered() {
        return PeriodsCovered;
    }

    public void setPeriodsCovered(Double PeriodsCovered) {
        this.PeriodsCovered = PeriodsCovered;
    }

    public boolean isInsufficientData() {
        return InsufficientData;
    }

    public void setInsufficientData(boolean InsufficientData) {
        this.InsufficientData = InsufficientData;
    }

    public boolean isIsTotalPeriod() {
        return IsTotalPeriod;
    }

    public void setIsTotalPeriod(boolean IsTotalPeriod) {
        this.IsTotalPeriod = IsTotalPeriod;
    }

    /**
     * @return the BeneficiaryId
     */
    public String getBeneficiaryId() {
        return BeneficiaryId;
    }

    /**
     * @param BeneficiaryId the BeneficiaryId to set
     */
    public void setBeneficiaryId(String BeneficiaryId) {
        this.BeneficiaryId = BeneficiaryId;
    }

    /**
     * @return the InvestmentCode
     */
    public String getInvestmentCode() {
        return InvestmentCode;
    }

    /**
     * @param InvestmentCode the InvestmentCode to set
     */
    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }

}
