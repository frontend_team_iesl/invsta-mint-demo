/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

/**
 *
 * @author IESL
 */
public class ApiCall {

    /**
     * @return the Id
     */
    public String getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return the ActionName
     */
    public String getActionName() {
        return ActionName;
    }

    /**
     * @param ActionName the ActionName to set
     */
    public void setActionName(String ActionName) {
        this.ActionName = ActionName;
    }

    /**
     * @return the CalledDate
     */
    public String getCalledDate() {
        return CalledDate;
    }

    /**
     * @param CalledDate the CalledDate to set
     */
    public void setCalledDate(String CalledDate) {
        this.CalledDate = CalledDate;
    }

    /**
     * @return the Active
     */
    public String getActive() {
        return Active;
    }

    /**
     * @param Active the Active to set
     */
    public void setActive(String Active) {
        this.Active = Active;
    }

    /**
     * @return the InvestmentCode
     */
    public String getInvestmentCode() {
        return InvestmentCode;
    }

    /**
     * @param InvestmentCode the InvestmentCode to set
     */
    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }
    private String Id;
    private String ActionName;
    private String CalledDate;
    private String Active;
    private String InvestmentCode;
    private String PortfolioCode;

    /**
     * @return the PortfolioCode
     */
    public String getPortfolioCode() {
        return PortfolioCode;
    }

    /**
     * @param PortfolioCode the PortfolioCode to set
     */
    public void setPortfolioCode(String PortfolioCode) {
        this.PortfolioCode = PortfolioCode;
    }
}
