/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IESL
 */
public class PortfolioPrice implements ToObjectConverter {

    private String CreatedDate, active;
    private String PortfolioCode;
    private String Portfolio;
    private String Date;
    private String id;

    private Double Prices, BidPrice, AskPrice, LastPrice, MidPrice;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PortfolioPrice.class);

    public String getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(String Portfolio) {
        this.Portfolio = Portfolio;
    }

    public String getId() {
        return id;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the PortfolioCode
     */
    public String getPortfolioCode() {
        return PortfolioCode;
    }

    /**
     * @param PortfolioCode the PortfolioCode to set
     */
    public void setPortfolioCode(String PortfolioCode) {
        this.PortfolioCode = PortfolioCode;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    public Double getBidPrice() {
        return BidPrice;
    }

    public void setBidPrice(Double BidPrice) {
        this.BidPrice = BidPrice;
    }

    public Double getAskPrice() {
        return AskPrice;
    }

    public void setAskPrice(Double AskPrice) {
        this.AskPrice = AskPrice;
    }

    public Double getLastPrice() {
        return LastPrice;
    }

    public void setLastPrice(Double LastPrice) {
        this.LastPrice = LastPrice;
    }

    public Double getMidPrice() {
        return MidPrice;
    }

    /**
     * @return the Prices
     */
    public void setMidPrice(Double MidPrice) {
        this.MidPrice = MidPrice;
    }

    public Double getPrices() {
        return Prices;
    }

    /**
     * @param Prices the Prices to set
     */
    public void setPrices(Double Prices) {
        this.Prices = Prices;
    }
}
