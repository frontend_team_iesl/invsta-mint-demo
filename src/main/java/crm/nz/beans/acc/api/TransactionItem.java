/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.beans.acc.api;

import static crm.nz.beans.acc.api.ToObjectConverter.checkNull;
import crm.nz.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADMIN
 */
public class TransactionItem implements ToObjectConverter {

    String Id, InvestmentCode, EffectiveDate, Type, TypeDisplayName, SubType, SubTypeDisplayName, PortfolioCode, PortfolioName,
            TransactionReasonType, TransactionSourceType, TransactionMethodType, TransactionDescription, TransactionDisplayName,
            TransactionTypeDescription, PaymentReference, ExternalReference,CreatedDate;

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }
    Double Units, Price, Value, Cash, Tax, Fee, AccruedIncome, CashClearing, PaymentClearing, PortfolioAccount, TaxRebate,TotalUnits;

    public Double getTotalUnits() {
        return TotalUnits;
    }

    public void setTotalUnits(Double TotalUnits) {
        this.TotalUnits = TotalUnits;
    }
    int TotalItemCount;

    public int getTotalItemCount() {
        return TotalItemCount;
    }

    public void setTotalItemCount(int TotalItemCount) {
        this.TotalItemCount = TotalItemCount;
    }
    Boolean isPending;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TransactionItem.class);

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getInvestmentCode() {
        return InvestmentCode;
    }

    public void setInvestmentCode(String InvestmentCode) {
        this.InvestmentCode = InvestmentCode;
    }

    public String getEffectiveDate() {
        return EffectiveDate;
    }

    public void setEffectiveDate(String EffectiveDate) {
        this.EffectiveDate = EffectiveDate;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getTypeDisplayName() {
        return TypeDisplayName;
    }

    public void setTypeDisplayName(String TypeDisplayName) {
        this.TypeDisplayName = TypeDisplayName;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String SubType) {
        this.SubType = SubType;
    }

    public String getSubTypeDisplayName() {
        return SubTypeDisplayName;
    }

    public void setSubTypeDisplayName(String SubTypeDisplayName) {
        this.SubTypeDisplayName = SubTypeDisplayName;
    }

    public String getPortfolioCode() {
        return PortfolioCode;
    }

    public void setPortfolioCode(String PortfolioCode) {
        this.PortfolioCode = PortfolioCode;
    }

    public String getPortfolioName() {
        return PortfolioName;
    }

    public void setPortfolioName(String PortfolioName) {
        this.PortfolioName = PortfolioName;
    }

    public String getTransactionReasonType() {
        return TransactionReasonType;
    }

    public void setTransactionReasonType(String TransactionReasonType) {
        this.TransactionReasonType = TransactionReasonType;
    }

    public String getTransactionSourceType() {
        return TransactionSourceType;
    }

    public void setTransactionSourceType(String TransactionSourceType) {
        this.TransactionSourceType = TransactionSourceType;
    }

    public String getTransactionMethodType() {
        return TransactionMethodType;
    }

    public void setTransactionMethodType(String TransactionMethodType) {
        this.TransactionMethodType = TransactionMethodType;
    }

    public String getTransactionDescription() {
        return TransactionDescription;
    }

    public void setTransactionDescription(String TransactionDescription) {
        this.TransactionDescription = TransactionDescription;
    }

    public String getTransactionDisplayName() {
        return TransactionDisplayName;
    }

    public void setTransactionDisplayName(String TransactionDisplayName) {
        this.TransactionDisplayName = TransactionDisplayName;
    }

    public String getTransactionTypeDescription() {
        return TransactionTypeDescription;
    }

    public void setTransactionTypeDescription(String TransactionTypeDescription) {
        this.TransactionTypeDescription = TransactionTypeDescription;
    }

    public String getPaymentReference() {
        return PaymentReference;
    }

    public void setPaymentReference(String PaymentReference) {
        this.PaymentReference = PaymentReference;
    }

    public String getExternalReference() {
        return ExternalReference;
    }

    public void setExternalReference(String ExternalReference) {
        this.ExternalReference = ExternalReference;
    }

    public Double getUnits() {
        return Units;
    }

    public void setUnits(Double Units) {
        this.Units = Units;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }

    public Double getValue() {
        return Value;
    }

    public void setValue(Double Value) {
        this.Value = Value;
    }

    public Double getCash() {
        return Cash;
    }

    public void setCash(Double Cash) {
        this.Cash = Cash;
    }

    public Double getTax() {
        return Tax;
    }

    public void setTax(Double Tax) {
        this.Tax = Tax;
    }

    public Double getFee() {
        return Fee;
    }

    public void setFee(Double Fee) {
        this.Fee = Fee;
    }

    public Double getAccruedIncome() {
        return AccruedIncome;
    }

    public void setAccruedIncome(Double AccruedIncome) {
        this.AccruedIncome = AccruedIncome;
    }

    public Double getCashClearing() {
        return CashClearing;
    }

    public void setCashClearing(Double CashClearing) {
        this.CashClearing = CashClearing;
    }

    public Double getPaymentClearing() {
        return PaymentClearing;
    }

    public void setPaymentClearing(Double PaymentClearing) {
        this.PaymentClearing = PaymentClearing;
    }

    public Double getPortfolioAccount() {
        return PortfolioAccount;
    }

    public void setPortfolioAccount(Double PortfolioAccount) {
        this.PortfolioAccount = PortfolioAccount;
    }

    public Double getTaxRebate() {
        return TaxRebate;
    }

    public void setTaxRebate(Double TaxRebate) {
        this.TaxRebate = TaxRebate;
    }

    public Boolean getIsPending() {
        return isPending;
    }

    public void setIsPending(Boolean isPending) {
        this.isPending = isPending;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
