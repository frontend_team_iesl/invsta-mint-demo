/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.config.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidParameterException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;
import com.google.gson.Gson;
import crm.nz.beans.acc.api.CognitoUserPoolBean;
import crm.nz.beans.acc.api.DataZooServicebean;
import crm.nz.beans.acc.api.NZBNApiBean;
import crm.nz.beans.acc.api.OAuth2TokenServiceBean;
import java.util.Base64;
import java.util.HashMap;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author innovative002
 */
@Configuration
public class AWSSecretsManagerConfig {

    {
        System.out.println("AWSSecretsManagerConfig");
    }

    private Gson gson = new Gson();
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(AWSSecretsManagerConfig.class);
    private String region = "ap-southeast-2";
    private String awsAccessKey = "AKIAI5GUSB5HL2LJPIVA";
    private String awsSecretKey = "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";
    private AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
    // Create a Secrets Manager client
    private AWSSecretsManager secretsManagerClient = AWSSecretsManagerClientBuilder.standard()
            .withRegion(region)
            .withCredentials(credentialsProvider)
            .build();

// Use this code snippet in your app.
// If you need more information about configurations or implementing the sample code, visit the AWS docs:
// https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-samples.html#prerequisites
//    public AWSSecretsManagerConfig(){} 
    private HashMap<String, String> getRetrievedSecretMap(String secretName) {
        String secret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;
        try {
            getSecretValueResult = secretsManagerClient.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }
        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
        } else {
            secret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }
        final HashMap<String, String> secretMap = gson.fromJson(secret, HashMap.class);
        return secretMap;
    }

    @Bean(name = "dataSource")
    public BasicDataSource dataSource() {
//        String secretName0 = "mysql/production";
        String secretName0 = "mysql/testing";
//        String secretName0 = "mysql/staging";  
//        String secretName0 = "mysql/localserver";  
        HashMap<String, String> secretMap0 = getRetrievedSecretMap(secretName0);
        final String url = secretMap0.get("url").concat(secretMap0.get("invstaFund"));
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(secretMap0.get("driverClassName"));
        dataSource.setUrl(url);
        dataSource.setUsername(secretMap0.get("username"));
        dataSource.setPassword(secretMap0.get("password"));
        dataSource.setInitialSize(10);
        dataSource.setMaxActive(25);
        dataSource.setMaxIdle(20);
        dataSource.setMinIdle(10);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
        System.out.println(dataSource);
        return dataSource;
    }

    @Bean(name = "oAuth2TokenServiceBean")
    public OAuth2TokenServiceBean getOAuth2Token1() {
        String secretName3 = "apikey/MintOAuth2TokenService1";
        HashMap<String, String> secretMap3 = getRetrievedSecretMap(secretName3);
        OAuth2TokenServiceBean oAuth2TokenServiceBean = new OAuth2TokenServiceBean(secretMap3.get("apiUrl"), secretMap3.get("clientId"), secretMap3.get("clientSecret"));
        return oAuth2TokenServiceBean;
    }
    
    @Bean(name = "oAuth2TokenServiceBean2")
    public OAuth2TokenServiceBean getOAuth2Token2() {
        String secretName3 = "apikey/MintOAuth2TokenService2";
        HashMap<String, String> secretMap3 = getRetrievedSecretMap(secretName3);
        OAuth2TokenServiceBean oAuth2TokenServiceBean = new OAuth2TokenServiceBean(secretMap3.get("apiUrl"), secretMap3.get("clientId"), secretMap3.get("clientSecret"));
        return oAuth2TokenServiceBean;
    }

    @Bean(name = "cognitoUserPoolBean")
    public CognitoUserPoolBean getUserPoolToken() {
        String secretName3 = "cognito/userpool";
        HashMap<String, String> secretMap3 = getRetrievedSecretMap(secretName3);
        CognitoUserPoolBean cognitoUserPoolBean = new CognitoUserPoolBean(secretMap3.get("poolId"), secretMap3.get("clientId"));
        return cognitoUserPoolBean;
    }

    @Bean(name = "nZBNApiBean")
    public NZBNApiBean getNZBN() {
        String secretName1 = "apikey/auth/nzbn";
        HashMap<String, String> secretMap1 = getRetrievedSecretMap(secretName1);
        NZBNApiBean nZBNApiBean = new NZBNApiBean(secretMap1.get("tokenUrl"), secretMap1.get("clientId"), secretMap1.get("clientSecret"));
        return nZBNApiBean;
    }

    @Bean(name = "dataZooServicebean")
    public DataZooServicebean getDataZooService() {
        String secretName2 = "apikey/DataZooService";
        HashMap<String, String> secretMap2 = getRetrievedSecretMap(secretName2);
        DataZooServicebean dataZooServicebean = new DataZooServicebean(secretMap2.get("username"), secretMap2.get("password"), secretMap2.get("apiUrl"));
        return dataZooServicebean;
    }

    @Bean(name = "s3Client")
    public AmazonS3 getAmazonS3Client() {
//        AmazonS3 s3client = new AmazonS3Client(credentialsProvider);
        AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(credentialsProvider)
                .build();
        return s3client;
    }
}
