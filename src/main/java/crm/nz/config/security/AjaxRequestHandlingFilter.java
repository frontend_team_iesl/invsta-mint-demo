/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.config.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author IESL
 */
public class AjaxRequestHandlingFilter implements Filter {

    private int errorCode = 401;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            String ajaxHeader = request.getHeader("X-Requested-With");
            if ("XMLHttpRequest".equals(ajaxHeader)) {
                String principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
                System.out.println(principal);
                if ("anonymousUser".equals(principal.toString())) {
                    response.setStatus(this.errorCode);
                    response.sendError(this.errorCode, "Ajax time out");
                    SecurityContextHolder.clearContext();
                    throw new AccessDeniedException("Ajax request time out.");
                }
            }
            filterChain.doFilter(request, response);
        } catch (IOException e) {
            throw e;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        System.out.println("AjaxRequestHandlingFilter");
        System.out.println(config);
    }

    @Override
    public void destroy() {
        
    }

}
