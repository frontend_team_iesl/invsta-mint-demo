/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crm.nz.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

/**
 *
 * @author innovative002
 */
@Configuration
public class SpringSecurityConfig {

    {
        System.out.println("SpringSecurityConfig");
    }

    @Bean(name = "sessionRegistry")
    public SessionRegistry getSessionRegistry() {
        SessionRegistryImpl sessionRegistryImpl = new SessionRegistryImpl();
        System.out.println("sessionRegistryImpl---->" + sessionRegistryImpl);
        return sessionRegistryImpl;
    }

}
